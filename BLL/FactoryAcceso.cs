﻿using DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTIL;
using UTIL.Models;
using UTIL.Objetos;

namespace BLL
{
    public class FactoryAcceso
    {

        #region Nota de Venta

        #region anterior

        public List<_NotaDeVentaDetalleModels> DatosCorreoVend(int NvNUmero, string basedatos)
        {
            var DatosUser = new List<_NotaDeVentaDetalleModels>();
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_DatosCorreoVend", new System.Collections.Hashtable()
                {
                    { "NvNumero", NvNUmero},
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];
                if (data.Rows.Count > 0)
                {
                    for (var i = 0; i < data.Rows.Count; i++)
                    {
                        var validador = new object();
                        var resultadoListado = new _NotaDeVentaDetalleModels();

                        validador = data.Rows[i].Field<object>("email");
                        resultadoListado.EmailVend = validador != null ? data.Rows[i].Field<string>("email") : "NO ASIGNADO";

                        validador = data.Rows[i].Field<object>("ContrasenaCorreo");
                        resultadoListado.PassCorreo = validador != null ? data.Rows[i].Field<string>("ContrasenaCorreo") : "NO ASIGNADO";

                        DatosUser.Add(resultadoListado);
                    }
                }
            }
            catch (SqlException ex)
            {
                new CapturaExcepciones(ex);
            }
            catch (Exception ex)
            {
                new CapturaExcepciones(ex);
            }
            return DatosUser;
        }

        public DataTable obtenerExcelVentas(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado1("reporteStock", new System.Collections.Hashtable()
                {
                 { "pb_baseDatos", baseDatos},


                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<StockModel> obtenerProductosStock(string codprod,string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("productosStock", new System.Collections.Hashtable()
                {
                    {"codProd", codprod },
                     {"pb_baseDatos", baseDatos },
                }).Tables[0];
                return UTIL.Mapper.BindDataList<StockModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public StockModel obtenerProdStock(string codprod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("productosStock", new System.Collections.Hashtable()
                {
                    {"codProd", codprod }
                }).Tables[0];
                return UTIL.Mapper.BindData<StockModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<_NotaDeVentaDetalleModels> DatosCorreoAprobador(string VendCod)
        {
            var DatosUser = new List<_NotaDeVentaDetalleModels>();
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_DatosCorreoAprobador", new System.Collections.Hashtable()
                                                                                            {
                                                                                                {"VendCod", int.Parse(VendCod)}
                                                                                            }).Tables[0];
                if (data.Rows.Count > 0)
                {
                    for (var i = 0; i < data.Rows.Count; i++)
                    {
                        var validador = new object();
                        var resultadoListado = new _NotaDeVentaDetalleModels();

                        validador = data.Rows[i].Field<object>("email");
                        resultadoListado.EmailVend = validador != null ? data.Rows[i].Field<string>("email") : "NO ASIGNADO";

                        validador = data.Rows[i].Field<object>("ContrasenaCorreo");
                        resultadoListado.PassCorreo = validador != null ? data.Rows[i].Field<string>("ContrasenaCorreo") : "NO ASIGNADO";

                        DatosUser.Add(resultadoListado);
                    }
                }
            }
            catch (SqlException ex)
            {
                new CapturaExcepciones(ex);
            }
            catch (Exception ex)
            {
                new CapturaExcepciones(ex);
            }
            return DatosUser;
        }

        public bool ActualizaCorreo(_UsuariosModels Usuario, string basedatos)
        {
            bool respuesta = false;
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_SET_ActualizaCorreo", new System.Collections.Hashtable()
                {
                    { "VendCod", Usuario.VenCod.Trim()},
                    { "Email", Usuario.email},
                    { "Contrasena", Usuario.Password},
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];
                if (data.Rows.Count > 0)
                {
                    respuesta = true;
                }
            }
            catch (SqlException ex)
            {
                new CapturaExcepciones(ex);
                respuesta = false;
            }
            catch (Exception ex)
            {
                new CapturaExcepciones(ex);
                respuesta = false;
            }
            return respuesta;
        }

        #endregion


        public List<ObjetoMenu> MenuUsuario(int idUsuario,int idempresa)
        {
            var listadoMenu = new List<ObjetoMenu>();
            var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_MenuII", new System.Collections.Hashtable()
               {
                     {"IdUsuario", idUsuario},
                      {"id_empresa",idempresa}
                      }).Tables[0];                                                                      
                                                                           
            if (data.Rows.Count > 0)
            {
                for (var i = 0; i < data.Rows.Count; i++)
                {
                    var validador = new object();
                    var resultadoListado = new ObjetoMenu();
                    validador = data.Rows[i].Field<object>("Id_Menu");
                    resultadoListado.IdMenu = validador != null ? data.Rows[i].Field<int>("Id_Menu") : -1;

                    validador = data.Rows[i].Field<object>("Clase");
                    resultadoListado.Clase = validador != null ? data.Rows[i].Field<string>("Clase") : "NO ASIGNADO";

                    validador = data.Rows[i].Field<object>("PieMenu");
                    resultadoListado.PieMenu = validador != null ? data.Rows[i].Field<string>("PieMenu") : "NO ASIGNADO";

                    validador = data.Rows[i].Field<object>("Titulo");
                    resultadoListado.Titulo = validador != null ? data.Rows[i].Field<string>("Titulo") : "NO ASIGNADO";

                    validador = data.Rows[i].Field<object>("Action");
                    resultadoListado.Action = validador != null ? data.Rows[i].Field<string>("Action") : "NO ASIGNADO";

                    validador = data.Rows[i].Field<object>("Controller");
                    resultadoListado.Controller = validador != null ? data.Rows[i].Field<string>("Controller") : "NO ASIGNADO";


                    listadoMenu.Add(resultadoListado);
                }
            }
            return listadoMenu;
        }

        public List<CentrodeCostoModels> ListarCentroCosto(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarCentroDeCosto", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];

                return UTIL.Mapper.BindDataList<CentrodeCostoModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<CanalVentaModels> ListarCanalVenta(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_ObtenerCanalVenta", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];

                return UTIL.Mapper.BindDataList<CanalVentaModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> GetClientes(string basedatos, ClientesModels cliente = null)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("JS_ListarClientesCodAuxRut", new System.Collections.Hashtable()
                {
                    { "vchrRutAux", cliente.RutAux},
                    { "vchrCodAux", cliente.CodAux},
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> GetContacto(string basedatos, ClientesModels cliente = null)
        {
            try
            {
                //var data = new DBConector().EjecutarProcedimientoAlmacenado("JS_ListarContactos", new System.Collections.Hashtable()
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_ListaContactos", new System.Collections.Hashtable()
                {
                    { "vchrCodAux", cliente.CodAux},
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> GetCliente(ClientesModels cliente = null)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("JS_ListarClientesCodAux", new System.Collections.Hashtable()
                {
                    { "vchrCodAux", cliente.CodAux},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> listarClientes(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarClientes", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<EmpresaModel> ListarEmpresas()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_ListaEmpresa", new System.Collections.Hashtable()
                {
                }).Tables[0];
                return UTIL.Mapper.BindDataList<EmpresaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> BuscarClientes(ClientesModels clientes)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarClientes", new System.Collections.Hashtable()
                {
                    { "RutAux", clientes.RutAux},
                    { "CodAux", clientes.CodAux},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<MonedaModel> ListarMonedas(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_Monedas", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<MonedaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<GrupoModel> ListarGrupos(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_Grupos", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<GrupoModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<SubGrupoModel> ListarSubGrupos(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_SubGrupos", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<SubGrupoModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> ListarClientesTodos()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarClientesTodos", new System.Collections.Hashtable()
                {
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        //Procedimiento devuelve clientes por su id//
        public List<ClientesModels> BuscarMisClientes(ClientesModels RutAux)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarMisClientes", new System.Collections.Hashtable()
                {
                    { "ID", RutAux.ID},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        //Busca los clientes por el VenCod del Usuario//
        public List<ClientesModels> BuscarMisClientesVenCod(UsuariosModels usuario, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("JS_ListarMisClientes", new System.Collections.Hashtable()
                {
                    { "cod", usuario.VenCod},
                    { "ID", usuario.id},
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> BuscarContacto(string basedatos, ClientesModels contacto)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarContactos", new System.Collections.Hashtable()
                {
                    { "CodAuc", contacto.CodAux},
                    { "NomCon", contacto.NomAux},
                    {"pv_BaseDatos",basedatos }

                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> AgregarContacto(ClientesModels contacto)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_AgregarContactos", new System.Collections.Hashtable()
                {
                    { "CodAux", contacto.CodAux},
                    { "NomCon", contacto.NomCon},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<DireccionDespachoModels> BuscarDirecDespach(DireccionDespachoModels DirDes, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarDirecDespa", new System.Collections.Hashtable()
                {
                    { "CodAxD", DirDes.CodAxD},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<DireccionDespachoModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<DireccionDespachoModels> BuscarDirecDespachMail(DireccionDespachoModels DirDes, string baseDatos, string NomDch)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarDirecDespaMail", new System.Collections.Hashtable()
                {
                    { "CodAxD", DirDes.CodAxD},
                    { "pv_BaseDatos", baseDatos},
                    { "NomDch",NomDch }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<DireccionDespachoModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel AgregarDireccionDespacho(DireccionDespachoModels DirDes, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_AgregarDireccionDespacho", new System.Collections.Hashtable()
                {
                    { "pv_CodAux", DirDes.CodAxD},
                    { "pv_DirDch", DirDes.DirDch},
                    { "pv_ComDch", DirDes.ComDch},
                    { "pv_NomDch", DirDes.NomDch},
                    { "pv_CiuDch", DirDes.CiuDch},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel ActualizarCliente(ClientesModels cliente, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ActualizarCliente", new System.Collections.Hashtable()
                {
                    { "CodAux", cliente.CodAux},
                    { "RutAux", cliente.RutAux},
                    { "NomAux", cliente.NomAux},
                    { "DirAux", cliente.DirAux},
                    { "NomCon", cliente.NomCon},
                    { "FonCon", cliente.FonCon},
                    { "Email", cliente.EMail},
                    { "pv_BaseDatos", basedatos}
            }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<CondicionVentasModels> listarConVen(string baseDatos, CondicionVentasModels conven)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarCondicionesDeVenta", new System.Collections.Hashtable()
                {
                    { "CodAux", conven.CodAux},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<CondicionVentasModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<CondicionVentasModels> listarConVenPret(string baseDatos, CondicionVentasModels conven)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarCondicionesDeVentaPret", new System.Collections.Hashtable()
                {
                    { "CodAux", conven.CodAux},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<CondicionVentasModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ListaDePrecioModels> listarListaDePrecio(string baseDatos, ListaDePrecioModels lista)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarListaDePrecio", new System.Collections.Hashtable()
                {
                    { "CodAux", lista.CodAux},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ListaDePrecioModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public ObjetoUsuario Login(ObjetoUsuario datosUsuarios)
        {
            var DatosLogin = new ObjetoUsuario();
            var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_LOGIN", new System.Collections.Hashtable()
                                                                                            {
                                                                                                {"Nombre", datosUsuarios.Nombre},
                                                                                                {"Contrasena", datosUsuarios.Contrasena }
                                                                                            }).Tables[0];

            if (data.Rows.Count > 0)
            {
                var validador = new object();

                validador = data.Rows[0].Field<object>("Id");
                DatosLogin.IdUsuario = validador != null ? data.Rows[0].Field<int>("Id") : 0;

                validador = data.Rows[0].Field<object>("Usuario");
                DatosLogin.Nombre = validador != null ? data.Rows[0].Field<string>("Usuario") : "NO ASIGNADO";

                validador = data.Rows[0].Field<object>("TipoUsuario");
                DatosLogin.TipoUsuario = validador != null ? data.Rows[0].Field<int>("TipoUsuario") : -1;

                validador = data.Rows[0].Field<object>("CodigoUsuario");
                DatosLogin.CodigoUsuario = validador != null ? data.Rows[0].Field<string>("CodigoUsuario") : "";

            }
            else
            {
                DatosLogin = null;
            }

            return DatosLogin;
        }

        public RespuestaNotaVentaModel AgregarNV(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotadeVentaCabeceraModels NVC)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_AgregarNVCabecera", new System.Collections.Hashtable()
                {
                    {"pi_IdEmpresaInterna",NVC.IdEmpresaInterna },
                    { "pv_EstadoNP", NVC.EstadoNP},
                    { "pv_BaseDatos", baseDatos},
                    { "pb_InsertaDisofi", insertaDisofi},
                    { "pb_InsertaSoftland", insertaSoftland},
                    { "pi_NVNumero", NVC.NVNumero},
                    { "pd_nvFem", NVC.nvFemYYYYMMDD},
                    { "pv_nvEstado", NVC.nvEstado},
                    { "pi_nvEstFact", NVC.nvEstFact},
                    { "pi_nvEstDesp", NVC.nvEstDesp},
                    { "pi_nvEstRese", NVC.nvEstRese},
                    { "pi_nvEstConc", NVC.nvEstConc},
                    { "pi_CotNum", NVC.CotNum},
                    { "pv_NumOC", NVC.NumOC},
                    { "pd_nvFeEnt", NVC.nvFeEntYYYYMMDD},
                    { "pv_CodAux", NVC.CodAux},
                    { "pv_VenCod", NVC.VenCod},
                    { "pv_CodMon", NVC.CodMon},
                    { "pv_CodLista", NVC.CodLista},
                    { "pt_nvObser", NVC.nvObser},
                    { "pv_nvCanalNV", NVC.nvCanalNV},
                    { "pv_CveCod", NVC.CveCod},
                    { "pv_NomCon", NVC.NomCon},
                    { "pv_CodiCC", NVC.CodiCC},
                    { "pv_CodBode", NVC.CodBode},
                    { "pf_nvSubTotal", NVC.nvSubTotal},
                    { "pf_nvPorcDesc01", NVC.nvPorcDesc01},
                    { "pf_nvDescto01", NVC.nvDescto01},
                    { "pf_nvPorcDesc02", NVC.nvPorcDesc02},
                    { "pf_nvDescto02", NVC.nvDescto02},
                    { "pf_nvPorcDesc03", NVC.nvPorcDesc03},
                    { "pf_nvDescto03", NVC.nvDescto03},
                    { "pf_nvPorcDesc04", NVC.nvPorcDesc04},
                    { "pf_nvDescto04", NVC.nvDescto04},
                    { "pf_nvPorcDesc05", NVC.nvPorcDesc05},
                    { "pf_nvDescto05", NVC.nvDescto05},
                    { "pf_nvMonto", NVC.nvMonto},
                    { "pd_nvFeAprob", NVC.nvFeAprobYYYYMMDD},
                    { "pi_NumGuiaRes", NVC.NumGuiaRes},
                    { "pf_nvPorcFlete", NVC.nvPorcFlete},
                    { "pf_nvValflete", NVC.nvValflete},
                    { "pf_nvPorcEmb", NVC.nvPorcEmb},
                    { "pf_nvValEmb", NVC.nvValEmb},
                    { "pf_nvEquiv", NVC.nvEquiv},
                    { "pf_nvNetoExento", NVC.nvNetoExento},
                    { "pf_nvNetoAfecto", NVC.nvNetoAfecto},
                    { "pf_nvTotalDesc", NVC.nvTotalDesc},
                    { "pv_ConcAuto", NVC.ConcAuto},
                    { "pv_CodLugarDesp", NVC.CodLugarDesp},
                    { "pv_SolicitadoPor", NVC.SolicitadoPor},
                    { "pv_DespachadoPor", NVC.DespachadoPor},
                    { "pv_Patente", NVC.Patente},
                    { "pv_RetiradoPor", NVC.RetiradoPor},
                    { "pv_CheckeoPorAlarmaVtas", NVC.CheckeoPorAlarmaVtas},
                    { "pi_EnMantencion", NVC.EnMantencion},
                    { "pv_Usuario", NVC.Usuario},
                    { "pv_UsuarioGeneraDocto", NVC.UsuarioGeneraDocto},
                    { "pd_FechaHoraCreacion", NVC.FechaHoraCreacionYYYYMMDD},
                    { "pv_Sistema", NVC.Sistema},
                    { "pv_ConcManual", NVC.ConcManual},
                    { "pv_RutSolicitante", NVC.RutSolicitante},
                    { "pv_proceso", NVC.proceso},
                    { "pf_TotalBoleta", NVC.TotalBoleta},
                    { "pi_NumReq", NVC.NumReq},
                    { "pv_CodVenWeb", NVC.CodVenWeb},
                    { "pv_CodBodeWms", NVC.CodBodeWms},
                    { "pv_CodLugarDocto", NVC.CodLugarDocto},
                    { "pv_RutTransportista", NVC.RutTransportista},
                    { "pv_Cod_Distrib", NVC.Cod_Distrib},
                    { "pv_Nom_Distrib", NVC.Nom_Distrib},
                    { "pi_MarcaWG", NVC.MarcaWG},
                    { "pb_ErrorAprobador", NVC.ErrorAprobador},
                    { "pv_ErrorAprobadorMensaje", NVC.ErrorAprobadorMensaje},
                    { "pv_IdCorreoManag", NVC.CorreoManag},
                    { "NroFinalCotizacion", NVC.NroFinalCotizacion}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaNotaVentaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }

        }

        public List<NotadeVentaCabeceraModels> EditarNV(NotadeVentaCabeceraModels NVC)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ModificarNVCabecera", new System.Collections.Hashtable()
                {
                    { "NVNumero", NVC.NVNumero},
                    { "nvFem", NVC.nvFem},
                    { "nvEstado", NVC.nvEstado},
                    { "nvEstFact", NVC.nvEstFact},
                    { "nvEstDesp", NVC.nvEstDesp},
                    { "nvEstRese", NVC.nvEstRese},
                    { "nvEstConc", NVC.nvEstConc},
                    { "nvFeEnt", NVC.nvFeEnt},
                    { "CodAux", NVC.CodAux},
                    { "VenCod", NVC.VenCod},
                    { "CodMon", NVC.CodMon},
                    { "CodLista", NVC.CodLista},
                    { "nvObser", NVC.nvObser},
                    { "CveCod", NVC.CveCod},
                    { "NomCon", NVC.NomCon},
                    { "CodiCC", NVC.CodiCC},
                    { "nvSubTotal", NVC.nvSubTotal},
                    { "nvPorcDesc01", NVC.nvPorcDesc01},
                    { "nvDescto01", NVC.nvDescto01},
                    { "nvPorcDesc02", NVC.nvPorcDesc02},
                    { "nvDescto02", NVC.nvDescto02},
                    { "nvPorcDesc03", NVC.nvPorcDesc03},
                    { "nvDescto03", NVC.nvDescto03},
                    { "nvPorcDesc04", NVC.nvPorcDesc04},
                    { "nvDescto04", NVC.nvDescto04},
                    { "nvPorcDesc05", NVC.nvPorcDesc05},
                    { "nvDescto05", NVC.nvDescto05},
                    { "nvMonto", NVC.nvMonto},
                    { "NumGuiaRes", NVC.NumGuiaRes},
                    { "nvPorcFlete", NVC.nvPorcFlete},
                    { "nvValflete", NVC.nvValflete},
                    { "nvPorcEmb", NVC.nvPorcEmb},
                    { "nvEquiv", NVC.nvEquiv},
                    { "nvNetoExento", NVC.nvNetoExento},
                    { "nvNetoAfecto", NVC.nvNetoAfecto},
                    { "nvTotalDesc", NVC.nvTotalDesc},
                    { "ConcAuto", NVC.ConcAuto},
                    { "CheckeoPorAlarmaVtas", NVC.CheckeoPorAlarmaVtas},
                    { "EnMantencion", NVC.EnMantencion},
                    { "Usuario", NVC.Usuario},
                    { "UsuarioGeneraDocto", NVC.UsuarioGeneraDocto},
                    { "FechaHoraCreacion", NVC.FechaHoraCreacion},
                    { "Sistema", NVC.Sistema},
                    { "ConcManual", NVC.ConcManual},
                    { "proceso", NVC.proceso},
                    { "TotalBoleta", NVC.TotalBoleta},
                    { "NumReq", NVC.NumReq},
                    { "CodVenWeb", NVC.CodVenWeb},
                    { "EstadoNP", NVC.EstadoNP},
                    { "CodLugarDesp", NVC.CodLugarDesp},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                throw ex;
            }

        }

        public RespuestaNotaVentaModel AgregarImpuesto(string baseDatos, NotadeVentaCabeceraModels NV)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_AgregaImpuestoNV", new System.Collections.Hashtable()
                {
                    /*--------------------------- CAMPOS DISOFI ---------------------------*/
                    { "pv_BaseDatos", baseDatos},
                    { "pv_nvNumero", NV.NVNumero},
                    { "pi_IdNotaVenta", NV.Id},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaNotaVentaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaNotaVentaModel AgregarDetalleNV(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotaDeVentaDetalleModels NVD)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_AgregarNVDetalle", new System.Collections.Hashtable()
                {
                    /*--------------------------- CAMPOS DISOFI ---------------------------*/
                    { "pv_BaseDatos", baseDatos},
                    { "pb_InsertaDisofi", insertaDisofi},
                    { "pb_InsertaSoftland", insertaSoftland},
                    { "pi_IdNotaVenta", NVD.IdNotaVenta},
                    { "pi_NVNumero", NVD.NVNumero},
                    { "pf_nvLinea", NVD.nvLinea},
                    { "pf_nvCorrela", NVD.nvCorrela},
                    { "pd_nvFecCompr", NVD.nvFecComprYYYYMMDD},
                    { "pv_CodProd", NVD.CodProd},
                    { "pf_nvCant", NVD.nvCant},
                    { "pf_nvPrecio", NVD.nvPrecio},
                    { "pf_nvEquiv", NVD.nvEquiv},
                    { "pf_nvSubTotal", NVD.nvSubTotal},
                    { "pf_nvDPorcDesc01", NVD.nvDPorcDesc01},
                    { "pf_nvDDescto01", NVD.nvDDescto01},
                    { "pf_nvDPorcDesc02", NVD.nvDPorcDesc02},
                    { "pf_nvDDescto02", NVD.nvDDescto02},
                    { "pf_nvDPorcDesc03", NVD.nvDPorcDesc03},
                    { "pf_nvDDescto03", NVD.nvDDescto03},
                    { "pf_nvDPorcDesc04", NVD.nvDPorcDesc04},
                    { "pf_nvDDescto04", NVD.nvDDescto04},
                    { "pf_nvDPorcDesc05", NVD.nvDPorcDesc05},
                    { "pf_nvDDescto05", NVD.nvDDescto05},
                    { "pf_nvTotDesc", NVD.nvTotDesc},
                    { "pf_nvTotLinea", NVD.nvTotLinea},
                    { "pf_nvCantDesp", NVD.nvCantDesp},
                    { "pf_nvCantProd", NVD.nvCantProd},
                    { "pf_nvCantFact", NVD.nvCantFact},
                    { "pf_nvCantDevuelto", NVD.nvCantDevuelto},
                    { "pf_nvCantNC", NVD.nvCantNC},
                    { "pf_nvCantBoleta", NVD.nvCantBoleta},
                    { "pt_DetProd", NVD.DetProd},
                    { "pv_CheckeoMovporAlarmaVtas", NVD.CheckeoMovporAlarmaVtas},
                    { "pv_KIT", NVD.KIT},
                    { "pi_CodPromocion", NVD.CodPromocion},
                    { "pv_CodUMed", NVD.CodUMed},
                    { "pf_CantUVta", NVD.CantUVta},
                    { "pv_Partida", NVD.Partida},
                    { "pv_Pieza", NVD.Pieza},
                    { "pd_FechaVencto", NVD.FechaVenctoYYYYMMDD},
                    { "pf_CantidadKit", NVD.CantidadKit},
                    { "pi_MarcaWG", NVD.MarcaWG},
                    { "pf_PorcIncidenciaKit", NVD.PorcIncidenciaKit},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaNotaVentaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel AgregarDetalleNVValorAdicional(string baseDatos, int idDetalleNotaVenta, int valorAdicional)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("uSP_INS_NotaVentaDetalleValorAdicional", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", baseDatos},
                    { "pi_IdDetalleNotaVenta", idDetalleNotaVenta},
                    { "pi_ValorAdicional", valorAdicional},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> BuscarNVPorNumero(int Id, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("JS_ListarNVNM", new System.Collections.Hashtable()
                {
                    { "nvId",Id},
                    {"pv_BaseDatos", basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }

        }

        public List<NotadeVentaCabeceraModels> BuscarNVNum(NotadeVentaCabeceraModels NVSoft)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_BuscaNVNumSoft", new System.Collections.Hashtable()
                {
                    { "NVNum", NVSoft.NVNumero},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }

        }

        public List<NotaDeVentaDetalleModels> BuscarNVDETALLEPorNumero(int Id, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("JS_ListarNVDETALLENM", new System.Collections.Hashtable()
                {
                    { "nvId", Id},
                    {"pv_BaseDatos", basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotaDeVentaDetalleModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }

        }

        public List<NotadeVentaCabeceraModels> InsertarNvSoftland(NotadeVentaCabeceraModels NVC)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("insertaNVSoftland", new System.Collections.Hashtable()
                {
                    { "nvNumero", NVC.NVNumero},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }

        }

        public ParametrosModels BuscarParametros(int idEmpresa)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarParametrosUsuarios", new System.Collections.Hashtable()
                {
                    { "pi_idEmpresa", idEmpresa},
                }).Tables[0];
                return UTIL.Mapper.BindData<ParametrosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<menuModels> BuscarMenus()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarMenus", new System.Collections.Hashtable()
                {
                   
                }).Tables[0];
                return UTIL.Mapper.BindDataList<menuModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        
        public RespuestaModel ModificarParametros(ParametrosModels parametro)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ModificarParametrosUsuarios", new System.Collections.Hashtable()
                {
                    { "pi_IdEmpresa", parametro.IdEmpresa},
                    { "pb_MultiEmpresa", parametro.MultiEmpresa},
                    { "pb_ManejaAdministrador", parametro.ManejaAdministrador},
                    { "pb_ManejaAprobador", parametro.ManejaAprobador},
                    { "pb_ListaClientesVendedor", parametro.ListaClientesVendedor},
                    { "pb_ListaClientesTodos", parametro.ListaClientesTodos},
                    { "pb_ValidaReglasNegocio", parametro.ValidaReglasNegocio},
                    { "pb_ManejaListaPrecios", parametro.ManejaListaPrecios},
                    { "pb_EditaPrecioProducto", parametro.EditaPrecioProducto},
                    { "pb_MuestraCondicionVentaCliente", parametro.MuestraCondicionVentaCliente},
                    { "pb_MuestraCondicionVentaTodos", parametro.MuestraCondicionVentaTodos},
                    { "pb_EditaDescuentoProducto", parametro.EditaDescuentoProducto},
                    { "pd_MaximoDescuentoProducto", parametro.MaximoDescuentoProducto},
                    { "pb_CantidadDescuentosProducto", parametro.CantidadDescuentosProducto},
                    { "pb_MuestraStockProducto", parametro.MuestraStockProducto},
                    { "pb_StockProductoEsMasivo", parametro.StockProductoEsMasivo},
                    { "pb_StockProductoEsBodega", parametro.StockProductoEsBodega},
                    { "pv_StockProductoCodigoBodega", parametro.StockProductoCodigoBodega},
                    { "pv_StockProductoCodigoBodegaAdicional", parametro.StockProductoCodigoBodegaAdicional},
                    { "pb_ControlaStockProducto", parametro.ControlaStockProducto},
                    { "pb_EnvioMailCliente", parametro.EnvioMailCliente},
                    { "pb_EnvioMailVendedor", parametro.EnvioMailVendedor},
                    { "pb_EnvioMailContacto", parametro.EnvioMailContacto},
                    { "pb_EnvioObligatorioAprobador", parametro.EnvioObligatorioAprobador},
                    { "pb_ManejaTallaColor", parametro.ManejaTallaColor},
                    { "pb_ManejaDescuentoTotalDocumento", parametro.ManejaDescuentoTotalDocumento},
                    { "pi_CantidadDescuentosTotalDocumento", parametro.CantidadDescuentosTotalDocumento},
                    { "pi_CantidadLineas", parametro.CantidadLineas},
                    { "pb_ManejaLineaCreditoVendedor", parametro.ManejaLineaCreditoVendedor},
                    { "pb_ManejaLineaCreditoAprobador", parametro.ManejaLineaCreditoAprobador},
                    { "pb_ManejaCanalVenta", parametro.ManejaCanalVenta},
                    { "pb_CreacionNotaVentaUsuariosBloqueados", parametro.CreacionNotaVentaUsuariosBloqueados},
                    { "pb_CreacionNotaVentaUsuariosInactivos", parametro.CreacionNotaVentaUsuariosInactivos},
                    { "pb_PermiteModificacionCondicionVenta", parametro.PermiteModificacionCondicionVenta},
                    { "pv_AtributoSoftlandDescuentoCliente", parametro.AtributoSoftlandDescuentoCliente},
                    { "pb_PermiteCrearDireccion", parametro.PermiteCrearDireccion},
                    { "pb_CrearClienteConDV", parametro.CrearClienteConDV},
                    { "pb_MuestraUnidadMedidaProducto", parametro.MuestraUnidadMedidaProducto},
                    { "pb_DescuentoLineaDirectoSoftland", parametro.DescuentoLineaDirectoSoftland},
                    { "pb_DescuentoTotalDirectoSoftland", parametro.DescuentoTotalDirectoSoftland},
                    { "pb_CambioVendedorCliente", parametro.CambioVendedorCliente},
                    { "pb_AgregaCliente", parametro.AgregaCliente},
                    { "pb_EnvioMailAprobador", parametro.EnvioMailAprobador},
                    { "pb_ManejaSaldo", parametro.ManejaSaldo},
                    { "pv_CodigoCondicionVentaPorDefecto", parametro.CodigoCondicionVentaPorDefecto},
                    { "pb_ManejaValorAdicional", parametro.ManejaValorAdicional},
                    { "pb_CorreosWebConfig", parametro.CorreosWebConfig },
                    { "pb_Booking", parametro.Booking },
                    { "pb_Backlog", parametro.Backlog },
                    { "pb_saldosClientes", parametro.saldosClientes },
                    { "pb_saldosProveedores", parametro.saldosProveedores },
                    { "pb_reporteStock", parametro.reporteStock },
                    { "pb_notaVenta", parametro.notaVenta },
                    { "pb_Dashboard", parametro.Dashboard },
                    { "pb_Cotizaciones", parametro.Cotizaciones},
                    { "pb_DashboardCobranza", parametro.DashboardCobranza},
                    { "pb_ReporteSaldo", parametro.ReporteSaldo}

                    


                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ProductosModels> BuscarProducto(ProductosModels producto)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarProducto", new System.Collections.Hashtable()
                {
                    { "DesProd", producto.DesProd},
                    { "CodLista", producto.CodLista},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ProductosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ProductosModels> ListarProducto(string ListaPrecio, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListaProductos", new System.Collections.Hashtable()
                {
                    { "pv_ListaProductos", ListaPrecio},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ProductosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<TallaColorProductoModels> ListarTallaColorProducto(string CodProd, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListaTallaColorProducto", new System.Collections.Hashtable()
                {
                    { "@pv_CodProd", CodProd},
                    { "@pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<TallaColorProductoModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ProductosModels> BuscarProductoRapido(ProductosModels producto)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarProductoRapido", new System.Collections.Hashtable()
                {
                    { "CodRapido", producto.CodRapido},
                    { "CodLista", producto.CodLista},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ProductosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> listarDocAprobados(string basedatos, int idEmpresa, string codigoVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarDocumentosAprobados", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos },
                    {"pi_IdEmpresaInterna",idEmpresa },
                    {"pv_CodigoVendedor",codigoVendedor },
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> listarDocPendientes(string basedatos, int idEmpresa, string codigoVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarDocumentosPendientes", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos },
                    {"pi_IdEmpresaInterna",idEmpresa },
                    {"pv_CodigoVendedor",codigoVendedor },
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> listarDocRechazadas(string basedatos, int idEmpresa, string codigoVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarDocumentosRechazadas", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos },
                    {"pi_IdEmpresaInterna",idEmpresa },
                    {"pv_CodigoVendedor",codigoVendedor },
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> actualizaEstado(NotadeVentaCabeceraModels nw, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("RRA_ActualizaEstadoNW", new System.Collections.Hashtable()
                {
                    { "nvId", nw.Id},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> BuscarNVC(NotadeVentaCabeceraModels nw, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarNVCabecera", new System.Collections.Hashtable()
                {
                    { "nvId", nw.Id},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public NotadeVentaCabeceraModels GetCab(int nvId)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_GetCab", new System.Collections.Hashtable()
                {
                    {"nvId",nvId }
                }).Tables[0];
                return UTIL.Mapper.BindData<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        public RespuestaModel InsertarClientesSoftland(string CodigoCliente,string BaseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_COT_ClienteSoftland", new System.Collections.Hashtable()
                {
                    { "CodCliente", CodigoCliente},
                    { "BaseDatos", BaseDatos }
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch(Exception ex)
            {
                string error =ex.ToString();
                return null;
            }
        }

        public RespuestaModel grabaAprobadoPor(int NVNumero, int nvId, int IdAprobador, int IdEmpresaInterna, string estado)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_NP_AprobadoPor", new System.Collections.Hashtable()
                {
                    { "NVNumero", NVNumero},
                    { "nvId", nvId},
                    { "IdAprobador", IdAprobador},
                    { "IdEmpresaInterna", IdEmpresaInterna},
                    { "estado", estado}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch(Exception Ex)
            {
                string error = Ex.ToString();
                return null;
            }
        }

        public List<NotaDeVentaDetalleModels> BuscarNVD(NotaDeVentaDetalleModels nw, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarNVDetalle", new System.Collections.Hashtable()
                {
                    { "nvId", nw.Id},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotaDeVentaDetalleModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotaDeVentaDetalleModels> ListarNotaDetalle(NotaDeVentaDetalleModels nw)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarNVDetalleStock", new System.Collections.Hashtable()
                {
                    { "nvNumero", nw.NVNumero},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotaDeVentaDetalleModels>(data);


            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<UsuariosModels> listarUsuarios()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarUsuarios", new System.Collections.Hashtable()
                {
                }).Tables[0];
                return UTIL.Mapper.BindDataList<UsuariosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel eliminaTodosUsuarioEmpresa(int idUsuario)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_DELALL_UsuarioEmpresa", new System.Collections.Hashtable()
                {
                    { "pi_IdUsuario", idUsuario},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel validaExisteUsuarioEmpresa(string venCod, int idEmpresa)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_ValidaExisteUsuarioEmpresa", new System.Collections.Hashtable()
                {
                    { "pv_VenCod", venCod},
                    { "pi_IdEmpresa", idEmpresa},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel insertaUsuarioEmpresa(int idUsuario, int idEmpresa, string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_UsuarioEmpresa", new System.Collections.Hashtable()
                {
                    { "pi_IdUsuario", idUsuario},
                    { "pi_IdEmpresa", idEmpresa},
                    { "pv_VenCod", venCod},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<UsuariosModels> BuscarUsuario(UsuariosModels usuario, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_BuscarUsuarios", new System.Collections.Hashtable()
                {
                    { "id", usuario.id},
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<UsuariosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel ActualizarUsuario(UsuariosModels usuario)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ActualizarUsuario", new System.Collections.Hashtable()
                {
                    { "id", usuario.id},
                    { "usuario", usuario.Usuario},
                    { "email", usuario.email},
                    { "tipoUsuario", usuario.tipoUsuario},
                    { "venCod", usuario.VenCod},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RespuestaModel AgregarUsuario(UsuariosModels usuario)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_AgregarUsuario", new System.Collections.Hashtable()
                {
                    { "Usuario", usuario.Usuario},
                    { "email", usuario.email},
                    { "tipoUsuario", usuario.tipoUsuario},
                    { "Contrasena", usuario.Password },
                    { "Nombre", usuario.Nombre },
                    { "VenCod", usuario.VenCod }
                }).Tables[0];

                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel EliminarUsuario(UsuariosModels usuarios)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_EliminarUsuario", new System.Collections.Hashtable()
                {
                    { "Id", usuarios.id},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public RespuestaModel EditarUsuario(UsuariosModels usuarios, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_SET_EditarUsuario", new System.Collections.Hashtable()
                {
                    {"@IdUsuario",usuarios.id },
                    {"@Usuario",usuarios.Usuario },
                    {"@Nombre",usuarios.Nombre },
                    {"@Password",usuarios.Password },
                    {"@Email",usuarios.email },
                    {"@TipoUsuario",int.Parse(usuarios.tipoUsuario) }
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UsuariosTiposModels> listarTipo()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarUsuariosTipos", new System.Collections.Hashtable()
                {
                }).Tables[0];
                return UTIL.Mapper.BindDataList<UsuariosTiposModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<VendedoresSoftlandModels> ListarVendedoresSoftland(string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarVendedorSoftland", new System.Collections.Hashtable()
                {
                    { "venCod", venCod},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<VendedoresSoftlandModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<VendedoresSoftlandModels> listarVendedoresSoftland(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ListarVendedorSoftland2", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<VendedoresSoftlandModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ObjetoPerfil> ListarPerfiles()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_ListarPefiles", new System.Collections.Hashtable()
                {
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ObjetoPerfil>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<VendedoresModel> obtenerVendedoresSoftlandTodos(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_ListarVendedoresSoftland", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<VendedoresModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<UsuariosModels> ListarCodVendedorSoft(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GetCodVendedor", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<UsuariosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ProductosModels> ListarCodProdSoft(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GetCodProducto", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ProductosModels>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<VendedorModels> GetVendedores(string basedatos, VendedorModels cliente = null)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("JS_ListarVendorVenCod", new System.Collections.Hashtable()
                {
                    { "VenCod", cliente.VenCod},
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<VendedorModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<AprobadorModels> GetAprobador(int IdAprobador)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_GetAprobadorNP", new System.Collections.Hashtable()
                {
                    {"pi_IdEmpresa",IdAprobador }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<AprobadorModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<UsuariosModels> GetDatosUsuario(string Id)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_GET_ObtenerDatosUsuario", new System.Collections.Hashtable()
                {
                    {"IdUsuario", int.Parse(Id) }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<UsuariosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> GetDatosClientes(string CodAux, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_GET_ObtenerDatosCliente", new System.Collections.Hashtable()
                {
                    {"CodAux",CodAux },
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<UsuarioEmpresaModel> ListaUsuarioEmpresas(int idUsuario)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_ListaUsuarioEmpresa", new System.Collections.Hashtable()
                {
                    { "pi_IdUsuario", idUsuario},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<UsuarioEmpresaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> ObtenerGiro(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_Giro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }

        }

        public List<CiudadModel> ObtenerCiudad(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_Ciudad", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<CiudadModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public ClientesModels ObtenerAtributoDescuento(string basedatos, string codaux, string textoAtributo)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ObtenerAtributoDescuentoCliente", new System.Collections.Hashtable()
                {
                    { "pv_CodAux", codaux},
                    { "pv_textoAtributo", textoAtributo},
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];
                return UTIL.Mapper.BindData<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ComunaModel> ObtenerComuna(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_Comuna", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ComunaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public CreditoModel ObtenerCredito(string CodAux, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("FR_ObtenerCredito", new System.Collections.Hashtable()
                {
                    { "pv_CodAux", CodAux},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindData<CreditoModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public VendedoresSoftlandModels ObtenerVendedorCliente(string CodAux, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_ObtenerVendedorCliente", new System.Collections.Hashtable()
                {
                    { "pv_CodAux", CodAux},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindData<VendedoresSoftlandModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> RechazarNotaVenta(NotadeVentaCabeceraModels nw)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("Ds_RechazarNP", new System.Collections.Hashtable()
                {
                    {"nvId", nw.Id}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> ActualizarCorreoCliente(ClientesModels cli, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_AgregarCorreoCli", new System.Collections.Hashtable()
                {
                    { "CodAux", cli.CodAux},
                    { "EMail", cli.EMail},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel InsertarProductoNuevoDisofi(string basedatos, string CodProdAntiguo, string codProd, string desProd, string codGrupo, string codSubGr, decimal precioVta, string codMon)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_ProductoNuevoDisofi", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                    { "pv_CodProdAntiguo", CodProdAntiguo},
                    { "pv_CodProd", codProd},
                    { "pv_DesProd", desProd},
                    { "pv_CodGrupo", codGrupo},
                    { "pv_CodSubGr", codSubGr},
                    { "pf_PrecioVta", precioVta},
                    { "pv_CodMon", codMon},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel CotizacionInsertarProductoNuevoDisofi(string basedatos, string CodProdAntiguo, string codProd, 
            string desProd, string codGrupo, string codSubGr, decimal precioVta, string codMon,string codigoFabrica)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_COT_ProductoNuevoDisofi", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                    { "pv_CodProdAntiguo", CodProdAntiguo},
                    { "pv_CodProd", codProd},
                    { "pv_DesProd", desProd},
                    { "pv_CodGrupo", codGrupo},
                    { "pv_CodSubGr", codSubGr},
                    { "pf_PrecioVta", precioVta},
                    { "pv_CodMon", codMon},
                    { "pv_CodFabrica", codigoFabrica},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel ActualizaNotaVentaProductoNuevo(string basedatos, int idDetalle, string codProd)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_UPD_NotaVentaProductoNuevo", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                    { "pi_IdDetalle", idDetalle},
                    { "@pv_CodProd", codProd},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ProductosModels> ObtenerProductoNuevoDisofi(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_ProductosNuevos", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ProductosModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel InsertarProductoNuevoSoftland(string basedatos, string codProd)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_ProductoNuevoSoftland", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                    { "pv_CodProd", codProd},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel AgregarCliente(ClientesModels cliente, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_AddCliente", new System.Collections.Hashtable()
                {
                    { "CodAux", cliente.CodAux },
                    { "NomAux", cliente.NomAux },
                    { "RutAux", cliente.RutAux },
                    { "FonAux1", cliente.FonAux1 },
                    { "Email", cliente.EMail },
                    { "GirAux", cliente.GirCod },
                    { "DirAux", cliente.DirAux },
                    { "pv_BaseDatos", basedatos },
                    { "EmailDte", cliente.EmailDte},
                    { "VenCod", cliente.VenCod},
                    { "ComAux", cliente.ComCod},
                    { "CiuAux", cliente.CiuCod}
                }).Tables[0];

                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel ExisteCliente(ClientesModels cliente, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_ExisteCliente", new System.Collections.Hashtable()
                {
                    { "CodAux", cliente.CodAux },
                    { "pv_BaseDatos", basedatos },
                }).Tables[0];

                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<EnvioCorreoCentroCostoModel> EnvioCorreoPorCentroCosto(string codigoCC, int idEmpresa)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_EnvioCorreo_PorCentroCosto ", new System.Collections.Hashtable()
                {
                    { "pv_CodigoCC", codigoCC },
                    { "pi_IdEmpresa", idEmpresa },
                }).Tables[0];

                return UTIL.Mapper.BindDataList<EnvioCorreoCentroCostoModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<AprobadorModels> GetAprobadorNP(int idEmpresa)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_GetAprobadorNP", new System.Collections.Hashtable()
                {
                    { "pi_IdEmpresa", idEmpresa },
                });
                return UTIL.Mapper.BindDataList<AprobadorModels>(data.Tables[0]);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ProjectManagerModels> obtieneCorreosProManag()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado1("SP_GET_CorreosProManag", new System.Collections.Hashtable());
                return UTIL.Mapper.BindDataList<ProjectManagerModels>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<SaldosModel> ObtenerSaldo(string RutAux, string CodAux, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_ObtenerSaldo", new System.Collections.Hashtable()
                {
                    { "RutAux", RutAux},
                    { "CodAux", CodAux},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<SaldosModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public ReporteComisionesModel ObtenerReporteComisiones(string basedatos, string idVendedores, DateTime fechaDesde, DateTime fechaHasta, int tipoUsuario)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("uSP_GET_Reporte_Comisiones", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                    { "pv_IdVendedores", idVendedores},
                    { "pv_FechaDesde", fechaDesde},
                    { "pv_FechaHasta", fechaHasta},
                      { "pv_TipoUsuario",tipoUsuario }
                });

                ReporteComisionesModel reporteComisiones = UTIL.Mapper.BindData<ReporteComisionesModel>(data.Tables[0]);
                reporteComisiones.Comisiones = UTIL.Mapper.BindDataList<ComisionModel>(data.Tables[1]);
                reporteComisiones.Totales = UTIL.Mapper.BindDataList<ReporteComisionesTotalModel>(data.Tables[2]);

                return reporteComisiones;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel validaNumOc(int numOc)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_ValidaNumOc", new System.Collections.Hashtable()
                {
                    { "NumOc", numOc},
                }).Tables[0];

                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel guardaRutaArchivo(string nvNum, string rutaArchivo)
        {
            var cadena = rutaArchivo.Split('.');
            var tipoArchivo = cadena[1].Replace(".", "");
            if (tipoArchivo.Length > 3)
            {
                tipoArchivo = tipoArchivo.Remove(tipoArchivo.Length - 1);
            }
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_SET_RutaArchivoSoftland", new System.Collections.Hashtable()
                {
                    {"NvNum",nvNum },
                    {"RutaArchivo",rutaArchivo.Trim() },
                    {"tipoArchivo",tipoArchivo.Trim() }
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public string obtenerCorreoManager(int idNv)
        {
            string resultado = "";
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_CorreoManager", new System.Collections.Hashtable()
                {
                    {"NvId", idNv }
                }).Tables[0];

                if (data.Rows.Count > 0)
                {
                    for (var i = 0; i < data.Rows.Count; i++)
                    {
                        var validador = new object();
                        validador = data.Rows[i].Field<object>("Email");
                        resultado = validador != null ? data.Rows[i].Field<string>("Email") : "NO ASIGNADO";
                    }
                }
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
            return resultado;
        }

        public List<NotadeVentaCabeceraModels> CotizacionBuscar_X_NV(string numcotizacion, string basedatos, string CodCliente, string venCod)
            //NotadeVentaCabeceraModels nw   ESO
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_BuscarCotizacionCabecera_X_NV", new System.Collections.Hashtable()
                {
                    //{ "nvId", nw.Id},
                    { "NroCotizacion", numcotizacion},
                    { "pv_BaseDatos", basedatos},
                    { "CodCliente", CodCliente},
                    { "VenCod", venCod}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        #endregion

        #region DashBoard

        public List<VendedoresModel> obtenerVendedoresSoftland(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetVendedoresSoftland", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos }
                });
                return UTIL.MapperDash.BindDataList<VendedoresModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeVentaActual(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeVentaActual", new System.Collections.Hashtable() {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_CodVendedor", codVendedor}
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeVentaActualAnterior(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeVentaActualAnterior", new System.Collections.Hashtable() {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeClientesAtendidosActual(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesAtendidosActual", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeClientesAtendidosActualAnterior(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesAtendidosActualAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeClientesCompraActual(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesCompraActual", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeCantVendedoresActual(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeCantVendedoresActual", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeCantVendedoresActualAnterior(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeCantVendedoresActualAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeVentasVendedoresActual(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeVentasVendedoresActual", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                    {"pv_VenCod",codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> GetInformeVentasMesAnterior(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("SP_GET_DASH_GetInformeVentasMesAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                    {"pv_VenCod",codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> getVentaTotalDia(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("SP_GET_DASH_GetVentaTotalDia", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> getVentaTotalDiaAnterior(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("SP_GET_DASH_GetVentaTotalDiaAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> getInformeClientesVentaDia(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesVentaDia", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                    {"pv_VenCod",codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> getInformeClientesVentaDiaAnterior(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesVentaDiaAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                    {"pv_VenCod",codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> getCantProductos(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetCantProductos", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeModel> getTopProductosActual(string baseDatos, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_TopProductosActual", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<ClientesModels> getClientesTotales(string baseDatos, string fd, string fh, string estado, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetClientesTotales", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fd },
                    {"fechaHasta", fh },
                    {"nvEstado", estado },
                    {"pv_VenCod", codVendedor }
                });
               
              
                    return UTIL.MapperDash.BindDataList<ClientesModels>(data);

                
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<ClientesModels> getClientesTotalesBlacklog(string baseDatos, string estado, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetClientesTotalesBlacklog", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"nvEstado", estado },
                    {"pv_VenCod", codVendedor }
                });

             
                return UTIL.MapperDash.BindDataList<ClientesModels>(data);

              
                
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<ClientesModels> getVentasPorClientes(string baseDatos, string fd, string fh, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetVentasPorCliente", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fd },
                    {"fechaHasta", fh },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<ClientesModels>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<TotalNotasDeVenta> getTotalNotasDeVenta(string baseDatos, string fd, string fh, string estado, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetTotalNotasDeVenta", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fd },
                    {"fechaHasta", fh },
                    {"nvEstado", estado},
                    {"pv_CodVendedor", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<TotalNotasDeVenta>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<TotalNotasDeVenta> getTotalNotasDeVentaBlacklog(string baseDatos, string estado, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetTotalNotasDeVentaBlacklog", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"nvEstado", estado},
                    {"pv_VenCod", codVendedor}
                });
                return UTIL.MapperDash.BindDataList<TotalNotasDeVenta>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> GetInformeVentaActualFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeVentaFiltro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fechaDesde },
                    {"fechaHasta", fechaHasta },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> GetInformeVentaFiltroAnterior(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string codVendor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeVentaFiltroAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fechaDesde },
                    {"fechaHasta", fechaHasta },
                    {"pv_VenCod", codVendor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> GetInformeClientesAtendidosFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesAtendidosFiltro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                    {"fechaDesde",fechaDesde },
                    {"fechaHasta",fechaHasta },
                    {"pv_VenCod",codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> GetInformeClientesAtendidosFiltroAnterior(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesAtendidosFiltroAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde",fechaDesde },
                    {"fechaHasta",fechaHasta },
                    {"pv_VenCod",codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> GetInformeClientesCompraFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeClientesCompraFiltro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fechaDesde },
                    {"fechaHasta", fechaHasta },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> GetInformeCantVendedoresFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeCantVendedoresFiltro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                    {"fechaDesde",fechaDesde },
                    {"fechaHasta", fechaHasta },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> GetInformeVentasVendedoresFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetInformeVentasVendedoresFiltro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fechaDesde },
                    {"fechaHasta", fechaHasta },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> getTopProductosFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_TopProductosFiltro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fechaDesde },
                    {"fechaHasta", fechaHasta },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<InformeFiltroModel> getCantProductosFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetCantProductosFiltro", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fechaDesde },
                    {"fechaHasta", fechaHasta },
                    {"pv_VenCod", codVendedor }
                });
                return UTIL.MapperDash.BindDataList<InformeFiltroModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public DataTable obtenerExcelVentasDash(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_ExcelVentas", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_FechaDesde", fechaDesde.ToString("yyyyMMdd") },
                    {"pv_FechaHasta", fechaHasta.ToString("yyyyMMdd") },
                    {"pv_VenCod", venCod }
                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public DataTable obtenerExcelNV(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_ExcelNotasDeVenta", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_FechaDesde", fechaDesde.ToString("yyyyMMdd") },
                    {"pv_FechaHasta", fechaHasta.ToString("yyyyMMdd") },
                    {"pv_VenCod", venCod }
                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<NotasDeVentaModel> getNotasDeVentaPorCliente(string baseDatos, string codaux, string fd, string fh, string estado)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetNotasDeVentaPorCliente", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"codAux", codaux},
                    {"fechaDesde", fd},
                    {"fechaHasta", fh},
                    {"nvEstado", estado}
                });
                return UTIL.MapperDash.BindDataList<NotasDeVentaModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<NotasDeVentaModel> getNotasDeVentaPorClienteBlacklog(string baseDatos, string codaux, string estado)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetNotasDeVentaPorClienteBlacklog", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"codAux", codaux},
                    {"nvEstado", estado}
                });
                return UTIL.MapperDash.BindDataList<NotasDeVentaModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<ProductosModel> getProductos(string baseDatos, string nvnumero)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetProductos", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"NVNumero", nvnumero}
                });
                return UTIL.MapperDash.BindDataList<ProductosModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<ProductosModel> getProductosBlacklog(string baseDatos, string nvnumero)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetProductosBlacklog", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"NVNumero", nvnumero}
                });
                return UTIL.MapperDash.BindDataList<ProductosModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<DetalleVentasModel> getDetalleVentasPorClientes(string baseDatos, string fd, string fh, string codaux)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetDetalleVentasPorCliente", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"fechaDesde", fd },
                    {"fechaHasta", fh },
                    {"codaux", codaux }
                });
                return UTIL.MapperDash.BindDataList<DetalleVentasModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<ProductosVentaModel> getProductosVenta(string baseDatos, decimal folio, string tipo)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetProductosVenta", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"folio", folio},
                    {"tipo", tipo }
                });
                return UTIL.MapperDash.BindDataList<ProductosVentaModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<DiagramaModel> getPeriodoActual(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_DASH_GetPeriodoActual", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos }
                });
                return UTIL.MapperDash.BindDataList<DiagramaModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<DiagramaModel> getPeriodoAnterior(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("SP_GET_DASH_GetPeriodoAnterior", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos }
                });
                return UTIL.MapperDash.BindDataList<DiagramaModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public List<DiagramaModel> getPeriodoTercero(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("SP_GET_DASH_GetPeriodoTercero", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos }
                });
                return UTIL.MapperDash.BindDataList<DiagramaModel>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public DataTable obtenerExcelVentasDia(string baseDatos, string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_ExcelVentasDia", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", venCod }
                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public DataTable obtenerExcelNVBacklog(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_ExcelNotasDeVentaBacklog", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_VenCod", venCod }
                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }
        #endregion

        #region Dashboard Cobranza

        // saldos
        public List<InformacionSaldosModel> getSaldoCuentaClientes(string baseDatos, string fd, string fh, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_GET_DASH_SaldosPorCliente", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                     {"fechaDesde", fd },
                     {"fechaHasta", fh }


                });

                return UTIL.MapperDash.BindDataList<InformacionSaldosModel>(data);

            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }

        }
        //saldos Proveedores
        public List<InformacionSaldosModel> getSaldosCuentasProveedor(string baseDatos, string fd, string fh, string codVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_GET_DASH_SaldosPorProveedor", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",baseDatos },
                    {"fechaDesde", fd },
                    {"fechaHasta", fh }


                });

                return UTIL.MapperDash.BindDataList<InformacionSaldosModel>(data);

            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }

        }


        //saldos clientes 
        public List<SaldosModel> ObtenerSaldoClientes(string RutAux, string fd, string fh, string CodAux, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_ObtenerSaldoClientes", new System.Collections.Hashtable()
                {
                    { "RutAux", RutAux},
                     { "FechaDesde", fd},
                    { "FechaHasta", fh},
                    { "CodAux", CodAux},
                    {"pv_BaseDatos", basedatos}

                }).Tables[0];
                return UTIL.Mapper.BindDataList<SaldosModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        //saldos por proveedor

        public List<SaldosModel> ObtenerSaldoProveedor(string RutAux, string fd, string fh, string CodAux, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("DS_ObtenerSaldoProveedor", new System.Collections.Hashtable()
                {
                    { "RutAux", RutAux},
                     { "FechaDesde", fd},
                    { "FechaHasta", fh},
                    { "CodAux", CodAux},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<SaldosModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<periodoSaldosModel> getPeriodos(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_PeriodosSaldosCliente", new System.Collections.Hashtable()
                {

                });
                return UTIL.MapperDash.BindDataList<periodoSaldosModel>(data);

            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }


        public List<periodoSaldosModel> getPeriodosProveedor(string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_PeriodosSaldosProveedores", new System.Collections.Hashtable()
                {

                });
                return UTIL.MapperDash.BindDataList<periodoSaldosModel>(data);

            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }




        // excel
        public DataTable obtenerExcelSaldosClientes(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_GET_DASH_ExcelSaldosClientes", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                      {"pv_FechaDesde", fechaDesde.ToString("yyyyMMdd") },
                    {"pv_FechaHasta", fechaHasta.ToString("yyyyMMdd") },
                    {"pv_VenCod", venCod }
                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }



        public DataTable obtenerExcelSaldosProveedores(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("DS_SP_GET_DASH_ExcelSaldosProveedor", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos", baseDatos },
                    {"pv_FechaDesde", fechaDesde.ToString("yyyyMMdd") },
                    {"pv_FechaHasta", fechaHasta.ToString("yyyyMMdd") },
                    {"pv_VenCod", venCod }
                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }
        #endregion

        #region Cotizacion

        public RespuestaNotaVentaModel AgregarCotizacion(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotadeVentaCabeceraModels NVC,string iniciales)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_COT_Cabecera", new System.Collections.Hashtable()
                {
                    {"pi_IdEmpresaInterna",NVC.IdEmpresaInterna },
                    { "pv_EstadoNP", NVC.EstadoNP},
                    { "pv_BaseDatos", baseDatos},
                    { "pb_InsertaDisofi", insertaDisofi},
                    { "pb_InsertaSoftland", insertaSoftland},
                    { "pi_NVNumero", NVC.NVNumero},
                    { "pd_nvFem", NVC.nvFemYYYYMMDD},
                    { "pv_nvEstado", NVC.nvEstado},
                    { "pi_nvEstFact", NVC.nvEstFact},
                    { "pi_nvEstDesp", NVC.nvEstDesp},
                    { "pi_nvEstRese", NVC.nvEstRese},
                    { "pi_nvEstConc", NVC.nvEstConc},
                    { "pi_CotNum", NVC.CotNum},
                    //{ "pv_NumOC", NVC.NumOC},
                    { "pv_NumOC", 0},
                    { "pd_nvFeEnt", NVC.nvFeEntYYYYMMDD},
                    { "pv_CodAux", NVC.CodAux},
                    { "pv_VenCod", NVC.VenCod},
                    { "pv_CodMon", NVC.CodMon},
                    { "pv_CodLista", NVC.CodLista},
                    { "pt_nvObser", NVC.nvObser},
                    { "pv_nvCanalNV", NVC.nvCanalNV},
                    { "pv_CveCod", NVC.CveCod},
                    { "pv_NomCon", NVC.NomCon},
                    { "pv_CodiCC", NVC.CodiCC},
                    { "pv_CodBode", NVC.CodBode},
                    { "pf_nvSubTotal", NVC.nvSubTotal},
                    { "pf_nvPorcDesc01", NVC.nvPorcDesc01},
                    { "pf_nvDescto01", NVC.nvDescto01},
                    { "pf_nvPorcDesc02", NVC.nvPorcDesc02},
                    { "pf_nvDescto02", NVC.nvDescto02},
                    { "pf_nvPorcDesc03", NVC.nvPorcDesc03},
                    { "pf_nvDescto03", NVC.nvDescto03},
                    { "pf_nvPorcDesc04", NVC.nvPorcDesc04},
                    { "pf_nvDescto04", NVC.nvDescto04},
                    { "pf_nvPorcDesc05", NVC.nvPorcDesc05},
                    { "pf_nvDescto05", NVC.nvDescto05},
                    { "pf_nvMonto", NVC.nvMonto},
                    { "pd_nvFeAprob", NVC.nvFeAprobYYYYMMDD},
                    { "pi_NumGuiaRes", NVC.NumGuiaRes},
                    { "pf_nvPorcFlete", NVC.nvPorcFlete},
                    { "pf_nvValflete", NVC.nvValflete},
                    { "pf_nvPorcEmb", NVC.nvPorcEmb},
                    { "pf_nvValEmb", NVC.nvValEmb},
                    { "pf_nvEquiv", NVC.nvEquiv},
                    { "pf_nvNetoExento", NVC.nvNetoExento},
                    { "pf_nvNetoAfecto", NVC.nvNetoAfecto},
                    { "pf_nvTotalDesc", NVC.nvTotalDesc},
                    { "pv_ConcAuto", NVC.ConcAuto},
                    { "pv_CodLugarDesp", NVC.CodLugarDesp},
                    { "pv_SolicitadoPor", NVC.SolicitadoPor},
                    { "pv_DespachadoPor", NVC.DespachadoPor},
                    { "pv_Patente", NVC.Patente},
                    { "pv_RetiradoPor", NVC.RetiradoPor},
                    { "pv_CheckeoPorAlarmaVtas", NVC.CheckeoPorAlarmaVtas},
                    { "pi_EnMantencion", NVC.EnMantencion},
                    { "pv_Usuario", NVC.Usuario},
                    { "pv_UsuarioGeneraDocto", NVC.UsuarioGeneraDocto},
                    { "pd_FechaHoraCreacion", NVC.FechaHoraCreacionYYYYMMDD},
                    { "pv_Sistema", NVC.Sistema},
                    { "pv_ConcManual", NVC.ConcManual},
                    { "pv_RutSolicitante", NVC.RutSolicitante},
                    { "pv_proceso", NVC.proceso},
                    { "pf_TotalBoleta", NVC.TotalBoleta},
                    { "pi_NumReq", NVC.NumReq},
                    { "pv_CodVenWeb", NVC.CodVenWeb},
                    { "pv_CodBodeWms", NVC.CodBodeWms},
                    { "pv_CodLugarDocto", NVC.CodLugarDocto},
                    { "pv_RutTransportista", NVC.RutTransportista},
                    { "pv_Cod_Distrib", NVC.Cod_Distrib},
                    { "pv_Nom_Distrib", NVC.Nom_Distrib},
                    { "pi_MarcaWG", NVC.MarcaWG},
                    { "pb_ErrorAprobador", NVC.ErrorAprobador},
                    { "pv_ErrorAprobadorMensaje", NVC.ErrorAprobadorMensaje},
                    { "pv_IdCorreoManag", NVC.CorreoManag},
                    { "iniciales", iniciales},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaNotaVentaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }

        }

        public RespuestaNotaVentaModel AgregarDetalleCotizacion(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotaDeVentaDetalleModels NVD)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_COT_Detalle", new System.Collections.Hashtable()
                {
                    /*--------------------------- CAMPOS DISOFI ---------------------------*/
                    { "pv_BaseDatos", baseDatos},
                    { "pb_InsertaDisofi", insertaDisofi},
                    { "pb_InsertaSoftland", insertaSoftland},
                    { "pi_IdNotaVenta", NVD.IdNotaVenta},
                    { "pi_NVNumero", NVD.NVNumero},
                    { "pf_nvLinea", NVD.nvLinea},
                    { "pf_nvCorrela", NVD.nvCorrela},
                    { "pd_nvFecCompr", NVD.nvFecComprYYYYMMDD},
                    { "pv_CodProd", NVD.CodProd},
                    { "pf_nvCant", NVD.nvCant},
                    { "pf_nvPrecio", NVD.nvPrecio},
                    { "pf_nvEquiv", NVD.nvEquiv},
                    { "pf_nvSubTotal", NVD.nvSubTotal},
                    { "pf_nvDPorcDesc01", NVD.nvDPorcDesc01},
                    { "pf_nvDDescto01", NVD.nvDDescto01},
                    { "pf_nvDPorcDesc02", NVD.nvDPorcDesc02},
                    { "pf_nvDDescto02", NVD.nvDDescto02},
                    { "pf_nvDPorcDesc03", NVD.nvDPorcDesc03},
                    { "pf_nvDDescto03", NVD.nvDDescto03},
                    { "pf_nvDPorcDesc04", NVD.nvDPorcDesc04},
                    { "pf_nvDDescto04", NVD.nvDDescto04},
                    { "pf_nvDPorcDesc05", NVD.nvDPorcDesc05},
                    { "pf_nvDDescto05", NVD.nvDDescto05},
                    { "pf_nvTotDesc", NVD.nvTotDesc},
                    { "pf_nvTotLinea", NVD.nvTotLinea},
                    { "pf_nvCantDesp", NVD.nvCantDesp},
                    { "pf_nvCantProd", NVD.nvCantProd},
                    { "pf_nvCantFact", NVD.nvCantFact},
                    { "pf_nvCantDevuelto", NVD.nvCantDevuelto},
                    { "pf_nvCantNC", NVD.nvCantNC},
                    { "pf_nvCantBoleta", NVD.nvCantBoleta},
                    { "pt_DetProd", NVD.DetProd},
                    { "pv_CheckeoMovporAlarmaVtas", NVD.CheckeoMovporAlarmaVtas},
                    { "pv_KIT", NVD.KIT},
                    { "pi_CodPromocion", NVD.CodPromocion},
                    { "pv_CodUMed", NVD.CodUMed},
                    { "pf_CantUVta", NVD.CantUVta},
                    { "pv_Partida", NVD.Partida},
                    { "pv_Pieza", NVD.Pieza},
                    { "pd_FechaVencto", NVD.FechaVenctoYYYYMMDD},
                    { "pf_CantidadKit", NVD.CantidadKit},
                    { "pi_MarcaWG", NVD.MarcaWG},
                    { "pf_PorcIncidenciaKit", NVD.PorcIncidenciaKit},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaNotaVentaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<ClientesModels> GetContactoCotizacion(string baseDatos, ClientesModels cliente = null)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_ListaContactos", new System.Collections.Hashtable()
                {
                    { "vchrCodAux", cliente.CodAux},
                    { "pv_BaseDatos", baseDatos },
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ClientesModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel AgregarDireccionDespachoCotizacion(DireccionDespachoModels DirDes, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_COT_AgregarDireccionDespacho", new System.Collections.Hashtable()
                {
                    { "pv_CodAux", DirDes.CodAxD},
                    { "pv_DirDch", DirDes.DirDch},
                    { "pv_ComDch", DirDes.ComDch},
                    { "pv_NomDch", DirDes.NomDch},
                    { "pv_CiuDch", DirDes.CiuDch},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<DireccionDespachoModels> BuscarDireccionDespachoCotizacion(DireccionDespachoModels DirDes, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_BuscarDirecDespa", new System.Collections.Hashtable()
                {
                    { "CodAxD", DirDes.CodAxD},
                    { "pv_BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindDataList<DireccionDespachoModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }


        public ReporteCotizacionModel ObtenerPdfCotizacion(string baseDatos,int Id)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_PdfCotizacion", new System.Collections.Hashtable()
                {
                    { "Id", Id},
                    {"pv_BaseDatos",baseDatos}
                });

                ReporteCotizacionModel reporteCotizacion = UTIL.Mapper.BindData<ReporteCotizacionModel>(data.Tables[0]);
                reporteCotizacion.Cabecera = UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data.Tables[0]);
                reporteCotizacion.Detalle = UTIL.Mapper.BindDataList<NotaDeVentaDetalleModels>(data.Tables[1]);
                reporteCotizacion.Cliente = UTIL.Mapper.BindDataList<COT_ClienteModel>(data.Tables[2]);
                reporteCotizacion.DatosEmpresa = UTIL.Mapper.BindDataList<COT_DatoEmpresaModel>(data.Tables[3]);
                return reporteCotizacion;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }


        public List<NotadeVentaCabeceraModels> listarCotizacionPendientes(string basedatos, int idEmpresa, string codigoVendedor)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_ListarCotizacionesPendientes", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos },
                    {"pi_IdEmpresaInterna",idEmpresa },
                    { "codigoVendedor", codigoVendedor}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<VendedorModels> ListarVendedores(string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_Vendedores", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<VendedorModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public List<NotadeVentaCabeceraModels> BuscarCotizacion(NotadeVentaCabeceraModels nw, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_BuscarCotizacionCabecera", new System.Collections.Hashtable()
                {
                    { "nvId", nw.Id},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        
        
        public List<NotaDeVentaDetalleModels> BuscarCotizacionDetalle(NotaDeVentaDetalleModels nw, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_BuscarDetalle", new System.Collections.Hashtable()
                {
                    { "nvId", nw.Id},
                    { "pv_BaseDatos", basedatos}
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotaDeVentaDetalleModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }



        public List<NotadeVentaCabeceraModels> CotizacionesPendientesBuscar(string basedatos, string numCotizacion, string vendedor,
                                                int tienefechaCot, string fechaCotizacion,int tienefechaCierre, string fechaCierre)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_FiltroCotizacionesPendientes", new System.Collections.Hashtable()
                {
                    {"pv_BaseDatos",basedatos },
                    {"numCotizacion",numCotizacion },
                    {"vendedor",vendedor },
                    {"tienefechaCot",tienefechaCot },
                    {"fechaCotizacion",fechaCotizacion },
                    {"tienefechaCierre",tienefechaCierre },
                    {"fechaCierre",fechaCierre }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<NotadeVentaCabeceraModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

         public RespuestaModel agregarCotizacionExtras(int IdCotizacion, DateTime FechaCierre, int nroSolicitud,string NroFinalCotizacion)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_NV_Extras", new System.Collections.Hashtable()
                {
                    { "pi_IdCotizacion",IdCotizacion},
                    { "FechaCierre",FechaCierre},
                    {"nroSolicitud", nroSolicitud },
                    { "NroFinalCotizacion", NroFinalCotizacion}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel AgregarClienteCotizacion(ClientesModels cliente, string basedatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_COT_Cliente", new System.Collections.Hashtable()
                {
                    { "CodAux", cliente.CodAux },
                    { "NomAux", cliente.NomAux },
                    { "RutAux", cliente.RutAux },
                    { "FonAux1", cliente.FonAux1 },
                    { "Email", cliente.EMail },
                    { "GirAux", cliente.GirCod },
                    { "DirAux", cliente.DirAux },
                    { "pv_BaseDatos", basedatos },
                    { "EmailDte", cliente.EmailDte},
                    { "VenCod", cliente.VenCod},
                    { "ComAux", cliente.ComCod},
                    { "CiuAux", cliente.CiuCod}
                }).Tables[0];

                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                LogUser.agregarLog(error);
                return null;
            }
        }
        public RespuestaModel grabarContacto(string codAux, string nombre, string fono, string email,string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("INS_COT_Contacto", new System.Collections.Hashtable()
                {
                    { "codAux", codAux },
                    { "nombre", nombre},
                    { "fono", fono},
                    { "email", email},
                    { "baseDatos", baseDatos}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                LogUser.agregarLog(error);
                return null;
            }
        }

        public RespuestaModel ActualizaCabeceraCotizacion(int numCot, string fechaEmision, string fechaEntrega, string fechaCierre,
                                                        string contacto, string observacion)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_UPD_COT_Cabecera", new System.Collections.Hashtable()
                {
                    { "Id", numCot},
                    { "fechaEmision", fechaEmision},
                    { "fechaEntrega", fechaEntrega},
                    { "fechaCierre", fechaCierre},
                    { "contacto", contacto},
                    { "observacion", observacion},                    
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        public ProductoCotizacionModels BuscaPro_X_Modificar(int nroLinea, string codProd,int Id, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_Producto_X_Modificar", new System.Collections.Hashtable()
                {
                    {"nroLinea",nroLinea},
                    {"codProd",codProd },
                    { "Id", Id},
                    { "pv_BaseDatos",baseDatos}
                }).Tables[0];
                return UTIL.Mapper.BindData<ProductoCotizacionModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel Detalle_X_Modificar(int id, double precio, string grupo, string subGrupo, string codmon,
                                    double cantidad, double descuento, string baseDato,double valorDescuento,
                                    string fechaEntrega, string codigoFabrica, string codProd)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_UPD_COT_Detalle", new System.Collections.Hashtable()
                {
                    { "Id", id},
                    { "precio", precio },
                    { "grupo", grupo},
                    { "subGrupo", subGrupo},
                    { "codmon", codmon},
                    { "cantidad", cantidad},
                    { "descuento", descuento},
                    { "pv_BaseDatos",baseDato},
                    { "valorDescuento", valorDescuento },
                    { "fechaEntrega", fechaEntrega},
                    { "codigoFabrica", codigoFabrica},
                    { "codProd", codProd}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel EliminarDetalle(int Linea,int IdCotizacion)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_DEL_COT_EliminaDetalle", new System.Collections.Hashtable()
                {
                    { "Linea",Linea},
                    { "IdCotizacion",IdCotizacion}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public VendedoresSoftlandModels ObtenerNombreVendedor(string codigo, string baseDatos)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_ObtenerNombreVendedor", new System.Collections.Hashtable()
                {
                    { "Codigo", codigo},
                    { "BaseDatos", baseDatos},
                }).Tables[0];
                return UTIL.Mapper.BindData<VendedoresSoftlandModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        #endregion

        #region Comisiones
        public DataTable ObtenerReporteComisionesExcel(string basedatos, string idVendedores, DateTime fechaDesde, DateTime fechaHasta, int tipoUsuario)

        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDT("SP_NV_GET_Reporte_ComisionesExcel", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                    { "pv_IdVendedores", idVendedores},
                    { "pv_FechaDesde", fechaDesde},
                    { "pv_FechaHasta", fechaHasta},
                    { "pv_TipoUsuario", tipoUsuario},
                });
                return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }


        public ReporteComisionConsolidadoModel ObtenerReporteConsolidado(string baseDatos, DateTime fechaDesde, DateTime fechaHasta)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_NV_GET_Reporte_ComisionesConsolidado", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", baseDatos},
                    { "pv_FechaDesde", fechaDesde},
                    { "pv_FechaHasta", fechaHasta}
                });

                if (data.Tables[0].Rows.Count > 0)
                {

                    ReporteComisionConsolidadoModel reporteConsolidado = UTIL.Mapper.BindData<ReporteComisionConsolidadoModel>(data.Tables[0]);
                    reporteConsolidado.Comisiones = UTIL.Mapper.BindDataList<ComisionModel>(data.Tables[1]);
                    reporteConsolidado.Consolidado = UTIL.Mapper.BindDataList<ReporteComisionesConsolidado>(data.Tables[2]);
                    return reporteConsolidado;
                }
                else
                {
                    return new ReporteComisionConsolidadoModel();

                }
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public DataTable ObtenerReporteConsolidadoExcel(string basedatos, DateTime fechaDesde, DateTime fechaHasta)

        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_NV_GET_Reporte_ComisionesConsolidado", new System.Collections.Hashtable()
                {
                    { "pv_BaseDatos", basedatos},
                    { "pv_FechaDesde", fechaDesde},
                    { "pv_FechaHasta", fechaHasta},
                });
                return data.Tables[2];
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public RespuestaModels GrabarComision(VendedorComision VendedorComision)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_COM_INS_VendedorComision", new System.Collections.Hashtable()
                {
                    {"pv_Codigo", VendedorComision.CodigoVendedor},
                    {"pv_Nombre", VendedorComision.Nombre},
                    {"pv_lp1", VendedorComision.lp1},
                    {"pv_lp2", VendedorComision.lp2},
                    {"pv_lp3", VendedorComision.lp3},
                    {"pv_lp4", VendedorComision.lp4},

            }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModels>(data);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }

        }


        #endregion

        #region Reporte Saldos
        public List<UsuarioEmpresaModels> GetEmpresas()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("SP_GET_Empresas", new System.Collections.Hashtable() { 
                
                
                });

                return UTIL.Mapper.BindDataList<UsuarioEmpresaModels>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public DataTable obtenerExcelSaldo(string bd, string Anio, string Fecha)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("reporteSaldos", new System.Collections.Hashtable()
                {
                    {"baseDatos",bd },
                    {"FechaPeriodo",Anio },
                    {"FContabiliza",Fecha }
                });
              return data;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }


        #endregion

        #region Mantenedores
        #region Consideracion
        public List<ConsideracionesModel> ListarConsideraciones()
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_COT_Consideracion", new System.Collections.Hashtable()
                {
                }).Tables[0];
                return UTIL.Mapper.BindDataList<ConsideracionesModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel GrabarConsideracion(string titulo, string consideracion)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_COT_Consideracion", new System.Collections.Hashtable()
                {
                    { "titulo", titulo},
                    { "consideracion", consideracion}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        public RespuestaModel ModificarConsideracion(int id, string titulo, string consideracion)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_UPD_COT_Consideracion", new System.Collections.Hashtable()
                {
                    { "id", id},
                    { "titulo", titulo},
                    { "consideracion", consideracion}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        public RespuestaModel EliminarConsideracion(int id)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_DEL_COT_Consideracion", new System.Collections.Hashtable()
                {
                    { "id", id}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }

        #endregion


        public RespuestaModel GrabarProducto(ProductosMatenedorModel producto)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("", new System.Collections.Hashtable()
                {
                    { "codigo", producto.Codigo}
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        #endregion

        #region Archivos Adjuntos

        public RespuestaModel insertarRutaAdjunto(string path, string codAux)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_INS_NV_RutaArchivoAdjunto", new System.Collections.Hashtable()
                {
                    {"Path", path },
                    {"CodAux", codAux }
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }catch(Exception e)
            {
                string error = e.Message.ToString();
                LogUser.agregarLog(error);
                return null;
            }
        }

        public List<AdjuntosModels> obtenerArchivosAdjuntos(string CodAux)
        {
            LogUser.agregarLog("FACTORY ADJUNTOS - CODAUX: " + CodAux);
            try
            {
                LogUser.agregarLog("Entra SP");
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_GET_NV_RutaArchivoAdjunto", new System.Collections.Hashtable()
                {
                    {"CodAux", CodAux }
                }).Tables[0];
                return UTIL.Mapper.BindDataList<AdjuntosModels>(data);
            }catch(Exception e)
            {
                string error = e.Message.ToString();
                LogUser.agregarLog(error);
                return null;
            }
        }

        public RespuestaModel limpiarAdjuntosPorCodAux(string CodAux)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenado("SP_DEL_NV_RutaArchivoAdjunto", new System.Collections.Hashtable() 
                {
                    {"CodAux", CodAux }
                }).Tables[0];
                return UTIL.Mapper.BindData<RespuestaModel>(data);
            }catch(Exception e)
            {
                string error = e.Message.ToString();
                LogUser.agregarLog(error);
                return null;
            }
        }

        #endregion

        public List<ParametroFiltradoModel> ObtenerParametroFiltrado(int idempresa)
        {
            try
            {
                var data = new DBConector().EjecutarProcedimientoAlmacenadoDash("FR_BuscarParametrosUsuariosFiltro", new System.Collections.Hashtable()
                {

                     {"pi_idEmpresa",idempresa }
                });

                return UTIL.Mapper.BindDataList<ParametroFiltradoModel>(data);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return null;
            }
        }
        
    }
}