﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UTIL.Models;
using UTIL.Objetos;

namespace BLL
{
    public class ControlDisofi
    {
        private FactoryAcceso _Control = new FactoryAcceso();

        #region Nota de Venta

        public List<_NotaDeVentaDetalleModels> DatosCorreoVend(int NvNUmero, string basedatos)
        {
            return _Control.DatosCorreoVend(NvNUmero, basedatos);
        }

        public DataTable obtenerExcelVentas(string baseDatos)
        {
            return _Control.obtenerExcelVentas(baseDatos);
        }

        public List<StockModel> obtenerProductosStock(string codprod, string baseDatos)
        {
            return _Control.obtenerProductosStock(codprod, baseDatos);
        }

        public StockModel obtenerProdStock(string codprod)
        {
            return _Control.obtenerProdStock(codprod);
        }

        public List<_NotaDeVentaDetalleModels> DatosCorreoAprobador(string VendCod)
        {
            return _Control.DatosCorreoAprobador(VendCod);
        }

        public bool ActualizaCorreo(_UsuariosModels Usuario, string basedatos)
        {
            return _Control.ActualizaCorreo(Usuario, basedatos);
        }

        public List<ObjetoMenu> MenuUsuario(int idUsuario, int idempresa)
        {
            return _Control.MenuUsuario(idUsuario, idempresa);
        }

        public List<CentrodeCostoModels> ListarCentroCosto(string basedatos)
        {
            return _Control.ListarCentroCosto(basedatos);
        }

        public List<CanalVentaModels> ListarCanalVenta(string basedatos)
        {
            return _Control.ListarCanalVenta(basedatos);
        }

        public List<ClientesModels> GetClientes(string basedatos, ClientesModels cliente = null)
        {
            return _Control.GetClientes(basedatos, cliente);
        }

        public List<ClientesModels> GetContacto(string basedatos, ClientesModels cliente = null)
        {
            return _Control.GetContacto(basedatos, cliente);
        }

        public List<ClientesModels> GetCliente(ClientesModels cliente = null)
        {
            return _Control.GetCliente(cliente);
        }

        public List<ClientesModels> listarClientes(string basedatos)
        {
            return _Control.listarClientes(basedatos);
        }

        public List<ClientesModels> BuscarClientes(ClientesModels clientes)
        {
            return _Control.BuscarClientes(clientes);
        }

        public List<EmpresaModel> ListarEmpresas()
        {
            return _Control.ListarEmpresas();
        }

        public List<MonedaModel> ListarMonedas(string basedatos)
        {
            return _Control.ListarMonedas(basedatos);
        }

        public List<GrupoModel> ListarGrupos(string basedatos)
        {
            return _Control.ListarGrupos(basedatos);
        }

        public List<SubGrupoModel> ListarSubGrupos(string basedatos)
        {
            return _Control.ListarSubGrupos(basedatos);
        }

        public List<ClientesModels> ListarClientesTodos()
        {
            return _Control.ListarClientesTodos();
        }

        public List<ClientesModels> BuscarMisClientes(ClientesModels RutAux)
        {
            return _Control.BuscarMisClientes(RutAux);
        }

        public List<ClientesModels> BuscarMisClientesVenCod(UsuariosModels usuario, string basedatos)
        {
            return _Control.BuscarMisClientesVenCod(usuario, basedatos);
        }

        public List<ClientesModels> BuscarContacto(string basedatos, ClientesModels contacto)
        {
            return _Control.BuscarContacto(basedatos, contacto);
        }

        public List<ClientesModels> AgregarContacto(ClientesModels contacto)
        {
            return _Control.AgregarContacto(contacto);
        }

        public List<DireccionDespachoModels> BuscarDirecDespach(DireccionDespachoModels DirDes, string baseDatos)
        {
            return _Control.BuscarDirecDespach(DirDes, baseDatos);
        }

        public List<DireccionDespachoModels> BuscarDirecDespachMail(DireccionDespachoModels DirDes, string baseDatos, string NomDch)
        {
            return _Control.BuscarDirecDespachMail(DirDes, baseDatos, NomDch);
        }

        public RespuestaModel AgregarDireccionDespacho(DireccionDespachoModels DirDes, string baseDatos)
        {
            return _Control.AgregarDireccionDespacho(DirDes, baseDatos);
        }

        public RespuestaModel ActualizarCliente(ClientesModels cliente, string basedatos)
        {
            return _Control.ActualizarCliente(cliente, basedatos);
        }

        public List<CondicionVentasModels> listarConVen(string baseDatos, CondicionVentasModels conven)
        {
            return _Control.listarConVen(baseDatos, conven);
        }

        public List<CondicionVentasModels> listarConVenPret(string baseDatos, CondicionVentasModels conven)
        {
            return _Control.listarConVenPret(baseDatos, conven);
        }

        public List<ListaDePrecioModels> listarListaDePrecio(string baseDatos, ListaDePrecioModels lista)
        {
            return _Control.listarListaDePrecio(baseDatos, lista);
        }

        public ObjetoUsuario Login(ObjetoUsuario usuario)
        {
            return _Control.Login(usuario);
        }
        public DataTable ObtenerReporteComisionesExcel(string basedatos, string idVendedores, DateTime fechaDesde, DateTime fechaHasta, int tipoUsuario)
        {
            return _Control.ObtenerReporteComisionesExcel(basedatos, idVendedores, fechaDesde, fechaHasta, tipoUsuario);
        }
        public RespuestaNotaVentaModel AgregarNV(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotadeVentaCabeceraModels NVC)
        {
            return _Control.AgregarNV(baseDatos, insertaDisofi, insertaSoftland, NVC);
        }

        public List<NotadeVentaCabeceraModels> EditarNV(NotadeVentaCabeceraModels NVC)
        {
            return _Control.EditarNV(NVC);
        }

        public RespuestaNotaVentaModel AgregarDetalleNV(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotaDeVentaDetalleModels NVD)
        {
            return _Control.AgregarDetalleNV(baseDatos, insertaDisofi, insertaSoftland, NVD);
        }

        public RespuestaModel AgregarDetalleNVValorAdicional(string baseDatos, int idDetalleNotaVenta, int valorAdicional)
        {
            return _Control.AgregarDetalleNVValorAdicional(baseDatos, idDetalleNotaVenta, valorAdicional);
        }

        public RespuestaNotaVentaModel AgregarImpuesto(string baseDatos, NotadeVentaCabeceraModels NV)
        {
            return _Control.AgregarImpuesto(baseDatos, NV);
        }

        public List<NotadeVentaCabeceraModels> BuscarNVPorNumero(int Id, string basedatos)
        {
            return _Control.BuscarNVPorNumero(Id, basedatos);
        }

        public List<NotadeVentaCabeceraModels> BuscarNVNum(NotadeVentaCabeceraModels NVSoft)
        {
            return _Control.BuscarNVNum(NVSoft);
        }

        public List<NotaDeVentaDetalleModels> BuscarNVDETALLEPorNumero(int Id, string basedatos)
        {
            return _Control.BuscarNVDETALLEPorNumero(Id, basedatos);
        }

        public List<NotadeVentaCabeceraModels> InsertarNvSoftland(NotadeVentaCabeceraModels NVC)
        {
            return _Control.InsertarNvSoftland(NVC);
        }

        public ParametrosModels BuscarParametros(int idEmpresa)
        {
            return _Control.BuscarParametros(idEmpresa);
        }

        public List<menuModels> BuscarMenus()
        {
            return _Control.BuscarMenus();
        }


        public RespuestaModel ModificarParametros(ParametrosModels Aprobador)
        {
            Aprobador.AtributoSoftlandDescuentoCliente = Aprobador.AtributoSoftlandDescuentoCliente == null ? "" : Aprobador.AtributoSoftlandDescuentoCliente;
            Aprobador.StockProductoCodigoBodega = Aprobador.StockProductoCodigoBodega == null ? "" : Aprobador.StockProductoCodigoBodega;
            Aprobador.StockProductoCodigoBodegaAdicional = Aprobador.StockProductoCodigoBodegaAdicional == null ? "" : Aprobador.StockProductoCodigoBodegaAdicional;
            Aprobador.CodigoCondicionVentaPorDefecto = Aprobador.CodigoCondicionVentaPorDefecto == null ? "" : Aprobador.CodigoCondicionVentaPorDefecto;
            return _Control.ModificarParametros(Aprobador);
        }

        public List<ProductosModels> BuscarProducto(ProductosModels producto)
        {
            return _Control.BuscarProducto(producto);
        }

        public List<ProductosModels> ListarProducto(string ListaPrecio, string baseDatos)
        {
            return _Control.ListarProducto(ListaPrecio, baseDatos);
        }

        public List<TallaColorProductoModels> ListarTallaColorProducto(string CodProd, string baseDatos)
        {
            return _Control.ListarTallaColorProducto(CodProd, baseDatos);
        }

        public List<ProductosModels> BuscarProductoRapido(ProductosModels producto)
        {
            return _Control.BuscarProductoRapido(producto);
        }

        public List<NotadeVentaCabeceraModels> listarDocAprobados(string basedatos, int idEmpresa, string codigoVendedor)
        {
            return _Control.listarDocAprobados(basedatos, idEmpresa, codigoVendedor);
        }

        public List<NotadeVentaCabeceraModels> listarDocPendientes(string basedatos, int idEmpresa, string codigoVendedor)
        {
            return _Control.listarDocPendientes(basedatos, idEmpresa, codigoVendedor);
        }

        public List<NotadeVentaCabeceraModels> listarDocRechazadas(string basedatos, int idEmpresa, string codigoVendedor)
        {
            return _Control.listarDocRechazadas(basedatos, idEmpresa, codigoVendedor);
        }

        public List<NotadeVentaCabeceraModels> actualizaEstado(NotadeVentaCabeceraModels nw, string basedatos)
        {
            return _Control.actualizaEstado(nw, basedatos);
        }

        public NotadeVentaCabeceraModels GetCab(int nvId)
        {
            return _Control.GetCab(nvId);
        }
        public RespuestaModel InsertarClientesSoftland(string CodigoCliente, string BaseDatos)
        {
            return _Control.InsertarClientesSoftland(CodigoCliente, BaseDatos);
        }

        public RespuestaModel grabaAprobadoPor(int NVNumero, int nvId, int IdAprobador, int IdEmpresaInterna, string estado)
        {
            return _Control.grabaAprobadoPor(NVNumero, nvId, IdAprobador, IdEmpresaInterna, estado);
        }
        public List<NotadeVentaCabeceraModels> BuscarNVC(NotadeVentaCabeceraModels nw, string basedatos)
        {
            return _Control.BuscarNVC(nw, basedatos);
        }

        public List<NotaDeVentaDetalleModels> BuscarNVD(NotaDeVentaDetalleModels nw, string basedatos)
        {
            return _Control.BuscarNVD(nw, basedatos);
        }

        public List<NotaDeVentaDetalleModels> ListarNotaDetalle(NotaDeVentaDetalleModels nw)
        {
            return _Control.ListarNotaDetalle(nw);
        }

        public List<UsuariosModels> listarUsuarios()
        {
            return _Control.listarUsuarios();
        }

        public RespuestaModel validaExisteUsuarioEmpresa(string venCod, int idEmpresa)
        {
            return _Control.validaExisteUsuarioEmpresa(venCod, idEmpresa);
        }

        public RespuestaModel eliminaTodosUsuarioEmpresa(int idUsuario)
        {
            return _Control.eliminaTodosUsuarioEmpresa(idUsuario);
        }

        public RespuestaModel insertaUsuarioEmpresa(int idUsuario, int idEmpresa, string venCod)
        {
            return _Control.insertaUsuarioEmpresa(idUsuario, idEmpresa, venCod);
        }

        public List<UsuariosModels> BuscarUsuario(UsuariosModels usuario, string basedatos)
        {
            return _Control.BuscarUsuario(usuario, basedatos);
        }

        public RespuestaModel ActualizarUsuario(UsuariosModels usuario)
        {
            return _Control.ActualizarUsuario(usuario);
        }

        public RespuestaModel AgregarUsuario(UsuariosModels usuario)
        {
            return _Control.AgregarUsuario(usuario);
        }

        public RespuestaModel EliminarUsuario(UsuariosModels usuarios)
        {
            return _Control.EliminarUsuario(usuarios);
        }

        public RespuestaModel EditarUsuario(UsuariosModels usuarios, string basedatos)
        {
            return _Control.EditarUsuario(usuarios, basedatos);
        }

        public List<UsuariosTiposModels> listarTipo()
        {
            return _Control.listarTipo();
        }

        public List<VendedoresSoftlandModels> ListarVendedoresSoftland(string venCod)
        {
            return _Control.ListarVendedoresSoftland(venCod);
        }

        public List<VendedoresSoftlandModels> listarVendedoresSoftland(string basedatos)
        {
            return _Control.listarVendedoresSoftland(basedatos);
        }

        public List<ObjetoPerfil> ListarPerfiles()
        {
            return _Control.ListarPerfiles();
        }

        public List<VendedoresModel> obtenerVendedoresSoftlandTodos(string baseDatos)
        {
            return _Control.obtenerVendedoresSoftlandTodos(baseDatos);
        }

        public List<UsuariosModels> ListarCodVendedorSoft(string basedatos)
        {
            return _Control.ListarCodVendedorSoft(basedatos);
        }

        public List<ProductosModels> ListarCodProdSoft(string basedatos)
        {
            return _Control.ListarCodProdSoft(basedatos);
        }

        public List<VendedorModels> GetVendedores(string basedatos, VendedorModels cliente = null)
        {
            return _Control.GetVendedores(basedatos, cliente);
        }

        public List<AprobadorModels> GetAprobador(int IdAprobador)
        {
            return _Control.GetAprobador(IdAprobador);
        }

        public List<UsuariosModels> GetDatosUsuario(string Id)
        {
            return _Control.GetDatosUsuario(Id);
        }

        public List<ClientesModels> GetDatosClientes(string CodAux, string basedatos)
        {
            return _Control.GetDatosClientes(CodAux, basedatos);
        }

        public List<UsuarioEmpresaModel> ListaUsuarioEmpresas(int idUsuario)
        {
            return _Control.ListaUsuarioEmpresas(idUsuario);
        }

        public List<ParametroFiltradoModel> ObtenerParametroFiltrado(int idempresa)
        {
            return _Control.ObtenerParametroFiltrado(idempresa);
        }
        

        public List<ClientesModels> ObtenerGiro(string basedatos)
        {
            return _Control.ObtenerGiro(basedatos);
        }

        public ClientesModels ObtenerAtributoDescuento(string basedatos, string codaux, string textoAtributo)
        {
            return _Control.ObtenerAtributoDescuento(basedatos, codaux, textoAtributo);
        }

        public List<CiudadModel> ObtenerCiudad(string baseDatos)
        {
            return _Control.ObtenerCiudad(baseDatos);
        }

        public List<ComunaModel> ObtenerComuna(string baseDatos)
        {
            return _Control.ObtenerComuna(baseDatos);
        }

        public CreditoModel ObtenerCredito(string CodAux, string baseDatos)
        {
            return _Control.ObtenerCredito(CodAux, baseDatos);
        }

        public VendedoresSoftlandModels ObtenerVendedorCliente(string CodAux, string baseDatos)
        {
            return _Control.ObtenerVendedorCliente(CodAux, baseDatos);
        }

        public List<NotadeVentaCabeceraModels> RechazarNP(NotadeVentaCabeceraModels nw)
        {
            return _Control.RechazarNotaVenta(nw);
        }

        public List<ClientesModels> ActualizarCorreoCliente(ClientesModels cli, string basedatos)
        {
            return _Control.ActualizarCorreoCliente(cli, basedatos);
        }

        public RespuestaModel AgregarCliente(ClientesModels cliente, string basedatos)
        {
            return _Control.AgregarCliente(cliente, basedatos);
        }

        public RespuestaModel InsertarProductoNuevoDisofi(string basedatos, string CodProdAntiguo, string codProd, string desProd, string codGrupo, string codSubGr, decimal precioVta, string codMon)
        {
            return _Control.InsertarProductoNuevoDisofi(basedatos, CodProdAntiguo, codProd, desProd, codGrupo, codSubGr, precioVta, codMon);
        }

        public RespuestaModel CotizacionInsertarProductoNuevoDisofi(string basedatos, string CodProdAntiguo, string codProd,
            string desProd, string codGrupo, string codSubGr, decimal precioVta, string codMon, string codigoFabrica)
        {
            return _Control.CotizacionInsertarProductoNuevoDisofi(basedatos, CodProdAntiguo, codProd, desProd, codGrupo, codSubGr, precioVta, codMon,codigoFabrica);
        }

        public RespuestaModel ActualizaNotaVentaProductoNuevo(string basedatos, int idDetalle, string codProd)
        {
            return _Control.ActualizaNotaVentaProductoNuevo(basedatos, idDetalle, codProd);
        }

        public List<ProductosModels> ObtenerProductoNuevoDisofi(string basedatos)
        {
            return _Control.ObtenerProductoNuevoDisofi(basedatos);
        }

        public RespuestaModel InsertarProductoNuevoSoftland(string basedatos, string codProd)
        {
            return _Control.InsertarProductoNuevoSoftland(basedatos, codProd);
        }

        public RespuestaModel ExisteCliente(ClientesModels cliente, string basedatos)
        {
            return _Control.ExisteCliente(cliente, basedatos);
        }

        public List<AprobadorModels> GetAprobadorNP(int idEmpresa)
        {
            return _Control.GetAprobadorNP(idEmpresa);
        }

        public List<ProjectManagerModels> obtieneCorreosProManag()
        {
            return _Control.obtieneCorreosProManag();
        }

        public List<SaldosModel> ObtenerSaldo(string RutAux, string CodAux, string basedatos)
        {
            return _Control.ObtenerSaldo(RutAux, CodAux, basedatos);
        }

        public ReporteComisionesModel ObtenerReporteComisiones(string basedatos, string idVendedores, DateTime fechaDesde, DateTime fechaHasta, int tipoUsuario)
        {
            return _Control.ObtenerReporteComisiones(basedatos, idVendedores, fechaDesde, fechaHasta, tipoUsuario);
        }

        public List<EnvioCorreoCentroCostoModel> EnvioCorreoPorCentroCosto(string codigoCC, int idEmpresa)
        {
            return _Control.EnvioCorreoPorCentroCosto(codigoCC, idEmpresa);
        }

        public RespuestaModel validaNumOc(int numOc)
        {
            return _Control.validaNumOc(numOc);
        }

        public RespuestaModel guardaRutaArchivo(string nvNum, string rutaArchivo)
        {
            return _Control.guardaRutaArchivo(nvNum, rutaArchivo);
        }

        public string obtenerCorreoManager(int idNv)
        {
            return _Control.obtenerCorreoManager(idNv);
        }

        public List<NotadeVentaCabeceraModels> CotizacionBuscar_X_NV(string numcotizacion, string basedatos, string CodCliente, string venCod)
        {
            return _Control.CotizacionBuscar_X_NV(numcotizacion, basedatos, CodCliente, venCod);
        }
        #endregion

        #region DashBoard

        public List<VendedoresModel> obtenerVendedoresSoftland(string baseDatos)
        {
            return _Control.obtenerVendedoresSoftland(baseDatos);
        }

        public List<InformeModel> GetInformeVentaActual(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeVentaActual(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeVentaActualAnterior(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeVentaActualAnterior(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeClientesAtendidosActual(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeClientesAtendidosActual(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeClientesAtendidosActualAnterior(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeClientesAtendidosActualAnterior(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeClientesCompraActual(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeClientesCompraActual(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeCantVendedoresActual(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeCantVendedoresActual(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeCantVendedoresActualAnterior(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeCantVendedoresActualAnterior(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeVentasVendedoresActual(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeVentasVendedoresActual(baseDatos, codVendedor);
        }

        public List<InformeModel> GetInformeVentasMesAnterior(string baseDatos, string codVendedor)
        {
            return _Control.GetInformeVentasMesAnterior(baseDatos, codVendedor);
        }

        public List<InformeModel> getVentaTotalDia(string baseDatos, string codVendedor)
        {
            return _Control.getVentaTotalDia(baseDatos, codVendedor);
        }

        public List<InformeModel> getVentaTotalDiaAnterior(string baseDatos, string codVendedor)
        {
            return _Control.getVentaTotalDiaAnterior(baseDatos, codVendedor);
        }

        public List<InformeModel> getInformeClientesVentaDia(string baseDatos, string codVendedor)
        {
            return _Control.getInformeClientesVentaDia(baseDatos, codVendedor);
        }

        public List<InformeModel> getInformeClientesVentaDiaAnterior(string baseDatos, string codVendedor)
        {
            return _Control.getInformeClientesVentaDiaAnterior(baseDatos, codVendedor);
        }

        public List<InformeModel> getCantProductos(string baseDatos, string codVendedor)
        {
            return _Control.getCantProductos(baseDatos, codVendedor);
        }

        public List<InformeModel> getTopProductosActual(string baseDatos, string codVendedor)
        {
            return _Control.getTopProductosActual(baseDatos, codVendedor);
        }

        public List<ClientesModels> getClientesTotales(string baseDatos, string fd, string fh, string estado, string codVendedor)
        {
            return _Control.getClientesTotales(baseDatos, fd, fh, estado, codVendedor);
        }

        public List<ClientesModels> getClientesTotalesBlacklog(string baseDatos, string estado, string codVendedor)
        {
            return _Control.getClientesTotalesBlacklog(baseDatos, estado, codVendedor);
        }

        public List<ClientesModels> getVentasPorClientes(string baseDatos, string fd, string fh, string codVendedor)
        {
            return _Control.getVentasPorClientes(baseDatos, fd, fh, codVendedor);
        }

        public List<TotalNotasDeVenta> getTotalNotasDeVenta(string baseDatos, string fd, string fh, string estado, string codVendedor)
        {
            return _Control.getTotalNotasDeVenta(baseDatos, fd, fh, estado, codVendedor);
        }

        public List<TotalNotasDeVenta> getTotalNotasDeVentaBlacklog(string baseDatos, string estado, string codVendedor)
        {
            return _Control.getTotalNotasDeVentaBlacklog(baseDatos, estado, codVendedor);
        }

        public List<InformeFiltroModel> GetInformeVentaActualFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            return _Control.GetInformeVentaActualFiltro(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> GetInformeVentaFiltroAnterior(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string codVendedor)
        {
            return _Control.GetInformeVentaFiltroAnterior(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> GetInformeClientesAtendidosFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            return _Control.GetInformeClientesAtendidosFiltro(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> GetInformeClientesAtendidosFiltroAnterior(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string codVendedor)
        {
            return _Control.GetInformeClientesAtendidosFiltroAnterior(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> GetInformeClientesCompraFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            return _Control.GetInformeClientesCompraFiltro(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> GetInformeCantVendedoresFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            return _Control.GetInformeCantVendedoresFiltro(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> GetInformeVentasVendedoresFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            return _Control.GetInformeVentasVendedoresFiltro(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> getTopProductosFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            return _Control.getTopProductosFiltro(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public List<InformeFiltroModel> getCantProductosFiltro(string baseDatos, string fechaDesde, string fechaHasta, string codVendedor)
        {
            return _Control.getCantProductosFiltro(baseDatos, fechaDesde, fechaHasta, codVendedor);
        }

        public DataTable obtenerExcelVentasDash(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            return _Control.obtenerExcelVentasDash(baseDatos, fechaDesde, fechaHasta, venCod);
        }

        public DataTable obtenerExcelNV(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            return _Control.obtenerExcelNV(baseDatos, fechaDesde, fechaHasta, venCod);
        }

        public List<NotasDeVentaModel> getNotasDeVentaPorCliente(string baseDatos, string codAux, string fd, string fh, string estado)
        {
            return _Control.getNotasDeVentaPorCliente(baseDatos, codAux, fd, fh, estado);
        }

        public List<NotasDeVentaModel> getNotasDeVentaPorClienteBlacklog(string baseDatos, string codAux, string estado)
        {
            return _Control.getNotasDeVentaPorClienteBlacklog(baseDatos, codAux, estado);
        }

        public List<ProductosModel> getProductos(string baseDatos, string nvnumero)
        {
            return _Control.getProductos(baseDatos, nvnumero);
        }

        public List<ProductosModel> getProductosBlacklog(string baseDatos, string nvnumero)
        {
            return _Control.getProductosBlacklog(baseDatos, nvnumero);
        }

        public List<DetalleVentasModel> getDetalleVentasPorClientes(string baseDatos, string fd, string fh, string codaux)
        {
            return _Control.getDetalleVentasPorClientes(baseDatos, fd, fh, codaux);
        }

        public List<ProductosVentaModel> getProductosVenta(string baseDatos, decimal folio, string tipo)
        {
            return _Control.getProductosVenta(baseDatos, folio, tipo);
        }

        public List<DiagramaModel> getPeriodoActual(string baseDatos)
        {
            return _Control.getPeriodoActual(baseDatos);
        }

        public List<DiagramaModel> getPeriodoAnterior(string baseDatos)
        {
            return _Control.getPeriodoAnterior(baseDatos);
        }

        public List<DiagramaModel> getPeriodoTercero(string baseDatos)
        {
            return _Control.getPeriodoTercero(baseDatos);
        }

        public DataTable obtenerExcelVentasDia(string baseDatos, string venCod)
        {
            return _Control.obtenerExcelVentasDia(baseDatos, venCod);
        }

        public DataTable obtenerExcelNVBacklog(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            return _Control.obtenerExcelNVBacklog(baseDatos, fechaDesde, fechaHasta, venCod);
        }
        #endregion

        #region Dashboard Cobranza

        
        //saldosClientes
        public List<InformacionSaldosModel> getSaldoCuentaClientes(string baseDatos, string fd, string fh, string codVendedor)
        {

            return _Control.getSaldoCuentaClientes(baseDatos, fd, fh, codVendedor);
        }


        //saldos Proveedor
        public List<InformacionSaldosModel> getSaldosCuentasProveedor(string baseDatos, string fd, string fh, string codVendedor)
        {

            return _Control.getSaldosCuentasProveedor(baseDatos, fd, fh, codVendedor);
        }

         // modal clientes detalle saldos
        public List<SaldosModel> ObtenerSaldoClientes(string RutAux, string fd, string fh, string CodAux, string basedatos)
        {
            return _Control.ObtenerSaldoClientes(RutAux, fd, fh, CodAux, basedatos);
        }

        //modal Proveedor saldos
        public List<SaldosModel> ObtenerSaldoProveedor(string RutAux, string fd, string fh, string CodAux, string basedatos)
        {
            return _Control.ObtenerSaldoProveedor(RutAux, fd, fh, CodAux, basedatos);
        }

        // grafico

        public List<periodoSaldosModel> getPeriodos(string baseDatos)
        {
            return _Control.getPeriodos(baseDatos);
        }

        // grafico Proveedores

        public List<periodoSaldosModel> getPeriodosProveedor(string baseDatos)
        {

            return _Control.getPeriodosProveedor(baseDatos);
        }

        // excel clientes

        public DataTable obtenerExcelSaldosClientes(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            return _Control.obtenerExcelSaldosClientes(baseDatos, fechaDesde, fechaHasta, venCod);
        }
        // excel Proveedores
        public DataTable obtenerExcelSaldosProveedores(string baseDatos, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            return _Control.obtenerExcelSaldosProveedores(baseDatos, fechaDesde, fechaHasta, venCod);
        }

        #endregion

        #region Cotizacion


        public RespuestaNotaVentaModel AgregarCotizacion(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotadeVentaCabeceraModels NVC, string iniciales)
        {
            return _Control.AgregarCotizacion(baseDatos, insertaDisofi, insertaSoftland, NVC,iniciales);
        }

        public RespuestaNotaVentaModel AgregarDetalleCotizacion(string baseDatos, bool insertaDisofi, bool insertaSoftland, NotaDeVentaDetalleModels NVD)
        {
            return _Control.AgregarDetalleCotizacion(baseDatos, insertaDisofi, insertaSoftland, NVD);
        }

        public RespuestaModel AgregarDireccionDespachoCotizacion(DireccionDespachoModels DirDes, string baseDatos)
        {
            return _Control.AgregarDireccionDespachoCotizacion(DirDes, baseDatos);
        }

        public List<DireccionDespachoModels> BuscarDireccionDespachoCotizacion(DireccionDespachoModels DirDes, string baseDatos)
        {
            return _Control.BuscarDireccionDespachoCotizacion(DirDes, baseDatos);
        }


        public ReporteCotizacionModel ObtenerPdfCotizacion(string baseDatos,int Id)
        {
            return _Control.ObtenerPdfCotizacion(baseDatos, Id);
        }

        public List<NotadeVentaCabeceraModels> listarCotizacionPendientes(string basedatos, int idEmpresa,string codigoVendedor)
        {
            return _Control.listarCotizacionPendientes(basedatos, idEmpresa,codigoVendedor);
        }

        public List<NotadeVentaCabeceraModels> BuscarCotizacion(NotadeVentaCabeceraModels nw, string basedatos)
        {
            return _Control.BuscarCotizacion(nw, basedatos);
        }
        
       
        public List<NotaDeVentaDetalleModels> BuscarCotizacionDetalle(NotaDeVentaDetalleModels nw, string basedatos)
        {
            return _Control.BuscarCotizacionDetalle(nw, basedatos);
        }

        public List<VendedorModels> ListarVendedores(string basedatos)
        {
            return _Control.ListarVendedores(basedatos);
        }

        public List<NotadeVentaCabeceraModels> CotizacionesPendientesBuscar(string basedatos, string numCotizacion, string vendedor,
                                                int tienefechaCot, string fechaCotizacion,int tienefechaCierre, string fechaCierre)
        {
            return _Control.CotizacionesPendientesBuscar(basedatos, numCotizacion, vendedor, tienefechaCot, fechaCotizacion, tienefechaCierre, fechaCierre);
        }

        public List<ClientesModels> GetContactoCotizacion(string baseDatos, ClientesModels cliente = null)
        {
            return _Control.GetContactoCotizacion(baseDatos, cliente);
        }

        public RespuestaModel agregarCotizacionExtras(int IdCotizacion, DateTime FechaCierre, int nroSolicitud,string NroFinalCotizacion)
        {
            return _Control.agregarCotizacionExtras(IdCotizacion,FechaCierre, nroSolicitud,NroFinalCotizacion);
        }

        public RespuestaModel AgregarClienteCotizacion(ClientesModels cliente, string basedatos)
        {
            return _Control.AgregarClienteCotizacion(cliente, basedatos);
        }
        public RespuestaModel grabarContacto(string codAux, string nombre, string fono, string email,string baseDatos)
        {
            return _Control.grabarContacto(codAux, nombre, fono, email, baseDatos);
        }

        public RespuestaModel ActualizaCabeceraCotizacion(int numCot, string fechaEmision, string fechaEntrega, string fechaCierre,
                                                        string contacto, string observacion)
        {
            return _Control.ActualizaCabeceraCotizacion(numCot,fechaEmision,fechaEntrega,fechaCierre,contacto,observacion);
        }
        public ProductoCotizacionModels BuscaPro_X_Modificar(int nroLinea, string codProd,int Id, string baseDatos)
        {
            return _Control.BuscaPro_X_Modificar(nroLinea, codProd,Id,baseDatos);
        }

        public RespuestaModel Detalle_X_Modificar(int id, double precio, string grupo, string subGrupo, string codmon,
                                    double cantidad, double descuento, string baseDato, double valorDescuento,
                                    string fechaEntrega, string codigoFabrica, string codProd)
        {
            return _Control.Detalle_X_Modificar( id, precio, grupo, subGrupo, codmon, cantidad, descuento,baseDato,valorDescuento,fechaEntrega,codigoFabrica,codProd);
        }
        public RespuestaModel EliminarDetalle(int Linea,int IdCotizacion)
        {
            return _Control.EliminarDetalle(Linea,IdCotizacion);
        }

       
        public VendedoresSoftlandModels ObtenerNombreVendedor(string codigo, string baseDatos)
        {
            return _Control.ObtenerNombreVendedor(codigo, baseDatos);
        }
        #endregion

        #region Reporte Saldo
        public List<UsuarioEmpresaModels> GetEmpresas()
        {
            return _Control.GetEmpresas();
        }

        public DataTable obtenerExcelSaldo(string bd, string Anio, string Fecha)
        {
            return _Control.obtenerExcelSaldo(bd, Anio, Fecha);
        }


        #endregion

        #region comisiones
        public ReporteComisionConsolidadoModel ObtenerReporteConsolidado(string basedatos, DateTime fechaDesde, DateTime fechaHasta)
        {
            return _Control.ObtenerReporteConsolidado(basedatos, fechaDesde, fechaHasta);
        }
        public DataTable ObtenerReporteConsolidadoExcel(string basedatos, DateTime fechaDesde, DateTime fechaHasta)
        {
            return _Control.ObtenerReporteConsolidadoExcel(basedatos, fechaDesde, fechaHasta);
        }

        public RespuestaModels GrabarComision(VendedorComision VendedorComision)
        {
            return _Control.GrabarComision(VendedorComision);
        }
        #endregion

        #region Mantenedores
        public RespuestaModel GrabarProducto(ProductosMatenedorModel producto)
        {
            return _Control.GrabarProducto(producto);
        }

        #region Consideracion
        public List<ConsideracionesModel> ListarConsideraciones()
        {
            return _Control.ListarConsideraciones();
        }
        public RespuestaModel GrabarConsideracion(string titulo,string consideracion)
        {
            return _Control.GrabarConsideracion(titulo, consideracion);
        }

        public RespuestaModel ModificarConsideracion(int id, string titulo, string consideracion)
        {
            return _Control.ModificarConsideracion(id,titulo, consideracion);
        }

        public RespuestaModel EliminarConsideracion(int id)
        {
            return _Control.EliminarConsideracion(id);
        }
        #endregion

        #endregion

        #region Archivos Adjuntos

        public RespuestaModel insertarRutaAdjunto(string path, string codAux)
        {
            return _Control.insertarRutaAdjunto(path,codAux);
        }
        
        public List<AdjuntosModels> obtenerArchivosAdjuntos(string CodAux)
        {
            return _Control.obtenerArchivosAdjuntos(CodAux);
        }

        public RespuestaModel limpiarAdjuntosPorCodAux(string CodAux)
        {
            return _Control.limpiarAdjuntosPorCodAux(CodAux);
        }

        #endregion
    }
}
