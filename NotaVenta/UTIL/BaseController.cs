﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NotaVenta.UTIL.Error;
using UTIL.Models;
using UTIL.Objetos;
using System.IO;
using Ionic.Zip;
using UTIL;

namespace NotaVenta.UTIL
{
    public class BaseController : Controller
    {
        private ControlDisofi _control = new ControlDisofi();

        public string baseDatosUsuario()
        {
            return SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.BaseDatos;
        }
        
        public UsuarioEmpresaModel EmpresaUsuario()
        {
            return SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel;
        }
       
        public string codigoVendedorUsuario()
        {
            return SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.VenCod;
        }
        
        public ObjetoUsuario obtenerUsuario()
        {
            return SessionVariables.SESSION_DATOS_USUARIO;
        }
        
        public ParametrosModels ObtieneParametros(int idEmpresa = -1)
        {
            if (idEmpresa == -1) { idEmpresa = SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.IdEmpresa; }
            return controlDisofi().BuscarParametros(idEmpresa);
        }
        public List<menuModels> obtieneMenus()
        {
            
            return controlDisofi().BuscarMenus();
        }



        public ControlDisofi controlDisofi()
        {
            return _control;
        }

        public JsonResult SeleccionaEmpresa(int _IdEmpresa)
        {
            UsuarioEmpresaModel ue = new UsuarioEmpresaModel();
            var tipoUsuario = SessionVariables.SESSION_DATOS_USUARIO.TipoUsuario;
            List<UsuarioEmpresaModel> empresas = controlDisofi().ListaUsuarioEmpresas(SessionVariables.SESSION_DATOS_USUARIO.IdUsuario);
            UsuarioEmpresaModel usuarioEmpresaModel = empresas.Where(m => m.IdEmpresa == _IdEmpresa).First();
            SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel = usuarioEmpresaModel;
            if (tipoUsuario==3) {

             List<ParametroFiltradoModel> param = controlDisofi().ObtenerParametroFiltrado(usuarioEmpresaModel.IdEmpresa);
                //filtro los parametros para saber cual esta activado
                if (param.Count==1) { // si devulve solo una fila, se redirecciona directamente a la pagina

                    return Json(new { Validador=2, action = param[0].Action, controlador = param[0].Controller, NombreEmpresa =usuarioEmpresaModel.NombreEmpresa });
                }
            }

            return Json(new { Validador = 1, NombreEmpresa = usuarioEmpresaModel.NombreEmpresa });
        }

        public ActionResult AbrirError(Errores.ERRORES codigoError, TipoAccionError.TIPO_ACCION_BTN tipoAccionBtn)
        {
            return RedirectToAction("Index", "Error", new { codigoError = codigoError, tipoAccionBtn = tipoAccionBtn });
        }
        
        public ActionResult AbrirError(Errores.ERRORES codigoError, TipoAccionError.TIPO_ACCION_BTN tipoAccionBtn,
            string controller, string action, string texto)
        {
            return RedirectToAction("Index", "Error", new { codigoError = codigoError, tipoAccionBtn = tipoAccionBtn, controller = controller, action = action, texto = texto });
        }
        
        public ActionResult DescargarArchivo(string path, string nombreArchivo = "")
        {
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = new System.IO.FileInfo(path).Name;
            string fileExtension = new System.IO.FileInfo(path).Extension;

            nombreArchivo = nombreArchivo == "" ? fileName : nombreArchivo;

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreArchivo + fileExtension);
        }

        public FileResult DescargarArchivos(int nvNum)
        {
            string nvNumero = Convert.ToString(nvNum);
            string path = Server.MapPath("~/Archivos/");
            DirectoryInfo di = new DirectoryInfo(path);

            
            if(di.GetFiles("*", SearchOption.AllDirectories).Count() > 0)
            {
                using (ZipFile zip = new ZipFile())
                {
                    foreach (var fi in di.GetFiles("*", SearchOption.AllDirectories))
                    {
                        if (fi.Name.Contains(Convert.ToString(nvNum + "_")))
                        {
                            var rutaArchivo = Path.Combine(Server.MapPath("~/Archivos"), fi.Name);

                            var fileName = Path.GetFileName(rutaArchivo);
                            var archivo = System.IO.File.ReadAllBytes(rutaArchivo);

                            zip.AddEntry(fileName, archivo);
                        }
                    }
                    var nombreDelZip = "Archivos_Adjuntos_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-tt") + ".zip";

                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        return File(output.ToArray(), "application/zip", nombreDelZip);
                    }
                }
            }
            else
            {
                return null;
            }
        }


        public JsonResult GenerePdfCotizacion(int Id)
        {
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Cotizaciones/");
                string rutas = "";

                ReporteCotizacionModel reporteComision = controlDisofi().ObtenerPdfCotizacion(baseDatosUsuario(),Id);

                List<ConsideracionesModel> consideraciones = new List<ConsideracionesModel>();
                consideraciones = controlDisofi().ListarConsideraciones();

                RespuestaModel response = new NotaVenta.UTIL.PDF.PDF().GeneraPdfCotizacionII(mapPath, reporteComision,consideraciones);
                rutas = response.RutaArchivo;
                SessionVariables.SESSION_RUTA_PDF_COTIZACION = rutas;
                return Json(response.Mensaje);
            }
            catch (Exception ex)
            {
                string error = ex.ToString();
                return Json(new { Verificador = false, Mensaje = "Se produjo un error al generar el reporte, favor reintentelo, si el problema persiste, comuniquese con su administrador" });
            }
        }

        public ActionResult DescargarArchivoCotizacion(int idCotizacion)
        {
            string path = idCotizacion == 0 ? SessionVariables.SESSION_RUTA_PDF_COTIZACION : Server.MapPath("~/Cotizaciones/pdf_informe_" + idCotizacion.ToString() + ".pdf");
            LogUser.agregarLog(path);
            byte[] fileBytes = System.IO.File.ReadAllBytes(path);
            string fileName = new System.IO.FileInfo(path).Name;
            string fileExtension = new System.IO.FileInfo(path).Extension;
            LogUser.agregarLog(fileName);
            LogUser.agregarLog(fileExtension);
            //nombreArchivo = nombreArchivo == "" ? fileName : nombreArchivo;

            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName /*+ fileExtension*/);
        }

        public FileResult DescargarArchivosCotizacionAdjunto(int IdCotizacion)
        {
            //string Id_Cotizacion = Convert.ToString(IdCotizacion);
            string path = Server.MapPath("~/ArchivosCotizacion/");
            LogUser.agregarLog(path);
            DirectoryInfo di = new DirectoryInfo(path);


            if (di.GetFiles("*", SearchOption.AllDirectories).Count() > 0)
            {
                using (ZipFile zip = new ZipFile())
                {
                    foreach (var fi in di.GetFiles("*", SearchOption.AllDirectories))
                    {
                        if (fi.Name.Contains(Convert.ToString(IdCotizacion + "_")))
                        {
                            var rutaArchivo = Path.Combine(Server.MapPath("~/ArchivosCotizacion"), fi.Name);

                            var fileName = Path.GetFileName(rutaArchivo);
                            var archivo = System.IO.File.ReadAllBytes(rutaArchivo);
                            LogUser.agregarLog(fileName);
                            LogUser.agregarLog(rutaArchivo);
                            zip.AddEntry(fileName, archivo);
                        }
                    }

                    var nombreDelZip = "Archivos_Adjuntos_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-tt") + ".zip";

                    using (MemoryStream output = new MemoryStream())
                    {
                        zip.Save(output);
                        return File(output.ToArray(), "application/zip", nombreDelZip);
                    }
                }
            }
            else
            {
                return null;
            }
        }
    }
}