﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using iTextSharp.text;
using iTextSharp.text.pdf;
using UTIL.Models;
using UTIL.Objetos;
using UTIL.Formato;
using UTIL;

namespace NotaVenta.UTIL.PDF
{
    public class PDF : FuncionesPDF
    {
        Document pdfDoc = new Document(iTextSharp.text.PageSize.A4, 15F, 15F, 30F, 30F);
        PdfWriter pdfWriter;

        #region Pdf Comisiones

        public RespuestaModel generaPdfComisiones(string ruta, List<UsuariosModels> vendedores, ReporteComisionesModel reporteComision, int tipoUsuario)
        {
            RespuestaModel respuesta = new RespuestaModel();
            string rutaCompletaArchivo = "";
            rutaCompletaArchivo = ruta + "pdf_informe_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff") + ".pdf";

            respuesta.Verificador = true;
            respuesta.Mensaje = "Reporte generado";
            respuesta.RutaArchivo = rutaCompletaArchivo;

            MemoryStream stream = new MemoryStream();

            for (int x = 0; x < vendedores.Count; x++)
            {
                try
                {
                    PdfPTable tblSaltoLinea = new PdfPTable(1);
                    //tblSaltoLinea.AddCell(new PdfPCell() { Padding = 5f });

                    PdfPTable tblEncabezado = generaPdfComisionesEncabezado(reporteComision, vendedores[x].VenDes);


                    List<ComisionModel> comision = reporteComision.Comisiones.Where(m => m.CodigoVendedor == vendedores[x].VenCod).ToList();
                    if (comision == null)
                    {
                        comision = new List<ComisionModel>();
                    }
                    PdfPTable tblTabla = generaPdfComisionesTabla(comision, tipoUsuario);

                    ReporteComisionesTotalModel total = reporteComision.Totales.Where(m => m.CodigoVendedor == vendedores[x].VenCod).FirstOrDefault();
                    if (total == null)
                    {
                        total = new ReporteComisionesTotalModel();
                    }
                    PdfPTable tblTotales = generaPdfComisionesTotales(total);

                    using (FileStream fs = new FileStream(rutaCompletaArchivo, FileMode.Create))
                    {
                        using (iTextSharp.text.Document doc = new iTextSharp.text.Document())
                        {

                            PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                            doc.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());

                            doc.Open();

                            doc.Add(tblEncabezado);
                            doc.Add(tblTabla);
                            doc.Add(tblTotales);
                            doc.SetPageSize(iTextSharp.text.PageSize.LETTER);

                            doc.Close();

                        }
                        fs.Close();
                    }

                }
                catch (DocumentException de)
                {
                    respuesta.Verificador = false;
                    respuesta.Mensaje = "Error: no se puede generar el reporte";
                    Console.Error.WriteLine(de.Message);
                }
                catch (IOException ioe)
                {
                    respuesta.Verificador = false;
                    respuesta.Mensaje = "Error: no se puede generar el reporte";
                    Console.Error.WriteLine(ioe.Message);
                }
            }

            return respuesta;
        }
        public RespuestaModel generaPdfComisionesConsolidado(string ruta, ReporteComisionConsolidadoModel reporteConsolidado)
        {
            RespuestaModel respuesta = new RespuestaModel();
            string rutaCompletaArchivo = "";
            rutaCompletaArchivo = ruta + "pdf_informe_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff") + ".pdf";

            respuesta.Verificador = true;
            respuesta.Mensaje = "Reporte generado";
            respuesta.RutaArchivo = rutaCompletaArchivo;

            MemoryStream stream = new MemoryStream();

            try
            {
                PdfPTable tblSaltoLinea = new PdfPTable(1);

                PdfPTable tblTotales = PdfComisionesConsolidado(reporteConsolidado.Consolidado);

                using (FileStream fs = new FileStream(rutaCompletaArchivo, FileMode.Create))
                {
                    using (iTextSharp.text.Document doc = new iTextSharp.text.Document())
                    {
                        PdfWriter writer = PdfWriter.GetInstance(doc, fs);
                        doc.SetPageSize(iTextSharp.text.PageSize.LETTER.Rotate());

                        doc.Open();
                        doc.Add(tblTotales);
                        doc.SetPageSize(iTextSharp.text.PageSize.LETTER);
                        doc.Close();
                    }
                    fs.Close();
                }
            }
            catch (DocumentException de)
            {
                respuesta.Verificador = false;
                respuesta.Mensaje = "Error: no se puede generar el reporte";
                Console.Error.WriteLine(de.Message);
            }
            catch (IOException ioe)
            {
                respuesta.Verificador = false;
                respuesta.Mensaje = "Error: no se puede generar el reporte";
                Console.Error.WriteLine(ioe.Message);
            }
            return respuesta;
        }

        private PdfPTable PdfComisionesConsolidado(List<ReporteComisionesConsolidado> TotalConsolidado)
        {
            float[] widths = new float[0];
            List<PdfPCell> columnas = new List<PdfPCell>();

            columnas.Add(creaColumna("Codigo", new PDFOpciones()));
            columnas.Add(creaColumna("Vendedor", new PDFOpciones()));
            columnas.Add(creaColumna("Suma de Total VENTA", new PDFOpciones()));
            columnas.Add(creaColumna("Suma Margen en Pesos", new PDFOpciones()));
            columnas.Add(creaColumna("Suma de Costo x Cantidad", new PDFOpciones()));
            columnas.Add(creaColumna("Suma de Comision Pesos", new PDFOpciones()));
            columnas.Add(creaColumna("Cantidad de Notas Creditos", new PDFOpciones()));
            columnas.Add(creaColumna("Monto de Nota Credito", new PDFOpciones()));
            columnas.Add(creaColumna("MG en porcentaje", new PDFOpciones()));

            widths = new float[]
            {
                obtenerPorcentaje(100f, 4),
                obtenerPorcentaje(100f, 20),
                obtenerPorcentaje(100f, 7),
                obtenerPorcentaje(100f, 7),
                obtenerPorcentaje(100f, 7),
                obtenerPorcentaje(100f, 6),
                obtenerPorcentaje(100f, 5),
                obtenerPorcentaje(100f, 6),
                obtenerPorcentaje(100f, 7),

            };

            PdfPTable tblActividades = new PdfPTable(columnas.Count);
            tblActividades.WidthPercentage = 100;
            tblActividades.SetWidths(widths);

            for (int x = 0; x < columnas.Count; x++)
            {
                tblActividades.AddCell(columnas[x]);
            }


            int contadorRowSpan = 0;
            List<PdfPCell> listaCeldas = new List<PdfPCell>();
            PdfPCell cell1 = null;
            PdfPCell cell2 = null;
            PdfPCell cell3 = null;
            PdfPCell cell4 = null;
            PdfPCell cell5 = null;
            PdfPCell cell6 = null;
            PdfPCell cell7 = null;
            PdfPCell cell8 = null;
            PdfPCell cell9 = null;

            double TotalVENTA = 0;
            double TotalMargenenPesos = 0;
            double TotalCostoxCantidad = 0;
            double TotalComisionPesos = 0;
            double CantidadNC = 0;
            double TotalNotaCredito = 0;
            double TotalMg = 0;
            for (int x = 0; x < TotalConsolidado.Count; x++)
            {
                listaCeldas = new List<PdfPCell>();

                cell1 = creaCelda(TotalConsolidado[x].CodigoVendedor, new PDFOpciones() { });
                listaCeldas.Add(cell1);

                cell2 = creaCelda(TotalConsolidado[x].Vendedor, new PDFOpciones() { });
                listaCeldas.Add(cell2);

                cell3 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalConsolidado[x].SumadeTotalVENTA), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                listaCeldas.Add(cell3);
                TotalVENTA += TotalConsolidado[x].SumadeTotalVENTA;

                cell4 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalConsolidado[x].SumaMargenenPesos), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                listaCeldas.Add(cell4);
                TotalMargenenPesos += TotalConsolidado[x].SumaMargenenPesos;

                cell5 = creaCelda(FormatoInt.agregarSeparadorMiles((int)TotalConsolidado[x].SumadeCostoxCantidad), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                listaCeldas.Add(cell5);
                TotalCostoxCantidad += TotalConsolidado[x].SumadeCostoxCantidad;

                cell6 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalConsolidado[x].SumadeComisionPesos), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                listaCeldas.Add(cell6);
                TotalComisionPesos += TotalConsolidado[x].SumadeComisionPesos;

                cell7 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalConsolidado[x].CantidaddeNotasCreditos), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                listaCeldas.Add(cell7);
                CantidadNC += TotalConsolidado[x].CantidaddeNotasCreditos;

                cell8 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalConsolidado[x].MontodeNotaCredito), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                listaCeldas.Add(cell8);
                TotalNotaCredito += TotalConsolidado[x].MontodeNotaCredito;

                cell9 = creaCelda(Math.Round(TotalConsolidado[x].MGenporcentaje, 6).ToString(), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                listaCeldas.Add(cell9);
                TotalMg += TotalConsolidado[x].MGenporcentaje;


                for (int z = 0; z < listaCeldas.Count; z++)
                {
                    tblActividades.AddCell(listaCeldas[z]);
                }
            }

            listaCeldas = new List<PdfPCell>();
            cell1 = creaCelda("Total General",
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            cell2 = creaCelda("", new PDFOpciones() { });
            cell3 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalVENTA),
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            cell4 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalMargenenPesos),
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });

            cell5 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalCostoxCantidad),
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            cell6 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalComisionPesos),
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            cell7 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)CantidadNC),
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            cell8 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)TotalNotaCredito),
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            cell9 = creaCelda(Math.Round(TotalMg, 6).ToString(),
                new PDFOpciones()
                {
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA,
                    10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            listaCeldas.Add(cell1);
            listaCeldas.Add(cell2);
            listaCeldas.Add(cell3);
            listaCeldas.Add(cell4);
            listaCeldas.Add(cell5);
            listaCeldas.Add(cell6);
            listaCeldas.Add(cell7);
            listaCeldas.Add(cell8);
            listaCeldas.Add(cell9);

            for (int i = 0; i < listaCeldas.Count; i++)
            {
                tblActividades.AddCell(listaCeldas[i]);
            }


            return tblActividades;
        }



        private PdfPTable generaPdfComisionesEncabezado(ReporteComisionesModel reporteComision, string nombreVendedor)
        {
            PdfPTable tblEncabezado = new PdfPTable(1);
            tblEncabezado.WidthPercentage = 100;

            PdfPCell celdaTitulo = creaCelda(reporteComision.Titulo,
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Centrado,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 16, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });

            PdfPCell celdaNombre = creaCelda("Vendedor: " + nombreVendedor,
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });

            tblEncabezado.AddCell(celdaTitulo);
            tblEncabezado.AddCell(celdaNombre);

            return tblEncabezado;
        }

        private PdfPTable generaPdfComisionesTabla(List<ComisionModel> comisiones, int tipoUsuario)
        {
            float[] widths = new float[0];
            List<PdfPCell> columnas = new List<PdfPCell>();

            columnas.Add(creaColumna("Tipo Factura", new PDFOpciones()));
            columnas.Add(creaColumna("Fecha", new PDFOpciones()));
            columnas.Add(creaColumna("Codigo", new PDFOpciones()));
            columnas.Add(creaColumna("Descripcion", new PDFOpciones()));
            columnas.Add(creaColumna("Lista Precio", new PDFOpciones()));
            columnas.Add(creaColumna("Cantidad", new PDFOpciones()));
            columnas.Add(creaColumna("Precio", new PDFOpciones()));
            columnas.Add(creaColumna("Totales", new PDFOpciones()));
            columnas.Add(creaColumna("%", new PDFOpciones()));
            columnas.Add(creaColumna("Comision", new PDFOpciones()));
            columnas.Add(creaColumna("Cliente", new PDFOpciones()));
            if (tipoUsuario == 4)
            {
                columnas.Add(creaColumna("CostoProm", new PDFOpciones()));
                columnas.Add(creaColumna("CostoTotalProm", new PDFOpciones()));
            }

            if (tipoUsuario == 4)
            {
                widths = new float[] {
                obtenerPorcentaje(100f, 10),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 20),
                obtenerPorcentaje(100f, 6),
                obtenerPorcentaje(100f, 6),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 4),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 20),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 8)
                };
            }
            else
            {
                widths = new float[] {
                obtenerPorcentaje(100f, 10),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 20),
                obtenerPorcentaje(100f, 6),
                obtenerPorcentaje(100f, 6),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 4),
                obtenerPorcentaje(100f, 8),
                obtenerPorcentaje(100f, 20)
                };
            }

            PdfPTable tblActividades = new PdfPTable(columnas.Count);
            tblActividades.WidthPercentage = 100;
            tblActividades.SetWidths(widths);

            for (int x = 0; x < columnas.Count; x++)
            {
                tblActividades.AddCell(columnas[x]);
            }


            int contadorRowSpan = 0;

            if (tipoUsuario == 4)
            {
                List<PdfPCell> listaCeldas = new List<PdfPCell>();
                PdfPCell cell1 = null;
                PdfPCell cell2 = null;
                PdfPCell cell3 = null;
                PdfPCell cell4 = null;
                PdfPCell cell5 = null;
                PdfPCell cell6 = null;
                PdfPCell cell7 = null;
                PdfPCell cell8 = null;
                PdfPCell cell9 = null;
                PdfPCell cell10 = null;
                PdfPCell cell11 = null;
                PdfPCell cell12 = null;
                PdfPCell cell13 = null;

                for (int x = 0; x < comisiones.Count; x++)
                {
                    listaCeldas = new List<PdfPCell>();

                    cell1 = creaCelda(comisiones[x].TipoFactura, new PDFOpciones() { });
                    listaCeldas.Add(cell1);

                    cell2 = creaCelda(comisiones[x].Fecha, new PDFOpciones() { });
                    listaCeldas.Add(cell2);

                    cell3 = creaCelda(comisiones[x].Codigo, new PDFOpciones() { });
                    listaCeldas.Add(cell3);

                    cell4 = creaCelda(comisiones[x].Descripcion, new PDFOpciones() { });
                    listaCeldas.Add(cell4);

                    cell5 = creaCelda(comisiones[x].ListaPrecio, new PDFOpciones() { });
                    listaCeldas.Add(cell5);

                    cell6 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Cantidad), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell6);

                    cell7 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Precio, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell7);

                    cell8 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Totales, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell8);

                    cell9 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Porcentaje, simboloDespues: "%"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell9);

                    cell10 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Comision, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell10);

                    cell11 = creaCelda(comisiones[x].Cliente, new PDFOpciones() { });
                    listaCeldas.Add(cell11);

                    cell12 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].CostoProm, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell12);

                    cell13 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].CostoTotalProm, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell13);

                    for (int z = 0; z < listaCeldas.Count; z++)
                    {
                        tblActividades.AddCell(listaCeldas[z]);
                    }
                }
            }
            else
            {
                List<PdfPCell> listaCeldas = new List<PdfPCell>();
                PdfPCell cell1 = null;
                PdfPCell cell2 = null;
                PdfPCell cell3 = null;
                PdfPCell cell4 = null;
                PdfPCell cell5 = null;
                PdfPCell cell6 = null;
                PdfPCell cell7 = null;
                PdfPCell cell8 = null;
                PdfPCell cell9 = null;
                PdfPCell cell10 = null;
                PdfPCell cell11 = null;

                for (int x = 0; x < comisiones.Count; x++)
                {
                    listaCeldas = new List<PdfPCell>();

                    cell1 = creaCelda(comisiones[x].TipoFactura, new PDFOpciones() { });
                    listaCeldas.Add(cell1);

                    cell2 = creaCelda(comisiones[x].Fecha, new PDFOpciones() { });
                    listaCeldas.Add(cell2);

                    cell3 = creaCelda(comisiones[x].Codigo, new PDFOpciones() { });
                    listaCeldas.Add(cell3);

                    cell4 = creaCelda(comisiones[x].Descripcion, new PDFOpciones() { });
                    listaCeldas.Add(cell4);

                    cell5 = creaCelda(comisiones[x].ListaPrecio, new PDFOpciones() { });
                    listaCeldas.Add(cell5);

                    cell6 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Cantidad), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell6);

                    cell7 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Precio, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell7);

                    cell8 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Totales, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell8);

                    cell9 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Porcentaje, simboloDespues: "%"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell9);

                    cell10 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)comisiones[x].Comision, simboloAntes: "$"), new PDFOpciones() { AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha });
                    listaCeldas.Add(cell10);

                    cell11 = creaCelda(comisiones[x].Cliente, new PDFOpciones() { });
                    listaCeldas.Add(cell11);

                    for (int z = 0; z < listaCeldas.Count; z++)
                    {
                        tblActividades.AddCell(listaCeldas[z]);
                    }
                }
            }

            return tblActividades;
        }

        private PdfPTable generaPdfComisionesTotales(ReporteComisionesTotalModel reporteComisionesTotal)
        {
            float[] widths = new float[]
            {
                obtenerPorcentaje(100f, 60),
                obtenerPorcentaje(100f, 25),
                obtenerPorcentaje(100f, 15),
            };

            PdfPTable tblEncabezado = new PdfPTable(3);
            tblEncabezado.WidthPercentage = 100;
            tblEncabezado.SetWidths(widths);

            tblEncabezado.WidthPercentage = 100;
            int tamanoLetra = 10;

            PdfPCell celdaTitulo1_1 = creaCelda("Total Facturado Vendedor: ",
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            PdfPCell celdaTitulo1_2 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)reporteComisionesTotal.TotalFacturado, simboloAntes: "$"),
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            PdfPCell celdaTitulo2_1 = creaCelda("Total Nota de Credito: ",
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            PdfPCell celdaTitulo2_2 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)reporteComisionesTotal.TotalNotaCredito, simboloAntes: "$"),
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            PdfPCell celdaTitulo3_1 = creaCelda("Total Vendido Real: ",
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            PdfPCell celdaTitulo3_2 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)reporteComisionesTotal.TotalVendidoReal, simboloAntes: "$"),
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            PdfPCell celdaTitulo4_1 = creaCelda("Total Comision: ",
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            PdfPCell celdaTitulo4_2 = creaCelda(FormatoInt.agregarSeparadorMiles((int?)reporteComisionesTotal.TotalComision, simboloAntes: "$"),
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    AlineacionHorizontal = PdfAlineacionHorizontalPdf.Derecha,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });

            PdfPCell celdaEnBlanco = creaCelda("",
                new PDFOpciones()
                {
                    TipoColor = PdfTipoColorFondo.SinColor,
                    TamanoBorde = 0,
                    Fuente = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, tamanoLetra, iTextSharp.text.Font.NORMAL, BaseColor.BLACK)
                });
            tblEncabezado.AddCell(celdaEnBlanco);
            tblEncabezado.AddCell(celdaTitulo1_1);
            tblEncabezado.AddCell(celdaTitulo1_2);

            tblEncabezado.AddCell(celdaEnBlanco);
            tblEncabezado.AddCell(celdaTitulo2_1);
            tblEncabezado.AddCell(celdaTitulo2_2);

            tblEncabezado.AddCell(celdaEnBlanco);
            tblEncabezado.AddCell(celdaTitulo3_1);
            tblEncabezado.AddCell(celdaTitulo3_2);

            tblEncabezado.AddCell(celdaEnBlanco);
            tblEncabezado.AddCell(celdaTitulo4_1);
            tblEncabezado.AddCell(celdaTitulo4_2);

            return tblEncabezado;
        }

        #endregion

        private float obtenerPorcentaje(float valorTotal, int porcentaje)
        {
            return (valorTotal * porcentaje) / 100;
        }

        #region PDFCotizacion

        public RespuestaModel GeneraPdfCotizacionII(string ruta, ReporteCotizacionModel reporteCotizacion, List<ConsideracionesModel> consideraciones)
        {
            RespuestaModel respuesta = new RespuestaModel();
            string rutaCompletaArchivo = "";
            rutaCompletaArchivo = ruta + "pdf_informe_" + reporteCotizacion.Cabecera[0].Id + ".pdf";
            pdfWriter = PdfWriter.GetInstance(pdfDoc, new FileStream(rutaCompletaArchivo, FileMode.Create));

            Font FontB6 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.BOLD));
            Font FontB7 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.BOLD));
            Font FontB8 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.BOLD));

            Font Font12 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 12, iTextSharp.text.Font.BOLD));
            Font Font5 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 5, iTextSharp.text.Font.NORMAL));
            Font Font6 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 6, iTextSharp.text.Font.NORMAL));
            Font Font7 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 7, iTextSharp.text.Font.NORMAL));
            Font Font8 = new Font(FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL));


            PdfPCell CVacio = new PdfPCell(new Phrase(""));
            CVacio.Border = 0;

            pdfDoc.Open();

            PdfPCell Col1;
            PdfPCell Col2;
            PdfPCell Col3;
            PdfPCell Col4;
            PdfPCell Col5;
            PdfPCell Col6;
            PdfPCell Col7;
            PdfPCell Col8;
            PdfPCell Col9;
            double ILine;
            int iFila;

            #region Encabezado

            string imagenURL = "c:\\temp\\LogoE.bmp";
            iTextSharp.text.Image imagenBMP;
            imagenBMP = iTextSharp.text.Image.GetInstance(imagenURL);

            //imagenBMP.ScaleToFit(110.0F, 140.0F);
            imagenBMP.ScaleToFit(510.0F, 140.0F);
            imagenBMP.SpacingBefore = 20.0F;
            imagenBMP.SpacingAfter = 10.0F;
            imagenBMP.SetAbsolutePosition(40, 780);
            //imagenBMP.SetAbsolutePosition(40, 780);
            pdfDoc.Add(imagenBMP);


            pdfDoc.Add(new Paragraph("  "));
            pdfDoc.Add(new Paragraph("  "));

            PdfPTable Table1 = new PdfPTable(4);
            Table1.WidthPercentage = 95;
            float[] widths = new float[] {
                2f,
                6f,
                1f,
                5f
                };
            Table1.SetWidths(widths);

            Col1 = new PdfPCell(new Phrase("RAZON SOCIAL", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.DatosEmpresa[0].NomB, FontB8));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Col3 = new PdfPCell(new Phrase(" ", FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);

            Table1.AddCell(CVacio);
            Col1 = new PdfPCell(new Phrase("RUT", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.DatosEmpresa[0].RutE, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase(" ", FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);

            Col1 = new PdfPCell(new Phrase("GIRO", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.DatosEmpresa[0].Giro, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase(" ", FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);

            Col1 = new PdfPCell(new Phrase("DIRECCION", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.DatosEmpresa[0].direccionEmpresa, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase("Fecha                   " + reporteCotizacion.Cabecera[0].nvFemString, FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);

            Col1 = new PdfPCell(new Phrase("TELEFONO", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase("(+56)"+ reporteCotizacion.DatosEmpresa[0].fono, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase("N° Cotizacion      " + reporteCotizacion.Cabecera[0].NroFinalCotizacion, FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);

            Col1 = new PdfPCell(new Phrase("E-MAIL", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.DatosEmpresa[0].EmailDTE, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase("N° Solicitud         " + reporteCotizacion.Cabecera[0].NroCotizacion, FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);


            Col1 = new PdfPCell(new Phrase("ATENCION A", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.Cliente[0].NombreContacto, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase("Validez Oferta     15 dias corridos", FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);

            Col1 = new PdfPCell(new Phrase(" ", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.Cliente[0].EmailContacto, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase("Plazo de pago      " + reporteCotizacion.Cabecera[0].PlazoPago, FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);

            Col1 = new PdfPCell(new Phrase(" ", FontB8));
            Col1.Border = 0;
            Table1.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.Cliente[0].FonoContacto, Font7));
            Col2.Border = 0;
            Table1.AddCell(Col2);
            Table1.AddCell(CVacio);
            Col3 = new PdfPCell(new Phrase("Forma de pago   " + reporteCotizacion.Cabecera[0].FormaPago, FontB8));
            Col3.Border = 0;
            Table1.AddCell(Col3);


            pdfDoc.Add(Table1);
            #endregion

            //PintaCuadro(0.7, 410, 710, 530, 770);
            //PintaLinea(0.5, 10, 705, 550, 705);

            #region Cliente
            PdfPTable Table2 = new PdfPTable(4);
            float[] widths2 = new float[] {
                2.2f,
                2f,
                6f,
                6f
            };
            Table2.WidthPercentage = 95;
            Table2.SetWidths(widths2);


            Col1 = new PdfPCell(new Phrase("DATOS EMPRESA", FontB7));
            Col1.Border = 0;
            Table2.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase("Razon Social", FontB7));
            Col2.Border = 0;
            Table2.AddCell(Col2);
            Col3 = new PdfPCell(new Phrase(reporteCotizacion.Cliente[0].cliente, Font7));
            Col3.Border = 0;
            Table2.AddCell(Col3);
            Col4 = new PdfPCell(new Phrase(" ", Font8));
            Col4.Border = 0;
            Table2.AddCell(Col4);

            Col1 = new PdfPCell(new Phrase(" ", FontB6));
            Col1.Border = 0;
            Table2.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase("RUT", FontB7));
            Col2.Border = 0;
            Table2.AddCell(Col2);
            Col3 = new PdfPCell(new Phrase(reporteCotizacion.Cliente[0].rut, Font7));
            Col3.Border = 0;
            Table2.AddCell(Col3);
            Col4 = new PdfPCell(new Phrase("*Favor enviar su Orden de Compra al correo electrónico:", Font6));
            Col4.HorizontalAlignment = Element.ALIGN_CENTER;
            Col4.Border = 0;
            Table2.AddCell(Col4);

            Col1 = new PdfPCell(new Phrase(" ", FontB6));
            Col1.Border = 0;
            Table2.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase("Direccion", FontB7));
            Col2.Border = 0;
            Table2.AddCell(Col2);
            Col3 = new PdfPCell(new Phrase(reporteCotizacion.Cliente[0].direccion, Font7));
            Col3.Border = 0;
            Table2.AddCell(Col3);
            Col4 = new PdfPCell(new Phrase("odc@emeltec.cl, indicando número de cotización o", Font6));
            Col4.HorizontalAlignment = Element.ALIGN_CENTER;
            Col4.Border = 0;
            Table2.AddCell(Col4);

            Col1 = new PdfPCell(new Phrase(" ", Font8));
            Col1.Border = 0;
            Table2.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase("Telefono", FontB7));
            Col2.Border = 0;
            Table2.AddCell(Col2);
            Col3 = new PdfPCell(new Phrase(reporteCotizacion.Cliente[0].fono, Font7));
            Col3.Border = 0;
            Table2.AddCell(Col3);
            Col4 = new PdfPCell(new Phrase("adjuntando PDF de la misma.", Font6));
            Col4.HorizontalAlignment = Element.ALIGN_CENTER;
            Col4.Border = 0;
            Table2.AddCell(Col4);

            Table2.AddCell(CVacio);
            Table2.AddCell(CVacio);
            pdfDoc.Add(Table2);
            #endregion

            #region CabeceraDetalle
            PdfPTable Table3 = new PdfPTable(9);
            float[] widths3 = new float[] {
                1.0F,  /*Item*/
                3.0F,  /*Codigo*/
                7.0F,  /*Descripcion*/
                2.0F,  /*Cantidad*/
                2.0F,  /*MONEDA*/
                3.0F,  /*Venta Emeltec*/
                2.5F,  /*Total venta*/
                1.0F,  /*vacio*/
                3.5F,  /*Plazo Entrega*/
            };
            Table3.WidthPercentage = 95;
            Table3.SetWidths(widths3);
            Col1 = new PdfPCell(new Phrase("Item", FontB8));
            Col1.Border = 0;
            Table3.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase("Codigo", FontB8));
            Col2.Border = 0;
            Table3.AddCell(Col2);

            Col3 = new PdfPCell(new Phrase("Descripcion", FontB8));
            Col3.Border = 0;
            Table3.AddCell(Col3);

            Col4 = new PdfPCell(new Phrase("Cantidad", FontB8));
            Col4.Border = 0;
            Table3.AddCell(Col4);

            Col5 = new PdfPCell(new Phrase("MONEDA", FontB8));
            Col5.Border = 0;
            Table3.AddCell(Col5);

            Col6 = new PdfPCell(new Phrase("Venta Emeltec", FontB8));
            Col6.Border = 0;
            Table3.AddCell(Col6);

            Col7 = new PdfPCell(new Phrase("Total venta", FontB8));
            Col7.Border = 0;
            Col7.HorizontalAlignment = 2;
            Table3.AddCell(Col7);

            Col8 = new PdfPCell(new Phrase("   ", FontB8));
            Col8.Border = 0;
            Col8.HorizontalAlignment = 2;
            Table3.AddCell(Col8);

            Col9 = new PdfPCell(new Phrase("Plazo Entrega", FontB8));
            Col9.Border = 0;
            Col9.HorizontalAlignment = 2;
            Table3.AddCell(Col9);

            Table3.AddCell(CVacio);
            pdfDoc.Add(Table3);
            #endregion

            #region Detalle

            int ciclo = reporteCotizacion.Detalle.Count;
            int item = 1;
            do
            {
                PdfPTable Table4 = new PdfPTable(9);
                float[] widths4 = new float[] {
                    1.0F,  /*Item*/
                    3.0F,  /*Codigo*/
                    7.0F,  /*Descripcion*/
                    2.0F,  /*Cantidad*/
                    2.0F,  /*MONEDA*/
                    3.0F,  /*Venta Emeltec*/
                    2.5F,  /*Total venta*/
                    1.0F,  /*vacio*/
                    3.5F,  /*Plazo Entrega*/
                };

                Table4.WidthPercentage = 95;
                Table4.SetWidths(widths4);

                string codigo = "";
                string producto = "";
                string cantidad = "";
                string precio = "";
                string total = "";
                string moneda = reporteCotizacion.Cliente[0].CodEdi;
                string fechacierre = "";
                if (reporteCotizacion.Detalle.Count >= item)
                {
                    codigo = reporteCotizacion.Detalle[item - 1].CodProd;
                    producto = reporteCotizacion.Detalle[item - 1].DetProd;
                    cantidad = reporteCotizacion.Detalle[item - 1].nvCant.ToString();
                    precio = "$ " + reporteCotizacion.Detalle[item - 1].nvPrecio.ToString("N0");
                    total = "$ " + reporteCotizacion.Detalle[item - 1].nvTotLinea.ToString("N0");
                    fechacierre = reporteCotizacion.Detalle[item - 1].nvFecComprYYYYMMDD2;
                }
                else
                {
                    moneda = " ";
                }

                Col1 = new PdfPCell(new Phrase(item.ToString(), Font8));
                Col1.HorizontalAlignment = Element.ALIGN_CENTER;
                Col1.Border = 0;
                Table4.AddCell(Col1);
                Col2 = new PdfPCell(new Phrase(codigo, Font8));
                Col2.Border = 0;
                Table4.AddCell(Col2);

                Col3 = new PdfPCell(new Phrase(producto, Font8));
                Col3.Border = 0;
                Table4.AddCell(Col3);

                Col4 = new PdfPCell(new Phrase(cantidad, Font8));
                Col4.Border = 0;
                Table4.AddCell(Col4);

                Col5 = new PdfPCell(new Phrase(moneda, Font8));
                Col5.Border = 0;
                Table4.AddCell(Col5);

                Col6 = new PdfPCell(new Phrase(precio, FontB8));
                Col6.Border = 0;
                Col6.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                Table4.AddCell(Col6);

                Col7 = new PdfPCell(new Phrase(total, FontB8));
                Col7.Border = 0;
                Col7.HorizontalAlignment = PdfPCell.ALIGN_RIGHT; ;
                Table4.AddCell(Col7);

                Col8 = new PdfPCell(new Phrase("   ", FontB8));
                Col8.Border = 0;
                Col8.HorizontalAlignment = 2;
                Table4.AddCell(Col8);

                Col9 = new PdfPCell(new Phrase(fechacierre, FontB8));
                Col9.Border = 0;
                Col9.HorizontalAlignment = 2;
                Table4.AddCell(Col9);

                Table4.AddCell(CVacio);
                pdfDoc.Add(Table4);
                item += 1;

                double ILineII;
                ILineII = pdfWriter.GetVerticalPosition(true);
                PintaLinea(0.1, 10, Convert.ToInt32(ILineII), 590, Convert.ToInt32(ILineII));
                ILineII += 1;
            }
            while (item <= 20);

            #endregion

            //PintaCuadro(0.7, 410, 710, 530, 770);
            PintaLinea(0.5, 10, 705, 590, 705);  //primera linea
            PintaLinea(0.5, 10, 667, 590, 667);  //segunda linea

            //ini lineas detalle
            PintaLinea(0.5, 10, 621, 590, 621);
            PintaLinea(0.5, 10, 610, 590, 610);
            //fin lineas detalle


            //Observacion
            PdfPTable TableObservacion = new PdfPTable(2);
            float[] widthsObs = new float[] {
                    3.0F,  
                    20.0F
                };

            TableObservacion.WidthPercentage = 95;
            TableObservacion.SetWidths(widthsObs);
            Col1 = new PdfPCell(new Phrase("Observaciones: ", Font8));
            Col1.HorizontalAlignment = Element.ALIGN_LEFT;
            Col1.Border = 0;
            TableObservacion.AddCell(Col1);

            Col2 = new PdfPCell(new Phrase(reporteCotizacion.Cabecera[0].Observaciones, Font8));
            Col2.HorizontalAlignment = Element.ALIGN_LEFT;
            Col2.Border = 0;
            TableObservacion.AddCell(Col2);

            pdfDoc.Add(TableObservacion);



            PintaLinea(0.5, 10, 300, 590, 300);
            for (iFila = 1; iFila <= 40; iFila++)
            {
                pdfDoc.Add(new Paragraph(" "));
                ILine = pdfWriter.GetVerticalPosition(true);
                if (ILine < 300)
                {
                    LogUser.agregarLog("ILine" + ILine);
                    break;
                }
            }

            string nvNetoAfecto = reporteCotizacion.Cabecera[0].nvNetoAfecto.ToString("N0");
            #region Totales
            PdfPTable Table5 = new PdfPTable(3);
            float[] widths5 = new float[] {
                3.0F,
                7.0F,
                5.0F
            };
            Table5.WidthPercentage = 56;
            Table5.SetWidths(widths5);

            Table5.AddCell(CVacio);

            Col2 = new PdfPCell(new Phrase("TOTAL NETO (NO INCLUYE IVA): " + reporteCotizacion.Cliente[0].CodEdi, Font8));
            Col2.Border = 0;
            Table5.AddCell(Col2);

            Col3 = new PdfPCell(new Phrase("$ "+nvNetoAfecto, FontB8));
            Col3.Border = 0;
            Col3.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
            Table5.AddCell(Col3);

            pdfDoc.Add(Table5);
            #endregion

            PdfPTable Table11 = new PdfPTable(1);
            float[] widths11 = new float[] {
                25F
            };
            Table11.WidthPercentage = 95;
            Table11.SetWidths(widths11);

            Col1 = new PdfPCell(new Phrase("Se adjuntan las Condiciones Comerciales y Contractuales, versión 2.4 de fecha 08 de septiembre de 2020, las cuales forman parte integrante de esta cotización y la comp", Font6));
            Col1.Border = 0;
            Table11.AddCell(Col1);
            pdfDoc.Add(Table11);
            #region Pie
            PdfPTable Table6 = new PdfPTable(3);
            float[] widths6 = new float[] {
                3.0F,
                7.0F,
                7.0F
            };
            Table6.WidthPercentage = 95;
            Table6.SetWidths(widths6);

            Table6.AddCell(CVacio);
            Col1 = new PdfPCell(new Phrase("", Font8));
            Col1.Border = 0;
            Table6.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase("_________________________", Font8));
            Col2.Border = 0;
            Table6.AddCell(Col2);

            Table6.AddCell(CVacio);
            Col1 = new PdfPCell(new Phrase("", Font8));
            Col1.Border = 0;
            Table6.AddCell(Col1); 
            Col2= new PdfPCell(new Phrase(reporteCotizacion.Cabecera[0].nombreVendedor.ToString(), Font8));
            Col2.Border = 0;
            Table6.AddCell(Col2);

            Table6.AddCell(CVacio);
            Col1 = new PdfPCell(new Phrase("", Font8));
            Col1.Border = 0;
            Table6.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.Cabecera[0].EmailVendedor.ToString(), Font8));
            Col2.Border = 0;
            Table6.AddCell(Col2);

            Table6.AddCell(CVacio);
            Col1 = new PdfPCell(new Phrase("", Font8));
            Col1.Border = 0;
            Table6.AddCell(Col1);
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.Cabecera[0].Telefono, Font8));
            Col2.Border = 0;
            Table6.AddCell(Col2);

            Table6.AddCell(CVacio);
            Col1 = new PdfPCell(new Phrase("", Font8));
            Col1.Border = 0;
            Table6.AddCell(Col1);
           
            Col2 = new PdfPCell(new Phrase(reporteCotizacion.DatosEmpresa[0].direccionEmpresa, Font8));
            Col2.Border = 0;
            Table6.AddCell(Col2);
            pdfDoc.Add(Table6);
            #endregion

            pdfDoc.Add(new Paragraph("  "));
            pdfDoc.Add(new Paragraph("  "));

            PdfPTable Table7 = new PdfPTable(1);
            float[] widths7 = new float[] {
                20.0F
            };
            Table7.WidthPercentage = 95;
            Table7.SetWidths(widths7);
            string pie = "";
            pie += "El plazo de entrega es informado por nuestro proveedor y es un plazo estimado. Ni EMELTEC ni el proveedor tienen el control ni responsabilidad por las demoras en los";
            pie += " despachos, procesos de desaduanamiento y logística en general.En consecuencia, EMELTEC no será responsable de atrasos.Se deja constancia que, el plazo de entrega,";
            pie += "no ha sido determinante ni esencial para la compra de los productos. Es compromiso y obligación de EMELTEC efectuar la entrega o despacho de los productos al cliente";
            pie += " tan pronto como sea posible, una vez que se encuentren en su bodega.";

            Col1 = new PdfPCell(new Phrase(pie, Font7));
            Table7.AddCell(Col1);
            pdfDoc.Add(Table7);

            //---------- SEGUNDA HOJA
            pdfDoc.Add(new Paragraph("  "));
            pdfDoc.Add(new Paragraph("  "));
            pdfDoc.Add(new Paragraph("  "));
            pdfDoc.Add(new Paragraph("  "));
            pdfDoc.Add(new Paragraph("  "));
            pdfDoc.Add(new Paragraph("  "));

            PdfPTable Tablex = new PdfPTable(1);
            float[] widthsx = new float[] {
                20.0F
            };
            Tablex.WidthPercentage = 95;
            Tablex.SetWidths(widthsx);
            
            Col1 = new PdfPCell(new Phrase("CONDICIONES COMERCIALES Y CONTRACTUALES", FontB6));
            Col1.Border = 0;
            Col1.HorizontalAlignment =1;
            Tablex.AddCell(Col1);
            pdfDoc.Add(Tablex);

           

            for (int x = 0; x < consideraciones.Count; x++)
            {

                PdfPTable Table8 = new PdfPTable(1);
                float[] widths8 = new float[] {
                20.0F
            };
                Table8.WidthPercentage = 95;
                Table8.SetWidths(widths8);

                Col1 = new PdfPCell(new Phrase(consideraciones[x].Titulo, FontB6));
                Col1.Border = 0;
                Table8.AddCell(Col1);
                

                Col1 = new PdfPCell();

                Col1 = new PdfPCell(new Phrase(consideraciones[x].Consideracion, Font6));
                Col1.Border = 0;
                Table8.AddCell(Col1);
                pdfDoc.Add(Table8);
            }
            
            pdfDoc.Close();

            respuesta.Verificador = true;
            respuesta.Mensaje = "Reporte generado";
            respuesta.RutaArchivo = rutaCompletaArchivo;
            return respuesta;
        }

        private void PintaLinea(double numGrosor, int X1, int Y1, int X2, int Y2)
        {
            PdfContentByte linea;
            linea = pdfWriter.DirectContent;
            linea.SetLineWidth(numGrosor);
            linea.MoveTo(X1, Y1);
            linea.LineTo(X2, Y2);
            linea.Stroke();
        }
        private void PintaLineaVertical(double numGrosor, int X1, int Y1, int X2, int Y2)
        {
            PdfContentByte linea;
            linea = pdfWriter.DirectContent;
            linea.SetLineWidth(numGrosor);
            linea.MoveTo(X1, X2);
            linea.LineTo(Y1, Y2);
            linea.Stroke();
        }
        private void PintaCuadro(double numGrosor, int X1, int Y1, int X2, int Y2)
        {
            PdfContentByte linea;
            linea = pdfWriter.DirectContent;
            linea.SetLineWidth(numGrosor);
            linea.MoveTo(X1, Y1);
            linea.LineTo(X2, Y1);
            linea.Stroke();

            linea.MoveTo(X2, Y1);
            linea.LineTo(X2, Y2);
            linea.Stroke();

            linea.MoveTo(X2, Y2);
            linea.LineTo(X1, Y2);
            linea.Stroke();

            linea.MoveTo(X1, Y2);
            linea.LineTo(X1, Y1);
            linea.Stroke();
            pdfDoc.Add(Chunk.NEWLINE);
        }
        #endregion
    }
}
