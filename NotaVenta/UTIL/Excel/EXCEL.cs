﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using UTIL;
using OfficeOpenXml;
using System.IO;
using System.Drawing;
using OfficeOpenXml.Style;
using OfficeOpenXml.Table;

namespace TexasHub.UTIL.Excel
{
    public class EXCEL : FuncionesEXCEL
    {
        List<string> estilosAll = new List<string>();
        ControlDisofi controlDisofi = null;

        public EXCEL(ControlDisofi controlDisofi) : base()
        {
            this.controlDisofi = controlDisofi;
        }

        public string generarExcelReporteVentas(string ruta,string baseDatos)
        {
            DataTable tabla = controlDisofi.obtenerExcelVentas(baseDatos);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));

            string rutaCompletaArchivo = generarExcel(ruta, "#FFDA68", tabla, "Reporte Ventas");

            return rutaCompletaArchivo;
        }
        // Reporte Saldo
        public string generarExcelReporteSaldo(string ruta, string bd, string Anio, string Fecha)
        {
            DataTable tabla = controlDisofi.obtenerExcelSaldo(bd, Anio, Fecha);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));

            string rutaCompletaArchivo = generarExcel(ruta, "#FFDA68", tabla, "Reporte Saldos");

            return rutaCompletaArchivo;
        }




        public string generarExcelReporteComisiones(string baseDatos, string ruta, DateTime fechaDesde, DateTime fechaHasta, string vendedor, int tipoUsuario)
        {
            DataTable tabla = controlDisofi.ObtenerReporteComisionesExcel(baseDatos, vendedor, fechaDesde, fechaHasta, tipoUsuario);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));

            //Se envia un 3(vendedores) para separa los totales del reporte consolidado
            string rutaCompletaArchivo = generarExcelComisiones(ruta, "#8B0000", tabla, "Reporte Comisiones", 3);

            return rutaCompletaArchivo;
        }

        private string generarExcelComisiones(string ruta, string colorHoja, DataTable datos, string nombreHoja, int Totalizado)
        {
            string rutaCompletaArchivo = "";
            rutaCompletaArchivo = ruta + "excel_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff") + ".xlsx";
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage pck = new ExcelPackage();

            TablaHojaModel tablaHoja1 = new TablaHojaModel();
            tablaHoja1.ConColumna = true;
            tablaHoja1.X = 0;
            tablaHoja1.Y = 0;
            tablaHoja1.Tabla = datos;

            pck = crearHojaCo(pck, new List<TablaHojaModel>() { tablaHoja1 }, nombreHoja, colorHoja, Totalizado);

            if (pck.Workbook.Worksheets.Count == 0)
            {
                TablaHojaModel tablaHojaDefault = new TablaHojaModel();
                tablaHojaDefault.ConColumna = false;
                tablaHojaDefault.X = 0;
                tablaHojaDefault.Y = 0;
                tablaHojaDefault.Tabla = new DataTable();

                pck = crearHojaCo(pck, new List<TablaHojaModel>() { tablaHojaDefault }, "SIN DATOS", null, Totalizado);
            }

            byte[] dataByte = pck.GetAsByteArray();
            File.WriteAllBytes(rutaCompletaArchivo, dataByte);

            return rutaCompletaArchivo;
        }





        private string generarExcel(string ruta, string colorHoja, DataTable datos, string nombreHoja)
        {
            string rutaCompletaArchivo = "";
            rutaCompletaArchivo = ruta + "excel_" + DateTime.Now.ToString("yyyyMMdd_HHmmss.fff") + ".xlsx";
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage pck = new ExcelPackage();

            TablaHojaModel tablaHoja1 = new TablaHojaModel();
            tablaHoja1.ConColumna = true;
            tablaHoja1.X = 0;
            tablaHoja1.Y = 0;
            tablaHoja1.Tabla = datos;

            pck = crearHoja(pck, new List<TablaHojaModel>() { tablaHoja1 }, nombreHoja, colorHoja);

            if (pck.Workbook.Worksheets.Count == 0)
            {
                TablaHojaModel tablaHojaDefault = new TablaHojaModel();
                tablaHojaDefault.ConColumna = false;
                tablaHojaDefault.X = 0;
                tablaHojaDefault.Y = 0;
                tablaHojaDefault.Tabla = new DataTable();

                pck = crearHoja(pck, new List<TablaHojaModel>() { tablaHojaDefault }, "SIN DATOS", null);
            }

            byte[] dataByte = pck.GetAsByteArray();
            File.WriteAllBytes(rutaCompletaArchivo, dataByte);

            return rutaCompletaArchivo;
        }


        private ExcelPackage crearHojaCo(ExcelPackage excelPackage, List<TablaHojaModel> datas, string nombreHoja, string colorHoja, int Totalizado)
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage excelPackageTemp = excelPackage;
            ExcelWorksheet hojaEstilo = excelPackageTemp.Workbook.Worksheets.Add(nombreHoja);

            hojaEstilo = crearCuerpoCo(hojaEstilo, datas, Totalizado);

            hojaEstilo.Cells.AutoFitColumns();
            hojaEstilo.Cells[1, 1, 1, 41].AutoFilter = true;

            if (colorHoja != null && colorHoja != "")
            {
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml(colorHoja);
                hojaEstilo.TabColor = colFromHex;
            }

            return excelPackageTemp;
        }
        private ExcelPackage crearHoja(ExcelPackage excelPackage, List<TablaHojaModel> datas, string nombreHoja, string colorHoja)
        {
            ExcelPackage.LicenseContext = LicenseContext.Commercial;
            ExcelPackage excelPackageTemp = excelPackage;
            ExcelWorksheet hojaEstilo = excelPackageTemp.Workbook.Worksheets.Add(nombreHoja);

            hojaEstilo = crearCuerpo(hojaEstilo, datas);

            var cantidadCab = 0;

            if (datas.Count > 0)
            {
                for (int z = 0; z < datas.Count; z++)
                {
                    if (datas[z].Tabla != null)
                    {
                        for (int y = 0; y < datas[z].Tabla.Columns.Count; y++)
                        {
                            cantidadCab++;
                        }
                    }
                }
            }

            hojaEstilo.Cells[1, 1, 1, cantidadCab].AutoFilter = true;

            hojaEstilo.Cells.AutoFitColumns();
            if (colorHoja != null && colorHoja != "")
            {
                Color colFromHex = System.Drawing.ColorTranslator.FromHtml(colorHoja);
                hojaEstilo.TabColor = colFromHex;
            }
            return excelPackageTemp;
        }

        private ExcelWorksheet crearCuerpoCo(ExcelWorksheet hojaEstilo, List<TablaHojaModel> datas, int? totalLinea = 0)
        {
            try
            {
                for (int z = 0; z < datas.Count; z++)
                {
                    if (datas[z].ConColumna)
                    {
                        for (int y = 0; y < datas[z].Tabla.Columns.Count; y++)
                        {
                            string celda = obtenerPosicionExcel(datas[z].X, datas[z].Y + y).nombreCelda;
                            hojaEstilo.Cells[celda].Value = datas[z].Tabla.Columns[y];

                            hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                            hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                            hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                            hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                        }
                    }

                    //NumberFormatInfo nfi = new CultureInfo("is-IS", false).NumberFormat;
                    //nfi = (NumberFormatInfo)nfi.Clone();
                    //nfi.CurrencySymbol = "";
                    //nfi.CurrencyPositivePattern = 0;
                    //nfi.CurrencyDecimalSeparator = ".";
                    if ((totalLinea == 1) || (totalLinea == 3))
                    {
                        double montoC = 0;
                        double montoD = 0;
                        double montoE = 0;
                        double montoF = 0;
                        double montoG = 0;
                        double montoH = 0;
                        double montoI = 0;
                        double montoJ = 0;
                        double montoK = 0;
                        double montoM = 0;
                        double montoO = 0;
                        double montoP = 0;
                        for (int x = 0; x < datas[z].Tabla.Rows.Count; x++)
                        {
                            for (int y = 0; y < datas[z].Tabla.Columns.Count; y++)
                            {
                                string celda = obtenerPosicionExcel((datas[z].ConColumna ? ((datas[z].X + 1) + x) : ((datas[z].X) + x)), datas[z].Y + y).nombreCelda;

                                hojaEstilo.Cells[celda].Value = datas[z].Tabla.Rows[x][y];

                                hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#add8e6"));


                                int nroFila = (datas[z].Tabla.Rows.Count) + 4;
                                string celdafinal = celda.Substring(0, 1) + nroFila.ToString();

                                if (celda.StartsWith("A"))
                                {
                                    hojaEstilo.Cells[celdafinal].Value = "Total general";
                                }
                                if (y >= 2)
                                {

                                    if (celda.StartsWith("C") && (totalLinea == 1))
                                    {
                                        montoC += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoC;
                                    }
                                    if (celda.StartsWith("D") && (totalLinea == 1))
                                    {
                                        montoD += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoD;
                                    }
                                    if (celda.StartsWith("E") && (totalLinea == 1))
                                    {
                                        montoE += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoE;
                                    }
                                    if (celda.StartsWith("F") && (totalLinea == 1))
                                    {
                                        montoF += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoF;
                                    }
                                    if (celda.StartsWith("G") && (totalLinea == 1))
                                    {
                                        montoG += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoG;
                                    }
                                    if (celda.StartsWith("H") && (totalLinea == 1))
                                    {
                                        montoH += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoH;
                                    }
                                    if (celda.StartsWith("I"))
                                    {
                                        try
                                        {
                                            montoI += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                            hojaEstilo.Cells[celdafinal].Value = montoI;
                                        }
                                        catch
                                        {
                                            montoI += 0;
                                            hojaEstilo.Cells[celdafinal].Value = montoI;
                                        }
                                    }
                                    if (celda.StartsWith("J") && (totalLinea == 1))
                                    {
                                        montoJ += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoJ;
                                    }

                                    if (celda.StartsWith("K") && (totalLinea == 3))
                                    {
                                        montoK += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoK;
                                    }
                                    if (celda.StartsWith("M") && (totalLinea == 3))
                                    {
                                        montoM += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoM;
                                    }
                                    if (celda.StartsWith("O") && (totalLinea == 3))
                                    {
                                        montoO += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoO;
                                    }
                                    if (celda.StartsWith("P") && (totalLinea == 3))
                                    {
                                        montoP += Convert.ToDouble(datas[z].Tabla.Rows[x][y]);
                                        hojaEstilo.Cells[celdafinal].Value = montoP;
                                    }

                                }

                                if (celda.StartsWith("L"))
                                {
                                    double comision = double.Parse(datas[z].Tabla.Rows[x][y].ToString());
                                    if ((comision > 3) || (comision == 0))
                                    {
                                        hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ffffe0"));
                                    }
                                    else
                                    {
                                        hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#add8e6"));
                                    }
                                }
                                else
                                {
                                    hojaEstilo.Cells[celdafinal].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celdafinal].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celdafinal].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celdafinal].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celdafinal].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    hojaEstilo.Cells[celdafinal].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#add8e6"));
                                }



                            }

                        }

                    }

                    else
                    {
                        for (int x = 0; x < datas[z].Tabla.Rows.Count; x++)
                        {
                            for (int y = 0; y < datas[z].Tabla.Columns.Count; y++)
                            {
                                string celda = obtenerPosicionExcel((datas[z].ConColumna ? ((datas[z].X + 1) + x) : ((datas[z].X) + x)), datas[z].Y + y).nombreCelda;

                                hojaEstilo.Cells[celda].Value = datas[z].Tabla.Rows[x][y];


                                if (celda.StartsWith("L"))
                                {
                                    int comision = int.Parse(datas[z].Tabla.Rows[x][y].ToString());
                                    if ((comision > 3) || (comision == 0))
                                    {
                                        hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ffffe0"));
                                    }
                                    else
                                    {
                                        hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                        hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                        hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#add8e6"));
                                    }
                                }
                                else
                                {
                                    hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                                    hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#add8e6"));
                                }

                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }

            return hojaEstilo;
        }




        private ExcelWorksheet crearCuerpo(ExcelWorksheet hojaEstilo, List<TablaHojaModel> datas)
        {
            int contador = 0;
            for (int z = 0; z < datas.Count; z++)
            {
                if (datas[z].ConColumna)
                {
                    for (int y = 0; y < datas[z].Tabla.Columns.Count; y++)
                    {
                        string celda = obtenerPosicionExcel(datas[z].X, datas[z].Y + y).nombreCelda;
                        hojaEstilo.Cells[celda].Value = datas[z].Tabla.Columns[y];

                        hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Medium;
                        hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Medium;
                        hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Medium;
                        hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Medium;
                        hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#ffff00"));
                        hojaEstilo.Cells[celda].Style.Font.Bold = true;
                    }
                }

                //NumberFormatInfo nfi = new CultureInfo("is-IS", false).NumberFormat;
                //nfi = (NumberFormatInfo)nfi.Clone();
                //nfi.CurrencySymbol = "";
                //nfi.CurrencyPositivePattern = 0;
                //nfi.CurrencyDecimalSeparator = ".";


                for (int x = 0; x < datas[z].Tabla.Rows.Count; x++)
                {
                    for (int y = 0; y < datas[z].Tabla.Columns.Count; y++)
                    {
                        string celda = obtenerPosicionExcel((datas[z].ConColumna ? ((datas[z].X + 1) + x) : ((datas[z].X) + x)), datas[z].Y + y).nombreCelda;

                        hojaEstilo.Cells[celda].Value = datas[z].Tabla.Rows[x][y];

                        hojaEstilo.Cells[celda].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        hojaEstilo.Cells[celda].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        hojaEstilo.Cells[celda].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        hojaEstilo.Cells[celda].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                        hojaEstilo.Cells[celda].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        hojaEstilo.Cells[celda].Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#add8e6"));
                        contador++;
                    }
                }
                //var range11 = hojaEstilo.Cells[1,30,(29),30];
                //ExcelTableCollection tblcollection = hojaEstilo.Tables;
                //ExcelTable table1 = tblcollection.Add(range11, "Reporte");
                //table1.TableStyle = OfficeOpenXml.Table.TableStyles.Medium23;
            }
            return hojaEstilo;
        }

        public string generarExcelReporteVentasDash(string baseDatos, string ruta, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            DataTable tabla = controlDisofi.obtenerExcelVentasDash(baseDatos, fechaDesde, fechaHasta, venCod);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));

            string rutaCompletaArchivo = generarExcel(ruta, "#8B0000", tabla, "Reporte Ventas");

            return rutaCompletaArchivo;
        }

        public string generarExcelReporteNV(string baseDatos, string ruta, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            DataTable tabla = controlDisofi.obtenerExcelNV(baseDatos, fechaDesde, fechaHasta, venCod);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));
            string rutaCompletaArchivo = generarExcel(ruta, "#8B0000", tabla, "Reporte Notas de Venta");

            return rutaCompletaArchivo;
        }

        public string generarExcelReporteVentasDia(string ruta,string baseDatos, string venCod)
        {
            DataTable tabla = controlDisofi.obtenerExcelVentasDia(baseDatos, venCod);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));

            string rutaCompletaArchivo = generarExcel(ruta, "#8B0000", tabla, "Reporte Ventas");

            return rutaCompletaArchivo;
        }

        public string generarExcelReporteNVBacklog(string baseDatos, string ruta, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            DataTable tabla = controlDisofi.obtenerExcelNVBacklog(baseDatos, fechaDesde, fechaHasta, venCod);

            for (int i = tabla.Rows.Count - 1; i >= 0; i--)
            {

                DataRow dr = tabla.Rows[i];
                if (dr["ValorNV"].ToString() == "0")
                {
                    tabla.Rows.Remove(dr);
                }

            }
            tabla.AcceptChanges();

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));
            string rutaCompletaArchivo = generarExcel(ruta, "#8B0000", tabla, "Reporte Notas de Venta");

            return rutaCompletaArchivo;
        }


        public string ObtenerReporteConsolidadoExcel(string baseDatos, string ruta, DateTime fechaDesde, DateTime fechaHasta)
        {
            DataTable tabla = controlDisofi.ObtenerReporteConsolidadoExcel(baseDatos, fechaDesde, fechaHasta);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));

            string rutaCompletaArchivo = generarExcelComisiones(ruta, "#8B0000", tabla, "Reporte Comisiones", 1);

            return rutaCompletaArchivo;
        }

        // saldos Clientes...
        public string generarExcelReporteSaldosClientes(string baseDatos, string ruta, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            DataTable tabla = controlDisofi.obtenerExcelSaldosClientes(baseDatos, fechaDesde, fechaHasta, venCod);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));
            string rutaCompletaArchivo = generarExcel(ruta, "#8B0000", tabla, "Reporte Saldos Clientes");

            return rutaCompletaArchivo;
        }
        // saldos Proveedore
        public string generarExcelReporteSaldosProveedores(string baseDatos, string ruta, DateTime fechaDesde, DateTime fechaHasta, string venCod)
        {
            DataTable tabla = controlDisofi.obtenerExcelSaldosProveedores(baseDatos, fechaDesde, fechaHasta, venCod);

            LogUser.agregarLog("tabla:" + (tabla == null ? "null" : (tabla.Rows.Count + "")));
            string rutaCompletaArchivo = generarExcel(ruta, "#8B0000", tabla, "Reporte Saldos Proveedores");

            return rutaCompletaArchivo;
        }



    }
}