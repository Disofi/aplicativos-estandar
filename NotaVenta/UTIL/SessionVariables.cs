﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using UTIL.Objetos;
using System.Web.Mvc;
using UTIL.Models;

namespace NotaVenta.UTIL
{
    public class SessionVariables
    {
        public static ObjetoUsuario SESSION_DATOS_USUARIO
        {
            get { return (ObjetoUsuario)HttpContext.Current.Session["EWRWEFVREFGVR"]; }
            set { HttpContext.Current.Session["EWRWEFVREFGVR"] = value; }
        }

        public static string SESSION_RUTA_ARCHIVO
        {
            get { return (string)HttpContext.Current.Session["rutaTemporal"]; }
            set { HttpContext.Current.Session["rutaTemporal"] = value; }
        }

        public static List<UsuarioEmpresaModel> SESSION_EMPRESAS_USUARIO
        {
            get { return (List<UsuarioEmpresaModel>)HttpContext.Current.Session["CNDSKOAOIODSFAF"]; }
            set { HttpContext.Current.Session["CNDSKOAOIODSFAF"] = value; }
        }
        public static string SESSION_RUTA_ARCHIVO_COMISIONES_EXCEL
        {
            get { return (string)HttpContext.Current.Session["KIGVRFHIUGHRTGHVLKRFNBVGGVA"]; }
            set { HttpContext.Current.Session["KIGVRFHIUGHRTGHVLKRFNBVGGVA"] = value; }
        }
        public static string SESSION_RUTA_ARCHIVO_CONSOLIDADO
        {
            get { return (string)HttpContext.Current.Session["demo"]; }
            set { HttpContext.Current.Session["demo"] = value; }
        }
        public static string SESSION_RUTA_ARCHIVO_CONSOLIDADO_EXCEL
        {
            get { return (string)HttpContext.Current.Session["KIGVRFHIUGHRTGHVLKRFNBVGGVAXX"]; }
            set { HttpContext.Current.Session["KIGVRFHIUGHRTGHVLKRFNBVGGVAXX"] = value; }
        }

        public static List<ClientesModels> SESSION_BUSCAR_CLIENTE
        {
            get { return (List<ClientesModels>)HttpContext.Current.Session["FERTG54YHTR"]; }
            set { HttpContext.Current.Session["FERTG54YHTR"] = value; }
        }

        public static EmpresaModel SESSION_CLIENTE_BASE_DATOS
        {
            get { return (EmpresaModel)HttpContext.Current.Session["DFVNVUIRHGBOVTHFOGIBHDFOIVGBFD"]; }
            set { HttpContext.Current.Session["DFVNVUIRHGBOVTHFOGIBHDFOIVGBFD"] = value; }
        }

        public static NotadeVentaCabeceraModels SESSION_NOTA_VENTA_CABECERA_MODEL
        {
            get { return (NotadeVentaCabeceraModels)HttpContext.Current.Session["4RTG54Y65EY"]; }
            set { HttpContext.Current.Session["4RTG54Y65EY"] = value; }
        }
        public static string SESSION_RUTA_ARCHIVO_COMISIONES
        {
            get { return (string)HttpContext.Current.Session["KIGVRFHIUGHRTGHVLKRFNBVGGVI"]; }
            set { HttpContext.Current.Session["KIGVRFHIUGHRTGHVLKRFNBVGGVI"] = value; }
        }
        public static string SESSION_RUTA_ARCHIVO_COMISIONES_FINAL
        {
            get { return (string)HttpContext.Current.Session["TGRKJTGHRDUIOGHRFNVORFGNVOIDF"]; }
            set { HttpContext.Current.Session["TGRKJTGHRDUIOGHRFNVORFGNVOIDF"] = value; }
        }
        public static RespuestaNotaVentaModel SESSION_RESPUESTA_NOTA_VENTA
        {
            get { return (RespuestaNotaVentaModel)HttpContext.Current.Session["RespuestaNV"]; }
            set { HttpContext.Current.Session["RespuestaNV"] = value; }
        }
        public static List<string> SESSION_PATH_IMAGENES
        {
            get { return (List<string>)HttpContext.Current.Session["PathImagen"]; }
            set { HttpContext.Current.Session["PathImagen"] = value; }
        }

        public static string SESSION_RUTA_PDF_COTIZACION
        {
            get { return (string)HttpContext.Current.Session["TGRKJTGHRDUIOGHRFNVORFGN"]; }
            set { HttpContext.Current.Session["TGRKJTGHRDUIOGHRFNVORFGN"] = value; }
        }

        public static VendedoresSoftlandModels SESSION_VENDEDOR
        {
            get { return (VendedoresSoftlandModels)HttpContext.Current.Session["Vendedor"]; }
            set { HttpContext.Current.Session["Vendedor"] = value; }
        }
    }
}
