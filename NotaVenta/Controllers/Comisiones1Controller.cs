﻿using NotaVenta.UTIL;
using NotaVenta.UTIL.FilterAttributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TexasHub.UTIL.Excel;
using UTIL.Models;
using UTIL.Objetos;

namespace TexasHub.Controllers
{
    public class Comisiones1Controller : BaseController
    {
        // GET: Comisiones1
        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.COMISIONES, PERFILES.REPORTES)]
        public ActionResult ReporteComisiones()
        {
            return View();
        }

        public JsonResult ObtenerVendedoresEmpresa()
        {
            List<EmpresaModel> empresaModels = controlDisofi().ListarEmpresas().Where(m => m.IdEmpresa == Convert.ToInt32(EmpresaUsuario().IdEmpresa)).ToList();

            if (empresaModels != null && empresaModels.Count > 0)
            {
                List<UsuariosModels> usuarios = controlDisofi().ListarCodVendedorSoft(empresaModels[0].BaseDatos);

                return Json(usuarios, JsonRequestBehavior.AllowGet); ;
            }
            else
            {
                return Json(new List<UsuariosModels>(), JsonRequestBehavior.AllowGet); ;
            }
        }


        [HttpPost]
        public JsonResult GenerarReporteComisiones(List<UsuariosModels> vendedores, string fechaDesde, string fechaHasta)
        {
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                string idVendedores = "";
                for (int x = 0; x < vendedores.Count; x++)
                {
                    idVendedores = idVendedores + vendedores[x].VenCod + ";";
                }
                idVendedores = idVendedores.Substring(0, (idVendedores.Length - 1));

                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string rutas = "";
                List<RespuestaModel> responseList = new List<RespuestaModel>();
                int tipoUsuario = SessionVariables.SESSION_DATOS_USUARIO.TipoUsuario;
                tipoUsuario = tipoUsuario != 4 ? -1 : tipoUsuario;

                for (int x = 0; x < vendedores.Count; x++)
                {
                    ReporteComisionesModel reporteComisiones = controlDisofi().ObtenerReporteComisiones(baseDatosUsuario(), vendedores[x].VenCod, fechaDesdeDT, fechaHastaDT, tipoUsuario);

                    RespuestaModel response = new NotaVenta.UTIL.PDF.PDF().generaPdfComisiones(mapPath, new List<UsuariosModels>() { vendedores[x] }, reporteComisiones, tipoUsuario);
                    response.Mensaje = vendedores[x].VenCod + "|" + response.Mensaje;
                    responseList.Add(response);
                    rutas = rutas + (vendedores[x].VenCod + "|") + (response.RutaArchivo + ";");
                }
                SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES = rutas;
                GenerarReporteComisionesExcel(vendedores, fechaDesde, fechaHasta, tipoUsuario);
                return Json(responseList.Select(m => new { VenCod = (m.Mensaje.Split('|')[0]), Verificador = m.Verificador, Mensaje = (m.Mensaje.Split('|')[1]) }));
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);

                var line = frame.GetFileLineNumber();
                var file = frame.GetFileName();

                return Json(new { Verificador = false, Mensaje = "Se produjo un error al generar el reporte, favor reintentelo, si el problema persiste, comuniquese con su administrador" });
            }
        }


        public JsonResult GenerarReporteComisionesExcel(List<UsuariosModels> vendedores, string fechaDesde, string fechaHasta, int tipoUsuario)
        {
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                string idVendedores = "";
                for (int x = 0; x < vendedores.Count; x++)
                {
                    idVendedores = idVendedores + vendedores[x].VenCod + ";";
                }
                idVendedores = idVendedores.Substring(0, (idVendedores.Length - 1));


                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string path = "";
                List<RespuestaModel> responseList = new List<RespuestaModel>();
                for (int x = 0; x < vendedores.Count; x++)
                {
                    var rutaaux = new EXCEL(controlDisofi()).generarExcelReporteComisiones(baseDatosUsuario(), mapPath, fechaDesdeDT, fechaHastaDT, vendedores[x].VenCod.ToString(), tipoUsuario);
                    path = path + (vendedores[x].VenCod + "|") + (rutaaux + ";");

                }
               
               
                
                SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES_EXCEL = path;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                return Json(new { Verificador = false });
            }
        }

        public JsonResult PreDescargarArchivoComisionesexcel(string VenCod)
        {
            SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES_FINAL = SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES_EXCEL.Split(';').Where(m => (m.Split('|')[0]) == VenCod).First().Split('|')[1];
            return Json(new { Verificador = true });
        }

        public JsonResult PreDescargarArchivoComisiones(string VenCod)
        {
            SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES_FINAL = SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES.Split(';').Where(m => (m.Split('|')[0]) == VenCod).First().Split('|')[1];
            return Json(new { Verificador = true });
        }

        public ActionResult DescargarArchivoComisiones(string VenCod)
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES_FINAL;
            return DescargarArchivo(path, "Reporte Comisiones");
        }

        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.COMISIONES, PERFILES.REPORTES)]
        public ActionResult ConsolidadoComisiones()
        {
            return View();
        }

        public ActionResult DescargarArchivoConsolidado()
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO_CONSOLIDADO;
            return DescargarArchivo(path, "Reporte Consolidado");
        }
        public ActionResult DescargarArchivoConsolidadoExcel()
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO_CONSOLIDADO_EXCEL;
            return DescargarArchivo(path, "Reporte Consolidado");
        }

        public JsonResult GeneraReporteComisionesConsolidado(string fechaDesde, string fechaHasta)
        {
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");
                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string rutas = "";
                List<RespuestaModel> responseList = new List<RespuestaModel>();

                ReporteComisionConsolidadoModel reporteConsolidado = controlDisofi().ObtenerReporteConsolidado(baseDatosUsuario(), fechaDesdeDT, fechaHastaDT);

                RespuestaModel response = new NotaVenta.UTIL.PDF.PDF().generaPdfComisionesConsolidado(mapPath, reporteConsolidado);
                rutas = response.RutaArchivo;
                SessionVariables.SESSION_RUTA_ARCHIVO_CONSOLIDADO = rutas;
                GenerarConsolidadoExcel(fechaDesde, fechaHasta);
                return Json(response.Mensaje);
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);
                var lines = frame.GetFileLineNumber();
                var file = frame.GetFileName();
                return Json(new { Verificador = false, Mensaje = "Se produjo un error al generar el reporte, favor reintentelo, si el problema persiste, comuniquese con su administrador" });
            }

        }
        public JsonResult GenerarConsolidadoExcel(string fechaDesde, string fechaHasta)
        {
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string path = "";
                List<RespuestaModel> responseList = new List<RespuestaModel>();

                var rutaaux = new EXCEL(controlDisofi()).ObtenerReporteConsolidadoExcel(baseDatosUsuario(), mapPath, fechaDesdeDT, fechaHastaDT);

                SessionVariables.SESSION_RUTA_ARCHIVO_CONSOLIDADO_EXCEL = rutaaux;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                return Json(new { Verificador = false });
            }
        }




    }
}