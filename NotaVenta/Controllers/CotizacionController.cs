﻿using NotaVenta.UTIL;
using NotaVenta.UTIL.FilterAttributes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UTIL;
using UTIL.Models;
using UTIL.Objetos;

namespace TexasHub.Controllers
{
    public class CotizacionController : BaseController
    {
        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.VENDEDOR)]
        public ActionResult MisClientes()
        {
            UsuariosModels usr = new UsuariosModels();
            ParametrosModels parametros = ObtieneParametros();
            List<ClientesModels> lclientes = new List<ClientesModels>();

            usr.VenCod = codigoVendedorUsuario().Trim();
            usr.id = SessionVariables.SESSION_DATOS_USUARIO.IdUsuario;
            var misClientes = controlDisofi().BuscarMisClientesVenCod(usr, baseDatosUsuario());

            if (misClientes != null)
            {
                lclientes = misClientes;
            }
            ViewBag.parametros = parametros;
            ViewBag.clientes = lclientes;

            IEnumerable<SelectListItem> clientesGiro = controlDisofi().ObtenerGiro(baseDatosUsuario()).Select(c => new SelectListItem()
            {
                Text = c.GirDes,
                Value = c.GirCod
            }).ToList();
            ViewBag.Giro = clientesGiro;

            IEnumerable<SelectListItem> clientesCiudad = controlDisofi().ObtenerCiudad(baseDatosUsuario()).Select(c => new SelectListItem()
            {
                Text = c.CiuDes,
                Value = c.CiuCod
            }).ToList();
            ViewBag.Ciudad = clientesCiudad;

            IEnumerable<SelectListItem> clientesComuna = controlDisofi().ObtenerComuna(baseDatosUsuario()).Select(c => new SelectListItem()
            {
                Text = c.ComDes,
                Value = c.ComCod
            }).ToList();
            ViewBag.Comuna = clientesComuna;

            return View();
        }

        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.VENDEDOR)]
        public JsonResult PreNotadeVenta(string CodAux, string NomAux, string RutAux)
        {
            string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

            //List<string> strFilesTemp = Directory.GetFiles(mapPath, "*", SearchOption.AllDirectories).ToList();
            //foreach (string fichero in strFilesTemp)
            //{
            //    System.IO.File.Delete(fichero);
            //}

            ParametrosModels parametros = ObtieneParametros();
            NotadeVentaCabeceraModels notadeVentaCabeceraModels = new NotadeVentaCabeceraModels();
            notadeVentaCabeceraModels.CodAux = CodAux;
            notadeVentaCabeceraModels.NomAux = NomAux;
            notadeVentaCabeceraModels.RutAux = RutAux;
            SessionVariables.SESSION_NOTA_VENTA_CABECERA_MODEL = notadeVentaCabeceraModels;


            NotadeVentaCabeceraModels NVC = SessionVariables.SESSION_NOTA_VENTA_CABECERA_MODEL;
            NVC.NVNumero = 0;
            NVC.NumOC = "";
            NVC.NumReq = 0;
            var validador = 0;

            ClientesModels cliente = new ClientesModels
            {
                CodAux = NVC.CodAux
            };
            List<ClientesModels> contactoCorreos = controlDisofi().GetContactoCotizacion(baseDatosUsuario(), cliente);
            List<ClientesModels> clientes = controlDisofi().GetClientes(baseDatosUsuario(), cliente);

            CreditoModel credito = controlDisofi().ObtenerCredito(cliente.CodAux, baseDatosUsuario());

            if (parametros.ManejaLineaCreditoVendedor)
            {
                if (credito != null)
                {
                    credito.Deuda = credito.Debe - credito.Haber;
                    credito.Saldo = (credito.Credito + credito.Deuda);
                    credito.Credito = 100000;
                    if (credito.Credito == 0)
                    {
                        validador = -999;
                        return Json(validador);
                    }
                    else
                    {
                        validador = 1;
                        return Json(validador);
                    }

                }
            }

            if (contactoCorreos != null && contactoCorreos.Count > 0 && contactoCorreos[0].EMail != "")
            {
                ViewBag.CorreoCliente = contactoCorreos[0].EMail;
                validador = 1;
                return Json(validador);
            }
            else
            {
                if (clientes != null && clientes.Count > 0)
                {
                    if (parametros.EnvioMailCliente && Convert.ToString(clientes[0].EMail).Trim() == "")
                    {
                        validador = -1;
                        return Json(validador);
                    }
                    else
                    {
                        ViewBag.CorreoCliente = clientes[0].EMail;
                        validador = 1;
                        return Json(validador);
                    }
                }
                else
                {
                    validador = -1;
                    return Json(validador);
                }
            }
        }

        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.VENDEDOR)]
        public ActionResult Cotizacion()
        {
            ParametrosModels parametros = ObtieneParametros();

            ViewBag.Parametros = parametros;

            var id_ = SessionVariables.SESSION_DATOS_USUARIO.IdUsuario.ToString();
            var VenCod = codigoVendedorUsuario();
            var id = id_;

            NotadeVentaCabeceraModels NVC = SessionVariables.SESSION_NOTA_VENTA_CABECERA_MODEL;
            NVC.NVNumero = 0;
            NVC.NumOC = "";
            NVC.NumReq = 0;

            ClientesModels cliente = new ClientesModels
            {
                CodAux = NVC.CodAux,
                RutAux = NVC.RutAux,
            };

            ClientesModels cm = controlDisofi().ObtenerAtributoDescuento(baseDatosUsuario(), cliente.CodAux, parametros.AtributoSoftlandDescuentoCliente);
            cliente.ValorAtributo = cm.ValorAtributo;

            ViewBag.ValorAtributo = cliente.ValorAtributo;
            VendedoresSoftlandModels vendedorCliente = controlDisofi().ObtenerVendedorCliente(cliente.CodAux, baseDatosUsuario());
            

            SessionVariables.SESSION_VENDEDOR = controlDisofi().ObtenerNombreVendedor(SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.VenCod,baseDatosUsuario());
            LogUser.agregarLog("sesionVendedor "+SessionVariables.SESSION_VENDEDOR.VenDes);
            ViewBag.VendedorCliente = SessionVariables.SESSION_VENDEDOR;
            

            CreditoModel credito = controlDisofi().ObtenerCredito(cliente.CodAux, baseDatosUsuario());

            ViewBag.Credito = credito;
            ViewBag.CodAux = NVC.CodAux;
            ViewBag.RutAux = NVC.RutAux;

            double MontoTotal = 0;
            double Saldo = 0;
            try
            {
                List<SaldosModel> Saldos = new List<SaldosModel>();
                Saldos = controlDisofi().ObtenerSaldo(ViewBag.RutAux, ViewBag.CodAux, baseDatosUsuario());

                foreach (SaldosModel item in Saldos)
                {
                    MontoTotal = item.Saldo;
                    Saldo = MontoTotal + Saldo;
                }
            }
            catch (Exception)
            {
                throw;
            }

            ViewBag.SaldoTotal = Saldo;

            List<ClientesModels> contactoCorreos = controlDisofi().GetContacto(baseDatosUsuario(), cliente);
            List<ClientesModels> clientes = controlDisofi().GetClientes(baseDatosUsuario(), cliente);

            if (contactoCorreos != null && contactoCorreos.Count > 0 && contactoCorreos[0].EMail != "")
            {
                ViewBag.CorreoCliente = contactoCorreos[0].EMail;
            }
            else
            {
                if (clientes != null && clientes.Count > 0 && clientes[0].EMail != "")
                {
                    ViewBag.CorreoCliente = clientes[0].EMail;
                }
            }

            ViewBag.numeronota = NVC;

            CondicionVentasModels conven = new CondicionVentasModels();

            //Se lista(n) la(s) condicion(es) de venta(s)

            List<CondicionVentasModels> lcondicion = new List<CondicionVentasModels>();
            List<CondicionVentasModels> lCondicionCliente = new List<CondicionVentasModels>();
            List<CondicionVentasModels> lCondicionTodo = new List<CondicionVentasModels>();

            conven.CodAux = NVC.CodAux.ToString();
            lCondicionCliente = controlDisofi().listarConVen(baseDatosUsuario(), conven);
            

            if (parametros.MuestraCondicionVentaCliente) 
            {
                conven.CodAux = NVC.CodAux.ToString();
                lcondicion = controlDisofi().listarConVen(baseDatosUsuario(), conven);
                if (lcondicion.Count == 0)
                {
                    lcondicion = controlDisofi().listarConVenPret(baseDatosUsuario(), conven);
                }
            }
            else 
            {
                conven.CodAux = "-1";
                lcondicion = controlDisofi().listarConVen(baseDatosUsuario(), conven);
                conven.CodAux = "-2";
                lCondicionTodo = controlDisofi().listarConVen(baseDatosUsuario(), conven);
            }

            ViewBag.condicion = lcondicion;

            ViewBag.condicionCliente = lCondicionTodo.Find(v => v.CodAux == NVC.CodAux.ToString());

            if(ViewBag.condicionCliente == null)
            {
                var objCondicion = new CondicionVentasModels()
                {
                    CodAux = NVC.CodAux.ToString(),
                    ConVta = "SC",
                    CveDes = "Sin Condicion de Venta"
                };
                ViewBag.condicionCliente = objCondicion;
            }
            

            ClientesModels contacto = new ClientesModels();

            contacto.CodAux = NVC.CodAux.ToString();
            contacto.NomAux = SessionVariables.SESSION_DATOS_USUARIO.Nombre.ToString();

            //Se ubica la lista de contactos
            List<ClientesModels> contactos = controlDisofi().BuscarContacto(baseDatosUsuario(), contacto);

            ViewBag.contactos = contactos;
            ViewBag.vcontactos = contactos == null || contactos.Count == 0 ? 0 : 1;

            List<ProjectManagerModels> projManag = controlDisofi().obtieneCorreosProManag();
            ViewBag.proManag = projManag;

            DireccionDespachoModels direc = new DireccionDespachoModels();

            direc.CodAxD = NVC.CodAux.ToString();

            //Se lista(n) la(s) dirección(es) de despacho
            List<DireccionDespachoModels> direciones = controlDisofi().BuscarDireccionDespachoCotizacion(direc, baseDatosUsuario());

            ViewBag.vdirecc = direciones == null ? 0 : 1;

            ViewBag.direccion = direciones;
            ViewBag.codigo = NVC.CodAux;
            ViewBag.nombre = NVC.NomAux;

            //Lista de precio
            ListaDePrecioModels ListPrecio = new ListaDePrecioModels();

            ListPrecio.CodAux = NVC.CodAux.ToString();

            List<ListaDePrecioModels> ListDePrecios = controlDisofi().listarListaDePrecio(baseDatosUsuario(), ListPrecio);

            ViewBag.lista = parametros.ManejaListaPrecios ? ListDePrecios : ViewBag.lista = new List<ListaDePrecioModels>();

            //Se listan los centros de costos
            List<CentrodeCostoModels> lcc = controlDisofi().ListarCentroCosto(baseDatosUsuario());
            ViewBag.cc = lcc;

            List<CanalVentaModels> dataCanalVenta = controlDisofi().ListarCanalVenta(baseDatosUsuario());
            ViewBag.canalVentas = dataCanalVenta;

            IEnumerable<SelectListItem> clientesCiudad = controlDisofi().ObtenerCiudad(baseDatosUsuario()).Select(c => new SelectListItem()
            {
                Text = c.CiuDes,
                Value = c.CiuCod
            }).ToList();
            ViewBag.Ciudad = clientesCiudad;

            IEnumerable<SelectListItem> clientesComuna = controlDisofi().ObtenerComuna(baseDatosUsuario()).Select(c => new SelectListItem()
            {
                Text = c.ComDes,
                Value = c.ComCod
            }).ToList();
            ViewBag.Comuna = clientesComuna;

            ViewBag.Monedas = controlDisofi().ListarMonedas(baseDatosUsuario());
            ViewBag.Grupos = controlDisofi().ListarGrupos(baseDatosUsuario());
            ViewBag.SubGrupos = controlDisofi().ListarSubGrupos(baseDatosUsuario());

            return View();
        }


        public JsonResult AgregarCliente(string NomAux, string RutAux, string FonAux1, string Email, string GirAux, string DirAux, string EmailDte,
         string Comuna, string Ciudad)
        {
            try
            {
                ParametrosModels parametros = ObtieneParametros();
                ClientesModels Cliente = new ClientesModels();
                if (parametros.CrearClienteConDV)
                {
                    Cliente.CodAux = RutAux.Replace("-", "").Replace(".", "");
                }
                else
                {
                    Cliente.CodAux = (RutAux.Replace("-", "").Replace(".", ""));
                    Cliente.CodAux = Cliente.CodAux.Substring(0, Cliente.CodAux.Length - 1);
                }
                Cliente.NomAux = NomAux;
                Cliente.RutAux = RutAux;
                Cliente.FonAux1 = FonAux1;
                Cliente.EMail = Email;
                Cliente.GirCod = GirAux;
                Cliente.DirAux = DirAux;
                Cliente.EmailDte = EmailDte;
                Cliente.ComCod = Comuna;
                Cliente.CiuCod = Ciudad;
                Cliente.VenCod = SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.VenCod;

                LogUser.agregarLog("nomaux "+NomAux);
                LogUser.agregarLog("RutAux "+RutAux);
                LogUser.agregarLog("FonAux1 "+FonAux1);
                LogUser.agregarLog("Email " +Email);
                LogUser.agregarLog("GirAux "+GirAux);
                LogUser.agregarLog("EmailDte "+EmailDte);
                LogUser.agregarLog("Comuna "+Comuna);
                LogUser.agregarLog("Ciudad " +Ciudad);
                LogUser.agregarLog("session " +SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.VenCod);

                RespuestaModel resultExiste = controlDisofi().ExisteCliente(Cliente, baseDatosUsuario());
                if (!resultExiste.Verificador)
                {
                    RespuestaModel result = controlDisofi().AgregarClienteCotizacion(Cliente, baseDatosUsuario());
                    return Json(result);
                }
                else
                {
                    return Json(new RespuestaModel() { Verificador = false, Mensaje = resultExiste.Mensaje });
                }
            }
            catch(Exception e)
            {
                string error = e.Message.ToString();
                LogUser.agregarLog(error);
                return null;
            }
            
        }

        public JsonResult grabarContacto(string codAux, string nombre, string fono, string email)
        {
            try
            {
                RespuestaModel contacto = controlDisofi().grabarContacto(codAux, nombre, fono, email,baseDatosUsuario());
                return Json(contacto);
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        #region"--- Web Métodos ---"

        public JsonResult AgregarDireccionDespachoCotizacion(DireccionDespachoModels direccion)
        {
            try
            {
                RespuestaModel rm = controlDisofi().AgregarDireccionDespachoCotizacion(direccion, baseDatosUsuario());
                return Json(rm);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public JsonResult BuscarDireccionDespachoCotizacion(string CodAux)
        {
            try
            {
                DireccionDespachoModels direc = new DireccionDespachoModels();
                direc.CodAxD = CodAux;

                //Se lista(n) la(s) dirección(es) de despacho
                List<DireccionDespachoModels> direciones = controlDisofi().BuscarDireccionDespachoCotizacion(direc, baseDatosUsuario());

                return Json(direciones);
            }
            catch (Exception ex)
            {
                throw (ex);
            }

        }

        public JsonResult AgregarCotizacion(NotadeVentaCabeceraModels cabecera, List<ProductoAgregadoModel> productos, NotadeVentaExtrasModel NvExtras)
        {
            try
            {
                var datosCorreoManag = cabecera.CorreoManag != null ? cabecera.CorreoManag.Split(',') : null;

                string correoManag = cabecera.CorreoManag != null ? datosCorreoManag[0] : "";
                List<ProductoAgregadoModel> productosNuevos = productos.Where(m => m.EsProductoNuevo).ToList();

                RespuestaModel respuestaNuevoProducto = new RespuestaModel();
                respuestaNuevoProducto.Verificador = true;
                respuestaNuevoProducto.Mensaje = "";

                for (int x = 0; x < productosNuevos.Count; x++)
                {

                    RespuestaModel respuestaTemporal = controlDisofi().CotizacionInsertarProductoNuevoDisofi(
                        baseDatosUsuario(),
                        productosNuevos[x].Codigo,
                        productosNuevos[x].Codigo,
                        productosNuevos[x].Descripcion,
                        productosNuevos[x].CodigoGrupo,
                        productosNuevos[x].CodigoSubGrupo,
                        productosNuevos[x].PrecioUnitario,
                        productosNuevos[x].CodigoMoneda,
                        productosNuevos[x].codigoFabrica);

                    if (!respuestaTemporal.Verificador)
                    {
                        respuestaNuevoProducto.Verificador = false;
                        respuestaNuevoProducto.Mensaje = "No se pudo ingresar uno o mas de los productos nuevos que se ingresaron (Favor verifique los datos)";
                    }
                }

                if (respuestaNuevoProducto.Verificador)
                {

                    ParametrosModels para = ObtieneParametros();

                    bool insertaDisofi = true;
                    bool insertaSoftland = !para.EnvioObligatorioAprobador;
                    IList<NotadeVentaCabeceraModels> listaCabecerasNotasDeVenta = new List<NotadeVentaCabeceraModels>();
                    List<List<ProductoAgregadoModel>> listaProductosNotasDeVenta = new List<List<ProductoAgregadoModel>>();

                    int contador = 1;
                    List<ProductoAgregadoModel> productosTemp = new List<ProductoAgregadoModel>();

                    for (int x = 0; x < productos.Count; x++)
                    {
                        productosTemp.Add(productos[x]);

                        if (para.CantidadLineas == contador)
                        {
                            CabeceraModel cabeceraModel = CalcularProductosAgregadosFunction(productosTemp, cabecera.Descuentos, productosTemp[0].DescuentoAtributo);
                            NotadeVentaCabeceraModels notadeVentaCabeceraModels = cabecera;
                            notadeVentaCabeceraModels.IdEmpresaInterna = EmpresaUsuario().IdEmpresa;
                            notadeVentaCabeceraModels.nvSubTotal = cabeceraModel.SubTotal;
                            notadeVentaCabeceraModels.nvSubTotalConDescuento = Convert.ToDouble(cabeceraModel.SubTotalConDescuento);

                            notadeVentaCabeceraModels.nvPorcDesc01 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 1) ? cabeceraModel.descuentos[0].Porcentaje : 0);
                            notadeVentaCabeceraModels.nvDescto01 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 1) ? cabeceraModel.descuentos[0].Valor : 0);
                            notadeVentaCabeceraModels.nvPorcDesc02 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 2) ? cabeceraModel.descuentos[1].Porcentaje : 0);
                            notadeVentaCabeceraModels.nvDescto02 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 2) ? cabeceraModel.descuentos[1].Valor : 0);
                            notadeVentaCabeceraModels.nvPorcDesc03 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 3) ? cabeceraModel.descuentos[2].Porcentaje : 0);
                            notadeVentaCabeceraModels.nvDescto03 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 3) ? cabeceraModel.descuentos[2].Valor : 0);
                            notadeVentaCabeceraModels.nvPorcDesc04 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 4) ? cabeceraModel.descuentos[3].Porcentaje : 0);
                            notadeVentaCabeceraModels.nvDescto04 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 4) ? cabeceraModel.descuentos[3].Valor : 0);
                            notadeVentaCabeceraModels.nvPorcDesc05 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 5) ? cabeceraModel.descuentos[4].Porcentaje : 0);
                            notadeVentaCabeceraModels.nvDescto05 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 5) ? cabeceraModel.descuentos[4].Valor : 0);

                            notadeVentaCabeceraModels.Descuentos = cabeceraModel.descuentos;

                            notadeVentaCabeceraModels.nvTotalDesc = 0;
                            notadeVentaCabeceraModels.nvMonto = Convert.ToDouble(cabeceraModel.SubTotalConDescuento);
                            notadeVentaCabeceraModels.nvNetoAfecto = cabeceraModel.Total;
                            notadeVentaCabeceraModels.TotalBoleta = cabeceraModel.Total;

                            listaProductosNotasDeVenta.Add(productosTemp);
                            listaCabecerasNotasDeVenta.Add(AgregarCotizacion_CrearCabecera(notadeVentaCabeceraModels));

                            productosTemp = new List<ProductoAgregadoModel>();
                            contador = 1;
                        }
                        else
                        {
                            contador = contador + 1;
                        }
                    }

                    if (contador != 1)
                    {
                        CabeceraModel cabeceraModel = CalcularProductosAgregadosFunction(productosTemp, cabecera.Descuentos, productosTemp[0].DescuentoAtributo);
                        NotadeVentaCabeceraModels notadeVentaCabeceraModels = cabecera;
                        notadeVentaCabeceraModels.IdEmpresaInterna = EmpresaUsuario().IdEmpresa;
                        notadeVentaCabeceraModels.nvSubTotal = cabeceraModel.SubTotal;
                        notadeVentaCabeceraModels.nvSubTotalConDescuento = Convert.ToDouble(cabeceraModel.SubTotalConDescuento);

                        notadeVentaCabeceraModels.nvPorcDesc01 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 1) ? cabeceraModel.descuentos[0].Porcentaje : 0);
                        notadeVentaCabeceraModels.nvDescto01 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 1) ? cabeceraModel.descuentos[0].Valor : 0);
                        notadeVentaCabeceraModels.nvPorcDesc02 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 2) ? cabeceraModel.descuentos[1].Porcentaje : 0);
                        notadeVentaCabeceraModels.nvDescto02 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 2) ? cabeceraModel.descuentos[1].Valor : 0);
                        notadeVentaCabeceraModels.nvPorcDesc03 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 3) ? cabeceraModel.descuentos[2].Porcentaje : 0);
                        notadeVentaCabeceraModels.nvDescto03 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 3) ? cabeceraModel.descuentos[2].Valor : 0);
                        notadeVentaCabeceraModels.nvPorcDesc04 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 4) ? cabeceraModel.descuentos[3].Porcentaje : 0);
                        notadeVentaCabeceraModels.nvDescto04 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 4) ? cabeceraModel.descuentos[3].Valor : 0);
                        notadeVentaCabeceraModels.nvPorcDesc05 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 5) ? cabeceraModel.descuentos[4].Porcentaje : 0);
                        notadeVentaCabeceraModels.nvDescto05 = Convert.ToDouble((cabeceraModel.descuentos != null && cabeceraModel.descuentos.Count >= 5) ? cabeceraModel.descuentos[4].Valor : 0);

                        notadeVentaCabeceraModels.Descuentos = cabeceraModel.descuentos;

                        notadeVentaCabeceraModels.nvTotalDesc = 0;
                        notadeVentaCabeceraModels.nvMonto = Convert.ToDouble(cabeceraModel.SubTotalConDescuento);
                        notadeVentaCabeceraModels.nvNetoAfecto = cabeceraModel.Total;
                        notadeVentaCabeceraModels.TotalBoleta = cabeceraModel.Total;

                        listaProductosNotasDeVenta.Add(productosTemp);
                        listaCabecerasNotasDeVenta.Add(AgregarCotizacion_CrearCabecera(notadeVentaCabeceraModels));

                        productosTemp = new List<ProductoAgregadoModel>();
                    }


                    List<RespuestaNotaVentaModel> listaRespuestaNotaVentaModel = new List<RespuestaNotaVentaModel>();
                    RespuestaNotaVentaModel respuestaNotaVenta = new RespuestaNotaVentaModel();
                    for (int i = 0; i < listaProductosNotasDeVenta.Count; i++)
                    {
                        respuestaNotaVenta = creacionCabeceraDetalleCotizacion(
                            listaCabecerasNotasDeVenta[i],
                            listaProductosNotasDeVenta[i],
                            insertaDisofi,
                            insertaSoftland,
                            para,
                            datosCorreoManag,
                            NvExtras);

                        respuestaNotaVenta.EstadoNP = cabecera.EstadoNP;

                        listaRespuestaNotaVentaModel.Add(respuestaNotaVenta);
                    }

                    SessionVariables.SESSION_RESPUESTA_NOTA_VENTA = respuestaNotaVenta;
                    return Json(listaRespuestaNotaVentaModel);
                }
                else
                {
                    return Json(respuestaNuevoProducto);
                }
            }
            catch (Exception ex)
            {
                LogUser.agregarLog("error "+ex.ToString());
                throw ex;
            }

        }

        private NotadeVentaCabeceraModels AgregarCotizacion_CrearCabecera(NotadeVentaCabeceraModels cabeceraModels)
        {
            string[] separadas;

            NotadeVentaCabeceraModels retorno = new NotadeVentaCabeceraModels();
            List<DescuentoProductoAgregadoModel> DescuentosTemp = (from item in cabeceraModels.Descuentos
                                                                   select new DescuentoProductoAgregadoModel()
                                                                   {
                                                                       Valor = item.Valor,
                                                                       Porcentaje = item.Porcentaje
                                                                   }).ToList();

            retorno.Id = cabeceraModels.Id;
            retorno.IdEmpresaInterna = cabeceraModels.IdEmpresaInterna;
            retorno.EstadoNP = cabeceraModels.EstadoNP;
            retorno.Saldo = cabeceraModels.Saldo;
            retorno.NomAux = cabeceraModels.NomAux;
            retorno.RutAux = cabeceraModels.RutAux;
            retorno.CveDes = cabeceraModels.CveDes;
            retorno.DesLista = cabeceraModels.DesLista;
            retorno.DescCC = cabeceraModels.DescCC;
            retorno.stocklista = cabeceraModels.stocklista;
            retorno.Descuentos = DescuentosTemp;
            retorno.nvSubTotalConDescuento = cabeceraModels.nvSubTotalConDescuento;
            retorno.NVNumero = cabeceraModels.NVNumero;
            retorno.nvFem = cabeceraModels.nvFem;
            retorno.nvEstado = cabeceraModels.nvEstado;
            retorno.nvEstFact = cabeceraModels.nvEstFact;
            retorno.nvEstDesp = cabeceraModels.nvEstDesp;
            retorno.nvEstRese = cabeceraModels.nvEstRese;
            retorno.nvEstConc = cabeceraModels.nvEstConc;
            retorno.CotNum = cabeceraModels.CotNum;
            retorno.NumOC = cabeceraModels.NumOC;
            retorno.nvFeEnt = cabeceraModels.nvFeEnt;
            retorno.CodAux = cabeceraModels.CodAux;
            retorno.VenCod = cabeceraModels.VenCod;
            retorno.CodMon = cabeceraModels.CodMon;
            retorno.CodLista = cabeceraModels.CodLista;
            retorno.nvObser = cabeceraModels.nvObser;
            retorno.nvCanalNV = cabeceraModels.nvCanalNV;
            retorno.CveCod = cabeceraModels.CveCod;
            retorno.NomCon = cabeceraModels.NomCon;
            retorno.CodiCC = cabeceraModels.CodiCC;
            retorno.CodBode = cabeceraModels.CodBode;
            retorno.nvSubTotal = cabeceraModels.nvSubTotal;
            retorno.nvPorcDesc01 = cabeceraModels.nvPorcDesc01;
            retorno.nvDescto01 = cabeceraModels.nvDescto01;
            retorno.nvPorcDesc02 = cabeceraModels.nvPorcDesc02;
            retorno.nvDescto02 = cabeceraModels.nvDescto02;
            retorno.nvPorcDesc03 = cabeceraModels.nvPorcDesc03;
            retorno.nvDescto03 = cabeceraModels.nvDescto03;
            retorno.nvPorcDesc04 = cabeceraModels.nvPorcDesc04;
            retorno.nvDescto04 = cabeceraModels.nvDescto04;
            retorno.nvPorcDesc05 = cabeceraModels.nvPorcDesc05;
            retorno.nvDescto05 = cabeceraModels.nvDescto05;
            retorno.nvMonto = cabeceraModels.nvMonto;
            retorno.nvFeAprob = cabeceraModels.nvFeAprob;
            retorno.NumGuiaRes = cabeceraModels.NumGuiaRes;
            retorno.nvPorcFlete = cabeceraModels.nvPorcFlete;
            retorno.nvValflete = cabeceraModels.nvValflete;
            retorno.nvPorcEmb = cabeceraModels.nvPorcEmb;
            retorno.nvValEmb = cabeceraModels.nvValEmb;
            retorno.nvEquiv = cabeceraModels.nvEquiv;
            retorno.nvNetoExento = cabeceraModels.nvNetoExento;
            retorno.nvNetoAfecto = cabeceraModels.nvNetoAfecto;
            retorno.nvTotalDesc = cabeceraModels.nvTotalDesc;
            retorno.ConcAuto = cabeceraModels.ConcAuto;
            ////////
            separadas = cabeceraModels.CodLugarDesp.Split(',');
            retorno.direccionCorreo = cabeceraModels.CodLugarDesp;
            retorno.CodLugarDesp = separadas[0];
            //retorno.CodLugarDesp = cabeceraModels.CodLugarDesp;
            retorno.SolicitadoPor = cabeceraModels.SolicitadoPor;
            retorno.DespachadoPor = cabeceraModels.DespachadoPor;
            retorno.Patente = cabeceraModels.Patente;
            retorno.RetiradoPor = cabeceraModels.RetiradoPor;
            retorno.CheckeoPorAlarmaVtas = cabeceraModels.CheckeoPorAlarmaVtas;
            retorno.EnMantencion = cabeceraModels.EnMantencion;
            retorno.Usuario = cabeceraModels.Usuario;
            retorno.UsuarioGeneraDocto = cabeceraModels.UsuarioGeneraDocto;
            retorno.FechaHoraCreacion = cabeceraModels.FechaHoraCreacion;
            retorno.Sistema = cabeceraModels.Sistema;
            retorno.ConcManual = cabeceraModels.ConcManual;
            retorno.RutSolicitante = cabeceraModels.RutSolicitante;
            retorno.proceso = cabeceraModels.proceso;
            retorno.TotalBoleta = cabeceraModels.TotalBoleta;
            retorno.NumReq = cabeceraModels.NumReq;
            retorno.CodVenWeb = cabeceraModels.CodVenWeb;
            retorno.CodBodeWms = cabeceraModels.CodBodeWms;
            retorno.CodLugarDocto = cabeceraModels.CodLugarDocto;
            retorno.RutTransportista = cabeceraModels.RutTransportista;
            retorno.Cod_Distrib = cabeceraModels.Cod_Distrib;
            retorno.Nom_Distrib = cabeceraModels.Nom_Distrib;
            retorno.MarcaWG = cabeceraModels.MarcaWG;

            return retorno;


        }

        private RespuestaNotaVentaModel creacionCabeceraDetalleCotizacion(NotadeVentaCabeceraModels cabecera,
                                        List<ProductoAgregadoModel> productos, bool insertaDisofi,
                                        bool insertaSoftland, ParametrosModels para, string[] correoManag,
                                        NotadeVentaExtrasModel NvExtras)
        {
            cabecera.IdEmpresaInterna = cabecera.IdEmpresaInterna;
            cabecera.NVNumero = cabecera.NVNumero;
            cabecera.nvFem = cabecera.nvFem;
            cabecera.nvEstado = insertaSoftland ? "A" : "P";
            cabecera.nvEstFact = 0;
            cabecera.nvEstDesp = 0;
            cabecera.nvEstRese = 0;
            cabecera.nvEstConc = 0;
            cabecera.CotNum = 0;
            cabecera.NumOC = cabecera.NumOC;
            cabecera.nvFeEnt = cabecera.nvFeEnt;
            cabecera.CodAux = cabecera.CodAux;
            cabecera.VenCod = SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.VenCod;
            cabecera.CodMon = cabecera.CodMon; //"01"; //PESO CHILENO

            if (para.ManejaListaPrecios)
            {
                cabecera.CodLista = (cabecera.CodLista == null || cabecera.CodLista == "" || cabecera.CodLista == "-1") ? null : cabecera.CodLista;
            }
            else
            {
                cabecera.CodLista = null;
            }

            cabecera.nvObser = cabecera.nvObser;
            cabecera.nvCanalNV = cabecera.nvCanalNV == "-1" ? null : cabecera.nvCanalNV;
            cabecera.CveCod = cabecera.CveCod == "-1" ? null : cabecera.CveCod;
            if (cabecera.CveCod == null)
            {
                if (para.CodigoCondicionVentaPorDefecto != null && para.CodigoCondicionVentaPorDefecto != "")
                {
                    cabecera.CveCod = para.CodigoCondicionVentaPorDefecto;
                }
            }
            cabecera.NomCon = (cabecera.NomCon == null || cabecera.NomCon == "") ? "SIN CONTACTO" : cabecera.NomCon;
            cabecera.CodiCC = cabecera.CodiCC == "-1" ? null : cabecera.CodiCC;
            cabecera.nvSubTotal = para.DescuentoTotalDirectoSoftland ? cabecera.nvSubTotal : cabecera.nvSubTotalConDescuento;

            int totalDescuento = 0;
            if (cabecera.Descuentos != null && cabecera.Descuentos.Count > 0 && para.DescuentoTotalDirectoSoftland)
            {
                cabecera.nvPorcDesc01 = Convert.ToDouble(cabecera.Descuentos[0].Porcentaje);
                cabecera.nvDescto01 = Convert.ToDouble(cabecera.Descuentos[0].Valor);
                totalDescuento = totalDescuento + Convert.ToInt32(cabecera.nvDescto01);
            }
            else
            {
                cabecera.nvPorcDesc01 = 0;
                cabecera.nvDescto01 = 0;
            }
            if (cabecera.Descuentos != null && cabecera.Descuentos.Count > 1 && para.DescuentoTotalDirectoSoftland)
            {
                cabecera.nvPorcDesc02 = Convert.ToDouble(cabecera.Descuentos[1].Porcentaje);
                cabecera.nvDescto02 = Convert.ToDouble(cabecera.Descuentos[1].Valor);
                totalDescuento = totalDescuento + Convert.ToInt32(cabecera.nvDescto02);
            }
            else
            {
                cabecera.nvPorcDesc02 = 0;
                cabecera.nvDescto02 = 0;
            }
            if (cabecera.Descuentos != null && cabecera.Descuentos.Count > 2 && para.DescuentoTotalDirectoSoftland)
            {
                cabecera.nvPorcDesc03 = Convert.ToDouble(cabecera.Descuentos[2].Porcentaje);
                cabecera.nvDescto03 = Convert.ToDouble(cabecera.Descuentos[2].Valor);
                totalDescuento = totalDescuento + Convert.ToInt32(cabecera.nvDescto03);
            }
            else
            {
                cabecera.nvPorcDesc03 = 0;
                cabecera.nvDescto03 = 0;
            }
            if (cabecera.Descuentos != null && cabecera.Descuentos.Count > 3 && para.DescuentoTotalDirectoSoftland)
            {
                cabecera.nvPorcDesc04 = Convert.ToDouble(cabecera.Descuentos[3].Porcentaje);
                cabecera.nvDescto04 = Convert.ToDouble(cabecera.Descuentos[3].Valor);
                totalDescuento = totalDescuento + Convert.ToInt32(cabecera.nvDescto04);
            }
            else
            {
                cabecera.nvPorcDesc04 = 0;
                cabecera.nvDescto04 = 0;
            }
            if (cabecera.Descuentos != null && cabecera.Descuentos.Count > 4 && para.DescuentoTotalDirectoSoftland)
            {
                cabecera.nvPorcDesc05 = Convert.ToDouble(cabecera.Descuentos[4].Porcentaje);
                cabecera.nvDescto05 = Convert.ToDouble(cabecera.Descuentos[4].Valor);
                totalDescuento = totalDescuento + Convert.ToInt32(cabecera.nvDescto05);
            }
            else
            {
                cabecera.nvPorcDesc05 = 0;
                cabecera.nvDescto05 = 0;
            }

            cabecera.nvTotalDesc = totalDescuento;
            cabecera.nvMonto = para.DescuentoTotalDirectoSoftland ? cabecera.nvNetoAfecto : cabecera.TotalBoleta;
            cabecera.nvFeAprob = insertaSoftland ? (DateTime?)DateTime.Now : null;
            cabecera.NumGuiaRes = 0;
            cabecera.nvPorcFlete = 0;
            cabecera.nvValflete = 0;
            cabecera.nvPorcEmb = 0;
            cabecera.nvValEmb = 0;
            cabecera.nvEquiv = 1;
            cabecera.nvNetoExento = 0;
            cabecera.nvNetoAfecto = cabecera.nvSubTotalConDescuento;
            cabecera.ConcAuto = "N";
            cabecera.CodLugarDesp = cabecera.CodLugarDesp == "-1" ? null : cabecera.CodLugarDesp;
            cabecera.SolicitadoPor = null;
            cabecera.DespachadoPor = null;
            cabecera.Patente = null;
            cabecera.RetiradoPor = null;
            cabecera.CheckeoPorAlarmaVtas = "N";
            cabecera.EnMantencion = 0;
            cabecera.Usuario = SessionVariables.SESSION_DATOS_USUARIO.VenDes;
            cabecera.UsuarioGeneraDocto = SessionVariables.SESSION_DATOS_USUARIO.VenDes;
            cabecera.FechaHoraCreacion = DateTime.Now;
            cabecera.Sistema = "NW";
            cabecera.ConcManual = "N";
            cabecera.RutSolicitante = null;
            cabecera.proceso = "Cotizacion";
            cabecera.TotalBoleta = cabecera.TotalBoleta;
            cabecera.NumReq = 0;
            //cabecera.CodVenWeb = cabecera.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa;
            //cabecera.CodBodeWms = cabecera.aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa;
            cabecera.CodLugarDocto = null;
            cabecera.RutTransportista = null;
            cabecera.Cod_Distrib = null;
            cabecera.Nom_Distrib = null;
            cabecera.MarcaWG = null;
            cabecera.CorreoManag = "";

            /*
            if (cabecera.Descuentos != null)
            {
                for (int x = 0; x < cabecera.Descuentos.Count; x++)
                {
                    if (cabecera.Descuentos[x].Porcentaje > 15)
                    {
                        cabecera.ErrorAprobador = true;
                        cabecera.ErrorAprobadorMensaje = "se ingreso un porcentaje mayor al 15% de descuento";
                    }
                }
            }
            */
            if (productos != null)
            {
                for (int x = 0; x < productos.Count; x++)
                {
                    if (productos[x].Descuentos != null)
                    {
                        for (int y = 0; y < productos[x].Descuentos.Count; y++)
                        {
                            if (productos[x].Descuentos[y].Porcentaje > 15)
                            {
                                cabecera.ErrorAprobador = true;
                                cabecera.ErrorAprobadorMensaje = "se ingreso un porcentaje mayor al 15% de descuento";
                            }
                        }
                    }
                }
            }

            string[] vendedor = SessionVariables.SESSION_VENDEDOR.VenDes.Split(' ');
            string iniciales = "";
            if (vendedor.Count() > 1)
            {
                iniciales = vendedor[0].Substring(0, 1) + vendedor[1].Substring(0, 1);
                LogUser.agregarLog("Inicial1 "+vendedor[0].Substring(0, 1));
                LogUser.agregarLog("Inicial2 "+vendedor[1].Substring(0, 1));
            }
            else
            {
                iniciales = vendedor[0].Substring(0, 1);
                LogUser.agregarLog("Inicial1 "+vendedor[0].Substring(0, 1));
            }
            
            RespuestaNotaVentaModel respuestaNotaVenta = controlDisofi().AgregarCotizacion(baseDatosUsuario(), insertaDisofi, insertaSoftland, cabecera,iniciales);

            cabecera.Id = respuestaNotaVenta.IdNotaVenta;
            cabecera.NVNumero = respuestaNotaVenta.NVNumero;

            if ((insertaDisofi && respuestaNotaVenta.VerificadorDisofi) ||
                (!insertaDisofi && !respuestaNotaVenta.VerificadorDisofi))
            {
                if ((insertaSoftland && respuestaNotaVenta.VerificadorSoftland) ||
                    (!insertaSoftland && !respuestaNotaVenta.VerificadorSoftland))
                {
                    for (int x = 0; x < productos.Count; x++)
                    {
                        NotaDeVentaDetalleModels detalle = new NotaDeVentaDetalleModels();

                        detalle.IdNotaVenta = respuestaNotaVenta.IdNotaVenta;
                        detalle.DesProd = productos[x].Descripcion;
                        //detalle.Stock
                        //detalle.Iva
                        //detalle.EmailVend
                        //detalle.PassCorreo
                        detalle.NVNumero = respuestaNotaVenta.NVNumero;
                        detalle.nvLinea = (x + 1);
                        detalle.nvCorrela = 0;
                        detalle.nvFecCompr = productos[x].FechaEntrega;
                        detalle.CodProd = productos[x].Codigo;
                        detalle.nvCant = Convert.ToDouble(productos[x].Cantidad);
                        detalle.nvPrecio = para.DescuentoLineaDirectoSoftland ? Convert.ToDouble(productos[x].PrecioUnitario) : Convert.ToDouble(productos[x].PrecioUnitarioConDescuento);
                        detalle.nvEquiv = 1;
                        detalle.nvSubTotal = para.DescuentoLineaDirectoSoftland ? Convert.ToDouble(productos[x].SubTotal) : Convert.ToDouble(productos[x].SubTotalConDescuento);
                        totalDescuento = 0;
                        if (productos[x].Descuentos != null && productos[x].Descuentos.Count > 0 && para.DescuentoLineaDirectoSoftland)
                        {
                            detalle.nvDPorcDesc01 = Convert.ToDouble(productos[x].Descuentos[0].Porcentaje);
                            detalle.nvDDescto01 = Convert.ToDouble(productos[x].Descuentos[0].Valor);
                            totalDescuento = totalDescuento + Convert.ToInt32(detalle.nvDDescto01);
                        }
                        else
                        {
                            detalle.nvDPorcDesc01 = 0;
                            detalle.nvDDescto01 = 0;
                        }
                        if (productos[x].Descuentos != null && productos[x].Descuentos.Count > 1 && para.DescuentoLineaDirectoSoftland)
                        {
                            detalle.nvDPorcDesc02 = Convert.ToDouble(productos[x].Descuentos[1].Porcentaje);
                            detalle.nvDDescto02 = Convert.ToDouble(productos[x].Descuentos[1].Valor);
                            totalDescuento = totalDescuento + Convert.ToInt32(detalle.nvDDescto02);
                        }
                        else
                        {
                            detalle.nvDPorcDesc02 = 0;
                            detalle.nvDDescto02 = 0;
                        }
                        if (productos[x].Descuentos != null && productos[x].Descuentos.Count > 2 && para.DescuentoLineaDirectoSoftland)
                        {
                            detalle.nvDPorcDesc03 = Convert.ToDouble(productos[x].Descuentos[2].Porcentaje);
                            detalle.nvDDescto03 = Convert.ToDouble(productos[x].Descuentos[2].Valor);
                            totalDescuento = totalDescuento + Convert.ToInt32(detalle.nvDDescto03);
                        }
                        else
                        {
                            detalle.nvDPorcDesc03 = 0;
                            detalle.nvDDescto03 = 0;
                        }
                        if (productos[x].Descuentos != null && productos[x].Descuentos.Count > 3 && para.DescuentoLineaDirectoSoftland)
                        {
                            detalle.nvDPorcDesc04 = Convert.ToDouble(productos[x].Descuentos[3].Porcentaje);
                            detalle.nvDDescto04 = Convert.ToDouble(productos[x].Descuentos[3].Valor);
                            totalDescuento = totalDescuento + Convert.ToInt32(detalle.nvDDescto04);
                        }
                        else
                        {
                            detalle.nvDPorcDesc04 = 0;
                            detalle.nvDDescto04 = 0;
                        }
                        if (productos[x].Descuentos != null && productos[x].Descuentos.Count > 4 && para.DescuentoLineaDirectoSoftland)
                        {
                            detalle.nvDPorcDesc05 = Convert.ToDouble(productos[x].Descuentos[4].Porcentaje);
                            detalle.nvDDescto05 = Convert.ToDouble(productos[x].Descuentos[4].Valor);
                            totalDescuento = totalDescuento + Convert.ToInt32(detalle.nvDDescto05);
                        }
                        else
                        {
                            detalle.nvDPorcDesc05 = 0;
                            detalle.nvDDescto05 = 0;
                        }

                        detalle.nvTotDesc = totalDescuento;
                        detalle.nvTotLinea = Convert.ToDouble(productos[x].Total);
                        detalle.nvCantDesp = 0;
                        detalle.nvCantProd = 0;
                        detalle.nvCantFact = 0;
                        detalle.nvCantDevuelto = 0;
                        detalle.nvCantNC = 0;
                        detalle.nvCantBoleta = 0;
                        detalle.nvCantOC = 0;
                        detalle.DetProd = productos[x].Descripcion;
                        detalle.CheckeoMovporAlarmaVtas = "N";
                        detalle.KIT = null;
                        detalle.CodPromocion = null;
                        detalle.CodUMed = productos[x].UnidadMedida;
                        detalle.CantUVta = Convert.ToDouble(productos[x].Cantidad);
                        detalle.Partida = productos[x].Talla;
                        detalle.Pieza = productos[x].Color;
                        detalle.FechaVencto = null;
                        detalle.CantidadKit = 0;
                        detalle.MarcaWG = null;
                        detalle.PorcIncidenciaKit = 0;

                        RespuestaNotaVentaModel respuestaDetalleCotizacion = controlDisofi().AgregarDetalleCotizacion(baseDatosUsuario(), insertaDisofi, insertaSoftland, detalle);
                        if (respuestaDetalleCotizacion.VerificadorDisofi)
                        {
                            if (productos[x].ValorAdicional != null)
                            {
                                for (int z = 0; z < productos[x].ValorAdicional.Count; z++)
                                {
                                    if (productos[x].ValorAdicional[z] != null)
                                    {
                                        if (productos[x].ValorAdicional[z].ValorAdicional != null && productos[x].ValorAdicional[z].ValorAdicional > 0)
                                        {
                                            RespuestaModel respuestaValorAdicional = controlDisofi().AgregarDetalleNVValorAdicional(baseDatosUsuario(), respuestaDetalleCotizacion.IdDetalleNotaVenta, (int)productos[x].ValorAdicional[z].ValorAdicional);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            LogUser.agregarLog("fechaCierre " + NvExtras.FechaCierre);
            LogUser.agregarLog("nroSolicitud " + NvExtras.nroSolicitud);
            LogUser.agregarLog("NroFinalCotizacion " + respuestaNotaVenta.NroFinalCotizacion);
            
            RespuestaModel AgregarCotizacionExtras = controlDisofi().agregarCotizacionExtras(respuestaNotaVenta.IdNotaVenta, NvExtras.FechaCierre, NvExtras.nroSolicitud,respuestaNotaVenta.NroFinalCotizacion);
            //try
            //{
            //    if (insertaSoftland)
            //    {
            //        RespuestaNotaVentaModel respuesta = controlDisofi().AgregarImpuesto(baseDatosUsuario(), cabecera);
            //    }
            //}
            //catch
            //{

            //}

            //EnviarEmailNotaVenta(cabecera, para,correoManag);

            GenerePdfCotizacion(cabecera.Id);

            return respuestaNotaVenta;
        }

       

        private CabeceraModel CalcularProductosAgregadosFunction(List<ProductoAgregadoModel> productos, List<DescuentoProductoAgregadoModel> descuentos, decimal porcentajeAtributoDescuento)
        {
            if (descuentos == null) { descuentos = new List<DescuentoProductoAgregadoModel>(); }
            decimal subTotal = 0;
            decimal subTotalConDescuento = 0;

            for (int x = 0; x < productos.Count; x++)
            {
                ProductoAgregadoModel producto = CalcularFilaFunction(productos[x].PrecioUnitario, productos[x].ValorAdicional,
                    productos[x].Cantidad, productos[x].Descuentos, porcentajeAtributoDescuento);

                productos[x].PrecioUnitario = producto.PrecioUnitario;
                productos[x].PrecioUnitarioConDescuento = producto.PrecioUnitarioConDescuento;
                productos[x].Cantidad = producto.Cantidad;
                productos[x].Descuentos = producto.Descuentos;
                productos[x].SubTotal = producto.SubTotal;
                productos[x].SubTotalConDescuento = producto.SubTotalConDescuento;
                productos[x].ValorDescuentoAtributo = producto.ValorDescuentoAtributo;
                productos[x].Total = producto.Total;

                subTotal = subTotal + producto.Total;
                subTotalConDescuento = subTotal;
            }

            for (int X = 0; X < descuentos.Count; X++)
            {
                decimal valorDescuento = Convert.ToInt32(((subTotalConDescuento * descuentos[X].Porcentaje) / 100));
                descuentos[X].Valor = valorDescuento;
                subTotalConDescuento = subTotalConDescuento - Convert.ToInt32(valorDescuento);
            }

            decimal impuesto = 0;
            decimal total = 0;

            subTotalConDescuento = Convert.ToInt32(subTotalConDescuento);
            impuesto = Convert.ToInt32(((subTotalConDescuento * 19) / 100));
            total = subTotalConDescuento + impuesto;

            return new CabeceraModel
            {
                Productos = productos,
                descuentos = descuentos,
                SubTotal = Convert.ToInt32(subTotal),
                SubTotalConDescuento = subTotalConDescuento,
                Impuesto = Convert.ToInt32(impuesto),
                Total = Convert.ToInt32(total)
            };
        }

        public ProductoAgregadoModel CalcularFilaFunction(decimal precioUnitario, List<ValorAdicionalModel> valorAdicional, decimal cantidad, List<DescuentoProductoAgregadoModel> descuentos, decimal porcentajeAtributoDescuento)
        {
            if (descuentos == null) { descuentos = new List<DescuentoProductoAgregadoModel>(); }
            decimal _precioUnitario = 0;
            decimal _cantidad = 0;

            if (precioUnitario > 0)
            {
                if (valorAdicional != null)
                {
                    for (int x = 0; x < valorAdicional.Count; x++)
                    {
                        precioUnitario = precioUnitario + Convert.ToDecimal(valorAdicional[x].ValorAdicional != null ? valorAdicional[x].ValorAdicional : 0);
                    }
                }

                _precioUnitario = precioUnitario == 0 ? _precioUnitario : Convert.ToDecimal((decimal)precioUnitario / (precioUnitario * 100)) * (precioUnitario * 100);
            }

            if (cantidad > 0)
            {
                _cantidad = cantidad == 0 ? _cantidad : Convert.ToDecimal((decimal)cantidad / (cantidad * 100)) * (cantidad * 100);
            }

            List<DescuentoProductoAgregadoModel> _descuentos = new List<DescuentoProductoAgregadoModel>();

            for (int x = 0; x < descuentos.Count; x++)
            {
                if (descuentos[x].Porcentaje > 0)
                {
                    DescuentoProductoAgregadoModel temp = new DescuentoProductoAgregadoModel()
                    {
                        Porcentaje = Convert.ToDecimal((decimal)descuentos[x].Porcentaje / (descuentos[x].Porcentaje * 100)) * (descuentos[x].Porcentaje * 100)
                    };
                    _descuentos.Add(temp);
                }
            }

            decimal precioUnitarioConDescuento = _precioUnitario;

            for (int x = 0; x < _descuentos.Count; x++)
            {
                if (_descuentos[x].Porcentaje != 0)
                {
                    decimal valorDescuento = Convert.ToInt32((precioUnitarioConDescuento * (_descuentos[x].Porcentaje / 100)));
                    _descuentos[x].Valor = valorDescuento * _cantidad;
                    precioUnitarioConDescuento = precioUnitarioConDescuento - valorDescuento;
                }
            }
            decimal valorAtributoDescuento = 0;
            if (porcentajeAtributoDescuento > 0)
            {
                valorAtributoDescuento = (precioUnitarioConDescuento * (porcentajeAtributoDescuento / 100));
                precioUnitarioConDescuento = precioUnitarioConDescuento - valorAtributoDescuento;
            }

            decimal subTotal = 0;
            decimal subTotalConDescuento = 0;
            decimal total = 0;

            subTotal = precioUnitario * _cantidad;
            subTotalConDescuento = precioUnitarioConDescuento * _cantidad;

            total = subTotalConDescuento;

            return new ProductoAgregadoModel()
            {
                PrecioUnitario = _precioUnitario,
                PrecioUnitarioConDescuento = precioUnitarioConDescuento,
                Cantidad = _cantidad,
                Descuentos = _descuentos,
                SubTotal = subTotal,
                SubTotalConDescuento = subTotalConDescuento,
                ValorDescuentoAtributo = valorAtributoDescuento,
                Total = total
            };
        }


        public JsonResult SubirArchivos(/*string numOc*/)
        {
            List<string> listaRutaArchivos = new List<string>();
            try
            {
                int contador = 0;
                NotadeVentaCabeceraModels NVC = SessionVariables.SESSION_NOTA_VENTA_CABECERA_MODEL;
                string codAux = NVC.CodAux;
                var path = "";
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        var stream = fileContent.InputStream;
                        string fname = Path.GetFileName(file);
                        if (SessionVariables.SESSION_RESPUESTA_NOTA_VENTA != null)
                        {
                            string nvNum = Convert.ToString(SessionVariables.SESSION_RESPUESTA_NOTA_VENTA.IdNotaVenta);
                            path = Path.Combine(Server.MapPath("~/ArchivosCotizacion"), nvNum + "_" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-tt") + "-" + fname);
                        }
                        else
                        {
                            path = Path.Combine(Server.MapPath("~/ArchivosCotizacion"), codAux + "-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-tt") + "_" + fname);
                        }
                        listaRutaArchivos.Add(path);
                        contador++;
                        using (var fileStream = System.IO.File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json("Upload failed");
            }
            SessionVariables.SESSION_RESPUESTA_NOTA_VENTA = null;
            SessionVariables.SESSION_PATH_IMAGENES = listaRutaArchivos;
            return Json("File uploaded successfully");
        }

        #endregion
    }
}
