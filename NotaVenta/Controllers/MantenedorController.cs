﻿using NotaVenta.UTIL;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UTIL.Models;
using UTIL.Objetos;

namespace TexasHub.Controllers
{
    public class MantenedorController : BaseController
    {
        #region ValorMoneda
        // GET: Mantenedor
        public ActionResult ValorMoneda()
        {
            ViewBag.Monedas = controlDisofi().ListarMonedas(baseDatosUsuario());
            return View();
        }
        #endregion

        #region Consideraciones
        public ActionResult Consideraciones()
        {
            ViewBag.Consideraciones = controlDisofi().ListarConsideraciones();
            return View();
        }

        public JsonResult GrabarConsideracion(string titulo,string consideracion)
        {
            RespuestaModel respuesta = new RespuestaModel();
            respuesta = controlDisofi().GrabarConsideracion(titulo, consideracion);

            List<ConsideracionesModel> consideraciones = new List<ConsideracionesModel>();
            consideraciones = controlDisofi().ListarConsideraciones();
            return Json(new {respuesta, consideraciones = consideraciones });
        }

        public JsonResult ModificarConsideracion(int id,string titulo, string consideracion)
        {
            RespuestaModel respuesta = new RespuestaModel();
            respuesta =controlDisofi().ModificarConsideracion(id,titulo, consideracion);
            List<ConsideracionesModel> consideraciones = new List<ConsideracionesModel>();
            consideraciones = controlDisofi().ListarConsideraciones();
            return Json(new {respuesta, consideraciones = consideraciones });
        }

        public JsonResult EliminarConsideracion(int id)
        {

            RespuestaModel respuesta = new RespuestaModel();
            respuesta = controlDisofi().EliminarConsideracion(id);
            List<ConsideracionesModel> consideraciones = new List<ConsideracionesModel>();
            consideraciones = controlDisofi().ListarConsideraciones();

            return Json(new {respuesta, consideraciones = consideraciones });
        }

        #endregion

        #region CargarArchivo
        public ActionResult CargarArchivo()
        {
            return View();
        }
        public JsonResult UploadPRoductos()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase file = Request.Files;

                    if (file != null && file.Count > 0)
                    {
                        byte[] fileBytes = new byte[Request.ContentLength];
                        var data = Request.InputStream.Read(fileBytes, 0, Convert.ToInt32(Request.ContentLength));

                        ExcelPackage.LicenseContext = LicenseContext.Commercial;

                        //hacer for para recorrer archivo y guardar por linea
                        using (var package = new ExcelPackage(Request.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();

                            for (int i = 3; i <= workSheet.Dimension.End.Row; i++)
                            {
                                ProductosMatenedorModel producto = new ProductosMatenedorModel();
                                try
                                {
                                    producto.Codigo = workSheet.Cells[i, 1].Value.ToString();
                            //        VendedorCom.Nombre = workSheet.Cells[i, 2].Value.ToString();
                            //        VendedorCom.lp1 = double.Parse(workSheet.Cells[i, 3].Value.ToString());
                            //        VendedorCom.lp2 = double.Parse(workSheet.Cells[i, 4].Value.ToString());
                            //        VendedorCom.lp3 = double.Parse(workSheet.Cells[i, 5].Value.ToString());
                            //        VendedorCom.lp4 = double.Parse(workSheet.Cells[i, 6].Value.ToString());

                                   RespuestaModel response = controlDisofi().GrabarProducto(producto);
                            //        if (!response.Verificador)
                            //        {
                            //            return Json(new RespuestaModel { Verificador = false, Mensaje = "Problemas Con Insercion" });
                            //        }
                                }
                                catch (Exception e)
                                {
                            //        package.Dispose();
                                    string error = e.Message.ToString();
                            //        return Json(new RespuestaModel { Verificador = false, Mensaje = "Problemas Con el Archivo" });
                                }
                            }

                        }

                    }
                }
                catch (Exception e)
                {
                    string error = e.Message.ToString();
                    //UTIL.LogUser.agregarLog(error);
                    return Json(new { Verificador = false, Mensaje = "Debe Cargar un Archivo" });
                }
            }
            return Json(new RespuestaModel { Verificador = true, Mensaje = "Archivo Cargado" });
        }
        #endregion
    }
}