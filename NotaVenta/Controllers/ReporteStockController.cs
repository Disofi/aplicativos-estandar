﻿using NotaVenta.UTIL;
using NotaVenta.UTIL.FilterAttributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UTIL;
using UTIL.Models;
using UTIL.Objetos;

namespace TexasHub.Controllers
{
    public class ReporteStockController : BaseController
    {
        // GET: Comisiones
        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.COMISIONES, PERFILES.APROBADOR, PERFILES.VENDEDOR)]
        public ActionResult ReporteStock()
        {
            return View();
        }

        public JsonResult ObtenerVendedoresEmpresa()
        {
            List<EmpresaModel> empresaModels = controlDisofi().ListarEmpresas().Where(m => m.IdEmpresa == Convert.ToInt32(EmpresaUsuario().IdEmpresa)).ToList();

            if (empresaModels != null && empresaModels.Count > 0)
            {
                //List<UsuariosModels> usuarios = controlDisofi().ListarCodVendedorSoft(empresaModels[0].BaseDatos);

                List<ProductosModels> usuarios = controlDisofi().ListarCodProdSoft(empresaModels[0].BaseDatos); //productos
                usuarios = usuarios.FindAll(m => m.Stock > 0);

                return Json(usuarios, JsonRequestBehavior.AllowGet); ;
            }
            else
            {
                return Json(new List<UsuariosModels>(), JsonRequestBehavior.AllowGet); ;
            }
        }

        [HttpPost]
        public JsonResult GenerarReporteComisiones(List<StockModel> productos,int estado)
        {
            try
            {
                if(estado   == 0)
                {
                    string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");
                    
                    string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteVentas(mapPath,baseDatosUsuario());

                    SessionVariables.SESSION_RUTA_ARCHIVO = path;
                    return Json(new { Verificador = true });
                }
                else
                {
                    List<StockModel> productosStock = new List<StockModel>();
                    List<StockModel> prod = new List<StockModel>();
                    for (int x = 0; x < productos.Count; x++)
                    {
                        productosStock = controlDisofi().obtenerProductosStock(productos[x].CodProd,baseDatosUsuario());
                        foreach (var item in productosStock)
                        {
                            prod.Add(item);
                        }
                    }
                    return Json(prod);
                }
            }
            catch (Exception ex)
            {
                var st = new StackTrace(ex, true);
                var frame = st.GetFrame(0);

                var line = frame.GetFileLineNumber();
                var file = frame.GetFileName();
                LogUser.agregarLog(ex.Message.ToString());
                return Json(new { Verificador = false, Mensaje = "Se produjo un error al generar el reporte, favor reintentelo, si el problema persiste, comuniquese con su administrador" });
            }
        }

        public ActionResult DescargarArchivoStock()
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO;
            return DescargarArchivo(path, "Reporte Stock");
        }

        public JsonResult PreDescargarArchivoComisiones(string VenCod)
        {
            SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES_FINAL = SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES.Split(';').Where(m => (m.Split('|')[0]) == VenCod).First().Split('|')[1];
            return Json(new { Verificador = true });
        }
        public ActionResult DescargarArchivoComisiones(string VenCod)
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO_COMISIONES_FINAL;
            return DescargarArchivo(path, "Reporte Comisiones");
        }

    }
}