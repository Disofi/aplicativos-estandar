﻿using NotaVenta.UTIL;
using NotaVenta.UTIL.FilterAttributes;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;
using UTIL;
using UTIL.Models;
using UTIL.Objetos;

namespace TexasHub.Controllers
{
    public class ReporteController : BaseController
    {
        #region Nota de Venta

        #region Vistas

        public ActionResult Index()
        {
            return null;
        }

        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.ADMINISTRADOR, PERFILES.APROBADOR, PERFILES.VENDEDOR)]
        public ActionResult FacturasPendientes()
        {
            ParametrosModels parametros = ObtieneParametros();

            ViewBag.Parametros = parametros;

            List<NotadeVentaCabeceraModels> doc = new List<NotadeVentaCabeceraModels>();
            string codigoVendedor = obtenerUsuario().TipoUsuario == Convert.ToInt32(PERFILES.APROBADOR) ? "-1" : codigoVendedorUsuario();
            var docPendientes = controlDisofi().listarDocPendientes(baseDatosUsuario(), EmpresaUsuario().IdEmpresa, codigoVendedor);

            if (docPendientes != null)
            {
                doc = docPendientes;
            }

            foreach (var item in doc)
            {
                for (int i = 0; i < doc.Count; i++)
                {
                    List<SaldosModel> Saldos = new List<SaldosModel>();
                    Saldos = controlDisofi().ObtenerSaldo(doc[i].RutAux, doc[i].CodAux, baseDatosUsuario());
                    if (Saldos != null && Saldos.Count > 0)
                    {
                        doc[i].Saldo = 0;
                        foreach (SaldosModel itemSaldo in Saldos)
                        {
                            doc[i].Saldo = doc[i].Saldo + itemSaldo.Saldo;
                        }
                    }
                }
            }


            ViewBag.doc = doc;


            ViewBag.Monedas = controlDisofi().ListarMonedas(baseDatosUsuario());
            ViewBag.Grupos = controlDisofi().ListarGrupos(baseDatosUsuario());
            ViewBag.SubGrupos = controlDisofi().ListarSubGrupos(baseDatosUsuario());

            return View();
        }

        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.ADMINISTRADOR, PERFILES.APROBADOR, PERFILES.VENDEDOR)]
        public ActionResult FacturasRechazadas()
        {
            ParametrosModels parametros = ObtieneParametros();

            ViewBag.Parametros = parametros;
            List<NotadeVentaCabeceraModels> doc = new List<NotadeVentaCabeceraModels>();
            string codigoVendedor = obtenerUsuario().TipoUsuario == Convert.ToInt32(PERFILES.APROBADOR) ? "-1" : codigoVendedorUsuario();
            var docPendientes = controlDisofi().listarDocRechazadas(baseDatosUsuario(), EmpresaUsuario().IdEmpresa, codigoVendedor);

            if (docPendientes != null)
            {
                doc = docPendientes;
            }

            ViewBag.doc = doc;

            return View();
        }

        [HttpPost]
        public JsonResult FacturasPendientes(int _nvId)
        {
            NotadeVentaCabeceraModels NVC = new NotadeVentaCabeceraModels();
            List<NotadeVentaCabeceraModels> NVCL = new List<NotadeVentaCabeceraModels>();
            NotaDeVentaDetalleModels NVD = new NotaDeVentaDetalleModels();
            List<NotaDeVentaDetalleModels> NVDL = new List<NotaDeVentaDetalleModels>();

            NVC.Id = _nvId;
            NVD.Id = _nvId;

            var nvc = controlDisofi().BuscarNVC(NVC, baseDatosUsuario());

            if (nvc != null)
            {
                NVCL = nvc;
            }
            else
            {
                ViewBag.mensaje = 1;
                ViewBag.NVnum = _nvId;
                return Json(new { Mensaje = ViewBag.mensaje, nvNum = ViewBag.NVnum });
            }

            ViewBag.cabecera = NVCL;

            var nvd = controlDisofi().BuscarNVD(NVD, baseDatosUsuario());

            if (nvd != null)
            {
                NVDL = nvd;
            }

            ViewBag.detalle = NVDL;

            return Json(new { Cabecera = NVCL, Detalle = NVDL, Mensaje = ViewBag.mensaje, NVNumero = ViewBag.NVnum }, JsonRequestBehavior.AllowGet);
        }

        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.ADMINISTRADOR, PERFILES.APROBADOR, PERFILES.VENDEDOR)]
        public ActionResult FacturasAprobadas()
        {
            ParametrosModels parametros = ObtieneParametros();

            ViewBag.Parametros = parametros;

            List<NotadeVentaCabeceraModels> doc = new List<NotadeVentaCabeceraModels>();
            string codigoVendedor = obtenerUsuario().TipoUsuario == Convert.ToInt32(PERFILES.APROBADOR) ? "-1" : codigoVendedorUsuario();
            var docAprobados = controlDisofi().listarDocAprobados(baseDatosUsuario(), EmpresaUsuario().IdEmpresa, codigoVendedor);


            if (docAprobados != null)
            {
                doc = docAprobados;
            }

            ViewBag.doc = doc;

            return View();
        }

        [HttpPost]
        public JsonResult FacturasAprobadas(int _nvId)
        {
            NotadeVentaCabeceraModels NVC = new NotadeVentaCabeceraModels();
            List<NotadeVentaCabeceraModels> NVCL = new List<NotadeVentaCabeceraModels>();
            NotaDeVentaDetalleModels NVD = new NotaDeVentaDetalleModels();
            List<NotaDeVentaDetalleModels> NVDL = new List<NotaDeVentaDetalleModels>();

            NVC.Id = _nvId;
            NVD.Id = _nvId;

            var nvc = controlDisofi().BuscarNVC(NVC, baseDatosUsuario());

            if (nvc != null)
            {
                NVCL = nvc;
            }
            else
            {
                ViewBag.mensaje = 1;
                ViewBag.NVnum = _nvId;
                return Json(new { Mensaje = ViewBag.mensaje, nvNum = ViewBag.NVnum });
            }

            ViewBag.cabecera = NVCL;

            var nvd = controlDisofi().BuscarNVD(NVD, baseDatosUsuario());

            if (nvd != null)
            {
                NVDL = nvd;
            }

            ViewBag.detalle = NVDL;

            return Json(new { Cabecera = NVCL, Detalle = NVDL, Mensaje = ViewBag.mensaje, NVNumero = ViewBag.NVnum }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public JsonResult ModificarProductoNuevo(string CodProdAntiguo, int IdDetalle, string CodProd, string DesProd, string CodGrupo, string CodSubGr, decimal PrecioVenta, string CodMon)
        {
            RespuestaModel respuesta = controlDisofi().InsertarProductoNuevoDisofi(baseDatosUsuario(), CodProdAntiguo, CodProd, DesProd, CodGrupo, CodSubGr, PrecioVenta, CodMon);
            RespuestaModel respuesta2 = controlDisofi().ActualizaNotaVentaProductoNuevo(baseDatosUsuario(), IdDetalle, CodProd);

            return Json(respuesta);
        }

        [HttpPost]
        public JsonResult AprobarNotaVenta(int _nvId)
        {
            NotadeVentaCabeceraModels notaVenta = new NotadeVentaCabeceraModels();

            ParametrosModels para = ObtieneParametros();
            notaVenta.Id = _nvId;

            NotaDeVentaDetalleModels NVD = new NotaDeVentaDetalleModels();
            NVD.Id = _nvId;



            List<NotaDeVentaDetalleModels> nvd = controlDisofi().BuscarNVD(NVD, baseDatosUsuario());
            RespuestaModel respuesta = new RespuestaModel();
            respuesta.Verificador = true;
            respuesta.Mensaje = "";

            for (int x = 0; x < nvd.Count; x++)
            {
                if (nvd[x].EsProductoNuevo)
                {
                    if (respuesta.Verificador)
                    {
                        respuesta = controlDisofi().InsertarProductoNuevoSoftland(baseDatosUsuario(), nvd[x].CodProd);
                    }
                }
            }


            if (respuesta.Verificador)
            {
                NotadeVentaCabeceraModels cabecera = controlDisofi().GetCab(_nvId);

                RespuestaModel resp = controlDisofi().InsertarClientesSoftland(cabecera.CodAux.ToString(), baseDatosUsuario());
                List<NotadeVentaCabeceraModels> proceso = controlDisofi().actualizaEstado(notaVenta, baseDatosUsuario());

                DireccionDespachoModels direc = new DireccionDespachoModels();
                direc.CodAxD = cabecera.CodAux.ToString();

                //Se lista(n) la(s) dirección(es) de despacho
                string direccionDx = "Sin Direccion";
                LogUser.agregarLog(cabecera.CodLugarDesp);

                if(cabecera.CodLugarDesp != null && cabecera.CodLugarDesp != "Sin Direccion")
                {
                    List<DireccionDespachoModels> direccion = controlDisofi().BuscarDirecDespachMail(direc, baseDatosUsuario(), cabecera.CodLugarDesp);
                    direccionDx = direccion[0].NomDch + ", " + direccion[0].ComDes + ", " + direccion[0].CiuDes;
                }

                List<string> paraEmail = new List<string>();



                var IdAprobador = SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.IdEmpresa;

                RespuestaModel grabaAprobadoPor = controlDisofi().grabaAprobadoPor(proceso[0].NVNumero,_nvId, IdAprobador, proceso[0].IdEmpresaInterna,"A");

                List<VendedorModels> vendedores = controlDisofi().GetVendedores(baseDatosUsuario(), new VendedorModels { VenCod = cabecera.VenCod });

                foreach (var item in vendedores)
                {
                    LogUser.agregarLog("VENDEDORES---" + item.Email.ToString());
                }

                List<AprobadorModels> aprobador = controlDisofi().GetAprobador(IdAprobador);

                foreach (var item in aprobador)
                {
                    LogUser.agregarLog("APROBADORES---" + item.email.ToString());
                }

                List<ClientesModels> clientes = controlDisofi().GetClientes(baseDatosUsuario(), new ClientesModels { CodAux = cabecera.CodAux });

                foreach (var item in clientes)
                {
                    LogUser.agregarLog("CLIENTES---" + item.EMail.ToString());
                }

                if (para.EnvioMailVendedor)
                {
                    if (vendedores != null && vendedores.Count > 0)
                    {
                        if (vendedores[0].Email != null && vendedores[0].Email != "")
                        {
                            LogUser.agregarLog("Email vendedor: " + vendedores[0].Email);
                            paraEmail.Add(vendedores[0].Email);
                        }
                    }
                }
                if (para.EnvioMailAprobador)
                {
                    if (aprobador != null && aprobador.Count > 0)
                    {
                        for (int x = 0; x < aprobador.Count; x++)
                        {
                            if (aprobador[x].email != null && aprobador[x].email != "")
                            {
                                LogUser.agregarLog("Email aprobador: " + aprobador[x].email);
                                paraEmail.Add(aprobador[x].email);
                            }
                        }
                    }
                }

                string correoManag = controlDisofi().obtenerCorreoManager(_nvId);

                //paraEmail.Add("odc@emeltec.cl");

                if (correoManag != null && correoManag != "")
                {
                    LogUser.agregarLog("CorreoMang:" + correoManag);
                    paraEmail.Add(correoManag);
                }

                if (para.CorreosWebConfig)
                {
                    string[] emails = System.Configuration.ConfigurationManager.AppSettings["EmailEnvioAprobadorFinal"].Split(';');
                    foreach (string item in emails)
                    {
                        if (item.Trim() != "")
                        {
                            LogUser.agregarLog("Correo EmailEnvioAprobadorFinal :" + item);
                            paraEmail.Add(item);
                        }
                    }
                }

                int opcion = 1;

                EnviarEmail(cabecera.NVNumero, notaVenta.Id, paraEmail, direccionDx, opcion);

                string path = System.Web.Hosting.HostingEnvironment.MapPath("~/Archivos/");
                DirectoryInfo di = new DirectoryInfo(path);
                try
                {
                    foreach (var fi in di.GetFiles("*", SearchOption.AllDirectories))
                    {
                        if (fi.Name.Contains(Convert.ToString(_nvId + "_")))
                        {
                            string rutaArchivo = path + fi.Name;

                            string rutaArchivoSoftland = @"C:\Softland_ERP\DISOFI\ARCHIVOS_NP\" + Convert.ToString(proceso[0].NVNumero) + "_" + fi.Name;
                            string rutaGrabaSoftland = @"W:\DISOFI\ARCHIVOS_NP\" + Convert.ToString(proceso[0].NVNumero) + "_" + fi.Name;

                            RespuestaModel response = controlDisofi().guardaRutaArchivo(Convert.ToString(proceso[0].NVNumero), rutaGrabaSoftland);

                            using (FileStream fs = System.IO.File.Create(rutaArchivoSoftland))
                            {
                                byte[] info = System.IO.File.ReadAllBytes(rutaArchivo);
                                fs.Write(info, 0, info.Length);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    string error = e.Message.ToString();
                    LogUser.agregarLog(error);
                }

                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/Archivos/");

                List<string> strFilesTemp = Directory.GetFiles(mapPath, "*", SearchOption.AllDirectories).ToList();
                foreach (string fichero in strFilesTemp)
                {
                    if (fichero.Contains(Convert.ToString(_nvId) + "_"))
                    {
                        System.IO.File.Delete(fichero);
                    }
                }

                return Json(new { nvNum = proceso[0].NVNumero });
            }
            else
            {
                return Json(respuesta);
            }
        }

        [HttpPost]
        public JsonResult RechazarNotaVenta(int _nvId, int _nvNum)
        {
            NotadeVentaCabeceraModels notaVenta = new NotadeVentaCabeceraModels();

            ParametrosModels para = ObtieneParametros();
            notaVenta.Id = _nvId;

            NotadeVentaCabeceraModels notaventa = new NotadeVentaCabeceraModels();
            notaventa.Id = _nvId;
            List<NotadeVentaCabeceraModels> proceso = controlDisofi().RechazarNP(notaventa);

            //Se lista(n) la(s) dirección(es) de despacho
            NotadeVentaCabeceraModels cabecera = controlDisofi().GetCab(_nvId);
            DireccionDespachoModels direc = new DireccionDespachoModels();
            direc.CodAxD = cabecera.CodAux.ToString();

            List<DireccionDespachoModels> direccion = controlDisofi().BuscarDirecDespachMail(direc, baseDatosUsuario(), cabecera.CodLugarDesp);
            string direccionDx = direccion != null ? direccion[0].NomDch + ", " + direccion[0].ComDes + ", " + direccion[0].CiuDes : "Sin direccion";

            List<string> paraEmail = new List<string>();

            var IdAprobador = SessionVariables.SESSION_DATOS_USUARIO.UsuarioEmpresaModel.IdEmpresa;

            List<VendedorModels> vendedores = controlDisofi().GetVendedores(baseDatosUsuario(), new VendedorModels { VenCod = cabecera.VenCod });

            foreach (var item in vendedores)
            {
                LogUser.agregarLog("VENDEDORES---" + item.Email.ToString());
            }

            List<AprobadorModels> aprobador = controlDisofi().GetAprobador(IdAprobador);

            foreach (var item in aprobador)
            {
                LogUser.agregarLog("APROBADORES---" + item.email.ToString());
            }

            string correoManag = controlDisofi().obtenerCorreoManager(_nvId);

            if (para.EnvioMailVendedor)
            {
                if (vendedores != null && vendedores.Count > 0)
                {
                    if (vendedores[0].Email != null && vendedores[0].Email != "")
                    {
                        paraEmail.Add(vendedores[0].Email);
                    }
                }
            }

            if (para.EnvioMailAprobador)
            {
                if (aprobador != null && aprobador.Count > 0)
                {
                    for (int x = 0; x < aprobador.Count; x++)
                    {
                        if (aprobador[x].email != null && aprobador[x].email != "")
                        {
                            paraEmail.Add(aprobador[x].email);
                        }
                    }
                }
            }

            if (para.CorreosWebConfig)
            {
                string[] emails = System.Configuration.ConfigurationManager.AppSettings["EmailEnvioAprobador"].Split(';');
                foreach (string item in emails)
                {
                    if (item.Trim() != "")
                    {
                        paraEmail.Add(item);
                    }
                }
            }

            if (correoManag != null && correoManag != "")
            {
                paraEmail.Add(correoManag);
            }

            int opcion = -1;

            EnviarEmail(cabecera.NVNumero, notaVenta.Id, paraEmail, direccionDx, opcion);

            return Json(new { nvNum = _nvId });
        }

        [HttpPost]
        public JsonResult ObtenerProductoNuevoDisofi()
        {
            List<ProductosModels> proceso = controlDisofi().ObtenerProductoNuevoDisofi(baseDatosUsuario());

            return Json(proceso);
        }

        [NonAction]
        public void EnviarEmail(int nvnumero, int Id, List<string> para, string direccionDx, int opcion)
        {
            try
            {
                string nombreEmpresa = EmpresaUsuario().NombreEmpresa;
                string subject = opcion == 1 ? string.Format("Nota de Pedido (" + nombreEmpresa + "): " + Id + " Aprobada", Id) : string.Format("Nota de Pedido (" + nombreEmpresa + "): " + Id + " Rechazada", Id);
                string from = System.Configuration.ConfigurationManager.AppSettings.Get("Para");
                string displayName = System.Configuration.ConfigurationManager.AppSettings.Get("Remitente");
                string password = System.Configuration.ConfigurationManager.AppSettings.Get("ClaveCorreo");
                string host = System.Configuration.ConfigurationManager.AppSettings.Get("Host");
                int port = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("Port"));
                bool enableSs1 = Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("EnableSsl"));
                bool useDefaultCredentials = Boolean.Parse(System.Configuration.ConfigurationManager.AppSettings.Get("UseDefaultCredentials"));


                var fromEmail = new MailAddress(from, displayName);

                var smtp = new SmtpClient
                {
                    Host = host,
                    Port = port,
                    EnableSsl = enableSs1,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = useDefaultCredentials,
                    Credentials = new NetworkCredential(fromEmail.Address, password)
                };

                List<NotadeVentaCabeceraModels> NVentaCabeceras = controlDisofi().BuscarNVPorNumero(Id, baseDatosUsuario());

                List<NotaDeVentaDetalleModels> NVentaDetalles = controlDisofi().BuscarNVDETALLEPorNumero(Id, baseDatosUsuario());

                ClientesModels cliente = new ClientesModels
                {
                    CodAux = NVentaCabeceras[0].CodAux,
                };
                List<ClientesModels> clientes = controlDisofi().GetClientes(baseDatosUsuario(), cliente);



                MailMessage mail = new MailMessage
                {
                    IsBodyHtml = true
                };
                mail.AlternateViews.Add(GetEmbeddedImage(NVentaCabeceras, NVentaDetalles, clientes, nombreEmpresa, direccionDx, opcion));
                mail.From = new MailAddress(from);

                para = para.Distinct().ToList();

                foreach (string item in para)
                {
                    LogUser.agregarLog("Cuenta Email To "+ item);
                    mail.To.Add(item);
                }

                if (mail != null)
                {
                    mail.Subject = subject;
                    smtp.Send(mail);
                }
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                LogUser.agregarLog(error);
            }
        }

        private AlternateView GetEmbeddedImage(List<NotadeVentaCabeceraModels> NVentaCabeceras,
        List<NotaDeVentaDetalleModels> NVentaDetalles, List<ClientesModels> Clientes, string nombreEmpresa, string direccionDx, int opcion)
        {
            try
            {
                ParametrosModels parametro = ObtieneParametros();

                bool displayPrecioConDescuento = true;

                char[] blanco = { ' ' };

                int colspan = 7;

                if (parametro.DescuentoLineaDirectoSoftland)
                {
                    colspan = colspan + parametro.CantidadDescuentosProducto;
                }

                string titulo = opcion == 1 ? "APROBACIÓN DE NOTA DE PEDIDO" : "RECHAZO DE NOTA DE PEDIDO";

                string htmlBody = String.Format(
                "<html><body>" +
                //"<img src='~/Image/logo.png' />" +
                "<H1>" + titulo + "<small>" + nombreEmpresa + "</small></H1>" +
                @"<H4> Nº de Cotización Interno: " + NVentaCabeceras[0].Id + @" </H4>" +
                @"<H4> Nº de Softland: " + NVentaCabeceras[0].NVNumero + @" </H4>" +
                @"<H4> Fecha Pedido: " + (NVentaCabeceras[0].nvFem == null ? "" : ((DateTime)NVentaCabeceras[0].nvFem).ToShortDateString()) + @" </H4>" +
                @"<H4> Cliente: " + NVentaCabeceras[0].NomAux + @" </H4>" +
                @"<H4> Dirección: " + direccionDx + @" </H4>" +
                @"<H4> Fecha Entrega: " + (NVentaCabeceras[0].nvFeEnt == null ? "" : ((DateTime)NVentaCabeceras[0].nvFeEnt).ToShortDateString()) + @" </H4>" +
                @"<H4> Observaciones: " + NVentaCabeceras[0].nvObser + @" </H4>" +
                @"<H4> Vendedor: " + NVentaCabeceras[0].VenDes + @" </H4>");

                if (NVentaCabeceras[0].CveCod != null && NVentaCabeceras[0].CveCod != "")
                {
                    htmlBody = htmlBody + String.Format(@"<H4> Condicion de Venta: " + NVentaCabeceras[0].CveDes + @" </H4>");
                }

                htmlBody = htmlBody + String.Format(@"<table border = ""1"" >" +
                @"<tr>" +
                @"<td>ID</td>" +
                @"<th nowrap=""nowrap"">Codigo Producto</th>");

                if (parametro.ManejaTallaColor)
                {
                    htmlBody = htmlBody + String.Format(@"<th>Talla</th>" +
                        @"<th>Color</th>");
                }

                htmlBody = htmlBody + String.Format(@"<th>Descripcion</th>" +
                @"<th>Cantidad</th>" +
                @"<th>Precio</th>" +
                @"<th>Sub-Total</th>");

                if (parametro.DescuentoLineaDirectoSoftland)
                {
                    for (int x = 0; x < parametro.CantidadDescuentosProducto; x++)
                    {
                        htmlBody = htmlBody + @"<th>Descuento N°" + (x + 1) + "</th>";
                    }
                }

                if (displayPrecioConDescuento)
                {
                    htmlBody = htmlBody + String.Format(@"<th>Precio C/Descuento    </th>");
                }
                htmlBody = htmlBody + String.Format(@"<th>Iva    </th>" +
                @"<th>Total   </th>" +
                @"</tr>");

                ProductosModels producto = new ProductosModels();
                List<LinkedResource> resources = new List<LinkedResource>();
                double Iva = 0;
                double subtotal = 0;
                double ivaux = 0;
                foreach (NotaDeVentaDetalleModels nvd in NVentaDetalles)
                {
                    double precioConIVa = Math.Round(nvd.nvSubTotal * 1.19);
                    subtotal = subtotal + nvd.nvSubTotal;
                    Iva = (precioConIVa - nvd.nvSubTotal);
                    ivaux = ivaux + Iva;
                    producto.CodProd = nvd.CodProd;
                    htmlBody = htmlBody + @"<tr>" +
                               @"<td>" + nvd.nvLinea + @"</td>" +
                               @"<td>" + nvd.CodProd + @"</td>";

                    if (parametro.ManejaTallaColor)
                    {
                        htmlBody = htmlBody + @"<td>" + nvd.Partida + @"</td>" +
                                   @"<td>" + nvd.Pieza + @"</td>";
                    }
                    htmlBody = htmlBody +
                           @"<td>" + nvd.DesProd + @"</td>" +
                           @"<td style='text-align: right;'>" + nvd.nvCant + @"</td>" +
                           @"<td style='text-align: right;'>" + "$ " + nvd.nvPrecio.ToString("N0") + @"</td>" +
                           @"<td style='text-align: right;'>" + "$ " + nvd.nvSubTotal.ToString("N0") + @"</td>";



                    if (parametro.DescuentoLineaDirectoSoftland)
                    {
                        for (int x = 0; x < parametro.CantidadDescuentosProducto; x++)
                        {
                            switch (x)
                            {
                                case 0:
                                    { htmlBody = htmlBody + @"<td style='text-align: right;'>" + "$ " + nvd.nvDDescto01 + @"</td>"; }
                                    break;
                                case 1:
                                    { htmlBody = htmlBody + @"<td style='text-align: right;'>" + "$ " + nvd.nvDDescto02 + @"</td>"; }
                                    break;
                                case 2:
                                    { htmlBody = htmlBody + @"<td style='text-align: right;'>" + "$ " + nvd.nvDDescto03 + @"</td>"; }
                                    break;
                                case 3:
                                    { htmlBody = htmlBody + @"<td style='text-align: right;'>" + "$ " + nvd.nvDDescto04 + @"</td>"; }
                                    break;
                                case 4:
                                    { htmlBody = htmlBody + @"<td style='text-align: right;'>" + "$ " + nvd.nvDDescto05 + @"</td>"; }
                                    break;
                            }
                        }
                    }
                    int precioConDescuento = 0;

                    if (displayPrecioConDescuento)
                    {
                        precioConDescuento = Convert.ToInt32(nvd.nvPrecio);
                        precioConDescuento = precioConDescuento - Convert.ToInt32((precioConDescuento * ((nvd.nvDPorcDesc01 == null) ? 0 : nvd.nvDPorcDesc01)) / 100);
                        precioConDescuento = precioConDescuento - Convert.ToInt32((precioConDescuento * ((nvd.nvDPorcDesc02 == null) ? 0 : nvd.nvDPorcDesc02)) / 100);
                        precioConDescuento = precioConDescuento - Convert.ToInt32((precioConDescuento * ((nvd.nvDPorcDesc03 == null) ? 0 : nvd.nvDPorcDesc03)) / 100);
                        precioConDescuento = precioConDescuento - Convert.ToInt32((precioConDescuento * ((nvd.nvDPorcDesc04 == null) ? 0 : nvd.nvDPorcDesc04)) / 100);
                        precioConDescuento = precioConDescuento - Convert.ToInt32((precioConDescuento * ((nvd.nvDPorcDesc05 == null) ? 0 : nvd.nvDPorcDesc05)) / 100);
                        htmlBody = htmlBody + @"<td style='text-align: right;'>" + "$ " + precioConDescuento.ToString("N0") + @"</td>";
                    }

                    htmlBody = htmlBody + @"<td style='text-align: right;'>" + "$ " + Iva.ToString("N0") + @"</td>" +
                               @"<td style='text-align: right;'>" + "$ " + precioConIVa.ToString("N0") + @"</td>" +
                               @"</tr>";
                }
                if (displayPrecioConDescuento)
                {
                    colspan++;
                }
                if (parametro.ManejaTallaColor)
                {
                    colspan++;
                    colspan++;
                }

                htmlBody += @"<tr><th style='text-align: right;' colspan =" + colspan + @">Sub Total</th><td style='text-align: right;'>" + "$ " + subtotal.ToString("N0") + @"</td></tr>" +
                        @"<tr><th style='text-align: right;' colspan =" + colspan + @">Total Iva</th><td style='text-align: right;'>" + "$ " + ivaux.ToString("N0") + @"</td></tr>" +
                        @"<tr><th style='text-align: right;' colspan =" + colspan + @">Total</th><td style='text-align: right;'>" + "$ " + NVentaCabeceras[0].TotalBoleta.ToString("N0") + @"</td></tr>";
                htmlBody += @" </body></html>";

                AlternateView alternateView = AlternateView.CreateAlternateViewFromString(htmlBody, null, MediaTypeNames.Text.Html);
                foreach (LinkedResource r in resources)
                {
                    alternateView.LinkedResources.Add(r);
                }
                return alternateView;
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                LogUser.agregarLog(error);
                return null;
            }
        }

        public ActionResult ListarNotasdeDetalle(int nvNumero)
        {
            try
            {

                NotadeVentaCabeceraModels NVC = new NotadeVentaCabeceraModels();
                List<NotadeVentaCabeceraModels> NVCL = new List<NotadeVentaCabeceraModels>();
                NotaDeVentaDetalleModels NVD = new NotaDeVentaDetalleModels();
                List<NotaDeVentaDetalleModels> NVDL = new List<NotaDeVentaDetalleModels>();

                NVC.NVNumero = nvNumero;
                NVD.NVNumero = nvNumero;

                var listaNVD = controlDisofi().ListarNotaDetalle(NVD);

                if (listaNVD != null)
                {
                    NVDL = listaNVD;
                }

                ViewBag.NVDL = NVDL;

                return View();
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }

        }

        public JsonResult ObtenerSaldo()
        {
            return Json(1, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region DashBoard

        [HttpGet]
        public ActionResult InformacionVentas(string fechaDesde, string fechaHasta, string esAprobador)
        {
            int perfil = SessionVariables.SESSION_DATOS_USUARIO.TipoUsuario; //idtipo
            ViewBag.Perfil = perfil;
            string codVendedor = SessionVariables.SESSION_DATOS_USUARIO.CodigoUsuario; //CodigoUsuario
            
            ParametrosModels parametros = ObtieneParametros();

            ViewBag.Parametros = parametros;


            string filtroVendedores = esAprobador == null || esAprobador == "undefined" ? "-3" : esAprobador;
            SessionVariables.SESSION_DATOS_USUARIO.Email = filtroVendedores;
            ViewBag.CodigoVendedor = esAprobador;

            #region Lista Vendedores

            List<VendedoresModel> vendedores = controlDisofi().obtenerVendedoresSoftland(baseDatosUsuario());
            ViewBag.listaVendedores = vendedores;

            #endregion

            try
            {
                if (string.IsNullOrEmpty(fechaDesde) && string.IsNullOrEmpty(fechaHasta))
                {
                    #region FormatoFecha

                    var fh = DateTime.Now.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
                    var year = DateTime.Now.Year;
                    var month = DateTime.Now.Month;
                    var fd = year + "/" + month + "/01";
                    #endregion

                    #region Total Ventas Actual
                    //Booking sales
                    List<InformeModel> informeVentasHoy = perfil == 3 ? controlDisofi().GetInformeVentaActual(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeVentaActual(baseDatosUsuario(), codVendedor);
                    ViewBag.informeVentasHoy = informeVentasHoy;

                    List<InformeModel> informeVentasAnterior = perfil == 3 ? controlDisofi().GetInformeVentaActualAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeVentaActualAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.informeVentasAnterior = informeVentasAnterior;

                    #endregion

                    #region Total Clientes Atendidos y Compras - Actual

                    List<InformeModel> informeCantCliAtendidosActual = perfil == 3 ? controlDisofi().GetInformeClientesAtendidosActual(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeClientesAtendidosActual(baseDatosUsuario(), codVendedor);
                    ViewBag.CantCliAtendidosActual = informeCantCliAtendidosActual;

                    List<InformeModel> informeCantCliAtendidosActualAnterior = perfil == 3 ? controlDisofi().GetInformeClientesAtendidosActualAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeClientesAtendidosActualAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.cantCliAtendidosActualAnterior = informeCantCliAtendidosActualAnterior;

                    List<InformeModel> informeClientesCompraActual = perfil == 3 ? controlDisofi().GetInformeClientesCompraActual(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeClientesCompraActual(baseDatosUsuario(), codVendedor);
                    ViewBag.clientesCompraActual = informeClientesCompraActual;

                    #endregion

                    #region Total Vendedores y Ventas por Vendedor - Actual

                    List<InformeModel> informeCantVendedorVentasActual = perfil == 3 ? controlDisofi().GetInformeCantVendedoresActual(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeCantVendedoresActual(baseDatosUsuario(), codVendedor);
                    ViewBag.cantVendVentasActual = informeCantVendedorVentasActual;

                    List<InformeModel> informeCantVendedorVentasActualAnterior = perfil == 3 ? controlDisofi().GetInformeCantVendedoresActualAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeCantVendedoresActualAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.cantVendVentasActualAnterior = informeCantVendedorVentasActualAnterior;

                    List<InformeModel> informeVendedorVentasActual = perfil == 3 ? controlDisofi().GetInformeVentasVendedoresActual(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeVentasVendedoresActual(baseDatosUsuario(), codVendedor);
                    ViewBag.VendedoresVentasActual = informeVendedorVentasActual;

                    #endregion

                    #region Ventas Mes Anterior

                    List<InformeModel> informeVentasMesAnterior = perfil == 3 ? controlDisofi().GetInformeVentasMesAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeVentasMesAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.VentasMesAnterior = informeVentasMesAnterior;

                    #endregion

                    #region Venta Total Diaria y Clientes Atendidos

                    List<InformeModel> informeVentaDiaria = perfil == 3 ? controlDisofi().getVentaTotalDia(baseDatosUsuario(), filtroVendedores) : controlDisofi().getVentaTotalDia(baseDatosUsuario(), codVendedor);
                    ViewBag.ventaDelDia = informeVentaDiaria;

                    List<InformeModel> informeVentaDiariaAnterior = perfil == 3 ? controlDisofi().getVentaTotalDiaAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().getVentaTotalDiaAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.ventaDelDiaAnterior = informeVentaDiariaAnterior;

                    List<InformeModel> informeCantCliDia = perfil == 3 ? controlDisofi().getInformeClientesVentaDia(baseDatosUsuario(), filtroVendedores) : controlDisofi().getInformeClientesVentaDia(baseDatosUsuario(), codVendedor);
                    ViewBag.infCantCliHoy = informeCantCliDia;

                    List<InformeModel> informeCantCliDiaAnterior = perfil == 3 ? controlDisofi().getInformeClientesVentaDiaAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().getInformeClientesVentaDiaAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.infCantCliHoyAnterior = informeCantCliDiaAnterior;

                    #endregion

                    #region Top Productos

                    double ventaTotalProducto = 0;

                    List<InformeModel> informeCantProductos = perfil == 3 ? controlDisofi().getCantProductos(baseDatosUsuario(), filtroVendedores) : controlDisofi().getCantProductos(baseDatosUsuario(), codVendedor);
                    ViewBag.CantProductos = informeCantProductos;

                    List<InformeModel> topProductosVenta = perfil == 3 ? controlDisofi().getTopProductosActual(baseDatosUsuario(), filtroVendedores) : controlDisofi().getTopProductosActual(baseDatosUsuario(), codVendedor);
                    ViewBag.topProdVentas = topProductosVenta;

                    for (int i = 0; i < topProductosVenta.Count; i++)
                    {
                        ventaTotalProducto += topProductosVenta[i].VentaTotalProducto;
                    }
                    ViewBag.ventaTotalProductos = ventaTotalProducto;

                    #endregion

                    #region Clientes Totales

                    //Aprobadas y Facturadas
                    if ((parametros.notaVenta==false) && (parametros.Backlog==false) && (parametros.Booking==false))
                    {
                        List<ClientesModels> clientes = new List<ClientesModels>();
                        ViewBag.ClientesTotales = clientes;
                    }
                    else {
                    

                        List<ClientesModels> clientes = perfil == 3 ? controlDisofi().getClientesTotales(baseDatosUsuario(), fd, fh, "A", filtroVendedores) : controlDisofi().getClientesTotales(baseDatosUsuario(), fd, fh, "A", codVendedor);
                        ViewBag.ClientesTotales = clientes;
                    }



                    //Pendientes
                    if ((parametros.notaVenta == false) && (parametros.Backlog == false) && (parametros.Booking == false))
                    {
                       
                        List<ClientesModels> clienteBackLog = new List<ClientesModels>();
                        ViewBag.ClientesTotalesPen = clienteBackLog;
                    }
                    else
                    {
                        List<ClientesModels> clientesPen = perfil == 3 ? controlDisofi().getClientesTotalesBlacklog(baseDatosUsuario(), "A", filtroVendedores) : controlDisofi().getClientesTotalesBlacklog(baseDatosUsuario(), "A", codVendedor);
                        clientesPen.OrderBy(v => v.CodCliente).ToList();

                        List<ClientesModels> clienteBackLog = new List<ClientesModels>();
                        ClientesModels cliAuxi = new ClientesModels();

                        if (clientesPen.Count == 0)
                        {
                            cliAuxi = new ClientesModels
                            {
                                CodCliente = "",
                                NomCliente = "",
                                Total = 0
                            };
                            clienteBackLog.Add(cliAuxi);

                            //clienteBackLog = 0;
                            ViewBag.ClientesTotalesPen = clienteBackLog;

                        }
                        else
                        {
                            string codAuxi = clientesPen[0].CodCliente;
                            double suma = 0;
                            int fila = 0;

                            for (int i = 0; i < clientesPen.Count; i++)
                            {
                                if (codAuxi == clientesPen[i].CodCliente)
                                {
                                    suma += clientesPen[i].Total;
                                }
                                else
                                {
                                    cliAuxi = new ClientesModels
                                    {
                                        CodCliente = clientesPen[i - 1].CodCliente,
                                        NomCliente = clientesPen[i - 1].NomCliente,
                                        Total = suma
                                    };
                                    clienteBackLog.Add(cliAuxi);
                                    suma = 0;
                                    suma = clientesPen[i].Total;
                                    codAuxi = clientesPen[i].CodCliente;
                                    fila = i;
                                }
                            }
                            cliAuxi = new ClientesModels
                            {
                                CodCliente = clientesPen[fila].CodCliente,
                                NomCliente = clientesPen[fila].NomCliente,
                                Total = suma
                            };
                            clienteBackLog.Add(cliAuxi);

                            clienteBackLog = clienteBackLog.OrderByDescending(m => m.Total).ToList();
                            ViewBag.ClientesTotalesPen = clienteBackLog;
                        }

                        ViewBag.ClientesTotalesPen = clienteBackLog;
                    }

                    //Por ventas
                    List<ClientesModels> ventasPorCliente = perfil == 3 ? controlDisofi().getVentasPorClientes(baseDatosUsuario(), fd, fh, filtroVendedores) : controlDisofi().getVentasPorClientes(baseDatosUsuario(), fd, fh, codVendedor);
                    ViewBag.VentasPorCliente = ventasPorCliente;

                    #endregion

                    #region Total Notas de Venta
                    //Aprobadas
                    if ((parametros.notaVenta == false) && (parametros.Backlog == false) && (parametros.Booking == false))
                    {
                        List<TotalNotasDeVenta> totalNv = new List<TotalNotasDeVenta>();
                        ViewBag.totalNv = totalNv;
                    }
                    else {
                        List<TotalNotasDeVenta> totalNv = perfil == 3 ? controlDisofi().getTotalNotasDeVenta(baseDatosUsuario(), fd, fh, "A", filtroVendedores) : controlDisofi().getTotalNotasDeVenta(baseDatosUsuario(), fd, fh, "A", codVendedor);
                        ViewBag.totalNv = totalNv;
                    }

                    //Pendientes
                    if ((parametros.notaVenta == false) && (parametros.Backlog == false) && (parametros.Booking == false))
                    {
                       

                        List<TotalNotasDeVenta> totalNvPen = new List<TotalNotasDeVenta>();
                        ViewBag.totalNvPendientes = 0;
                        ViewBag.totalNvPen = totalNvPen;
                    }
                    else {

                        List<TotalNotasDeVenta> totalNvPen = perfil == 3 ? controlDisofi().getTotalNotasDeVentaBlacklog(baseDatosUsuario(), "A", filtroVendedores) : controlDisofi().getTotalNotasDeVentaBlacklog(baseDatosUsuario(), "A", codVendedor);

                        double sumaTotal = 0;

                        foreach (var item in totalNvPen)
                        {
                            sumaTotal += item.Total;
                        }

                        ViewBag.totalNvPendientes = sumaTotal;
                        ViewBag.totalNvPen = totalNvPen;//totalNvPen;

                    }
                    #endregion

                    ViewBag.bandera = "1";

                    return View();
                }
                else
                {
                    #region FormatoFecha
                    //para la informacion con filtro se formatea fecha mes,año,dia
                    //para la informacion de años anteriores se formatea fecha dia,mes,año
                    var fechaD = DateTime.Parse(fechaDesde);
                    fechaD.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    var fechaH = DateTime.Parse(fechaHasta);
                    fechaH.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    #endregion

                    #region Ventas Total con Filtro

                    List<InformeFiltroModel> informeVentasFiltro = perfil == 3 ? controlDisofi().GetInformeVentaActualFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().GetInformeVentaActualFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.informeVentasFiltro = informeVentasFiltro;

                    List<InformeFiltroModel> informeVentasFiltroAnterior = perfil == 3 ? controlDisofi().GetInformeVentaFiltroAnterior(baseDatosUsuario(), fechaD, fechaH, filtroVendedores) : controlDisofi().GetInformeVentaFiltroAnterior(baseDatosUsuario(), fechaD, fechaH, codVendedor);
                    ViewBag.informeVentasFiltroAnterior = informeVentasFiltroAnterior;

                    #endregion

                    #region Total Clientes Atendidos y Compras - Filtro

                    List<InformeFiltroModel> informeCantCliAtendidosFiltro = perfil == 3 ? controlDisofi().GetInformeClientesAtendidosFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().GetInformeClientesAtendidosFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.cantCliAtendidosFiltro = informeCantCliAtendidosFiltro;

                    List<InformeFiltroModel> informeCantCliAtendidosFiltroAnterior = perfil == 3 ? controlDisofi().GetInformeClientesAtendidosFiltroAnterior(baseDatosUsuario(), fechaD, fechaH, filtroVendedores) : controlDisofi().GetInformeClientesAtendidosFiltroAnterior(baseDatosUsuario(), fechaD, fechaH, codVendedor);
                    ViewBag.cantCliAtendidosFiltroAnterior = informeCantCliAtendidosFiltroAnterior;

                    List<InformeFiltroModel> informeClienteCompraFiltro = perfil == 3 ? controlDisofi().GetInformeClientesCompraFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().GetInformeClientesCompraFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.clientesCompraFiltro = informeClienteCompraFiltro;

                    #endregion

                    #region Total Vnededores y Vnetas por Vendedor - Filtro

                    List<InformeFiltroModel> informeCantVendedorVentasActualAnterior = perfil == 3 ? controlDisofi().GetInformeCantVendedoresFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().GetInformeCantVendedoresFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.cantVendVentasFiltro = informeCantVendedorVentasActualAnterior;

                    List<InformeFiltroModel> informeVendedorVentasFiltro = perfil == 3 ? controlDisofi().GetInformeVentasVendedoresFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().GetInformeVentasVendedoresFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.VendedoresVentasFiltro = informeVendedorVentasFiltro;

                    #endregion

                    #region Top Productos - Filtro

                    List<InformeFiltroModel> informeTopProductosFiltro = perfil == 3 ? controlDisofi().getTopProductosFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().getTopProductosFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.TopProductosFiltro = informeTopProductosFiltro;

                    List<InformeFiltroModel> informeCantProductosFiltro = perfil == 3 ? controlDisofi().getCantProductosFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().getCantProductosFiltro(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.cantProductosFiltro = informeCantProductosFiltro;

                    #endregion

                    #region Clientes Totales
                    //Aprobadas
                    if ((parametros.notaVenta==false)&&(parametros.Booking==false)&&(parametros.Backlog==false)) {
                       
                        List<ClientesModels> clientes = new List<ClientesModels>();
                        ViewBag.ClientesTotales = clientes;
                    }
                    else {
                        List<ClientesModels> clientes = perfil == 3 ? controlDisofi().getClientesTotales(baseDatosUsuario(), fechaDesde, fechaHasta, "A", filtroVendedores) : controlDisofi().getClientesTotales(baseDatosUsuario(), fechaDesde, fechaHasta, "A", codVendedor);
                        ViewBag.ClientesTotales = clientes;
                    }
                    //Pendientes
                    if ((parametros.notaVenta == false) && (parametros.Booking == false) && (parametros.Backlog == false))
                    {
                     

                        List<ClientesModels> clienteBackLog = new List<ClientesModels>();
                        ViewBag.ClientesTotalesPen = clienteBackLog;
                    }
                    else {
                        List<ClientesModels> clientesPen = perfil == 3 ? controlDisofi().getClientesTotalesBlacklog(baseDatosUsuario(), "A", filtroVendedores) : controlDisofi().getClientesTotalesBlacklog(baseDatosUsuario(), "A", codVendedor);
                        clientesPen.OrderBy(v => v.CodCliente).ToList();

                        List<ClientesModels> clienteBackLog = new List<ClientesModels>();

                        ClientesModels cliAuxi = new ClientesModels();

                        string codAuxi = clientesPen.Count > 0 ? clientesPen[0].CodCliente : "";
                        double suma = 0;
                        int fila = 0;

                        for (int i = 0; i < clientesPen.Count; i++)
                        {
                            if (codAuxi == clientesPen[i].CodCliente)
                            {
                                suma += clientesPen[i].Total;
                            }
                            else
                            {
                                cliAuxi = new ClientesModels
                                {
                                    CodCliente = clientesPen[i - 1].CodCliente,
                                    NomCliente = clientesPen[i - 1].NomCliente,
                                    Total = suma
                                };
                                clienteBackLog.Add(cliAuxi);
                                suma = 0;
                                suma = clientesPen[i].Total;
                                codAuxi = clientesPen[i].CodCliente;
                                fila = i;
                            }
                        }
                        if (clientesPen.Count > 0)
                        {
                            cliAuxi = new ClientesModels
                            {
                                CodCliente = clientesPen[fila].CodCliente,
                                NomCliente = clientesPen[fila].NomCliente,
                                Total = suma
                            };
                        }
                        else
                        {
                            cliAuxi = new ClientesModels
                            {
                                CodCliente = "-1",
                                NomCliente = "-1",
                                Total = -1
                            };
                        }

                        clienteBackLog.Add(cliAuxi);

                        clienteBackLog = clienteBackLog.OrderByDescending(m => m.Total).ToList();


                        ViewBag.ClientesTotalesPen = clienteBackLog;
                    }
                    //Ventas por Cliente
                    List<ClientesModels> ventasPorCliente = perfil == 3 ? controlDisofi().getVentasPorClientes(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().getVentasPorClientes(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    ViewBag.VentasPorCliente = ventasPorCliente;

                    #endregion

                    #region Total Notas de Venta

                    //Aprobadas
                    if ((parametros.notaVenta == false) && (parametros.Backlog == false) && (parametros.Booking == false))
                    {
                        List<TotalNotasDeVenta> totalNv = new List<TotalNotasDeVenta>();
                        ViewBag.totalNv = totalNv;
                    }
                    else {
                        List<TotalNotasDeVenta> totalNv = perfil == 3 ? controlDisofi().getTotalNotasDeVenta(baseDatosUsuario(), fechaDesde, fechaHasta, "A", filtroVendedores) : controlDisofi().getTotalNotasDeVenta(baseDatosUsuario(), fechaDesde, fechaHasta, "A", codVendedor);
                        ViewBag.totalNv = totalNv;

                    }



                    //Pendientes
                    if ((parametros.notaVenta == false) && (parametros.Backlog == false) && (parametros.Booking == false))
                    {


                        List<TotalNotasDeVenta> totalNvPen = new List<TotalNotasDeVenta>();
                        ViewBag.totalNvPendientes = 0;
                        ViewBag.totalNvPen = totalNvPen;
                    }else
                    {
                        List<TotalNotasDeVenta> totalNvPen = perfil == 3 ? controlDisofi().getTotalNotasDeVentaBlacklog(baseDatosUsuario(), "A", filtroVendedores) : controlDisofi().getTotalNotasDeVentaBlacklog(baseDatosUsuario(), "A", codVendedor);

                        double sumaTotal = 0;

                        foreach (var item in totalNvPen)
                        {
                            sumaTotal += item.Total;
                        }

                        ViewBag.totalNvPendientes = sumaTotal;

                        ViewBag.totalNvPen = totalNvPen;

                    }
                    

                    #endregion

                    #region ultima

                    ViewBag.ventaTotalProductos = 0;

                    List<InformeModel> informeVentaDiaria = perfil == 3 ? controlDisofi().getVentaTotalDia(baseDatosUsuario(), filtroVendedores) : controlDisofi().getVentaTotalDia(baseDatosUsuario(), codVendedor);
                    ViewBag.ventaDelDia = informeVentaDiaria;

                    List<InformeModel> informeVentaDiariaAnterior = perfil == 3 ? controlDisofi().getVentaTotalDiaAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().getVentaTotalDiaAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.ventaDelDiaAnterior = informeVentaDiariaAnterior;

                    List<InformeModel> informeCantCliDia = perfil == 3 ? controlDisofi().getInformeClientesVentaDia(baseDatosUsuario(), filtroVendedores) : controlDisofi().getInformeClientesVentaDia(baseDatosUsuario(), codVendedor);
                    ViewBag.infCantCliHoy = informeCantCliDia;

                    List<InformeModel> informeCantCliDiaAnterior = perfil == 3 ? controlDisofi().getInformeClientesVentaDiaAnterior(baseDatosUsuario(), filtroVendedores) : controlDisofi().getInformeClientesVentaDiaAnterior(baseDatosUsuario(), codVendedor);
                    ViewBag.infCantCliHoyAnterior = informeCantCliDiaAnterior;


                    ViewBag.bandera = "0";
                    ViewBag.FechaDesde = fechaDesde;
                    ViewBag.FechafechaHasta = fechaHasta;

                    #endregion
                    return View();
                }
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        [HttpPost]
        public JsonResult GenerarReporteVentas(string fechaDesde, string fechaHasta, string venCod)
        {
            venCod = venCod == "" ? "-3" : venCod;
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                
                string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteVentasDash(baseDatosUsuario(), mapPath, fechaDesdeDT, fechaHastaDT, venCod);

                SessionVariables.SESSION_RUTA_ARCHIVO = path;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new System.Diagnostics.StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                LogUser.agregarLog(archivo + "");
                LogUser.agregarLog(linea + "");
                LogUser.agregarLog(e.Message);
                return Json(new { Verificador = false });
            }
        }

        [HttpPost]
        public JsonResult GenerarReporteVentasDia(string venCod)
        {
            venCod = venCod == "" ? "-3" : venCod;
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteVentasDia(mapPath,baseDatosUsuario(), venCod);

                SessionVariables.SESSION_RUTA_ARCHIVO = path;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new System.Diagnostics.StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                LogUser.agregarLog(archivo + "");
                LogUser.agregarLog(linea + "");
                LogUser.agregarLog(e.Message);
                return Json(new { Verificador = false });
            }
        }

        //Notas de Venta NotasDeVentaCliente NotasDeVentaClienteSinFacturar
        [HttpPost]
        public JsonResult NotasDeVentaCliente(string codigo, string fd, string fh, string estado)
        {
            try
            {
                List<NotasDeVentaModel> nv = controlDisofi().getNotasDeVentaPorCliente(baseDatosUsuario(), codigo, fd, fh, estado);

                return Json(new { data = nv });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        //Notas de Venta NotasDeVentaClienteBlacklog
        [HttpPost]
        public JsonResult NotasDeVentaClienteBlacklog(string codigo, string estado)
        {
            try
            {
                List<NotasDeVentaModel> nv = controlDisofi().getNotasDeVentaPorClienteBlacklog(baseDatosUsuario(), codigo, estado);

                List<NotasDeVentaModel> newNv = new List<NotasDeVentaModel>();

                var nvnumeroAux = nv[0].nvNumero;
                double suma = 0;
                var fila = 0;

                NotasDeVentaModel nvAuxi = new NotasDeVentaModel();

                for (int i = 0; i < nv.Count; i++)
                {
                    if (nvnumeroAux == nv[i].nvNumero)
                    {
                        suma += nv[i].nvTotLinea;
                    }
                    else
                    {
                        nvAuxi = new NotasDeVentaModel
                        {
                            VenDes = nv[i - 1].VenDes,
                            codAux = nv[i - 1].codAux,
                            nvestado = nv[i - 1].nvestado,
                            nvNumero = nv[i - 1].nvNumero,
                            nvfem = nv[i - 1].nvfem,
                            nvmonto = suma
                        };
                        newNv.Add(nvAuxi);
                        suma = 0;
                        suma = nv[i].nvTotLinea;
                        nvnumeroAux = nv[i].nvNumero;
                        fila = i;
                    }
                }
                nvAuxi = new NotasDeVentaModel
                {
                    VenDes = nv[fila].VenDes,
                    codAux = nv[fila].codAux,
                    nvestado = nv[fila].nvestado,
                    nvNumero = nv[fila].nvNumero,
                    nvfem = nv[fila].nvfem,
                    nvmonto = suma
                };
                newNv.Add(nvAuxi);

                return Json(new { data = newNv });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        [HttpPost]
        public JsonResult Productos(string nvnumero)
        {
            try
            {
                List<ProductosModel> prod = controlDisofi().getProductos(baseDatosUsuario(), nvnumero);

                return Json(new { data = prod });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        //Blacklog
        [HttpPost]
        public JsonResult ProductosBlacklog(string nvnumero)
        {
            try
            {
                List<ProductosModel> prod = controlDisofi().getProductosBlacklog(baseDatosUsuario(), nvnumero);

                return Json(new { data = prod });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        [HttpPost]
        public JsonResult GenerarReporteNV(string fechaDesde, string fechaHasta, string venCod)
        {
            venCod = venCod == "" ? "-3" : venCod;
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteNV(baseDatosUsuario(), mapPath, fechaDesdeDT, fechaHastaDT,venCod);

                SessionVariables.SESSION_RUTA_ARCHIVO = path;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new System.Diagnostics.StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                LogUser.agregarLog(archivo + "");
                LogUser.agregarLog(linea + "");
                LogUser.agregarLog(e.Message);
                return Json(new { Verificador = false });
            }
        }

        [HttpPost]
        public JsonResult GenerarReporteNVBacklog(string fechaDesde, string fechaHasta, string venCod)
        {
            venCod = venCod == "" ? "-3" : venCod;
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteNVBacklog(baseDatosUsuario(), mapPath, fechaDesdeDT, fechaHastaDT, venCod);

                SessionVariables.SESSION_RUTA_ARCHIVO = path;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new System.Diagnostics.StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                LogUser.agregarLog(archivo + "");
                LogUser.agregarLog(linea + "");
                LogUser.agregarLog(e.Message);
                return Json(new { Verificador = false });
            }
        }

        //Ventas
        [HttpPost]
        public JsonResult DetalleVentas(string codigo, string fd, string fh)
        {
            try
            {
                List<DetalleVentasModel> detalle = controlDisofi().getDetalleVentasPorClientes(baseDatosUsuario(), fd, fh, codigo);

                return Json(new { data = detalle });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        [HttpPost]
        public JsonResult ProductosVenta(decimal folio, string tipo)
        {
            try
            {
                List<ProductosVentaModel> prodVen = controlDisofi().getProductosVenta(baseDatosUsuario(), folio, tipo);

                return Json(new { data = prodVen });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }


        [HttpPost]
        public JsonResult informePeriodos()
        {
            try
            {
                var periodoActual = controlDisofi().getPeriodoActual(baseDatosUsuario());
                var periodoAnterior = controlDisofi().getPeriodoAnterior(baseDatosUsuario());
                var periodoTercero = controlDisofi().getPeriodoTercero(baseDatosUsuario());

                if (periodoActual.Count != 0 || periodoAnterior.Count != 0)
                {
                    if (periodoActual.Count == 0)
                    {
                        var periodoActualAuxi = new DiagramaModel
                        {
                            VentaTotalActual = 0
                        };
                        periodoActual.Add(periodoActualAuxi);
                    }
                    else
                    {
                        for (int i = 0; i < periodoActual.Count; i++)
                        {
                            periodoActual[i].VentaTotalActual = Math.Round((periodoActual[i].VentaTotalActual / 1000), 0);
                        }
                    }

                    for (int i = 0; i < periodoAnterior.Count; i++)
                    {
                        periodoAnterior[i].VentaTotalAnterior = Math.Round((periodoAnterior[i].VentaTotalAnterior / 1000), 0);
                    }

                    for (int i = 0; i < periodoTercero.Count; i++)
                    {
                        periodoTercero[i].VentaTotalTercero = Math.Round((periodoTercero[i].VentaTotalTercero / 1000), 0);
                    }

                    return Json(new { pActual = periodoActual, pAnterior = periodoAnterior, pTercero = periodoTercero });
                }
                else
                {
                    return Json(new { pActual = periodoActual, pAnterior = periodoAnterior, pTercero = periodoTercero });
                }
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }

        public ActionResult DescargarArchivoVentas()
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO;
            return DescargarArchivo(path, "Reporte Ventas");
        }

        public ActionResult DescargarArchivoNVBooking()
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO;
            return DescargarArchivo(path, "Reporte Notas de Venta Booking");
        }

        public ActionResult DescargarArchivoNVBlacklog()
        {
            string path = SessionVariables.SESSION_RUTA_ARCHIVO;
            return DescargarArchivo(path, "Reporte Notas de Venta Backlog");
        }

        public JsonResult buscarVendedorActual(int bandera, string fd, string fh)
        {
            int perfil = SessionVariables.SESSION_DATOS_USUARIO.TipoUsuario;
            string codVendedor = SessionVariables.SESSION_DATOS_USUARIO.CodigoUsuario;
            string filtroVendedores = SessionVariables.SESSION_DATOS_USUARIO.Email;
            if (bandera == 1)
            {
                List<InformeModel> informeVendedorVentasActual = perfil == 3 ? controlDisofi().GetInformeVentasVendedoresActual(baseDatosUsuario(), filtroVendedores) : controlDisofi().GetInformeVentasVendedoresActual(baseDatosUsuario(), codVendedor);
                return Json(informeVendedorVentasActual);
            }
            else
            {
                List<InformeFiltroModel> informeVendedorVentasFiltro = perfil == 3 ? controlDisofi().GetInformeVentasVendedoresFiltro(baseDatosUsuario(), fd, fh, filtroVendedores) : controlDisofi().GetInformeVentasVendedoresFiltro(baseDatosUsuario(), fd, fh, codVendedor);
                return Json(informeVendedorVentasFiltro);
            }

        }

        #endregion

        #region Dashboard Cobranza

        #region vista
        [HttpGet]
        public ActionResult InformacionReporteSaldos(string fechaDesde, string fechaHasta, string esAprobador)
        {
            int perfil = SessionVariables.SESSION_DATOS_USUARIO.TipoUsuario; //idtipo
            ViewBag.Perfil = perfil;
            string codVendedor = SessionVariables.SESSION_DATOS_USUARIO.CodigoUsuario; //CodigoUsuario

            string filtroVendedores = esAprobador == null || esAprobador == "undefined" ? "-3" : esAprobador;
            SessionVariables.SESSION_DATOS_USUARIO.Email = filtroVendedores;
            ViewBag.CodigoVendedor = esAprobador;

            #region Lista Vendedores

            List<VendedoresModel> vendedores = controlDisofi().obtenerVendedoresSoftland(baseDatosUsuario());
            ViewBag.listaVendedores = vendedores;

            #endregion
            ParametrosModels parametros = ObtieneParametros();

            ViewBag.Parametros = parametros;

            try
            {
                if (string.IsNullOrEmpty(fechaDesde) && string.IsNullOrEmpty(fechaHasta))
                {
                    #region FormatoFecha

                    var fh = DateTime.Now.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
                    var year = DateTime.Now.Year;
                    var month = DateTime.Now.Month;
                    var fd = year + "/" + month + "/01";
                    #endregion
                    #region Informacion Saldos

                    double MontoTotal = 0;
                    double SaldoAux = 0;
                    fd = "2012/01/01";
                    double MontoProveedor = 0;
                    double saldoProvee = 0;

                    List<InformacionSaldosModel> SaldosClientes = perfil == 3 ? controlDisofi().getSaldoCuentaClientes(baseDatosUsuario(), fd, fh, filtroVendedores) : controlDisofi().getSaldoCuentaClientes(baseDatosUsuario(), fd, fh, codVendedor);
                    List<InformacionSaldosModel> SaldosProveedor = perfil == 3 ? controlDisofi().getSaldosCuentasProveedor(baseDatosUsuario(), fd, fh, filtroVendedores) : controlDisofi().getSaldosCuentasProveedor(baseDatosUsuario(), fd, fh, codVendedor);


                    foreach (InformacionSaldosModel item in SaldosClientes)
                    {
                        MontoTotal = item.saldototal;
                        SaldoAux = MontoTotal + SaldoAux;

                    }


                    foreach (InformacionSaldosModel item in SaldosProveedor)
                    {

                        MontoProveedor = item.saldototal;
                        saldoProvee = MontoProveedor + saldoProvee;


                    }

                    SaldosClientes = SaldosClientes.OrderBy(v => v.saldototal).ToList();
                    ViewBag.saldosClientes = SaldosClientes;
                    ViewBag.saldoTotal = SaldoAux;

                    ViewBag.saldoTotalProveedor = saldoProvee;
                    ViewBag.saldosProveedor = SaldosProveedor;



                    #endregion

                    ViewBag.bandera = "1";

                    return View();
                }
                else
                {
                    #region FormatoFecha
                    //para la informacion con filtro se formatea fecha mes,año,dia
                    //para la informacion de años anteriores se formatea fecha dia,mes,año
                    var fechaD = DateTime.Parse(fechaDesde);
                    fechaD.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                    var fechaH = DateTime.Parse(fechaHasta);
                    fechaH.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    #endregion
                    #region Informacion Saldos
                    double MontoTotal = 0;
                    double SaldoAux = 0;

                    double MontoProveedor = 0;
                    double saldoProvee = 0;

                    List<InformacionSaldosModel> SaldosClientes = perfil == 3 ? controlDisofi().getSaldoCuentaClientes(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().getSaldoCuentaClientes(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);
                    List<InformacionSaldosModel> SaldosProveedor = perfil == 3 ? controlDisofi().getSaldosCuentasProveedor(baseDatosUsuario(), fechaDesde, fechaHasta, filtroVendedores) : controlDisofi().getSaldosCuentasProveedor(baseDatosUsuario(), fechaDesde, fechaHasta, codVendedor);

                    foreach (InformacionSaldosModel item in SaldosClientes)
                    {
                        MontoTotal = item.saldototal;
                        SaldoAux = MontoTotal + SaldoAux;

                    }

                    foreach (InformacionSaldosModel item in SaldosProveedor)
                    {

                        MontoProveedor = item.saldototal;
                        saldoProvee = MontoProveedor + saldoProvee;


                    }

                    SaldosClientes = SaldosClientes.OrderBy(v => v.saldototal).ToList();
                    ViewBag.saldosClientes = SaldosClientes;
                    ViewBag.saldoTotal = SaldoAux;

                    ViewBag.saldoTotalProveedor = saldoProvee;
                    ViewBag.saldosProveedor = SaldosProveedor;


                    #endregion

                    return View();
                }
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }
        }
        #endregion


        //saldos por clientes
        [HttpPost]
        public JsonResult ObtenerSaldoClientes(string RutAuxiliar, string fd, string fh, string CodAux)
        {
            double MontoTotal = 0;
            double Saldo = 0;
            try
            {
                List<SaldosModel> Saldos = new List<SaldosModel>();
                Saldos = controlDisofi().ObtenerSaldoClientes(RutAuxiliar, fd, fh, CodAux, baseDatosUsuario());

                foreach (SaldosModel item in Saldos)
                {
                    MontoTotal = item.Saldo;
                    Saldo = MontoTotal + Saldo;
                }

                return Json(new { DetalleSaldo = Saldos, SaldoTotal = MontoTotal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }


        //Saldos por Proveedor

        [HttpPost]
        public JsonResult ObtenerSaldoProveedor(string RutAuxiliar, string fd, string fh, string CodAux)
        {
            double MontoTotal = 0;
            double Saldo = 0;
            try
            {
                List<SaldosModel> Saldos = new List<SaldosModel>();
                Saldos = controlDisofi().ObtenerSaldoProveedor(RutAuxiliar, fd, fh, CodAux, baseDatosUsuario());

                foreach (SaldosModel item in Saldos)
                {
                    MontoTotal = item.Saldo;
                    Saldo = MontoTotal + Saldo;
                }

                return Json(new { DetalleSaldo = Saldos, SaldoTotal = MontoTotal }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #region Graficos 
        [HttpPost]
        public JsonResult informePeriodosSaldos()
        {
            List<periodoSaldosModel> periodoActual = new List<periodoSaldosModel>();
            List<periodoSaldosModel> periodoAnterior = new List<periodoSaldosModel>();
            List<periodoSaldosModel> periodoTercero = new List<periodoSaldosModel>();
            try
            {
                string[] meses = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
                int[] aniosPeriodos;
                aniosPeriodos = new int[3];
                aniosPeriodos[0] = DateTime.Now.Year - 2;
                aniosPeriodos[1] = DateTime.Now.Year - 1;
                aniosPeriodos[2] = DateTime.Now.Year;
                var periodos = controlDisofi().getPeriodos(baseDatosUsuario());

                double saldoActual = 0;
                double montoAux = 0;
                for (int i = 0; i < periodos.Count; i++)
                {
                    if (int.Parse(periodos[i].Cpbano) < aniosPeriodos[0])
                    {
                        montoAux = saldoActual;
                        saldoActual = montoAux + periodos[i].saldo;
                    }
                    else
                    {
                        break;
                    }
                }
                double valorX = saldoActual;
                for (int i = 0; i < aniosPeriodos.Count(); i++)
                {
                    List<periodoSaldosModel> periodoAux1 = periodos.FindAll(v => v.Cpbano == aniosPeriodos[i].ToString()).OrderBy(v => v.cpbMes).ToList();

                    var result = from item in periodoAux1
                                 group item by item.cpbMes into g
                                 select new periodoSaldosModel()
                                 {
                                     cpbMes = g.Key,
                                     saldo = g.Sum(x => x.saldo)
                                 };
                    var periodoAux = new List<periodoSaldosModel>();
                    foreach (var item in result)
                    {
                        item.Cpbano = periodoAux1.Count < 0 ? "0" : periodoAux1[0].Cpbano;
                        periodoAux.Add(item);
                    }

                    int contaux = 0;

                    for (int x = 0; x < meses.Count(); x++)
                    {
                        //if (aniosPeriodos[i] == DateTime.Now.Year && int.Parse(meses[x]) == DateTime.Now.Month)
                        //{
                        //    break;
                        //}
                        if (periodoAux[contaux].cpbMes != meses[x])
                        {
                            //var numero = int.Parse(periodoAux[contaux].cpbMes) < int.Parse(meses[x]) ? 0 : valorX;
                            periodoSaldosModel aux = new periodoSaldosModel()
                            {
                                Cpbano = aniosPeriodos[i].ToString(),
                                cpbMes = meses[x],
                                saldo = valorX
                            };
                            if (aniosPeriodos[i] == 2019)
                            {
                                periodoTercero.Add(aux);

                            }
                            else if (aniosPeriodos[i] == 2020)
                            {

                                periodoAnterior.Add(aux);
                            }
                            else
                            {

                                periodoActual.Add(aux);
                            }

                        }
                        else
                        {
                            periodoSaldosModel aux = new periodoSaldosModel()
                            {
                                Cpbano = aniosPeriodos[i].ToString(),
                                cpbMes = meses[x],
                                saldo = valorX + periodoAux[contaux].saldo
                            };
                            valorX += periodoAux[contaux].saldo;
                            
                            if((periodoAux.Count -1) == contaux)
                            {
                                contaux = periodoAux.Count - 1;
                            }
                            else
                            {
                                contaux++;
                            }

                            if (aniosPeriodos[i] == 2019)
                            {
                                periodoTercero.Add(aux);

                            }
                            else if (aniosPeriodos[i] == 2020)
                            {

                                periodoAnterior.Add(aux);
                            }
                            else
                            {

                                periodoActual.Add(aux);
                            }
                        }
                    }
                }
                return Json(new { pActual = periodoActual, pAnterior = periodoAnterior, pTercero = periodoTercero });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }

        }


        // Grafico proveedores.
        [HttpPost]
        public JsonResult informePeriodosSaldosProveedor()
        {
            List<periodoSaldosModel> periodoActual = new List<periodoSaldosModel>();
            List<periodoSaldosModel> periodoAnterior = new List<periodoSaldosModel>();
            List<periodoSaldosModel> periodoTercero = new List<periodoSaldosModel>();
            try
            {
                string[] meses = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" };
                int[] aniosPeriodos;
                aniosPeriodos = new int[3];
                aniosPeriodos[0] = DateTime.Now.Year - 2;
                aniosPeriodos[1] = DateTime.Now.Year - 1;
                aniosPeriodos[2] = DateTime.Now.Year;
                var periodos = controlDisofi().getPeriodosProveedor(baseDatosUsuario());

                double saldoActual = 0;
                double montoAux = 0;
                for (int i = 0; i < periodos.Count; i++)
                {
                    if (int.Parse(periodos[i].Cpbano) < aniosPeriodos[0])
                    {
                        montoAux = saldoActual;
                        saldoActual = montoAux + periodos[i].saldo;
                    }
                    else
                    {
                        break;
                    }
                }
                double valorX = saldoActual;
                for (int i = 0; i < aniosPeriodos.Count(); i++)
                {
                    List<periodoSaldosModel> periodoAux = periodos.FindAll(v => v.Cpbano == aniosPeriodos[i].ToString()).OrderBy(v => v.cpbMes).ToList();

                    var result = from item in periodoAux
                                 group item by item.cpbMes into g
                                 select new periodoSaldosModel()
                                 {
                                     cpbMes = g.Key,
                                     saldo = g.Sum(x => x.saldo)
                                 };
                    periodoAux = new List<periodoSaldosModel>();
                    foreach (var item in result)
                    {
                      
                        periodoAux.Add(item);
                    }

                    int contaux = 0;

                    for (int x = 0; x < meses.Count(); x++)
                    {
                        //if (aniosPeriodos[i] == DateTime.Now.Year && int.Parse(meses[x]) == DateTime.Now.Month)
                        //{
                        //    break;
                        //}
                        if (periodoAux[contaux].cpbMes != meses[x])
                        {
                            periodoSaldosModel aux = new periodoSaldosModel()
                            {
                                Cpbano = aniosPeriodos[i].ToString(),
                                cpbMes = meses[x],
                                saldo = valorX
                            };
                            if (aniosPeriodos[i] == 2019)
                            {
                                periodoTercero.Add(aux);

                            }
                            else if (aniosPeriodos[i] == 2020)
                            {

                                periodoAnterior.Add(aux);
                            }
                            else
                            {

                                periodoActual.Add(aux);
                            }

                        }
                        else
                        {
                            periodoSaldosModel aux = new periodoSaldosModel()
                            {
                                Cpbano = aniosPeriodos[i].ToString(),
                                cpbMes = meses[x],
                                saldo = valorX + periodoAux[contaux].saldo
                            };
                            valorX += periodoAux[contaux].saldo;
                            
                            if ((periodoAux.Count - 1) == contaux)
                            {
                                contaux = periodoAux.Count - 1;
                            }
                            else
                            {
                                contaux++;
                            }

                          
                            if (aniosPeriodos[i] == 2019)
                            {
                                periodoTercero.Add(aux);

                            }
                            else if (aniosPeriodos[i] == 2020)
                            {

                                periodoAnterior.Add(aux);
                            }
                            else
                            {

                                periodoActual.Add(aux);
                            }
                        }
                    }
                }
                return Json(new { pActual = periodoActual, pAnterior = periodoAnterior, pTercero = periodoTercero }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                return null;
            }

        }


        #endregion

        #region excel saldos

        // Generar reportes saldos
        [HttpPost]
        public JsonResult GenerarReporteSaldosClientes(string fechaDesde, string fechaHasta, string venCod)
        {
            venCod = venCod == "" ? "-3" : venCod;

            if (fechaDesde == null)
            {

                fechaDesde = "2012-01-01";
            }
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteSaldosClientes(baseDatosUsuario(), mapPath, fechaDesdeDT, fechaHastaDT, venCod);

                SessionVariables.SESSION_RUTA_ARCHIVO = path;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new System.Diagnostics.StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                LogUser.agregarLog(archivo + "");
                LogUser.agregarLog(linea + "");
                LogUser.agregarLog(e.Message);
                return Json(new { Verificador = false });
            }
        }


        //Generar excel saldos proveedor

        [HttpPost]
        public JsonResult GenerarReporteSaldosProveedores(string fechaDesde, string fechaHasta, string venCod)
        {
            venCod = venCod == "" ? "-3" : venCod;
            if (fechaDesde == null) { fechaDesde = "2012-01-01"; }

            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");

                DateTime fechaDesdeDT = DateTime.Parse(fechaDesde);
                DateTime fechaHastaDT = DateTime.Parse(fechaHasta);

                string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteSaldosProveedores(baseDatosUsuario(), mapPath, fechaDesdeDT, fechaHastaDT, venCod);

                SessionVariables.SESSION_RUTA_ARCHIVO = path;
                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                var st = new System.Diagnostics.StackTrace(e, true);
                var frame = st.GetFrame(0);

                var linea = frame.GetFileLineNumber();
                var archivo = frame.GetFileName();

                LogUser.agregarLog(archivo + "");
                LogUser.agregarLog(linea + "");
                LogUser.agregarLog(e.Message);
                return Json(new { Verificador = false });
            }
        }



        #endregion

        #endregion

        #region Reporte Saldo
       
        #region Vista
        public ActionResult ReporteSaldo()
        {
            var empresas = controlDisofi().GetEmpresas();
            ViewBag.empresas = empresas;
            return View();
        }

        #endregion


        [HttpPost]
        public JsonResult GenerarReporteSaldo(string NombreBase, string Anio, string Fecha)
        {
            try
            {
                string mapPath = System.Web.Hosting.HostingEnvironment.MapPath("~/ArchivosTemporales/");
                string path = new UTIL.Excel.EXCEL(controlDisofi()).generarExcelReporteSaldo(mapPath, NombreBase, Anio, Fecha);
                SessionVariables.SESSION_RUTA_ARCHIVO = path;

                return Json(new { Verificador = true });
            }
            catch (Exception e)
            {
                string error = e.Message.ToString();
                throw (e);
            }
        }
        public ActionResult DescargarArchivoReporte()
        {
            string nombreArchivo = "";
            string path = SessionVariables.SESSION_RUTA_ARCHIVO;

            if (path == null)
            {
                return RedirectToAction("Error", "Error");
            }
            else
            {
                byte[] fileBytes = System.IO.File.ReadAllBytes(path);
                string fileName = new System.IO.FileInfo(path).Name;
                string fileExtension = new System.IO.FileInfo(path).Extension;

                nombreArchivo = nombreArchivo == "" ? fileName : nombreArchivo;

                SessionVariables.SESSION_RUTA_ARCHIVO = null;
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, nombreArchivo + fileExtension);
            }
        }




        #endregion

        #region Cotizacion

        public ActionResult CotizacionesPendientes()
        {
            ParametrosModels parametros = ObtieneParametros();
            var tipoUsuario = SessionVariables.SESSION_DATOS_USUARIO.TipoUsuario;
            ViewBag.Parametros = parametros;

            List<NotadeVentaCabeceraModels> doc = new List<NotadeVentaCabeceraModels>();
            string codigoVendedor = obtenerUsuario().TipoUsuario == Convert.ToInt32(PERFILES.APROBADOR) ? "-1" : codigoVendedorUsuario();
            ViewBag.vencod = codigoVendedor.Trim();
            var docPendientes = controlDisofi().listarCotizacionPendientes(baseDatosUsuario(), EmpresaUsuario().IdEmpresa,codigoVendedor);

            if (docPendientes != null)
            {
                doc = docPendientes;
            }

            ViewBag.doc = doc;


            ViewBag.Monedas = controlDisofi().ListarMonedas(baseDatosUsuario());
            ViewBag.Grupos = controlDisofi().ListarGrupos(baseDatosUsuario());
            ViewBag.SubGrupos = controlDisofi().ListarSubGrupos(baseDatosUsuario());
            ViewBag.Vendedores = controlDisofi().ListarVendedores(baseDatosUsuario());
            ViewBag.TipoUsuario = tipoUsuario;

            return View();
        }

        public JsonResult CotizacionesPendientesDetalle(int _nvId)
        {
            NotadeVentaCabeceraModels NVC = new NotadeVentaCabeceraModels();
            List<NotadeVentaCabeceraModels> NVCL = new List<NotadeVentaCabeceraModels>();
            NotaDeVentaDetalleModels NVD = new NotaDeVentaDetalleModels();
            List<NotaDeVentaDetalleModels> NVDL = new List<NotaDeVentaDetalleModels>();

            NVC.Id = _nvId;
            NVD.Id = _nvId;

            var nvc = controlDisofi().BuscarCotizacion(NVC, baseDatosUsuario());

            if (nvc != null)
            {
                NVCL = nvc;
                ViewBag.mensaje = 0;
            }
            else
            {
                ViewBag.mensaje = 1;
                ViewBag.NVnum = _nvId;
                return Json(new { Mensaje = ViewBag.mensaje, nvNum = ViewBag.NVnum });
            }

            ViewBag.cabecera = NVCL;

            var nvd = controlDisofi().BuscarCotizacionDetalle(NVD, baseDatosUsuario());

            if (nvd != null)
            {
                NVDL = nvd;
            }

            ViewBag.detalle = NVDL;

            return Json(new { Cabecera = NVCL, Detalle = NVDL, Mensaje = ViewBag.mensaje, NVNumero = ViewBag.NVnum }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult CotizacionesPendientesBuscar(string numCotizacion, string vendedor,int tienefechaCot, string fechaCotizacion, 
                                                          int tienefechaCierre, string fechaCierre)
        {

            ParametrosModels parametros = ObtieneParametros();

            ViewBag.Parametros = parametros;
            List<NotadeVentaCabeceraModels> doc = new List<NotadeVentaCabeceraModels>();

            LogUser.agregarLog("NumCot: " + numCotizacion + "\n"
                + "Vendedor: " + vendedor + "\n" + "tieneFechaCot: " + tienefechaCot + "\n"
                + "fechaCotizacion: " + fechaCotizacion + "\n" + "tieneFechaCierre:" + tienefechaCierre + "\n"
                + "fechaCierre: " + fechaCierre);

            var docPendientes = controlDisofi().CotizacionesPendientesBuscar(baseDatosUsuario(), numCotizacion, vendedor, tienefechaCot,
                                                                             fechaCotizacion,tienefechaCierre,fechaCierre);
            var mensaje = 0;
            if (docPendientes != null)
            {
                doc = docPendientes;
            }

            mensaje = docPendientes.Count>0 ? 0 : 1;

            return Json(new{cabecera=doc,Mensaje=mensaje });
        }

        public JsonResult ActualizaCabeceraCotizacion(int numCot, string fechaEmision,string fechaEntrega,string fechaCierre,
                                                        string contacto, string observacion)
        {
            RespuestaModel respuesta = controlDisofi().ActualizaCabeceraCotizacion(numCot,fechaEmision,fechaEntrega,fechaCierre,contacto,observacion);
            return Json(respuesta);
        }

        public JsonResult BuscaPro_X_Modificar(int nroLinea, string codProd,int Id)
        {
            ProductoCotizacionModels producto = controlDisofi().BuscaPro_X_Modificar(nroLinea, codProd,Id,baseDatosUsuario());
            List<MonedaModel> tipomoneda = controlDisofi().ListarMonedas(baseDatosUsuario());
            List<GrupoModel> Grupos = controlDisofi().ListarGrupos(baseDatosUsuario());
            List<SubGrupoModel> SubGrupos = controlDisofi().ListarSubGrupos(baseDatosUsuario());

            return Json(new {tipomoneda = tipomoneda, Grupos = Grupos, SubGrupos = SubGrupos, producto = producto });
        }

        public JsonResult Detalle_X_Modificar(int id, double precio, string grupo,string subGrupo, string codmon, 
                                    double cantidad, double descuento, int IdCotizacion, double valorDescuento,string fechaEntrega,
                                    string codigoFabrica, string codProd)
        {
            RespuestaModel respuesta = controlDisofi().Detalle_X_Modificar(id,precio,grupo,subGrupo,codmon,cantidad,descuento,baseDatosUsuario(),
                                                                            valorDescuento,fechaEntrega,codigoFabrica,codProd);

            NotadeVentaCabeceraModels NVC = new NotadeVentaCabeceraModels();
            List<NotadeVentaCabeceraModels> NVCL = new List<NotadeVentaCabeceraModels>();
            NotaDeVentaDetalleModels NVD = new NotaDeVentaDetalleModels();
            List<NotaDeVentaDetalleModels> NVDL = new List<NotaDeVentaDetalleModels>();

            NVC.Id = IdCotizacion;
            NVD.Id = IdCotizacion;

            var nvc = controlDisofi().BuscarCotizacion(NVC, baseDatosUsuario());

            if (nvc != null)
            {
                NVCL = nvc;
                ViewBag.mensaje = 0;
            }
            else
            {
                ViewBag.mensaje = 1;
                ViewBag.NVnum = IdCotizacion;
                return Json(new { Mensaje = ViewBag.mensaje, nvNum = ViewBag.NVnum });
            }

            ViewBag.cabecera = NVCL;

            var nvd = controlDisofi().BuscarCotizacionDetalle(NVD, baseDatosUsuario());

            if (nvd != null)
            {
                NVDL = nvd;
            }

            ViewBag.detalle = NVDL;

            #region CotizacionesTodas - Por Usuario
            List<NotadeVentaCabeceraModels> doc = new List<NotadeVentaCabeceraModels>();
            string codigoVendedor = obtenerUsuario().TipoUsuario == Convert.ToInt32(PERFILES.APROBADOR) ? "-1" : codigoVendedorUsuario();
            ViewBag.vencod = codigoVendedor.Trim();
            var docPendientes = controlDisofi().listarCotizacionPendientes(baseDatosUsuario(), EmpresaUsuario().IdEmpresa, codigoVendedor);

            if (docPendientes != null)
            {
                doc = docPendientes;
            }

            foreach (var item in doc)
            {
                for (int i = 0; i < doc.Count; i++)
                {
                    List<SaldosModel> Saldos = new List<SaldosModel>();
                    Saldos = controlDisofi().ObtenerSaldo(doc[i].RutAux, doc[i].CodAux, baseDatosUsuario());
                    if (Saldos != null && Saldos.Count > 0)
                    {
                        doc[i].Saldo = 0;
                        foreach (SaldosModel itemSaldo in Saldos)
                        {
                            doc[i].Saldo = doc[i].Saldo + itemSaldo.Saldo;
                        }
                    }
                }
            }


            List<NotadeVentaCabeceraModels> listaCompletaCot = doc;
            var tipoUsuario = SessionVariables.SESSION_DATOS_USUARIO.TipoUsuario;
            #endregion


            return Json(new { Cabecera = NVCL
                            , Detalle = NVDL
                            , Mensaje = ViewBag.mensaje
                            , NVNumero = ViewBag.NVnum
                            , respuesta
                            , Verificador = true
                            , listaCompletaCot
                            , tipoUsuario}, JsonRequestBehavior.AllowGet);

        }

        public JsonResult EliminarDetalle (int Linea,int IdCotizacion)
        {
            RespuestaModel respuesta = controlDisofi().EliminarDetalle(Linea,IdCotizacion);

            NotadeVentaCabeceraModels NVC = new NotadeVentaCabeceraModels();
            List<NotadeVentaCabeceraModels> NVCL = new List<NotadeVentaCabeceraModels>();
            NotaDeVentaDetalleModels NVD = new NotaDeVentaDetalleModels();
            List<NotaDeVentaDetalleModels> NVDL = new List<NotaDeVentaDetalleModels>();

            NVC.Id = IdCotizacion;
            NVD.Id = IdCotizacion;

            var nvc = controlDisofi().BuscarCotizacion(NVC, baseDatosUsuario());

            if (nvc != null)
            {
                NVCL = nvc;
                ViewBag.mensaje = 0;
            }
            else
            {
                ViewBag.mensaje = 1;
                ViewBag.NVnum = IdCotizacion;
                return Json(new { Mensaje = ViewBag.mensaje, nvNum = ViewBag.NVnum });
            }

            ViewBag.cabecera = NVCL;

            var nvd = controlDisofi().BuscarCotizacionDetalle(NVD, baseDatosUsuario());

            if (nvd != null)
            {
                NVDL = nvd;
            }

            ViewBag.detalle = NVDL;

            return Json(new { Cabecera = NVCL, Detalle = NVDL, Mensaje = ViewBag.mensaje, NVNumero = ViewBag.NVnum, respuesta, Verificador = true }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}