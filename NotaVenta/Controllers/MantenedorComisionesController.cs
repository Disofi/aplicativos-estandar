﻿using NotaVenta.UTIL;
using NotaVenta.UTIL.FilterAttributes;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UTIL.Models;


namespace TexasHub.Controllers
{
    public class MantenedorComisionesController : BaseController
    {
        // GET: MantenedorComisiones
        [Autorizacion(PERFILES.SUPER_ADMINISTRADOR, PERFILES.COMISIONES, PERFILES.REPORTES)]
        public ActionResult MantenedorComisiones()
        {
            return View();
        }

        public JsonResult UploadVentas()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase file = Request.Files;

                    if (file != null && file.Count > 0)
                    {
                        byte[] fileBytes = new byte[Request.ContentLength];
                        var data = Request.InputStream.Read(fileBytes, 0, Convert.ToInt32(Request.ContentLength));

                        ExcelPackage.LicenseContext = LicenseContext.Commercial;

                        //hacer for para recorrer archivo y guardar por linea
                        using (var package = new ExcelPackage(Request.InputStream))
                        {
                            var currentSheet = package.Workbook.Worksheets;
                            var workSheet = currentSheet.First();

                            for (int i = 3; i <= workSheet.Dimension.End.Row; i++)
                            {
                                if (workSheet.Cells[i, 3].Value == null) { break; }
                                VendedorComision VendedorCom = new VendedorComision();

                                try
                                {
                                    VendedorCom.CodigoVendedor = workSheet.Cells[i, 1].Value.ToString();
                                    VendedorCom.Nombre = workSheet.Cells[i, 2].Value.ToString();
                                    VendedorCom.lp1 = double.Parse(workSheet.Cells[i, 3].Value.ToString());
                                    VendedorCom.lp2 = double.Parse(workSheet.Cells[i, 4].Value.ToString());
                                    VendedorCom.lp3 = double.Parse(workSheet.Cells[i, 5].Value.ToString());
                                    VendedorCom.lp4 = double.Parse(workSheet.Cells[i, 6].Value.ToString());

                                    RespuestaModels response = controlDisofi().GrabarComision(VendedorCom);
                                    if (!response.Verificador)
                                    {
                                        return Json(new RespuestaModels { Verificador = false, Mensaje = "Problemas Con Insercion" });
                                    }
                                }
                                catch (Exception e)
                                {
                                    package.Dispose();
                                    string error = e.Message.ToString();
                                    return Json(new RespuestaModels { Verificador = false, Mensaje = "Problemas Con el Archivo" });
                                }
                            }

                        }

                    }
                }
                catch (Exception e)
                {
                    string error = e.Message.ToString();
                    //UTIL.LogUser.agregarLog(error);
                    return Json(new { Verificador = false, Mensaje = "Debe Cargar un Archivo" });
                }
            }
            return Json(new RespuestaModels { Verificador = true, Mensaje = "Archivo Cargado" });
        }




    }
}