﻿var objetosEditable = {};
var caracterSeparadorMiles = ".";
var caracterDecimal = ",";
var parametros = {};
var operacion = 0;



var precioProducto = 0;
var productos = [];
var productosAgregados = [];
var totales = {};
var descuentosAgregadosProducto = [];
var cantidadMaximaLineas = 0;
var PorcentajeAtributoDescuento = 0;



var FormEditable = function () {
    "use strict";
    return {
        //main function
        init: function () {
            handleEditableFormAjaxCall();
            handleEditableFieldConstruct();
        }
    };
}();

var handleEditableFieldConstruct = function () {
    $.fn.editable.defaults.mode = 'inline';
    $.fn.editable.defaults.inputclass = 'form-control input-sm';
    $.fn.editable.defaults.url = '/post';

    objetosEditable = {
        codigoRapido: new EditableClass(document, "txtIngresoCodigoRapido", TiposEditable.Texto, { textoVacio: '', desactivar: true }),
        setTextoCodigoRapido: (texto) => { $('#txtIngresoDescripcionCodigo').html(texto); },
        cantidad: new EditableClass(document, "txtIngresoCantidad", TiposEditable.Texto, { textoVacio: 'Seleccione cantidad', desactivar: false }),
        stock: new EditableClass(document, "txtIngresoStock", TiposEditable.Texto, { textoVacio: '', desactivar: true }),
        unidadDeMedida: new EditableClass(document, "txtIngresoUnidadDeMedida", TiposEditable.Texto, { textoVacio: '', desactivar: true }),
        neto: new EditableClass(document, "txtIngresoNeto", TiposEditable.Texto, { textoVacio: 'Seleccione neto', desactivar: false }),
        valorAdicional: new EditableClass(document, "txtIngresoValorAdicional", TiposEditable.Texto, { textoVacio: 'Seleccione valor adicional', desactivar: false }),
        descuento: new EditableClass(document, "txtIngresoDescuento", TiposEditable.Texto, { textoVacio: 'Seleccione descuento', desactivar: false }),
        subTotal: new EditableClass(document, "txtIngresoSubTotal", TiposEditable.Texto, { textoVacio: '', desactivar: true }),
        total: new EditableClass(document, "txtIngresoTotal", TiposEditable.Texto, { textoVacio: '', desactivar: true }),
        valorAtributoDescuento: new EditableClass(document, "txtIngresoDescuentoAtributo", TiposEditable.Texto, { textoVacio: '', desactivar: true }),
    };
    if (!parametros.EditaPrecioProducto) {
        objetosEditable.neto.desactivar();
    }
};

$(document).ready(function () {
    //convertirFullScreenModal("modalTallaColor");
    $(".ui-sortable").removeClass("ui-sortable");

    parametros = {
        /**/EditaPrecioProducto: ($("#EditaPrecioProducto").val().toLowerCase() === "true"),
        /**/MuestraCondicionVentaCliente: ($("#MuestraCondicionVentaCliente").val().toLowerCase() === "true"),
        /**/MuestraCondicionVentaTodos: ($("#MuestraCondicionVentaTodos").val().toLowerCase() === "true"),
        /**/EditaDescuentoProducto: ($("#EditaDescuentoProducto").val().toLowerCase() === "true"),
        /**/MaximoDescuentoProducto: parseFloat($("#MaximoDescuentoProducto").val()),
        /**/CantidadDescuentosProducto: parseInt($("#CantidadDescuentosProducto").val()),
        /**/MuestraStockProducto: ($("#MuestraStockProducto").val().toLowerCase() === "true"),
        /**/StockProductoEsMasivo: ($("#StockProductoEsMasivo").val().toLowerCase() === "true"),
        /**/StockProductoEsBodega: ($("#StockProductoEsBodega").val().toLowerCase() === "true"),
        /**/StockProductoCodigoBodega: $("#StockProductoCodigoBodega").val(),
        /**/StockProductoCodigoBodegaAdicional: $("#StockProductoCodigoBodegaAdicional").val(),
        /**/ControlaStockProducto: ($("#ControlaStockProducto").val().toLowerCase() === "true"),
        /**/ManejaTallaColor: ($("#ManejaTallaColor").val().toLowerCase() === "true"),
        /**/ManejaDescuentoTotalDocumento: ($("#ManejaDescuentoTotalDocumento").val().toLowerCase() === "true"),
        /**/CantidadDescuentosTotalDocumento: parseInt($("#CantidadDescuentosTotalDocumento").val()),
        /**/ManejaLineaCreditoVendedor: ($("#ManejaLineaCreditoVendedor").val().toLowerCase() === "true"),
        /**/ManejaLineaCreditoAprobador: ($("#ManejaLineaCreditoAprobador").val().toLowerCase() === "true"),
        ManejaCanalVenta: ($("#ManejaCanalVenta").val().toLowerCase() === "true"),
        PermiteModificacionCondicionVenta: ($("#PermiteModificacionCondicionVenta").val().toLowerCase() === "true"),
        /**/AtributoSoftlandDescuentoCliente: $("#AtributoSoftlandDescuentoCliente").val(),
        /**/PermiteCrearDireccion: ($("#PermiteCrearDireccion").val().toLowerCase() === "true"),
        /**/MuestraUnidadMedidaProducto: ($("#MuestraUnidadMedidaProducto").val().toLowerCase() === "true"),
    };

    PorcentajeAtributoDescuento = parseFloat($("#PorcentajeAtributoDescuento").val());

    //console.log(parametros);

    precioProducto = 0;
    productos = [];
    productosAgregados = [];
    descuentosAgregadosProducto = [];
    cantidadMaximaLineas = 20;

    if (parametros.ManejaDescuentoTotalDocumento) {
        for (i = 1; i <= parametros.CantidadDescuentosTotalDocumento; i++) {
            $("#trDescuento" + i).fadeIn();
            $("#txtDescuentoTotal" + i).change(function () {
                CalcularProductosAgregados();
            });
        }
    }

    $("#btnAgregarDireccionDespacho").click(function () {
        $("#aModalAgregarDireccionDespacho").click();
    });
    $("#modalAgregarDireccionDespachoAgregar").click(function () {
        var codigoCliente = $("#modalAgregarDireccionDespachoCodigoCliente").html();
        var direccion = $("#modalAgregarDireccionDespachoDireccion").val();
        var codigoCiudad = $("#modalAgregarDireccionDespachoCiudad").val();
        var textoCiudad = $("#modalAgregarDireccionDespachoCiudad option:selected").text();
        var codigoComuna = $("#modalAgregarDireccionDespachoComuna").val();

        var data = {
            CodAxD: codigoCliente,
            DirDch: direccion.trim(),
            CiuDch: codigoCiudad.trim(),
            ComDch: codigoComuna.trim(),
            NomDch: textoCiudad.trim()
        };


        if (data.DirDch !== "" && data.CiuDch !== "" && data.DirDch !== "") {
            $.ajax({
                url: "AgregarDireccionDespachoCotizacion",
                data: { direccion: data },
                type: "POST",
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response.Verificador) {
                        abrirInformacion("Agregar Direccion Despacho", response.Mensaje);
                        $.ajax({
                            url: "BuscarDireccionDespachoCotizacion",
                            data: { CodAux: data.CodAxD },
                            type: "POST",
                            dataType: "json",
                            async: true,
                            success: function (response) {
                                $("#cbxDireccion").find('option').remove().end();

                                $("#cbxDireccion").append('<option selected value="-1"> -- Seleccione Dirección de Despacho -- </option>');
                                $.each(response, function (index, item) {
                                    $("#cbxDireccion").append('<option selected value="' + item.NomDch + '">' + item.DirDch + ", " + (item.ComDes == null ? "Sin Comuna" : item.ComDes) + ", " + (item.CiuDes == null ? "Sin Ciudad" : item.CiuDes) + '</option>');
                                });

                                //$("#cbxDireccion").val(data.NomDch);

                                //$("#modalAgregarDireccionDespachoCodigoCliente").html("");
                                $("#modalAgregarDireccionDespachoDireccion").val("");
                                $("#modalAgregarDireccionDespachoCiudad").val("");
                                $("#modalAgregarDireccionDespachoComuna").val("");

                                $("#modalAgregarDireccionDespachoCerrar").click(function () {
                                    location.reload();
                                });
                            },
                            error: function (response) {
                            },
                            failure: function (response) {
                                alert(response.responseText);
                            }
                        });
                    }
                    else {
                        abrirError("Agregar Direccion Despacho", response.Mensaje);
                    }
                },
                error: function (response) {
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }
        else {
            abrirError("Agregar Direccion Despacho", "Favor complete los campos requeridos");
        }
    });

    FormEditable.init();

    cbxlistaChange();
    crearControlDescuentos();

    $("#modalModificarCantidadModificar").click(function () {
        var contador = $("#modalModificarCantidadContador").val();
        var codProd = $("#modalModificarCantidadCodigoProducto").text();
        var talla = $("#modalModificarCantidadTalla").text();
        var color = $("#modalModificarCantidadColor").text();
        var cantidad = $("#modalModificarCantidadCantidad").val();

        productosAgregados.find(m => m.Codigo === codProd && m.Talla === talla && m.Color === color).Cantidad = cantidad;

        $("#lblCantidad_" + contador).html(cantidad);
        $("#lblCantidad_" + contador).attr("onclick", 'abrirModalModificarCantidad(' + contador + ', \'' + codProd + '\',\'' + talla + '\',\'' + color + '\',\'' + cantidad + '\')');
        CalcularProductosAgregados();

        $("#modalModificarCantidadCerrar").click();

        /*

        var productoAgregado = {
            Codigo: codigo,
            Talla: talla,
            Color: color,
            contador: contador,
            PrecioUnitario: preciounitario,
            PrecioUnitarioConDescuento: precioUnitarioConDescuento,
            Cantidad: cantidad,
            Stock: stock,
            UnidadMedida: medida,
            Descuentos: descuento,
            DescuentoAtributo: PorcentajeAtributoDescuento,
            SubTotal: subTotal,
            SubTotalConDescuento: subTotalConDescuento,
            Total: total
        };
         */
    });
    $("#modalTallaColorBodegas").change(function () {
        var bodegaNombre = $("#modalTallaColorBodegas").val();
        var indexBodega = 0;
        $("#modalTallaColorControlesTable thead > tr > th").each(function (index) {
            if ($(this).text() === bodegaNombre) {
                indexBodega = index - 2;
                jQuery.moverColumna("#modalTallaColorControlesTable", indexBodega, 4);
            }
        });
    });
    $("#modalTallaColorAgregarTallaColor").click(function () {
        var valores = [];

        var filas = $('#modalTallaColorControlesTable tbody tr');
        for (i = 0; i < filas.length; i++) {
            var valorCodigoProducto = $($(filas[i]).children("td")[0]).html();
            var valorTalla = $($(filas[i]).children("td")[1]).html();
            var valorColor = $($(filas[i]).children("td")[2]).html();
            var valorCantidad = $($(filas[i]).children("td")[3]).children("input").val();
            var valorStock = 0;

            if (parametros.MuestraStockProducto) {
                valorStock = $($(filas[i]).children("td")[4]).html();
            }

            if (parseFloat(valorCantidad) > 0) {
                valores.push({
                    codigoProducto: valorCodigoProducto,
                    talla: valorTalla,
                    color: valorColor,
                    stock: parseFloat(valorStock),
                    cantidad: parseFloat(valorCantidad)
                });
            }
        }

        if (parametros.ControlaStockProducto) {
            for (i = 0; i < valores.length; i++) {
                if (valores[i].stock < valores[i].cantidad) {
                    abrirError("Error Cantidad VS Stock", "cantidad no puede superar al stock del producto (Producto: " +
                        valores[i].codigoProducto +
                        ", Talla: " +
                        valores[i].talla +
                        ", Color: " +
                        valores[i].color +
                        ")");
                    return;
                }
            }
        }
        var descuentosTemp = descuentosAgregadosProducto;
        for (z = 0; z < valores.length; z++) {
            objetosEditable.codigoRapido.setValor(valores[z].codigoProducto);
            objetosEditable.descripcionCodigo.setValor(verTallaColorDescripcion);
            objetosEditable.cantidad.setValor(valores[z].cantidad);
            if (parametros.MuestraUnidadMedidaProducto) {
                objetosEditable.unidadDeMedida.setValor(verTallaColorMedida);
            }
            else {
                objetosEditable.unidadDeMedida.setValor("");
            }
            objetosEditable.neto.setValor(verTallaColorPrecioUnitario);

            verTallaColorTalla = valores[z].talla;
            verTallaColorColor = valores[z].color;
            //descuentosAgregadosProducto = [{ Porcentaje: verTallaColorDescuento }];
            descuentosAgregadosProducto = descuentosTemp;

            CalcularFila(
                objetosEditable.neto.getValor(),
                objetosEditable.valorAdicional.getValor(),
                objetosEditable.cantidad.getValor(),
                descuentosAgregadosProducto,
                PorcentajeAtributoDescuento
            );

            addRow();
        }

        $("#modalTallaColorCerrar").click();
    });
    $("#modalTallaColor").on("hidden.bs.modal", function () {
        //limpiarCamposLinea();
    });










    /*------------------------ A G R E G A R   N U E V O   P R O D U C T O ------------------------*/

    $("#btnAgregarProductoNuevo").click(() => {
        $("#modalAgregarProductoNuevoCodigo").val("");
        $("#modalAgregarProductoNuevoDescripcion").val("");
        //$("#modalAgregarProductoNuevoGrupo").val("");
        //$("#modalAgregarProductoNuevoSubGrupo").val("");
        $("#modalAgregarProductoNuevoValorNeto").val("");
        $("#modalAgregarProductoNuevoCodigo").focus();
        $("#aModalAgregarProductoNuevo").click();
    });
    $("#modalAgregarProductoNuevoAgregar").click(() => {
        activarLoadingBoton("modalAgregarProductoNuevoAgregar");
        var data = {
            codigo: $("#modalAgregarProductoNuevoCodigo").val(),
            descripcion: $("#modalAgregarProductoNuevoDescripcion").val(),
            grupo: $("#modalAgregarProductoNuevoGrupo").val(),
            subGrupo: $("#modalAgregarProductoNuevoSubGrupo").val(),
            //valorNeto: $("#modalAgregarProductoNuevoValorNeto").val().replace(",", "."),
            valorNeto: $("#modalAgregarProductoNuevoPrecioVenta").val().replace(",", "."),
            tipoMoneda: $("#modalAgregarProductoNuevoTipoMoneda").val(),
            codigoFabrica: $("#modalCodigoFabrica").val()
        };

        if (data.codigo.length > 0 && data.codigo.length <= 20) {
            if (jQuery.grep(productosAgregados, function (value) { return value.Codigo === data.codigo; }).length === 0) {
                //var _url = $("#modalAgregarProductoNuevoAgregar").val();
                //no es necesario traer la url del controller de esa forma ok
                //gracias
                $.ajax({
                    url: "/venta/ObtieneProductosPorListaPrecio",
                    data: { ListaPrecio: "-1" },
                    type: "POST",
                    dataType: "json",
                    async: true,
                    success: (response) => {
                        if (response.filter(m => m.CodProd === data.codigo).length === 0) {
                            if (data.descripcion.length > 0 && data.codigo.length <= 50) {
                                if (data.valorNeto != "") {
                                    if (!isNaN(data.valorNeto)) {
                                        var moneda = JSON.parse($("#hfMonedas").val()).find(m => m.CodigoMoneda === data.tipoMoneda);

                                        if (((data.valorNeto.length - data.valorNeto.indexOf(".")) - 1) <= moneda.DecimalesPrecioMoneda || data.valorNeto.indexOf(".") === -1) {
                                            objetosEditable.codigoRapido.setValor(data.codigo);
                                            objetosEditable.descripcionCodigo.setValor(data.descripcion);
                                            objetosEditable.setTextoCodigoRapido(data.codigo + " - " + data.descripcion);
                                            objetosEditable.cantidad.setValor(1);
                                            if (parametros.MuestraUnidadMedidaProducto) {
                                                objetosEditable.unidadDeMedida.setValor("");
                                                //objetosEditable.unidadDeMedida.setValor(verTallaColorMedida);
                                            }
                                            else {
                                                objetosEditable.unidadDeMedida.setValor("");
                                            }
                                            objetosEditable.neto.setValor(data.valorNeto);

                                            CalcularFila(
                                                objetosEditable.neto.getValor(),
                                                objetosEditable.valorAdicional.getValor(),
                                                objetosEditable.cantidad.getValor(),
                                                descuentosAgregadosProducto,
                                                PorcentajeAtributoDescuento
                                            );

                                            //addRow();
                                            esProductoNuevo = true;
                                            $("#modalAgregarProductoNuevoCodigo").val("");
                                            $("#modalAgregarProductoNuevoDescripcion").val("");
                                            //$("#modalAgregarProductoNuevoGrupo").val("");
                                            //$("#modalAgregarProductoNuevoSubGrupo").val("");
                                            $("#modalAgregarProductoNuevoValorNeto").val("");
                                            //$("#modalAgregarProductoNuevoTipoMoneda").val("");


                                            productoNuevoCodigoGrupo = data.grupo;
                                            productoNuevoDescripcion = data.descripcion;
                                            productoNuevoCodigoSubGrupo = data.subGrupo;
                                            productoNuevoCodigoMoneda = data.tipoMoneda;
                                            productoNuevoCodigoFabrica = data.codigoFabrica;

                                            desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
                                            $("#modalAgregarProductoNuevoCerrar").click();
                                        }
                                        else {
                                            var mensaje = "";
                                            if (moneda.DecimalesPrecioMoneda > 0) {
                                                mensaje = "La cantidad de decimales permitidos es de " + moneda.DecimalesPrecioMoneda + " para el valor neto";
                                            }
                                            else {
                                                mensaje = "Valor neto no permite decimales para la moneda " + moneda.DescripcionMoneda;
                                            }
                                            desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
                                            abrirError("Valor Neto", mensaje);
                                        }
                                    }
                                    else {
                                        desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
                                        abrirError("Valor Neto", "Valor neto debe ser un numero");
                                    }
                                }
                                else {
                                    desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
                                    abrirError("Valor Neto", "Valor neto es un campo obligatorio");
                                }
                            }
                            else {
                                desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
                                abrirError("Descripcion de Producto", "Descripcion de producto debe contener entre 1 y 50 caracteres");
                            }
                        }
                        else {
                            desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
                            abrirError("Producto", "Codigo de producto ya existe en el sistema");
                        }
                    }
                });
            }
            else {
                desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
                abrirError("Codigo de Producto", "Codigo de producto ya fue ingresado");
            }
        }
        else {
            desactivarLoadingBoton("modalAgregarProductoNuevoAgregar");
            abrirError("Codigo de Producto", "Codigo de producto debe contener entre 1 y 20 caracteres");
        }
    });

    /*-------------------- F I N   A G R E G A R   N U E V O   P R O D U C T O --------------------*/

    $("#fileUpload").on("change", function () {
        var maxBytes = 20000000;
        if ($("#fileUpload")[0].files.length > 4) {
            abrirError("Error usuario", "Puede adjuntar hasta 4 archivos");
            $("#fileUpload").val("");
            operacion = 0;
        } else {
            var fileUpload = $("#fileUpload").get(0);
            var files = fileUpload.files;

            for (var i = 0; i < files.length; i++) {
                var separador;
                separador = files[i].name.split('.');
                if (separador[1] == 'xlsm') {
                    abrirError("Error usuario", "Archivo xlsm no puede ser procesado");
                    $("#fileUpload").val("");
                    operacion = 0;
                    break;
                } else {
                    if (files[i].size > maxBytes) {
                        abrirError("Error usuario", "Archivo no puede superar los 20 MB - " + files[i].name);
                        $("#fileUpload").val("");
                        opcion = 0;
                        break;
                    } else {
                        operacion = 1;
                    }
                }
            }
        }
    });
});
/*------------------------ A G R E G A R   N U E V O   P R O D U C T O ------------------------*/

var productoNuevoCodigoGrupo;
var productoNuevoDescripcion;
var productoNuevoCodigoSubGrupo;
var productoNuevoCodigoMoneda;
var productoNuevoCodigoFabrica;

/*-------------------- F I N   A G R E G A R   N U E V O   P R O D U C T O --------------------*/

jQuery.moverColumna = function (table, from, to) {
    var rows = jQuery('tr', table);
    var cols;
    rows.each(function () {
        cols = jQuery(this).children('th, td');
        cols.eq(from).detach().insertBefore(cols.eq(to));
    });
}
var handleEditableFormAjaxCall = function () {
    $.mockjax({
        url: '/post',
        response: function (settings) {
            if (settings.data.name === "txtIngresoDescripcionCodigo") {
                var producto = productos.find(m => m.busqueda === settings.data.value);

                if (jQuery.grep(productosAgregados, function (value) { return value.Codigo === producto.CodProd; }).length > 0) {
                    this.status = 500;
                    this.statusText = "Producto ya fue ingresado";
                }
                else {
                    if (!parametros.EditaPrecioProducto) {
                        if (producto.PrecioVta === 0) {
                            this.status = 500;
                            this.statusText = "Producto no tiene precio asociado y este no se puede editar";
                            limpiarCamposLinea();
                            return;
                        }
                    }

                    llenarCamposSeleccionProducto(producto);
                }
            }
            if (settings.data.name === "txtIngresoCantidad") {
                if (/^([0-9])*$/.test(settings.data.value)) {
                    if (parseFloat(settings.data.value) > 0) {
                        if (parametros.ControlaStockProducto && parametros.MuestraStockProducto) {
                            var stock = parseFloat(objetosEditable.stock.getValor());
                            var cantidad = parseFloat(settings.data.value);

                            if (stock < cantidad) {
                                this.status = 500;
                                this.statusText = "cantidad no puede superar al stock del producto";
                                return;
                            }
                        }

                        CalcularFila(
                            objetosEditable.neto.getValor(),
                            objetosEditable.valorAdicional.getValor(),
                            settings.data.value,
                            descuentosAgregadosProducto,
                            PorcentajeAtributoDescuento
                        );
                    }
                    else {
                        this.status = 500;
                        this.statusText = "Debe ser mayor a 0";
                    }
                }
                else {
                    this.status = 500;
                    this.statusText = "Solo se admiten numeros";
                }
            }
            if (settings.data.name === "txtIngresoNeto") {
                if (/^([0-9])*$/.test(settings.data.value)) {
                    if (parseFloat(settings.data.value) > 0) {
                        CalcularFila(
                            settings.data.value,
                            objetosEditable.valorAdicional.getValor(),
                            objetosEditable.cantidad.getValor(),
                            descuentosAgregadosProducto,
                            PorcentajeAtributoDescuento
                        );
                    }
                    else {
                        this.status = 500;
                        this.statusText = "Debe ser mayor a 0";
                    }
                }
                else {
                    this.status = 500;
                    this.statusText = "Solo se admiten numeros";
                }
            }
            if (settings.data.name === "txtIngresoValorAdicional") {
                if (/^([0-9])*$/.test(settings.data.value)) {
                    if (parseFloat(settings.data.value) > 0) {
                        CalcularFila(
                            objetosEditable.neto.getValor(),
                            settings.data.value,
                            objetosEditable.cantidad.getValor(),
                            descuentosAgregadosProducto,
                            PorcentajeAtributoDescuento
                        );
                    }
                    else {
                        this.status = 500;
                        this.statusText = "Debe ser mayor a 0";
                    }
                }
                else {
                    this.status = 500;
                    this.statusText = "Solo se admiten numeros";
                }
            }
            if (settings.data.name === "txtIngresoDescuento") {
                if (/^([0-9])*$/.test(settings.data.value)) {
                    if (parseFloat(settings.data.value) <= 100 && parseFloat(settings.data.value) >= 0) {
                        if (parametros.MaximoDescuentoProducto === 0 || parametros.MaximoDescuentoProducto >= parseFloat(settings.data.value)) {

                            descuentosAgregadosProducto = [];
                            descuentosAgregadosProducto.push({
                                Porcentaje: parseFloat(settings.data.value)
                            });
                            CalcularFila(
                                objetosEditable.neto.getValor(),
                                objetosEditable.valorAdicional.getValor(),
                                objetosEditable.cantidad.getValor(),
                                descuentosAgregadosProducto,
                                PorcentajeAtributoDescuento
                            );
                        }
                        else {
                            this.status = 500;
                            this.statusText = "Descuento no pueden superar el maximo permitido (" + parametros.MaximoDescuentoProducto + "%)";
                        }
                    }
                    else {
                        this.status = 500;
                        this.statusText = "El valor debe estar entre 0 y 100 (%)";
                    }
                }
                else {
                    this.status = 500;
                    this.statusText = "Solo se admiten numeros";
                }
            }
        }
    });
};

function llenarCamposSeleccionProducto(producto) {
    objetosEditable.codigoRapido.setValor(producto.CodProd);
    objetosEditable.neto.setValor(producto.PrecioVta);
    if (parametros.MuestraUnidadMedidaProducto) {
        objetosEditable.unidadDeMedida.setValor(producto.codumed);
    }
    else {
        objetosEditable.unidadDeMedida.setValor("");
    }
    if (parametros.MuestraStockProducto) {
        objetosEditable.stock.setValor(producto.Stock);
    }
    else {
        objetosEditable.stock.setValor(0);
    }
    esProductoNuevo = false;

    CalcularFila(
        producto.PrecioVta,
        objetosEditable.valorAdicional.getValor(),
        objetosEditable.cantidad.getValor(),
        descuentosAgregadosProducto,
        PorcentajeAtributoDescuento
    );
}

function crearControlDescuentos() {
    descuentosAgregadosProducto = [];
    if (!parametros.EditaDescuentoProducto) {
        objetosEditable.descuento.setValor("0");
        objetosEditable.descuento.desactivar();
    }
    else if (parametros.CantidadDescuentosProducto === 0) {
        objetosEditable.descuento.setValor("0");
        objetosEditable.descuento.desactivar();
    }
    else if (parametros.CantidadDescuentosProducto === 1) {
        objetosEditable.descuento.setValor("0");
    }
    else {
        $('#txtIngresoDescuento').fadeOut();
        $('#txtIngresoDescuentoModal').fadeIn();
        $('#txtIngresoDescuentoModal').addClass("editable editable-click editable-empty");

        var descuentosAppend = "";
        descuentosAppend = descuentosAppend
            + '<div class="table-responsive">'
            + '<table id="table" class="table">'
            + '<thead></thead>'
            + '<tbody>';

        for (i = 1; i <= parametros.CantidadDescuentosProducto; i++) {
            descuentosAgregadosProducto.push({
                Porcentaje: 0
            });
            descuentosAppend = descuentosAppend
                + '<tr>'
                + '<td>'
                + 'Descuento N°' + i
                + '</td>'
                + '<td style="text-align: right">'
                + '<input id="descuento_' + i + '" type="text" value="0" class="form-control" />'
                + '</td>'
                + '</tr>';
        }
        descuentosAppend = descuentosAppend
            + '</tbody>'
            + '</table>';

        $("#modalDescuentosControles").html(descuentosAppend);

        $('#txtIngresoDescuentoModal').click(function () {
            $("#aModalDescuentos").click();
        });
        $('#modalDescuentosAgregarDescuentos').click(function () {
            var descuentosTemp = [];
            var hayErrorMaximoPermitido = false;
            var hayErrorMaximo = false;
            var hayErrorMinimo = false;

            for (i = 1; i <= parametros.CantidadDescuentosProducto; i++) {
                var descuento = $("#descuento_" + i).val();
                descuento = parseFloat(descuento);

                if (descuento > 100) {
                    abrirError("Error de descuentos", "Los descuentos no pueden superar el 100%");
                    return;
                }
                if (descuento < 0) {
                    abrirError("Error de descuentos", "Los descuentos no pueden ser inferior a 0%");
                    return;
                }
                if (parametros.MaximoDescuentoProducto > 0 && parametros.MaximoDescuentoProducto < descuento) {
                    abrirError("Error de descuentos", "Los descuentos no pueden superar el maximo permitido (" + parametros.MaximoDescuentoProducto + "%)");
                    return;
                }

                descuentosTemp.push({
                    Porcentaje: descuento
                });
            }

            descuentosAgregadosProducto = [];
            descuentosAgregadosProducto = descuentosTemp;

            CalcularFila(
                objetosEditable.neto.getValor(),
                objetosEditable.valorAdicional.getValor(),
                objetosEditable.cantidad.getValor(),
                descuentosAgregadosProducto,
                PorcentajeAtributoDescuento
            );
            $("#modalDescuentosCerrar").click();
        });
    }
}

function cbxlistaChange() {
    var _url = $("#urlObtieneProductosPorListaPrecio").val();

    $.ajax({
        url: _url,
        data: { ListaPrecio: $("#cbxlista").val() },
        type: "POST",
        dataType: "json",
        async: false,
        success: function (response) {
            productos = response;

            productos = productos.map(function (x) {
                x.busqueda = x.CodProd + " - " + x.DesProd;
                x.PrecioVta = Math.round(x.PrecioVta);
                return x
            });

            objetosEditable.descripcionCodigo = new EditableClass(document,
                "txtIngresoDescripcionCodigo",
                TiposEditable.Select2,
                {
                    textoVacio: "Busqueda de Producto",
                    fuenteDatos: productos.map(m => { return { id: m.busqueda, text: m.busqueda } }),
                    select2: {
                        width: '300',
                        placeholder: 'Seleccione Productos',
                        allowClear: true
                    }
                });
        },
        error: function (response) {
            //alert("Este Producto no se encuentra en esta lista de Precio");
        },
        failure: function (response) {
            alert(response.responseText);
        }
    });
}

function valida(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

function eliminarFilas(filaDelete) {
    var subtotal = $('#lblSubTotal_' + filaDelete).text();

    fila = $('#generaTabla tr[id=id_' + filaDelete + ']');

    productosAgregados = jQuery.grep(productosAgregados, function (value) {
        return value.contador !== filaDelete;
    });

    CalcularProductosAgregados();

    fila.remove();
}

function CalcularFila(precioUnitario, valorAdicional, cantidad, descuentos, porcentajeAtributoDescuento) {
    if (precioUnitario === undefined || precioUnitario === null || precioUnitario === "") { precioUnitario = 0; }
    if (cantidad === undefined || cantidad === null || cantidad === "") { cantidad = 0; }
    if (descuentos === undefined || descuentos === null || descuentos === "") { descuentos = []; }
    if (porcentajeAtributoDescuento === undefined || porcentajeAtributoDescuento === null || porcentajeAtributoDescuento === "") { porcentajeAtributoDescuento = 0; }

    precioUnitario = parseInt(precioUnitario);
    valorAdicional = parseInt(valorAdicional);
    cantidad = parseInt(cantidad);

    var url = $("#urlCalcularFila").val();

    var dataPost = { precioUnitario: precioUnitario, valorAdicional: [{ ValorAdicional: valorAdicional }], cantidad: cantidad, descuentos: descuentos, porcentajeAtributoDescuento: porcentajeAtributoDescuento };

    $.ajax({
        url: url,
        type: "POST",
        data: dataPost,
        success: function (result) {
            var subTotal = result.SubTotal;
            var total = result.Total;
            descuentosAgregadosProducto = result.Descuentos;
            var valorDescuentoAtributo = result.ValorDescuentoAtributo;

            $("#hfSubTotalConDescuento").val(result.SubTotalConDescuento);
            $("#hfPrecioUnitarioConDescuento").val(result.PrecioUnitarioConDescuento);

            objetosEditable.valorAtributoDescuento.setValor(parseInt(valorDescuentoAtributo));
            objetosEditable.subTotal.setValor(parseInt(subTotal));
            objetosEditable.total.setValor(parseInt(total));
        },
        error: function (a, b, c) {
            console.log(a, b, c);
        },
        async: false
    });
}


function CalcularProductosAgregados() {
    if (productosAgregados.length > 0) {
        var url = $("#urlCalcularProductosAgregados").val();
        var dataPost = productosAgregados;
        var descuentos = obtieneDescuentosTotales();
        var porcentajeAtributoDescuento = PorcentajeAtributoDescuento;

        $.ajax({
            url: url,
            type: "POST",
            data: { productos: dataPost, descuentos: descuentos, porcentajeAtributoDescuento: porcentajeAtributoDescuento },
            success: function (result) {
                var impuesto = result.Impuesto;
                descuentos = result.descuentos;
                var total = result.Total;

                totales.subTotal = result.SubTotal;
                totales.subTotalConDescuento = result.SubTotalConDescuento;
                totales.impuesto = impuesto;
                totales.total = total;
                totales.descuentos = descuentos;

                for (i = 0; i < result.Productos.length; i++) {
                    var contador = result.Productos[i].ContadorFront;

                    $("#lblSubTotal_" + contador).html(agregarSeparadorMiles(result.Productos[i].SubTotal, caracterSeparadorMiles, caracterDecimal));
                    $("#lblTotal_" + contador).html(agregarSeparadorMiles(result.Productos[i].Total, caracterSeparadorMiles, caracterDecimal));


                }

                $('#lbtotal').text(agregarSeparadorMiles(result.SubTotalConDescuento, caracterSeparadorMiles, caracterDecimal));
                $('#lbimpuesto').text(agregarSeparadorMiles(impuesto, caracterSeparadorMiles, caracterDecimal));
                $('#lbtotalfinal').text(agregarSeparadorMiles(total, caracterSeparadorMiles, caracterDecimal));
            },
            error: function (a, b, c) {
                console.log(a, b, c);
            },
            async: false
        });
    }
    else {
        $('#lbtotal').text(0);
        $('#lbimpuesto').text(0);
        $('#lbtotalfinal').text(0);
    }
}

function obtieneDescuentosTotales() {
    var descuentos = [];
    if (parametros.ManejaDescuentoTotalDocumento) {
        for (i = 1; i <= parametros.CantidadDescuentosTotalDocumento; i++) {
            descuentos.push({
                Porcentaje: parseInt($("#txtDescuentoTotal" + i).val())
            });
        }
    }

    return descuentos;
}
var esProductoNuevo = false;
function addRow() {
    var validador = 0;

    var codigo = objetosEditable.codigoRapido.getValor();
    var descripcion = objetosEditable.descripcionCodigo.getValor();
    var cantidad = objetosEditable.cantidad.getValor();
    var stock = objetosEditable.stock.getValor();
    var medida = objetosEditable.unidadDeMedida.getValor();
    var preciounitario = objetosEditable.neto.getValor();
    var valorAdicional = objetosEditable.valorAdicional.getValor();
    var precioUnitarioConDescuento = $("#hfPrecioUnitarioConDescuento").val();
    var porcentajeAtributoDescuento = objetosEditable.valorAtributoDescuento.getValor();
    var subTotal = objetosEditable.subTotal.getValor();
    var subTotalConDescuento = $("#hfSubTotalConDescuento").val();
    var descuento = descuentosAgregadosProducto;
    var total = objetosEditable.total.getValor();
    var talla = verTallaColorTalla;
    var color = verTallaColorColor;
    //var fecha_Entrega = $("#fechaEntrega").val();
    var fechaEntrega = $("#fechaEntrega").val().split('-');
    var fechaEntregaFin = fechaEntrega[2] + '-' + fechaEntrega[1] + '-' + fechaEntrega[0];


    preciounitario = parseInt(preciounitario) + parseInt(valorAdicional);

    if (talla === null || talla === undefined || talla === "") {
        talla = "";
    }
    if (color === null || color === undefined || color === "") {
        color = "";
    }

    if (codigo === null || codigo === undefined || codigo === "" ||
        cantidad === null || cantidad === undefined || cantidad === "" || ("" + cantidad) === "0" ||
        preciounitario === null || preciounitario === undefined || preciounitario === "" || ("" + preciounitario) === "0" ||
        descuento === null || descuento === undefined || descuento === "" || ("" + descuento) === "0") {

        abrirError("Error datos producto", "Favor, complete todos los campos necesarios para agregar producto");
    }
    else {
        var contador = 0;
        for (i = 0; i < $("#generaTabla tr").length; i++) {
            item = $("#generaTabla tr")[i];
            contador = parseInt(item.id.substring(3, 90)) >= contador ? parseInt(item.id.substring(3, 90)) + 1 : contador;
        }

        var texto_insertar =
            '<tr id="id_' + contador + '" class="thproductolist' + contador + '">'
            + '<td><span id="lblCodigo_' + contador + '">' + codigo + '</td>'
            + '<td><span id="lblDescripcion_' + contador + '">' + descripcion + '</td>'
            + '<td style="' + (parametros.ManejaTallaColor ? '' : 'display: none') + '"><span id="lblTalla_' + contador + '">' + talla + '</td>'
            + '<td style="' + (parametros.ManejaTallaColor ? '' : 'display: none') + '"><span id="lblColor_' + contador + '">' + color + '</td>'
            + '<td style="text-align: right"><button class="btn btn-link" onclick="abrirModalModificarCantidad(' + contador + ', \'' + codigo + '\',\'' + talla + '\',\'' + color + '\',\'' + cantidad + '\')" id="lblCantidad_' + contador + '">' + agregarSeparadorMiles(cantidad, caracterSeparadorMiles, caracterDecimal) + '</button></td>'
            + '<td style="' + (parametros.MuestraUnidadMedidaProducto ? '' : 'display: none') + '"><span id="lblMedida_' + contador + '">' + medida + '</td>'
            + '<td style="text-align: right"><span id="lblPrecioUnitario_' + contador + '">' + agregarSeparadorMiles(preciounitario, caracterSeparadorMiles, caracterDecimal) + '</td>'
            + '<td style="text-align: right"><span id="lblSubTotal_' + contador + '">' + agregarSeparadorMiles(subTotal, caracterSeparadorMiles, caracterDecimal) + '</td>'
            + '<td style="' + (PorcentajeAtributoDescuento > 0 ? '' : 'display: none') + '"><span id="lblAtributoDescuento_' + contador + '">' + agregarSeparadorMiles(porcentajeAtributoDescuento, caracterSeparadorMiles, caracterDecimal) + '</td>'
            + (descuento.length === 0 ? '<td style="text-align: right"><span id="lblDescuento_' + contador + '">0</td>' :
                descuento.length === 1 ?
                    '<td style="text-align: right"><span id="lblDescuento_' + contador + '">' + agregarSeparadorMiles(descuento[0].Porcentaje, caracterSeparadorMiles, caracterDecimal) + '%' + '</td>' :
                    '<td><a class="editable editable-click editable-empty" id="verDescuentos_' + contador + '" onclick="verDescuentos(' + contador + ')">Ver Descuentos</a></td>')
            + '<td style="text-align: right"><span id="lblTotal_' + contador + '">' + agregarSeparadorMiles(total, caracterSeparadorMiles, caracterDecimal) + '</td>'
            + '<td><span id="fechaEntregaTh_' + contador + '"> ' +  fechaEntregaFin + ' </td>'
            + '<td><a id="thproductolist' + contador + '" href="#divCodigo" onclick="eliminarFilas(' + contador + ');"><img src="../Content/Image/delete.png" /></a></td>'
            + '</tr>';

        var nuevo_campo = $(texto_insertar);
        $("#generaTabla").append(nuevo_campo);
        var _esProductoNuevo = esProductoNuevo;

        limpiarCamposLinea();

        var productoAgregado = {
            ContadorFront: contador,
            Codigo: codigo,
            CodigoGrupo: ((_esProductoNuevo) ? productoNuevoCodigoGrupo : null),
            Descripcion: ((_esProductoNuevo) ? productoNuevoDescripcion : null),
            CodigoSubGrupo: ((_esProductoNuevo) ? productoNuevoCodigoSubGrupo : null),
            CodigoMoneda: ((_esProductoNuevo) ? productoNuevoCodigoMoneda : null),
            Talla: talla,
            Color: color,
            contador: contador,
            PrecioUnitario: preciounitario,
            ValorAdicional: [{ ValorAdicional: valorAdicional }],
            PrecioUnitarioConDescuento: precioUnitarioConDescuento,
            Cantidad: cantidad,
            Stock: stock,
            UnidadMedida: medida,
            Descuentos: descuento,
            DescuentoAtributo: PorcentajeAtributoDescuento,
            SubTotal: subTotal,
            SubTotalConDescuento: subTotalConDescuento,
            Total: total,
            EsProductoNuevo: _esProductoNuevo,
            FechaEntrega: fechaEntregaFin,
            codigoFabrica: ((_esProductoNuevo) ? productoNuevoCodigoFabrica : null)
        };

        productosAgregados.push(productoAgregado);

        CalcularProductosAgregados();
    }
}

function abrirModalModificarCantidad(contador, codProd, talla, color, cantidad) {
    $("#modalModificarCantidadContador").val(contador);
    $("#modalModificarCantidadCodigoProducto").text(codProd);
    $("#modalModificarCantidadTalla").text(talla);
    $("#modalModificarCantidadColor").text(color);
    $("#modalModificarCantidadCantidad").val(cantidad);

    $("tr[name=modalModificarCantidadFilaTallaColor]").attr("style", "display: none");
    if (parametros.ManejaTallaColor) {
        $("tr[name=modalModificarCantidadFilaTallaColor]").removeAttr("style");
    }
    else {
        $("tr[name=modalModificarCantidadFilaTallaColor]").attr("style", "display: none");
    }

    $("#aModalModificarCantidad").click();
}

function limpiarCamposLinea() {
    esProductoNuevo = false;
    objetosEditable.codigoRapido.setValor("");
    objetosEditable.descripcionCodigo.setValor("");
    objetosEditable.cantidad.setValor("1");
    objetosEditable.stock.setValor("0");
    objetosEditable.unidadDeMedida.setValor("");
    objetosEditable.neto.setValor("0");
    objetosEditable.subTotal.setValor("0");
    objetosEditable.total.setValor("0");
    objetosEditable.valorAtributoDescuento.setValor("0");
    crearControlDescuentos();
    verTallaColorTalla = "";
    verTallaColorColor = "";
    fechaEntrega();
}

function verDescuentos(contador) {
    var descuentosAppend = "";
    descuentosAppend = descuentosAppend
        + '<div class="table-responsive" >'
        + '<table id="table" class="table">'
        + '<thead></thead>'
        + '<tbody>';

    var producto = jQuery.grep(productosAgregados, function (value) {
        return value.contador === contador;
    })[0];

    for (i = 0; i < producto.Descuentos.length; i++) {
        descuentosAppend = descuentosAppend
            + '<tr>'
            + '<td>'
            + 'Descuento N°' + (i + 1)
            + '</td>'
            + '<td style="text-align: right">'
            + producto.Descuentos[i].Porcentaje + '%'
            + '</td>'
            + '</tr>';
    }
    descuentosAppend = descuentosAppend
        + '</tbody>'
        + '</table>';

    $("#modalDescuentosRealizadosControles").html(descuentosAppend);

    $("#aModalDescuentosRealizados").click();
}

var verTallaColorDescripcion = "";
var verTallaColorMedida = "";
var verTallaColorTalla = "";
var verTallaColorColor = "";
var verTallaColorPrecioUnitario = 0;
var verTallaColorDescuento = 0;

function verTallaColor(datos) {
    var bodegas = [];
    /*
    if (parametros.StockProductoEsBodega) {
        bodegas.push(parametros.StockProductoCodigoBodega);
    }
    */
    /*
    if (parametros.StockProductoEsMasivo) {
        for (i = 0; i < datos.length; i++) {
            if (bodegas.find(m => m === datos[i].CodigoBodega) === undefined) {
                bodegas.push(datos[i].CodigoBodega);
            }
        }
    }
    */
    bodegas.push({ codigo: parametros.StockProductoCodigoBodega, esPrincipal: true });
    if (parametros.StockProductoCodigoBodegaAdicional != null
        && parametros.StockProductoCodigoBodegaAdicional != undefined
        && parametros.StockProductoCodigoBodegaAdicional != "") {
        for (i = 0; i < parametros.StockProductoCodigoBodegaAdicional.split("|").length; i++) {
            bodegas.push({ codigo: parametros.StockProductoCodigoBodegaAdicional.split("|")[i], esPrincipal: false });
        }
    }
    $("#modalTallaColorBodegas").find('option').remove().end();

    for (i = 0; i < bodegas.length; i++) {
        $("#modalTallaColorBodegas").append('<option value="' + bodegas[i].codigo + '">' + bodegas[i].codigo + '</option>');
    }
    if (parametros.MuestraStockProducto) {
        $("#modalTallaColorBodegas").fadeIn();
    }

    var tallaColorAppend = "";
    tallaColorAppend = tallaColorAppend
        + '<div class="table-responsive" >'
        + '<table id="modalTallaColorControlesTable" class="table">'
        + '<thead>'
        + '<tr>'
        + '<th class="text-center" colspan="4"><h4>Informacion Producto</h4></th>'
        + (parametros.MuestraStockProducto ? '<th class="text-center" colspan="' + bodegas.length + '"><h4>Bodegas</h4></th>' : '')
        + '</tr>'
        + '<tr>'
        + '<th>Codigo Producto</th>'
        + '<th>Talla</th>'
        + '<th>Color</th>'
        + '<th>Cantidad</th>';
    if (parametros.MuestraStockProducto) {
        for (y = 0; y < bodegas.length; y++) {
            tallaColorAppend = tallaColorAppend
                + '<th ' + (bodegas[y].esPrincipal ? "style=\"background-color: green; color: white\"" : "") + '><h5>Bod. ' + bodegas[y].codigo + '</h5></th>';
        }
    }

    tallaColorAppend = tallaColorAppend
        + '</tr>'
        + '</thead>'
        + '<tbody>';

    var productoTallaColor = [];
    for (i = 0; i < datos.length; i++) {
        if (productoTallaColor.find(m =>
            m.CodigoProducto === datos[i].CodigoProducto &&
            m.Talla === datos[i].Talla &&
            m.Color === datos[i].Color) === undefined) {
            productoTallaColor.push(datos[i]);
        }
    }

    for (i = 0; i < productoTallaColor.length; i++) {
        tallaColorAppend = tallaColorAppend
            + '<tr>'
            + '<td>' + productoTallaColor[i].CodigoProducto + '</td>'
            + '<td>' + productoTallaColor[i].Talla + '</td>'
            + '<td>' + productoTallaColor[i].Color + '</td>'
            + '<td><input type="number" value="0" class="form-control" /></td>';

        if (parametros.MuestraStockProducto) {
            for (y = 0; y < bodegas.length; y++) {
                var valorBodega = datos.find(m =>
                    m.CodigoProducto === productoTallaColor[i].CodigoProducto &&
                    m.Talla === productoTallaColor[i].Talla &&
                    m.Color === productoTallaColor[i].Color &&
                    m.CodigoBodega === bodegas[y].codigo);
                try {
                    tallaColorAppend = tallaColorAppend
                        + '<td ' + (bodegas[y].esPrincipal ? "style=\"background-color: green; color: white\"" : "") + '>' + valorBodega.CantidadBodega + '</td>';
                } catch (e) {
                    tallaColorAppend = tallaColorAppend
                        + '<td ' + (bodegas[y].esPrincipal ? "style=\"background-color: green; color: white\"" : "") + '>0</td>';
                }
            }
        }

        tallaColorAppend = tallaColorAppend
            + '</tr>';
    }
    tallaColorAppend = tallaColorAppend
        + '</tbody>'
        + '</table>';

    $("#modalTallaColorControles").html(tallaColorAppend);

    $("#aModalTallaColor").click();
}

function validaNumOc() {
    var numOc = $("#txtNumOc").val();
    if (numOc != "") {
        $.ajax({
            url: "validaNUmOc",
            type: "POST",
            data: { NumOc: numOc },
            async: true,
            success: function (response) {
                if (response.Verficador) {
                    abrirError("Error Usuario", "El Numero de Oc ya Existe");
                } else {
                    $("#txtNumOc").val(numOc);
                }
            }
        });
    }
}

function agregarnotadeventa() {
    var numOc = $("#nroSolicitud").val();
    var correoManag = $("#txtCorreoManag").val();
    if (numOc == "") {
        abrirError("Usuario Error", "Debe Ingresar Numero de Solicitud");
        return null;
    }
    else {
        correoManag = null;
        var urlAgregarNotaVenta = $("#urlAgregarCotizacion").val();
        var urlAgregarNotaVentaDetalle = $("#urlDetalleCotizacion").val();

        //Subir archivo
        var fileUpload = $("#fileUpload").get(0);
        var files = fileUpload.files;
        //

        var NVNumero = $('#txtnventa').val();
        var validespa = $('#cbxDireccion').val();
        var nvFem = $('#txtfechapedido').val();
        var nvFeEnt = $('#txtfechaentrega').val();
        var CodAux = $('#txtcliente').val();
        var CodLista = $('#cbxlista').val();
        var nvObser = $('#txtobservacion').val();
        var CveCod = $('#cbxconven').val();
        var CodiCC = $('#cbxccosto').val();
        var nvSubTotal = $('#txttotal').val();
        var nvMonto = $('#txttotalfinal').val();
        var nvNetoAfecto = $('#txttotal').val();
        var numOc = $("#txtNumOc").val();
        var FechaHoraCreacion = $('#txtfechapedido').val();
        var TotalBoleta = $('#txttotalfinal').val();
        var id = $('#txtid').val();
        var canalVenta = $("#ddlCanalVenta").val();
        var CodLugarDesp = '';
        var CodMon = $("#TipoMoneda").val();

        if (validespa == 'No Tiene Dirección Asociado') {
        }
        else {
            CodLugarDesp = $('#cbxDireccion').val();
        }

        var contacto = $('#txtcontacto').val();

        var cabecera = {
            Descuentos: totales.descuentos,
            NVNumero: NVNumero,
            nvFem: nvFem,
            //nvEstado: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvEstFact: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvEstDesp: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvEstRese: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvEstConc: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //CotNum: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            NumOC: numOc,
            nvFeEnt: nvFeEnt,
            CodAux: CodAux,
            //VenCod: aaaaaaaaaaaaaaaaaaa,
            CodMon: CodMon,
            CodLista: CodLista,
            nvObser: nvObser,
            nvCanalNV: canalVenta,
            CveCod: CveCod,
            NomCon: contacto,
            CodiCC: CodiCC,
            //CodBode: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            nvSubTotal: totales.subTotal,
            nvSubTotalConDescuento: totales.subTotalConDescuento,
            //nvPorcDesc01: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvDescto01: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvPorcDesc02: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvDescto02: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvPorcDesc03: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvDescto03: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvPorcDesc04: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvDescto04: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvPorcDesc05: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvDescto05: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            nvMonto: totales.subTotal,
            //nvFeAprob: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //NumGuiaRes: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvPorcFlete: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvValflete: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvPorcEmb: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvValEmb: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvEquiv: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //nvNetoExento: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            nvNetoAfecto: totales.total,
            //nvTotalDesc: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //ConcAuto: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            CodLugarDesp: CodLugarDesp,
            //SolicitadoPor: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //DespachadoPor: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //Patente: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //RetiradoPor: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //CheckeoPorAlarmaVtas: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //EnMantencion: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //Usuario: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //UsuarioGeneraDocto: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            FechaHoraCreacion: FechaHoraCreacion,
            //Sistema: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //ConcManual: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //RutSolicitante: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //proceso: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            TotalBoleta: totales.total,
            //NumReq: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //CodVenWeb: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //CodBodeWms: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //CodLugarDocto: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //RutTransportista: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //Cod_Distrib: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //Nom_Distrib: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            //MarcaWG: aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa,
            CorreoManag: correoManag
        };
        var productos = productosAgregados;
        console.log(productos);

        var fechaCierre = $("#txtFechaCierre").val();
        var nroSolicitud = $("#nroSolicitud").val();
        if (nroSolicitud == "")
        {
            nroSolicitud = 0;
        }

        var NvExtras = {
            fechaCierre,
            nroSolicitud
        }
        var dataNotaVenta = {
            cabecera: cabecera,
            productos: productos,
            NvExtras: NvExtras
        }

        activarLoadingBoton("btnCot");

        $.ajax({
            url: urlAgregarNotaVenta,
            type: "POST",
            data: dataNotaVenta,
            success: function (result) {
                if (result.Verificador !== undefined) {
                    if (result.Verificador) {
                        abrirInformacion("Agregar nota de venta", result.Mensaje);
                    }
                    else {
                        abrirError("Agregar nota de venta", result.Mensaje);
                    }
                }
                else {
                    if (result.EstadoNP === "P") {
                        $("#divAlertaOkGeneracionNVPendiente").fadeIn();
                    }
                    else {
                        $("#divAlertaOkGeneracionNVPendiente").fadeOut();
                    }
                    var tblFinal = $("#tblResultadoAgregarNV tbody");
                    for (i = 0; i < result.length; i++) {
                        var idInterno = result[i].IdNotaVenta;
                        var idSoftland = result[i].NVNumero;
                        var nroCotizacion = result[i].NroFinalCotizacion;

                        if (idInterno <= 0) {
                            idInterno = "Sin registros";
                        }
                        if (idSoftland <= 0) {
                            idSoftland = "Sin registros";
                        }

                        var fila = "<tr><td>" + nroCotizacion + "</td><td><button class='btn btn-danger' onclick='DescargarPdf(" + idInterno + ")'>Descargar</td></tr>";

                        tblFinal.append(fila);
                    }
                    $("#divFormulacioCompletoIngresoDatos").fadeOut();
                    $("#divAlertaOkGeneracionNV").fadeIn();
                }
                desactivarLoadingBoton("btnCot");
            },
            error: function (a, b, c) {
                console.log(a, b, c);
                desactivarLoadingBoton("btnCot");
            },
            async: true
        });

        if (operacion == 1) {
            if (files.length > 0) {
                var fileData = new FormData();
                for (var i = 0; i < files.length; i++) {
                    fileData.append(files[i].name, files[i]);
                }

                $.ajax({
                    url: "SubirArchivos",
                    type: "POST",
                    contentType: false,
                    processData: false,
                    data: fileData,
                    success: function (response) {
                        console.log(response);
                    },
                    error: function (err) {
                        console.log(err.statusText);
                    }
                });
            }
        }

        return;
    }

}
//}

function DescargarPdf(idCotizacion) {
    idCotizacion = 0;
    window.location = 'DescargarArchivoCotizacion?idCotizacion=' + idCotizacion;
}


function ObtenerSaldo(RutAux, CodAux, Nombre, Saldo) {
    $("#modalSaldoSubtitulo").text(CodAux + " - " + Nombre);
    $("#tblSaldos").html("");
    $.ajax({
        type: "POST",
        url: "ObtenerSaldo",
        data: { RutAuxiliar: RutAux, CodAux: CodAux },
        async: true,
        success: function (response) {
            console.log(response);
            if (response == 1) {
                alert("Cliente sin Saldo.");
            }
            else {

                var tblSaldos = $("#tblSaldos");

                //$.each(data.Cabecera, function (index, value) {

                var htmlCabecera = "";
                var htmlDetalle = "";

                htmlCabecera = htmlCabecera + "<th>Cuenta</th>";
                htmlCabecera = htmlCabecera + "<th>Desc Cuenta</th>";
                htmlCabecera = htmlCabecera + "<th>CodigoAux</th>";
                htmlCabecera = htmlCabecera + "<th>RutCliente</th>";
                htmlCabecera = htmlCabecera + "<th>Nombre</th>";
                htmlCabecera = htmlCabecera + "<th>FecEmision</th>";
                htmlCabecera = htmlCabecera + "<th>TipoDoc</th>";
                htmlCabecera = htmlCabecera + "<th>NomDoc</th>";
                htmlCabecera = htmlCabecera + "<th>Saldo</th>";
                htmlCabecera = htmlCabecera + "<th>Cod Documento</th>";
                htmlCabecera = htmlCabecera + "<th>Año Comprobante</th>";
                htmlCabecera = htmlCabecera + "<th>Fecha Ven</th>";

                tblSaldos.append(htmlCabecera);

                $.each(response.DetalleSaldo, function (index, value) {
                    htmlDetalle = "";
                    htmlDetalle = htmlDetalle + "<tr>";
                    htmlDetalle = htmlDetalle + "<td>" + value.pccodi + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.pcdesc + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.codaux + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.RutAux + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.nomaux + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.fechaemiString + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.desdoc + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right'>" + value.movnumdocref + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right'>" + formatearNumero(value.Saldo, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.coddoc + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.Cpbano + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.movfvString + "</td>";

                    htmlDetalle = htmlDetalle + "</tr>";

                    tblSaldos.append(htmlDetalle);
                });
            }
        }
    });
}

function fechaEntrega() {
    var fEntrega = $("#txtfechaentrega").val();
    $("#fechaEntrega").val(fEntrega);
}

function agregarFilaProducto() {
    var codigo = objetosEditable.codigoRapido.getValor();
    var producto = productos.find(m => m.CodProd === codigo);
    var fechacierreCab = $("#txtfechaentrega").val();
    var fechacierreDet = $("#fechaEntrega").val();

    if (Date.parse(fechacierreDet) < Date.parse(fechacierreCab))
    {
        abrirInformacion("Usuario","La fecha entrega detalle debe ser mayor o igual a fecha entrega");
        fechaEntrega();
        return null;
    }

    if (parametros.ManejaTallaColor) {
        var urlTC = $("#urlObtieneTallaColorProducto").val();
        if (producto != null && producto != undefined) {
            $.ajax({
                url: urlTC,
                data: { CodProd: producto.CodProd },
                type: "POST",
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response.length === 0) {
                        addRow();
                    }
                    else {
                        verTallaColorDescripcion = codigo;
                        if (parametros.MuestraUnidadMedidaProducto) {
                            verTallaColorMedida = producto.codumed;
                        }
                        else {
                            verTallaColorMedida = "";
                        }
                        verTallaColorPrecioUnitario = producto.PrecioVta;
                        verTallaColorDescuento = 0;
                        verTallaColor(response);
                    }
                },
                error: function (response) {
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }
        else {
            abrirError("Producto", "Favor seleccione un producto antes de continuar");
        }
    }
    else {
        addRow();
    }
}

function calculoPrecioVenta() {
    var precioNeto = $("#modalAgregarProductoNuevoValorNeto").val();
    $("#modalAgregarProductoNuevoPrecioVenta").val(parseInt(precioNeto / 0.48));
}

function GrabaContacto() {
    var codAux = $("#CodAux").val();
    var nombre = $("#txtNombre").val();
    var fono = $("#txtFono").val();
    var email = $("#txtEmail").val();
    $.ajax({
        url: "grabarContacto",
        type: "POST",
        data: {
            codAux,
            nombre,
            fono,
            email
        },
        async: true,
        success: function (response) {
            if (!response.Verificador) {
                abrirError("Error Usuario", "No se puede crrear contacto");
            } else {
                $("#txtcontacto").append('<option selected value="' + nombre + '">' + nombre  + '</option>');             
            }
        }
    });
}

function validaCodProd() {
    var codProd = $("#modalAgregarProductoNuevoCodigo").val();
    codProd = codProd.replace("'", "");
    codProd = codProd.replace("'", "");
    codProd = codProd.replace("'", "");
    codProd = codProd.replace('"', "");
    codProd = codProd.replace('"', "");
    codProd = codProd.replace('"', "");
    if (codProd != "") {
        $.ajax({
            type: "POST",
            url: "/Venta/validaProdSoftland",
            data: { codProd },
            async: true,
            success: function (response) {
                if (response) {
                    abrirError("Error", "Codigo de producto ingresado ya existe en Softland");
                    $("#modalAgregarProductoNuevoCodigo").focus();
                } else {
                    $("#modalAgregarProductoNuevoCodigo").val(codProd);
                    var boton = document.getElementById('modalAgregarProductoNuevoAgregar');
                    boton.disabled = false;
                }
            }
        });
    } else {
        var boton = document.getElementById('modalAgregarProductoNuevoAgregar');
        boton.disabled = true;
    }
}

function validaDesProd() {
    var desProd = $("#modalAgregarProductoNuevoDescripcion").val();
    desProd = desProd.replace("'", "");
    desProd = desProd.replace("'", "");
    desProd = desProd.replace("'", "");
    desProd = desProd.replace('"', "");
    desProd = desProd.replace('"', "");
    desProd = desProd.replace('"', "");
    $("#modalAgregarProductoNuevoDescripcion").val(desProd);
}

function validaFabCodProd() {
    var fabCodProd = $("#modalCodigoFabrica").val();
    fabCodProd = fabCodProd.replace("'", "");
    fabCodProd = fabCodProd.replace("'", "");
    fabCodProd = fabCodProd.replace("'", "");
    fabCodProd = fabCodProd.replace('"', "");
    fabCodProd = fabCodProd.replace('"', "");
    fabCodProd = fabCodProd.replace('"', "");
    $("#modalCodigoFabrica").val(fabCodProd);
}

