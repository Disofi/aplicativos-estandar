﻿$(document).ready(function () {
    $("#btnCrearReporte").click(function () {
        $("#divCreandoReporte").fadeOut();
        //$("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        $("#divTablaStock").fadeOut();

        activarLoadingBoton("btnCrearReporte");

        var productos = $("#ddlVendedores").val();
        var estado = 1;
        if (productos.length != 0) {
            productos = vendedoresTodos.filter(m => {
                var vendedortemp = productos.find(m2 => m2 === m.CodProd);
                if (vendedortemp !== undefined) { return vendedortemp; }
            });
        } else {
            productos = null;
            estado = 0;
        }
        //$("#divCreandoReporte").fadeIn();
        $.ajax({
            type: "POST",
            url: "GenerarReporteComisiones",
            data: { productos: productos, estado: estado },
            async: true,
            success: function (response) {
                if (response.Verificador) {
                    $("#divCreandoReporteCargando").fadeOut("", function () {
                        desactivarLoadingBoton("btnCrearReporte");
                        window.open("DescargarArchivoStock");
                        //$("#divCreandoReporteCreado").fadeIn();
                    });
                }
                else {
                    $("#divCreandoReporte").fadeOut("", function () {
                        desactivarLoadingBoton("btnCrearReporte");
                        var tabla = "";
                        $.each(response, function (index, item) {
                            console.log(item);
                            tabla = tabla + "<tr>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.CodigoProducto;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.DescripcionProducto;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.UnidadMedida;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.Grupo;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.SubGrupo;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.CodigoBodega;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.DescripcionBodega;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.StockTipo;
                            tabla = tabla + "</td>";
                            tabla = tabla + "<td>";
                            tabla = tabla + item.CantidadStock;
                            tabla = tabla + "</td>";
                            tabla = tabla + "</tr>";

                        });
                        $("#divCreandoReporteCreadoTabla").html(tabla);

                        $("#divTablaStock").fadeIn();
                    });
                }
            },
            error: function (a, b, c) {
                $("#divCreandoReporteError").fadeIn();
            }
        });
    });
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoStock");
    });

    $("#ddlVendedores").select2({ placeholder: "Seleccione Productos" });

    $.ajax({
        type: "POST",
        url: "ObtenerVendedoresEmpresa",
        data: {},
        async: true,
        success: function (response) {
            var opcion = "<optgroup label='Productos'>";
            vendedoresTodos = response;
            $("#ddlVendedores").append(opcion);

            $.each(response, function (index, item) {
                opcion = "<option value='" + item.CodProd + "'>" + item.CodProd + ' - ' + item.DesProd + "</option>";

                $("#ddlVendedores").append(opcion);
            });

            opcion = "</optgroup>";

            //$("#ddlVendedores").append(opcion);
        },
        error: function (a, b, c) {
        }
    });

});

var vendedoresTodos = [];