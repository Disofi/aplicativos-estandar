﻿$(document).ready(function () {
    $("input[switcher]").change(function () {
        logicaCambioSwitch(this.id);
        var id = this.id;
    });
    $("#Dashboard").change(function () {
        mostrarDashboard();

    });
    $("#DashboardCobranza").change(function () {
        mostrarDashboardCobranza()
    });
  
    $("#notaVenta").change(function () {

        activaNotaVenta()
    });
   
   
    $("#btnGuardarParametro").click(function () {
        GuardarParametros();
    });
    $("#ddlEmpresas").change(function () {
        var idEmpresa = $("#ddlEmpresas").val();
        if (idEmpresa !== '-1') {
            $.ajax({
                url: "ObtieneParametro",
                data: { idEmpresa: idEmpresa },
                type: "POST",
                dataType: "json",
                async: false,
                success: function (response) {
                    cambiarValorSwitcher("MultiEmpresa", response.MultiEmpresa);
                    cambiarValorSwitcher("ManejaAdministrador", response.ManejaAdministrador);
                    cambiarValorSwitcher("ManejaValorAdicional", response.ManejaValorAdicional);
                    cambiarValorSwitcher("ManejaAprobador", response.ManejaAprobador);
                    cambiarValorSwitcher("ListaClientesVendedor", response.ListaClientesVendedor);
                    cambiarValorSwitcher("ListaClientesTodos", response.ListaClientesTodos);
                    cambiarValorSwitcher("ValidaReglasNegocio", response.ValidaReglasNegocio);
                    cambiarValorSwitcher("ManejaListaPrecios", response.ManejaListaPrecios);
                    cambiarValorSwitcher("EditaPrecioProducto", response.EditaPrecioProducto);
                    cambiarValorSwitcher("MuestraCondicionVentaCliente", response.MuestraCondicionVentaCliente);
                    cambiarValorSwitcher("MuestraCondicionVentaTodos", response.MuestraCondicionVentaTodos);
                    cambiarValorSwitcher("EditaDescuentoProducto", response.EditaDescuentoProducto);
                    $("#MaximoDescuentoProducto").val(response.MaximoDescuentoProducto);
                    $("#CantidadDescuentosProducto").val(response.CantidadDescuentosProducto);
                    cambiarValorSwitcher("MuestraStockProducto", response.MuestraStockProducto);
                    cambiarValorSwitcher("StockProductoEsMasivo", response.StockProductoEsMasivo);
                    cambiarValorSwitcher("StockProductoEsBodega", response.StockProductoEsBodega);
                    $("#StockProductoCodigoBodega").val(response.StockProductoCodigoBodega);
                    $("#StockProductoCodigoBodegaAdicional").val(response.StockProductoCodigoBodegaAdicional);
                    cambiarValorSwitcher("ControlaStockProducto", response.ControlaStockProducto);
                    cambiarValorSwitcher("EnvioMailCliente", response.EnvioMailCliente);
                    cambiarValorSwitcher("EnvioMailVendedor", response.EnvioMailVendedor);
                    cambiarValorSwitcher("EnvioMailContacto", response.EnvioMailContacto);
                    cambiarValorSwitcher("EnvioObligatorioAprobador", response.EnvioObligatorioAprobador);
                    cambiarValorSwitcher("ManejaTallaColor", response.ManejaTallaColor);
                    cambiarValorSwitcher("ManejaDescuentoTotalDocumento", response.ManejaDescuentoTotalDocumento);
                    $("#CantidadDescuentosTotalDocumento").val(response.CantidadDescuentosTotalDocumento);
                    $("#CantidadLineas").val(response.CantidadLineas);
                    cambiarValorSwitcher("ManejaLineaCreditoVendedor", response.ManejaLineaCreditoVendedor);
                    cambiarValorSwitcher("ManejaLineaCreditoAprobador", response.ManejaLineaCreditoAprobador);
                    cambiarValorSwitcher("ManejaCanalVenta", response.ManejaCanalVenta);
                    cambiarValorSwitcher("CambioVendedorCliente", response.CambioVendedorCliente);
                    cambiarValorSwitcher("CreacionNotaVentaUsuariosBloqueados", response.CreacionNotaVentaUsuariosBloqueados);
                    cambiarValorSwitcher("CreacionNotaVentaUsuariosInactivos", response.CreacionNotaVentaUsuariosInactivos);
                    cambiarValorSwitcher("PermiteModificacionCondicionVenta", response.PermiteModificacionCondicionVenta);
                    $("#AtributoSoftlandDescuentoCliente").val(response.AtributoSoftlandDescuentoCliente);
                    $("#CodigoCondicionVentaPorDefecto").val(response.CodigoCondicionVentaPorDefecto);
                    cambiarValorSwitcher("PermiteCrearDireccion", response.PermiteCrearDireccion);
                    cambiarValorSwitcher("CrearClienteConDV", response.CrearClienteConDV);
                    cambiarValorSwitcher("MuestraUnidadMedidaProducto", response.MuestraUnidadMedidaProducto);
                    cambiarValorSwitcher("DescuentoLineaDirectoSoftland", response.DescuentoLineaDirectoSoftland);
                    cambiarValorSwitcher("DescuentoTotalDirectoSoftland", response.DescuentoTotalDirectoSoftland);
                    cambiarValorSwitcher("EnvioMailAprobador", response.EnvioMailAprobador);
                    cambiarValorSwitcher("AgregaCliente", response.AgregaCliente);
                    cambiarValorSwitcher("ManejaSaldo", response.ManejaSaldo);
                    cambiarValorSwitcher("CorreosWebConfig", response.CorreosWebConfig);


                    /*cambiarValorSwitcher("Booking", response.Booking);*/
                    $("#Booking").prop('checked', response.Booking);

                    /*cambiarValorSwitcher("Backlog", response.Backlog);*/
                    $("#Backlog").prop('checked', response.Backlog);

                    /*cambiarValorSwitcher("saldosClientes", response.saldosClientes);*/
                    $("#saldosClientes").prop('checked', response.saldosClientes);

                    /*cambiarValorSwitcher("saldosProveedores", response.saldosProveedores);*/
                    $("#saldosProveedores").prop('checked', response.saldosProveedores);

                    /*cambiarValorSwitcher("notaVenta", response.notaVenta);*/
                    $("#notaVenta").prop('checked', response.notaVenta);

                    /*cambiarValorSwitcher("reporteStock", response.reporteStock);*/
                    $("#reporteStock").prop('checked', response.reporteStock);

                    /*cambiarValorSwitcher("Cotizaciones", response.Cotizaciones);*/
                    $("#Cotizaciones").prop('checked', response.Cotizaciones);

                    /*cambiarValorSwitcher("Dashboard", response.Dashboard);*/
                    $("#Dashboard").prop('checked', response.Dashboard);

                    /*cambiarValorSwitcher("DashboardCobranza", response.DashboardCobranza);*/
                    $("#DashboardCobranza").prop('checked', response.DashboardCobranza);

                    /*cambiarValorSwitcher("ReporteSaldo", response.ReporteSaldo);*/
                    $("#ReporteSaldo").prop('checked', response.ReporteSaldo);

                    if ($("#Dashboard").is(':checked')) {
                        $("#DashboardPanel").removeAttr("hidden");
                    } else {
                        $("#DashboardPanel").attr("hidden", "true");
                    }
                    if ($("#DashboardCobranza").is(':checked')) {
                        $("#CobranzaPanel").removeAttr("hidden");
                    } else {
                        $("#CobranzaPanel").attr("hidden", "true");
                    }
                    $("#divDatos").fadeIn();
                },
                error: function (response) {
                },
                failure: function (response) {
                    alert(response.responseText);
                }
            });
        }
        else {
            $("#divDatos").fadeOut();
        }
    });

   

    
});

function logicaCambioSwitch(id) {
    var check = estaHabilitadoSwitcher(id);
    if (check) {
        $("#" + id).removeAttr("checked");
    }
    else {
        $("#" + id).attr("checked", "");
    }
}
function estaHabilitadoSwitcher(id) {
    var check = $("#" + id).attr("checked");
    if (check !== undefined) {
        return true;
    }
    else {
        return false;
    }
}
function cambiarValorSwitcher(id, valor) {
    $("#" + id).removeAttr("checked");
    if (valor) {
        $("#" + id).attr("checked", "");
    }
    else {
        $("#" + id).removeAttr("checked");
    }
}

function mostrarDashboard() {

    if ($("#Dashboard").is(':checked')) {
        $("#DashboardPanel").removeAttr("hidden");
    } else {

        $("#DashboardPanel").attr("hidden","true");
    }
}

function mostrarDashboardCobranza() {

    if ($("#DashboardCobranza").is(':checked')) {
        $("#CobranzaPanel").removeAttr("hidden");
    } else {

        $("#CobranzaPanel").attr("hidden", "true");
    }
}

function activaNotaVenta() {
    var check = $("#notaVenta").is(':checked');
    var booking = $("#Booking").is(':checked');
    var backlog = $("#Backlog").is(':checked');

    if (!check) {
        if (booking == true || backlog == true) {
           
            //$("#Booking").removeAttr("checked");
            //$("#Backlog").removeAttr("checked");
            //$("#Booking").prop('checked', false);
            //$("#Backlog").prop('checked', false);
            $("#Cotizaciones").prop('checked', false);
            $("#Cotizaciones").prop('disabled', true);
            //$("#Booking").prop('disabled', true);
            //$("#Backlog").prop('disabled', true);
            logicaCambioSwitch("Cotizaciones", false);
            //logicaCambioSwitch("Booking", false);
            //logicaCambioSwitch("Backlog", false);
        }
    } else{
        //$("#Booking").removeAttr("disabled");
        //$("#Backlog").removeAttr("disabled");
        $("#Cotizaciones").removeAttr("disabled");
        //$("#Booking").prop('checked', true);
        //$("#Backlog").prop('checked', true);
        //logicaCambioSwitch("Booking", true);
        //logicaCambioSwitch("Backlog", true);
    }
}



function GuardarParametros() {
    var IdEmpresa = $("#ddlEmpresas").val();
    var MultiEmpresa = estaHabilitadoSwitcher("MultiEmpresa");
    var ManejaAdministrador = estaHabilitadoSwitcher("ManejaAdministrador");
    var ManejaValorAdicional = estaHabilitadoSwitcher("ManejaValorAdicional");
    var ManejaAprobador = estaHabilitadoSwitcher("ManejaAprobador");
    var ListaClientesVendedor = estaHabilitadoSwitcher("ListaClientesVendedor");
    var ListaClientesTodos = estaHabilitadoSwitcher("ListaClientesTodos");
    var ValidaReglasNegocio = estaHabilitadoSwitcher("ValidaReglasNegocio");
    var ManejaListaPrecios = estaHabilitadoSwitcher("ManejaListaPrecios");
    var EditaPrecioProducto = estaHabilitadoSwitcher("EditaPrecioProducto");
    var MuestraCondicionVentaCliente = estaHabilitadoSwitcher("MuestraCondicionVentaCliente");
    var MuestraCondicionVentaTodos = estaHabilitadoSwitcher("MuestraCondicionVentaTodos");
    var EditaDescuentoProducto = estaHabilitadoSwitcher("EditaDescuentoProducto");
    var MaximoDescuentoProducto = $("#MaximoDescuentoProducto").val();
    var CantidadDescuentosProducto = $("#CantidadDescuentosProducto").val();
    var MuestraStockProducto = estaHabilitadoSwitcher("MuestraStockProducto");
    var StockProductoEsMasivo = estaHabilitadoSwitcher("StockProductoEsMasivo");
    var StockProductoEsBodega = estaHabilitadoSwitcher("StockProductoEsBodega");
    var StockProductoCodigoBodega = $("#StockProductoCodigoBodega").val();
    var StockProductoCodigoBodegaAdicional = $("#StockProductoCodigoBodegaAdicional").val();
    var ControlaStockProducto = estaHabilitadoSwitcher("ControlaStockProducto");
    var EnvioMailCliente = estaHabilitadoSwitcher("EnvioMailCliente");
    var EnvioMailVendedor = estaHabilitadoSwitcher("EnvioMailVendedor");
    var EnvioMailContacto = estaHabilitadoSwitcher("EnvioMailContacto");
    var EnvioObligatorioAprobador = estaHabilitadoSwitcher("EnvioObligatorioAprobador");
    var ManejaTallaColor = estaHabilitadoSwitcher("ManejaTallaColor");
    var ManejaDescuentoTotalDocumento = estaHabilitadoSwitcher("ManejaDescuentoTotalDocumento");
    var CantidadDescuentosTotalDocumento = $("#CantidadDescuentosTotalDocumento").val();
    var CantidadLineas = $("#CantidadLineas").val();
    var ManejaLineaCreditoVendedor = estaHabilitadoSwitcher("ManejaLineaCreditoVendedor");
    var ManejaLineaCreditoAprobador = estaHabilitadoSwitcher("ManejaLineaCreditoAprobador");
    var ManejaCanalVenta = estaHabilitadoSwitcher("ManejaCanalVenta");
    var CambioVendedorCliente = estaHabilitadoSwitcher("CambioVendedorCliente");
    var CreacionNotaVentaUsuariosBloqueados = estaHabilitadoSwitcher("CreacionNotaVentaUsuariosBloqueados");
    var CreacionNotaVentaUsuariosInactivos = estaHabilitadoSwitcher("CreacionNotaVentaUsuariosInactivos");
    var PermiteModificacionCondicionVenta = estaHabilitadoSwitcher("PermiteModificacionCondicionVenta");
    var AtributoSoftlandDescuentoCliente = $("#AtributoSoftlandDescuentoCliente").val();
    var CodigoCondicionVentaPorDefecto = $("#CodigoCondicionVentaPorDefecto").val();
    var PermiteCrearDireccion = estaHabilitadoSwitcher("PermiteCrearDireccion");
    var CrearClienteConDV = estaHabilitadoSwitcher("CrearClienteConDV");
    var MuestraUnidadMedidaProducto = estaHabilitadoSwitcher("MuestraUnidadMedidaProducto");
    var DescuentoLineaDirectoSoftland = estaHabilitadoSwitcher("DescuentoLineaDirectoSoftland");
    var DescuentoTotalDirectoSoftland = estaHabilitadoSwitcher("DescuentoTotalDirectoSoftland");

    var AgregaCliente = estaHabilitadoSwitcher("AgregaCliente");
    var EnvioMailAprobador = estaHabilitadoSwitcher("EnvioMailAprobador");
    var ManejaSaldo = estaHabilitadoSwitcher("ManejaSaldo");
    var CorreosWebConfig = estaHabilitadoSwitcher("CorreosWebConfig");
    var Booking = estaHabilitadoSwitcher("Booking");
    var Backlog = estaHabilitadoSwitcher("Backlog");
    var saldosClientes = estaHabilitadoSwitcher("saldosClientes");
    var saldosProveedores = estaHabilitadoSwitcher("saldosProveedores");

    var notaVenta = estaHabilitadoSwitcher("notaVenta");
    var Dashboard = estaHabilitadoSwitcher("Dashboard");
    var reporteStock = estaHabilitadoSwitcher("reporteStock");
    var Cotizaciones = estaHabilitadoSwitcher("Cotizaciones");
    var DashboardCobranza = estaHabilitadoSwitcher("DashboardCobranza");
    var ReporteSaldo = estaHabilitadoSwitcher("ReporteSaldo");
    var datosRequest = {
        IdEmpresa: IdEmpresa,
        MultiEmpresa: MultiEmpresa,
        ManejaAdministrador: ManejaAdministrador,
        ManejaValorAdicional: ManejaValorAdicional,
        ManejaAprobador: ManejaAprobador,
        ListaClientesVendedor: ListaClientesVendedor,
        ListaClientesTodos: ListaClientesTodos,
        ValidaReglasNegocio: ValidaReglasNegocio,
        ManejaListaPrecios: ManejaListaPrecios,
        EditaPrecioProducto: EditaPrecioProducto,
        MuestraCondicionVentaCliente: MuestraCondicionVentaCliente,
        MuestraCondicionVentaTodos: MuestraCondicionVentaTodos,
        EditaDescuentoProducto: EditaDescuentoProducto,
        MaximoDescuentoProducto: MaximoDescuentoProducto,
        CantidadDescuentosProducto: CantidadDescuentosProducto,
        MuestraStockProducto: MuestraStockProducto,
        StockProductoEsMasivo: StockProductoEsMasivo,
        StockProductoEsBodega: StockProductoEsBodega,
        StockProductoCodigoBodega: StockProductoCodigoBodega,
        StockProductoCodigoBodegaAdicional: StockProductoCodigoBodegaAdicional,
        ControlaStockProducto: ControlaStockProducto,
        EnvioMailCliente: EnvioMailCliente,
        EnvioMailVendedor: EnvioMailVendedor,
        EnvioMailContacto: EnvioMailContacto,
        EnvioObligatorioAprobador: EnvioObligatorioAprobador,
        ManejaTallaColor: ManejaTallaColor,
        ManejaDescuentoTotalDocumento: ManejaDescuentoTotalDocumento,
        CantidadDescuentosTotalDocumento: CantidadDescuentosTotalDocumento,
        CantidadLineas: CantidadLineas,
        ManejaLineaCreditoVendedor: ManejaLineaCreditoVendedor,
        ManejaLineaCreditoAprobador: ManejaLineaCreditoAprobador,
        ManejaCanalVenta: ManejaCanalVenta,
        CambioVendedorCliente: CambioVendedorCliente,
        CreacionNotaVentaUsuariosBloqueados: CreacionNotaVentaUsuariosBloqueados,
        CreacionNotaVentaUsuariosInactivos: CreacionNotaVentaUsuariosInactivos,
        PermiteModificacionCondicionVenta: PermiteModificacionCondicionVenta,
        AtributoSoftlandDescuentoCliente: AtributoSoftlandDescuentoCliente,
        CodigoCondicionVentaPorDefecto: CodigoCondicionVentaPorDefecto,
        PermiteCrearDireccion: PermiteCrearDireccion,
        CrearClienteConDV: CrearClienteConDV,
        MuestraUnidadMedidaProducto: MuestraUnidadMedidaProducto,
        DescuentoLineaDirectoSoftland: DescuentoLineaDirectoSoftland,
        DescuentoTotalDirectoSoftland: DescuentoTotalDirectoSoftland,
        AgregaCliente: AgregaCliente,
        EnvioMailAprobador: EnvioMailAprobador,
        ManejaSaldo: ManejaSaldo,
        CorreosWebconfig: CorreosWebConfig,
        Booking: Booking,
        Backlog: Backlog,
        saldosClientes: saldosClientes,
        saldosProveedores: saldosProveedores,
        notaVenta: notaVenta,
        Dashboard: Dashboard,
        reporteStock: reporteStock,
        Cotizaciones: Cotizaciones,
        DashboardCobranza: DashboardCobranza,
        ReporteSaldo: ReporteSaldo
    }

    $.ajax({
        url: "GuardarParametro",
        data: { parametro: datosRequest },
        type: "POST",
        dataType: "json",
        async: false,
        success: function (response) {
            console.log(response);
            if (response.Verificador) {
                abrirInformacion("Guardar Parametro", response.Mensaje);
            }
            else {
                abrirError("Guardar Parametro", response.Mensaje);
            }
        },
        error: function (response) {
        },
        complete: function (a, b, c) {
            console.log(a);
            console.log(b);
            console.log(c);
        },
        failure: function (response) {
            alert(response.responseText);
        }
    });
}


function envioDatos() {
    var valor = $('input[name=gender]:checked', '#ff').val();
    $("#valorRadio").val(valor);

    document.getElementById("ff").submit();




    //alert($('input[name=gender]:checked', '#ff').val());
}


