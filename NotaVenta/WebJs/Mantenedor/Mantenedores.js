﻿function GrabarConsideracion()
{
    var titulo = $("#modelTitulo").val();
    var consideracion = $("#modelDescripcion").val();
    if (titulo != "" && consideracion != "") {
        $.ajax({
            url: "GrabarConsideracion",
            type: "POST",
            data: {
                titulo,
                consideracion
            },
            async: true,
            success: function (response) {
                if (response.Verficador) {
                    abrirError("Error Usuario", "El Numero de Oc ya Existe");
                } else {
                    $("#detalle tbody").html("");
                    var detalleTbody = $("#detalle tbody");
                    var detalleFila = "";
                    $.each(response.consideraciones, function (index, item) {
                        detalleFila = "";
                        detalleFila += "<tr>";
                        detalleFila += "<tr>";
                        detalleFila += "<tr>";
                        detalleFila += "<td id='filaId_" + index + "'>" + item.Id + " </td>";
                        detalleFila += "<td id='filaTitulo_" + index + "'>" + item.Titulo + " </td>";
                        if (item.Consideracion.length > 275) {
                            detalleFila += "<td id='Consideracion'>" + item.Consideracion.substring(0, 275) + " </td>";
                            detalleFila += "<td hidden id='filaConsideracion_" + index + "'>" + item.Consideracion + " </td>";
                        } else {
                            detalleFila += "<td id='filaConsideracion_" + index + "'>" + item.Consideracion + " </td>";
                        }
                        detalleFila += "<td>";
                        detalleFila += "<input class='btn btn-primary' type='button' name='Editar'  value='Editar' onclick='MostarDatos(" + item.Id +","+ index +")'>";
                        detalleFila += "</td>";
                                    
                        detalleFila += "<td>";     
                        detalleFila += "<input class='btn btn-primary' type='button' name='Eliminar' value='Eliminar' onclick='EliminarConsideracion(" + item.Id + ")'>";
                        detalleFila += "</td>";
                        detalleFila += "</tr>";            
                                
                        detalleTbody.append(detalleFila);
                    });
                    abrirConfirmacion("Usuario", "se agrego consideracion", function () {
                        $("#AgregaConsideracion").modal('hide');
                    });
                }
            }
        });
    } else
    {
        alert("Debe ingresar datos")
    }
}

function MostarDatos(id,x)
{
    var titulo = $("#filaTitulo_" + x).text();
    titulo = titulo.trim();

    var id = $("#filaId_" + x).text();
    id = id.trim();

    var descripcion = $("#filaConsideracion_" + x).text();
    descripcion.trim();
    
    $("#editarId").val("");
    $("#editarId").val(id);

    $("#editarTitulo").val("");
    $("#editarTitulo").val(titulo);

    $("#editarDescripcion").val("");
    $("#editarDescripcion").val(descripcion);

    $("#modalModificarConsideracion").modal('show');
}

function ModificarConsideracion()
{
    var id = $("#editarId").val();
    var titulo = $("#editarTitulo").val();
    var consideracion = $("#editarDescripcion").val();
    if (titulo != "" && consideracion != "") {
        $.ajax({
            url: "ModificarConsideracion",
            type: "POST",
            data: {
                id,
                titulo,
                consideracion
            },
            async: true,
            success: function (response) {
                if (response.Verficador) {
                    abrirError("Error Usuario", "El Numero de Oc ya Existe");
                } else {
                    $("#detalle tbody").html("");
                    var detalleTbody = $("#detalle tbody");
                    var detalleFila = "";
                    $.each(response.consideraciones, function (index, item) {
                        detalleFila = "";
                        detalleFila += "<tr>";
                        detalleFila += "<tr>";
                        detalleFila += "<tr>";
                        detalleFila += "<td id='filaId_" + index + "'>" + item.Id + " </td>";
                        detalleFila += "<td id='filaTitulo_" + index + "'>" + item.Titulo + " </td>";
                        if (item.Consideracion.length > 275) {
                            detalleFila += "<td id='Consideracion'>" + item.Consideracion.substring(0, 275) + " </td>";
                            detalleFila += "<td hidden id='filaConsideracion_" + index + "'>" + item.Consideracion + " </td>";
                        } else {
                            detalleFila += "<td id='filaConsideracion_" + index + "'>" + item.Consideracion + " </td>";
                        }
                        detalleFila += "<td>";
                        detalleFila += "<input class='btn btn-primary' type='button' name='Editar'  value='Editar' onclick='MostarDatos(" + item.Id +","+ index +")'>";
                        detalleFila += "</td>";
                                    
                        detalleFila += "<td>";     
                        detalleFila += "<input class='btn btn-primary' type='button' name='Eliminar' value='Eliminar' onclick='EliminarConsideracion(" + item.Id + ")'>";
                        detalleFila += "</td>";
                        detalleFila += "</tr>";            
                                
                        detalleTbody.append(detalleFila);
                    });
                    abrirConfirmacion("Usuario", "Modifico consideracion", function () {
                        $("#modalModificarConsideracion").modal('hide');
                    });
                }
            }
        });
    } else
    {
        alert("Debe ingresar datos")
    }
}

function EliminarConsideracion(id)
{
        $.ajax({
            url: "EliminarConsideracion",
            type: "POST",
            data: {
                id
            },
            async: true,
            success: function (response) {
                if (response.Verficador) {
                    abrirError("Error Usuario", "El Numero de Oc ya Existe");
                } else {
                    abrirConfirmacion("Usuario", "Elimino consideracion", function () {
                        $("#detalle tbody").html("");
                        var detalleTbody = $("#detalle tbody");
                        var detalleFila = "";
                        $.each(response.consideraciones, function (index, item) {
                            detalleFila = "";
                            detalleFila += "<tr>";
                            detalleFila += "<td id='filaId_" + index + "'>" + item.Id + " </td>";
                            detalleFila += "<td id='filaTitulo_" + index + "'>" + item.Titulo + " </td>";
                            if (item.Consideracion.length > 275) {
                                detalleFila += "<td id='Consideracion'>" + item.Consideracion.substring(0, 275) + " </td>";
                                detalleFila += "<td hidden id='filaConsideracion_" + index + "'>" + item.Consideracion + " </td>";
                            } else {
                                detalleFila += "<td id='filaConsideracion_" + index + "'>" + item.Consideracion + " </td>";
                            }
                            detalleFila += "<td>";
                            detalleFila += "<input class='btn btn-primary' type='button' name='Editar'  value='Editar' onclick='MostarDatos(" + item.Id +","+ index +")'/>";
                            detalleFila += "</td>";
                                    
                            detalleFila += "<td>";     
                            detalleFila += "<input class='btn btn-primary' type='button' name='Eliminar' value='Eliminar' onclick='EliminarConsideracion(" + item.Id + ")'/>";
                            detalleFila += "</td>";
                            detalleFila += "</tr>";            
                                
                            detalleTbody.append(detalleFila);
                        }); 
                    });                    
                }

                }
        });
    
}

function limpiarAgregar() {
    $("#modelTitulo").val("");
    $("#modelDescripcion").val("");
}