﻿var parametros = {};
var datosDetalle = [];

$(document).ready(function () {
    parametros = {
        ControlaStockProducto: ($("#ControlaStockProducto").val().toLowerCase() === "true"),
        DescuentoLineaDirectoSoftland: ($("#DescuentoLineaDirectoSoftland").val().toLowerCase() === "true"),
        DescuentoTotalDirectoSoftland: ($("#DescuentoTotalDirectoSoftland").val().toLowerCase() === "true"),
        CantidadDescuentosProducto: ($("#CantidadDescuentosProducto").val().toLowerCase()),
        CantidadDescuentosTotalDocumento: ($("#CantidadDescuentosTotalDocumento").val().toLowerCase()),
        MuestraUnidadMedidaProducto: ($("#MuestraUnidadMedidaProducto").val().toLowerCase() === "true"),
        ManejaTallaColor: ($("#ManejaTallaColor").val().toLowerCase() === "true"),
    };

    convertirFullScreenModal("modalSaldo");

    $("#modalModificarProductoNuevoModificar").click(() => {
        activarLoadingBoton("modalModificarProductoNuevoModificar");
        var elementos = {
            idNotaVenta: $("#modalModificarProductoNuevoIdNotaVenta").val(),
            idDetalleNotaVenta: $("#modalModificarProductoNuevoIdDetalleNotaVenta").val(),
            linea: $("#modalModificarProductoNuevoLinea").val(),
            codigoAntiguo: $("#modalModificarProductoNuevoCodigoAntiguo").val(),
            codigo: $("#modalModificarProductoNuevoCodigo").val(),
            descripcion: $("#modalModificarProductoNuevoDescripcion").val(),
            grupo: $("#modalModificarProductoNuevoGrupo").val(),
            subGrupo: $("#modalModificarProductoNuevoSubGrupo").val(),
            valorNeto: $("#modalModificarProductoNuevoValorNeto").val().replace(",", "."),
            tipoMoneda: $("#modalModificarProductoNuevoTipoMoneda").val(),
        };
        console.log("dasldoaskoo")
        if (elementos.codigo.length > 0 && elementos.codigo.length <= 20) {
            var _url = $("#urlObtieneProductosPorListaPrecio").val();

            if (datosDetalle.filter(m => m.CodProd === elementos.codigo && (m.nvLinea + "") !== (elementos.linea + "")).length === 0) {
                $.ajax({
                    url: _url,
                    data: { ListaPrecio: "-1" },
                    type: "POST",
                    dataType: "json",
                    async: true,
                    success: (response) => {
                        if (response.filter(m => m.CodProd === elementos.codigo).length === 0) {
                            if (elementos.descripcion.length > 0 && elementos.codigo.length <= 50) {
                                if (elementos.valorNeto != "") {
                                    if (!isNaN(elementos.valorNeto)) {
                                        var moneda = JSON.parse($("#hfMonedas").val()).find(m => m.CodigoMoneda === elementos.tipoMoneda);

                                        if (((elementos.valorNeto.length - elementos.valorNeto.indexOf(".")) - 1) <= moneda.DecimalesPrecioMoneda || elementos.valorNeto.indexOf(".") === -1) {
                                            abrirConfirmacion("Producto Nuevo", "¿Esta seguro de modificar producto nuevo?", () => {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "/Reporte/ModificarProductoNuevo",
                                                    data: {
                                                        IdDetalle: elementos.idDetalleNotaVenta,
                                                        CodProdAntiguo: elementos.codigoAntiguo,
                                                        CodProd: elementos.codigo,
                                                        DesProd: elementos.descripcion,
                                                        CodGrupo: elementos.grupo,
                                                        CodSubGr: elementos.subGrupo,
                                                        PrecioVenta: elementos.valorNeto,
                                                        CodMon: elementos.tipoMoneda
                                                    },
                                                    async: true,
                                                    success: (response) => {
                                                        var onclickString = "";
                                                        onclickString = onclickString + "abrirModalModificarProductoNuevo(";
                                                        onclickString = onclickString + "" + elementos.idNotaVenta + ", ";
                                                        onclickString = onclickString + "" + elementos.idDetalleNotaVenta + ", ";
                                                        onclickString = onclickString + "" + elementos.linea + ", ";
                                                        onclickString = onclickString + "'" + elementos.codigo + "', ";
                                                        onclickString = onclickString + "'" + elementos.descripcion + "', ";
                                                        onclickString = onclickString + "'" + elementos.grupo + "', ";
                                                        onclickString = onclickString + "'" + elementos.subGrupo + "', ";
                                                        onclickString = onclickString + "'" + elementos.valorNeto + "', ";
                                                        onclickString = onclickString + "'" + elementos.tipoMoneda + "', ";
                                                        onclickString = onclickString + ")";

                                                        $("#td_detalleProducto_codigoProducto_" + elementos.idNotaVenta + "_linea_" + elementos.linea).html(elementos.codigo);
                                                        $("#td_detalleProducto_descripcionProducto_" + elementos.idNotaVenta + "_linea_" + elementos.linea).html(elementos.descripcion);

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "FacturasAprobadas",
                                                            data: { _nvId: elementos.idNotaVenta },
                                                            async: true,
                                                            success: (responseDetalle) => {
                                                                datosDetalle = responseDetalle.Detalle;
                                                            }
                                                        });
                                                        document.getElementById("btnModificarProductoNuevo_" + elementos.linea + "").setAttribute("onclick", onclickString);

                                                        abrirInformacion("Producto Nuevo", response.Mensaje);
                                                    }
                                                });
                                            });

                                            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                            $("#modalModificarProductoNuevoCerrar").click();
                                        }
                                        else {
                                            var mensaje = "";
                                            if (moneda.DecimalesPrecioMoneda > 0) {
                                                mensaje = "La cantidad de decimales permitidos es de " + moneda.DecimalesPrecioMoneda + " para el valor neto";
                                            }
                                            else {
                                                mensaje = "Valor neto no permite decimales para la moneda " + moneda.DescripcionMoneda;
                                            }
                                            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                            abrirError("Valor Neto", mensaje);
                                        }
                                    }
                                    else {
                                        desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                        abrirError("Valor Neto", "Valor neto debe ser un numero");
                                    }
                                }
                                else {
                                    desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                    abrirError("Valor Neto", "Valor neto es un campo obligatorio");
                                }
                            }
                            else {
                                desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                abrirError("Descripcion de Producto", "Descripcion de producto debe contener entre 1 y 50 caracteres");
                            }
                        }
                        else {
                            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                            abrirError("Producto", "Codigo de producto ya existe en el sistema");
                        }
                    }
                });
            }
            else {
                desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                abrirError("Codigo de Producto", "El codigo de producto ya fue ingresado dentro de la Nota de Venta");
            }
        }
        else {
            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
            abrirError("Codigo de Producto", "Codigo de producto debe contener entre 1 y 20 caracteres");
        }
    });
});

function DetalleNotaPedido(nvId, RutAux, estado, tipoUsuario) {
    var tableCabecera = $("#tblDetalleNotaPedido");
    var tableDetalle = $("#tblDetalleNotaPedidoDetalle");
    var htmlDetalle = "";
    var subtotal = 0;
    var displayPrecioConDescuento = true;
    var ivatotal = 0;
    var totalconiva = 0;
    var total = 0;

    tableCabecera.html("");
    tableDetalle.html("");
    $.ajax({
        type: "POST",
        url: "FacturasAprobadas",
        data: { _nvId: nvId },
        async: true,
        success: function (data) {
            if (data.Mensaje == 1) {
                alert("No se encontró el documento." + data.nvNum);
            }
            else {
                $("#tblDetalleNotaPedido").html("");
                $("#tblDetalleNotaPedidoDetalle").html("");

                $.each(data.Cabecera, function (index, value) {
                    subtotal = value.nvSubTotal;
                    totalconiva = value.TotalBoleta;
                    ivatotal = totalconiva - subtotal;

                    var vendedor = "Sin vendedor";
                    var listaPrecio = "Sin Lista de Precio";
                    var centroCosto = "Sin centro de costo";


                    if (value.VenDes !== null) {
                        vendedor = value.VenDes;
                    }
                    if (value.CodLista !== null) {
                        listaPrecio = value.CodLista + "-" + value.DesLista
                    }
                    if (value.CodiCC !== null) {
                        centroCosto = value.CodiCC + "-" + value.DescCC;
                    }
                    var htmlCabecera = "<tr><th nowrap='nowrap'>N&ordm; Int.</th>" +
                        "<td>" + nvId + "</td>" +
                        "<th nowrap='nowrap'>Vendedor</th>" +
                        "<td>" + vendedor + "</td></tr>" +

                        "<tr><th nowrap='nowrap'>Cond. Venta</th>" +
                        "<td>" + value.CveCod + "-" + value.CveDes + "</td>" +
                        "<th nowrap='nowrap'>Fecha Pedido</th>" +
                        "<td>" + value.nvFemString + "</td></tr>" +

                        "<tr><th nowrap='nowrap'>Fecha Entrega</th>" +
                        "<td>" + value.nvFeEntString + "</td>" +
                        "<th nowrap='nowrap'>Lista de Precio</th>" +
                        "<td>" + listaPrecio + "</td></tr>" +
                        "<tr><th nowrap='nowrap'>Cod. Cliente</th>" +
                        "<td>" + value.CodAux + "-" + value.NomAux + "</td>" +
                        "<th nowrap='nowrap'>Contacto</th>" +
                        "<td>" + value.NomCon + "</td></tr>" +

                        "<tr><th nowrap='nowrap'>Centro de Costo</th>" +
                        "<td>" + centroCosto + "</td>" +

                        "<th now rap='nowrap'>Nro Cotizacion</th>" +
                        "<td>" + value.NroFinalCotizacion + "</td></tr>" +

                        "<tr><th nowrap='nowrap'>Observacion</th>" +
                        "<td>" + value.nvObser + "</td></tr>";
                    tableCabecera.append(htmlCabecera);
                });

                htmlDetalle = "";
                htmlDetalle = htmlDetalle + "<th>ID</th>";
                htmlDetalle = htmlDetalle + "<th>Codigo Producto</th>";
                htmlDetalle = htmlDetalle + "<th>Detalle Producto</th>";
                if (parametros.ManejaTallaColor) {
                    htmlDetalle = htmlDetalle + "<th>Talla</th>";
                    htmlDetalle = htmlDetalle + "<th>Color</th>";
                }
                htmlDetalle = htmlDetalle + "<th>Cantidad</th>";
                htmlDetalle = htmlDetalle + "<th>Stock</th>";

                if (parametros.MuestraUnidadMedidaProducto) {
                    htmlDetalle = htmlDetalle + "<th>U.Medida</th>";
                }

                htmlDetalle = htmlDetalle + "<th>Precio</th>";
                htmlDetalle = htmlDetalle + "<th>Sub Total</th>";
                if (parametros.DescuentoLineaDirectoSoftland) {
                    for (z = 0; z < parametros.CantidadDescuentosProducto; z++) {
                        htmlDetalle = htmlDetalle + "<th>Desc. N°" + (z + 1) + " (%)</th>";
                    }
                }
                if (displayPrecioConDescuento) {
                    htmlDetalle = htmlDetalle + "<th>Precio C/Descuento</th>";
                }
                htmlDetalle = htmlDetalle + "<th>Total</th>";
                if (estado === "P" && tipoUsuario != 2) {
                    htmlDetalle = htmlDetalle + "<th></th>";
                }

                tableDetalle.append(htmlDetalle);
                datosDetalle = data.Detalle;
                $.each(data.Detalle, function (index, value) {
                    htmlDetalle = "";
                    if (parametros.ControlaStockProducto) {
                        if (value.Stock < value.nvCant) {
                            htmlDetalle = htmlDetalle + "<tr style='background-color: red; color: white'>";
                        }
                        else {
                            htmlDetalle = htmlDetalle + "<tr>";
                        }
                    }
                    htmlDetalle = htmlDetalle + "<tr>";
                    htmlDetalle = htmlDetalle + "<td>" + value.nvLinea + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_codigoProducto_" + nvId + "_linea_" + value.nvLinea + "'>" + value.CodProd + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_descripcionProducto_" + nvId + "_linea_" + value.nvLinea + "'>" + value.DesProd + "</td>";
                    if (parametros.ManejaTallaColor) {
                        htmlDetalle = htmlDetalle + "<td>" + value.Partida + "</td>";
                        htmlDetalle = htmlDetalle + "<td>" + value.Pieza + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvCant) + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.Stock) + "</td>";

                    if (parametros.MuestraUnidadMedidaProducto) {
                        htmlDetalle = htmlDetalle + "<td>" + value.CodUMed + "</td>";
                    }

                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvPrecio, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvSubTotal, "$") + "</td>";
                    if (parametros.DescuentoLineaDirectoSoftland) {
                        for (z = 0; z < parametros.CantidadDescuentosProducto; z++) {
                            if (z === 0) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc01 + "</td>"; }
                            if (z === 1) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc02 + "</td>"; }
                            if (z === 2) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc03 + "</td>"; }
                            if (z === 3) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc04 + "</td>"; }
                            if (z === 4) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc05 + "</td>"; }
                        }
                    }
                    var totalPrecioConDescuento = 0;
                    if (displayPrecioConDescuento) {
                        totalPrecioConDescuento = (value.nvPrecio);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc01)) ? 0 : value.nvDPorcDesc01)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc02)) ? 0 : value.nvDPorcDesc02)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc03)) ? 0 : value.nvDPorcDesc03)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc04)) ? 0 : value.nvDPorcDesc04)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc05)) ? 0 : value.nvDPorcDesc05)) / 100);

                        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(parseInt(totalPrecioConDescuento), "$") + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvTotLinea, "$") + "</td>";
                    if (estado === "P" && tipoUsuario != 2) {
                        if (value.EsProductoNuevo) {
                            htmlDetalle = htmlDetalle + "<td style='text-align: right;'>";
                            htmlDetalle = htmlDetalle + "<button id='btnModificarProductoNuevo_" + value.nvLinea + "' class='btn btn-primary'";
                            htmlDetalle = htmlDetalle + "onclick=\"abrirModalModificarProductoNuevo(";
                            htmlDetalle = htmlDetalle + "" + nvId + ", ";
                            htmlDetalle = htmlDetalle + "" + value.Id + ", ";
                            htmlDetalle = htmlDetalle + "" + value.nvLinea + ", ";
                            htmlDetalle = htmlDetalle + "'" + value.CodProd + "', ";
                            htmlDetalle = htmlDetalle + "'" + value.DesProd + "', ";
                            htmlDetalle = htmlDetalle + "'" + value.CodGrupo + "', ";
                            htmlDetalle = htmlDetalle + "'" + value.CodSubGr + "', ";
                            htmlDetalle = htmlDetalle + "'" + value.ValorNetoProducto + "', ";
                            htmlDetalle = htmlDetalle + "'" + value.CodMon + "', ";
                            htmlDetalle = htmlDetalle + ")\"";
                            htmlDetalle = htmlDetalle + ">";
                            htmlDetalle = htmlDetalle + "<i class='fa fa-edit'></i>";
                            htmlDetalle = htmlDetalle + "</button>";
                            htmlDetalle = htmlDetalle + "</td>";
                        }
                        else {
                            htmlDetalle = htmlDetalle + "<td style='text-align: right;'>";
                            htmlDetalle = htmlDetalle + "</td>";
                        }
                    }
                    htmlDetalle = htmlDetalle + "</tr>";


                    var subtotalaux = value.nvSubTotal;

                    var ivaaux = (value.nvPrecio * value.nvCant) * 0.19;

                    var totalaux = value.nvTotLinea;
                    total = total + totalaux;

                    tableDetalle.append(htmlDetalle);
                });

                var colspanTotales = 7;
                if (parametros.ManejaTallaColor) {
                    colspanTotales = colspanTotales + 1;
                    colspanTotales = colspanTotales + 1;
                }
                if (displayPrecioConDescuento) {
                    colspanTotales = colspanTotales + 1;
                }
                if (parametros.DescuentoLineaDirectoSoftland) {
                    colspanTotales = colspanTotales + parseInt(parametros.CantidadDescuentosProducto);
                }

                var htmldetalleTotal = "";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>SubTotal</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(subtotal, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Total Iva</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(ivatotal, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Venta Total</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(totalconiva, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                tableDetalle.append(htmldetalleTotal);
            }
        }
    });

}

function abrirModalModificarProductoNuevo(nvId, nvIdDetalle, linea, codProd, desProd, codGrupo, codSubGrupo, valorNeto, codMon) {
    $("#modalModificarProductoNuevoIdNotaVenta").val(nvId);
    $("#modalModificarProductoNuevoIdDetalleNotaVenta").val(nvIdDetalle);
    $("#modalModificarProductoNuevoLinea").val(linea);
    $("#modalModificarProductoNuevoCodigoAntiguo").val(codProd);
    $("#modalModificarProductoNuevoCodigo").val(codProd);
    $("#modalModificarProductoNuevoDescripcion").val(desProd);
    $("#modalModificarProductoNuevoGrupo").val(codGrupo);
    $("#modalModificarProductoNuevoSubGrupo").val(codSubGrupo);
    $("#modalModificarProductoNuevoValorNeto").val(valorNeto);
    $("#modalModificarProductoNuevoTipoMoneda").val(codMon);

    $("#modalModificarProductoNuevoCodigo").focus();

    $("#aModalModificarProductoNuevo").click();
}
function AprobarNotaVenta(nvId, id) {
    abrirConfirmacion("Aprobar", "¿Esta seguro de aprobar la nota de venta N°" + nvId + "?", () => {
        activarLoadingBoton(id);
        $.ajax({
            type: "POST",
            url: "/Reporte/AprobarNotaVenta",
            data: { _nvId: nvId },
            async: true,
            success: function (data) {
                abrirConfirmacion("Nota Venta", "Numero Nota de Venta:" + data.nvNum, function () { location.reload() });
                //alert("Numero Nota de Venta:" + data.nvNum);
                desactivarLoadingBoton(id);
                //location.reload();
            }
        });
    }, null);
}

function RechazarNotaVenta(nvId, nvNum) {
    abrirConfirmacion("Rechazar", "¿Esta seguro de rechazar la nota de venta N°" + nvId + "?", () => {
        $.ajax({
            type: "POST",
            url: "/Reporte/RechazarNotaVenta",
            data: { _nvId: nvId, _nvNum: nvNum },
            async: true,
            success: function (data) {
                alert("Numero Nota de Venta Rechazada: " + data.nvNum);
                location.reload();
            }
        });
    }, null, true);
}
function ObtenerSaldo(RutAux, CodAux, Nombre, Saldo) {
    $("#modalSaldoSubtitulo").text(CodAux + " - " + Nombre);
    $("#tblSaldos").html("");
    $.ajax({
        type: "POST",
        url: "/Venta/ObtenerSaldo",
        data: { RutAuxiliar: RutAux, CodAux: CodAux },
        async: true,
        success: function (response) {
            console.log(response);
            if (response == 1) {
                alert("Cliente sin Saldo.");
            }
            else {

                var tblSaldos = $("#tblSaldos");

                //$.each(data.Cabecera, function (index, value) {

                var htmlCabecera = "";
                var htmlDetalle = "";

                htmlCabecera = htmlCabecera + "<th>Cuenta</th>";
                htmlCabecera = htmlCabecera + "<th>Desc Cuenta</th>";
                htmlCabecera = htmlCabecera + "<th>CodigoAux</th>";
                htmlCabecera = htmlCabecera + "<th>RutCliente</th>";
                htmlCabecera = htmlCabecera + "<th>Nombre</th>";
                htmlCabecera = htmlCabecera + "<th>FecEmision</th>";
                htmlCabecera = htmlCabecera + "<th>TipoDoc</th>";
                htmlCabecera = htmlCabecera + "<th>NomDoc</th>";
                htmlCabecera = htmlCabecera + "<th>Saldo</th>";
                htmlCabecera = htmlCabecera + "<th>Cod Documento</th>";
                htmlCabecera = htmlCabecera + "<th>Año Comprobante</th>";
                htmlCabecera = htmlCabecera + "<th>Fecha Ven</th>";

                tblSaldos.append(htmlCabecera);

                $.each(response.DetalleSaldo, function (index, value) {
                    htmlDetalle = "";
                    htmlDetalle = htmlDetalle + "<tr>";
                    htmlDetalle = htmlDetalle + "<td>" + value.pccodi + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.pcdesc + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.codaux + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.RutAux + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.nomaux + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.fechaemiString + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.desdoc + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.movnumdocref + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.Saldo, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.coddoc + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.Cpbano + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.movfvString + "</td>";

                    htmlDetalle = htmlDetalle + "</tr>";

                    tblSaldos.append(htmlDetalle);
                });
            }
        }
    });
}