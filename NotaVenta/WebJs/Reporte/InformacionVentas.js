﻿$(document).ready(function () {
    $("#VendedoresSelect").val(-3);
    function getGET() {
        // capturamos la url
        var loc = document.location.href;
        // si existe el interrogante
        if (loc.indexOf('?') > 0) {
            // cogemos la parte de la url que hay despues del interrogante
            var getString = loc.split('?')[1];
            // obtenemos un array con cada clave=valor
            var GET = getString.split('&');
            var get = {};

            // recorremos todo el array de valores
            for (var i = 0, l = (GET.length - 1); i < l; i++) {
                var tmp = GET[i].split('=');
                get[tmp[0]] = unescape(decodeURI(tmp[1]));
            }
            return get;
        }
    }

    //span datepicker con fecha de parametros

    var valores = getGET();
    var cont = 0;
    var estado = 0;
    if (valores) {
        // hacemos un bucle para pasar por cada indice del array de valores
        for (var index in valores) {
            console.log("<br>clave: " + index + " - valor: " + valores[index]);
            if (cont == 0) {
                var fecha = valores[index];
                var fechaAuxi = fecha.replace("-", "/");
                fechaAuxi = fechaAuxi.replace("-", "/");
                var fechaSeparador = fechaAuxi.split("/");
                fechaDesde = fechaSeparador[2] + "/" + fechaSeparador[1] + "/" + fechaSeparador[0];
                cont++;
            } else {
                var fecha = valores[index];
                var fechaAuxi = fecha.replace("-", "/");
                fechaAuxi = fechaAuxi.replace("-", "/");
                var fechaSeparador = fechaAuxi.split("/");
                fechaHasta = fechaSeparador[2] + "/" + fechaSeparador[1] + "/" + fechaSeparador[0];
            }
        }
        estado = 1;
    } else {
        // no se ha recibido ningun parametro por GET
        fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
        fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");
    }

    ///////////////////////

    $("#btnCrearReporte").click(function () {
        activarLoadingBoton("descargarReporteFechas");
        //$("#divCreandoReporte").fadeOut();
        //$("#divCreandoReporteCargando").fadeIn();
        //$("#divCreandoReporteCreado").fadeOut();
        //$("#divCreandoReporteError").fadeOut();

        if (fechaDesde !== "" && fechaHasta !== "") {
            //$("#divCreandoReporte").fadeIn();
            $.ajax({
                type: "POST",
                url: "GenerarReporteVentas",
                data: {
                    fechaDesde: fechaDesde,
                    fechaHasta: fechaHasta
                },
                async: true,
                success: function (response) {
                    if (response.Verificador) {
                        $("#divCreandoReporteCargando").fadeOut("", function () {
                            desactivarLoadingBoton("descargarReporteFechas");
                            window.open("DescargarArchivoVentas");
                            //$("#divCreandoReporteCreado").fadeIn();
                        });
                    }
                    else {
                        $("#divCreandoReporte").fadeOut("", function () {
                            $("#divCreandoReporteError").fadeIn();
                        });
                    }
                },
                error: function (a, b, c) {
                    $("#divCreandoReporteError").fadeIn();
                }
            });
        }
        else {
            abrirError("Ingreso Datos", "Favor debe ingresar todos los datos para continuar");
        }
        $("#divCreandoReporte").fadeOut();
    });

    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoVentas");
    });

    if (estado == 0) {
        $('#rangoFecha span').html(fechaSeleccionada.fechaDesde.format('DD/MM/YYYY') + ' - ' + fechaSeleccionada.fechaHasta.format('DD/MM/YYYY'));
    } else {
        $('#rangoFecha span').html(fechaDesde + ' - ' + fechaHasta);
    }

    $('#rangoFecha').daterangepicker({
        format: 'DD/MM/YYYY',
        //startDate: moment().subtract(7, 'days'),
        startDate: moment().startOf('month'),
        endDate: moment(),
        minDate: '01/01/2012',
        //maxDate: '12/31/2015',
        //dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Aceptar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'a',
            customRangeLabel: 'Personalizado',
            daysOfWeek: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            firstDay: 1
        }
    }, function (start, end, label) {
        fechaSeleccionada.fechaDesde = start;
        fechaSeleccionada.fechaHasta = end;
        $('#rangoFecha span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
    });
});

var qs = (function (a) {
    if (a == "") return {};
    var b = {};
    for (var i = 0; i < a.length; ++i) {
        var p = a[i].split('=', 2);
        if (p.length == 1)
            b[p[0]] = "";
        else
            b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, " "));
    }
    return b;
})(window.location.search.substr(1).split('&'));

var fechaSeleccionada = {
    fechaDesde: moment().startOf('month'),
    fechaHasta: moment()
};

function crearReporte(VenCod) {
    var venCod = VenCod == undefined ? "-3" : VenCod;
    function getGET() {
        // capturamos la url
        var loc = document.location.href;
        // si existe el interrogante
        if (loc.indexOf('?') > 0) {
            // cogemos la parte de la url que hay despues del interrogante
            var getString = loc.split('?')[1];
            // obtenemos un array con cada clave=valor
            var GET = getString.split('&');
            var get = {};

            // recorremos todo el array de valores
            for (var i = 0, l = GET.length - 1; i < l; i++) {
                var tmp = GET[i].split('=');
                get[tmp[0]] = unescape(decodeURI(tmp[1]));
            }
            return get;
        }
    }

    //span datepicker con fecha de parametros

    var valores = getGET();
    var fechaDesde;
    var fechaHasta;
    var cont = 0;

    if (valores) {
        // hacemos un bucle para pasar por cada indice del array de valores
        for (var index in valores) {
            console.log("<br>clave: " + index + " - valor: " + valores[index]);
            if (cont == 0) {
                var fecha = valores[index];
                fechaDesde = fecha;
                cont++;
            } else {
                var fecha = valores[index];
                fechaHasta = fecha;
            }
        }
    } else {
        // no se ha recibido ningun parametro por GET
        fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
        fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");
    }
    activarLoadingBoton("descargarReporteFechas");
    //$("#divCreandoReporte").fadeOut();
    //$("#divCreandoReporteCargando").fadeIn();
    //$("#divCreandoReporteCreado").fadeOut();
    //$("#divCreandoReporteError").fadeOut();

    if (fechaDesde !== "" && fechaHasta !== "") {
        $.ajax({
            type: "POST",
            url: "GenerarReporteVentas",
            data: {
                fechaDesde: fechaDesde,
                fechaHasta: fechaHasta,
                venCod
            },
            async: true,
            success: function (response) {
                if (response.Verificador) {
                    desactivarLoadingBoton("descargarReporteFechas");
                    window.open("DescargarArchivoVentas");
                    //$("#divCreandoReporteCargando").fadeOut("", function () {
                    //    $("#divCreandoReporteCreado").fadeIn();
                    //});
                }
                else {
                    $("#divCreandoReporte").fadeOut("", function () {
                        $("#divCreandoReporteError").fadeIn();
                    });
                }
            },
            error: function (a, b, c) {
                $("#divCreandoReporteError").fadeIn();
            }
        });
    }
    else {
        abrirError("Ingreso Datos", "Favor debe ingresar todos los datos para continuar");
    }
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoVentas");
    });
}

function crearReporteDia(VenCod) {
    var venCod = VenCod == undefined || VenCod == '' ? "-3" : VenCod;
    activarLoadingBoton("btnCrearReporteDia");
    //$("#divCreandoReporte").fadeOut();
    //$("#divCreandoReporteCargando").fadeIn();
    //$("#divCreandoReporteCreado").fadeOut();
    //$("#divCreandoReporteError").fadeOut();


    $.ajax({
        type: "POST",
        url: "GenerarReporteVentasDia",
        data: {venCod},
        async: true,
        success: function (response) {
            if (response.Verificador) {
                desactivarLoadingBoton("btnCrearReporteDia");
                window.open("DescargarArchivoVentas");

            }
            else {
                $("#divCreandoReporte").fadeOut("", function () {
                    $("#divCreandoReporteError").fadeIn();
                });
            }
        },
        error: function (a, b, c) {
            $("#divCreandoReporteError").fadeIn();
        }
    });
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoVentas");
    });
}

function crearReporteNV(VenCod) {
    var venCod = VenCod == undefined ? "-3" : VenCod;
    function getGET() {
        // capturamos la url
        var loc = document.location.href;
        // si existe el interrogante
        if (loc.indexOf('?') > 0) {
            // cogemos la parte de la url que hay despues del interrogante
            var getString = loc.split('?')[1];
            // obtenemos un array con cada clave=valor
            var GET = getString.split('&');
            var get = {};

            // recorremos todo el array de valores
            for (var i = 0, l = GET.length - 1; i < l; i++) {
                var tmp = GET[i].split('=');
                get[tmp[0]] = unescape(decodeURI(tmp[1]));
            }
            return get;
        }
    }

    //span datepicker con fecha de parametros

    var valores = getGET();
    var fechaDesde;
    var fechaHasta;
    var cont = 0;

    if (valores) {
        // hacemos un bucle para pasar por cada indice del array de valores
        for (var index in valores) {
            console.log("<br>clave: " + index + " - valor: " + valores[index]);
            if (cont == 0) {
                var fecha = valores[index];
                fechaDesde = fecha;
                cont++;
            } else {
                var fecha = valores[index];
                fechaHasta = fecha;
            }
        }
    } else {
        // no se ha recibido ningun parametro por GET
        fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
        fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");
    }
    activarLoadingBoton("descargarReporteBooking");
    //$("#divCreandoReporte").fadeOut();
    //$("#divCreandoReporteCargando").fadeIn();
    //$("#divCreandoReporteCreado").fadeOut();
    //$("#divCreandoReporteError").fadeOut();


    if (fechaDesde !== "" && fechaHasta !== "") {
        $.ajax({
            type: "POST",
            url: "GenerarReporteNV",
            data: {
                fechaDesde: fechaDesde,
                fechaHasta: fechaHasta,
                venCod
            },
            async: true,
            success: function (response) {
                if (response.Verificador) {
                    desactivarLoadingBoton("descargarReporteBooking");
                    window.open("DescargarArchivoNVBooking");

                    //$("#divCreandoReporteCargando").fadeOut("", function () {
                    //    $("#divCreandoReporteCreado").fadeIn();
                    //});
                }
                else {
                    $("#divCreandoReporte").fadeOut("", function () {
                        $("#divCreandoReporteError").fadeIn();
                    });
                }
            },
            error: function (a, b, c) {
                $("#divCreandoReporteError").fadeIn();
            }
        });
    }
    else {
        abrirError("Ingreso Datos", "Favor debe ingresar todos los datos para continuar");
    }
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoNV");
    });
}

function crearReporteNVBacklog(VenCod) {
    var venCod = VenCod == undefined ? "-3" : VenCod;
    function getGET() {
        // capturamos la url
        var loc = document.location.href;
        // si existe el interrogante
        if (loc.indexOf('?') > 0) {
            // cogemos la parte de la url que hay despues del interrogante
            var getString = loc.split('?')[1];
            // obtenemos un array con cada clave=valor
            var GET = getString.split('&');
            var get = {};

            // recorremos todo el array de valores
            for (var i = 0, l = GET.length - 1; i < l; i++) {
                var tmp = GET[i].split('=');
                get[tmp[0]] = unescape(decodeURI(tmp[1]));
            }
            return get;
        }
    }

    //span datepicker con fecha de parametros

    var valores = getGET();
    var fechaDesde;
    var fechaHasta;
    var cont = 0;

    if (valores) {
        // hacemos un bucle para pasar por cada indice del array de valores
        for (var index in valores) {
            console.log("<br>clave: " + index + " - valor: " + valores[index]);
            if (cont == 0) {
                var fecha = valores[index];
                fechaDesde = fecha;
                cont++;
            } else {
                var fecha = valores[index];
                fechaHasta = fecha;
            }
        }
    } else {
        // no se ha recibido ningun parametro por GET
        fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
        fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");
    }
    activarLoadingBoton("descargarReporteBacklog");
    //$("#divCreandoReporte").fadeOut();
    //$("#divCreandoReporteCargando").fadeIn();
    //$("#divCreandoReporteCreado").fadeOut();
    //$("#divCreandoReporteError").fadeOut();


    if (fechaDesde !== "" && fechaHasta !== "") {
        $.ajax({
            type: "POST",
            url: "GenerarReporteNVBacklog",
            data: {
                fechaDesde: fechaDesde,
                fechaHasta: fechaHasta,
                venCod
            },
            async: true,
            success: function (response) {
                if (response.Verificador) {
                    desactivarLoadingBoton("descargarReporteBacklog");
                    window.open("DescargarArchivoNVBlacklog");

                    //$("#divCreandoReporteCargando").fadeOut("", function () {
                    //    $("#divCreandoReporteCreado").fadeIn();
                    //});
                }
                else {
                    $("#divCreandoReporte").fadeOut("", function () {
                        $("#divCreandoReporteError").fadeIn();
                    });
                }
            },
            error: function (a, b, c) {
                $("#divCreandoReporteError").fadeIn();
            }
        });
    }
    else {
        abrirError("Ingreso Datos", "Favor debe ingresar todos los datos para continuar");
    }
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoNV");
    });
}

function generarDashBoard() {
    var fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
    var fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");
    var esAprobador = $("#VendedoresSelect").val();
    window.location = '/Reporte/InformacionVentas?fechaDesde=' + fechaDesde + '&fechaHasta=' + fechaHasta + '&esAprobador=' + esAprobador;
}

//Formateo pesos
const formatterPeso = new Intl.NumberFormat('es-CO', {
    style: 'currency',
    currency: 'COP',
    minimumFractionDigits: 0
});
//Notas de Venta
function modalNV(codigo, estado, nombre) {
    var fd = fechaDesde;
    var fh = fechaHasta;
    //Se da vuelta la fecha
    if (fd.charAt(2) == '/') {
        fd = fd.split('/').reverse().join('/');
        fh = fh.split('/').reverse().join('/');
    }
    if (nombre.Length >= 23) {
        nombre = nombre.Substring(0, 23);
    }
    if (estado == 'A') {
        $('#titleModalNV').html('Notas de Venta - ' + nombre.toLowerCase());
        $.ajax({
            type: "POST",
            url: "NotasDeVentaCliente",
            data: {
                codigo: codigo,
                fd: fd,
                fh: fh,
                estado: estado
            },
            async: true,
            dataType: "JSON",
            success: function (response) {
                var html = '<thead><tr>';
                html += '<th scope="col">Estado</th>';
                html += '<th scope="col">Numero Nota De Venta</th>';
                html += '<th scope="col">Fecha</th>';
                html += '<th scope="col" style="text-align:right">Monto</th>';
                html += '<th scope="col"></th></tr ></thead ><tbody>';
                var i;
                var total = 0;
                for (i = 0; i < response.data.length; i++) {
                    html += '<tr>' +
                        '<td>' + response.data[i].nvestado + '</td>' +
                        '<td>' + response.data[i].nvNumero + '</td>' +
                        '<td>' + response.data[i].nvfem + '</td>' +
                        '<td style="text-align:right">' + formatterPeso.format(response.data[i].nvmonto) + '</td>' +
                        '<td style="text-align:center"><button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modalProductos" onclick="modalProductos(' + response.data[i].nvNumero + ',1)">Ver Productos</button></td>' +
                        '</tr>';
                    total += response.data[i].nvmonto;
                }
                html += '<tr><td></td><td></td>';
                html += '<td style="text-align:right">Total</td>';
                html += '<td style="text-align:right">' + formatterPeso.format(total) + '</td>';
                html += '<td></td></tr>';
                html += '</tbody>';
                $('#tablaNotasDeVenta').html(html);
            },
            error: function (a, b, c) {
                alert('Error...');
            }
        });
    }
    //Blacklog
    else if (estado == 'P') {
        $('#titleModalNVPen').html('Notas de Venta - ' + nombre.toLowerCase());
        $.ajax({
            type: "POST",
            url: "NotasDeVentaClienteBlacklog",
            data: {
                codigo: codigo,
                estado: "A"
            },
            async: true,
            dataType: "JSON",
            success: function (response) {
                var html = '<thead><tr>';
                html += '<th scope="col">Estado</th>';
                html += '<th scope="col">Numero Nota De Venta</th>';
                html += '<th scope="col">Fecha</th>';
                html += '<th scope="col" style="text-align:right">Monto</th>';
                html += '<th scope="col"></th></tr ></thead ><tbody>';
                var i;
                var total = 0;
                for (i = 0; i < response.data.length; i++) {
                    html += '<tr>' +
                        '<td>' + response.data[i].nvestado + '</td>' +
                        '<td>' + response.data[i].nvNumero + '</td>' +
                        '<td>' + response.data[i].nvfem + '</td>' +
                        '<td style="text-align:right">' + formatterPeso.format(response.data[i].nvmonto) + '</td>' +
                        '<td style="text-align:center"><button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modalProductosPen" onclick="modalProductos(' + response.data[i].nvNumero + ',2)">Ver Productos</button></td>' +
                        '</tr>';
                    total += response.data[i].nvmonto;
                }
                html += '<tr><td></td><td></td>';
                html += '<td style="text-align:right">Total</td>';
                html += '<td style="text-align:right">' + formatterPeso.format(total) + '</td>';
                html += '<td></td></tr>';
                html += '</tbody>';
                $('#tablaNotasDeVentaPen').html(html);

            },
            error: function (a, b, c) {
                alert('Error...');
            }
        });
    }



}
function modalProductos(nvnumero, flag) {
    if (flag == 1) {
        $('#titleModalPro').html('Productos -  Nv: N° ' + nvnumero);
        $.ajax({
            type: "POST",
            url: "Productos",
            data: {
                nvnumero: nvnumero
            },
            async: true,
            dataType: "JSON",
            success: function (response) {
                var html = '<thead><tr>';
                html += '<th scope="col">Fecha de Compra</th>';
                html += '<th scope="col">Descripción</th>';
                html += '<th scope="col">Cantidad</th>';
                html += '<th scope="col" style="text-align:right">Precio</th>';
                html += '<th scope="col" style="text-align:right">Total</th></tr ></thead ><tbody>';
                html += '</tr ></thead ><tbody>';
                var i;
                var total2 = 0;
                for (i = 0; i < response.data.length; i++) {
                    html += '<tr>' +
                        '<td>' + response.data[i].nvFecCompr + '</td>' +
                        '<td>' + response.data[i].DesProd + '</td>' +
                        '<td>' + response.data[i].nvCant + '</td>' +
                        '<td style="text-align:right">' + formatterPeso.format(response.data[i].nvPrecio) + '</td>' +
                        '<td style="text-align:right">' + formatterPeso.format(response.data[i].nvTotLinea) + '</td>' +
                        '</tr>';
                    total2 += response.data[i].nvTotLinea;
                }
                html += '<tr><td></td><td></td><td></td>';
                html += '<td style="text-align:right">Total</td>';
                html += '<td style="text-align:right">' + formatterPeso.format(total2) + '</td>';
                html += '</tr>';
                html += '</tbody>';
                $('#tablaProductos').html(html);

            },
            error: function (a, b, c) {
                alert('Error...');
            }
        });
    } else {
        $('#titleModalProPen').html('Productos - Nv: N° ' + nvnumero);
        $.ajax({
            type: "POST",
            url: "ProductosBlacklog",
            data: {
                nvnumero: nvnumero
            },
            async: true,
            dataType: "JSON",
            success: function (response) {
                var html = '<thead><tr>';
                html += '<th scope="col">Fecha de Compra</th>';
                html += '<th scope="col">Descripción</th>';
                html += '<th scope="col">Cantidad</th>';
                html += '<th scope="col" style="text-align:right">Precio</th>';
                html += '<th scope="col" style="text-align:right">Total</th></tr ></thead ><tbody>';
                html += '</tr ></thead ><tbody>';
                var i;
                var total2 = 0;
                for (i = 0; i < response.data.length; i++) {
                    html += '<tr>' +
                        '<td>' + response.data[i].nvFecCompr + '</td>' +
                        '<td>' + response.data[i].DesProd + '</td>' +
                        '<td>' + response.data[i].nvCant + '</td>' +
                        '<td style="text-align:right">' + formatterPeso.format(response.data[i].nvPrecio) + '</td>' +
                        '<td style="text-align:right">' + formatterPeso.format(response.data[i].nvTotLinea) + '</td>' +
                        '</tr>';
                    total2 += response.data[i].nvTotLinea;
                }
                html += '<tr><td></td><td></td><td></td>';
                html += '<td style="text-align:right">Total</td>';
                html += '<td style="text-align:right">' + formatterPeso.format(total2) + '</td>';
                html += '</tr>';
                html += '</tbody>';
                $('#tablaProductosPen').html(html);
            },
            error: function (a, b, c) {
                alert('Error...');
            }
        });
    }

}
//Ventas
function modalDetalleVentas(codigo, nombre) {
    var fd = fechaDesde;
    var fh = fechaHasta;
    if (nombre.Length >= 23) {
        nombre = nombre.Substring(0, 23);
    }
    $('#titleModalDetalleVentas').html('Ventas - ' + nombre.toLowerCase());

    //Doy vuelta la fecha para no hacer 58 mil lineas de codigo y procedimientos anterior anterior del anterior anterior
    if (fd.charAt(2) == '/') {
        fd = fd.split('/').reverse().join('/');
        fh = fh.split('/').reverse().join('/');
    }
    $.ajax({
        type: "POST",
        url: "DetalleVentas",
        data: {
            codigo: codigo,
            fd: fd,
            fh: fh
        },
        async: true,
        dataType: "JSON",
        success: function (response) {
            var html = '<thead><tr>';
            html += '<th scope="col">Tipo</th>';
            html += '<th scope="col">Folio</th>';
            html += '<th scope="col">Fecha</th>';
            html += '<th scope="col" style="text-align:right">Total</th>';
            html += '<th scope="col"></th></tr ></thead ><tbody>';
            var i;
            var total = 0;
            for (i = 0; i < response.data.length; i++) {
                let tipo = "'" + response.data[i].Tipo + "'";
                html += '<tr>' +
                    '<td>' + response.data[i].Tipo + '</td>' +
                    '<td>' + response.data[i].Folio + '</td>' +
                    '<td>' + response.data[i].Fecha + '</td>' +
                    '<td style="text-align:right">' + formatterPeso.format(response.data[i].Total) + '</td>' +
                    '<td style="text-align:center"><button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#modalProductosVenta" onclick="modalProductosVenta(' + response.data[i].Folio + ',' + tipo + ')">Ver Productos</button></td>' +
                    '</tr>';
                total += response.data[i].Total;
            }
            html += '<tr><td></td><td></td>';
            html += '<td style="text-align:right">Total</td>';
            html += '<td style="text-align:right">' + formatterPeso.format(total) + '</td>';
            html += '<td></td></tr>';
            html += '</tbody>';
            $('#tablaDetalleVentas').html(html);
        },
        error: function (a, b, c) {
            alert('Error...');
        }
    });
}

function modalProductosVenta(folio, tipo) {

    $('#titleModalProVenta').html('Productos -  Venta: N° ' + folio);

    $.ajax({
        type: "POST",
        url: "ProductosVenta",
        data: {
            folio: folio,
            tipo: tipo
        },
        async: true,
        dataType: "JSON",
        success: function (response) {
            var html = '<thead><tr>';
            html += '<th scope="col">Folio</th>';
            html += '<th scope="col">Descripción</th>';
            html += '<th scope="col">Cantidad</th>';
            html += '<th scope="col" style="text-align:right">Precio</th>';
            html += '<th scope="col" style="text-align:right">Total</th></tr ></thead ><tbody>';
            html += '</tr ></thead ><tbody>';
            var i;
            var total2 = 0;
            for (i = 0; i < response.data.length; i++) {
                html += '<tr>' +
                    '<td>' + response.data[i].Folio + '</td>' +
                    '<td>' + response.data[i].DetProd + '</td>' +
                    '<td>' + response.data[i].CantFacturada + '</td>' +
                    '<td style="text-align:right">' + formatterPeso.format(response.data[i].PreUniMB) + '</td>' +
                    '<td style="text-align:right">' + formatterPeso.format(response.data[i].TotLinea) + '</td>' +
                    '</tr>';
                total2 += response.data[i].TotLinea;
            }
            html += '<tr><td></td><td></td><td></td>';
            html += '<td style="text-align:right">Total</td>';
            html += '<td style="text-align:right">' + formatterPeso.format(total2) + '</td>';
            html += '</tr>';
            html += '</tbody>';
            $('#tablaProductosVenta').html(html);
        },
        error: function (a, b, c) {
            alert('Error...');
        }
    });
}

function getGET() {
    // capturamos la url
    var loc = document.location.href;
    // si existe el interrogante
    if (loc.indexOf('?') > 0) {
        // cogemos la parte de la url que hay despues del interrogante
        var getString = loc.split('?')[1];
        // obtenemos un array con cada clave=valor
        var GET = getString.split('&');
        var get = {};

        // recorremos todo el array de valores
        for (var i = 0, l = GET.length - 1; i < l; i++) {
            var tmp = GET[i].split('=');
            get[tmp[0]] = unescape(decodeURI(tmp[1]));
        }
        return get;
    }
}

function listarVendedores(bandera) {//1 sin filtro -- 0 con filtro
    var valores = getGET();
    var cont = 0;
    var estado = 0;
    if (valores) {
        // hacemos un bucle para pasar por cada indice del array de valores
        for (var index in valores) {
            console.log("<br>clave: " + index + " - valor: " + valores[index]);
            if (cont == 0) {
                var fecha = valores[index];
                var fechaAuxi = fecha.replace("-", "/");
                fechaAuxi = fechaAuxi.replace("-", "/");
                var fechaSeparador = fechaAuxi.split("/");
                fechaDesde = fechaSeparador[2] + "/" + fechaSeparador[1] + "/" + fechaSeparador[0];
                cont++;
            } else {
                var fecha = valores[index];
                var fechaAuxi = fecha.replace("-", "/");
                fechaAuxi = fechaAuxi.replace("-", "/");
                var fechaSeparador = fechaAuxi.split("/");
                fechaHasta = fechaSeparador[2] + "/" + fechaSeparador[1] + "/" + fechaSeparador[0];
            }
        }
        estado = 1;
    } else {
        // no se ha recibido ningun parametro por GET
        fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
        fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");
    }
    var fd = fechaDesde;
    var fh = fechaHasta;

    if (fd.charAt(2) == '/') {
        fd = fd.split('/').reverse().join('/');
        fh = fh.split('/').reverse().join('/');
    }
    $.ajax({
        type: 'POST',
        url: 'buscarVendedorActual',
        data: { bandera, fd, fh },
        success: function (response) {
            $("#tblVendedores thead").html("");
            var thTablaVendedores = "";
            var filaThVendedores = "";
            thTablaVendedores = $("#tblVendedores thead");
            filaThVendedores += "<tr>";
            filaThVendedores += "<th scope='col'>Nombre</th>";
            filaThVendedores += "<th scope='col'>Ventas</th>"
            filaThVendedores += "</tr>";
            thTablaVendedores.append(filaThVendedores);

            $("#tblVendedores tbody").html("");
            var tbTblVendedores = "";
            tbTblVendedores = $("#tblVendedores tbody");
            var filaTbTblVendedores = "";

            $.each(response, function (index, item) {
                if (bandera == 1) {
                    if (item.nombreVendedor != null) {
                        filaTbTblVendedores += "<tr>";
                        filaTbTblVendedores += "<td>" + item.nombreVendedor + "</td>";
                        filaTbTblVendedores += "<td style='text-align:right'>" + formatterPeso.format(item.VentaTotalVendedor) + "</td>";
                        filaTbTblVendedores += "</tr>";
                    }
                } else {
                    if (item.nombreVendedorFiltro != null) {
                        filaTbTblVendedores += "<tr>";
                        filaTbTblVendedores += "<td>" + item.nombreVendedorFiltro + "</td>";
                        filaTbTblVendedores += "<td style='text-align:right'>" + formatterPeso.format(item.VentaTotalVendedorFiltro) + "</td>";
                        filaTbTblVendedores += "</tr>";
                    }
                }

            })
            tbTblVendedores.append(filaTbTblVendedores);
        }
    });
}

