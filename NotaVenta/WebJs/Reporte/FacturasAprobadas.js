﻿var parametros = {};

$(document).ready(function () {
    parametros = {
        ControlaStockProducto: ($("#ControlaStockProducto").val().toLowerCase() === "true"),
        DescuentoLineaDirectoSoftland: ($("#DescuentoLineaDirectoSoftland").val().toLowerCase() === "true"),
        DescuentoTotalDirectoSoftland: ($("#DescuentoTotalDirectoSoftland").val().toLowerCase() === "true"),
        CantidadDescuentosProducto: ($("#CantidadDescuentosProducto").val().toLowerCase()),
        CantidadDescuentosTotalDocumento: ($("#CantidadDescuentosTotalDocumento").val().toLowerCase()),
        MuestraUnidadMedidaProducto: ($("#MuestraUnidadMedidaProducto").val().toLowerCase() === "true"),
        ManejaTallaColor: ($("#ManejaTallaColor").val().toLowerCase() === "true"),
    };

    convertirFullScreenModal("modalSaldo");
});

function aprobarNW(nvnumero) {
    console.log(nvnumero);

    $("#nvnumero").val(nvnumero);
    var url = $("#urlFacturasPendientes").val();

    var data = {
        NVNumero: nvnumero
    };

    console.log(data);
    debugger
    $.post(url, data).done(function (data) {
        confirm("Numero Nota de Venta:\n" + data[0].NVNumero);
        debugger
    });
}

function DetalleNotaPedido(nvId) {
    var tableCabecera = $("#tblDetalleNotaPedido");
    var tableDetalle = $("#tblDetalleNotaPedidoDetalle");
    var htmlDetalle = "";
    var subtotal = 0;
    var displayPrecioConDescuento = true;
    var ivatotal = 0;
    var totalconiva = 0;
    var total = 0;
    $.ajax({
        type: "POST",
        url: "FacturasAprobadas",
        data: { _nvId: nvId },
        async: true,
        success: function (data) {
            if (data.Mensaje == 1) {
                alert("No se encontró el documento." + data.nvNum);
            }
            else {
                $("#tblDetalleNotaPedido").html("");
                $("#tblDetalleNotaPedidoDetalle").html("");

                $.each(data.Cabecera, function (index, value) {
                    subtotal = value.nvSubTotal;
                    totalconiva = value.TotalBoleta;
                    ivatotal = totalconiva - subtotal;

                    var vendedor = "Sin vendedor";
                    var listaPrecio = "Sin Lista de Precio";
                    var centroCosto = "Sin centro de costo";


                    if (value.VenDes !== null) {
                        vendedor = value.VenDes;
                    }
                    if (value.CodLista !== null) {
                        listaPrecio = value.CodLista + "-" + value.DesLista
                    }
                    if (value.CodiCC !== null) {
                        centroCosto = value.CodiCC + "-" + value.DescCC;
                    }

                    htmlCabecera = "<tr><th>N&ordm; Softland.</th>" +
                        "<td>" + value.NVNumero + "</td>" +
                        "<th nowrap='nowrap'>N&ordm; Int.</th>" +
                        "<td>" + nvId + "</td>" +
                        "<th nowrap='nowrap'>Vendedor</th>" +
                        "<td>" + vendedor + "</td></tr>" +
                        "<tr><th nowrap='nowrap'>Cond. Venta</th>" +
                        "<td>" + value.CveCod + "-" + value.CveDes + "</td>" +
                        "<th>Fecha Pedido</th>" +
                        "<td>" + value.nvFemString + "</td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td></td></tr>" +

                        "<tr><th nowrap='nowrap'>Fecha Entrega</th>" +
                        "<td>" + value.nvFeEntString + "</td>" +
                        "<th nowrap='nowrap'>Nro Cotizacion</th>" +
                        "<td>" + value.NroFinalCotizacion + "</td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td></td></tr>" +

                        "<tr><th nowrap='nowrap'>Lista de Precio</th>" +
                        "<td>" + listaPrecio + "</td>" +
                        "<th nowrap='nowrap'>Cod. Cliente</th>" +
                        "<td>" + value.CodAux + "-" + value.NomAux + "</td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td></td></tr>" +

                        "<tr><th nowrap='nowrap'>Contacto</th>" +
                        "<td>" + value.NomCon + "</td>" +
                        "<th nowrap='nowrap'>Centro de Costo</th>" +
                        "<td>" + centroCosto + "</td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td></td></tr>" +

                        "<tr><th nowrap='nowrap'>Observacion</th>" +
                        "<td>" + value.nvObser + "</td>" +  
                        "<td></td>" +
                        "<td></td>" +
                        "<td></td></tr>";
                    tableCabecera.append(htmlCabecera);
                });

                htmlDetalle = "";
                htmlDetalle = htmlDetalle + "<th>ID</th>";
                htmlDetalle = htmlDetalle + "<th>Codigo Producto</th>";
                htmlDetalle = htmlDetalle + "<th>Detalle Producto</th>";
                if (parametros.ManejaTallaColor) {
                    htmlDetalle = htmlDetalle + "<th>Talla</th>";
                    htmlDetalle = htmlDetalle + "<th>Color</th>";
                }
                htmlDetalle = htmlDetalle + "<th>Cantidad</th>";
                htmlDetalle = htmlDetalle + "<th>Stock</th>";

                if (parametros.MuestraUnidadMedidaProducto) {
                    htmlDetalle = htmlDetalle + "<th>U.Medida</th>";
                }

                htmlDetalle = htmlDetalle + "<th>Precio</th>";
                htmlDetalle = htmlDetalle + "<th>Sub Total</th>";
                if (parametros.DescuentoLineaDirectoSoftland) {
                    for (z = 0; z < parametros.CantidadDescuentosProducto; z++) {
                        htmlDetalle = htmlDetalle + "<th>Desc. N°" + (z + 1) + " (%)</th>";
                    }
                }
                if (displayPrecioConDescuento) {
                    htmlDetalle = htmlDetalle + "<th>Precio C/Descuento</th>";
                }
                htmlDetalle = htmlDetalle + "<th>Total</th>";

                tableDetalle.append(htmlDetalle);

                $.each(data.Detalle, function (index, value) {
                    htmlDetalle = "";
                    if (parametros.ControlaStockProducto) {
                        if (value.Stock < value.nvCant) {
                            htmlDetalle = htmlDetalle + "<tr style='background-color: red; color: white'>";
                        }
                        else {
                            htmlDetalle = htmlDetalle + "<tr>";
                        }
                    }
                    htmlDetalle = htmlDetalle + "<tr>";

                    htmlDetalle = htmlDetalle + "<td>" + value.nvLinea + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.CodProd + "</td>";
                    htmlDetalle = htmlDetalle + "<td>" + value.DesProd + "</td>";
                    if (parametros.ManejaTallaColor) {
                        htmlDetalle = htmlDetalle + "<td>" + value.Partida + "</td>";
                        htmlDetalle = htmlDetalle + "<td>" + value.Pieza + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvCant) + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.Stock) + "</td>";

                    if (parametros.MuestraUnidadMedidaProducto) {
                        htmlDetalle = htmlDetalle + "<td>" + value.CodUMed + "</td>";
                    }

                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvPrecio, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvSubTotal, "$") + "</td>";
                    if (parametros.DescuentoLineaDirectoSoftland) {
                        for (z = 0; z < parametros.CantidadDescuentosProducto; z++) {
                            if (z === 0) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc01 + "</td>"; }
                            if (z === 1) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc02 + "</td>"; }
                            if (z === 2) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc03 + "</td>"; }
                            if (z === 3) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc04 + "</td>"; }
                            if (z === 4) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc05 + "</td>"; }
                        }
                    }
                    var totalPrecioConDescuento = 0;
                    if (displayPrecioConDescuento) {
                        totalPrecioConDescuento = (value.nvPrecio);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc01)) ? 0 : value.nvDPorcDesc01)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc02)) ? 0 : value.nvDPorcDesc02)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc03)) ? 0 : value.nvDPorcDesc03)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc04)) ? 0 : value.nvDPorcDesc04)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc05)) ? 0 : value.nvDPorcDesc05)) / 100);

                        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(parseInt(totalPrecioConDescuento), "$") + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvTotLinea, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "</tr>";

                    var subtotalaux = value.nvSubTotal;

                    var ivaaux = (value.nvPrecio * value.nvCant) * 0.19;

                    var totalaux = value.nvTotLinea;
                    total = total + totalaux;

                    tableDetalle.append(htmlDetalle);
                });

                var colspanTotales = 7;
                if (parametros.ManejaTallaColor) {
                    colspanTotales = colspanTotales + 1;
                    colspanTotales = colspanTotales + 1;
                }
                if (displayPrecioConDescuento) {
                    colspanTotales = colspanTotales + 1;
                }
                if (parametros.DescuentoLineaDirectoSoftland) {
                    colspanTotales = colspanTotales + parseInt(parametros.CantidadDescuentosProducto);
                }


                var htmldetalleTotal = "";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>SubTotal</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(subtotal, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Total Iva</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(ivatotal, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Venta Total</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(totalconiva, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                tableDetalle.append(htmldetalleTotal);
            }
        }
    });
}
