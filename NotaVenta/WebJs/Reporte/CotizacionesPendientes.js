﻿var CotPendiente = [];
var IdCotizacion = 0;
var datosDetalle = 0;

$(document).ready(function () {
    $("#dataPrueba").dataTable().fnDestroy();
    $('#dataPrueba').DataTable({
        ordering: false,
        searching: false,
            language: {
                "sProcessing": "Procesando...",
                "sLengthMenu": "Mostrar _MENU_ Registros",
                "sZeroRecords": "&nbsp;",
                "sEmptyTable": "&nbsp;",
                "sInfo": "Encontrados: _TOTAL_ Registros (Mostrando del _START_ al _END_)",
                "sInfoEmpty": "* No se han encontrado resultados en la búsqueda",
                "sInfoFiltered": "",
                "sInfoPostFix": "",
                "sSearch": "Buscar:",
                "sUrl": "",
                "sInfoThousands": ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst": "Primero",
                    "sLast": "Último",
                    "sNext": "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                }
            },
            //aoColumnDefs: [{ 'bSortable': false, 'aTargets': ['no-sortable'] }]
        });

    if ($("#filtroVenCod").val() != -1) {
        $("#listaVendedores").val($("#filtroVenCod").val());
    } else {
        $("#listaVendedores").val(-1);
    }
    parametros = {
        ControlaStockProducto: ($("#ControlaStockProducto").val().toLowerCase() === "true"),
        DescuentoLineaDirectoSoftland: ($("#DescuentoLineaDirectoSoftland").val().toLowerCase() === "true"),
        DescuentoTotalDirectoSoftland: ($("#DescuentoTotalDirectoSoftland").val().toLowerCase() === "true"),
        CantidadDescuentosProducto: ($("#CantidadDescuentosProducto").val().toLowerCase()),
        CantidadDescuentosTotalDocumento: ($("#CantidadDescuentosTotalDocumento").val().toLowerCase()),
        MuestraUnidadMedidaProducto: ($("#MuestraUnidadMedidaProducto").val().toLowerCase() === "true"),
        ManejaTallaColor: ($("#ManejaTallaColor").val().toLowerCase() === "true"),
    };

    convertirFullScreenModal("modalSaldo");

    $("#modalModificarProductoNuevoModificar").click(() => {
        activarLoadingBoton("modalModificarProductoNuevoModificar");
        var elementos = {
            idNotaVenta: $("#modalModificarProductoNuevoIdNotaVenta").val(),
            idDetalleNotaVenta: $("#modalModificarProductoNuevoIdDetalleNotaVenta").val(),
            linea: $("#modalModificarProductoNuevoLinea").val(),
            codigoAntiguo: $("#modalModificarProductoNuevoCodigoAntiguo").val(),
            codigo: $("#modalModificarProductoNuevoCodigo").val(),
            descripcion: $("#modalModificarProductoNuevoDescripcion").val(),
            grupo: $("#modalModificarProductoNuevoGrupo").val(),
            subGrupo: $("#modalModificarProductoNuevoSubGrupo").val(),
            valorNeto: $("#modalModificarProductoNuevoValorNeto").val().replace(",", "."),
            tipoMoneda: $("#modalModificarProductoNuevoTipoMoneda").val(),
        };
        console.log("dasldoaskoo")
        if (elementos.codigo.length > 0 && elementos.codigo.length <= 20) {
            var _url = $("#urlObtieneProductosPorListaPrecio").val();

            if (datosDetalle.filter(m => m.CodProd === elementos.codigo && (m.nvLinea + "") !== (elementos.linea + "")).length === 0) {
                $.ajax({
                    url: _url,
                    data: { ListaPrecio: "-1" },
                    type: "POST",
                    dataType: "json",
                    async: true,
                    success: (response) => {
                        if (response.filter(m => m.CodProd === elementos.codigo).length === 0) {
                            if (elementos.descripcion.length > 0 && elementos.codigo.length <= 50) {
                                if (elementos.valorNeto != "") {
                                    if (!isNaN(elementos.valorNeto)) {
                                        var moneda = JSON.parse($("#hfMonedas").val()).find(m => m.CodigoMoneda === elementos.tipoMoneda);

                                        if (((elementos.valorNeto.length - elementos.valorNeto.indexOf(".")) - 1) <= moneda.DecimalesPrecioMoneda || elementos.valorNeto.indexOf(".") === -1) {
                                            abrirConfirmacion("Producto Nuevo", "¿Esta seguro de modificar producto nuevo?", () => {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "/Reporte/ModificarProductoNuevo",
                                                    data: {
                                                        IdDetalle: elementos.idDetalleNotaVenta,
                                                        CodProdAntiguo: elementos.codigoAntiguo,
                                                        CodProd: elementos.codigo,
                                                        DesProd: elementos.descripcion,
                                                        CodGrupo: elementos.grupo,
                                                        CodSubGr: elementos.subGrupo,
                                                        PrecioVenta: elementos.valorNeto,
                                                        CodMon: elementos.tipoMoneda
                                                    },
                                                    async: true,
                                                    success: (response) => {
                                                        var onclickString = "";
                                                        onclickString = onclickString + "abrirModalModificarProductoNuevo(";
                                                        onclickString = onclickString + "" + elementos.idNotaVenta + ", ";
                                                        onclickString = onclickString + "" + elementos.idDetalleNotaVenta + ", ";
                                                        onclickString = onclickString + "" + elementos.linea + ", ";
                                                        onclickString = onclickString + "'" + elementos.codigo + "', ";
                                                        onclickString = onclickString + "'" + elementos.descripcion + "', ";
                                                        onclickString = onclickString + "'" + elementos.grupo + "', ";
                                                        onclickString = onclickString + "'" + elementos.subGrupo + "', ";
                                                        onclickString = onclickString + "'" + elementos.valorNeto + "', ";
                                                        onclickString = onclickString + "'" + elementos.tipoMoneda + "', ";
                                                        onclickString = onclickString + ")";

                                                        $("#td_detalleProducto_codigoProducto_" + elementos.idNotaVenta + "_linea_" + elementos.linea).html(elementos.codigo);
                                                        $("#td_detalleProducto_descripcionProducto_" + elementos.idNotaVenta + "_linea_" + elementos.linea).html(elementos.descripcion);

                                                        $.ajax({
                                                            type: "POST",
                                                            url: "FacturasAprobadas",
                                                            data: { _nvId: elementos.idNotaVenta },
                                                            async: true,
                                                            success: (responseDetalle) => {
                                                                datosDetalle = responseDetalle.Detalle;
                                                            }
                                                        });
                                                        document.getElementById("btnModificarProductoNuevo_" + elementos.linea + "").setAttribute("onclick", onclickString);

                                                        abrirInformacion("Producto Nuevo", response.Mensaje);
                                                    }
                                                });
                                            });

                                            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                            $("#modalModificarProductoNuevoCerrar").click();
                                        }
                                        else {
                                            var mensaje = "";
                                            if (moneda.DecimalesPrecioMoneda > 0) {
                                                mensaje = "La cantidad de decimales permitidos es de " + moneda.DecimalesPrecioMoneda + " para el valor neto";
                                            }
                                            else {
                                                mensaje = "Valor neto no permite decimales para la moneda " + moneda.DescripcionMoneda;
                                            }
                                            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                            abrirError("Valor Neto", mensaje);
                                        }
                                    }
                                    else {
                                        desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                        abrirError("Valor Neto", "Valor neto debe ser un numero");
                                    }
                                }
                                else {
                                    desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                    abrirError("Valor Neto", "Valor neto es un campo obligatorio");
                                }
                            }
                            else {
                                desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                                abrirError("Descripcion de Producto", "Descripcion de producto debe contener entre 1 y 50 caracteres");
                            }
                        }
                        else {
                            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                            abrirError("Producto", "Codigo de producto ya existe en el sistema");
                        }
                    }
                });
            }
            else {
                desactivarLoadingBoton("modalModificarProductoNuevoModificar");
                abrirError("Codigo de Producto", "El codigo de producto ya fue ingresado dentro de la Nota de Venta");
            }
        }
        else {
            desactivarLoadingBoton("modalModificarProductoNuevoModificar");
            abrirError("Codigo de Producto", "Codigo de producto debe contener entre 1 y 20 caracteres");
        }
    });
});

function DetalleCotizacion(Id, RutAux, EstadoNP, TipoUsuario) {
    var tableCabecera = $("#tblDetalleNotaPedido");
    var tableDetalle = $("#tblDetalleNotaPedidoDetalle");
    var htmlDetalle = "";
    var subtotal = 0;
    var displayPrecioConDescuento = true;
    var ivatotal = 0;
    var totalconiva = 0;
    var total = 0;

    tableCabecera.html("");
    tableDetalle.html("");
    $.ajax({
        type: "POST",
        url: "CotizacionesPendientesDetalle",
        data: { _nvId: Id },
        async: true,
        success: function (data) {
            if (data.Mensaje == 1) {
                alert("No se encontró el documento." + data.nvNum);
            }
            else {
                $("#tblDetalleNotaPedido").html("");
                $("#tblDetalleNotaPedidoDetalle").html("");

                $.each(data.Cabecera, function (index, value) {
                    subtotal = value.nvNetoAfecto;
                    //totalconiva = value.TotalBoleta;
                    ivatotal = value.nvNetoAfecto * 19 / 100;
                    totalconiva = subtotal + ivatotal;

                    var vendedor = "Sin vendedor";
                    var listaPrecio = "Sin Lista de Precio";
                    var centroCosto = "Sin centro de costo";


                    if (value.VenDes !== null) {
                        vendedor = value.VenDes;
                    }
                    if (value.CodLista !== null) {
                        listaPrecio = value.CodLista + "-" + value.DesLista
                    }
                    if (value.CodiCC !== null) {
                        centroCosto = value.CodiCC + "-" + value.DescCC;
                    }
                    var htmlCabecera = "<tr><th nowrap='nowrap'>N&ordm; Int.</th>" +
                        "<td>" + Id + "</td>" +
                        "<th nowrap='nowrap'>Vendedor</th>" +
                        "<td>" + vendedor + "</td></tr>" +
                        "<tr><th nowrap='nowrap'>Cond. Venta</th>" +
                        "<td>" + value.CveCod + "-" + value.CveDes + "</td>" +
                        "<th nowrap='nowrap'>Fecha Pedido</th>" +
                        "<td>" + value.nvFemString + "</td></tr>" +
                        "<tr><th nowrap='nowrap'>Fecha Entrega</th>" +
                        "<td>" + value.nvFeEntString + "</td>" +

                        "<th nowrap='nowrap'>Fecha Cierre</th>" +
                        "<td>" + value.FechaCierreString + "</td></tr>" +

                        "<tr><th nowrap='nowrap'>Cod. Cliente</th>" +
                        "<td>" + value.CodAux + "-" + value.NomAux + "</td>" +
                        "<th nowrap='nowrap'>Contacto</th>" +
                        "<td>" + value.NomCon + "</td></tr>" +

                        "<tr><th nowrap='nowrap'>Centro de Costo</th>" +
                        "<td>" + centroCosto + "</td>" +

                        "<th nowrap='nowrap'>Lista de Precio</th>" +
                        "<td>" + listaPrecio + "</td>" +
                                                     
                        "<tr><th nowrap='nowrap'>Observacion</th>" +                        
                        "<td>" + value.nvObser + "</td>" +
                        "<td></td>" +
                        "<td></td></tr>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td></td>" +
                        "<td></td>";
                    tableCabecera.append(htmlCabecera);
                });

                htmlDetalle = "";
                htmlDetalle = htmlDetalle + "<th>ID</th>";
                htmlDetalle = htmlDetalle + "<th>Codigo Producto</th>";
                htmlDetalle = htmlDetalle + "<th>Detalle Producto</th>";
                if (parametros.ManejaTallaColor) {
                    htmlDetalle = htmlDetalle + "<th>Talla</th>";
                    htmlDetalle = htmlDetalle + "<th>Color</th>";
                }
                htmlDetalle = htmlDetalle + "<th>Cantidad</th>";
                htmlDetalle = htmlDetalle + "<th>Stock</th>";

                if (parametros.MuestraUnidadMedidaProducto) {
                    htmlDetalle = htmlDetalle + "<th>U.Medida</th>";
                }

                htmlDetalle = htmlDetalle + "<th>Precio</th>";
                htmlDetalle = htmlDetalle + "<th>Sub Total</th>";
                if (parametros.DescuentoLineaDirectoSoftland) {
                    for (z = 0; z < parametros.CantidadDescuentosProducto; z++) {
                        htmlDetalle = htmlDetalle + "<th>Desc. N°" + (z + 1) + " (%)</th>";
                    }
                }
                if (displayPrecioConDescuento) {
                    htmlDetalle = htmlDetalle + "<th>Precio C/Descuento</th>";
                }
                htmlDetalle = htmlDetalle + "<th>Total</th>";
                if (EstadoNP === "P" && TipoUsuario != 2) {
                    htmlDetalle = htmlDetalle + "<th></th>";
                }

                tableDetalle.append(htmlDetalle);
                datosDetalle = data.Detalle;
                $.each(data.Detalle, function (index, value) {
                    htmlDetalle = "";
                    if (parametros.ControlaStockProducto) {
                        if (value.Stock < value.nvCant) {
                            htmlDetalle = htmlDetalle + "<tr style='background-color: red; color: white'>";
                        }
                        else {
                            htmlDetalle = htmlDetalle + "<tr>";
                        }
                    }
                    htmlDetalle = htmlDetalle + "<tr>";
                    htmlDetalle = htmlDetalle + "<td>" + value.nvLinea + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_codigoProducto_" + Id + "_linea_" + value.nvLinea + "'>" + value.CodProd + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_descripcionProducto_" + Id + "_linea_" + value.nvLinea + "'>" + value.DesProd + "</td>";
                    if (parametros.ManejaTallaColor) {
                        htmlDetalle = htmlDetalle + "<td>" + value.Partida + "</td>";
                        htmlDetalle = htmlDetalle + "<td>" + value.Pieza + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvCant) + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.Stock) + "</td>";

                    if (parametros.MuestraUnidadMedidaProducto) {
                        htmlDetalle = htmlDetalle + "<td>" + value.CodUMed + "</td>";
                    }

                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvPrecio, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvSubTotal, "$") + "</td>";
                    if (parametros.DescuentoLineaDirectoSoftland) {
                        for (z = 0; z < parametros.CantidadDescuentosProducto; z++) {
                            if (z === 0) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc01 + "</td>"; }
                            if (z === 1) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc02 + "</td>"; }
                            if (z === 2) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc03 + "</td>"; }
                            if (z === 3) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc04 + "</td>"; }
                            if (z === 4) { htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + value.nvDPorcDesc05 + "</td>"; }
                        }
                    }
                    var totalPrecioConDescuento = 0;
                    if (displayPrecioConDescuento) {
                        totalPrecioConDescuento = (value.nvPrecio);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc01)) ? 0 : value.nvDPorcDesc01)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc02)) ? 0 : value.nvDPorcDesc02)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc03)) ? 0 : value.nvDPorcDesc03)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc04)) ? 0 : value.nvDPorcDesc04)) / 100);
                        totalPrecioConDescuento = totalPrecioConDescuento - (((totalPrecioConDescuento) * ((isNaN(value.nvDPorcDesc05)) ? 0 : value.nvDPorcDesc05)) / 100);

                        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(parseInt(totalPrecioConDescuento), "$") + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvTotLinea, "$") + "</td>";
                    //if (EstadoNP === "P" && TipoUsuario != 2) {
                    //    //if (value.EsProductoNuevo) {
                    //    //    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>";
                    //    //    htmlDetalle = htmlDetalle + "<button id='btnModificarProductoNuevo_" + value.nvLinea + "' class='btn btn-primary'";
                    //    //    htmlDetalle = htmlDetalle + "onclick=\"abrirModalModificarProductoNuevo(";
                    //    //    htmlDetalle = htmlDetalle + "" + Id + ", ";
                    //    //    htmlDetalle = htmlDetalle + "" + value.Id + ", ";
                    //    //    htmlDetalle = htmlDetalle + "" + value.nvLinea + ", ";
                    //    //    htmlDetalle = htmlDetalle + "'" + value.CodProd + "', ";
                    //    //    htmlDetalle = htmlDetalle + "'" + value.DesProd + "', ";
                    //    //    htmlDetalle = htmlDetalle + "'" + value.CodGrupo + "', ";
                    //    //    htmlDetalle = htmlDetalle + "'" + value.CodSubGr + "', ";
                    //    //    htmlDetalle = htmlDetalle + "'" + value.ValorNetoProducto + "', ";
                    //    //    htmlDetalle = htmlDetalle + "'" + value.CodMon + "', ";
                    //    //    htmlDetalle = htmlDetalle + ")\"";
                    //    //    htmlDetalle = htmlDetalle + ">";
                    //    //    htmlDetalle = htmlDetalle + "<i class='fa fa-edit'></i>";
                    //    //    htmlDetalle = htmlDetalle + "</button>";
                    //    //    htmlDetalle = htmlDetalle + "</td>";
                    //    }
                    //    else {
                    //        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>";
                    //        htmlDetalle = htmlDetalle + "</td>";
                    //    }
                    //}
                    htmlDetalle = htmlDetalle + "</tr>";


                    var subtotalaux = value.nvSubTotal;

                    var ivaaux = (value.nvPrecio * value.nvCant) * 0.19;

                    var totalaux = value.nvTotLinea;
                    total = total + totalaux;

                    tableDetalle.append(htmlDetalle);
                });

                var colspanTotales = 7;
                if (parametros.ManejaTallaColor) {
                    colspanTotales = colspanTotales + 1;
                    colspanTotales = colspanTotales + 1;
                }
                if (displayPrecioConDescuento) {
                    colspanTotales = colspanTotales + 1;
                }
                if (parametros.DescuentoLineaDirectoSoftland) {
                    colspanTotales = colspanTotales + parseInt(parametros.CantidadDescuentosProducto);
                }

                if (ivatotal < 0)
                    { ivatotal = ivatotal * -1 }

                var htmldetalleTotal = "";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>SubTotal</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + agregarSeparadorMiles(subtotal) + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Total Iva</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + agregarSeparadorMiles(ivatotal.toFixed()) + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Venta Total</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + agregarSeparadorMiles(totalconiva.toFixed()) + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                tableDetalle.append(htmldetalleTotal);
            }
        }
    });
}

function EditarCotizacion(Id) {/*, RutAux, EstadoNP, TipoUsuario) {*/
    var tableCabecera = $("#tblCabecera");
    var subtotal = 0;
    var ivatotal = 0;
    var totalconiva = 0;
    var total = 0;

    tableCabecera.html("");
    $.ajax({
        type: "POST",
        url: "CotizacionesPendientesDetalle",
        data: { _nvId: Id },
        async: true,
        success: function (data) {
            if (data.Mensaje == 1) {
                alert("No se encontró el documento." + data.nvNum);
            }
            else {
                $.each(data.Cabecera, function (index, value) {
                    subtotal = value.nvSubTotal;
                    totalconiva = value.TotalBoleta;
                    ivatotal = totalconiva - subtotal;

                    var vendedor = "Sin vendedor";
                    var listaPrecio = "Sin Lista de Precio";
                    var centroCosto = "Sin centro de costo";


                    if (value.VenDes !== null) {
                        vendedor = value.VenDes;
                    }
                    if (value.CodLista !== null) {
                        listaPrecio = value.CodLista + "-" + value.DesLista
                    }
                    if (value.CodiCC !== null) {
                        centroCosto = value.CodiCC + "-" + value.DescCC;
                    }

                    var htmlCabecera = "<tr><th nowrap='nowrap'>N&ordm; Int.</th>" +
                        "<td><input type='text' id='idCotizacion' value="+Id+" disabled /></td>" +
                        "<th nowrap='nowrap'>Vendedor</th>" +
                        "<td><input type='text' value=" + vendedor + " disabled/></td></tr>" +

                        //"<tr><th nowrap='nowrap'>Cond. Venta</th>" +
                        //"<td><input type='text' value=" + value.CveCod + "-" + value.CveDes + " disabled/></td>" +

                        "<tr><th nowrap='nowrap'>Fecha Pedido</th>" +
                        "<td><input type='date' id='fechaEmision' value='" + value.nvFemYYYYMMDD2 + "'></td>" +
                        "<th nowrap='nowrap'>Fecha Entrega</th>" +
                        "<td><input type='date' id='fechaEntrega' value='" + value.nvFeEntYYYYMMDD2 + "'></td></tr>" +

                        "<tr><th nowrap='nowrap'>Fecha Cierre</th>" +
                        "<td><input type='date' id='fechaCierre' value='" + value.FechaCierreYYYYMMDD + "'></td>" +
                        "<th nowrap='nowrap'>Cod. Cliente</th>" +
                        "<td><input type='text' value=" + value.CodAux + "-" + value.NomAux + " disabled/></td></tr>" +

                        "<tr><th nowrap='nowrap'>Contacto</th>" +
                        "<td><input disabled type='text' id='Contacto' value='" + value.NomCon + "'/></td></tr>" +

                        //"<tr><th nowrap='nowrap'>Centro de Costo</th>" +
                        //"<td>" + centroCosto + "</td>" +
                        //"<th nowrap='nowrap'>Lista de Precio</th>" +
                        //"<td>" + listaPrecio + "</td>" +

                        "<tr><th nowrap='nowrap'>Observacion</th>" +
                        "<td><input type='text' id='observacion' value='" +value.nvObser+ "'></td>" +
                        "<td></td>" +
                        "<td></td></tr>" +
                        "<td></td>" +
                        "<td><button class='btn btn-warning btn-xs' data-toggle='modal' data-target='#DetalleCotizacion' onclick='EditarDetalleCotizacion("+value.Id+")'>Editar Detalle</button></td>" +
                        "<td><button class='btn btn-danger btn-xs' onclick='grabaEditaCotizacion("+value.Id+")'>Guardar Cabecera</button></td>" +
                        "<td></td>";
                    tableCabecera.append(htmlCabecera);
                });
            }
        }
    });
}

function grabaEditaCotizacion() {
    IdCotizacion = $("#idCotizacion").val();
    var numCot = $("#idCotizacion").val();
    var fechaEmision = $("#fechaEmision").val();
    var fechaEntrega = $("#fechaEntrega").val();
    var fechaCierre = $("#fechaCierre").val();
    var contacto = $("#Contacto").val();
    var observacion = $("#observacion").val();
    $.ajax({
        type: "POST",
        url: "ActualizaCabeceraCotizacion",
        data:
        {
            numCot,
            fechaEmision,
            fechaEntrega,
            fechaCierre,
            contacto,
            observacion
        },
        async: true,
        success: function (data) {
            if (data.Verificador) {
                abrirInformacion("Modificacion", data.Mensaje);
            }
        }
    });
 
}

function EditarDetalleCotizacion(Id) {
    IdCotizacion = $("#idCotizacion").val();
    var tableDetalle = $("#tblDetalle tbody");
    var htmlDetalle = "";
    tableDetalle.html("");
    var total = 0;
    var subtotalaux = 0;
    var ivaaux = 0;
    var totalaux = 0;
    var totalIva = 0;
    var neto=0
    $.ajax({
        type: "POST",
        url: "CotizacionesPendientesDetalle",
        data: { _nvId: Id },
        async: true,
        success: function (data) {
            if (data.Mensaje == 1) {
                alert("No se encontro detalle." + data.Id);
            }
            else {
                $("#tblDetalle tbody").html("");
                $.each(data.Detalle, function (index, value) {
                    htmlDetalle = "";
                    
                    if (parametros.ControlaStockProducto) {
                        if (value.Stock < value.nvCant) {
                            htmlDetalle = htmlDetalle + "<tr style='background-color: red; color: white'>";
                        }
                        else {
                            htmlDetalle = htmlDetalle + "<tr>";
                        }
                    }
                    htmlDetalle = htmlDetalle + "<tr>";
                    htmlDetalle = htmlDetalle + "<td>" + value.nvLinea + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_codigoProducto_" + Id + "_linea_" + value.nvLinea + "'>" + value.CodProd + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_descripcionProducto_" + Id + "_linea_" + value.nvLinea + "'>" + value.DesProd + "</td>";
                    if (parametros.ManejaTallaColor) {
                        htmlDetalle = htmlDetalle + "<td>" + value.Partida + "</td>";
                        htmlDetalle = htmlDetalle + "<td>" + value.Pieza + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvCant) + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.Stock) + "</td>";

                    if (parametros.MuestraUnidadMedidaProducto) {
                        htmlDetalle = htmlDetalle + "<td>" + value.CodUMed + "</td>";
                    }

                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvPrecio, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(Math.round(value.nvTotLinea), "$") + "</td>";
                    //htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvSubTotal, "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td></td>";
                    htmlDetalle = htmlDetalle + "<td><button class='btn btn-warning btn-xs' data-toggle='modal' data-target='#EditarDetalleCotizacion' onclick='modificarDetalle(" + value.nvLinea + ", \"" + value.CodProd  +"\"," + Id + ")'>Editar</button></td>";
                    htmlDetalle = htmlDetalle + "<td><button class='btn btn-danger btn-xs' onclick='eliminarDetalle(" + value.nvLinea +","+ Id+")'>Eliminar</button></td>";
                    htmlDetalle = htmlDetalle + "</tr>";
                    
                    subtotalaux += value.nvTotLinea;

                    ivaaux += (value.nvTotLinea) * 0.19;

                    //totalaux = subtotalaux / 1.19;
                    //totalIva = Math.round(subtotalaux - totalaux);
                    totalIva = Math.round(subtotalaux * 19 / 100);
                    total = subtotalaux + totalIva;
                    if (ivaaux < 0)
                    {
                        ivaaux = ivaaux * -1;
                    }
                    neto = subtotalaux - totalIva;

                    tableDetalle.append(htmlDetalle);
                });

                var colspanTotales = 7;
                if (parametros.ManejaTallaColor) {
                    colspanTotales = colspanTotales + 1;
                    colspanTotales = colspanTotales + 1;
                }
               
                if (parametros.DescuentoLineaDirectoSoftland) {
                    colspanTotales = colspanTotales + parseInt(parametros.CantidadDescuentosProducto);
                }

                var htmldetalleTotal = "";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>SubTotal</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(Math.round(subtotalaux),"$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Total Iva</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(totalIva, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Venta Total</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(Math.round(total), "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                tableDetalle.append(htmldetalleTotal);

                datosDetalle = data.Detalle;
                
            }
        }
    });
}

function modificarDetalle(nrolinea, codprod,Id) {
    $.ajax({
        type: "POST",
        url: "BuscaPro_X_Modificar",
        data: {
            nrolinea,
            codprod,
            Id
        },
        async: true,
        success: function (data) {
            if (data.Mensaje == 1) {
                alert("No se encontro detalle." + data.Id);
            }
            else {
                $("#idDetalle").val(data.producto.Id);
                $("#txtCodProducto").val(data.producto.CodProd);
                $("#txtProducto").val(data.producto.DesProd);
                $("#txtPrecio").val(data.producto.PrecioVta);
                $("#txtCantidad").val(data.producto.Cantidad);
                $("#txtPorciento").val(data.producto.PorcentajeDescuento);
                $("#txt_fecha_entrega").val(data.producto.nvFecComprYYYYMMDD2);
                $("#txtCodFabrica").val(data.producto.CodigoFabrica);


                if (data.producto.ProdLocal == 1) {
                    data.producto.CodGrupo = data.producto.CodGrupo == null ? "Sin Grupo" : data.producto.CodGrupo;
                    data.producto.CodigoSubGrupo = data.producto.CodSubGr == null ? "Sin SubGrupo" : data.producto.CodSubGrup;
                    data.producto.CodigoMoneda = data.producto.CodMon == null ? "Sin Tipo" : data.producto.CodMon;

                    $("#cbxGrupo").find('option').remove().end();
                    $("#cbxGrupo").append('<option selected value="-1"> -- Seleccione Grupo -- </option>');
                    $.each(data.Grupos, function (index, item) {
                        $("#cbxGrupo").append('<option selected value="' + item.CodigoGrupo + '">' + item.DescripcionGrupo + '</option>');
                    });
                    $("#cbxGrupo").val(data.producto.CodGrupo);


                    $("#cbxSubGrupo").find('option').remove().end();
                    $("#cbxSubGrupo").append('<option selected value="-1"> -- Seleccione SubGrupo -- </option>');
                    $.each(data.SubGrupos, function (index, item) {
                        $("#cbxSubGrupo").append('<option selected value="' + item.CodigoSubGrupo + '">' + item.DescripcionSubGrupo + '</option>');
                    });
                    $("#cbxSubGrupo").val(data.producto.CodSubGr);

                    $("#cbxtipomoneda").find('option').remove().end();
                    $("#cbxtipomoneda").append('<option selected value="-1"> -- Seleccione Tipo Moneda -- </option>');
                    $.each(data.tipomoneda, function (index, item) {
                        $("#cbxtipomoneda").append('<option selected value="' + item.CodigoMoneda + '">' + item.DescripcionMoneda + '</option>');
                    });

                    $("#cbxtipomoneda").val(data.producto.CodMon);
                    var txtPrecio = document.getElementById('txtPrecio');
                    var codigo_Fabrica = document.getElementById('txtCodFabrica');
                    $("#txt_fecha_entrega").val(data.producto.nvFecComprYYYYMMDD2);
                    txtPrecio.disabled = false;
                    codigo_Fabrica.disabled = false;
                } else
                {
                    data.producto.CodGrupo = data.producto.CodGrupo == null ? "Sin Grupo" : data.producto.DescripcionGrupo;
                    data.producto.CodigoSubGrupo = data.producto.CodSubGr == null ? "Sin SubGrupo" : data.producto.DescripcionSubGrupo;
                    data.producto.CodigoMoneda = data.producto.CodigoMoneda == null ? "Sin Tipo" : data.producto.DescripcionMoneda;
                    $("#cbxGrupo").find('option').remove().end();
                    $("#cbxSubGrupo").find('option').remove().end();
                    $("#cbxtipomoneda").find('option').remove().end();
                    $("#cbxGrupo").append('<option selected disabled value="' + data.producto.CodGrupo + '">' + data.producto.DescripcionGrupo + '</option>');
                    $("#cbxSubGrupo").append('<option selected disabled value="' + data.producto.CodigoSubGrupo + '">' + data.producto.DescripcionSubGrupo + '</option>');
                    $("#cbxtipomoneda").append('<option selected disabled value="' + data.producto.CodigoMoneda + '">' + data.producto.DescripcionMoneda + '</option>');
                    $("#txt_fecha_entrega").val(data.producto.nvFecComprYYYYMMDD2);
                    var txtPrecio = document.getElementById('txtPrecio');
                    var codigo_Fabrica = document.getElementById('txtCodFabrica');
                    txtPrecio.disabled = true;
                    codigo_Fabrica.disabled = true;
                }

            }
        }
    });
}

function Detalle_X_Modificar() {
    IdCotizacion = $("#idCotizacion").val();
    var id = $("#idDetalle").val();
    var precio = $("#txtPrecio").val();
    var grupo = $("#cbxGrupo").val();
    var subGrupo = $("#cbxSubGrupo").val();
    var codmon = $("#cbxtipomoneda").val();
    var cantidad = $("#txtCantidad").val();
    var descuento = $("#txtPorciento").val();
    var total = 0;
    var subtotalaux = 0;
    var ivaaux = 0;
    var totalaux = 0;
    var valorDescuento = 0;
    var descuentoMaximo = 0;
    var neto = 0;
    var totalIva=0
    var porcentaje = $("#txtPorciento").val();
    var tipoUsuario = $("#tipoUsuario").val();
    var fechaEntrega = $("#txt_fecha_entrega").val();
    var codigoFabrica = $("#txtCodFabrica").val();
    var codProd = $("#txtCodProducto").val();
    if (/^([0-9])*$/.test(precio))
    {

    }
    else {
        abrirError("Usuario", "Cantidad debe ser numerico");
        return false;
    }

    if (/^([0-9])*$/.test(porcentaje))
    {
        if (porcentaje > 0)
        {
            valorDescuento = precio * porcentaje / 100;
            descuentoMaximo = precio * 30 / 100;
            
            if (tipoUsuario != 3)
            {
                if (descuentoMaximo < valorDescuento)
                {
                    abrirError("Usuario", "Descuento no puede ser mayor a 30%");
                    return false;
                } 
            }             
        }
    }
    else
    {
        abrirError("Usuario", "Descuento debe ser numerico");
    }

    if (valorDescuento.toString().includes(","))
    {
        valorDescuento = valorDescuento.toString().includes(",") ? valorDescuento.toString().replace(",", ".") : valorDescuento;
    }
    else
    {
        valorDescuento = valorDescuento.toString().includes(".") ? valorDescuento.toString().replace(".", ",") : valorDescuento;
    }
    

    $.ajax({
        type: "POST",
        url: "Detalle_X_Modificar",
        data:
        {
            id,
            precio,
            grupo,
            subGrupo,
            codmon,
            cantidad,
            descuento,
            IdCotizacion,
            valorDescuento,
            fechaEntrega,
            codigoFabrica,
            codProd
        },
        async: true,
        success: function (data) {
            if (data.Verificador) {
                abrirInformacion("Modificacion", data.respuesta.Mensaje);
                $("#tblDetalle tbody").html("");
                var tableDetalle = $("#tblDetalle tbody");
                var htmlDetalle = "";
                
                //------------------------
                $.each(data.Detalle, function (index, value) {
                    htmlDetalle = "";
                    if (parametros.ControlaStockProducto)
                    {
                        if (value.Stock < value.nvCant)
                        {
                            htmlDetalle = htmlDetalle + "<tr style='background-color: red; color: white'>";
                        }
                        else
                        {
                            htmlDetalle = htmlDetalle + "<tr>";
                        }
                    }
                    htmlDetalle = htmlDetalle + "<tr>";
                    htmlDetalle = htmlDetalle + "<td>" + value.nvLinea + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_codigoProducto_" + index + "_linea_" + value.nvLinea + "'>" + value.CodProd + "</td>";
                    htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_descripcionProducto_" + index + "_linea_" + value.nvLinea + "'>" + value.DesProd + "</td>";
                    if (parametros.ManejaTallaColor)
                    {
                        htmlDetalle = htmlDetalle + "<td>" + value.Partida + "</td>";
                        htmlDetalle = htmlDetalle + "<td>" + value.Pieza + "</td>";
                    }
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvCant) + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.Stock) + "</td>";

                    if (parametros.MuestraUnidadMedidaProducto)
                    {
                        htmlDetalle = htmlDetalle + "<td>" + value.CodUMed + "</td>";
                    }

                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(Math.round(value.nvPrecio), "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(Math.round(value.nvTotLinea), "$") + "</td>";
                    htmlDetalle = htmlDetalle + "<td></td>";
                    htmlDetalle = htmlDetalle + "<td><button class='btn btn-warning btn-xs' data-toggle='modal' data-target='#EditarDetalleCotizacion' onclick='modificarDetalle(" + value.nvLinea + ", \"" +value.CodProd +"\"," + IdCotizacion+")'>Editar</button></td>";
                    htmlDetalle = htmlDetalle + "<td><button class='btn btn-danger btn-xs' onclick='eliminarDetalle(" + value.nvLinea +","+ IdCotizacion+")'>Eliminar</button></td>";
                    htmlDetalle = htmlDetalle + "</tr>";
                    
                    subtotalaux += value.nvTotLinea;
                    
                    ivaaux += (value.nvTotLinea) * 0.19;  /*(value.nvPrecio * value.nvCant) * 0.19;*/

                    //totalaux = subtotalaux / 1.19;
                    //totalIva = Math.round(subtotalaux-totalaux);
                    totalIva = Math.round(subtotalaux * 19 / 100);
                    total += totalaux;
                    neto = subtotalaux - totalIva;
                    total = subtotalaux + totalIva;
                    tableDetalle.append(htmlDetalle);
                });

                var colspanTotales = 7;
                if (parametros.ManejaTallaColor)
                {
                    colspanTotales = colspanTotales + 1;
                    colspanTotales = colspanTotales + 1;
                }
               
                if (parametros.DescuentoLineaDirectoSoftland)
                {
                    colspanTotales = colspanTotales + parseInt(parametros.CantidadDescuentosProducto);
                }

                var htmldetalleTotal = "";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>SubTotal</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(subtotalaux, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Total Iva</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(totalIva, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Venta Total</th>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(total, "$") + "</td>";
                htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                htmldetalleTotal = htmldetalleTotal + "</tr>";
                tableDetalle.append(htmldetalleTotal);

                $("#dataPrueba tbody").html("");
                var TbodyCotizaciones = $("#dataPrueba tbody");

                $.each(data.listaCompletaCot, function (index, item) {
                    var filaTbody = "";
                    if (item.ErrorAprobador) {
                        filaTbody += "<tr style='background-color: #FF7F7F' title='" + item.ErrorAprobadorMensaje + "'>";
                    } else {
                        filaTbody += "<tr>";
                    }
                    filaTbody += "<td hidden>" + item.Id + "</td>";
                    filaTbody += "<td>" + item.NroFinalCotizacion + "</td>";
                    filaTbody += "<td>" + item.RutAux + "</td>";
                    filaTbody += "<td>" + item.NomAux + "</td>";
                    filaTbody += "<td>" + item.nvFemString + "</td>";
                    filaTbody += item.VenDes != null ? "<td>" + item.VenDes + "</td>" : "<td>" + "" + "</td>";
                    filaTbody += "<td style='text-align: right;'>" + agregarSeparadorMiles(Math.round(item.nvNetoAfecto)) + "</td>";
                    filaTbody += "<td style='text-align: right;'>" + agregarSeparadorMiles(Math.round(item.nvMonto)) + "</td>";
                    filaTbody += "<td>" + item.EstadoNP + "</td>";
                    filaTbody += "<td><button class='btn btn-warning btn-xs' data-toggle='modal' data-target='#CabeceraCotizacion' onclick='EditarCotizacion(" + item.Id + ")'>Editar</button></td>";
                    filaTbody += "<td><button class='btn btn-info btn-xs' data-toggle='modal' data-target='#DetalleNotaPedido' onclick='DetalleCotizacion(" + item.Id + ")'>Detalle</button></td>";

                    if (tipoUsuario != 2)
                        {
                            filaTbody += "<td><button class='btn btn-dark btn-xs' onclick='DescargarArchivos("+item.Id+")'>Descargar</button>";
                        }

                    filaTbody += "<a>&nbsp;</a>";
                    filaTbody += "<td><button class='fa fa-download btn btn-warning btn-xs' onclick='DescargarPdf(" + item.Id + ")'></button></td>";
                    filaTbody += "</tr>";
                    TbodyCotizaciones.append(filaTbody);
                });

            }
        }
    });
}

function eliminarDetalle(Linea, IdCotizacion) {
    IdCotizacion = $("#idCotizacion").val();
    var total = 0;
    var subtotalaux = 0;
    var ivaaux = 0;
    var totalaux = 0;
    if (datosDetalle.length > 1) {
        $.ajax({
            type: "POST",
            url: "EliminarDetalle",
            data:
            {
                Linea,
                IdCotizacion
            },
            async: true,
            success: function (data) {
                if (data.Verificador) {
                    abrirInformacion("Modificacion", data.respuesta.Mensaje);
                    $("#tblDetalle tbody").html("");
                    var tableDetalle = $("#tblDetalle tbody");
                    var htmlDetalle = "";
                    datosDetalle = datosDetalle.filter(m => m.nvLinea !== Linea);
                    $.each(data.Detalle, function (index, value) {
                        htmlDetalle = "";
                        if (parametros.ControlaStockProducto) {
                            if (value.Stock < value.nvCant) {
                                htmlDetalle = htmlDetalle + "<tr style='background-color: red; color: white'>";
                            }
                            else {
                                htmlDetalle = htmlDetalle + "<tr>";
                            }
                        }
                        htmlDetalle = htmlDetalle + "<tr>";
                        htmlDetalle = htmlDetalle + "<td>" + value.nvLinea + "</td>";
                        htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_codigoProducto_" + index + "_linea_" + value.nvLinea + "'>" + value.CodProd + "</td>";
                        htmlDetalle = htmlDetalle + "<td id='td_detalleProducto_descripcionProducto_" + index + "_linea_" + value.nvLinea + "'>" + value.DesProd + "</td>";
                        if (parametros.ManejaTallaColor) {
                            htmlDetalle = htmlDetalle + "<td>" + value.Partida + "</td>";
                            htmlDetalle = htmlDetalle + "<td>" + value.Pieza + "</td>";
                        }
                        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvCant) + "</td>";
                        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.Stock) + "</td>";

                        if (parametros.MuestraUnidadMedidaProducto) {
                            htmlDetalle = htmlDetalle + "<td>" + value.CodUMed + "</td>";
                        }

                        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvPrecio, "$") + "</td>";
                        htmlDetalle = htmlDetalle + "<td style='text-align: right;'>" + formatearNumero(value.nvSubTotal, "$") + "</td>";
                        htmlDetalle = htmlDetalle + "<td></td>";
                        htmlDetalle = htmlDetalle + "<td><button class='btn btn-warning btn-xs' data-toggle='modal' data-target='#EditarDetalleCotizacion' onclick='modificarDetalle(" + value.nvLinea + ", \"" + value.CodProd  +"\"," + IdCotizacion + ")'>Editar</button></td>";
                        htmlDetalle = htmlDetalle + "<td><button class='btn btn-danger btn-xs' onclick='eliminarDetalle(" + IdCotizacion + "," + value.nvLinea + ")'>Eliminar</button></td>";
                        htmlDetalle = htmlDetalle + "</tr>";

                        subtotalaux = value.nvSubTotal;

                        ivaaux = (value.nvPrecio * value.nvCant) * 0.19;

                        totalaux = value.nvTotLinea;
                        total = total + totalaux;

                        tableDetalle.append(htmlDetalle);
                    });
                    
                    var colspanTotales = 7;
                    if (parametros.ManejaTallaColor) {
                        colspanTotales = colspanTotales + 1;
                        colspanTotales = colspanTotales + 1;
                    }

                    if (parametros.DescuentoLineaDirectoSoftland) {
                        colspanTotales = colspanTotales + parseInt(parametros.CantidadDescuentosProducto);
                    }

                    var htmldetalleTotal = "";
                    htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>SubTotal</th>";
                    htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(subtotalaux, "$") + "</td>";
                    htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                    htmldetalleTotal = htmldetalleTotal + "</tr>";
                    htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Total Iva</th>";
                    htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(ivaaux, "$") + "</td>";
                    htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                    htmldetalleTotal = htmldetalleTotal + "</tr>";
                    htmldetalleTotal = htmldetalleTotal + "<tr><th colspan='" + colspanTotales + "'>Venta Total</th>";
                    htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'>" + formatearNumero(total, "$") + "</td>";
                    htmldetalleTotal = htmldetalleTotal + "<td style='text-align: right;'></td>";
                    htmldetalleTotal = htmldetalleTotal + "</tr>";
                    tableDetalle.append(htmldetalleTotal);
                }
            }
        });

    } else {
        abrirError("Usuario", "No se puede dejar sin productos");
    }
}


function BuscarCotizacion() {
    var tipoUsuario = $("#tipoUsuario").val();
    activarLoadingBoton("btnBuscarCot");
    var numCotizacion = $("#txtNumCotizacion").val();
    var vendedor = "";
    //var vendedor = $("#listaVendedores").val();
    if ($("#listaVendedores").val() != null) {
        vendedor = $("#listaVendedores").val();
    } else {
        vendedor = $("#filtroVenCod").val();
    }
    var fechaCotizacion = $("#txtfechacotizacion").val();
    var fechaCierre = $("#txtfechacierre").val();
    var subtotal = 0;
    var displayPrecioConDescuento = true;
    var ivatotal = 0;
    var totalconiva = 0;
    var total = 0;
    var tienefechaCot = 0;
    var tienefechaCierre = 0
    var fecha = new Date();
    if (numCotizacion == "") {
        numCotizacion = 0;
    }

    if (fechaCotizacion == "") {
        tienefechaCot = -1
        fechaCotizacion = formatearFecha(fecha);
    } else { tienefechaCot = 1;}
    if (fechaCierre == "") {
        tienefechaCierre = -1;
        fechaCierre = formatearFecha(fecha);
    } else { tienefechaCierre = 1;}

    if (tienefechaCierre == -1 && tienefechaCot == -1 && vendedor == "-1" && numCotizacion == 0 && tipoUsuario!=3) {
        abrirError("Usuario", "Debe selecionar filtro");
        desactivarLoadingBoton("btnBuscarCot");
    }
    else {
        $.ajax({
            type: "POST",
            url: "CotizacionesPendientesBuscar",
            data: {
                numCotizacion,
                vendedor,
                tienefechaCot,
                fechaCotizacion,
                tienefechaCierre,
                fechaCierre
            },
            async: true,
            success: function (data) {
                if (data.Mensaje == 1) {
                    abrirError("Usuario","No se encontró el documento." + $("#txtNumCotizacion").val());
                    desactivarLoadingBoton("btnBuscarCot");
                }
                else {
                    $("#dataPrueba").dataTable().fnClearTable();
                    $("#dataPrueba").dataTable().fnDraw();
                    $("#dataPrueba").dataTable().fnDestroy();
                    
                    var tablaBody = $("#dataPrueba tbody");
                    $.each(data.cabecera, function (index, item) {
                        var filaTbody = "";
                        filaTbody = "<tr>";
                        filaTbody += "<td hidden>" + item.Id + "</td>";
                        filaTbody += "<td>" + item.NroFinalCotizacion + "</td>";
                        filaTbody += "<td>" + item.RutAux + "</td>";
                        filaTbody += "<td>" + item.NomAux + "</td>";
                        filaTbody += "<td>" + item.nvFemString + "</td>";
                        filaTbody += "<td>" + item.VenDes + "</td>";
                        filaTbody += "<td style='text-align: right;'> $" + agregarSeparadorMiles(Math.round(item.nvNetoAfecto)) + "</td>";
                        filaTbody += "<td style='text-align: right;'> $" + agregarSeparadorMiles(item.nvMonto) + "</td>";
                        filaTbody += "<td>" + item.EstadoNP + "</td>";
                        filaTbody = filaTbody + "<td><button class='btn btn-warning btn-xs' data-toggle='modal' data-target='#CabeceraCotizacion' onclick='EditarCotizacion(" + item.Id + ")'>Editar</button></td>";

                        filaTbody += "<td> <button class='btn btn-info btn-xs' data-toggle='modal' data-target='#DetalleNotaPedido' onclick='DetalleCotizacion(" + item.Id + ")'>Detalle</button> ";
                        if (tipoUsuario != 2) {
                            filaTbody += "<button class='btn btn-dark btn-xs' onclick='DescargarArchivos(" + item.Id + ")'>Descargar</button></td>";
                        } else {
                            filaTbody += "</td>";
                        }
                        
                        filaTbody+= "<td> <button class='fa fa-download btn btn-warning btn-xs' onclick='DescargarPdf("+item.Id+")'></button></td>";
                        filaTbody += "</tr>";
                      
                        tablaBody.append(filaTbody);
                        });
                        try {
                        $("#dataPrueba").dataTable().fnDestroy();
                        $("#dataPrueba").dataTable({
                            ordering: false,
                            searching: false,
                            language: {
                                "sProcessing": "Procesando...",
                                "sLengthMenu": "Mostrar _MENU_ Registros",
                                "sZeroRecords": "&ndsp;",
                                "sEmptyTable": "&ndsp;",
                                "sInfo": "Encontrados: _TOTAL_ Registros (Mostrando del _START_ al _END_)",
                                "sInfoEmpty": "* No se han encontrado resultados en la búsqueda",
                                "sInfoFiltered": "",
                                "sInfoPostFix": "",
                                "sSearch": "Buscar:",
                                "sUrl": "",
                                "sInfoThousands": ",",
                                "sLoadingRecords": "Cargando...",
                                "oPaginate": {
                                    "sFirst": "Primero",
                                    "sLast": "Último",
                                    "sNext": "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            },
                            aoColumnDefs: [{ 'bSortable': false, 'aTargets': ['no-sortable'] }]
                        });
                    }
                    catch (e) { console.log(e); }
                    
                }
                desactivarLoadingBoton("btnBuscarCot");
            }
        });
        limpiarCampos();
    }
}

const formatearFecha = fecha => {
    const mes = fecha.getMonth() + 1; // meses los cuenta desde el 0
    const dia = fecha.getDate();
    return `${fecha.getFullYear()}-${(mes < 10 ? '0' : '').concat(mes)}-${(dia < 10 ? '0' : '').concat(dia)}`;
};


function DescargarPdf(idCotizacion) {
    $.ajax({
        type: "POST",
        url: "GenerePdfCotizacion",
        data: {
            Id : idCotizacion
        },
        async: true,
        success: function (data) {
            window.location = 'DescargarArchivoCotizacion?idCotizacion=' + idCotizacion;
        }
    });
}

function DescargarArchivos(idCotizacion) {
        window.location = 'DescargarArchivosCotizacionAdjunto?IdCotizacion=' + idCotizacion;
}

function limpiarCampos() {
    $("#txtNumCotizacion").val("");
    $("#txtfechacotizacion").val("");
    $("#txtfechacierre").val("");
}