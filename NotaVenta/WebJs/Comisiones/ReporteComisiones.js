﻿$(document).ready(function () {
    $("#btnCrearReporte").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();

        var fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
        var fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");
        var vendedores = $("#ddlVendedores").val();

        if (vendedores.length == 0) {
            vendedores = vendedoresTodos;
        } else {
            vendedores = vendedoresTodos.filter(m => {
                var vendedortemp = vendedores.find(m2 => m2 === m.VenCod);
                if (vendedortemp !== undefined) { return vendedortemp; }
            });
        }

        if (fechaDesde !== "" && fechaHasta !== "") {
            if (vendedores != null && vendedores != undefined && vendedores != "" && vendedores.length > 0) {
                $("#divCreandoReporte").fadeIn();
                $.ajax({
                    type: "POST",
                    url: "GenerarReporteComisiones",
                    data: {
                        fechaDesde: fechaDesde,
                        fechaHasta: fechaHasta,
                        vendedores: vendedores
                    },
                    async: true,
                    success: function (response) {
                        $("#divCreandoReporteCreadoTabla").html("");

                        if (response.Verificador !== undefined) {
                            if (response.Verificador) {
                                $("#divCreandoReporteCargando").fadeOut("", function () {
                                    $("#divCreandoReporteCreado").fadeIn();
                                });
                            }
                            else {
                                $("#divCreandoReporteErrorMensaje").html(response.Mensaje);
                                $("#divCreandoReporte").fadeOut("", function () {
                                    $("#divCreandoReporteError").fadeIn();
                                });
                            }
                        }
                        else {
                            var tabla = "";
                            $.each(response, function (index, item) {
                                console.log(item);
                                var vendedor = vendedoresTodos.find(m => m.VenCod === item.VenCod);
                                tabla = tabla + "<tr>";
                                tabla = tabla + "<td>";
                                tabla = tabla + vendedor.VenCod;
                                tabla = tabla + "</td>";
                                tabla = tabla + "<td>";
                                tabla = tabla + vendedor.VenDes;
                                tabla = tabla + "</td>";
                                tabla = tabla + "<td>";
                                tabla = tabla + "<button name='____btnDescargar' id='____btnDescargar" + vendedor.VenCod + "' onclick='btnDescargarClick(\"____btnDescargar\", \"" + vendedor.VenCod + "\")'><i class='fa fa-download'></i></button>";

                                tabla = tabla + "</td>";
                                tabla = tabla + "<td>";
                                tabla = tabla + "<button name='____btnDescargarexcel' id='____btnDescargarexcel" + vendedor.VenCod + "' onclick='btnDescargarexcelClick(\"____btnDescargarexcel\", \"" + vendedor.VenCod + "\")'><i class='fa fa-download'></i></button>";
                                tabla = tabla + "</td>";
                                tabla = tabla + "</tr>";

                            });
                            $("#divCreandoReporteCreadoTabla").html(tabla);



                            $("#divCreandoReporteCargando").fadeOut("", function () {
                                $("#divCreandoReporteCreado").fadeIn();
                            });
                        }
                    },
                    error: function (a, b, c) {
                        $("#divCreandoReporteErrorMensaje").html("Se produjo un error al generar el reporte, favor reintentelo, si el problema persiste, comuniquese con su administrador");
                        $("#divCreandoReporte").fadeOut("", function () {
                            $("#divCreandoReporteError").fadeIn();
                        });
                    }
                });
            }
            else {
                abrirError("Ingreso Datos", "Favor debe ingresar a lo menos 1 vendedor");
            }
        }
        else {
            abrirError("Ingreso Datos", "Favor ingrese fecha a consultar");
        }
    });
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoComisiones");
    });





    var texto = "" + fechaSeleccionada.fechaDesde.format('dddd') + ", " + fechaSeleccionada.fechaDesde.format('DD') + " de " + fechaSeleccionada.fechaDesde.format('MMMM') + " de " + fechaSeleccionada.fechaDesde.format('YYYY');
    texto = texto + " - ";
    texto = texto + "" + fechaSeleccionada.fechaHasta.format('dddd') + ", " + fechaSeleccionada.fechaHasta.format('DD') + " de " + fechaSeleccionada.fechaHasta.format('MMMM') + " de " + fechaSeleccionada.fechaHasta.format('YYYY');

    $('#rangoFecha span').html(texto);

    $('#rangoFecha').daterangepicker({
        format: 'DD/MM/YYYY',
        startDate: moment().subtract(7, 'days'),
        endDate: moment(),
        //minDate: '01/01/2012',
        //maxDate: '12/31/2015',
        //dateLimit: { days: 60 },
        showDropdowns: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment()]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Aceptar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'a',
            customRangeLabel: 'Personalizado',
            daysOfWeek: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            firstDay: 1
        }
    }, function (start, end, label) {
        fechaSeleccionada.fechaDesde = start;
        fechaSeleccionada.fechaHasta = end;

        var texto = "" + fechaSeleccionada.fechaDesde.format('dddd') + ", " + fechaSeleccionada.fechaDesde.format('DD') + " de " + fechaSeleccionada.fechaDesde.format('MMMM') + " de " + fechaSeleccionada.fechaDesde.format('YYYY');
        texto = texto + " - ";
        texto = texto + "" + fechaSeleccionada.fechaHasta.format('dddd') + ", " + fechaSeleccionada.fechaHasta.format('DD') + " de " + fechaSeleccionada.fechaHasta.format('MMMM') + " de " + fechaSeleccionada.fechaHasta.format('YYYY');

        $('#rangoFecha span').html(texto);
    });

    $("#ddlVendedores").select2({ placeholder: "Seleccione Vendedores" });

    $.ajax({
        type: "POST",
        url: "ObtenerVendedoresEmpresa",
        data: {},
        async: true,
        success: function (response) {
            vendedoresTodos = response;
            var opcion = "<optgroup label='Vendedores'>";

            $("#ddlVendedores").append(opcion);

            $.each(response, function (index, item) {
                opcion = "<option value='" + item.VenCod + "'>" + item.VenDes + "</option>";

                $("#ddlVendedores").append(opcion);
            });

            opcion = "</optgroup>";

            $("#ddlVendedores").append(opcion);
        },
        error: function (a, b, c) {
        }
    });

});

function btnDescargarClick(nameBTN, VenCod) {
    var botones = $("*[name=____btnDescargar]");
    for (i = 0; i < botones.length; i++) {
        activarLoadingBoton(botones[i].id);
    }
    $.ajax({
        type: "POST",
        url: "PreDescargarArchivoComisiones",
        data: {
            VenCod: VenCod
        },
        async: true,
        success: function (response) {
            window.location = ("DescargarArchivoComisiones");
            for (i = 0; i < botones.length; i++) {
                desactivarLoadingBoton(botones[i].id);
            }
        },
        error: function () {
            for (i = 0; i < botones.length; i++) {
                desactivarLoadingBoton(botones[i].id);
            }
        }
    });
}


function btnDescargarexcelClick(nameBTN, VenCod) {
    var botones = $("*[name=____btnDescargarexcel]");
    for (i = 0; i < botones.length; i++) {
        activarLoadingBoton(botones[i].id);
    }
    $.ajax({
        type: "POST",
        url: "PreDescargarArchivoComisionesexcel",
        data: {
            VenCod: VenCod
        },
        async: true,
        success: function (response) {
            window.location = ("DescargarArchivoComisiones");
            for (i = 0; i < botones.length; i++) {
                desactivarLoadingBoton(botones[i].id);
            }
        },
        error: function () {
            for (i = 0; i < botones.length; i++) {
                desactivarLoadingBoton(botones[i].id);
            }
        }
    });
}

var vendedoresTodos = [];

var fechaSeleccionada = {
    fechaDesde: moment().subtract('days', 7),
    fechaHasta: moment()
};