﻿$(document).ready(function () {
    $("#btnCrearReporte").click(function () {
        $("#divCreandoReporte").fadeOut();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        $("#textoDescarga").show();
        var fechaDesde = fechaSeleccionada.fechaDesde.format("YYYY-MM-DD");
        var fechaHasta = fechaSeleccionada.fechaHasta.format("YYYY-MM-DD");

        if (fechaDesde !== "" && fechaHasta !== "") {
            $("#divCreandoReporte").fadeIn();
            $.ajax({
                type: "POST",
                url: "GeneraReporteComisionesConsolidado",
                data: {
                    fechaDesde: fechaDesde,
                    fechaHasta: fechaHasta
                },
                async: true,
                success: function (response) {
                    $("#divCreandoReporteCreadoTabla").html("");
                    if (response.Verificador != undefined) {
                        if (response.Verificador) {
                            $("#divCreandoReporteCargando").fadeOut("", function () {
                                $("#divCreandoReporteCreado").fadeIn();
                            });
                        }
                        else {
                            $("#divCreandoReporteErrorMensaje").html(response.Mensaje);
                            $("#divCreandoReporteError").fadeOut("", function () {
                                $("#divCreandReporteError").fadeIn();
                            });
                        }
                    }
                    else {
                        var tabla = "";

                        tabla = tabla + "<tr>";
                        tabla = tabla + "<td>";
                        tabla = tabla + "<button name='____btnDescargar' id='____btnDescargar ' onclick='btnDescargarClick(\"____btnDescargar\")'><i class='fa fa-download'></i></button>";

                        tabla = tabla + "</td>";
                        tabla = tabla + "<td>";
                        tabla = tabla + "<button name='____btnDescargarexcel' id='____btnDescargarexcel ' onclick='btnDescargarexcelClick(\"____btnDescargarexcel\")'><i class='fa fa-download'></i></button>";
                        tabla = tabla + "</td>";
                        tabla = tabla + "</tr>";

                        $("#divCreandoReporteCreadoTabla").html(tabla);
                        $("#textoDescarga").hide();
                        $("#divCreandoReporteCargando").fadeOut("", function () {
                            $("#divCreandoReporteCreado").fadeIn();
                        });
                    }
                },
                error: function (a, b, c) {
                    $("#divCreandoReporteErrorMensaje").html("Se produjo un error al generar el reporte, favor reintentelo, si el problema persiste, comuniquese con su administrador");
                    $("#divCreandoReporte").fadeOut("", function () {
                        $("#divCreandoReporteError").fadeIn();
                    });
                }
            });
        }
        else {
            abrirError("Ingreso Datos", "Favor ingrese fecha a consultar");
        }
    });
    $("#divCreandoReporteCreadoDescargar").click(function () {
        $("#divCreandoReporte").fadeout();
        $("#divCreandoReporteCargando").fadeIn();
        $("#divCreandoReporteCreado").fadeOut();
        $("#divCreandoReporteError").fadeOut();
        window.open("DescargarArchivoConsolidado");
    });




    var texto = "" + fechaSeleccionada.fechaDesde.format('dddd') + ", " + fechaSeleccionada.fechaDesde.format('DD') + " de " + fechaSeleccionada.fechaDesde.format('MMMM') + " de " + fechaSeleccionada.fechaDesde.format('YYYY');
    texto = texto + " - ";
    texto = texto + "" + fechaSeleccionada.fechaHasta.format('dddd') + "," + fechaSeleccionada.fechaHasta.format('DD') + " de " + fechaSeleccionada.fechaHasta.format('MMM') + " de " + fechaSeleccionada.fechaHasta.format('YYYY');

    $('#rangoFecha span').html(texto);

    $('#rangoFecha').daterangepicker({
        format: 'DD/MM/YYYY',
        startDate: moment().subtract(7, 'Days'),
        endDate: moment(),
        showDropdows: true,
        showWeekNumbers: true,
        timePicker: false,
        timePickerIncrement: 1,
        timePicker12Hour: true,
        ranges: {
            'Hoy': [moment(), moment()],
            'Ayer': [moment().subtract(1, 'days'), moment()],
            'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
            'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
            'Este Mes': [moment().startOf('month'), moment().endOf('month')],
            'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        opens: 'right',
        drops: 'down',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-primary',
        cancelClass: 'btn-default',
        separator: ' to ',
        locale: {
            applyLabel: 'Aceptar',
            cancelLabel: 'Cancelar',
            fromLabel: 'Desde',
            toLabel: 'a',
            customRangeLabel: 'Personalizado',
            daysOfWeek: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
            monthName: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            firstDay: 1
        }
    }, function (start, end, label) {
        fechaSeleccionada.fechaDesde = start;
        fechaSeleccionada.fechaHasta = end;

        var texto = "" + fechaSeleccionada.fechaDesde.format('dddd') + ", " + fechaSeleccionada.fechaDesde.format('DD') + " de " + fechaSeleccionada.fechaDesde.format('MMMM') + " de " + fechaSeleccionada.fechaDesde.format('YYYY');
        texto = texto + " - ";
        texto = texto + "" + fechaSeleccionada.fechaHasta.format('dddd') + ", " + fechaSeleccionada.fechaHasta.format('DD') + " de " + fechaSeleccionada.fechaHasta.format('MMMM') + " de " + fechaSeleccionada.fechaHasta.format('YYYY');

        $('#rangoFecha span').html(texto);
    });


});

function btnDescargarClick(nameBTN) {
    var botones = $("*[name=____btnDescargar]");
    for (i = 0; i < botones.length; i++) {
        activarLoadingBoton(botones[i].id);
    }
    window.location = ("DescargarArchivoConsolidado");
    for (i = 0; i < botones.length; i++) {
        desactivarLoadingBoton(botones[i].id);
    }

}

function btnDescargarexcelClick(nameBTN) {
    var botones = $("*[name=____btnDescargarexcel]");
    for (i = 0; i < botones.length; i++) {
        activarLoadingBoton(botones[i].id);
    }
    window.location = ("DescargarArchivoConsolidadoExcel");
    for (i = 0; i < botones.length; i++) {
        desactivarLoadingBoton(botones[i].id);
    }
}


var fechaSeleccionada = {
    fechaDesde: moment().subtract('days', 7),
    fechaHasta: moment()
};

