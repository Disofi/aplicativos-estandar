﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class COT_ClienteModel
    {
        public string codaux { get; set; }
        public string cliente { get; set; }
        public string NoFAux { get; set; }
        public string rut { get; set; }
        public string direccion { get; set; }
        public string fono { get; set; }
        public string Ciudad { get; set; }
        public string Comuna { get; set; }
        public string Moneda { get; set; }
        public string CodEdi { get; set; }
        public string NombreContacto { get; set; }
        public string FonoContacto { get; set; }
        public string EmailContacto { get; set; }
    }
}

