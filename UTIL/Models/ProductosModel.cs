﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ProductosModel
    {
        public int NVNumero { get; set; }
        public string nvFecCompr { get; set; }
        public string DesProd { get; set; }
        public double nvCant { get; set; }
        public double nvPrecio { get; set; }
        public double nvTotLinea { get; set; }

    }
}
