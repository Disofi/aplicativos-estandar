﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class DetalleVentasModel
    {
        public decimal Folio { get; set; }
        public string Fecha { get; set; }
        public string Tipo { get; set; }
        public double Total { get; set; }
    }
}
