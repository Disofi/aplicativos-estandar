﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ReporteComisionConsolidadoModel
    {
        public List<ComisionModel> Comisiones { get; set; }
        public List<ReporteComisionesConsolidado> Consolidado { get; set; }

    }
}
