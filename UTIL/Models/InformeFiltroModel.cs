﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class InformeFiltroModel
    {
        public double VentaTotalFiltro { get; set; }
        public double VentaTotalFiltroAnterior { get; set; }

        public int CantClientesFiltro { get; set; }
        public int CantClientesFiltroAnterior { get; set; }

        public string nombreClienteFiltro { get; set; }
        public double VentaTotalCliFiltro { get; set; }

        public int CantVendedorFiltro { get; set; }

        public string nombreVendedorFiltro { get; set; }
        public double VentaTotalVendedorFiltro { get; set; }

        public string codProductoFiltro { get; set; }
        public string nombreProductoFiltro { get; set; }
        public double VentaTotalProductoFiltro { get; set; }

        public int CantProductosFiltro { get; set; }
    }
}
