﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ValorAdicionalModel
    {
        public int Id { get; set; }
        public int IdNotaVentaDetalle { get; set; }
        public int? ValorAdicional { get; set; }
    }
}
