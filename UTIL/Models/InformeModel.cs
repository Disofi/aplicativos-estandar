﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class InformeModel
    {
        public double VentaTotalHoy { get; set; }
        public double VentaTotalAnterior { get; set; }
        public int CantClientesActual { get; set; }
        public int CantClientesActualAnterior { get; set; }
        public string nombreCliente { get; set; }
        public double VentaTotal { get; set; }
        public int CantVendedorActual { get; set; }
        public int CantVendedorActualAnterior { get; set; }
        public string nombreVendedor { get; set; }
        public double VentaTotalVendedor { get; set; }
        public double VentaMesAnterior { get; set; }
        //public string Bodega { get; set; }
        public double VentaTotalDia { get; set; }
        public double VentaTotalDiaAnterior { get; set; }
        public int CantClientesDia { get; set; }
        public int CantClientesDiaAnterior { get; set; }
        public string codProducto { get; set; }
        public string nombreProducto { get; set; }
        public double VentaTotalProducto { get; set; }
        public int CantProductos { get; set; }
    }
}
