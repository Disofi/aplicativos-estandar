﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ReporteCotizacionModel
    {
        public List<NotadeVentaCabeceraModels> Cabecera { get; set; }
        public List<NotaDeVentaDetalleModels> Detalle { get; set; }
        public List<COT_ClienteModel> Cliente { get; set; }
        public List<COT_DatoEmpresaModel> DatosEmpresa { get; set; }
    }
}
