﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class MonedaModel
    {
        private string codigoMoneda;
        private string descripcionMoneda;
        private string simboloMoneda;
        private int decimalesPrecioMoneda;

        public string CodigoMoneda { get { return this.codigoMoneda; } set { this.codigoMoneda = value; } }
        public string DescripcionMoneda { get { return this.descripcionMoneda; } set { this.descripcionMoneda = value; } }
        public string SimboloMoneda { get { return this.simboloMoneda; } set { this.simboloMoneda = value; } }
        public int DecimalesPrecioMoneda { get { return this.decimalesPrecioMoneda; } set { this.decimalesPrecioMoneda = value; } }
    }
}
