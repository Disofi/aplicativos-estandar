﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
     public class ReporteComisionesConsolidado
    {
        public string CodigoVendedor { get; set; }
        public string Vendedor { get; set; }
        public double SumadeTotalVENTA { get; set; }
        public double SumaMargenenPesos { get; set; }
        public double SumadeCostoxCantidad { get; set; }
        public double SumadeComisionPesos { get; set; }
        public double CantidaddeNotasCreditos { get; set; }
        public double MontodeNotaCredito { get; set; }
        public double MGenporcentaje { get; set; }
    }
}
