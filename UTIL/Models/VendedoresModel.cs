﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class VendedoresModel
    {
        public string VenCod { get; set; }
        public string VenDes { get; set; }
        public string CodTipV { get; set; }
        public string EMail { get; set; }
        public string Usuario { get; set; }
    }
}
