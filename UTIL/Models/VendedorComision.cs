﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class VendedorComision
    {
        public string CodigoVendedor { get; set; }
        public string Nombre { get; set; }
        public double lp1 { get; set; }
        public double lp2 { get; set; }
        public double lp3 { get; set; }
        public double lp4 { get; set; }

    }
}
