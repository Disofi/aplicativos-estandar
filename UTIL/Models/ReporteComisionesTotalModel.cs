﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ReporteComisionesTotalModel
    {
        public string CodigoVendedor { get; set; }
        public double TotalFacturado { get; set; }
        public double TotalNotaCredito { get; set; }
        public double TotalVendidoReal { get; set; }
        public double TotalComision { get; set; }
        public double TotalFacturadoGeneral { get; set; }
        public double TotalNotaCreditoGeneral { get; set; }
        public double TotalVendidoRealGeneral { get; set; }
        public double TotalComisionGeneral { get; set; }
    }
}
