﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class SubGrupoModel
    {
        private string codigoSubGrupo;
        private string descripcionSubGrupo;

        public string CodigoSubGrupo { get { return this.codigoSubGrupo; } set { this.codigoSubGrupo = value; } }
        public string DescripcionSubGrupo { get { return this.descripcionSubGrupo; } set { this.descripcionSubGrupo = value; } }
    }
}
