﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class COT_DatoEmpresaModel
    {
        public string direccionEmpresa { get; set; }
        public string RutE { get; set; }
        public string NomB { get; set; }
        public string Giro { get; set; }
        public string fono { get; set; }
        public string EmailDTE { get; set; }

    }
}
