﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class UsuarioEmpresaModels
    {
        public int IdUsuario { get; set; }
        public int IdEmpresa { get; set; }
        public string NombreEmpresa { get; set; }
        public string BaseDatos { get; set; }
        public int tipoUsuario { get; set; }

    }
}
