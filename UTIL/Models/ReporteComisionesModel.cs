﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
     public class ReporteComisionesModel
    {
        public string Titulo { get; set; }
        public List<ComisionModel> Comisiones { get; set; }
        public List<ReporteComisionesTotalModel> Totales { get; set; }
    }
}
