﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class InformacionSaldosModel
    {
        public string pccodi { set; get; }
        public string pcdesc { set; get; }
        public string rutaux { set; get; }

        public string nomaux { set; get; }
        public string codaux { set; get; }
        public double saldo { set; get; }

        public double saldonegativo { set; get; }
        public double saldototal { set; get; }

    }
}
