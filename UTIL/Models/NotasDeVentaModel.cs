﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class NotasDeVentaModel
    {
        public string nvestado { get; set; }
        public int nvNumero { get; set; }
        public string nvfem { get; set; }
        public string codAux { get; set; }
        public string VenDes { get; set; }
        public double nvmonto { get; set; }
        public double nvTotLinea { get; set; }
    }
}
