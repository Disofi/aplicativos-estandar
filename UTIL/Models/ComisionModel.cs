﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ComisionModel
    {
        public string CodigoVendedor { get; set; }
        public string TipoFactura { get; set; }
        public string Fecha { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public string ListaPrecio { get; set; }
        public double Cantidad { get; set; }
        public double Precio { get; set; }
        public double Totales { get; set; }
        public double Porcentaje { get; set; }
        public double Comision { get; set; }
        public string Cliente { get; set; }
        public double CostoProm { get; set; }
        public double CostoTotalProm { get; set; }
    }
}
