﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class DiagramaModel
    {
        public int mes { get; set; }
        public double VentaTotalActual { get; set; }

        public int mesAnterior { get; set; }
        public double VentaTotalAnterior { get; set; }

        public int mesTercero { get; set; }
        public double VentaTotalTercero { get; set; }
    }
}
