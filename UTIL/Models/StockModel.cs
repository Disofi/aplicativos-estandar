﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class StockModel
    {
        public string CodigoProducto { get; set; }
        public string DescripcionProducto { get; set; }
        public string UnidadMedida { get; set; }
        public string Grupo { get; set; }
        public string SubGrupo { get; set; }
        public string CodigoBodega { get; set; }
        public string DescripcionBodega { get; set; }
        public string StockTipo   { get; set; }
        public double CantidadStock { get; set; }

        public string CodProd { get; set; }
        public string DesProd { get; set; }
        public string CodGrupo { get; set; }
        public string CodSubGr { get; set; }
        public double PrecioVta { get; set; }
        public string codumed { get; set; }
        public string desumed { get; set; }
        public string CodLista { get; set; }
        public string CodRapido { get; set; }

        public double Stock { get; set; }
    }
}
