﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ConsideracionesModel
    {
        public int Id { get; set; }
        public string Consideracion { get; set; }
        public string Titulo { get; set; }
    }
}
