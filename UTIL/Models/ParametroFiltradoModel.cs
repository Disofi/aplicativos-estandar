﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ParametroFiltradoModel
    {
        public bool valor { get; set; }
        public string parametro { get; set; }
        public int id_Menu { get; set; }
        public string Clase { get; set; }
        public string PieMenu { get; set; }
        public string Titulo { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
        public  int TipoUsuario { get; set; }
        public int Activo { get; set; }
        public int Orden { get; set;
        }
    }
}
