﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class GrupoModel
    {
        private string codigoGrupo;
        private string descripcionGrupo;

        public string CodigoGrupo { get { return this.codigoGrupo; } set { this.codigoGrupo = value; } }
        public string DescripcionGrupo { get { return this.descripcionGrupo; } set { this.descripcionGrupo = value; } }
    }
}
