﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ProductosVentaModel
    {
        public decimal Folio { get; set; }
        public string DetProd { get; set; }
        public double CantFacturada { get; set; }
        public double PreUniMB { get; set; }
        public double TotLinea { get; set; }

    }
}
