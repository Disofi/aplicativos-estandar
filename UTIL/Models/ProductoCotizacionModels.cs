﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class ProductoCotizacionModels
    {
        public int Id { get; set; }
        public string CodProd { get; set; }
        public string DesProd { get; set; }
        public string CodGrupo { get; set; }
        public string CodSubGr { get; set; }
        public double PrecioVta { get; set; }
        public double Cantidad { get; set; }
        public double PorcentajeDescuento  { get; set; }
        public string CodMon  { get; set; }
        public int ProdLocal { get; set; }
        public string DescripcionGrupo { get; set; }
        public string DescripcionSubGrupo { get; set; }
        public string DescripcionMoneda { get; set; }
        public DateTime nvFecCompr { get; set; }
        public string nvFecComprYYYYMMDD2 { get { return this.nvFecCompr == null ? null :( (DateTime)nvFecCompr).ToString("yyyy-MM-dd"); } }
        public string CodigoFabrica { get; set; }
    }
}
