﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class EnvioCorreoCentroCostoModel
    {
        public string CodigoCC { get; set; }
        public int IdEmpresa { get; set; }
        public string Email { get; set; }
    }
}
