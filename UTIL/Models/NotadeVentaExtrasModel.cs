﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UTIL.Models
{
    public class NotadeVentaExtrasModel
    {
        public DateTime FechaCierre { get; set;}
        public int nroSolicitud { get; set; }
    }
}
