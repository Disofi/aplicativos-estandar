USE [master]
GO
/****** Object:  Database [AplicativoEstandar]    Script Date: 18/06/2021 8:55:29 ******/
CREATE DATABASE [AplicativoEstandar]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'AplicativoEstandar', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\AplicativoEstandar.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'AplicativoEstandar_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\AplicativoEstandar_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [AplicativoEstandar] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [AplicativoEstandar].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [AplicativoEstandar] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET ARITHABORT OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [AplicativoEstandar] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [AplicativoEstandar] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET  ENABLE_BROKER 
GO
ALTER DATABASE [AplicativoEstandar] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [AplicativoEstandar] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET RECOVERY FULL 
GO
ALTER DATABASE [AplicativoEstandar] SET  MULTI_USER 
GO
ALTER DATABASE [AplicativoEstandar] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [AplicativoEstandar] SET DB_CHAINING OFF 
GO
ALTER DATABASE [AplicativoEstandar] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [AplicativoEstandar] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [AplicativoEstandar] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'AplicativoEstandar', N'ON'
GO
ALTER DATABASE [AplicativoEstandar] SET QUERY_STORE = OFF
GO
USE [AplicativoEstandar]
GO
/****** Object:  User [prueba]    Script Date: 18/06/2021 8:55:30 ******/
CREATE USER [prueba] FOR LOGIN [prueba] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[FnCodProd]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create FUNCTION [dbo].[FnCodProd] 
(
	@CodProd varchar(20)
)
RETURNS nvarchar(max)
AS
BEGIN
	Declare @Result nvarchar(max)
	Select @Result = CodProd from emeltec.softland.iw_tprod where CodProd = @CodProd
	RETURN @Result
END

GO
/****** Object:  View [dbo].[StockPorBodega]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[StockPorBodega]
AS
SELECT        p.CodProd, mov.CodBode, SUM(CASE WHEN TipoBod = 'D' THEN Ingresos - Egresos ELSE 0 END) AS StockDisponible, SUM(CASE WHEN TipoBod = 'R' THEN Ingresos - Egresos ELSE 0 END) AS StockReserva, 
                         SUM(CASE WHEN TipoBod = 'C' THEN Ingresos - Egresos ELSE 0 END) AS StockConsignacion, SUM(CASE WHEN TipoBod = 'T' THEN Ingresos - Egresos ELSE 0 END) AS StockTransitorio
FROM            EMELTEC.softland.iw_tprod AS p LEFT OUTER JOIN
                         EMELTEC.softland.IW_vsnpMovimStockTipoBod AS mov ON p.CodProd = mov.CodProd
WHERE        (p.Inactivo = 0)
GROUP BY mov.CodBode, p.CodProd
GO
/****** Object:  View [dbo].[StockPorTipoBodega]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[StockPorTipoBodega]
AS
SELECT        CodProd, CodBode, StockTipo, StockCantidad
FROM            [dbo].[StockPorBodega] UNPIVOT (StockCantidad FOR StockTipo IN (StockDisponible, StockReserva, StockConsignacion, StockTransitorio)) unpiv
WHERE        StockCantidad > 0  
GO
/****** Object:  Table [dbo].[DS_Acceso]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Acceso](
	[UsuarioId] [int] NULL,
	[IdMenu] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Consideracion]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Consideracion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Consideracion] [varchar](max) NOT NULL,
	[Titulo] [varchar](200) NOT NULL,
 CONSTRAINT [PK_DS_Consideracion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_CorreosManager]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_CorreosManager](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](150) NULL,
	[Email] [varchar](150) NULL,
 CONSTRAINT [PK_DS_CorreosManager] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Cot_Folios]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Cot_Folios](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Vendedor] [char](4) NOT NULL,
	[NroCotizacion] [int] NOT NULL,
	[Iniciales] [char](2) NOT NULL,
	[NroFinalCotizacion] [varchar](20) NOT NULL,
	[Reiniciado] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Cotizacion]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Cotizacion](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdEmpresaInterna] [int] NOT NULL,
	[EstadoNP] [varchar](1) NULL,
	[NVNumero] [int] NOT NULL,
	[nvFem] [datetime] NULL,
	[nvEstado] [varchar](1) NULL,
	[nvEstFact] [int] NULL,
	[nvEstDesp] [int] NULL,
	[nvEstRese] [int] NULL,
	[nvEstConc] [int] NULL,
	[CotNum] [int] NULL,
	[NumOC] [varchar](12) NOT NULL,
	[nvFeEnt] [datetime] NULL,
	[CodAux] [varchar](10) NULL,
	[VenCod] [varchar](4) NULL,
	[CodMon] [varchar](2) NULL,
	[CodLista] [varchar](3) NULL,
	[nvObser] [text] NULL,
	[nvCanalNV] [varchar](3) NULL,
	[CveCod] [varchar](3) NULL,
	[NomCon] [varchar](30) NULL,
	[CodiCC] [varchar](8) NULL,
	[CodBode] [varchar](10) NULL,
	[nvSubTotal] [float] NULL,
	[nvPorcDesc01] [float] NULL,
	[nvDescto01] [float] NULL,
	[nvPorcDesc02] [float] NULL,
	[nvDescto02] [float] NULL,
	[nvPorcDesc03] [float] NULL,
	[nvDescto03] [float] NULL,
	[nvPorcDesc04] [float] NULL,
	[nvDescto04] [float] NULL,
	[nvPorcDesc05] [float] NULL,
	[nvDescto05] [float] NULL,
	[nvMonto] [float] NULL,
	[nvFeAprob] [datetime] NULL,
	[NumGuiaRes] [int] NULL,
	[nvPorcFlete] [float] NULL,
	[nvValflete] [float] NULL,
	[nvPorcEmb] [float] NULL,
	[nvValEmb] [float] NULL,
	[nvEquiv] [float] NULL,
	[nvNetoExento] [float] NULL,
	[nvNetoAfecto] [float] NULL,
	[nvTotalDesc] [float] NULL,
	[ConcAuto] [varchar](1) NULL,
	[CodLugarDesp] [varchar](30) NULL,
	[SolicitadoPor] [varchar](30) NULL,
	[DespachadoPor] [varchar](30) NULL,
	[Patente] [varchar](9) NULL,
	[RetiradoPor] [varchar](30) NULL,
	[CheckeoPorAlarmaVtas] [varchar](1) NULL,
	[EnMantencion] [int] NULL,
	[Usuario] [varchar](8) NULL,
	[UsuarioGeneraDocto] [varchar](8) NULL,
	[FechaHoraCreacion] [datetime] NULL,
	[Sistema] [varchar](2) NULL,
	[ConcManual] [varchar](1) NULL,
	[RutSolicitante] [varchar](20) NULL,
	[proceso] [varchar](50) NULL,
	[TotalBoleta] [float] NULL,
	[NumReq] [int] NOT NULL,
	[CodVenWeb] [varchar](50) NULL,
	[CodBodeWms] [varchar](10) NULL,
	[CodLugarDocto] [varchar](30) NULL,
	[RutTransportista] [varchar](20) NULL,
	[Cod_Distrib] [varchar](10) NULL,
	[Nom_Distrib] [varchar](60) NULL,
	[MarcaWG] [int] NULL,
	[ErrorAprobador] [bit] NULL,
	[ErrorAprobadorMensaje] [varchar](max) NULL,
 CONSTRAINT [DS_Cotizacion_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_CotizacionDetalle]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_CotizacionDetalle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdNotaVenta] [int] NULL,
	[NVNumero] [int] NOT NULL,
	[nvLinea] [float] NOT NULL,
	[nvCorrela] [float] NULL,
	[nvFecCompr] [datetime] NULL,
	[CodProd] [varchar](20) NULL,
	[nvCant] [float] NULL,
	[nvPrecio] [float] NULL,
	[nvEquiv] [float] NULL,
	[nvSubTotal] [float] NULL,
	[nvDPorcDesc01] [float] NULL,
	[nvDDescto01] [float] NULL,
	[nvDPorcDesc02] [float] NULL,
	[nvDDescto02] [float] NULL,
	[nvDPorcDesc03] [float] NULL,
	[nvDDescto03] [float] NULL,
	[nvDPorcDesc04] [float] NULL,
	[nvDDescto04] [float] NULL,
	[nvDPorcDesc05] [float] NULL,
	[nvDDescto05] [float] NULL,
	[nvTotDesc] [float] NULL,
	[nvTotLinea] [float] NULL,
	[nvCantDesp] [float] NULL,
	[nvCantProd] [float] NULL,
	[nvCantFact] [float] NULL,
	[nvCantDevuelto] [float] NULL,
	[nvCantNC] [float] NULL,
	[nvCantBoleta] [float] NULL,
	[nvCantOC] [float] NULL,
	[DetProd] [text] NULL,
	[CheckeoMovporAlarmaVtas] [varchar](1) NULL,
	[KIT] [varchar](20) NULL,
	[CodPromocion] [int] NULL,
	[CodUMed] [varchar](6) NULL,
	[CantUVta] [float] NULL,
	[Partida] [varchar](20) NULL,
	[Pieza] [varchar](20) NULL,
	[FechaVencto] [datetime] NULL,
	[CantidadKit] [float] NOT NULL,
	[MarcaWG] [int] NULL,
	[PorcIncidenciaKit] [float] NOT NULL,
 CONSTRAINT [DS_CotizacionDetalle_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_cwtauxd]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_cwtauxd](
	[CodAxD] [varchar](10) NOT NULL,
	[NomDch] [varchar](30) NOT NULL,
	[DirDch] [varchar](60) NULL,
	[ComDch] [varchar](7) NULL,
	[CiuDch] [varchar](7) NULL,
	[PaiDch] [varchar](3) NULL,
	[Fon1Dch] [varchar](15) NULL,
	[Fon2Dch] [varchar](15) NULL,
	[Fon3Dch] [varchar](15) NULL,
	[FaxDch] [varchar](15) NULL,
	[AteDch] [varchar](60) NULL,
	[ProviDch] [varchar](5) NULL,
	[RegionDch] [int] NULL,
	[CodPostalDch] [int] NULL,
	[Usuario] [varchar](8) NULL,
	[Proceso] [varchar](100) NULL,
	[FechaUlMod] [datetime] NULL,
	[Sistema] [varchar](2) NULL,
	[CodGLN] [varchar](13) NULL,
	[CodEspWalMart] [float] NULL,
 CONSTRAINT [PK_DS_cwtauxd] PRIMARY KEY CLUSTERED 
(
	[CodAxD] ASC,
	[NomDch] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_cwtauxi]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_cwtauxi](
	[CodAux] [varchar](10) NOT NULL,
	[NomAux] [varchar](60) NULL,
	[NoFAux] [varchar](60) NULL,
	[RutAux] [varchar](20) NULL,
	[ActAux] [varchar](1) NULL,
	[GirAux] [varchar](3) NULL,
	[ComAux] [varchar](7) NULL,
	[CiuAux] [varchar](7) NULL,
	[PaiAux] [varchar](3) NULL,
	[DirAux] [varchar](60) NULL,
	[DirNum] [varchar](10) NULL,
	[FonAux1] [varchar](15) NULL,
	[FonAux2] [varchar](15) NULL,
	[FonAux3] [varchar](15) NULL,
	[FaxAux1] [varchar](15) NULL,
	[FaxAux2] [varchar](15) NULL,
	[ClaCli] [varchar](1) NOT NULL,
	[ClaPro] [varchar](1) NOT NULL,
	[ClaEmp] [varchar](1) NOT NULL,
	[ClaSoc] [varchar](1) NOT NULL,
	[ClaDis] [varchar](1) NOT NULL,
	[ClaOtr] [varchar](1) NOT NULL,
	[DiaPlazo] [varchar](2) NULL,
	[Bloqueado] [varchar](1) NULL,
	[EMail] [varchar](250) NULL,
	[Casilla] [varchar](15) NULL,
	[WebSite] [varchar](250) NULL,
	[Notas] [text] NULL,
	[Region] [int] NULL,
	[TipoSaludo] [int] NULL,
	[DirDpto] [varchar](10) NULL,
	[DirOtro] [varchar](255) NULL,
	[CodPostal] [int] NULL,
	[CodAreaFon] [int] NULL,
	[AnexoFon] [int] NULL,
	[CodAreaFax] [int] NULL,
	[FechaNacim] [datetime] NULL,
	[Username] [varchar](20) NULL,
	[Password] [varchar](12) NULL,
	[PalabraSecreta] [varchar](120) NULL,
	[PreguntaSecreta] [varchar](120) NULL,
	[ClienteDesde] [datetime] NULL,
	[TipoUsuario] [int] NULL,
	[ProvAux] [varchar](5) NULL,
	[eMailDTE] [varchar](250) NULL,
	[esReceptorDTE] [varchar](1) NULL,
	[BloqueadoPro] [varchar](1) NULL,
	[Id_RecepExtranjero] [varchar](20) NULL,
	[PaisRecepExtranjero] [varchar](3) NULL,
	[Usuario] [varchar](8) NULL,
	[Proceso] [varchar](100) NULL,
	[FechaUlMod] [datetime] NULL,
	[ClaPros] [varchar](10) NULL,
	[CodCamp] [varchar](10) NULL,
	[CodOrigen] [varchar](10) NULL,
	[Sistema] [varchar](2) NULL,
	[CtaCliente] [varchar](18) NULL,
	[CtaCliMonExt] [varchar](18) NULL,
	[PasswordResetToken] [uniqueidentifier] NULL,
	[sincronizado] [int] NOT NULL,
 CONSTRAINT [CWTAuxi_PK] PRIMARY KEY CLUSTERED 
(
	[CodAux] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_cwtaxco]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_cwtaxco](
	[CodAuc] [varchar](10) NOT NULL,
	[NomCon] [varchar](30) NOT NULL,
	[CarCon] [varchar](4) NULL,
	[FonCon] [varchar](15) NULL,
	[FonCon2] [varchar](15) NULL,
	[FonCon3] [varchar](15) NULL,
	[FaxCon] [varchar](15) NULL,
	[Casilla] [varchar](15) NULL,
	[Email] [varchar](250) NULL,
	[IDNotas] [varchar](10) NULL,
	[TipoSaludo] [varchar](10) NULL,
	[Usuario] [varchar](10) NULL,
	[Proceso] [varchar](100) NULL,
	[FechaUlMod] [datetime] NULL,
	[Sistema] [varchar](2) NULL,
	[FechaUltEnvCorreo] [datetime] NULL,
 CONSTRAINT [CWTAxCo_PK] PRIMARY KEY CLUSTERED 
(
	[CodAuc] ASC,
	[NomCon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Empresa]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Empresa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[BaseDatos] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_EnvioCorreo_PorCentroCosto]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_EnvioCorreo_PorCentroCosto](
	[CodigoCC] [varchar](100) NULL,
	[IdEmpresa] [int] NULL,
	[Email] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_NotasVenta]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_NotasVenta](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdEmpresaInterna] [int] NOT NULL,
	[EstadoNP] [varchar](1) NULL,
	[NVNumero] [int] NOT NULL,
	[nvFem] [datetime] NULL,
	[nvEstado] [varchar](1) NULL,
	[nvEstFact] [int] NULL,
	[nvEstDesp] [int] NULL,
	[nvEstRese] [int] NULL,
	[nvEstConc] [int] NULL,
	[CotNum] [int] NULL,
	[NumOC] [varchar](12) NOT NULL,
	[nvFeEnt] [datetime] NULL,
	[CodAux] [varchar](10) NULL,
	[VenCod] [varchar](4) NULL,
	[CodMon] [varchar](2) NULL,
	[CodLista] [varchar](3) NULL,
	[nvObser] [text] NULL,
	[nvCanalNV] [varchar](3) NULL,
	[CveCod] [varchar](3) NULL,
	[NomCon] [varchar](30) NULL,
	[CodiCC] [varchar](8) NULL,
	[CodBode] [varchar](10) NULL,
	[nvSubTotal] [float] NULL,
	[nvPorcDesc01] [float] NULL,
	[nvDescto01] [float] NULL,
	[nvPorcDesc02] [float] NULL,
	[nvDescto02] [float] NULL,
	[nvPorcDesc03] [float] NULL,
	[nvDescto03] [float] NULL,
	[nvPorcDesc04] [float] NULL,
	[nvDescto04] [float] NULL,
	[nvPorcDesc05] [float] NULL,
	[nvDescto05] [float] NULL,
	[nvMonto] [float] NULL,
	[nvFeAprob] [datetime] NULL,
	[NumGuiaRes] [int] NULL,
	[nvPorcFlete] [float] NULL,
	[nvValflete] [float] NULL,
	[nvPorcEmb] [float] NULL,
	[nvValEmb] [float] NULL,
	[nvEquiv] [float] NULL,
	[nvNetoExento] [float] NULL,
	[nvNetoAfecto] [float] NULL,
	[nvTotalDesc] [float] NULL,
	[ConcAuto] [varchar](1) NULL,
	[CodLugarDesp] [varchar](30) NULL,
	[SolicitadoPor] [varchar](30) NULL,
	[DespachadoPor] [varchar](30) NULL,
	[Patente] [varchar](9) NULL,
	[RetiradoPor] [varchar](30) NULL,
	[CheckeoPorAlarmaVtas] [varchar](1) NULL,
	[EnMantencion] [int] NULL,
	[Usuario] [varchar](8) NULL,
	[UsuarioGeneraDocto] [varchar](8) NULL,
	[FechaHoraCreacion] [datetime] NULL,
	[Sistema] [varchar](2) NULL,
	[ConcManual] [varchar](1) NULL,
	[RutSolicitante] [varchar](20) NULL,
	[proceso] [varchar](50) NULL,
	[TotalBoleta] [float] NULL,
	[NumReq] [int] NOT NULL,
	[CodVenWeb] [varchar](50) NULL,
	[CodBodeWms] [varchar](10) NULL,
	[CodLugarDocto] [varchar](30) NULL,
	[RutTransportista] [varchar](20) NULL,
	[Cod_Distrib] [varchar](10) NULL,
	[Nom_Distrib] [varchar](60) NULL,
	[MarcaWG] [int] NULL,
	[ErrorAprobador] [bit] NULL,
	[ErrorAprobadorMensaje] [varchar](max) NULL,
 CONSTRAINT [DS_NotasVenta_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_NotasVentaDetalle]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_NotasVentaDetalle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdNotaVenta] [int] NULL,
	[NVNumero] [int] NOT NULL,
	[nvLinea] [float] NOT NULL,
	[nvCorrela] [float] NULL,
	[nvFecCompr] [datetime] NULL,
	[CodProd] [varchar](20) NULL,
	[nvCant] [float] NULL,
	[nvPrecio] [float] NULL,
	[nvEquiv] [float] NULL,
	[nvSubTotal] [float] NULL,
	[nvDPorcDesc01] [float] NULL,
	[nvDDescto01] [float] NULL,
	[nvDPorcDesc02] [float] NULL,
	[nvDDescto02] [float] NULL,
	[nvDPorcDesc03] [float] NULL,
	[nvDDescto03] [float] NULL,
	[nvDPorcDesc04] [float] NULL,
	[nvDDescto04] [float] NULL,
	[nvDPorcDesc05] [float] NULL,
	[nvDDescto05] [float] NULL,
	[nvTotDesc] [float] NULL,
	[nvTotLinea] [float] NULL,
	[nvCantDesp] [float] NULL,
	[nvCantProd] [float] NULL,
	[nvCantFact] [float] NULL,
	[nvCantDevuelto] [float] NULL,
	[nvCantNC] [float] NULL,
	[nvCantBoleta] [float] NULL,
	[nvCantOC] [float] NULL,
	[DetProd] [text] NULL,
	[CheckeoMovporAlarmaVtas] [varchar](1) NULL,
	[KIT] [varchar](20) NULL,
	[CodPromocion] [int] NULL,
	[CodUMed] [varchar](6) NULL,
	[CantUVta] [float] NULL,
	[Partida] [varchar](20) NULL,
	[Pieza] [varchar](20) NULL,
	[FechaVencto] [datetime] NULL,
	[CantidadKit] [float] NOT NULL,
	[MarcaWG] [int] NULL,
	[PorcIncidenciaKit] [float] NOT NULL,
 CONSTRAINT [DS_NotasVentaDetalle_PK] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_NotasVentaDetalleValorAdicional]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_NotasVentaDetalleValorAdicional](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdNotaVentaDetalle] [int] NULL,
	[ValorAdicional] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_NotaVentaExtras]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_NotaVentaExtras](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdNotaVenta] [int] NOT NULL,
	[Cubicaje] [char](15) NOT NULL,
	[Peso] [char](15) NOT NULL,
	[TieneDescuento] [int] NOT NULL,
	[EsCotizacion] [int] NOT NULL,
	[FechaCierre] [datetime] NULL,
	[nroSolicitud] [int] NOT NULL,
	[NroFinalCotizacion] [varchar](20) NOT NULL,
	[IdEmpresaInterna] [int] NOT NULL,
	[IdUsuario] [int] NOT NULL,
	[Estado] [char](1) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Parametros]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Parametros](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdEmpresa] [int] NULL,
	[MultiEmpresa] [bit] NULL,
	[ManejaAdministrador] [bit] NULL,
	[ManejaAprobador] [bit] NULL,
	[ListaClientesVendedor] [bit] NULL,
	[ListaClientesTodos] [bit] NULL,
	[ValidaReglasNegocio] [bit] NULL,
	[ManejaListaPrecios] [bit] NULL,
	[EditaPrecioProducto] [bit] NULL,
	[MuestraCondicionVentaCliente] [bit] NULL,
	[MuestraCondicionVentaTodos] [bit] NULL,
	[EditaDescuentoProducto] [bit] NULL,
	[MaximoDescuentoProducto] [decimal](18, 2) NULL,
	[CantidadDescuentosProducto] [int] NULL,
	[MuestraStockProducto] [bit] NULL,
	[StockProductoEsMasivo] [bit] NULL,
	[StockProductoEsBodega] [bit] NULL,
	[StockProductoCodigoBodega] [varchar](1000) NULL,
	[ControlaStockProducto] [bit] NULL,
	[EnvioMailCliente] [bit] NULL,
	[EnvioMailVendedor] [bit] NULL,
	[EnvioMailContacto] [bit] NULL,
	[EnvioObligatorioAprobador] [bit] NULL,
	[ManejaTallaColor] [bit] NULL,
	[ManejaDescuentoTotalDocumento] [bit] NULL,
	[CantidadDescuentosTotalDocumento] [int] NULL,
	[CantidadLineas] [int] NULL,
	[ManejaLineaCreditoVendedor] [bit] NULL,
	[ManejaLineaCreditoAprobador] [bit] NULL,
	[ManejaCanalVenta] [bit] NULL,
	[CreacionNotaVentaUsuariosBloqueados] [bit] NULL,
	[CreacionNotaVentaUsuariosInactivos] [bit] NULL,
	[PermiteModificacionCondicionVenta] [bit] NULL,
	[AtributoSoftlandDescuentoCliente] [varchar](1000) NULL,
	[PermiteCrearDireccion] [bit] NULL,
	[CrearClienteConDV] [bit] NULL,
	[MuestraUnidadMedidaProducto] [bit] NULL,
	[DescuentoLineaDirectoSoftland] [bit] NULL,
	[DescuentoTotalDirectoSoftland] [bit] NULL,
	[CambioVendedorCliente] [bit] NULL,
	[AgregaCliente] [bit] NULL,
	[EnvioMailAprobador] [bit] NULL,
	[ManejaSaldo] [bit] NULL,
	[CodigoCondicionVentaPorDefecto] [varchar](1000) NULL,
	[StockProductoCodigoBodegaAdicional] [varchar](max) NULL,
	[ManejaValorAdicional] [bit] NULL,
	[ManejaClasificacionCliente] [bit] NULL,
	[ManejaClasificacionProveedor] [bit] NULL,
	[CodigoCentroCostoPorDefecto] [varchar](1000) NULL,
	[CodigoBodegaWMSPorDefecto] [varchar](1000) NULL,
	[CorreosWebConfig] [bit] NULL,
	[ManejaFiltroGrupo] [bit] NOT NULL,
	[ManejaCubicaje] [bit] NOT NULL,
	[ManejaDescuentoPesos] [bit] NULL,
	[Booking] [bit] NULL,
	[Backlog] [bit] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_ProductosNuevos]    Script Date: 18/06/2021 8:55:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_ProductosNuevos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[BaseDatos] [varchar](200) NULL,
	[CodProd] [varchar](20) NULL,
	[DesProd] [varchar](60) NULL,
	[DesProd2] [varchar](60) NULL,
	[CodRapido] [varchar](3) NULL,
	[CodBarra] [varchar](20) NULL,
	[CodUMed] [varchar](6) NULL,
	[Origen] [int] NULL,
	[CodMonOrig] [varchar](2) NULL,
	[CodGrupo] [varchar](10) NULL,
	[CodSubGr] [varchar](10) NULL,
	[CodCateg] [varchar](3) NULL,
	[CodMonPVta] [varchar](2) NULL,
	[PrecioVta] [float] NULL,
	[PrecioBol] [float] NULL,
	[FichaTec] [int] NULL,
	[EsConfig] [int] NULL,
	[FactorConfig] [float] NULL,
	[Impuesto] [int] NULL,
	[Inventariable] [int] NULL,
	[EsSerie] [int] NULL,
	[EsTallaColor] [int] NULL,
	[EsPartida] [int] NULL,
	[EsCaducidad] [int] NULL,
	[EsPieza] [int] NULL,
	[CantPieza] [int] NULL,
	[PesoKgs] [float] NULL,
	[CtaActivo] [varchar](18) NULL,
	[CtaVentas] [varchar](18) NULL,
	[CtaGastos] [varchar](18) NULL,
	[CtaCosto] [varchar](18) NULL,
	[FecUltCom] [datetime] NULL,
	[ValorUltCom] [float] NULL,
	[CostoRep] [float] NULL,
	[FecCostoRep] [datetime] NULL,
	[FecCMonet] [datetime] NULL,
	[ValorCMonet] [float] NULL,
	[NivMin] [float] NULL,
	[NivRep] [float] NULL,
	[NivMax] [float] NULL,
	[Inamovible] [int] NULL,
	[ManejaDim] [int] NULL,
	[Ancho] [float] NULL,
	[esUbicPar] [int] NULL,
	[CtaDevolucion] [varchar](18) NULL,
	[TipProd] [varchar](2) NULL,
	[esParaVenta] [int] NULL,
	[esParaCompra] [int] NULL,
	[EsTalla] [int] NULL,
	[EsColor] [int] NULL,
	[MetodoCosteo] [varchar](1) NULL,
	[CodUMedVta1] [varchar](6) NULL,
	[EquivUMVta1] [float] NULL,
	[PrecioVtaUM1] [float] NULL,
	[PrecioBolUM1] [float] NULL,
	[CodUMedVta2] [varchar](6) NULL,
	[EquivUMVta2] [float] NULL,
	[PrecioVtaUM2] [float] NULL,
	[PrecioBolUM2] [float] NULL,
	[UMDefecto] [int] NULL,
	[ManProdAnticipo] [int] NULL,
	[ImprimeEnBoleta] [int] NULL,
	[EsParaAutoservicio] [int] NULL,
	[Inactivo] [int] NULL,
	[Usuario] [varchar](8) NULL,
	[Proceso] [varchar](50) NULL,
	[FechaUlMod] [datetime] NULL,
	[AgregadoASoftland] [bit] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_UsuarioEmpresa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_UsuarioEmpresa](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUsuario] [int] NULL,
	[IdEmpresa] [int] NULL,
	[VenCod] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Usuarios]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Usuarios](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](100) NOT NULL,
	[Contrasena] [varchar](max) NULL,
	[Cliente] [varchar](50) NULL,
	[CCosto] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[tipoUsuario] [int] NULL,
	[Nombre] [varchar](150) NULL,
	[ContrasenaCorreo] [varchar](100) NULL,
	[Estado] [int] NOT NULL,
	[CodigoUsuario] [varchar](10) NULL,
	[Telefono] [varchar](100) NOT NULL,
	[Direccion] [varchar](200) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_Usuarios_20200811_1749]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_Usuarios_20200811_1749](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Usuario] [varchar](100) NOT NULL,
	[Contrasena] [varchar](max) NULL,
	[Cliente] [varchar](50) NULL,
	[CCosto] [varchar](50) NULL,
	[email] [varchar](50) NULL,
	[tipoUsuario] [int] NULL,
	[Nombre] [varchar](150) NULL,
	[ContrasenaCorreo] [varchar](100) NULL,
	[Estado] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DS_UsuariosTipos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DS_UsuariosTipos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[tipoUsuario] [varchar](20) NOT NULL,
	[urlInicio] [varchar](100) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menu]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Menu](
	[Id_Menu] [int] IDENTITY(1,1) NOT NULL,
	[Clase] [varchar](150) NULL,
	[PieMenu] [varchar](150) NULL,
	[Titulo] [varchar](150) NULL,
	[Action] [varchar](150) NULL,
	[Controller] [varchar](150) NULL,
	[TipoUsuario] [int] NULL,
	[Activo] [int] NULL,
	[Orden] [int] NULL,
 CONSTRAINT [PK_Menu] PRIMARY KEY CLUSTERED 
(
	[Id_Menu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[nventa_manag]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[nventa_manag](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idNotaVenta] [int] NULL,
	[idCorreoManag] [int] NULL,
 CONSTRAINT [PK_nventa_manag] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RutaArchivoAdjunto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RutaArchivoAdjunto](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Ruta] [nvarchar](max) NULL,
	[CodAux] [varchar](50) NULL,
	[Estado] [int] NULL,
 CONSTRAINT [PK_RutaArchivoAdjunto] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tabla]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tabla](
	[CodAux] [varchar](10) NOT NULL,
	[NomAux] [varchar](60) NULL,
	[DirAux] [varchar](60) NULL,
	[DirNum] [varchar](10) NULL,
	[FonAux1] [varchar](15) NULL,
	[Notas] [text] NULL,
	[DeudaVencida] [float] NULL,
	[Deuda] [float] NULL,
	[Credito] [float] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (82, 1)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 9)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 11)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 13)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 16)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 20)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 23)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 1022)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (76, 1023)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 7)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 22)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 9)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 11)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 13)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 16)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 20)
INSERT [dbo].[DS_Acceso] ([UsuarioId], [IdMenu]) VALUES (79, 23)
SET IDENTITY_INSERT [dbo].[DS_CorreosManager] ON 

INSERT [dbo].[DS_CorreosManager] ([Id], [Nombre], [Email]) VALUES (1, N'pamela alcantara', N'palcantara@disofi.cl')
INSERT [dbo].[DS_CorreosManager] ([Id], [Nombre], [Email]) VALUES (2, N'Nicolas Espinoza', N'nespinoza@disofi.cl')
SET IDENTITY_INSERT [dbo].[DS_CorreosManager] OFF
SET IDENTITY_INSERT [dbo].[DS_Cot_Folios] ON 

INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (6, N'99  ', 11, N'LV', N'202106CM011 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (7, N'34  ', 26, N'VG', N'202106LV026 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (8, N'95  ', 2, N'RS', N'202103RS002  ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (9, N'37  ', 160, N'BM', N'202106BM160', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (10, N'01  ', 6, N'E ', N'202106E006  ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (11, N'89  ', 120, N'EC', N'202106EC120', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (12, N'39  ', 85, N'MV', N'202106MV085 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (13, N'35  ', 52, N'NP', N'202106NP052 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (14, N'40  ', 121, N'FD', N'202106FD121', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (15, N'32  ', 35, N'HT', N'202105HT035 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (16, N'31  ', 106, N'JM', N'202106JM106', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (17, N'41  ', 21, N'CG', N'202106CG021 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (18, N'38  ', 22, N'CM', N'202106CM022 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (19, N'24  ', 32, N'PA', N'202106PA032 ', 0)
INSERT [dbo].[DS_Cot_Folios] ([Id], [Vendedor], [NroCotizacion], [Iniciales], [NroFinalCotizacion], [Reiniciado]) VALUES (20, N'7   ', 2, N'v ', N'202106v002  ', 0)
SET IDENTITY_INSERT [dbo].[DS_Cot_Folios] OFF
SET IDENTITY_INSERT [dbo].[DS_Empresa] ON 

INSERT [dbo].[DS_Empresa] ([Id], [Nombre], [BaseDatos]) VALUES (2, N'DEMO', N'DEMO')
SET IDENTITY_INSERT [dbo].[DS_Empresa] OFF
SET IDENTITY_INSERT [dbo].[DS_Parametros] ON 

INSERT [dbo].[DS_Parametros] ([Id], [IdEmpresa], [MultiEmpresa], [ManejaAdministrador], [ManejaAprobador], [ListaClientesVendedor], [ListaClientesTodos], [ValidaReglasNegocio], [ManejaListaPrecios], [EditaPrecioProducto], [MuestraCondicionVentaCliente], [MuestraCondicionVentaTodos], [EditaDescuentoProducto], [MaximoDescuentoProducto], [CantidadDescuentosProducto], [MuestraStockProducto], [StockProductoEsMasivo], [StockProductoEsBodega], [StockProductoCodigoBodega], [ControlaStockProducto], [EnvioMailCliente], [EnvioMailVendedor], [EnvioMailContacto], [EnvioObligatorioAprobador], [ManejaTallaColor], [ManejaDescuentoTotalDocumento], [CantidadDescuentosTotalDocumento], [CantidadLineas], [ManejaLineaCreditoVendedor], [ManejaLineaCreditoAprobador], [ManejaCanalVenta], [CreacionNotaVentaUsuariosBloqueados], [CreacionNotaVentaUsuariosInactivos], [PermiteModificacionCondicionVenta], [AtributoSoftlandDescuentoCliente], [PermiteCrearDireccion], [CrearClienteConDV], [MuestraUnidadMedidaProducto], [DescuentoLineaDirectoSoftland], [DescuentoTotalDirectoSoftland], [CambioVendedorCliente], [AgregaCliente], [EnvioMailAprobador], [ManejaSaldo], [CodigoCondicionVentaPorDefecto], [StockProductoCodigoBodegaAdicional], [ManejaValorAdicional], [ManejaClasificacionCliente], [ManejaClasificacionProveedor], [CodigoCentroCostoPorDefecto], [CodigoBodegaWMSPorDefecto], [CorreosWebConfig], [ManejaFiltroGrupo], [ManejaCubicaje], [ManejaDescuentoPesos], [Booking], [Backlog]) VALUES (1, 2, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, CAST(100.00 AS Decimal(18, 2)), 1, 1, 0, 1, N'01', 0, 0, 1, 0, 1, 0, 0, 0, 30, 0, 1, 0, 0, 0, 1, N'', 1, 0, 0, 1, 0, 1, 1, 1, 1, N'01', N'', 0, NULL, NULL, NULL, NULL, 1, 0, 0, 0, 1, 1)
SET IDENTITY_INSERT [dbo].[DS_Parametros] OFF
SET IDENTITY_INSERT [dbo].[DS_UsuarioEmpresa] ON 

INSERT [dbo].[DS_UsuarioEmpresa] ([Id], [IdUsuario], [IdEmpresa], [VenCod]) VALUES (96, 76, 2, N'6         ')
INSERT [dbo].[DS_UsuarioEmpresa] ([Id], [IdUsuario], [IdEmpresa], [VenCod]) VALUES (97, 79, 2, N'7         ')
INSERT [dbo].[DS_UsuarioEmpresa] ([Id], [IdUsuario], [IdEmpresa], [VenCod]) VALUES (98, 82, 2, N'8         ')
SET IDENTITY_INSERT [dbo].[DS_UsuarioEmpresa] OFF
SET IDENTITY_INSERT [dbo].[DS_Usuarios] ON 

INSERT [dbo].[DS_Usuarios] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado], [CodigoUsuario], [Telefono], [Direccion]) VALUES (76, N'aprobador', N'81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, N'palcantara@disofi.cl', 3, N'aprobador', NULL, 1, N'6', N'', N'')
INSERT [dbo].[DS_Usuarios] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado], [CodigoUsuario], [Telefono], [Direccion]) VALUES (79, N'vendedor', N'81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, N'palcantara@disofi.cl', 2, N'vendedor', NULL, 1, N'7', N'', N'')
INSERT [dbo].[DS_Usuarios] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado], [CodigoUsuario], [Telefono], [Direccion]) VALUES (82, N'adm', N'81dc9bdb52d04dc20036dbd8313ed055', NULL, NULL, N'palcantara@disofi.cl', 1, N'admin', NULL, 1, N'8', N'', N'')
SET IDENTITY_INSERT [dbo].[DS_Usuarios] OFF
SET IDENTITY_INSERT [dbo].[DS_Usuarios_20200811_1749] ON 

INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (1, N'adm', N'81dc9bdb52d04dc20036dbd8313ed055', N'CAFSANMART', N'057', N'prueba.disofi@gmail.com', 1, N'Admin', N'Disofi2019', 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (4, N'reporte', N'81dc9bdb52d04dc20036dbd8313ed055', N'CAFSANMART', N'057', N'prueba.disofi@gmail.com', 4, N'Reporte', N'Disofi2019', 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (12, N'vendedor2', N'81dc9bdb52d04dc20036dbd8313ed055', N'', N'', N'palcantara@disofi.cl', 2, N'VENDEDOR', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (13, N'aprobador', N'81dc9bdb52d04dc20036dbd8313ed055', N'', N'', NULL, 3, N'APROBADOR', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (14, N'LVillegas', N'2ee329747c88652b42cb75dd0e21e09f', NULL, NULL, N'lvillegas@emeltec.cl', 2, N'Luis Villegas', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (15, N'Lmanchileo', N'663fc65823e205107dc8997105201355', NULL, NULL, N'lmanchileo@emeltec.cl', 2, N'Lorenzo Manchileo', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (16, N'Fnavarrete', N'680f5ee46f8a49204f53930dc976d402', NULL, NULL, N'fnavarrete@emeltec.cl', 2, N'Fernando Navarrete', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (17, N'Dcarrasco', N'e129b28dbfff44711cf3880a686110f1', NULL, NULL, N'dcarrasco@emeltec.cl', 2, N'Diego Carrasco', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (18, N'Htorres', N'f274728665ce2a0e4a4edf58c1f6edc0', NULL, NULL, N'htorres@emeltec.cl', 2, N'Heidy Torres', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (19, N'Porellana', N'2c78e6a4e2647ed34386a9dd7b3ea222', N'', N'', NULL, 3, N'Paula Orellana', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (20, N'Macevedo', N'21f8499640e75825ef468ff73b5cd5b3', N'', N'', N'macevedo@emeltec.cl', 2, N'Marcelo Acevedo', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (21, N'Asegura', N'a911b33a0263d0eca7702abd573a26c5', NULL, NULL, NULL, 3, N'Alexa Segura', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (22, N'Parroyo', N'5442b561e00e959683592de00b657419', NULL, NULL, N'parroyo@emeltec.cl', 2, N'Patricio Arroyo', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (23, N'Jmartinich', N'91dcda2a9acabff7808da1578adbdedf', NULL, NULL, N'jmartinich@emeltec.cl', 2, N'Joel Martinich', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (24, N'Lvillegas2', N'2ee329747c88652b42cb75dd0e21e09f', NULL, NULL, NULL, 3, N'Luis Villegas', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (25, N'Dcarrasco2', N'e129b28dbfff44711cf3880a686110f1', NULL, NULL, NULL, 3, N'Diego Carrasco', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (26, N'Macevedo2', N'21f8499640e75825ef468ff73b5cd5b3', N'', N'', NULL, 3, N'Marcelo Acevedo', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (27, N'Lmanchileo2', N'663fc65823e205107dc8997105201355', NULL, NULL, NULL, 3, N'Lorenzo Manchileo', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (28, N'Rsalas', N'fe282a080a859769a53f54dab8cbf944', NULL, NULL, N'rsalas@emeltec.cl', 2, N'Rodrigo Salas', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (29, N'Rsalas2', N'fe282a080a859769a53f54dab8cbf944', NULL, NULL, NULL, 3, N'Rodrigo Salas', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (30, N'Asegura2', N'a911b33a0263d0eca7702abd573a26c5', NULL, NULL, N'asegura@emeltec.cl', 2, N'Alexa Segura', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (31, N'Vendedor', N'883f58c6af511820181797577f27e3b0', NULL, NULL, N'aviso@emeltec.cl', 2, N'Vendedor Generico', NULL, 1)
INSERT [dbo].[DS_Usuarios_20200811_1749] ([ID], [Usuario], [Contrasena], [Cliente], [CCosto], [email], [tipoUsuario], [Nombre], [ContrasenaCorreo], [Estado]) VALUES (32, N'Nprovoste', N'2724a4fa95c6a011c3ace45825663a19', NULL, NULL, N'nprovoste@emeltec.cl', 2, N'Natalia Provoste', NULL, 1)
SET IDENTITY_INSERT [dbo].[DS_Usuarios_20200811_1749] OFF
SET IDENTITY_INSERT [dbo].[DS_UsuariosTipos] ON 

INSERT [dbo].[DS_UsuariosTipos] ([ID], [tipoUsuario], [urlInicio]) VALUES (1, N'Administrador', N'Parametros')
INSERT [dbo].[DS_UsuariosTipos] ([ID], [tipoUsuario], [urlInicio]) VALUES (2, N'Vendedor', N'Todolosclientes')
INSERT [dbo].[DS_UsuariosTipos] ([ID], [tipoUsuario], [urlInicio]) VALUES (3, N'Aprobador', N'Reporte')
INSERT [dbo].[DS_UsuariosTipos] ([ID], [tipoUsuario], [urlInicio]) VALUES (4, N'Comisiones', N'Comisiones')
SET IDENTITY_INSERT [dbo].[DS_UsuariosTipos] OFF
SET IDENTITY_INSERT [dbo].[Menu] ON 

INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (1, N'fa fa-home', N'Administracion', N'Usuario', N'Usuarios', N'Administracion', 1, 1, 1)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (7, N'fa fa-home', N'Realizar Venta', N'Ventas', N'MisClientes', N'Venta', 1, 1, 1)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (9, N'fa fa-home', N'Reporte', N'Notas de Venta Aprobadas', N'FacturasAprobadas', N'Reporte', 1, 1, 3)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (11, N'fa fa-home', N'Reporte', N'Notas de Venta Pendientes', N'FacturasPendientes', N'Reporte', 1, 1, 4)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (13, N'fa fa-home', N'Reporte', N'Notas de Venta Rechazadas', N'FacturasRechazadas', N'Reporte', 1, 1, 6)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (16, N'fa fa-home', N'Reporte', N'Reporte Stock', N'ReporteStock', N'ReporteStock', 1, 1, 9)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (20, N'fa fa-home', N'Reporte', N'DashBoard', N'InformacionVentas', N'Reporte', 1, 1, 10)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (22, N'fa fa-home', N'Realizar Cotizacion', N'Cotizacion', N'MisClientes', N'Cotizacion', 1, 1, 2)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (23, N'fa fa-home', N'Reporte', N'Cotizaciones Pendientes', N'CotizacionesPendientes', N'Reporte', 1, 1, 12)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (1022, N'fa fa-home', N'Mantenedor', N'Consideraciones', N'Consideraciones', N'Mantenedor', 1, 1, 15)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (1023, N'fa fa-home', N'Mantenedor', N'Cargar Archivo', N'CargarArchivo', N'Mantenedor', 1, 1, 16)
INSERT [dbo].[Menu] ([Id_Menu], [Clase], [PieMenu], [Titulo], [Action], [Controller], [TipoUsuario], [Activo], [Orden]) VALUES (2021, N'fa fa-home', N'Mantenedor', N'Valor Moneda', N'ValorMoneda', N'Mantenedor', 1, 1, 14)
SET IDENTITY_INSERT [dbo].[Menu] OFF
SET IDENTITY_INSERT [dbo].[nventa_manag] ON 

INSERT [dbo].[nventa_manag] ([id], [idNotaVenta], [idCorreoManag]) VALUES (3316, 947, 2)
SET IDENTITY_INSERT [dbo].[nventa_manag] OFF
ALTER TABLE [dbo].[DS_Consideracion] ADD  CONSTRAINT [DF_DS_Consideracion_Consideracion]  DEFAULT ('') FOR [Consideracion]
GO
ALTER TABLE [dbo].[DS_Consideracion] ADD  CONSTRAINT [DF_DS_Consideracion_Titulo]  DEFAULT ('') FOR [Titulo]
GO
ALTER TABLE [dbo].[DS_Cot_Folios] ADD  CONSTRAINT [DF_DS_Cot_Folios_Vendedor]  DEFAULT ('') FOR [Vendedor]
GO
ALTER TABLE [dbo].[DS_Cot_Folios] ADD  CONSTRAINT [DF_DS_Cot_Folios_NroCotizacion]  DEFAULT ((0)) FOR [NroCotizacion]
GO
ALTER TABLE [dbo].[DS_Cot_Folios] ADD  CONSTRAINT [DF_DS_Cot_Folios_Iniciales]  DEFAULT ('') FOR [Iniciales]
GO
ALTER TABLE [dbo].[DS_Cot_Folios] ADD  CONSTRAINT [DF_DS_Cot_Folios_NroFinalCotizacion]  DEFAULT ('') FOR [NroFinalCotizacion]
GO
ALTER TABLE [dbo].[DS_Cot_Folios] ADD  CONSTRAINT [DF_DS_Cot_Folios_Reiniciado]  DEFAULT ((0)) FOR [Reiniciado]
GO
ALTER TABLE [dbo].[DS_cwtauxd] ADD  DEFAULT ((0)) FOR [CodPostalDch]
GO
ALTER TABLE [dbo].[DS_cwtauxd] ADD  DEFAULT ((0)) FOR [CodEspWalMart]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_ActAux]  DEFAULT ('S') FOR [ActAux]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_ClaCli]  DEFAULT ('N') FOR [ClaCli]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_ClaPro]  DEFAULT ('N') FOR [ClaPro]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_ClaEmp]  DEFAULT ('N') FOR [ClaEmp]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_ClaSoc]  DEFAULT ('N') FOR [ClaSoc]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_ClaDis]  DEFAULT ('N') FOR [ClaDis]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_ClaOtr]  DEFAULT ('N') FOR [ClaOtr]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_Bloqueado]  DEFAULT ('N') FOR [Bloqueado]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_Region]  DEFAULT (NULL) FOR [Region]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_TipoSaludo]  DEFAULT ((0)) FOR [TipoSaludo]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_CodPostal]  DEFAULT ((0)) FOR [CodPostal]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_CodAreaFon]  DEFAULT ((0)) FOR [CodAreaFon]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_AnexoFon]  DEFAULT ((0)) FOR [AnexoFon]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  CONSTRAINT [DCWTAuxi_CodAreaFax]  DEFAULT ((0)) FOR [CodAreaFax]
GO
ALTER TABLE [dbo].[DS_cwtauxi] ADD  DEFAULT ((0)) FOR [TipoUsuario]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  CONSTRAINT [DF__DS_NotaVe__Cubic__17036CC0]  DEFAULT ('') FOR [Cubicaje]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  CONSTRAINT [DF__DS_NotaVen__Peso__17F790F9]  DEFAULT ('') FOR [Peso]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  CONSTRAINT [DF__DS_NotaVe__Tiene__3A4CA8FD]  DEFAULT ((0)) FOR [TieneDescuento]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  DEFAULT ((0)) FOR [EsCotizacion]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  DEFAULT ((0)) FOR [nroSolicitud]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  DEFAULT ('') FOR [NroFinalCotizacion]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  DEFAULT ((0)) FOR [IdEmpresaInterna]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  DEFAULT ((0)) FOR [IdUsuario]
GO
ALTER TABLE [dbo].[DS_NotaVentaExtras] ADD  DEFAULT ('') FOR [Estado]
GO
ALTER TABLE [dbo].[DS_Parametros] ADD  DEFAULT ((0)) FOR [CambioVendedorCliente]
GO
ALTER TABLE [dbo].[DS_Parametros] ADD  DEFAULT ((0)) FOR [ManejaFiltroGrupo]
GO
ALTER TABLE [dbo].[DS_Parametros] ADD  DEFAULT ((0)) FOR [ManejaCubicaje]
GO
ALTER TABLE [dbo].[DS_ProductosNuevos] ADD  DEFAULT ((0)) FOR [AgregadoASoftland]
GO
ALTER TABLE [dbo].[DS_Usuarios] ADD  CONSTRAINT [DF_DS_Usuarios_Telefono]  DEFAULT ('') FOR [Telefono]
GO
ALTER TABLE [dbo].[DS_Usuarios] ADD  CONSTRAINT [DF_DS_Usuarios_Direccion]  DEFAULT ('') FOR [Direccion]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_ActAux] CHECK  (([ActAux]='N' OR [ActAux]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_ActAux]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_Bloqueado] CHECK  (([Bloqueado]='N' OR [Bloqueado]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_Bloqueado]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_ClaCli] CHECK  (([ClaCli]='N' OR [ClaCli]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_ClaCli]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_ClaDis] CHECK  (([ClaDis]='N' OR [ClaDis]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_ClaDis]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_ClaEmp] CHECK  (([ClaEmp]='N' OR [ClaEmp]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_ClaEmp]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_ClaOtr] CHECK  (([ClaOtr]='N' OR [ClaOtr]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_ClaOtr]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_ClaPro] CHECK  (([ClaPro]='N' OR [ClaPro]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_ClaPro]
GO
ALTER TABLE [dbo].[DS_cwtauxi]  WITH CHECK ADD  CONSTRAINT [VCWTAuxi_ClaSoc] CHECK  (([ClaSoc]='N' OR [ClaSoc]='S'))
GO
ALTER TABLE [dbo].[DS_cwtauxi] CHECK CONSTRAINT [VCWTAuxi_ClaSoc]
GO
/****** Object:  StoredProcedure [dbo].[DS_AddCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_AddCliente]
@CodAux varchar (15),
@NomAux varchar(60),
@RutAux varchar (20),
@FonAux1 varchar (15),
@Email varchar (250),
@GirAux varchar (3),
@DirAux varchar (250),
@pv_BaseDatos varchar (100),
@EmailDte varchar (150),
@VenCod varchar (25),
@ComAux varchar(7),
@CiuAux varchar(7)
AS
	DECLARE @query varchar (max)
	SELECT @query = ''
	SELECT @query = '

	DECLARE @existe int 
	
	SET @existe = (SELECT count(*) FROM ['+@pv_BaseDatos+'].softland.cwtauxi where CodAux = '''+@CodAux+''')
	if(@existe = 0)
	BEGIN
		INSERT INTO ['+@pv_BaseDatos+'].softland.cwtauxi (Codaux,NomAux,NoFAux,DirAux,RutAux,ComAux,CiuAux,ActAux,GirAux,FonAux1,ClaCli,ClaPro,ClaEmp,ClaSoc,ClaDis,ClaOtr,Bloqueado,Email,eMailDTE) values
		('''+@CodAux+''','''+@NomAux+''','''+@NomAux+''','''+@DirAux+''','''+@RutAux+''','''+@ComAux+''','''+@CiuAux+''',''S'','''+@GirAux+''','''+@FonAux1+''',''S'',''N'',''N'',''N'',''N'',''N'',''N'','''+@Email+''','''+@EmailDte+''');
	
		INSERT INTO ['+@pv_BaseDatos+'].softland.cwtauxven (CodAux,VenCod,Usuario) values
		('''+@CodAux+''','''+@VenCod+''',''softland'')

		INSERT INTO ['+@pv_BaseDatos+'].softland.cwtcvcl (CodAux,CodLista) values
		('''+@CodAux+''',''LVG'')

		SELECT Verificador = cast(1 AS bit),
		Mensaje = ''Cliente Creado''
	END
	else
	BEGIN
		SELECT Verificador = cast(0 AS bit),
		Mensaje = ''Cliente ya Existe''
	END
	'
	EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_AddCwauxi]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_AddCwauxi]
@CodAux varchar (15),
@NomAux varchar(60),
@RutAux varchar (20),
@FonAux1 varchar (15),
@Email varchar (250),
@GirAux varchar (3),
@DirAux varchar (250),
@pv_BaseDatos varchar (100),
@EmailDte varchar (150),
@VenCod varchar (25),
@ComAux varchar(7),
@CiuAux varchar(7)
AS
	
	DECLARE @existe int 
	
	SET @existe = (SELECT count(*) FROM DS_cwtauxi where CodAux = @CodAux)
	if(@existe = 0)
	BEGIN
		INSERT INTO DS_cwtauxi (Codaux,NomAux,NoFAux,DirAux,RutAux,ComAux,CiuAux,ActAux,GirAux,FonAux1,ClaCli,ClaPro,ClaEmp,ClaSoc,ClaDis,ClaOtr,Bloqueado,Email,eMailDTE,sincronizado) values
		(@CodAux,@NomAux,@NomAux,@DirAux,@RutAux,@ComAux,@CiuAux,'S',@GirAux,@FonAux1,'S','N','N','N','N','N','N',@Email,@EmailDte,0);
	
		--INSERT INTO ['+@pv_BaseDatos+'].softland.cwtauxven (CodAux,VenCod,Usuario) values
		--('''+@CodAux+''','''+@VenCod+''',''softland'')

		--INSERT INTO ['+@pv_BaseDatos+'].softland.cwtcvcl (CodAux,CodLista) values
		--('''+@CodAux+''',''LVG'')

		SELECT Verificador = cast(1 AS bit),
		Mensaje = 'Cliente Creado'
	END
	else
	BEGIN
		SELECT Verificador = cast(0 AS bit),
		Mensaje = 'Cliente ya Existe'
	END
	

GO
/****** Object:  StoredProcedure [dbo].[DS_AgregarCorreoCli]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_AgregarCorreoCli]
@CodAux varchar (50),
@Email varchar (150),
@pv_BaseDatos varchar (100)
AS
DECLARE @query varchar(max)
SELECT @query = ''
SELECT @query = @query + '
UPDATE '+@pv_BaseDatos+'.softland.cwtauxi
SET
   cwtauxi.EMail = '''+@Email+'''
   where cwtauxi.CodAux = '''+@CodAux	+'''

   SELECT 1
'
exec (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_AgregarDireccionDespacho]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_AgregarDireccionDespacho]							*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DS_AgregarDireccionDespacho]
(
	@pv_CodAux varchar(500)
,	@pv_DirDch varchar(500)
,	@pv_ComDch varchar(500)
,	@pv_NomDch varchar(500)
,	@pv_CiuDch varchar(500)
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
	declare @query varchar(max)

	select @query = ''
	declare @sincronizado int
	select @sincronizado=sincronizado  from DS_cwtauxi where CodAux=@pv_CodAux
	if @sincronizado is null
	  set @sincronizado=1

	if @sincronizado=0
	begin
	select @query = @query + '
	DECLARE @nomdch varchar(100)
	DECLARE @paso BIT
	DECLARE @contador INT

	select @paso = 0
	select @contador = 1
	
	WHILE @paso = 0 begin
		SELECT	@nomdch = ''SUC '' + CONVERT(VARCHAR(MAX), @contador)
		IF NOT EXISTS (SELECT top 1 1 FROM ds_cwtauxd WHERE CodAxD = ''' + @pv_CodAux + ''' AND NomDch = @nomdch) BEGIN
			select	@paso = 1
		END
		select	@contador = @contador + 1
	end
		INSERT INTO ds_cwtauxd
		(
			CodAxD
		,	DirDch
		,	ComDch
		,	NomDch
		,	CiuDch
		)
		VALUES
		(
			''' + @pv_CodAux + '''
		,	''' + @pv_DirDch + '''
		,	''' + @pv_ComDch + '''
		,	@nomdch
		,	''' + @pv_CiuDch + '''
		)

		SELECT	Verificador = Cast(1 as bit)
		,		Mensaje = ''Se agrega direccion de despacho satisfactoriamente''
	
	'
	end

	if @sincronizado>0
	begin
	select @query = @query + '
	DECLARE @nomdch varchar(100)
	DECLARE @paso BIT
	DECLARE @contador INT

	select @paso = 0
	select @contador = 1


	WHILE @paso = 0 begin
		SELECT	@nomdch = ''SUC '' + CONVERT(VARCHAR(MAX), @contador)
		IF NOT EXISTS (SELECT top 1 1 FROM [' + @pv_BaseDatos + '].[softland].[cwtauxd] WHERE CodAxD = ''' + @pv_CodAux + ''' AND NomDch = @nomdch) BEGIN
			select	@paso = 1
		END

		select	@contador = @contador + 1
	end

	--IF NOT EXISTS (SELECT TOP 1 1 FROM [' + @pv_BaseDatos + '].[softland].[cwtauxd] WHERE CodAxD = ''' + @pv_CodAux + ''' AND NomDch = ''' + @pv_NomDch + ''') BEGIN
		INSERT INTO [' + @pv_BaseDatos + '].[softland].[cwtauxd]
		(
			CodAxD
		,	DirDch
		,	ComDch
		,	NomDch
		,	CiuDch
		)
		VALUES
		(
			''' + @pv_CodAux + '''
		,	''' + @pv_DirDch + '''
		,	''' + @pv_ComDch + '''
		,	@nomdch
		,	''' + @pv_CiuDch + '''
		)

		SELECT	Verificador = Cast(1 as bit)
		,		Mensaje = ''Se agrega direccion de despacho satisfactoriamente''
	
	'
	end
	
	EXEC (@query)
END  
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelNotasDeVenta]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelNotasDeVenta]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelNotasDeVenta]
@pv_BaseDatos AS varchar(100),
@pv_FechaDesde varchar(20),
@pv_FechaHasta varchar(20),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT nventa.CodAux as CodigoCliente
	   , NombreCliente = (select NomAux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = nventa.CodAux)
	   , detnv.NVNumero as NotaDeVenta
	   , nventa.NumOC as NumeroOc
	   , nventa.VenCod as CodigoVendedor
	   , NombreVendedor = (select VenDes from [' + @pv_BaseDatos + '].softland.cwtvend where VenCod = nventa.VenCod)
	   , NombreProducto = (select DesProd from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , CodigoProducto = (select CodProd from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , CodigoFabrica = (select DesProd2 from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , CodMarca = (select CodGrupo from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , DescripcionMarca = (select DesGrupo from [' + @pv_BaseDatos + '].[softland].iw_tgrupo where CodGrupo = (select CodGrupo from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd))
	   , detnv.nvCant as Cantidad
	   --,CASE 
				--	 WHEN (nventa.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nventa.nvnumero and vis.CodProd = detnv.CodProd)
				--		ELSE  detnv.nvcant
				--END as Cantidad,
	   --,CASE 
				--	 WHEN (nventa.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nventa.nvnumero and vis.CodProd = detnv.CodProd)
				--		ELSE  detnv.nvcant
				--END)*nvprecio) as ''nvTotLinea''
	   , (detnv.nvTotLinea) as ValorNv 
	   , convert(varchar,nventa.nvFem,105) AS FechaEmision
	   , convert(varchar,detnv.nvFecCompr,105) as FechaDespacho
	   , convert(varchar,nventa.nvFeEnt,105) as FechaEntrega
from [' + @pv_BaseDatos + '].softland.nw_detnv detnv
left join [' + @pv_BaseDatos + '].softland.nw_nventa nventa on nventa.NVNumero = detnv.NVNumero
where nventa.nvEstado in (''a'',''c'',''p'')
and nventa.nvFem BETWEEN convert(datetime,''' + @pv_FechaDesde + ''',120) AND convert(datetime,''' + @pv_FechaHasta + ''',120)
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
		SELECT nventa.CodAux as CodigoCliente
	   , NombreCliente = (select NomAux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = nventa.CodAux)
	   , detnv.NVNumero as NotaDeVenta
	   , nventa.NumOC as NumeroOc
	   , nventa.VenCod as CodigoVendedor
	   , NombreVendedor = (select VenDes from [' + @pv_BaseDatos + '].softland.cwtvend where VenCod = nventa.VenCod)
	   , NombreProducto = (select DesProd from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , CodigoProducto = (select CodProd from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , CodigoFabrica = (select DesProd2 from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , CodMarca = (select CodGrupo from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd)
	   , Marca = (select DesGrupo from [' + @pv_BaseDatos + '].[softland].iw_tgrupo where CodGrupo = (select CodGrupo from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = detnv.CodProd))
	   , detnv.nvCant as Cantidad
	   , (detnv.nvCant * detnv.nvPrecio) as ValorNv 
	   , convert(varchar,nventa.nvFem,105) AS FechaEmision
	   , convert(varchar,detnv.nvFecCompr,105) as FechaDespacho
	   , convert(varchar,nventa.nvFeEnt,105) as FechaEntrega
from [' + @pv_BaseDatos + '].softland.nw_detnv detnv
left join [' + @pv_BaseDatos + '].softland.nw_nventa nventa on nventa.NVNumero = detnv.NVNumero
where nventa.nvEstado in (''a'',''c'',''p'') 
and nventa.nvFem BETWEEN convert(datetime,''' + @pv_FechaDesde + ''',120) AND convert(datetime,''' + @pv_FechaHasta + ''',120)
and nventa.vencod = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelNotasDeVentaBacklog]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_DASH_ExcelNotasDeVentaBacklog]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelNotasDeVentaBacklog]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)
SELECT @query = ''

SELECT @query = @query + '
SELECT 
				NombreCliente = (select nomaux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = nv.CodAux)
				,nv.codAux as CodigoCliente
				,nv.nvNumero as NotaDeVenta
				,nv.NumOC
				,nv.VenCod as CodigoVendedor
				,ven.VenDes as NombreVendedor
				,det.CodProd
				,CodigoFabrica = (select desprod2 from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd)
				,Producto = (select desprod from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd)
				,CodMarca = (select CodGrupo from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd)
				,Marca = (select DesGrupo from [' + @pv_BaseDatos + '].[softland].iw_tgrupo where CodGrupo = (select CodGrupo from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd))
				,Cantidad = ((nvprod.nvcant - nvprod.NVCantFact) 
					+ ( case when nvprod.nvCantNC < 0 then nvprod.nvCantNC * -1 else nvprod.nvCantNC end))
				,sum((det.nvPrecio) * case when det.NVNumero = 5086 and det.nvLinea = 2 then 0 else ((nvprod.nvcant - nvprod.NVCantFact) 
					+ ( case when nvprod.nvCantNC < 0 then nvprod.nvCantNC * -1 else nvprod.nvCantNC end))end) as ValorNV
				,convert(varchar,nv.nvFem,105) AS FechaEmision
				,convert(varchar,nvFecCompr,105) as FechaDespacho
				,convert(varchar,nvFeEnt,105) as FechaEntrega
				,Stock=isnull(sum(t.StockCantidad),0) 
				,Pendiente=isnull((select sum(saldo)as saldo from [' + @pv_BaseDatos + '].softland.ow_vsnpSaldoDetalleOC where codprod=det.CodProd and estado=''AP''),0) 
				,Despachado = isnull((select NVCantDesp from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd where CodProd = det.CodProd and NVNumero = nv.NVNumero),0)
				FROM ['+@pv_BaseDatos +'].softland.nw_nventa nv
					left join ['+@pv_BaseDatos +'].softland.nw_detnv det 
						ON nv.NVNumero= det.NVNumero
					left join ['+@pv_BaseDatos +'].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and nv.nvnumero = nvprod.nvnumero		
					left join  StockPorTipoBodega t
					    On  nvprod.CodProd = t.CodProd		
					left join ['+@pv_BaseDatos +'].softland.cwtvend ven
						on ven.VenCod = nv.VenCod 		 
					WHERE 
						nvEstado in ( ''A'', ''P'') '
					if(@pv_VenCod = '-3') begin
					select @query = @query + 'AND nvFem BETWEEN convert(datetime,''2000-01-01'',120) AND convert(datetime,getdate(),120)'
					end 
					else begin
					select @query = @query + 'AND nvFem BETWEEN convert(datetime,''2000-01-01'',120) AND convert(datetime,getdate(),120) 						
						AND nv.VenCod = ''' + @pv_VenCod + ''''
					end
					select @query = @query + 'group by nv.CodAux,nv.NVNumero,nv.NumOC,nv.VenCod, ven.VenDes,det.CodProd,nv.nvFem,det.nvFecCompr,nv.nvFeEnt 
				,t.StockCantidad,NVProd.nvcant,NVProd.NVCantFact,NVProd.nvCantNC
				order by CodAux asc'
--print (@query)
EXEC (@query)
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelVentas]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelVentas]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelVentas]
@pv_BaseDatos AS varchar(100),
@pv_FechaDesde varchar(20),
@pv_FechaHasta varchar(20),
@pv_VenCod varchar(5)
AS
DECLARE	@query nvarchar(max)

if(@pv_VenCod = '-3') begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		NombreCliente = (select NomAux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = gsaen.CodAux)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		prod.desprod2 as CodigoFabrica
				,		PROD.CodGrupo as CodMarca
				,		grupo.DesGrupo as DescripcionMarca
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
'
EXEC ( @query)
end
else begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		NombreCliente = (select NomAux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = gsaen.CodAux)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		prod.desprod2 as CodigoFabrica
				,		PROD.CodGrupo as CodMarca
				,		grupo.DesGrupo as DescripcionMarca
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC ( @query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelVentasDia]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelVentasDia]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelVentasDia]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	select distinct 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		NombreCliente = (select NomAux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = gsaen.CodAux)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes 
				,		gmovi.CodProd
				,		prod.desprod2 as CodigoFabrica
				,		prod.CodGrupo as CodMarca
				,		grupo.DesGrupo as DescripcionMarca
				,		prod.desprod 
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) AND convert(datetime,getdate(),120)	
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	select distinct 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		NombreCliente = (select NomAux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = gsaen.CodAux)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes 
				,		gmovi.CodProd
				,		prod.desprod2 as CodigoFabrica
				,		prod.CodGrupo as CodMarca
				,		grupo.DesGrupo as DescripcionMarca
				,		prod.desprod 
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) AND convert(datetime,getdate(),120)	
				AND		GSAEN.Estado = ''V''
				AND		GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_TopProductosActual]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_TopProductosActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_GET_DASH_TopProductosActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar (5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProducto,
				PROD.DesProd as nombreProducto,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProducto
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProducto DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProducto,
				PROD.DesProd as nombreProducto,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProducto
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProducto DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_TopProductosFiltro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_TopProductosFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_GET_DASH_TopProductosFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProductoFiltro,
				PROD.DesProd as nombreProductoFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProductoFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProductoFiltro DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProductoFiltro,
				PROD.DesProd as nombreProductoFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProductoFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProductoFiltro DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_GET_ObtenerDatosCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_GET_ObtenerDatosCliente]
@CodAux varchar (50),
@pv_BaseDatos varchar (100)
as
declare @query varchar (max)

select @query = ''

select @query = @query + '
	select 
	EMail = clientes.EMail,
	CodAux = clientes.CodAux,
	NomAux = clientes.[NomAux],
	RutAux = clientes.[RutAux],
	DirAux = clientes.[DirAux] ,
	DirNum = clientes.[DirNum], 
	NomCon = contacto.[NomCon] , 
	FonCon = contacto.[FonCon]
	from ['+@pv_BaseDatos+'].[softland].[cwtauxi] clientes
	inner join ['+@pv_BaseDatos+'].[softland].[cwtaxco] contacto on clientes.CodAux = contacto.CodAuc 
	WHERE clientes.CodAux = '''+@CodAux+'''
'
exec (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_GET_ObtenerDatosUsuario]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DS_GET_ObtenerDatosUsuario]
@IdUsuario int
AS
	SELECT 
	id = u.ID,
	Usuario = u.Usuario,
	Nombre = u.Nombre,
	email = u.email,
	tipoId = u.tipoUsuario,
	tipoUsuario = dut.tipoUsuario
	FROM DS_Usuarios u 
	LEFT JOIN dbo.DS_UsuariosTipos dut ON u.tipoUsuario = dut.id
	LEFT JOIN dbo.DS_UsuarioEmpresa due ON due.IdUsuario = u.ID
	WHERE u.ID = @IdUsuario	

GO
/****** Object:  StoredProcedure [dbo].[DS_GetAprobador]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_GetAprobador]
@IdAprobador int
as
SELECT 
Email = email, 
Contrasena = ContrasenaCorreo 
FROM dbo.DS_Usuarios du
WHERE ID = @IdAprobador

GO
/****** Object:  StoredProcedure [dbo].[DS_GetAprobadorNP]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_GetAprobadorNP]
(
	@pi_IdEmpresa INT
)
AS
BEGIN
	SELECT	* 
	FROM	dbo.DS_Usuarios du	
		inner join dbo.DS_UsuarioEmpresa due
			on du.ID = due.IdUsuario	
	WHERE	du.tipoUsuario = 3
	and		due.IdEmpresa = @pi_IdEmpresa
END

GO
/****** Object:  StoredProcedure [dbo].[DS_GetCab]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_GetCab]
@nvId int
AS
SELECT CodAux, VenCod, NVNumero, CodiCC, CodLugarDesp, NumOC, CotNum FROM dbo.DS_NotasVenta dnv WHERE dnv.Id = @nvId



GO
/****** Object:  StoredProcedure [dbo].[DS_ListaEmpresa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_ListaEmpresa]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DS_ListaEmpresa]
AS  
BEGIN
	SELECT	IdEmpresa = a.Id
	,		NombreEmpresa = a.Nombre
	,		BaseDatos = a.BaseDatos
	FROM	DS_Empresa a
END  

GO
/****** Object:  StoredProcedure [dbo].[DS_listarBodegas]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_listarBodegas]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_listarBodegas]
(
	@pv_BaseDatos varchar(100)
)
as
begin
	declare @query varchar(max)
	set @query = ''
	-- ==========================================================================================  
	-- Lista las bodegas solicitadas
	-- ========================================================================================== 
	select @query = @query + '
		select CodBode, DesBode 
		from ['+@pv_BaseDatos+'].[softland].[iw_tbode] 
		where CodBode IN (''60'',''35'',''27'')
	'
	EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[DS_ListarPefiles]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_ListarPefiles]
AS
SELECT dut.ID, dut.tipoUsuario AS TipoUsuario FROM dbo.DS_UsuariosTipos dut

GO
/****** Object:  StoredProcedure [dbo].[DS_ListarVendedoresSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DS_ListarVendedoresSoftland]
@pv_BaseDatos varchar(100)
as
declare @query nvarchar(max)

select @query = ''

if(@pv_BaseDatos = 'SIN_BD') begin
set @pv_BaseDatos = 'EMELTEC'
end

select @query = @query + '
select VenCod, VenDes 
from [' + @pv_BaseDatos + '].softland.cwtvend
'

exec(@query)
GO
/****** Object:  StoredProcedure [dbo].[DS_ListaUsuarioEmpresa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_ListaUsuarioEmpresa]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[DS_ListaUsuarioEmpresa]
(
	@pi_IdUsuario INT
)
AS  
BEGIN
	SELECT	IdUsuario = a.IdUsuario
	,		IdEmpresa = a.IdEmpresa
	,		NombreEmpresa = b.Nombre
	,		BaseDatos = b.BaseDatos
	,		VenCod = a.VenCod	
	,		tipoUsuario = du.tipoUsuario
	FROM	DS_UsuarioEmpresa a
		INNER JOIN DS_Empresa b
			on a.IdEmpresa = b.Id
			JOIN dbo.DS_Usuarios du ON a.IdUsuario = du.Id
	WHERE	a.IdUsuario = @pi_IdUsuario
END  

GO
/****** Object:  StoredProcedure [dbo].[DS_ObtenerSaldo]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_ObtenerSaldo]
@RutAux varchar (20),
@CodAux varchar (50),
@pv_BaseDatos varchar (100)
AS
BEGIN
DECLARE @query varchar (max)
SELECT @query = ''
SELECT @query = '

SELECT pccodi = isnull(mov.pccodi, ''''), pcdesc = isnull(mov.pcdesc, ''''), codaux = isnull(mov.codaux, ''''), 
RutAux = isnull(mov.RutAux, ''''), 
nomaux = isnull(mov.nomaux, ''''), mov.fechaemi, fechaven = isnull(mov.fechaven, ''''), desdoc = isnull(desdoc, ''''), 
movnumdocref = isnull(mov.movnumdocref, 0), Saldo = isnull(mov.Saldo, 0),    DesArn = isnull(mov.DesArn, ''''),    AreaCod = isnull(mov.AreaCod, ''''),    
PCAUXI = isnull(mov.PCAUXI, ''''), PCCDOC = isnull(mov.PCCDOC, ''''), coddoc = isnull(mov.coddoc, ''''), VendCod = isnull(mov.VendCod, ''''), 
Vendedor = isnull(mov.Vendedor, ''''), FecEmi = isnull(mov.FecEmi, ''''), Debe = isnull(mov.Debe, 0), Haber = isnull(mov.Haber, 0), 
movtipdocref = isnull(mov.movtipdocref, ''''), mov.MovFv as movfv, mov.Cpbano
FROM
(select cwpctas.pccodi, cwpctas.pcdesc, cwtauxi.codaux, cwtauxi.RutAux, cwtauxi.nomaux, min(cwmovim.movfe) as fechaemi, 
''                                                    '' as fechaven, cwttdoc.desdoc, cwmovim.movnumdocref, cwmovim.movtipdocref,min(cwmovim.MovFv) as MovFv,
sum(cwmovim.movdebe - cwmovim.movhaber) as Saldo, min(cwmovim.MovDebe) as Debe, min(cwmovim.MovHaber) as Haber, cwmovim.AreaCod, cwTAren.DesArn , cwpctas.PCAUXI, cwpctas.PCCDOC,  
cwttdoc.coddoc, 
max(cwmovim.Cpbano) as Cpbano,  ''    '' as VendCod,''                                                                                           '' as Vendedor,
''                                                    '' as FecEmi  
FROM [' + @pv_BaseDatos + '].softland.cwcpbte 
inner join [' + @pv_BaseDatos + '].softland.cwmovim on cwcpbte.cpbano = cwmovim.cpbano and cwcpbte.cpbnum = cwmovim.cpbnum 
inner join [' + @pv_BaseDatos + '].softland.cwtauxi on cwtauxi.codaux = cwmovim.codaux 
inner join [' + @pv_BaseDatos + '].softland.cwpctas on cwmovim.pctcod = cwpctas.pccodi 
left join [' + @pv_BaseDatos + '].softland.cwttdoc on cwmovim.movtipdocref = cwttdoc.coddoc 
left join [' + @pv_BaseDatos + '].softland.cwtaren on cwmovim.AreaCod = cwTAren.CodArn 
WHERE
	(((cwcpbte.cpbNum <> ''00000000'')  
or (cwcpbte.cpbano = (select min(cpbano) from [' + @pv_BaseDatos + '].softland.cwcpbte) AND cwcpbte.cpbNum = ''00000000'' ))) 
and (cwcpbte.cpbest = ''V'') 
			and cwmovim.codaux = ''' + @CodAux + '''
and (CWCpbte.CpbFec <= convert(datetime,CONVERT(varchar, CURRENT_TIMESTAMP),102)) 
Group By cwpctas.pccodi , cwpctas.pcdesc, cwtauxi.codaux, cwtauxi.RutAux, cwmovim.movnumdocref, cwtauxi.nomaux, 
cwttdoc.desdoc, cwmovim.AreaCod, cwTAren.DesArn, cwpctas.PCAUXI, cwpctas.PCCDOC,  cwttdoc.coddoc , cwmovim.movtipdocref
Having (Sum((cwmovim.movdebe - cwmovim.movhaber)) <> 0) 
) as mov
order by movnumdocref asc, FecEmi asc
'
EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[Ds_RechazarNP]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Ds_RechazarNP]
@nvId int
AS
UPDATE dbo.DS_NotasVenta
SET
    dbo.DS_NotasVenta.EstadoNP	= 'R',dbo.DS_NotasVenta.nvEstado = 'R', dbo.DS_NotasVenta.nvObser = 'Rechazada' WHERE dbo.DS_NotasVenta.Id = @nvId

GO
/****** Object:  StoredProcedure [dbo].[DS_SET_ActualizaCorreo]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_SET_ActualizaCorreo]
@VendCod varchar(50),
@Email varchar(100),
@Contrasena varchar(50),
@pv_BaseDatos varchar(100)
AS
UPDATE dbo.DS_Usuarios SET    
dbo.DS_Usuarios.email = @Email,
dbo.DS_Usuarios.ContrasenaCorreo = @Contrasena	
WHERE dbo.DS_Usuarios.Id in (select sub_a.idUsuario from ds_usuarioEmpresa sub_a inner join ds_empresa sub_b on sub_a.idempresa = sub_b.id where sub_b.basedatos = @pv_BaseDatos and sub_a.VenCod = @VendCod)
SELECT 1


GO
/****** Object:  StoredProcedure [dbo].[DS_SET_EditarUsuario]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DS_SET_EditarUsuario]
@IdUsuario int,
@Usuario varchar (100),
@Nombre varchar (100),
@Password varchar (max),
@Email varchar (150),
@TipoUsuario int
AS
BEGIN
UPDATE dbo.DS_Usuarios
SET
    dbo.DS_Usuarios.Usuario = @Usuario,
    dbo.DS_Usuarios.Contrasena = @Password,
    dbo.DS_Usuarios.Cliente = '',
    dbo.DS_Usuarios.CCosto = '',
    dbo.DS_Usuarios.email = @Email,
    dbo.DS_Usuarios.tipoUsuario = @TipoUsuario,
    dbo.DS_Usuarios.Nombre = @Nombre,
    dbo.DS_Usuarios.Estado = 1
	WHERE dbo.DS_Usuarios.ID = @IdUsuario

	SELECT Verificador = cast(1 as bit),
	Mensaje = 'Usuario Modificado'
END

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetCantProductos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetCantProductos]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetCantProductos]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductos
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductos
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetCantProductosFiltro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetCantProductosFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetCantProductosFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductosFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductosFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetClientesTotales]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetClientesTotales]								*/
/*-- Detalle			: Clientes y montos totales de ventas														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetClientesTotales]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@nvEstado varchar(1),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
SELECT 
	codaux as CodCliente,nomaux as NomCliente,sum(suma) as Total
		FROM (
				SELECT 
					--CASE 
					--	WHEN nv.nvEstado = ''C''  
					--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
					--	ELSE nvSubTotal 
					--END as suma
					nvsubtotal as ''suma''
					, nv.CodAux as ''codaux'', aux.NomAux as ''nomaux''
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
				LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
					ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS	
				WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta+ ''',120 )
					and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
		) tmp	
	group by codaux, nomaux
	ORDER BY
		Total desc
'	
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
SELECT 
	codaux as CodCliente,nomaux as NomCliente,sum(suma) as Total
		FROM (
				SELECT 
					--CASE 
					--	WHEN nv.nvEstado = ''C''  
					--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
					--	ELSE nvSubTotal 
					--END
					nvSubTotal as ''suma'', nv.CodAux as ''codaux'', aux.NomAux as ''nomaux''
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
				LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
					ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS	
				WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta+ ''',120 )
					and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
					and nv.VenCod = ''' + @pv_VenCod + '''
		) tmp	
	group by codaux, nomaux
	ORDER BY
		Total desc
'	
EXEC (@query)
end




GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetClientesTotalesBlacklog]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetClientesTotales]								*/
/*-- Detalle			: Clientes y montos totales de ventas historico													*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetClientesTotalesBlacklog]
@pv_BaseDatos AS varchar(100),
@nvEstado varchar(1),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT 
				nv.CodAux as CodCliente
				, aux.NomAux as NomCliente
				, (det.nvPrecio) * ((nvprod.nvcant - nvprod.NVCantFact) 
				+ ( case when nvprod.nvCantNC < 0 then nvprod.nvCantNC * -1 else nvprod.nvCantNC end)) as Total
				, det.nvnumero --SUM(NSUBTOTAL)
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det
						on nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
						ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS		
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd
						and det.nvnumero = nvprod.nvnumero
					left join  StockPorTipoBodega t
						On  nvprod.CodProd = t.CodProd	
					WHERE 
						nv.nvEstado in (''A'',''P'')
						order by nv.codaux			
'	
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT 
				nv.CodAux as CodCliente
				, aux.NomAux as NomCliente
				, (det.nvPrecio) * ((nvprod.nvcant - nvprod.NVCantFact) 
				+ ( case when nvprod.nvCantNC < 0 then nvprod.nvCantNC * -1 else nvprod.nvCantNC end)) as Total
				, det.nvnumero --SUM(NSUBTOTAL)
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det
						on nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
						ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS		
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd
						and det.nvnumero = nvprod.nvnumero
					left join  StockPorTipoBodega t
						On  nvprod.CodProd = t.CodProd	
					WHERE 
						nv.nvEstado in (''A'',''P'')
						and nv.VenCod = ''' + @pv_VenCod + '''
'	
--print @query
EXEC (@query)
end


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetDetalleVentasPorCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetDetalleVentasPorCliente]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN						*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetDetalleVentasPorCliente]
@pv_BaseDatos AS varchar (100),
@fechaDesde AS varchar (10),
@fechaHasta varchar (10),
@codaux varchar (100)
AS
declare @query as nvarchar(max)

SELECT @query = ''

SELECT @query = @query + '
	SELECT

				gsaen.Folio,CONVERT(varchar,gsaen.Fecha,105) as Fecha, gsaen.Tipo, sum(gsaen.NetoAfecto) as Total
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 			 					
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,'''+@fechaDesde+''',120) AND convert(datetime,'''+@fechaHasta+''',120)
				AND		GSAEN.Estado = ''V''
				AND     gsaen.codaux = '''+@codaux+'''				
				GROUP BY GSAEN.FOLIO, gsaen.Fecha, GSAEN.TIPO, total
				ORDER BY Total DESC
'
EXEC (@query)


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeCantVendedoresActual]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeCantVendedoresActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeCantVendedoresActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeCantVendedoresActualAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeCantVendedoresActualAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeCantVendedoresActualAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
'
exec(@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
exec(@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeCantVendedoresFiltro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeCantVendedoresFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeCantVendedoresFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActual]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end



GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActualAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosActualAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActualAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
as
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltroAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosFiltroAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltroAnterior]
@pv_BaseDatos AS varchar(100),
@fechaHasta varchar(12),
@fechaDesde varchar(12),
@pv_VenCod varchar(5)
as
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesCompraActual]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesCompraActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeClientesCompraActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreCliente,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotal
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotal DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreCliente,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotal
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotal DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesCompraFiltro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesCompraFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeClientesCompraFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreClienteFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalCliFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotalCliFiltro DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreClienteFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalCliFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotalCliFiltro DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesVentaDia]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesVentaDia]				*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetInformeClientesVentaDia]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDia
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00 '' ,120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDia
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00 '' ,120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesVentaDiaAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesVentaDiaAnterior]		*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetInformeClientesVentaDiaAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaActual]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetInformeVentaActual]
@pv_BaseDatos AS varchar (100),
@pv_CodVendedor as varchar (100)
AS
declare @query as nvarchar(max)

if(@pv_CodVendedor = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalHoy
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalHoy
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_CodVendedor + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaActualAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaActualAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetInformeVentaActualAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalAnterior
				FROM	['+@pv_BaseDatos+'].[softland].IW_GMOVI GMOVI 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN ['+ @pv_BaseDatos +'].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join ['+ @pv_BaseDatos +'].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join ['+ @pv_BaseDatos +'].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join ['+ @pv_BaseDatos +'].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
'
exec (@query)
end
else begin
SELECT @query = ''
SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalAnterior
				FROM	['+@pv_BaseDatos+'].[softland].IW_GMOVI GMOVI 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN ['+ @pv_BaseDatos +'].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join ['+ @pv_BaseDatos +'].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join ['+ @pv_BaseDatos +'].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join ['+ @pv_BaseDatos +'].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
exec (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaFiltro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeVentaFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaFiltroAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaFiltroAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeVentaFiltroAnterior]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentasVendedoresActual]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentasVendedoresActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeVentasVendedoresActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedor,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedor
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedor DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedor,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedor
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedor DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentasVendedoresFiltro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentasVendedoresFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetInformeVentasVendedoresFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3')begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedorFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedorFiltro DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedorFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedorFiltro DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetNotasDeVentaPorCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetNotasDeVentaPorCliente]								*/
/*-- Detalle			: Detalle Notas de Venta por cliente														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetNotasDeVentaPorCliente]
@pv_BaseDatos AS varchar(100),
@codAux AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@nvEstado varchar(12)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
	SELECT 
				nv.nvestado,nv.nvNumero, CONVERT(varchar,nvfem,105) as nvfem, codaux, VenDes
				, nv.nvsubtotal as ''nvmonto''
				--,CASE 
				--	 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select sum(subtotal) from[' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
				--		ELSE  nvSubTotal
				--END as ''nvmonto''
					FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
						LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtvend ven
							ON nv.VenCod= ven.VenCod	
						LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero
						WHERE 
							nv.codaux='''+@codAux+''' 
							AND nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120 )
							AND nvEstado in ('''+@nvEstado+''',''C'',''P'')	
							group by nv.nvNumero, nvfem, codaux, VenDes
							, nvSubTotal
							--, nv.nvmonto
							, nv.nvestado
						ORDER BY
							nvfem desc
'	
EXEC (@query)



GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetNotasDeVentaPorClienteBlacklog]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetClientesTotalesBlacklog]								*/
/*-- Detalle			: Detalle Notas de Venta por cliente														*/
/*-- Autor				: SIABA												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetNotasDeVentaPorClienteBlacklog]
@pv_BaseDatos AS varchar(100),
@codAux AS varchar(100),
@nvEstado varchar(12)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
SELECT 
nv.nvestado
, nv.nvNumero
, CONVERT(varchar,nvfem,105) as nvfem
, nv.codAux
, VenDes
, nv.nvSubTotal as nvmonto
,  (det.nvPrecio) * ((nvprod.nvcant - nvprod.NVCantFact) 
				+ ( case when nvprod.nvCantNC < 0 then nvprod.nvCantNC * -1 else nvprod.nvCantNC end)) as  nvTotLinea
FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtvend ven
ON nv.VenCod= ven.VenCod
left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
ON nv.NVNumero= det.NVNumero
left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
ON det.CodProd = nvprod.CodProd	
and det.nvnumero = nvprod.nvnumero		
left join  StockPorTipoBodega t
On  nvprod.CodProd = t.CodProd			 
WHERE 
nvEstado in (''A'',''P'') and nv.codaux='''+@codAux+''' 						
order by nv.codaux
'	
EXEC (@query)


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetPeriodoActual]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetPeriodoActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetPeriodoActual]
@pv_BaseDatos AS varchar(100)
AS
DECLARE @query AS nvarchar(max)
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				month(GSAEN.Fecha) as mes, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-01-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				group by month(GSAEN.Fecha)
				order by month(GSAEN.Fecha)
'
exec (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetProductos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetProductos]								*/
/*-- Detalle			: Productos por nota de venta														*/
/*-- Autor				: SIABA												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetProductos]
@pv_BaseDatos AS varchar(100),
@NVNumero AS varchar(100)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
	SELECT 
				nv.NVNumero, CONVERT(varchar,nvFecCompr,105) as nvFecCompr, pro.DesProd,
				--CASE 
				--	 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
				--		ELSE  det.nvcant
				--END as ''nvCant'',	
				det.nvcant as nvCant,
				nvPrecio,
				--((CASE 
				--	 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
				--		ELSE  det.nvcant
				--END)*nvprecio) as ''nvTotLinea''	
				det.nvcant * det.nvprecio as nvTotLinea
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
							ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					LEFT JOIN [' + @pv_BaseDatos + '].softland.nw_nventa nv 
							ON nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero
					WHERE 
						nv.NVNumero='''+@NVNumero+'''		
					group by nv.NVNumero, nvFecCompr,pro.DesProd, det.CodProd, nv.nvEstado, det.nvCant, det.nvPrecio, nvTotLinea									
'	
EXEC (@query)



GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetProductosBlacklog]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetProductos]								*/
/*-- Detalle			: Productos por nota de venta														*/
/*-- Autor				: SIABA												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetProductosBlacklog]
@pv_BaseDatos AS varchar(100),
@NVNumero AS varchar(100)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
	SELECT
				det.NVNumero, CONVERT(varchar,nvFecCompr,105) as nvFecCompr, pro.DesProd, det.nvCant, nvPrecio, nvTotLinea 
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
						ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and det.nvnumero = nvprod.nvnumero	
					left join  StockPorTipoBodega t
						On  nvprod.CodProd = t.CodProd		 
					WHERE 
						det.NVNumero = ''' + @NVNumero + '''
					group by det.NVNumero,nvFecCompr, pro.DesProd, det.nvCant, nvPrecio, nvTotLinea, det.CodProd,t.StockCantidad
					having SUM( (nvprod.nvcant - nvprod.NVCantFact) + (nvprod.nvCantNC * -1)) > 0
					order by det.CodProd
'	
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetProductosVenta]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetProductosVenta]								*/
/*-- Detalle			:														*/
/*-- Autor				: FUDRAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetProductosVenta]
@pv_BaseDatos AS varchar (100),
@folio varchar (100),
@tipo varchar (1)
AS
declare @query as nvarchar(max)

SELECT @query = ''

SELECT @query = @query + '
	SELECT
				gsaen.Folio, DetProd, CantFacturada, PreUniMB, TotLinea 
				FROM [' + @pv_BaseDatos + '].softland.iw_gsaen gsaen 
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_gmovi mov
						ON gsaen.NroInt = mov.NroInt
				WHERE gsaen.Folio = '''+@folio+''' 
					  AND mov.tipo='''+@tipo+'''
					  AND GSAEN.tipo!=''s''
					  AND GSAEN.tipo!=''a''
					  AND GSAEN.tipo!=''e''
					  AND GSAEN.Estado = ''V''
				ORDER BY Total DESC
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetTotalNotasDeVenta]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetTotalNotasDeVenta]								*/
/*-- Detalle			: Total Montos Notas de Venta														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetTotalNotasDeVenta]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@nvEstado varchar(1),
@pv_CodVendedor varchar (5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_CodVendedor = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT sum(suma) as Total
	FROM (
		SELECT 
				--CASE 
				--	WHEN nv.nvEstado = ''C''  
				--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
				--	ELSE nvSubTotal 
				--END as suma
				nvsubtotal as ''suma'' 
			FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
			WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120 )
				and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
	) tmp
'
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT sum(suma) as Total
	FROM (
		SELECT 
				--CASE 
				--	WHEN nv.nvEstado = ''C''  
				--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
				--	ELSE nvSubTotal 
				--END
				nvSubTotal as ''suma'' 
			FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
			WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120 )
				and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
				and nv.VenCod = ''' + @pv_CodVendedor + '''
	) tmp
'
EXEC (@query)
end



GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetTotalNotasDeVentaBlacklog]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetTotalNotasDeVenta]								*/
/*-- Detalle			: Total Montos Notas de Venta blacklog														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetTotalNotasDeVentaBlacklog]
@pv_BaseDatos AS varchar(100),
@nvEstado varchar(1),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT
		sum((det.nvPrecio) * ((nvprod.nvcant - nvprod.NVCantFact) 
		+ ( case when nvprod.nvCantNC < 0 then nvprod.nvCantNC * -1 else nvprod.nvCantNC end))) as Total
		--det.nvTotLinea as Total
		,nv.nvnumero
		FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
			left join [' + @pv_BaseDatos + '].softland.nw_detnv det on det.NVNumero = nv.NVNumero
			left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod ON det.CodProd = nvprod.CodProd
																	and det.nvnumero = nvprod.nvnumero
			left join  StockPorTipoBodega t On  nvprod.CodProd = t.CodProd	
		WHERE 
		nvEstado in (''A'',''P'')
		group by nv.nvnumero,t.StockCantidad
'
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT
		sum((det.nvPrecio) * ((nvprod.nvcant - nvprod.NVCantFact) 
		+ ( case when nvprod.nvCantNC < 0 then nvprod.nvCantNC * -1 else nvprod.nvCantNC end))) as Total
		--det.nvTotLinea as Total
		,nv.nvnumero
		FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
			left join [' + @pv_BaseDatos + '].softland.nw_detnv det on det.NVNumero = nv.NVNumero
			left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod ON det.CodProd = nvprod.CodProd
																	and det.nvnumero = nvprod.nvnumero
			left join  StockPorTipoBodega t On  nvprod.CodProd = t.CodProd	
		WHERE 
		nvEstado in (''A''	,''P'')
		and nv.VenCod = ''' + @pv_VenCod + '''
		group by nv.nvnumero,t.StockCantidad
'
EXEC (@query)
end


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetVendedoresSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetVendedoresSoftland]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetVendedoresSoftland]
@pv_BaseDatos varchar(100)
as
begin

declare @query nvarchar(max)

select @query = ''

select @query = @query + '
	SELECT DISTINCT ven.VenCod, VenDes, CodTipV, ven.EMail, ven.Usuario 
     FROM ' + @pv_BaseDatos + '.[softland].[cwtvend] AS ven INNER JOIN
     DS_UsuarioEmpresa AS u ON u.VenCod COLLATE SQL_Latin1_General_CP1_CI_AI = ven.VenCod LEFT OUTER JOIN
     DS_Usuarios ON u.Idusuario = DS_Usuarios.ID
	 where DS_Usuarios.Estado = 1 and tipoUsuario <> 3
	 order by ven.VenCod
'
exec(@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetVentasPorCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetVentasPorCliente]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetVentasPorCliente]
@pv_BaseDatos AS varchar (100),
@fechaDesde AS varchar (10),
@fechaHasta varchar (10),
@pv_VenCod varchar(5)
AS
declare @query as nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				gsaen.codaux as CodCliente, cwt.NomAux as NomCliente, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as Total
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,'''+@fechaDesde+''',120) AND convert(datetime,'''+@fechaHasta+''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY gsaen.codaux, cwt.NomAux
				ORDER BY Total DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				gsaen.codaux as CodCliente, cwt.NomAux as NomCliente, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as Total
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,'''+@fechaDesde+''',120) AND convert(datetime,'''+@fechaHasta+''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY gsaen.codaux, cwt.NomAux
				ORDER BY Total DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[FR_ActualizaClienteVendedorSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ActualizaClienteVendedorSoftland]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_ActualizaClienteVendedorSoftland]
(
	/*--------------------------- CAMPOS DISOFI ---------------------------*/
	@pv_BaseDatos [varchar](100)
,	@pi_IdNotaVenta INT = null
)
AS
BEGIN
	declare @query nvarchar(max)
	
	declare @lv_codaux varchar(100)
	declare @lv_vendedor varchar(100)

	--Validacion tabla clientes softland -- cwtauxi

	declare @queryInsertAuxi nvarchar(max)
	
	select @queryInsertAuxi = '
		if not exists(select CodAux from ' + @pv_BaseDatos + '.softland.cwtauxi where CodAux = ''' + @lv_codaux + ''')
		begin

		insert into ' + @pv_BaseDatos + '.softland.cwtauxi
		select [CodAux],[NomAux],[NoFAux],[RutAux],[ActAux],[GirAux],[ComAux],[CiuAux],[PaiAux],[ProvAux],[DirAux],[DirNum]
			   ,[FonAux1],[FonAux2],[FonAux3],[FaxAux1],[FaxAux2],[ClaCli],[ClaPro],[ClaEmp],[ClaSoc],[ClaDis],[ClaOtr],[DiaPlazo]
			   ,[Bloqueado],[EMail],[Casilla],[WebSite],[Notas],[Region],[TipoSaludo],[DirDpto],[DirOtro],[CodPostal],[CodAreaFon]
			   ,[AnexoFon],[CodAreaFax],[FechaNacim],[Username],[Password],[PalabraSecreta],[PreguntaSecreta],[ClienteDesde],[TipoUsuario]
			   ,[eMailDTE],[esReceptorDTE],[BloqueadoPro],[ClaPros],[CodCamp],[CodOrigen],[Id_RecepExtranjero],[PaisRecepExtranjero]
			   ,[Usuario],[Sistema],[Proceso],[FechaUlMod],[CtaCliente],[CtaCliMonExt],[PasswordResetToken] 
			   from DS_cwtauxi where CodAux = ''' + @lv_codaux + '''

		update DS_cwtauxi set sincronizado = 1 where CodAux = ''' + @lv_codaux + '''
	
		end
	'
	exec(@queryInsertAuxi)
	--


	IF (select top 1 a.CambioVendedorCliente from ds_parametros a 
	inner join ds_empresa b on a.idEmpresa = b.id where b.basedatos = @pv_BaseDatos) = 1 BEGIN

		select	@lv_codaux  = codaux 
		,		@lv_vendedor = vencod
		from	[dbo].[ds_notasventa] 
		where	id = @pi_IdNotaVenta

		SELECT @query = ''

		select @query = @query + 'delete from [' + @pv_BaseDatos + '].[softland].[cwtauxven]
					where vencod='''  + @lv_vendedor + '''
					and   codaux='''  + convert(varchar(100), @lv_codaux) + ''''

		exec (@query)

		SELECT @query = ''

		select @query = @query + 'insert into [' + @pv_BaseDatos + '].[softland].[cwtauxven]
					(vencod, codaux) values ('''  + @lv_vendedor + ''','''  + convert(varchar(100), @lv_codaux) + ''')'

		exec (@query)




		--SELECT @query = @query + '
		----select vencod, ''' + @lv_vendedor + ''', * from [' + @pv_BaseDatos + '].[softland].[cwtauxven] a where codaux = ''' + convert(varchar(100), @lv_codaux) + '''
		--update	[' + @pv_BaseDatos + '].[softland].[cwtauxven]
		--set		vencod = ''' + @lv_vendedor + '''
		--where	codaux = ''' + convert(varchar(100), @lv_codaux) + '''
		--'

		--exec (@query)
	end
END

GO
/****** Object:  StoredProcedure [dbo].[FR_ActualizarCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ActualizarCliente]							*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_ActualizarCliente]  
 @CodAux VARCHAR(10),  
 @RutAux VARCHAR(20),  
 @NomAux VARCHAR(60),   
 @DirAux NCHAR(60),   
 @NomCon VARCHAR(50),  
 @FonCon NCHAR(10),  
 @Email varchar(100),
 @pv_BaseDatos varchar(100)
AS  
DECLARE @query varchar(max)
SELECT @query = ''

SELECT @query = @query + '
BEGIN  	
	 UPDATE ['+@pv_BaseDatos+'].[softland].[cwtauxi]  
	  SET  
	  RutAux = '''+@RutAux+''',
	  NomAux = '''+@NomAux+''',
	  DirAux = '''+@DirAux+''',
	  EMail = '''+@Email+'''
	  WHERE CodAux = '''+@CodAux+'''
  
	 UPDATE ['+@pv_BaseDatos+'].[softland].[cwtaxco]  
	  SET NomCon = '''+@NomCon+''',
	  FonCon = '''+@FonCon+'''

	  WHERE CodAuc = '''+@CodAux+'''

	END  
'
exec (@query)

	  SELECT	Verificador = Cast(1 as bit)
	  ,			Mensaje = 'Se actualiza cliente'

GO
/****** Object:  StoredProcedure [dbo].[FR_ActualizarUsuario]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ActualizarUsuario]							*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_ActualizarUsuario]  
 @id INT,  
 @usuario VARCHAR(10),  
 @email VARCHAR(50),  
 @tipoUsuario VARCHAR(50),  
 @venCod NCHAR(10)  
AS  
BEGIN  
 UPDATE [DSTexsa_NV].[dbo].[DS_Usuarios]  
  SET Usuario=@usuario, email=@email, tipoUsuario=@tipoUsuario, VenCod=@venCod WHERE ID=@id

  SELECT	Verificador = Cast(1 as bit)
  ,			Mensaje = 'Se actualiza usuario'   
END  

GO
/****** Object:  StoredProcedure [dbo].[FR_AgregarContactos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_AgregarContactos]
 @CodAux	varchar(10),
 @NomCon	varchar(30)

as
begin
	insert into [KUPPEL].softland.cwtaxco 
	([CodAuc],[NomCon])
	values 
	(@CodAux, @NomCon)

end


GO
/****** Object:  StoredProcedure [dbo].[FR_AgregarNVCabecera]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FR_AgregarNVCabecera]
(
	/*--------------------------- CAMPOS DISOFI ---------------------------*/
	@pi_IdEmpresaInterna int
,	@pv_EstadoNP [varchar](1) = 'P'
,	@pv_BaseDatos [varchar](100)
,	@pb_InsertaDisofi BIT
,	@pb_InsertaSoftland BIT
,	@pi_IdNotaVenta INT = null
	/*--------------------------- CAMPOS SOFTLAND ---------------------------*/
,	@pi_NVNumero [int]
,	@pd_nvFem [datetime] = NULL
,	@pv_nvEstado [varchar](1) = NULL
,	@pi_nvEstFact [int] = NULL
,	@pi_nvEstDesp [int] = NULL
,	@pi_nvEstRese [int] = NULL
,	@pi_nvEstConc [int] = NULL
,	@pi_CotNum [int] = NULL
,	@pv_NumOC [varchar](12)
,	@pd_nvFeEnt [datetime] = NULL
,	@pv_CodAux [varchar](10) = NULL
,	@pv_VenCod [varchar](4) = NULL
,	@pv_CodMon [varchar](2) = NULL
,	@pv_CodLista [varchar](3) = NULL
,	@pt_nvObser [text] = NULL
,	@pv_nvCanalNV [varchar](3) = NULL
,	@pv_CveCod [varchar](3) = NULL
,	@pv_NomCon [varchar](30) = NULL
,	@pv_CodiCC [varchar](8) = NULL
,	@pv_CodBode [varchar](10) = NULL
,	@pf_nvSubTotal [float] = NULL
,	@pf_nvPorcDesc01 [float] = NULL
,	@pf_nvDescto01 [float] = NULL
,	@pf_nvPorcDesc02 [float] = NULL
,	@pf_nvDescto02 [float] = NULL
,	@pf_nvPorcDesc03 [float] = NULL
,	@pf_nvDescto03 [float] = NULL
,	@pf_nvPorcDesc04 [float] = NULL
,	@pf_nvDescto04 [float] = NULL
,	@pf_nvPorcDesc05 [float] = NULL
,	@pf_nvDescto05 [float] = NULL
,	@pf_nvMonto [float] = NULL
,	@pd_nvFeAprob [datetime] = NULL
,	@pi_NumGuiaRes [int] = NULL
,	@pf_nvPorcFlete [float] = NULL
,	@pf_nvValflete [float] = NULL
,	@pf_nvPorcEmb [float] = NULL
,	@pf_nvValEmb [float] = NULL
,	@pf_nvEquiv [float] = NULL
,	@pf_nvNetoExento [float] = NULL
,	@pf_nvNetoAfecto [float] = NULL
,	@pf_nvTotalDesc [float] = NULL
,	@pv_ConcAuto [varchar](1) = NULL
,	@pv_CodLugarDesp [varchar](30) = NULL
,	@pv_SolicitadoPor [varchar](30) = NULL
,	@pv_DespachadoPor [varchar](30) = NULL
,	@pv_Patente [varchar](9) = NULL
,	@pv_RetiradoPor [varchar](30) = NULL
,	@pv_CheckeoPorAlarmaVtas [varchar](1) = NULL
,	@pi_EnMantencion [int] = NULL
,	@pv_Usuario [varchar](8) = NULL
,	@pv_UsuarioGeneraDocto [varchar](8) = NULL
,	@pd_FechaHoraCreacion [datetime] = NULL
,	@pv_Sistema [varchar](2) = NULL
,	@pv_ConcManual [varchar](1) = NULL
,	@pv_RutSolicitante [varchar](20) = NULL
,	@pv_proceso [varchar](50) = NULL
,	@pf_TotalBoleta [float] = NULL
,	@pi_NumReq [int]
,	@pv_CodVenWeb [varchar](50) = NULL
,	@pv_CodBodeWms [varchar](10) = NULL
,	@pv_CodLugarDocto [varchar](30) = NULL
,	@pv_RutTransportista [varchar](20) = NULL
,	@pv_Cod_Distrib [varchar](10) = NULL
,	@pv_Nom_Distrib [varchar](60) = NULL
,	@pi_MarcaWG [int] = NULL
,	@pb_ErrorAprobador [bit] = NULL
,	@pv_ErrorAprobadorMensaje varchar(max) = NULL
,	@pv_IdCorreoManag int
,   @NroFinalCotizacion char(20)
)
AS
BEGIN
	DECLARE	@VerificadorDisofi BIT
	DECLARE	@MensajeDisofi VARCHAR(MAX)

	DECLARE	@VerificadorSoftland BIT
	DECLARE	@MensajeSoftland VARCHAR(MAX)

	SELECT	@VerificadorDisofi = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'
	,		@VerificadorSoftland = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'

	IF @pb_InsertaDisofi = 1 BEGIN
		INSERT INTO [dbo].[DS_NotasVenta]
		(
			IdEmpresaInterna
		,	EstadoNP
		,	NVNumero
		,	nvFem
		,	nvEstado
		,	nvEstFact
		,	nvEstDesp
		,	nvEstRese
		,	nvEstConc
		,	CotNum
		,	NumOC
		,	nvFeEnt
		,	CodAux
		,	VenCod
		,	CodMon
		,	CodLista
		,	nvObser
		,	nvCanalNV
		,	CveCod
		,	NomCon
		,	CodiCC
		,	CodBode
		,	nvSubTotal
		,	nvPorcDesc01
		,	nvDescto01
		,	nvPorcDesc02
		,	nvDescto02
		,	nvPorcDesc03
		,	nvDescto03
		,	nvPorcDesc04
		,	nvDescto04
		,	nvPorcDesc05
		,	nvDescto05
		,	nvMonto
		,	nvFeAprob
		,	NumGuiaRes
		,	nvPorcFlete
		,	nvValflete
		,	nvPorcEmb
		,	nvValEmb
		,	nvEquiv
		,	nvNetoExento
		,	nvNetoAfecto
		,	nvTotalDesc
		,	ConcAuto
		,	CodLugarDesp
		,	SolicitadoPor
		,	DespachadoPor
		,	Patente
		,	RetiradoPor
		,	CheckeoPorAlarmaVtas
		,	EnMantencion
		--,	Usuario
		,	UsuarioGeneraDocto
		,	FechaHoraCreacion
		,	Sistema
		,	ConcManual
		,	RutSolicitante
		,	proceso
		,	TotalBoleta
		,	NumReq
		,	CodVenWeb
		,	CodBodeWms
		,	CodLugarDocto
		,	RutTransportista
		,	Cod_Distrib
		,	Nom_Distrib
		,	MarcaWG
		,	ErrorAprobador
		,	ErrorAprobadorMensaje
		)
		VALUES
		(
			@pi_IdEmpresaInterna
		,	@pv_EstadoNP
		,	@pi_NVNumero
		,	@pd_nvFem
		,	@pv_nvEstado
		,	@pi_nvEstFact
		,	@pi_nvEstDesp
		,	@pi_nvEstRese
		,	@pi_nvEstConc
		,	@pi_CotNum
		,	@pv_NumOC
		,	@pd_nvFeEnt
		,	@pv_CodAux
		,	@pv_VenCod
		,	@pv_CodMon
		,	@pv_CodLista
		,	@pt_nvObser
		,	@pv_nvCanalNV
		,	@pv_CveCod
		,	@pv_NomCon
		,	@pv_CodiCC
		,	@pv_CodBode
		,	@pf_nvSubTotal
		,	@pf_nvPorcDesc01
		,	@pf_nvDescto01
		,	@pf_nvPorcDesc02
		,	@pf_nvDescto02
		,	@pf_nvPorcDesc03
		,	@pf_nvDescto03
		,	@pf_nvPorcDesc04
		,	@pf_nvDescto04
		,	@pf_nvPorcDesc05
		,	@pf_nvDescto05
		,	@pf_nvMonto
		,	@pd_nvFeAprob
		,	@pi_NumGuiaRes
		,	@pf_nvPorcFlete
		,	@pf_nvValflete
		,	@pf_nvPorcEmb
		,	@pf_nvValEmb
		,	@pf_nvEquiv
		,	@pf_nvNetoExento
		,	@pf_nvNetoAfecto
		,	@pf_nvTotalDesc
		,	@pv_ConcAuto
		,	@pv_CodLugarDesp
		,	@pv_SolicitadoPor
		,	@pv_DespachadoPor
		,	@pv_Patente
		,	@pv_RetiradoPor
		,	@pv_CheckeoPorAlarmaVtas
		,	@pi_EnMantencion
		--,	@pv_Usuario
		,	'softland'--@pv_UsuarioGeneraDocto
		,	@pd_FechaHoraCreacion
		,	@pv_Sistema
		,	@pv_ConcManual
		,	@pv_RutSolicitante
		,	@pv_proceso
		,	@pf_TotalBoleta
		,	@pi_NumReq
		,	@pv_CodVenWeb
		,	@pv_CodBodeWms
		,	@pv_CodLugarDocto
		,	@pv_RutTransportista
		,	@pv_Cod_Distrib
		,	@pv_Nom_Distrib
		,	@pi_MarcaWG
		,	@pb_ErrorAprobador
		,	@pv_ErrorAprobadorMensaje
		)

		SELECT	@pi_IdNotaVenta = @@identity
		
		SELECT	@VerificadorDisofi = 1
		,		@MensajeDisofi = 'Se agrego en disofi satisfactoriamente'

		UPDATE	[dbo].[DS_NotasVenta]
		set		nvObser = ('N. Int: ' + convert(varchar(20), @pi_IdNotaVenta) + ' Obs: ' + convert(varchar(max), isnull(nvObser, '')))
		where	Id = @pi_IdNotaVenta

		--if @pi_CotNum >0
		--update DS_Cotizacion set EstadoNP='A', NVNumero=@pi_IdNotaVenta where id=@pi_CotNum
		declare @IdCotizacion int
		SELECT @IdCotizacion = C.Id
		FROM   DS_Cotizacion AS C LEFT OUTER JOIN DS_NotaVentaExtras AS e ON C.Id = e.IdNotaVenta
		WHERE  (e.NroFinalCotizacion =rtrim(ltrim( @NroFinalCotizacion)))

		UPDATE	[dbo].[DS_Cotizacion]
		SET		NVNumero = @pi_IdNotaVenta, EstadoNP = 'A'
		WHERE	Id = @IdCotizacion

	END
	IF @pb_InsertaSoftland = 1 BEGIN
		EXEC [FR_AgregarNVCabeceraSoftland]
			@pi_IdNotaVenta = @pi_IdNotaVenta
		,	@pv_BaseDatos = @pv_BaseDatos
		,	@pb_Verificador = @VerificadorSoftland out
		,	@pv_Mensaje = @MensajeSoftland out
		,	@pi_NVNumero = @pi_NVNumero out

		
	END

	exec SP_INS_DatosCorreoManag @pi_IdNotaVenta,@pv_IdCorreoManag

	SELECT	IdNotaVenta = @pi_IdNotaVenta
	,		NVNumero = @pi_NVNumero
	,		VerificadorDisofi = @VerificadorDisofi
	,		MensajeDisofi = @MensajeDisofi
	,		VerificadorSoftland = @VerificadorSoftland
	,		MensajeSoftland = @MensajeSoftland
END

GO
/****** Object:  StoredProcedure [dbo].[FR_AgregarNVCabeceraSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[FR_AgregarNVCabeceraSoftland]
(
	@pi_IdNotaVenta INT
,	@pv_BaseDatos varchar(100)
,	@pb_Verificador BIT OUTPUT
,	@pv_Mensaje VARCHAR(max) OUTPUT
,	@pi_NVNumero INT OUTPUT
)
AS
	declare	@lb_Verificador BIT
	,		@lv_Mensaje VARCHAR(max)

	declare @query nvarchar(max)

	select	@pb_Verificador = cast(1 as bit)
	,		@pv_Mensaje = 'Nota Venta Agregado Satisfactoriamente'

	select @query = ''
	select @query = '
	begin try
	DECLARE @NOMBRE_COLUMNA VARCHAR(MAX)
	DECLARE @QUERY_FINAL VARCHAR(MAX)
	DECLARE @QUERY_INSERT VARCHAR(MAX)
	DECLARE @QUERY_SELECT VARCHAR(MAX)
	DECLARE @QUERY_UPDATE VARCHAR(MAX)
	DECLARE @QUERY_INSERT_COLUMNAS VARCHAR(MAX)
	DECLARE @QUERY_SELECT_COLUMNAS VARCHAR(MAX)
	
	SELECT	@QUERY_FINAL = ''''
	SELECT	@QUERY_INSERT = ''''
	SELECT	@QUERY_SELECT = ''''
	SELECT	@QUERY_UPDATE = ''''
	SELECT	@QUERY_INSERT_COLUMNAS = ''''
	SELECT	@QUERY_SELECT_COLUMNAS = ''''

	SELECT	@NVNumero = (SELECT ISNULL((MAX(sub_a.NVNumero) + 1), 0) FROM [' + @pv_BaseDatos + '].[softland].[nw_nventa] sub_a)
	IF CURSOR_STATUS(''global'',''CURSOR_COLUMNAS'') >= -1 BEGIN
		DEALLOCATE CURSOR_COLUMNAS
	END

	DECLARE CURSOR_COLUMNAS CURSOR FOR
		SELECT	COLUMN_NAME
		FROM	[' + @pv_BaseDatos + '].INFORMATION_SCHEMA.COLUMNS
		WHERE	TABLE_NAME = ''nw_nventa''

	OPEN CURSOR_COLUMNAS

	FETCH NEXT FROM CURSOR_COLUMNAS
	INTO @NOMBRE_COLUMNA

	WHILE @@FETCH_STATUS = 0 BEGIN
		SELECT	@QUERY_INSERT_COLUMNAS = @QUERY_INSERT_COLUMNAS + 
''		['' + @NOMBRE_COLUMNA + '']
,''
		IF (@NOMBRE_COLUMNA IN (' + '''NVNumero''' + ')) BEGIN
			SELECT	@QUERY_SELECT_COLUMNAS = @QUERY_SELECT_COLUMNAS + 
''		'''''' + CONVERT(VARCHAR(20), @NVNumero) + ''''''
,''
		END
		ELSE BEGIN
		SELECT	@QUERY_SELECT_COLUMNAS = @QUERY_SELECT_COLUMNAS + 
''		['' + @NOMBRE_COLUMNA + '']
,''
		END

		FETCH NEXT FROM CURSOR_COLUMNAS
		INTO @NOMBRE_COLUMNA
	END
	
	SELECT	@QUERY_INSERT_COLUMNAS = SUBSTRING(@QUERY_INSERT_COLUMNAS, 1, LEN(@QUERY_INSERT_COLUMNAS) - 3)
	SELECT	@QUERY_SELECT_COLUMNAS = SUBSTRING(@QUERY_SELECT_COLUMNAS, 1, LEN(@QUERY_SELECT_COLUMNAS) - 3)

	SELECT	@QUERY_INSERT = ''
INSERT INTO [' + @pv_BaseDatos + '].[softland].[nw_nventa]
(
'' + @QUERY_INSERT_COLUMNAS + ''
)''
	SELECT	@QUERY_SELECT = ''
SELECT	
'' + @QUERY_SELECT_COLUMNAS + ''
FROM	[dbo].[DS_NotasVenta] 
WHERE	Id = ' + CONVERT(VARCHAR(20), @pi_IdNotaVenta) + '''

	SELECT	@QUERY_UPDATE = ''
	UPDATE	[dbo].[DS_NotasVenta] 
	SET		NVNumero = '''''' + CONVERT(VARCHAR(100), @NVNumero) + ''''''
	WHERE	Id = ' + CONVERT(VARCHAR(20), @pi_IdNotaVenta) + '
	
	UPDATE	[dbo].[DS_NotasVentaDetalle] 
	SET		NVNumero = '''''' + CONVERT(VARCHAR(100), @NVNumero) + ''''''
	WHERE	IdNotaVenta = ' + CONVERT(VARCHAR(20), @pi_IdNotaVenta) + '
	''

	
	SELECT	@QUERY_FINAL = @QUERY_INSERT + @QUERY_SELECT + @QUERY_UPDATE

	EXEC (@QUERY_FINAL)

	CLOSE CURSOR_COLUMNAS
	DEALLOCATE CURSOR_COLUMNAS

	end try
	begin catch
		select	@lb_Verificador = cast(0 as bit)
		,		@lv_Mensaje = error_message()
	end catch
	'
	
	EXEC sp_executesql	@query
	,					N'@NVNumero INT OUTPUT, @lv_Mensaje VARCHAR(max) OUTPUT, @lb_Verificador BIT OUTPUT',
	@pi_NVNumero OUTPUT, @lv_Mensaje OUTPUT, @lb_Verificador OUTPUT

	select	@pv_Mensaje = case when @lv_Mensaje is null then @pv_Mensaje else @lv_Mensaje end
	,		@pb_Verificador = case when @lb_Verificador is null then @pb_Verificador else @lb_Verificador END

	EXEC [dbo].[FR_ActualizaClienteVendedorSoftland]
				@pv_BaseDatos = @pv_BaseDatos
	,			@pi_IdNotaVenta = @pi_IdNotaVenta


GO
/****** Object:  StoredProcedure [dbo].[FR_AgregarNVDetalle]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_AgregarNVDetalle]							*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_AgregarNVDetalle]
(
	/*--------------------------- CAMPOS DISOFI ---------------------------*/
	@pv_BaseDatos [varchar](100)
,	@pb_InsertaDisofi BIT
,	@pb_InsertaSoftland BIT
,	@pi_IdNotaVenta INT
	/*--------------------------- CAMPOS SOFTLAND ---------------------------*/
,	@pi_NVNumero int
,	@pf_nvLinea float
,	@pf_nvCorrela float = NULL
,	@pd_nvFecCompr datetime = NULL
,	@pv_CodProd varchar(20) = NULL
,	@pf_nvCant float = NULL
,	@pf_nvPrecio float = NULL
,	@pf_nvEquiv float = NULL
,	@pf_nvSubTotal float = NULL
,	@pf_nvDPorcDesc01 float = NULL
,	@pf_nvDDescto01 float = NULL
,	@pf_nvDPorcDesc02 float = NULL
,	@pf_nvDDescto02 float = NULL
,	@pf_nvDPorcDesc03 float = NULL
,	@pf_nvDDescto03 float = NULL
,	@pf_nvDPorcDesc04 float = NULL
,	@pf_nvDDescto04 float = NULL
,	@pf_nvDPorcDesc05 float = NULL
,	@pf_nvDDescto05 float = NULL
,	@pf_nvTotDesc float = NULL
,	@pf_nvTotLinea float = NULL
,	@pf_nvCantDesp float = NULL
,	@pf_nvCantProd float = NULL
,	@pf_nvCantFact float = NULL
,	@pf_nvCantDevuelto float = NULL
,	@pf_nvCantNC float = NULL
,	@pf_nvCantBoleta float = NULL
,	@pf_nvCantOC float = NULL
,	@pt_DetProd text = NULL
,	@pv_CheckeoMovporAlarmaVtas varchar(1) = NULL
,	@pv_KIT varchar(20) = NULL
,	@pi_CodPromocion int = NULL
,	@pv_CodUMed varchar(6) = NULL
,	@pf_CantUVta float = NULL
,	@pv_Partida varchar(20) = NULL
,	@pv_Pieza varchar(20) = NULL
,	@pd_FechaVencto datetime = NULL
,	@pf_CantidadKit float
,	@pi_MarcaWG int = NULL
,	@pf_PorcIncidenciaKit float
)
AS
BEGIN
	DECLARE	@IdDetalleNotaVenta INT
	DECLARE	@VerificadorDisofi BIT
	DECLARE	@MensajeDisofi VARCHAR(MAX)

	DECLARE	@VerificadorSoftland BIT
	DECLARE	@MensajeSoftland VARCHAR(MAX)

	SELECT	@VerificadorDisofi = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'
	,		@VerificadorSoftland = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'
	
	DECLARE @query1 nvarchar(max);
	DECLARE @ParmDefinition nvarchar(500);  
	
	 IF @pt_DetProd IS NULL or CONVERT(VARCHAR(MAX), @pt_DetProd) = '' BEGIN  
	  SELECT @query1 = N'SELECT @DetProdOUT = convert(varchar(max), desprod) FROM ' + @pv_BaseDatos + '.[softland].[iw_tprod] WHERE CodProd = ''' + @pv_CodProd + '''';  
  
	  SET @ParmDefinition = N'@DetProdOUT varchar(max) OUTPUT';     
  
	  EXEC sp_executesql @query1, @ParmDefinition, @DetProdOUT=@pt_DetProd OUTPUT;    
	 END  
	 IF @pv_CodUMed IS NULL or @pv_CodUMed = '' BEGIN  
	  SELECT @query1 = N'SELECT @CodUMedOUT = convert(varchar(max), codumed) FROM ' + @pv_BaseDatos + '.[softland].[iw_tprod] WHERE CodProd = ''' + @pv_CodProd + '''';  
  
	  SET @ParmDefinition = N'@CodUMedOUT varchar(max) OUTPUT';     
  
	  EXEC sp_executesql @query1, @ParmDefinition, @CodUMedOUT=@pv_CodUMed OUTPUT;    
	 END  
	 declare @IDCotizacion int
	 select @IDCotizacion=IdNotaVenta from DS_NotaVentaExtras where IdNotaVenta = IdNotaVenta and EsCotizacion=1
	 if @IDCotizacion is null
	   set @IDCotizacion=0

	   if @IDCotizacion>0
	   select @pd_nvFecCompr=nvFecCompr 
	   from DS_CotizacionDetalle 
	   where IdNotaVenta=@IDCotizacion and nvLinea = @pf_nvLinea


	IF @pb_InsertaDisofi = 1 BEGIN
		INSERT INTO [dbo].[DS_NotasVentaDetalle]
		(
			IdNotaVenta
		,	NVNumero
		,	nvLinea
		,	nvCorrela
		,	nvFecCompr
		,	CodProd
		,	nvCant
		,	nvPrecio
		,	nvEquiv
		,	nvSubTotal
		,	nvDPorcDesc01
		,	nvDDescto01
		,	nvDPorcDesc02
		,	nvDDescto02
		,	nvDPorcDesc03
		,	nvDDescto03
		,	nvDPorcDesc04
		,	nvDDescto04
		,	nvDPorcDesc05
		,	nvDDescto05
		,	nvTotDesc
		,	nvTotLinea
		,	nvCantDesp
		,	nvCantProd
		,	nvCantFact
		,	nvCantDevuelto
		,	nvCantNC
		,	nvCantBoleta
		,	nvCantOC
		,	DetProd
		,	CheckeoMovporAlarmaVtas
		,	KIT
		,	CodPromocion
		,	CodUMed
		,	CantUVta
		,	Partida
		,	Pieza
		,	FechaVencto
		,	CantidadKit
		,	MarcaWG
		,	PorcIncidenciaKit
		)
		VALUES
		(
			@pi_IdNotaVenta
		,	@pi_NVNumero
		,	@pf_nvLinea
		,	@pf_nvCorrela
		,	@pd_nvFecCompr
		,	@pv_CodProd
		,	@pf_nvCant
		,	@pf_nvPrecio
		,	@pf_nvEquiv
		,	@pf_nvSubTotal
		,	@pf_nvDPorcDesc01
		,	@pf_nvDDescto01
		,	@pf_nvDPorcDesc02
		,	@pf_nvDDescto02
		,	@pf_nvDPorcDesc03
		,	@pf_nvDDescto03
		,	@pf_nvDPorcDesc04
		,	@pf_nvDDescto04
		,	@pf_nvDPorcDesc05
		,	@pf_nvDDescto05
		,	@pf_nvTotDesc
		,	@pf_nvTotLinea
		,	@pf_nvCantDesp
		,	@pf_nvCantProd
		,	@pf_nvCantFact
		,	@pf_nvCantDevuelto
		,	@pf_nvCantNC
		,	@pf_nvCantBoleta
		,	0--@pf_nvCantOC
		,	@pt_DetProd
		,	@pv_CheckeoMovporAlarmaVtas
		,	@pv_KIT
		,	@pi_CodPromocion
		,	@pv_CodUMed
		,	@pf_CantUVta
		,	@pv_Partida
		,	@pv_Pieza
		,	@pd_FechaVencto
		,	@pf_CantidadKit
		,	@pi_MarcaWG
		,	@pf_PorcIncidenciaKit
		)

		SELECT	@IdDetalleNotaVenta = @@identity
		
		SELECT	@VerificadorDisofi = 1
		,		@MensajeDisofi = 'Se agrego en disofi satisfactoriamente'
	END
	IF @pb_InsertaSoftland = 1 BEGIN
		EXEC [FR_AgregarNVDetalleSoftland]
			@pi_IdNotaVenta = @pi_IdNotaVenta
		,	@pv_BaseDatos = @pv_BaseDatos
		,	@pi_IdDetalleNotaVenta = @IdDetalleNotaVenta
		,	@pb_Verificador = @VerificadorSoftland out
		,	@pv_Mensaje = @MensajeSoftland out
		,	@pi_NVNumero = @pi_NVNumero out
	END

	SELECT	IdNotaVenta = @pi_IdNotaVenta
	,		IdDetalleNotaVenta = @IdDetalleNotaVenta
	,		NVNumero = @pi_NVNumero
	,		VerificadorDisofi = @VerificadorDisofi
	,		MensajeDisofi = @MensajeDisofi
	,		VerificadorSoftland = @VerificadorSoftland
	,		MensajeSoftland = @MensajeSoftland
END
GO
/****** Object:  StoredProcedure [dbo].[FR_AgregarNVDetalleSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_AgregarNVDetalleSoftland]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_AgregarNVDetalleSoftland]
(
	@pi_IdNotaVenta INT
,	@pi_IdDetalleNotaVenta INT
,	@pv_BaseDatos varchar(100)
,	@pb_Verificador BIT OUTPUT
,	@pv_Mensaje VARCHAR(max) OUTPUT
,	@pi_NVNumero INT OUTPUT
)
AS
	declare @query nvarchar(max)

	select @query = ''

	select @query = '
	DECLARE @NOMBRE_COLUMNA VARCHAR(MAX)
	DECLARE @QUERY_FINAL VARCHAR(MAX)
	DECLARE @QUERY_INSERT VARCHAR(MAX)
	DECLARE @QUERY_SELECT VARCHAR(MAX)
	DECLARE @QUERY_INSERT_COLUMNAS VARCHAR(MAX)
	DECLARE @QUERY_SELECT_COLUMNAS VARCHAR(MAX)
	
	SELECT	@QUERY_FINAL = ''''
	SELECT	@QUERY_INSERT = ''''
	SELECT	@QUERY_SELECT = ''''
	SELECT	@QUERY_INSERT_COLUMNAS = ''''
	SELECT	@QUERY_SELECT_COLUMNAS = ''''
	
	IF CURSOR_STATUS(''global'',''CURSOR_COLUMNAS'') >= -1 BEGIN
		DEALLOCATE CURSOR_COLUMNAS
	END

	DECLARE CURSOR_COLUMNAS CURSOR FOR
		SELECT	COLUMN_NAME
		FROM	[' + @pv_BaseDatos + '].INFORMATION_SCHEMA.COLUMNS
		WHERE	TABLE_NAME = ''nw_detnv''

	OPEN CURSOR_COLUMNAS

	FETCH NEXT FROM CURSOR_COLUMNAS
	INTO @NOMBRE_COLUMNA

	WHILE @@FETCH_STATUS = 0 BEGIN
		SELECT	@QUERY_INSERT_COLUMNAS = @QUERY_INSERT_COLUMNAS + 
''		['' + @NOMBRE_COLUMNA + '']
,''

		SELECT	@QUERY_SELECT_COLUMNAS = @QUERY_SELECT_COLUMNAS + 
''		['' + @NOMBRE_COLUMNA + '']
,''

		FETCH NEXT FROM CURSOR_COLUMNAS
		INTO @NOMBRE_COLUMNA
	END
	
	SELECT	@QUERY_INSERT_COLUMNAS = SUBSTRING(@QUERY_INSERT_COLUMNAS, 1, LEN(@QUERY_INSERT_COLUMNAS) - 3)
	SELECT	@QUERY_SELECT_COLUMNAS = SUBSTRING(@QUERY_SELECT_COLUMNAS, 1, LEN(@QUERY_SELECT_COLUMNAS) - 3)

	SELECT	@QUERY_INSERT = ''
INSERT INTO [' + @pv_BaseDatos + '].[softland].[nw_detnv]
(
'' + @QUERY_INSERT_COLUMNAS + ''
)''
	SELECT	@QUERY_SELECT = ''
SELECT	
'' + @QUERY_SELECT_COLUMNAS + ''
FROM	[dbo].[DS_NotasVentaDetalle] 
WHERE	IdNotaVenta = ' + CONVERT(VARCHAR(20), @pi_IdNotaVenta) + '
AND		Id = ' + CONVERT(VARCHAR(20), @pi_IdDetalleNotaVenta) + '''

	SELECT	@QUERY_FINAL = @QUERY_INSERT + @QUERY_SELECT

	EXEC (@QUERY_FINAL)

	CLOSE CURSOR_COLUMNAS
	DEALLOCATE CURSOR_COLUMNAS
	'

	select	@pb_Verificador = cast(1 as bit)
	,		@pv_Mensaje = 'Nota Venta Agregado Satisfactoriamente'

	EXEC sp_executesql	@query
	,					N'@NVNumero INT OUTPUT',
	@pi_NVNumero OUTPUT;

GO
/****** Object:  StoredProcedure [dbo].[FR_AgregarUsuario]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FR_AgregarUsuario]
	@Usuario	varchar(10),
	@email	varchar(50),
	@Contrasena varchar(150),
	@tipoUsuario	varchar(50),
	@Nombre varchar (100)
 AS
DECLARE @CantVenCod int
DECLARE @IdUsuario int
SET @CantVenCod	= (SELECT count(*) AS cantidad FROM dbo.DS_Usuarios du WHERE Usuario = @Usuario AND du.Estado = 1)
if(@CantVenCod = 0)
BEGIN
	INSERT INTO [dbo].[DS_Usuarios] ([Usuario],[Contrasena],[email],[tipoUsuario],[Nombre],[Estado])
		VALUES(@Usuario,(@Contrasena), @email, @tipoUsuario, @Nombre,1)
	 
SET @IdUsuario = (SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY])	

		SELECT Verificador = cast(1 AS bit),
		Mensaje = 'Usuario Creado'
END
ELSE
SELECT	Verificador = cast(0 AS bit),
		Mensaje = 'Usuario ya Existe'

GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarClientes]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_BuscarClientes]
 @RutAux	varchar(30),
	@CodAux	varchar(150)
as
begin
	select 
	clientes.EMail,
	clientes.CodAux,
	clientes.[NomAux],
	clientes.[RutAux],
	clientes.[DirAux] ,
	clientes.[DirNum], 
	contacto.[NomCon] , 
	contacto.[FonCon]
    from [KUPPEL].[softland].[cwtauxi] clientes
	inner join [KUPPEL].[softland].[cwtaxco] contacto on clientes.CodAux = contacto.CodAuc 
	where RutAux = @RutAux AND
		CodAux = @CodAux

	--where clientes.RutAux ='76.387.390-0' and contacto.[NomCon] like '%jorge naza%'
end


GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarDirecDespa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_BuscarDirecDespa]							*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE procedure [dbo].[FR_BuscarDirecDespa]
(
	@CodAxD varchar(10)
,	@pv_BaseDatos varchar(100)
)
as
begin
	declare @query varchar(max)
	select @query = ''
	declare @sincronizado int
	select @sincronizado=sincronizado from DS_cwtauxi where CodAux=@CodAxD
	if @sincronizado is null
	  set @sincronizado=1

	if @sincronizado=0
	begin
	select @query = @query + '
	select	clientes.NomDch 
	,		clientes.DirDch
	,		clientes.ComDch
	,		com.ComDes
	,		clientes.CiuDch
	,		ciu.CiuDes
	from	ds_cwtauxd clientes
		left join [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
			ON clientes.ComDch collate SQL_Latin1_General_CP1_CI_AI = com.ComCod
		left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
			ON clientes.CiuDch  collate SQL_Latin1_General_CP1_CI_AI = ciu.CiuCod
	where	CodAxD = ''' + @CodAxD + ''''
	end


	if @sincronizado<>0
	begin
	select @query = @query + '
	select	clientes.NomDch 
	,		clientes.DirDch
	,		clientes.ComDch
	,		com.ComDes
	,		clientes.CiuDch
	,		ciu.CiuDes
	from	[' + @pv_BaseDatos + '].[Softland].cwtauxd clientes
		left join [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
			ON clientes.ComDch = com.ComCod
		left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
			ON clientes.CiuDch = ciu.CiuCod
	where	CodAxD = ''' + @CodAxD + ''''
	end
	
	EXEC (@query)
end  
    
GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarDirecDespaMail]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_BuscarDirecDespaMail]
(
	@CodAxD varchar(10)
,	@pv_BaseDatos varchar(100)
,	@NomDch varchar(150)
)
as
begin
	declare @query varchar(max)

	select @query = ''
	declare @sincronizado int
	select @sincronizado = count(*) from DS_cwtauxi where CodAux=@CodAxD and sincronizado=0
	if @sincronizado=0
	   select @sincronizado=1

	if @sincronizado=0
	begin
	select @query = @query + '
	select	clientes.NomDch 
	,		clientes.DirDch
	,		clientes.ComDch
	,		com.ComDes
	,		clientes.CiuDch
	,		ciu.CiuDes
	from DS_cwtauxd clientes left join
	     [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
			ON clientes.ComDch collate SQL_Latin1_General_CP1_CI_AI = com.ComCod
		left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
			ON clientes.CiuDch collate SQL_Latin1_General_CP1_CI_AI = ciu.CiuCod
	where	CodAxD = ''' + @CodAxD + ''' and NomDch = '''+ @NomDch +''' ' 
	end

	else

	begin
	select @query = @query + '
	select	clientes.NomDch 
	,		clientes.DirDch
	,		clientes.ComDch
	,		com.ComDes
	,		clientes.CiuDch
	,		ciu.CiuDes
	from	[' + @pv_BaseDatos + '].[Softland].cwtauxd clientes
		left join [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
			ON clientes.ComDch = com.ComCod
		left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
			ON clientes.CiuDch = ciu.CiuCod
	where	CodAxD = ''' + @CodAxD + ''' and NomDch = '''+ @NomDch +''' ' 
	end
	
	EXEC (@query)
end  


GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarNVCabecera]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[FR_BuscarNVCabecera] 
	@nvId   INT,
	@pv_BaseDatos varchar(100)
AS
DECLARE @query varchar (max)
SELECT @query = ''
DECLARE @Sincronizado int 
declare @CodAux varchar(10)

    select @CodAux = CodAux from DS_NotasVenta where Id= @nvId
	select @Sincronizado = sincronizado FROM DS_cwtauxi where CodAux = @CodAux
	if @Sincronizado is null 
	   Set @Sincronizado=1
	if(@Sincronizado = 0)
	BEGIN
		SELECT @query = @query + '
		BEGIN
			SELECT
				nv.NVNumero,
				vend.VenCod,
				vend.VenDes,
				vend.Usuario,
				isnull(conven.CveCod,'''')CveCod,
				isnull(conven.CveDes,''Sin condicion'')CveDes,
				convert(datetime, nv.nvFem,103) ''nvFem'',
				convert(datetime, nv.nvFeEnt,103) ''nvFeEnt'',
				lista.CodLista,
				lista.DesLista,
				cliente.CodAux,
				cliente.NomAux,
				nv.NomCon,
				cc.CodiCC,
				cc.DescCC,
				nv.nvObser,
				nv.nvSubTotal,
				nv.TotalBoleta,
				nv.ErrorAprobador,
				nv.ErrorAprobadorMensaje,
				isnull(DS_Cotizacion.Id,0) AS NroCotizacion,
				isnull(DS_NotaVentaExtras.NroFinalCotizacion,'''') NroFinalCotizacion
			FROM    DS_NotaVentaExtras RIGHT OUTER JOIN
					DS_Cotizacion ON DS_NotaVentaExtras.IdNotaVenta = DS_Cotizacion.Id RIGHT OUTER JOIN
					DS_NotasVenta AS nv INNER JOIN
					DS_cwtauxi AS cliente ON cliente.CodAux COLLATE Modern_Spanish_CI_AS = nv.CodAux ON DS_Cotizacion.NVNumero = nv.Id LEFT OUTER JOIN
					['+@pv_BaseDatos+'].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = nv.VenCod LEFT OUTER JOIN
					['+@pv_BaseDatos+'].softland.cwtconv AS conven ON conven.CveCod COLLATE Modern_Spanish_CI_AS = nv.CveCod LEFT OUTER JOIN
					['+@pv_BaseDatos+'].softland.iw_tlispre AS lista ON lista.CodLista COLLATE Modern_Spanish_CI_AS = nv.CodLista LEFT OUTER JOIN
					['+@pv_BaseDatos+'].softland.cwtccos AS cc ON cc.CodiCC COLLATE Modern_Spanish_CI_AS = nv.CodiCC
			WHERE
				nv.Id = '+convert(varchar(100),@nvId)+'
		END'
	END


	if(@Sincronizado>0)
	begin
		SELECT @query = @query + '
		BEGIN
			SELECT
				nv.NVNumero,
				vend.VenCod,
				vend.VenDes,
				vend.Usuario,
				isnull(conven.CveCod,'''')CveCod,
				isnull(conven.CveDes,''Sin condicion'')CveDes,
				convert(datetime, nv.nvFem,103) ''nvFem'',
				convert(datetime, nv.nvFeEnt,103) ''nvFeEnt'',
				lista.CodLista,
				lista.DesLista,
				cliente.CodAux,
				cliente.NomAux,
				nv.NomCon,
				cc.CodiCC,
				cc.DescCC,
				nv.nvObser,
				nv.nvSubTotal,
				nv.TotalBoleta,
				nv.ErrorAprobador,
				nv.ErrorAprobadorMensaje,
				isnull(DS_Cotizacion.Id,0) AS NroCotizacion,
				isnull(DS_NotaVentaExtras.NroFinalCotizacion,'''') NroFinalCotizacion
				FROM    DS_NotaVentaExtras RIGHT OUTER JOIN
						DS_Cotizacion ON DS_NotaVentaExtras.IdNotaVenta = DS_Cotizacion.Id RIGHT OUTER JOIN
						DS_NotasVenta AS nv INNER JOIN
						['+@pv_BaseDatos+'].softland.cwtauxi AS cliente ON cliente.CodAux COLLATE Modern_Spanish_CI_AS = nv.CodAux ON DS_Cotizacion.NVNumero = nv.Id LEFT OUTER JOIN
						['+@pv_BaseDatos+'].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = nv.VenCod LEFT OUTER JOIN
						['+@pv_BaseDatos+'].softland.cwtconv AS conven ON conven.CveCod COLLATE Modern_Spanish_CI_AS = nv.CveCod LEFT OUTER JOIN
						['+@pv_BaseDatos+'].softland.iw_tlispre AS lista ON lista.CodLista COLLATE Modern_Spanish_CI_AS = nv.CodLista LEFT OUTER JOIN
						['+@pv_BaseDatos+'].softland.cwtccos AS cc ON cc.CodiCC COLLATE Modern_Spanish_CI_AS = nv.CodiCC
			WHERE
				nv.Id = '+convert(varchar(100),@nvId)+'
		END'
	END
	print (@query)
EXEC (@query)
GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarNVDetalle]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[FR_BuscarNVDetalle]  
 @nvId   int,  
 @pv_BaseDatos varchar(100)  
AS  
DECLARE @query varchar (max)  
  
 declare @lv_bodega varchar(50)  
  
select top 1   
  @lv_bodega =   
    case when a.stockproductoesbodega = 1   
       then isnull(stockproductocodigobodega, '')   
       else ''   
    end   
from ds_parametros a   
 inner join ds_empresa b   
  on a.idempresa = b.id   
where b.basedatos = @pv_BaseDatos  
  
SELECT @query = ''  
SELECT @query = @query + '  
begin  
select   
a.Id,
a.nvLinea,  
a.CodProd,   
EsProductoNuevo = CAST(case when tp.DesProd collate Modern_Spanish_CI_AS is not null then 0 else 1 end AS BIT),
DesProd = case when tp.DesProd collate Modern_Spanish_CI_AS is not null then tp.DesProd collate Modern_Spanish_CI_AS else productosNuevos.DesProd collate Modern_Spanish_CI_AS end,
CodMon = case when tp.CodMonOrig collate Modern_Spanish_CI_AS is not null then tp.CodMonOrig collate Modern_Spanish_CI_AS else productosNuevos.CodMonOrig collate Modern_Spanish_CI_AS end,
CodGrupo = case when tp.CodGrupo collate Modern_Spanish_CI_AS is not null then tp.CodGrupo collate Modern_Spanish_CI_AS else productosNuevos.CodGrupo collate Modern_Spanish_CI_AS end,
CodSubGr = case when tp.CodSubGr collate Modern_Spanish_CI_AS is not null then tp.CodSubGr collate Modern_Spanish_CI_AS else productosNuevos.CodSubGr collate Modern_Spanish_CI_AS end,
ValorNetoProducto = case when tp.PrecioVta is not null then tp.PrecioVta else productosNuevos.PrecioVta end,
a.Partida,  
a.Pieza,  
a.nvCant,   
a.CodUMed,   
a.nvPrecio,   
a.nvSubTotal,  
ROUND(a.nvDPorcDesc01,0) as nvDPorcDesc01,   
ROUND(a.nvDPorcDesc02,0) as nvDPorcDesc02,   
ROUND(a.nvDPorcDesc03,0) as nvDPorcDesc03,   
ROUND(a.nvDPorcDesc04,0) as nvDPorcDesc04,   
ROUND(a.nvDPorcDesc05,0) as nvDPorcDesc05,   
a.nvTotLinea,  
  Stock = ISNULL((    
      select  Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible    
      FROM  [' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro))     
      WHERE  Fecha <= GETDATE()    
      and   CodProd = tp.CodProd   ' +  
  case when @lv_bodega = '' then '' else ' and codbode = ''' + @lv_bodega + '''' end + '  
      GROUP BY CodProd    
     ), 0)--Stock = [dbo].[stock2018](tp.CodProd)    
from [dbo].[DS_NotasVentaDetalle] a  
left JOIN ['+@pv_BaseDatos+'].[softland].[iw_tprod] AS tp on a.CodProd collate Modern_Spanish_CI_AS = tp.CodProd   
left JOIN DS_ProductosNuevos as productosNuevos on a.CodProd collate Modern_Spanish_CI_AS = productosNuevos.CodProd   
where a.IdNotaVenta = '+convert(varchar(100),@nvId)+'  
order by a.nvLinea  
end  
'  
EXEC (@query)  
PRINT (@query)  

GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarParametrosUsuarios]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_BuscarParametrosUsuarios]					*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
  
CREATE procedure [dbo].[FR_BuscarParametrosUsuarios]
(
	@pi_idEmpresa INT
)
AS
BEGIN  
	SELECT	* 
	FROM	[dbo].[DS_Parametros]
	where	IdEmpresa = @pi_idEmpresa  
END  

GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarProducto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_BuscarProducto]
	@DesProd	varchar(max),
	@CodLista	varchar(3)
AS
BEGIN
	SELECT DISTINCT	
		CodProd = tp.CodProd, 
		DesProd = ISNULL(tp.DesProd,''), 
		codgrupo = ISNULL(tp.CodGrupo,''),
		codsubgr = ISNULL(tp.CodSubGr,''),
		PrecioVta = pd.valorPct, 
		codumed = pd.CodUmed,
		desumed = ISNULL(detumed.desumed,''),
		CodLista = lp.CodLista,
		Stock =		ISNULL((
						select		Sum (CASE WHEN TipoBod = 'D' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible
						FROM		transporte.softland.IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro)) 
						WHERE		Fecha <= GETDATE()  
						and			CodProd = tp.CodProd 
						GROUP BY	CodProd
					), 0)--[DSNotaVenta].[dbo].[stock2018](tp.CodProd)
	FROM 
		[KUPPEL].[softland].[iw_tprod] AS tp 
		LEFT JOIN [KUPPEL].[softland].[iw_gmovi] AS gm ON tp.CodProd = gm.CodProd 
		LEFT JOIN [KUPPEL].[softland].[iw_tlprprod] AS pd ON tp.CodProd = pd.CodProd
		LEFT JOIN [KUPPEL].[softland].[iw_tlispre] AS lp ON pd.CodLista = lp.CodLista 
		LEFT JOIN [KUPPEL].[softland].[iw_tumed] AS detumed on pd.CodUmed = detumed.CodUMed
		WHERE --tp.DesProd = 'GUANTE CABRITILLA CON FORRO'  AND lp.CodLista = '01'
		tp.DesProd LIKE (CASE @DesProd WHEN '' THEN tp.DesProd ELSE '%' + @DesProd +'%' END)
		AND lp.CodLista = @CodLista OR tp.CodProd LIKE (CASE  @DesProd WHEN '' THEN tp.CodProd ElSE '%'+ @DesProd + '%' END) 
					
					
	GROUP BY 
		tp.CodProd, 
		tp.DesProd, 
		tp.CodGrupo, 
		tp.CodSubGr, 
		tp.PrecioVta, 
		pd.ValorPct,
		pd.CodUmed,
		detumed.desumed,
		tp.codumedvta1,
		tp.codumedvta2, 
		tp.codumed,
		tp.preciovtaum1,
		tp.preciovtaum1,
		lp.CodLista
	ORDER BY 
		DesProd ASC 
END

GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarProductoRapido]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_BuscarProductoRapido]
-- @DesProd   varchar(60),
 @CodRapido	varchar(3),
-- @CodSubGr  varchar(10),
 @CodLista	varchar(3)--,
-- @CodGrupo	varchar(10)
as
begin
SELECT
	tp.CodProd, 
	tp.DesProd, 
	tp.CodGrupo, 
	tp.CodSubGr, 
	pd.valorPct as PrecioVta, 
	pd.CodUmed as codumed, 
	detumed.desumed as desumed,
	lp.CodLista,
	ISNULL((
		select		Sum (CASE WHEN TipoBod = 'D' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible
		FROM		transporte.softland.IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro)) 
		WHERE		Fecha <= GETDATE()  
		and			CodProd = tp.CodProd 
		GROUP BY	CodProd
	), 0)--[dbo].[stock2018](tp.CodProd)  'Stock' 
FROM 
	[KUPPEL].[softland].[iw_tprod] AS tp 
	LEFT JOIN [KUPPEL].[softland].[iw_gmovi] AS gm ON tp.CodProd=gm.CodProd 
	LEFT JOIN [KUPPEL].[softland].[iw_tlprprod] AS pd ON tp.CodProd=pd.CodProd
	LEFT JOIN [KUPPEL].[softland].[iw_tlispre] AS lp ON pd.CodLista=lp.CodLista 
	LEFT JOIN [KUPPEL].[softland].[iw_tumed] AS detumed on pd.CodUmed = detumed.CodUMed
WHERE  
	tp.CodRapido =  @CodRapido 
	AND
	lp.CodLista = @CodLista
GROUP BY 
	tp.CodProd, 
	tp.DesProd, 
	tp.CodGrupo, 
	tp.CodSubGr, 
	tp.PrecioVta, 
	pd.ValorPct,
	pd.CodUmed,
	detumed.desumed,
	tp.codumedvta1,
	tp.codumedvta2, 
	tp.codumed,
	tp.preciovtaum1,
	tp.preciovtaum1,
	lp.CodLista
ORDER BY 
	DesProd ASC 
end



GO
/****** Object:  StoredProcedure [dbo].[FR_BuscarUsuarios]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_BuscarUsuarios]
 @id    int,
 @pv_BaseDatos varchar(100)
as
begin
	select usuario.Usuario,
	usuario.id, 
	usuario.email, 
	tipo.tipoUsuario
	,VenCod =	(
					select	top 1 
							sub_a.VenCod 
					from	ds_usuarioEmpresa sub_a 
						inner join ds_empresa sub_b 
							on sub_a.idempresa = sub_b.id 
					where	sub_b.basedatos = @pv_BaseDatos 
					and		sub_a.IdUsuario = @id
				)
	from [dbo].[DS_Usuarios] usuario
	inner join [dbo].[DS_UsuariosTipos] tipo on usuario.tipoUsuario = tipo.id
	where usuario.id = @id 

	--where clientes.RutAux ='76.387.390-0' and contacto.[NomCon] like '%jorge naza%'
end

GO
/****** Object:  StoredProcedure [dbo].[FR_EliminarUsuario]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_EliminarUsuario]							*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_EliminarUsuario]  
@Id int  
AS  
DECLARE @Estado int
SET @Estado	= (SELECT Estado FROM dbo.DS_Usuarios du WHERE du.ID = @Id)
IF(@Estado = 1)
BEGIN

	UPDATE dbo.DS_Usuarios SET dbo.DS_Usuarios.Estado = 0   
	WHERE dbo.DS_Usuarios.ID = @Id  

	SELECT	Verificador = Cast(1 as bit)
	,		Mensaje = 'Se Deshabilito usuario'   
END
ELSE
BEGIN
	UPDATE dbo.DS_Usuarios SET dbo.DS_Usuarios.Estado = 1   
	WHERE dbo.DS_Usuarios.ID = @Id  

	SELECT	Verificador = Cast(1 as bit)
	,		Mensaje = 'Se Habilito usuario'
END

GO
/****** Object:  StoredProcedure [dbo].[FR_ListaProductos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/  
/*-- Empresa   : DISOFI            */  
/*-- Tipo    : Procedimiento           */  
/*-- Nombre    : [dbo].[FR_ListaProductos]        */  
/*-- Detalle   :              */  
/*-- Autor    : FDURAN            */  
/*-- Modificaciones  :              */  
/*------------------------------------------------------------------------------*/  
CREATE PROCEDURE [dbo].[FR_ListaProductos] 
(  
 @pv_ListaProductos varchar(max)  
, @pv_BaseDatos varchar(100)  
)  
AS    
BEGIN    
 declare @query varchar(max)  
  
 select @query = ''  

 declare @lv_bodega varchar(50)

select	top 1 
		@lv_bodega = 
				case	when a.stockproductoesbodega = 1 
							then isnull(stockproductocodigobodega, '') 
							else '' 
				end 
from	ds_parametros a 
	inner join ds_empresa b 
		on a.idempresa = b.id 
where	b.basedatos = @pv_BaseDatos
  
 if @pv_ListaProductos is not null and rtrim(ltrim(@pv_ListaProductos)) <> '' and rtrim(ltrim(@pv_ListaProductos)) <> '-1' begin  
  select @query = @query + '  
  --CON LISTA DE PRECIO  
  SELECT  DISTINCT     
     CodProd = tp.CodProd  
  ,   DesProd = ISNULL(tp.DesProd,'''')  
  ,   codgrupo = ISNULL(tp.CodGrupo,'''')  
  ,   codsubgr = ISNULL(tp.CodSubGr,'''')  
  ,   PrecioVta = pd.valorPct  
  ,   codumed = pd.CodUmed  
  ,   desumed = ISNULL(detumed.desumed,'''')  
  ,   CodLista = lp.CodLista  
  ,   Stock =   
       ISNULL((  
        select  Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible  
        FROM  [' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro))   
        WHERE  Fecha <= GETDATE()    
        and   CodProd = tp.CodProd   ' +
		case when @lv_bodega = '' then '' else ' and codbode = ''' + @lv_bodega + '''' end + '
        GROUP BY CodProd  
       ), 0)--[dbo].[stock2018](tp.CodProd)    
  FROM  [' + @pv_BaseDatos + '].[softland].[iw_tprod] AS tp     
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_gmovi] AS gm ON tp.CodProd = gm.CodProd     
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tlprprod] AS pd ON tp.CodProd = pd.CodProd    
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tlispre] AS lp ON pd.CodLista = lp.CodLista     
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tumed] AS detumed on pd.CodUmed = detumed.CodUMed    
  where  lp.CodLista = ''' + @pv_ListaProductos + '''  
  and   tp.Inactivo = 0  
  GROUP BY tp.CodProd,   tp.DesProd,   tp.CodGrupo, tp.CodSubGr  
  ,   tp.PrecioVta,  pd.ValorPct,  pd.CodUmed,  detumed.desumed  
  ,   tp.codumedvta1,  tp.codumedvta2,  tp.codumed,  tp.preciovtaum1  
  ,   tp.preciovtaum1, lp.CodLista  
  ORDER BY DesProd ASC  
  '  
 end  
 else begin  
  select @query = @query + '  
  --SIN LISTA DE PRECIO  
  SELECT  DISTINCT     
     CodProd = tp.CodProd  
  ,   DesProd = ISNULL(tp.DesProd,'''')  
  ,   codgrupo = ISNULL(tp.CodGrupo,'''')  
  ,   codsubgr = ISNULL(tp.CodSubGr,'''')  
  ,   PrecioVta = tp.PrecioVta  
  ,   codumed = tp.CodUmed  
  ,   desumed = ISNULL(detumed.desumed,'''')  
  ,   CodLista = ''''  
  ,   Stock = ISNULL((  
      select  Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible  
      FROM  [' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro))   
      WHERE  Fecha <= GETDATE()  
      and   CodProd = tp.CodProd   ' +
		case when @lv_bodega = '' then '' else ' and codbode = ''' + @lv_bodega + '''' end + '
      GROUP BY CodProd  
     ), 0)--Stock = [dbo].[stock2018](tp.CodProd)    
  FROM  [' + @pv_BaseDatos + '].[softland].[iw_tprod] AS tp     
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_gmovi] AS gm ON tp.CodProd = gm.CodProd     
   --LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tlprprod] AS pd ON tp.CodProd = pd.CodProd    
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tumed] AS detumed on tp.CodUmed = detumed.CodUMed    
  where  tp.Inactivo = 0  
  GROUP BY tp.CodProd,   tp.DesProd,   tp.CodGrupo, tp.CodSubGr  
  ,   tp.PrecioVta,  tp.PrecioVta,  tp.CodUmed,  detumed.desumed  
  ,   tp.codumedvta1,  tp.codumedvta2,  tp.codumed,  tp.preciovtaum1  
  ,   tp.preciovtaum1  
  ORDER BY DesProd ASC  
  '  
  /*  
  Msg 4104, Level 16, State 1, Line 18  
El identificador formado por varias partes "pd.ValorPct" no se pudo enlazar.  
Msg 4104, Level 16, State 1, Line 18  
El identificador formado por varias partes "pd.CodUmed" no se pudo enlazar.  
Msg 4104, Level 16, State 1, Line 18  
El identificador formado por varias partes "detumed.desumed" no se pudo enlazar.  
Msg 4104, Level 16, State 1, Line 8  
El identificador formado por varias partes "pd.valorPct" no se pudo enlazar.  
Msg 4104, Level 16, State 1, Line 9  
El identificador formado por varias partes "pd.CodUmed" no se pudo enlazar.  
Msg 4104, Level 16, State 1, Line 10  
El identificador formado por varias partes "detumed.desumed" no se pudo enlazar.  
  */  
 end  
  
 exec  (@query)  
END    

GO
/****** Object:  StoredProcedure [dbo].[FR_ListaProductosPorGrupo]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FR_ListaProductosPorGrupo]
(  
 @pv_ListaProductos varchar(max)  
, @pv_BaseDatos varchar(100)
, @pv_codgrupo varchar (10)
)  
AS    
BEGIN    
 declare @query varchar(max)  
  
 select @query = ''  

 declare @lv_bodega varchar(50)

select	top 1 
		@lv_bodega = 
				case	when a.stockproductoesbodega = 1 
							then isnull(stockproductocodigobodega, '') 
							else '' 
				end 
	from	ds_parametros a with(nolock)
	inner join ds_empresa b with(nolock)  on a.idempresa = b.id 
where	b.basedatos = @pv_BaseDatos
  
 if @pv_ListaProductos is not null and rtrim(ltrim(@pv_ListaProductos)) <> '' and rtrim(ltrim(@pv_ListaProductos)) <> '-1' begin  
  select @query = @query + '  
  --CON LISTA DE PRECIO  
  SELECT  DISTINCT     
     CodProd = tp.CodProd  
  ,   DesProd = ISNULL(tp.DesProd,'''')  
  ,   CodGrupo = ISNULL(tp.CodGrupo,'''')  
  ,   codsubgr = ISNULL(tp.CodSubGr,'''')  
  ,   PrecioVta = pd.valorPct  
  ,   codumed = pd.CodUmed  
  ,   desumed = ISNULL(detumed.desumed,'''')  
  ,   CodLista = lp.CodLista  
  ,   Stock =   
       ISNULL((  
        select  Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible  
        FROM  [' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro))   
        WHERE  Fecha <= GETDATE()    
        and   CodProd = tp.CodProd   ' +
		case when @lv_bodega = '' then '' else ' and codbode = ''' + @lv_bodega + '''' end + '
        GROUP BY CodProd  
       ), 0)--[dbo].[stock2018](tp.CodProd)    
  FROM  [' + @pv_BaseDatos + '].[softland].[iw_tprod] AS tp  with(nolock)     
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_gmovi] AS gm  with(nolock) ON tp.CodProd = gm.CodProd     
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tlprprod] AS pd   with(nolock) ON tp.CodProd = pd.CodProd    
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tlispre] AS lp  with(nolock) ON pd.CodLista = lp.CodLista     
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tumed] AS detumed  with(nolock) on pd.CodUmed = detumed.CodUMed    
  where  lp.CodLista = ''' + @pv_ListaProductos + '''  
  and	tp.codgrupo = ''' + @pv_codgrupo + '''  
  and   tp.Inactivo = 0  
  GROUP BY tp.CodProd,   tp.DesProd,   tp.CodGrupo, tp.CodSubGr  
  ,   tp.PrecioVta,  pd.ValorPct,  pd.CodUmed,  detumed.desumed  
  ,   tp.codumedvta1,  tp.codumedvta2,  tp.codumed,  tp.preciovtaum1  
  ,   tp.preciovtaum1, lp.CodLista  
  ORDER BY DesProd ASC  
  '  
 end  
 else begin  
  select @query = @query + '  
  --SIN LISTA DE PRECIO  
  SELECT  DISTINCT     
     CodProd = tp.CodProd  
  ,   DesProd = ISNULL(tp.DesProd,'''')  
  ,   CodGrupo = ISNULL(tp.CodGrupo,'''')  
  ,   codsubgr = ISNULL(tp.CodSubGr,'''')  
  ,   PrecioVta = tp.PrecioVta  
  ,   codumed = tp.CodUmed  
  ,   desumed = ISNULL(detumed.desumed,'''')  
  ,   CodLista = ''''  
  ,   Stock = ISNULL((  
      select  Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible  
      FROM  [' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro))   
      WHERE  Fecha <= GETDATE()  
      and   CodProd = tp.CodProd   ' +
		case when @lv_bodega = '' then '' else ' and codbode = ''' + @lv_bodega + '''' end + '
      GROUP BY CodProd  
     ), 0)--Stock = [dbo].[stock2018](tp.CodProd)    
  FROM  [' + @pv_BaseDatos + '].[softland].[iw_tprod] AS tp   with(nolock)    
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_gmovi] AS gm  with(nolock) ON tp.CodProd = gm.CodProd     
   --LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tlprprod] AS pd  with(nolock) ON tp.CodProd = pd.CodProd    
   LEFT JOIN [' + @pv_BaseDatos + '].[softland].[iw_tumed] AS detumed  with(nolock) on tp.CodUmed = detumed.CodUMed    
  where  tp.Inactivo = 0
  and	tp.codgrupo = ''' + @pv_codgrupo + '''  
  GROUP BY tp.CodProd,   tp.DesProd,   tp.CodGrupo, tp.CodSubGr  
  ,   tp.PrecioVta,  tp.PrecioVta,  tp.CodUmed,  detumed.desumed  
  ,   tp.codumedvta1,  tp.codumedvta2,  tp.codumed,  tp.preciovtaum1  
  ,   tp.preciovtaum1  
  ORDER BY DesProd ASC  
  '  

 end  
  
 exec  (@query)  
END    



GO
/****** Object:  StoredProcedure [dbo].[FR_ListarCentroDeCosto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ListarCentroDeCosto]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE procedure [dbo].[FR_ListarCentroDeCosto]
(
	@pv_BaseDatos VARCHAR(100)
)
AS
BEGIN
	declare @query varchar(max)

	select @query = ''

	-- ==========================================================================================  
	-- Lista los clientes filtrados por Codigo Aux y RUT Aux  
	-- ==========================================================================================  
	select @query = @query + '
		select	* 
		from	[' + @pv_BaseDatos + '].[softland].[cwtccos] 
		where	activo = ''S'' 
		and		DescCC != '''' --and CodiCC = 002  
	'

	exec (@query)
end  

GO
/****** Object:  StoredProcedure [dbo].[FR_ListarClientes]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_ListarClientes]
	@pv_BaseDatos varchar(100)
	as
	declare @query varchar(max)
	select @query = ''

	select @query = @query + '
	select clientes.CodAux,
	clientes.[NomAux],
	clientes.[RutAux],
	clientes.[DirAux],
	clientes.[DirNum],
	contacto.[NomCon], 
	contacto.[FonCon]
    from ['+@pv_BaseDatos+'].[softland].[cwtauxi] clientes
	inner join ['+@pv_BaseDatos+'].[softland].[cwtaxco] contacto on clientes.CodAux = contacto.CodAuc 

	UNION
	select clientes.CodAux collate SQL_Latin1_General_CP1_CI_AS,
	clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AS,
	clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AS,
	clientes.[DirAux] collate SQL_Latin1_General_CP1_CI_AS,
	clientes.[DirNum] collate SQL_Latin1_General_CP1_CI_AS,
	''''[NomCon], 
	''''[FonCon]
	from  DS_cwtauxi clientes where sincronizado=0
	'
	exec (@query)
	



GO
/****** Object:  StoredProcedure [dbo].[FR_ListarClientesTodos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_ListarClientesTodos]

as
begin

SELECT dir.CodAux, dir.NomAux, dir.DirAux, dir.DirNum, ven.NomCon, 
CASE	WHEN ven.FonCon != null	THEN ven.FonCon
		WHEN ven.FonCon != null	THEN ven.FonCon
		ELSE ven.FonCon
		END AS 'FonAux1',
		dir.Notas      
FROM [KUPPEL].softland.cwtauxi dir
inner join [KUPPEL].softland.cwtaxco ven ON ven.CodAuc = dir.CodAux 
ORDER BY dir.CodAux ASC

	
end


GO
/****** Object:  StoredProcedure [dbo].[FR_ListarCondicionesDeVenta]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[FR_ListarCondicionesDeVenta]
(
	@CodAux varchar(15) 
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
	declare @query varchar(max)

	select @query = ''

	-- ==========================================================================================  
	-- Lista los clientes filtrados por Codigo Aux y RUT Aux  
	-- ==========================================================================================  

	if(@CodAux = '-2') begin
	select @query = @query + '
	select	distinct
			condicion.CodAux
	,		condicion.ConVta
	from	[' + @pv_BaseDatos + '].[softland].cwtcvcl condicion 
	where ConVta <> ''''
		'
	end 
	else begin
	select @query = @query + '
	select	distinct
			ven.CveDes
	,		condicion.ConVta
	--,		cliente.CodAux 
	from	[' + @pv_BaseDatos + '].[softland].[cwtauxi] cliente   
		INNER JOIN [' + @pv_BaseDatos + '].[softland].cwtcvcl condicion 
			ON cliente.CodAux = condicion.CodAux  
		INNER JOIN [' + @pv_BaseDatos + '].[softland].cwtconv ven 
			ON condicion.ConVta = ven.CveCod  
	WHERE	condicion.ConVta != ''''
	AND		condicion.ConVta is NOT null   
	' + CASE WHEN @codaux = '-1' THEN '' ELSE
	'AND	cliente.CodAux = ''' + @CodAux + '''' end + '
	
	union 

	select ''Sin Condicion de Venta'' CveDes, ''SC''ConVta
	order by CveDes
	'
	end
	exec(@query)
end 
GO
/****** Object:  StoredProcedure [dbo].[FR_ListarCondicionesDeVentaPret]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ListarCondicionesDeVenta]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE Procedure [dbo].[FR_ListarCondicionesDeVentaPret]
(
	@CodAux varchar(15) 
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
	declare @query varchar(max)

	select @query = ''

	-- ==========================================================================================  
	-- Lista los clientes filtrados por Codigo Aux y RUT Aux  
	-- ==========================================================================================  
	select @query = @query + '
	select	distinct
			ven.CveDes
	,		condicion.ConVta
	--,		cliente.CodAux 
	from	[' + @pv_BaseDatos + '].[softland].[cwtauxi] cliente   
		INNER JOIN [' + @pv_BaseDatos + '].[softland].cwtcvcl condicion 
			ON cliente.CodAux = condicion.CodAux  
		INNER JOIN [' + @pv_BaseDatos + '].[softland].cwtconv ven 
			ON condicion.ConVta = ven.CveCod  
	WHERE	condicion.ConVta != ''''
	AND		condicion.ConVta is NOT null
	and ven.cvecod in (''001'',''002'')
	order by ven.CveDes'
	
	
	exec(@query)
end  

GO
/****** Object:  StoredProcedure [dbo].[FR_ListarContactos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
CREATE procedure [dbo].[FR_ListarContactos]  
	@CodAuc varchar(10)  
,	@NomCon varchar(30)
,	@pv_BaseDatos varchar(100)  
as  
BEGIN  
	declare @query varchar(max)
	select @query = ''
	declare @sincronizado int

	select @sincronizado=count(*) from DS_cwtauxi where CodAux=@CodAuc and sincronizado=0
	if @sincronizado is null
	   set @sincronizado=0

	if @sincronizado>0
	begin
	select @query = @query + '
	SELECT	ven.CodAuc as CodAux
	,		ven.NomCon       
	FROM	ds_cwtauxi dir 
		INNER JOIN DS_cwtaxco ven 
			ON ven.CodAuc = dir.CodAux   
	WHERE	CodAuc = ''' + @CodAuc + '''
	'
	end

	if @sincronizado=0
	begin
	select @query = @query + '
	SELECT	ven.CodAuc as CodAux
	,		ven.NomCon       
	FROM	[' + @pv_BaseDatos + '].[softland].[cwtauxi] dir 
		INNER JOIN [' + @pv_BaseDatos + '].[softland].[cwtaxco] ven 
			ON ven.CodAuc = dir.CodAux   
	WHERE	CodAuc = ''' + @CodAuc + '''
	'
	end
	EXEC (@query)
END
GO
/****** Object:  StoredProcedure [dbo].[FR_ListarDocumentosAprobados]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[FR_ListarDocumentosAprobados]
(
	@pv_BaseDatos varchar (100)
,	@pi_IdEmpresaInterna int
,	@pv_CodigoVendedor varchar(100)
)
AS
	DECLARE @query varchar(max)
	SELECT @query = ''
	SELECT @query = @query + '
	select 
	a.Id,
	a.NVNumero, 
	clientes.[NomAux], 
	clientes.[RutAux], 
	a.nvFem, 
	a.CodLista,
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	ISNULL(a.RutSolicitante,0) as RutSolicitante,
	vend.VenDes,
	a.ErrorAprobador,
	a.ErrorAprobadorMensaje,
	isnull(DS_Usuarios.Nombre,'' '')UsuarioAprobador

	FROM    DS_Usuarios RIGHT OUTER JOIN
        DS_NotaVentaExtras ON DS_Usuarios.ID = DS_NotaVentaExtras.IdUsuario RIGHT OUTER JOIN
        DS_NotasVenta AS a INNER JOIN
        ['+@pv_BaseDatos+'].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux ON DS_NotaVentaExtras.IdNotaVenta = a.Id LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod

	where	a.IdEmpresaInterna = ' + convert(varchar(20), @pi_IdEmpresaInterna) + '
	and		a.EstadoNP = ''A''
	' + case when @pv_CodigoVendedor is not null and @pv_CodigoVendedor <> '-1' then 'and	a.VenCod = ''' + @pv_CodigoVendedor + '''' else '' end + '
	order by a.Id desc
'
EXEC (@query)
GO
/****** Object:  StoredProcedure [dbo].[FR_ListarDocumentosPendientes]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[FR_ListarDocumentosPendientes]
(
	@pv_BaseDatos varchar (100)
,	@pi_IdEmpresaInterna int
,	@pv_CodigoVendedor varchar(100)
)
AS
	DECLARE @query varchar (max)
	SELECT @query = ''
	SELECT @query = @query + '
	
	select	distinct 
	a.NVNumero,
	a.Id,
	clientes.[NomAux] collate Modern_Spanish_CI_AS NomAux,
	clientes.[RutAux] collate Modern_Spanish_CI_AS RutAux,
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.CodLista, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.CodAux,
	a.EstadoNP, 
	a.nvSubTotal,
	ISNULL(a.RutSolicitante,0) as RutSolicitante,
	vend.VenDes,
	ErrorAprobador,
	ErrorAprobadorMensaje
	from [dbo].[DS_NotasVenta] a
	inner join [DS_cwtauxi] clientes on  clientes.CodAux collate Modern_Spanish_CI_AS = a.CodAux 
	LEFT JOIN [dbo].[DS_NotasVentaDetalle] b on a.NVNumero = b.NVNumero
	LEFT JOIN ['+ @pv_BaseDatos +'].[softland].[iw_tprod] AS tp on b.CodProd = tp.CodProd collate SQL_Latin1_General_CP1_CI_AS
	left join [dbo].[DS_NotasVentaDetalle] c on a.Id = c.IdNotaVenta
	left join ['+ @pv_BaseDatos +'].[softland].cwtvend vend on vend.VenCod collate Modern_Spanish_CI_AS = a.VenCod
	where	a.IdEmpresaInterna = ' + convert(varchar(20), @pi_IdEmpresaInterna) + '
	and		a.EstadoNP = ''P''
	' + case when @pv_CodigoVendedor is not null and @pv_CodigoVendedor <> '-1' then 'and	a.VenCod = ''' + @pv_CodigoVendedor + '''' else '' end + '
	and clientes.sincronizado=0

	union 

	select	distinct 	
	a.NVNumero,
	a.Id,
	clientes.[NomAux],
	clientes.[RutAux],
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.CodLista, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.CodAux,
	a.EstadoNP, 
	a.nvSubTotal,
	ISNULL(a.RutSolicitante,0) as RutSolicitante,
	vend.VenDes,
	ErrorAprobador,
	ErrorAprobadorMensaje
	from [dbo].[DS_NotasVenta] a
	inner join ['+ @pv_BaseDatos +'].[softland].[cwtauxi] clientes on  clientes.CodAux collate Modern_Spanish_CI_AS = a.CodAux 
	LEFT JOIN [dbo].[DS_NotasVentaDetalle] b on a.NVNumero = b.NVNumero
	LEFT JOIN ['+ @pv_BaseDatos +'].[softland].[iw_tprod] AS tp on b.CodProd = tp.CodProd collate SQL_Latin1_General_CP1_CI_AS
	left join [dbo].[DS_NotasVentaDetalle] c on a.Id = c.IdNotaVenta
	left join ['+ @pv_BaseDatos +'].[softland].cwtvend vend on vend.VenCod collate Modern_Spanish_CI_AS = a.VenCod
	where	a.IdEmpresaInterna = ' + convert(varchar(20), @pi_IdEmpresaInterna) + '
	and		a.EstadoNP = ''P''
	' + case when @pv_CodigoVendedor is not null and @pv_CodigoVendedor <> '-1' then 'and	a.VenCod = ''' + @pv_CodigoVendedor + '''' else '' end + '
	order by a.Id desc
'
PRINT @query
exec (@query)

--[FR_ListarDocumentosPendientes] 'transporte'

GO
/****** Object:  StoredProcedure [dbo].[FR_ListarDocumentosRechazadas]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[FR_ListarDocumentosRechazadas]
(
	@pv_BaseDatos varchar (100)
,	@pi_IdEmpresaInterna int
,	@pv_CodigoVendedor varchar(100)
)
AS
	DECLARE @query varchar(max)
	SELECT @query = ''
	SELECT @query = @query + '
	select 
	a.Id,
	a.NVNumero, 
	clientes.[NomAux], 
	clientes.[RutAux], 
	a.nvFem, 
	a.CodLista,
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	A.nvSubTotal,
	ISNULL(a.RutSolicitante,0) as RutSolicitante,
	vend.VenDes,
	a.ErrorAprobador,
	a.ErrorAprobadorMensaje
	from [dbo].[DS_NotasVenta] a
	inner join ['+@pv_BaseDatos+'].[softland].[cwtauxi] clientes on  clientes.CodAux collate Modern_Spanish_CI_AS = a.CodAux 
	left join ['+ @pv_BaseDatos +'].[softland].cwtvend vend on vend.VenCod collate Modern_Spanish_CI_AS = a.VenCod
	where	a.IdEmpresaInterna = ' + convert(varchar(20), @pi_IdEmpresaInterna) + '
	and		a.EstadoNP = ''R''
	' + case when @pv_CodigoVendedor is not null and @pv_CodigoVendedor <> '-1' then 'and	a.VenCod = ''' + @pv_CodigoVendedor + '''' else '' end + '
	order by a.Id desc
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[FR_ListarListaDePrecio]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ListarListaDePrecio]							*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE procedure [dbo].[FR_ListarListaDePrecio]  
(
	@CodAux varchar(15)  
,	@pv_BaseDatos varchar(100)
)
as  
BEGIN
	declare @query varchar(max)

	select @query = ''

	select @query = @query + '
	DECLARE @contar  int  
  
	SET @contar =	(
						SELECT	count(*)  
						FROM	[' + @pv_BaseDatos + '].[softland].[cwtauxi] cliente 
									INNER JOIN [' + @pv_BaseDatos + '].[softland].cwtcvcl condicion 
										on cliente.CodAux = condicion.CodAux 
									INNER JOIN [' + @pv_BaseDatos + '].[softland].iw_tlispre lista 
										on condicion.codlista = lista.CodLista  
						WHERE	condicion.codlista !='''' 
						AND		condicion.codlista IS NOT NULL 
						AND		cliente.CodAux = ''' + @CodAux + '''
					)  
	IF(@contar = 0)	BEGIN  
		Select	Codlista = CodLista
		,		DesLista = DesLista
		from	[' + @pv_BaseDatos + '].[softland].iw_tlispre 
		where	codlista = ''001''  
	END
	ELSE BEGIN
		Select	Codlista = condicion.codlista
		,		DesLista = lista.DesLista
		,		CodAux = condicion.codaux   
		FROM	[' + @pv_BaseDatos + '].[softland].[cwtauxi] cliente 
			INNER JOIN [' + @pv_BaseDatos + '].[softland].cwtcvcl condicion 
				on cliente.CodAux = condicion.CodAux 
			INNER JOIN [' + @pv_BaseDatos + '].[softland].iw_tlispre lista 
				on condicion.codlista = lista.CodLista
		WHERE	condicion.codlista != ''''
		AND		condicion.codlista IS NOT NULL 
		AND		cliente.CodAux = ''' + @CodAux + '''
	END
	'

	EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[FR_ListarUsuarios]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_ListarUsuarios]
	as
	select usuario.Usuario,
	usuario.id,
	usuario.email,
	usuario.Nombre,
	tipo.tipoUsuario,
	usuario.Estado	
	from [dbo].[DS_Usuarios] usuario
	inner join [dbo].[DS_UsuariosTipos] tipo on usuario.tipoUsuario = tipo.id
	


GO
/****** Object:  StoredProcedure [dbo].[FR_ListarUsuariosTipos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_ListarUsuariosTipos]
	as
	SELECT [ID]
      ,[tipoUsuario]
      ,[urlInicio]
  FROM [dbo].[DS_UsuariosTipos]



GO
/****** Object:  StoredProcedure [dbo].[FR_ListarVendedorSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FR_ListarVendedorSoftland]
	@venCod VARCHAR(8)
AS
BEGIN
		IF NOT EXISTS (SELECT VenCod FROM [KUPPEL].[Softland].[cwtvend] WHERE VenCod=@venCod)
			SET @venCod=''

		SELECT
			VenCod = vendedor.VenCod,
			VenDes = vendedor.VenCod + ' - ' + vendedor.VenDes,
			CodTipV = ISNULL(vendedor.CodTipV,''),
			EMail = ISNULL(vendedor.EMail,''),
			Usuario = ISNULL(vendedor.Usuario,'')
		FROM
			[KUPPEL].[Softland].[cwtvend] vendedor
		WHERE
			vendedor.VenCod = (CASE @venCod WHEN '' THEN vendedor.VenCod ELSE @venCod END)
END

GO
/****** Object:  StoredProcedure [dbo].[FR_ListarVendedorSoftland2]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FR_ListarVendedorSoftland2]
@pv_BaseDatos varchar(100)
AS
DECLARE @query varchar(max)

SELECT @query = ''

SELECT @query = @query + '
		SELECT
			VenCod = vendedor.VenCod,
			VenDes = vendedor.VenCod + '' - '' + vendedor.VenDes,
			CodTipV = ISNULL(vendedor.CodTipV,''''),
			EMail = ISNULL(vendedor.EMail,''''),
			Usuario = ISNULL(vendedor.Usuario,'''')
		FROM
			['+@pv_BaseDatos+'].[Softland].[cwtvend] vendedor
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[FR_ListaTallaColorProducto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ListaTallaColorProducto]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_ListaTallaColorProducto]
(
	@pv_CodProd varchar(max)
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
	declare @query varchar(max)

	select @query = ''
	select @query = @query + '
	SELECT		CodigoBodega = codbode
	,			CodigoProducto = codprod
	,			Talla = partida
	,			Color = pieza
	,			CantidadBodega = ISNULL(CONVERT(INT, sum(ingresos-egresos)), 0)
	FROM		[' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod  
	where		codprod = ''' + @pv_CodProd + '''
	and			partida is not null
	and			pieza is not null
	group by	codprod, codbode, partida, pieza
	'


	exec (@query)
END  

GO
/****** Object:  StoredProcedure [dbo].[FR_ModificarNVCabecera]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_ModificarNVCabecera]

@NVNumero int,

@nvFem datetime,
@nvEstado varchar(1),
@nvEstFact int,
@nvEstDesp int,
@nvEstRese int,
@nvEstConc int,
@nvFeEnt datetime,
@CodAux varchar(10),
@VenCod varchar(4),
@CodMon varchar(2),
@CodLista varchar(3),
@nvObser text,
@CveCod varchar(3),
@NomCon varchar(30),
@CodiCC varchar(8),
@nvSubTotal float,
@nvPorcDesc01 float,
@nvDescto01 float,
@nvPorcDesc02 float,
@nvDescto02 float,
@nvPorcDesc03 float,
@nvDescto03 float,
@nvPorcDesc04 float,
@nvDescto04 float,
@nvPorcDesc05 float,
@nvDescto05 float,
@nvMonto float,
@NumGuiaRes int,
@nvPorcFlete float,
@nvValflete float,
@nvPorcEmb float,
@nvEquiv float,
@nvNetoExento float,
@nvNetoAfecto float,
@nvTotalDesc float,
@ConcAuto varchar(1),
@CheckeoPorAlarmaVtas varchar(1),
@EnMantencion int,
@Usuario varchar(8),
@UsuarioGeneraDocto varchar(8),
@FechaHoraCreacion datetime,
@Sistema varchar(2),
@ConcManual varchar(1),
@proceso varchar(50),
@TotalBoleta float,
@NumReq int,
@CodVenWeb varchar(50),
@EstadoNP varchar(1),
@CodLugarDesp varchar(30)
as
begin
	update [DSNotaVenta].[dbo].[DS_NotasVenta]
	SET
	[nvFem]=@nvFem,
	[nvEstado]=@nvEstado,
	[nvEstFact]=@nvEstFact,
	[nvEstDesp]=@nvEstDesp,
	[nvEstRese]=@nvEstRese,
	[nvEstConc]=@nvEstConc,
	[nvFeEnt]=@nvFeEnt,
	[CodAux]=@CodAux,
	[VenCod]=@VenCod,
	[CodMon]=@CodMon,
	[CodLista]=@CodLista,
	[nvObser]=@nvObser,
	[CveCod]=@CveCod,
	[NomCon]=@NomCon,
	[CodiCC]=@CodiCC,
	[nvSubTotal]=@nvSubTotal,
	[nvPorcDesc01]=@nvPorcDesc01,
	[nvDescto01]=@nvDescto01,
	[nvPorcDesc02]=@nvPorcDesc02,
	[nvDescto02]=@nvDescto02,
	[nvPorcDesc03]=@nvPorcDesc03,
	[nvDescto03]=@nvDescto03,
	[nvPorcDesc04]=@nvPorcDesc04,
	[nvDescto04]=@nvDescto04,
	[nvPorcDesc05]=@nvPorcDesc05,
	[nvDescto05]=@nvDescto05,
	[nvMonto]=@nvMonto,
	[NumGuiaRes]=@NumGuiaRes,
	[nvPorcFlete]=@nvPorcFlete,
	[nvValflete]=@nvValflete,
	[nvPorcEmb]=@nvPorcEmb,
	[nvEquiv]=@nvEquiv,
	[nvNetoExento]=@nvNetoExento,
	[nvNetoAfecto]=@nvNetoAfecto,
	[nvTotalDesc]=@nvTotalDesc,
	[ConcAuto]=@ConcAuto,
	[CheckeoPorAlarmaVtas]=@CheckeoPorAlarmaVtas,
	[EnMantencion]=@EnMantencion,
	[Usuario]=@Usuario,
	[UsuarioGeneraDocto]=@UsuarioGeneraDocto,
	[FechaHoraCreacion]=@FechaHoraCreacion,
	[Sistema]=@Sistema,
	[ConcManual]=@ConcManual,
	[proceso]=@proceso,
	[TotalBoleta]=@TotalBoleta,
	[NumReq]=@NumReq,
	[CodVenWeb]=@CodVenWeb,
	[EstadoNP]=@EstadoNP,
	[CodLugarDesp]=@CodLugarDesp
	where 
	[NVNumero] = @NVNumero




	DECLARE @tableHTML  NVARCHAR(MAX) ; 
	declare @NVNumeroc varchar(20);
	declare @cliente varchar(max);
	declare @Usuarioc varchar(max);
	declare @direccion varchar(max);
	declare @fpedido varchar(max);
	declare @fentrega varchar(max);
	declare @Obser varchar(max);
	declare @total varchar(max);
	declare @correo varchar(max);
	declare @totalboleta1 varchar(max);
	declare @iva varchar(max);

	set @NVNumeroc = @NVNumero
	set @cliente = (select (cliente.CodAux+' - '+cliente.NomAux) as Cliente from [DSNotaVenta].dbo.[DS_NotasVenta] nv inner join [KUPPEL].[softland].[cwtauxi] cliente on cliente.CodAux = nv.CodAux  collate Modern_Spanish_CI_AS where nv.NVNumero = @NVNumeroc)
	set @Usuarioc = (select (vend.VenCod+' - '+vend.VenDes) as usuario from [DSNotaVenta].dbo.[DS_NotasVenta] nv inner join [KUPPEL].[softland].cwtvend vend on vend.VenCod = nv.VenCod  collate Modern_Spanish_CI_AS where nv.NVNumero = @NVNumeroc)
	set @direccion = (select CodLugarDesp from  [DSNotaVenta].dbo.[DS_NotasVenta] where NVNumero = @NVNumeroc)
	set @fpedido = (select CONVERT (char(10), nvFem, 103) as fecha from  [DSNotaVenta].dbo.[DS_NotasVenta] where NVNumero = @NVNumeroc)
	set @fentrega = (select CONVERT (char(10), nvFeEnt, 103) as nvFeEnt from [DSNotaVenta].dbo.[DS_NotasVenta] where NVNumero = @NVNumeroc)
	set @Obser = (select nvObser FROM [DSNotaVenta].dbo.[DS_NotasVenta] where NVNumero = @NVNumeroc)
	set @total = (select REPLACE(CONVERT(varchar, convert(money, nvNetoAfecto), 1),'.00','') as monto from [DSNotaVenta].dbo.[DS_NotasVenta] where NVNumero = @NVNumeroc)
	set @correo = 'frojas@disofi.cl;'
	set @totalboleta1 = (select REPLACE(CONVERT(varchar, convert(money, TotalBoleta), 1),'.00','') as monto from [DSNotaVenta].dbo.[DS_NotasVenta] where NVNumero=@NVNumeroc) 
	set @iva = (select REPLACE(CONVERT(varchar, convert(money, TotalBoleta-nvNetoAfecto), 1),'.00','') as monto from  [DSNotaVenta].dbo.[DS_NotasVenta] where NVNumero=@NVNumeroc) 
	

	SET @tableHTML = 
	
		N'<H1>Nota de Venta</H1>' + 
		N'<H4>Nº Int: '+@NVNumeroc+'</H4>'+ 
		N'<H4>Cliente: ' + @cliente +'</H4>'+
		N'<H4>Direccion: '+ @Direccion +'</H4>'+
		N'<H4>Fecha Pedido: ' + @fpedido +'</H4>'+
		N'<H4>Fecha Entrega: '+ @fentrega +'</H4>'+
		N'<H4>Observaciones: '+ @Obser+'</H4>'+
		N'<H4>Vendedor: '+ @Usuarioc +'</H4>'+
	
		N'<table border="1">' +  
		N'<tr>' +  
		N'<td>ID</td>'+
		N'<th nowrap="nowrap">Codigo Producto</th>'+
		N'<th>Detalle Producto</th>'+
		N'<th>Cantidad</th>'+
		N'<th>U. Medida</th>'+
		N'<th>Precio</th>'+
		N'<th>Sub-Total</th>'+
		N'<th>Desc.%</th>'+
		N'<th>Total</th>'+
		N'</tr>' +  
		CAST ( ( SELECT td = cast(a.nvLinea as int), '',  
						td = a.CodProd, '',  
						td = b.DesProd, '',  
						td = cast(a.nvCant as int), '',  
						td = a.CodUMed, '',  
						td = REPLACE(CONVERT(varchar, convert(money,  a.nvPrecio), 1),'.00',''), '',
						td = REPLACE(CONVERT(varchar, convert(money,  a.nvSubTotal), 1),'.00',''), '',
						td = REPLACE(CONVERT(varchar, convert(money,  a.nvDPorcDesc01), 1),'.00',''), '',
						td = REPLACE(CONVERT(varchar, convert(money,  a.nvTotLinea), 1),'.00','')
				from [DSNotaVenta].dbo.DS_NotasVentaDetalle as a
				inner join [KUPPEL].[softland].[iw_tprod] as b on b.CodProd = a.CodProd collate Modern_Spanish_CI_AS
				where a.NVNumero = @NVNumeroc 
				order by a.nvLinea
				  FOR XML PATH('tr'), TYPE   
		) AS NVARCHAR(MAX) ) +  

		N'<td colspan="8">Total</td>'+
		N'<td>'+@total+'</td>'+
		N'<tr><td colspan="8">Iva</td>'+
		N'<td>'+@iva+'</td></tr>'+
		N'<tr><td colspan="8">Total</td>'+
		N'<td>'+@totalboleta1+'</td></tr>'+
		N'</table>' ;   


	EXEC  msdb.dbo.sp_send_dbmail @profile_name='Sistema' , 
	@recipients= @correo ,
	@subject= 'Nota de Venta',
	@body = @tableHTML,  
	@body_format = 'HTML' ;





end


GO
/****** Object:  StoredProcedure [dbo].[FR_ModificarParametrosUsuarios]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[FR_ModificarParametrosUsuarios]  
(
	@pi_IdEmpresa INT
,	@pb_MultiEmpresa BIT
,	@pb_ManejaAdministrador BIT
,	@pb_ManejaAprobador BIT
,	@pb_ListaClientesVendedor BIT
,	@pb_ListaClientesTodos BIT
,	@pb_ValidaReglasNegocio BIT
,	@pb_ManejaListaPrecios BIT
,	@pb_EditaPrecioProducto BIT
,	@pb_MuestraCondicionVentaCliente BIT
,	@pb_MuestraCondicionVentaTodos BIT
,	@pb_EditaDescuentoProducto BIT
,	@pd_MaximoDescuentoProducto DECIMAL(18, 2)
,	@pb_CantidadDescuentosProducto INT
,	@pb_MuestraStockProducto BIT
,	@pb_StockProductoEsMasivo BIT
,	@pb_StockProductoEsBodega BIT
,	@pv_StockProductoCodigoBodega VARCHAR(1000)
,	@pv_StockProductoCodigoBodegaAdicional VARCHAR(1000)
,	@pb_ControlaStockProducto BIT
,	@pb_EnvioMailCliente BIT
,	@pb_EnvioMailVendedor BIT
,	@pb_EnvioMailContacto BIT
,	@pb_EnvioObligatorioAprobador BIT 
,	@pb_ManejaTallaColor BIT 
,	@pb_CambioVendedorCliente BIT
,	@pb_ManejaDescuentoTotalDocumento BIT 
,	@pi_CantidadDescuentosTotalDocumento INT
,	@pi_CantidadLineas INT
,	@pb_ManejaLineaCreditoVendedor BIT 
,	@pb_ManejaLineaCreditoAprobador BIT 
,	@pb_ManejaCanalVenta BIT 
,	@pb_CreacionNotaVentaUsuariosBloqueados BIT 
,	@pb_CreacionNotaVentaUsuariosInactivos BIT 
,	@pb_PermiteModificacionCondicionVenta BIT 
,	@pv_AtributoSoftlandDescuentoCliente VARCHAR(1000)
,	@pb_PermiteCrearDireccion BIT 
,	@pb_CrearClienteConDV BIT 
,	@pb_MuestraUnidadMedidaProducto BIT 
,	@pb_DescuentoLineaDirectoSoftland BIT 
,	@pb_DescuentoTotalDirectoSoftland BIT 
,	@pb_AgregaCliente BIT 
,	@pb_EnvioMailAprobador BIT 
,	@pb_ManejaSaldo BIT
,	@pv_CodigoCondicionVentaPorDefecto VARCHAR(1000)
,	@pb_ManejaValorAdicional BIT
,	@pb_CorreosWebConfig BIT
,	@pb_Booking BIT
,	@pb_Backlog BIT 
--,	@pb_ManejaClasificacionCliente BIT
--,	@pb_ManejaClasificacionProveedor BIT
--,	@pv_CodigoCentroCostoPorDefecto VARCHAR(1000)
--,	@pv_CodigoBodegaWMSPorDefecto VARCHAR(1000)
)
as  
begin
	UPDATE	[dbo].[DS_Parametros]  
	SET		MultiEmpresa = @pb_MultiEmpresa
	,		ManejaAdministrador = @pb_ManejaAdministrador
	,		ManejaAprobador = @pb_ManejaAprobador
	,		ListaClientesVendedor = @pb_ListaClientesVendedor
	,		ListaClientesTodos = @pb_ListaClientesTodos
	,		ValidaReglasNegocio = @pb_ValidaReglasNegocio
	,		ManejaListaPrecios = @pb_ManejaListaPrecios
	,		EditaPrecioProducto = @pb_EditaPrecioProducto
	,		MuestraCondicionVentaCliente = @pb_MuestraCondicionVentaCliente
	,		MuestraCondicionVentaTodos = @pb_MuestraCondicionVentaTodos
	,		EditaDescuentoProducto = @pb_EditaDescuentoProducto
	,		MaximoDescuentoProducto = @pd_MaximoDescuentoProducto
	,		CantidadDescuentosProducto = @pb_CantidadDescuentosProducto
	,		MuestraStockProducto = @pb_MuestraStockProducto
	,		StockProductoEsMasivo = @pb_StockProductoEsMasivo
	,		StockProductoEsBodega = @pb_StockProductoEsBodega
	,		StockProductoCodigoBodega = @pv_StockProductoCodigoBodega
	,		StockProductoCodigoBodegaAdicional = @pv_StockProductoCodigoBodegaAdicional
	,		ControlaStockProducto = @pb_ControlaStockProducto
	,		EnvioMailCliente = @pb_EnvioMailCliente
	,		EnvioMailVendedor = @pb_EnvioMailVendedor
	,		EnvioMailContacto = @pb_EnvioMailContacto
	,		EnvioObligatorioAprobador = @pb_EnvioObligatorioAprobador
	,		ManejaTallaColor = @pb_ManejaTallaColor
	,		CambioVendedorCliente = @pb_CambioVendedorCliente
	,		ManejaDescuentoTotalDocumento = @pb_ManejaDescuentoTotalDocumento
	,		CantidadDescuentosTotalDocumento = @pi_CantidadDescuentosTotalDocumento
	,		CantidadLineas = @pi_CantidadLineas
	,		ManejaLineaCreditoVendedor = @pb_ManejaLineaCreditoVendedor
	,		ManejaLineaCreditoAprobador = @pb_ManejaLineaCreditoAprobador
	,		ManejaCanalVenta = @pb_ManejaCanalVenta
	,		CreacionNotaVentaUsuariosBloqueados = @pb_CreacionNotaVentaUsuariosBloqueados
	,		CreacionNotaVentaUsuariosInactivos = @pb_CreacionNotaVentaUsuariosInactivos
	,		PermiteModificacionCondicionVenta = @pb_PermiteModificacionCondicionVenta
	,		AtributoSoftlandDescuentoCliente = @pv_AtributoSoftlandDescuentoCliente
	,		PermiteCrearDireccion = @pb_PermiteCrearDireccion
	,		CrearClienteConDV = @pb_CrearClienteConDV
	,		MuestraUnidadMedidaProducto = @pb_MuestraUnidadMedidaProducto
	,		DescuentoLineaDirectoSoftland = @pb_DescuentoLineaDirectoSoftland
	,		DescuentoTotalDirectoSoftland = @pb_DescuentoTotalDirectoSoftland
	,		AgregaCliente = @pb_AgregaCliente
	,		EnvioMailAprobador = @pb_EnvioMailAprobador	
	,		ManejaSaldo = @pb_ManejaSaldo
	,		CodigoCondicionVentaPorDefecto = @pv_CodigoCondicionVentaPorDefecto
	,		ManejaValorAdicional = @pb_ManejaValorAdicional
	,		CorreosWebConfig = @pb_CorreosWebConfig
	,		Booking = @pb_Booking
	,		Backlog = @pb_Backlog
	--,		ManejaClasificacionCliente = @pb_ManejaClasificacionCliente 
	--,		ManejaClasificacionProveedor = @pb_ManejaClasificacionProveedor 
	--,		CodigoCentroCostoPorDefecto = @pv_CodigoCentroCostoPorDefecto
	--,		CodigoBodegaWMSPorDefecto = @pv_CodigoBodegaWMSPorDefecto
	WHERE	IdEmpresa = @pi_IdEmpresa
	
	SELECT	Verificador = CAST(1 as bit)
	,		Mensaje = 'Parametros Actualizados satisfactoriamente'
end

GO
/****** Object:  StoredProcedure [dbo].[FR_ObtenerAtributoDescuentoCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ObtenerAtributoDescuentoCliente]			*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[FR_ObtenerAtributoDescuentoCliente]
(
	@pv_CodAux VARCHAR(100)
,	@pv_textoAtributo varchar(100)
,	@pv_BaseDatos varchar(100)
)
AS
	declare @query nvarchar(max)

	select @query = ''

	select @query = ' 
		DECLARE @ValorAtributo FLOAT
		DECLARE @CODAUX VARCHAR(200)
		DECLARE @RUTAUX VARCHAR(200)

		SELECT	TOP 1 
				@CODAUX = ''' + @pv_CodAux + '''
		,		@RUTAUX = RutAux
		FROM	[' + @pv_BaseDatos + '].[softland].[cwtauxi]
		where	codaux = ''' + @pv_CodAux + '''

		select	@ValorAtributo = convert(int, atv.Valor)
		from	[' + @pv_BaseDatos + '].[softland].[CWTAuxiTTAtr] atc
			left join [' + @pv_BaseDatos + '].[softland].[CWTAuxiTVAtrV] atv 
				on atc.CodTat = atv.CodTat 
		where	atv.codigo = @CODAUX
		and		atc.NombreTipo = ''' + @pv_textoAtributo + '''

		if @ValorAtributo is null begin
			select	aux.RutAux
			,		atv.codigo
			,		atv.Valor 
			from	softland.CWTAuxiTTAtr atc
				left join softland.CWTAuxiTVAtrV atv 
					on atc.CodTat = atv.CodTat 
				left join softland.cwtauxi aux 
					on aux.CodAux = atv.Codigo
			where	atc.NombreTipo = ''' + @pv_textoAtributo + '''
			and		aux.RutAux = @RUTAUX
		end

		select ValorAtributo = @ValorAtributo 
	'
	
	begin try
		--select	ValorAtributo = 15
		EXEC (@query)
	end try
	begin catch
		select	ValorAtributo = 0
	end catch

GO
/****** Object:  StoredProcedure [dbo].[FR_ObtenerCredito]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[FR_ObtenerCredito]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE procedure [dbo].[FR_ObtenerCredito]
	@pv_CodAux varchar(15)
,	@pv_BaseDatos varchar(100)
as
BEGIN
	DECLARE @query varchar(max)
	
	select	@query = ''

	select	@query = @query + '
	IF EXISTS (select codaux from [' + @pv_BaseDatos + '].[softland].[CWDocSaldos] where CodAux = ''' + @pv_CodAux + ''') BEGIN
 		SELECT
			CONVERT(numeric(18,2),vcl.MtoCre) as Credito,
			CONVERT(NUMERIC(18,2),SUM(doc.DEBE)) as Debe,
			CONVERT(numeric(18,2),SUM(doc.HABER)) as Haber
		FROM
			[' + @pv_BaseDatos + '].softland.cwtcvcl as vcl INNER JOIN [' + @pv_BaseDatos + '].softland.CWDocSaldos as doc on vcl.CodAux = ''' + @pv_CodAux + ''' and doc.CodAux= vcl.CodAux 
		GROUP by
			vcl.MtoCre
	END
	ELSE BEGIN
		SELECT
			CONVERT(numeric(18,2),MtoCre) as Credito,
			Debe = CONVERT(numeric(18,2),0),
			Haber = CONVERT(numeric(18,2),0)
		FROM
			[' + @pv_BaseDatos + '].softland.cwtcvcl where CodAux = ''' + @pv_CodAux + '''	
	END
	'

	exec (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[Get_Credito_Cliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_Credito_Cliente]
@pv_BaseDatos varchar (100),
@CodAux varchar (25)
AS
DECLARE @query varchar (max)

SELECT @query = ''

SELECT @query = @query + '
	Select CONVERT(numeric(18,2),vcl.MtoCre)
	From ['+@pv_BaseDatos+'].softland.cwtcvcl as vcl INNER JOIN ['+@pv_BaseDatos+'].softland.CWDocSaldos as doc on
	 vcl.CodAux = '''+@CodAux+''' and doc.CodAux= vcl.CodAux 
	Group by vcl.MtoCre '

exec (@query)
	

GO
/****** Object:  StoredProcedure [dbo].[Get_cwmovimFV]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_cwmovimFV] 
@pv_BaseDatos varchar (100),
@MovNumDocRef float
AS
BEGIN
DECLARE @query varchar (max)

SELECT @query = ''
SELECT @query = @query + '
select min(MovFv) from  ['+@pv_BaseDatos+'].softland.cwmovim cwom
where cwom.MovNumDocRef = '+convert(varchar(30),@MovNumDocRef)+' and ttdcod= ''FV'' 
'
print (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[Get_DeudaClienteDia]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_DeudaClienteDia]
@CodAux varchar(20)
AS
DECLARE @deuda numeric
Set @deuda = (SELECT convert (numeric,( (sum(DEBE)) - (sum(HABER)) ))  from JS_DebeCliente2 
where CODAUX=  @CodAux)

IF @deuda = null
Set @deuda=0
IF @deuda is null
Set @deuda=0
IF @deuda < 0

SELECT @deuda

GO
/****** Object:  StoredProcedure [dbo].[Get_DeudaClienteTotal]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_DeudaClienteTotal]
@pv_BaseDatos varchar (100),
@CodAux AS varchar(20)
AS
DECLARE @query varchar (max)
SELECT @query = ''
SELECT @query = @query + 'SELECT convert (numeric,( (sum(DEBE)) - (sum(HABER)) ))  from  ['+@pv_BaseDatos+'].softland.CWDocSaldos
where CODAUX= '''+@CodAux+'''
'

EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[INS_COT_Contacto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[INS_COT_Contacto]
@Codaux varchar(10),
@nombre varchar(30),
@fono varchar(15),
@email  varchar(250),
@baseDatos varchar(100)
as
begin

declare @existe int
declare @sincronizado int

select @sincronizado=count(*) from DS_cwtauxi where CodAux=@Codaux and sincronizado=0
select @existe=COUNT(*) from DS_cwtaxco where CodAuc=@Codaux

DECLARE @Mensaje varchar(100)
select @Mensaje=' '
declare @query nvarchar(max)

if @sincronizado>0 and @existe=0
BEGIN
INSERT INTO DS_cwtaxco 
	  (CodAuc, NomCon, CarCon, FonCon, FonCon2, FonCon3, FaxCon, Casilla, Email, IDNotas, TipoSaludo, Usuario, Proceso, FechaUlMod, Sistema, FechaUltEnvCorreo)
	  VALUES 
	  (@Codaux, @nombre, '99', @fono, '', '', '', '', @email, '', '', '', '', GETDATE(), '', GETDATE());
	  select @Mensaje='Contacto Creado'
END
else
begin

select @query='
  insert into [' + @baseDatos + '].[softland].[cwtaxco] 
  (CodAuc, NomCon, CarCon, FonCon, FonCon2, FonCon3, FaxCon, Casilla, Email, IDNotas, TipoSaludo, Usuario, Proceso, FechaUlMod, Sistema, FechaUltEnvCorreo)
  values 
  ('''+@Codaux+''','''+@nombre+''', ''99'', '''+@fono+''', '''', '''', '''', '''','''+@email+''', '''', '''', '''', '''', GETDATE(), '''', GETDATE())
'
exec (@query)
--print @query
end


SELECT Verificador = cast(1 AS bit),
	   @Mensaje Mensaje 
end 
GO
/****** Object:  StoredProcedure [dbo].[insertaNVSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[insertaNVSoftland]
	@nvId int,
	@pv_BaseDatos varchar(100)
AS
	declare @fechanv datetime
	Select @fechanv =  nvfem from [dbo].[DS_NotasVenta] WHERE [Id] = @nvId
	DECLARE @query nvarchar (max)

	--Setear variable @ultimaNvNum
	declare @ultimaNvNumero nvarchar(100) ;
	DECLARE @query1 nvarchar(max);
	SELECT @query1 = N'SELECT @ultimaNvOUT =  max([NVNumero]) + 1  FROM ' + @pv_BaseDatos + '.[softland].[nw_nventa]';
	DECLARE @ParmDefinition nvarchar(500);  
	SET @ParmDefinition = N'@ultimaNvOUT int OUTPUT';  	
	EXEC sp_executesql @query1, @ParmDefinition, @ultimaNvOUT=@ultimaNvNumero OUTPUT;  
	--*****************************
	



	SET @query =  N'insert into ['+@pv_BaseDatos+'].[softland].[nw_nventa] select '+convert(varchar(100),@ultimaNvNumero)+'
	,[nvFem],[nvEstado],[nvEstFact],[nvEstDesp],[nvEstRese],[nvEstConc],''0'',[NumOC],[nvFeEnt],[CodAux],[VenCod],[CodMon],[CodLista],[nvObser],[nvCanalNV]
	,[CveCod],[NomCon],[CodiCC],[CodBode],[nvSubTotal],[nvPorcDesc01],[nvDescto01],[nvPorcDesc02],[nvDescto02],[nvPorcDesc03],[nvDescto03],[nvPorcDesc04]
	,[nvDescto04],[nvPorcDesc05],[nvDescto05],[nvMonto],[nvFeAprob],[NumGuiaRes], Case when nvValflete <> 0 then (([nvValflete]/ [nvSubTotal]) * 100) else [nvPorcFlete] end   as  [nvPorcFlete],[nvValflete],
	Case when [nvValEmb] <> 0 then (([nvValEmb]/ [nvSubTotal]) * 100) else [nvPorcFlete] end   as  [nvPorcEmb],''0'',[nvEquiv],[nvNetoExento]
	,[nvNetoAfecto],[nvTotalDesc],[ConcAuto],[CodLugarDesp],[SolicitadoPor],[DespachadoPor],[Patente],[RetiradoPor],[CheckeoPorAlarmaVtas],[EnMantencion],[Usuario]
	,[UsuarioGeneraDocto],[FechaHoraCreacion],[Sistema],[ConcManual],''0-0'',[MarcaWG],[proceso],[TotalBoleta],[NumReq],[CodBodeWms],CodLugarDocto,RutTransportista, null, null
	FROM [dbo].[DS_NotasVenta] WHERE [Id] = '+CONVERT(VARCHAR(100), @nvId)+';
	
	insert into ['+@pv_BaseDatos+'].[softland].[nw_detnv] SELECT  '+convert(varchar(100),@ultimaNvNumero)+'
	,[nvLinea],[nvCorrela],[nvFecCompr],[CodProd],[nvCant],[nvPrecio],[nvEquiv],[nvSubTotal],[nvDPorcDesc01],[nvDDescto01],[nvDPorcDesc02],[nvDDescto02]
	,[nvDPorcDesc03],[nvDDescto03],[nvDPorcDesc04],[nvDDescto04],[nvDPorcDesc05],[nvDDescto05],[nvTotDesc],[nvTotLinea],[nvCantDesp],[nvCantProd],[nvCantFact]
	,[nvCantDevuelto],[nvCantNC],[nvCantBoleta],[nvCantOC],[DetProd],[CheckeoMovporAlarmaVtas],[KIT],[CodPromocion],[CodUMed],[CantUVta],[MarcaWG],[Partida],[Pieza]
	,[FechaVencto],[CantidadKit],[PorcIncidenciaKit] FROM [dbo].[DS_NotasVentaDetalle] WHERE [IdNotaVenta] =  '+CONVERT(VARCHAR(100), @nvId)+';
	
	insert into ['+@pv_BaseDatos+'].[softland].[nw_impto](nvNumero,codimpto,valpctIni,afectoImpto,Impto)
	select '+convert(varchar(100),@ultimaNvNumero)+' ,iwti.codimpto,iwtv.valpctini,
	Case when iwti.CodImpto =''IVA'' then (nwv.nvvalflete + Sum(nwd.nvSubtotal)) else Sum(nwd.nvSubtotal) END  as afectoImpto,
	Case when iwti.CodImpto =''IVA'' then ((nwv.nvvalflete + Sum(nwd.nvSubtotal))* iwtv.valpctini)/100 else Sum(((iwtv.valpctini * nwd.nvSubtotal)/100)) END  as Impto
	
	from ['+@pv_BaseDatos+'].[softland].[iw_timprod] iwti 
	left join ['+@pv_BaseDatos+'].softland.[iw_timpval] iwtv on iwti.codimpto = iwtv.codimpto
	left join ['+@pv_BaseDatos+'].softland.nw_detnv nwd on nwd.codprod = iwti.codprod 
	left join ['+@pv_BaseDatos+'].softland.nw_nventa nwv on nwv.nvnumero = nwd.nvnumero
	where nwd.codprod = iwti.codprod 
	and '''+convert(varchar(100),@fechanv,103)+''' >iwtv.fecinivig and '''+convert(varchar(100),@fechanv,103)+''' < iwtv.fecfinvig
	and nwd.nvnumero = '+convert(varchar(100),@ultimaNvNumero)+'
	group by iwti.codimpto,iwtv.valpctini,nwv.nvvalflete; 
	'
	EXEC (@query)
	--Fin insercion de impuestos

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarClientesCodAuxRut]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[JS_ListarClientesCodAuxRut]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE Procedure [dbo].[JS_ListarClientesCodAuxRut]  
(
	@vchrRutAux VARCHAR(20) = ''
,	@vchrCodAux VARCHAR(10) = ''
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
-- ==========================================================================================  
-- Lista los clientes filtrados por Codigo Aux y RUT Aux  
-- ==========================================================================================  

	declare @query varchar(max)
	select @query = ''
	DECLARE @Sincronizado int 
	select @Sincronizado=count(*) from DS_cwtauxi where CodAux=@vchrCodAux and sincronizado=0

	if @Sincronizado>0
	begin
		select @query = @query + '
		SELECT	CodAux = clientes.CodAux,
		NomAux = clientes.[NomAux],
		RutAux = clientes.[RutAux],
		DirAux = clientes.[DirAux],
		DirNum = ISNULL(clientes.[DirNum],''''),
		NomCon = contacto.[NomCon],
		FonCon = ISNULL(contacto.[FonCon],''''),
		EMail = ISNULL(clientes.EMail,''''),
		Notas = clientes.Notas,
		ComCod = com.ComCod,
		CiuCod = ciu.CiuCod,
		ComDes = com.ComDes,
		CiuDes = ciu.CiuDes
		FROM	DS_cwtauxi clientes 
		left JOIN [' + @pv_BaseDatos + '].[softland].[cwtaxco] contacto 
		ON (clientes.CodAux collate SQL_Latin1_General_CP1_CI_AI = contacto.CodAuc)  
		left join [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
		ON clientes.ComAux collate SQL_Latin1_General_CP1_CI_AI = com.ComCod
		left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
		ON clientes.CiuAux collate SQL_Latin1_General_CP1_CI_AI = ciu.CiuCod
		WHERE	CodAux = ''' + @vchrCodAux + '''
		AND		RutAux = (CASE ''' + @vchrRutAux + ''' WHEN '''' THEN RutAux ELSE ''' + @vchrRutAux + ''' END) 
		'
	end

	if @Sincronizado=0
	begin
		select @query = @query + '
		SELECT	CodAux = clientes.CodAux,
		NomAux = clientes.[NomAux],
		RutAux = clientes.[RutAux],
		DirAux = clientes.[DirAux],
		DirNum = ISNULL(clientes.[DirNum],''''),
		NomCon = contacto.[NomCon],
		FonCon = ISNULL(contacto.[FonCon],''''),
		EMail = ISNULL(clientes.EMail,''''),
		Notas = clientes.Notas,
		ComCod = com.ComCod,
		CiuCod = ciu.CiuCod,
		ComDes = com.ComDes,
		CiuDes = ciu.CiuDes
		FROM	[' + @pv_BaseDatos + '].[softland].[cwtauxi] clientes 
		left JOIN [' + @pv_BaseDatos + '].[softland].[cwtaxco] contacto 
		ON (clientes.CodAux = contacto.CodAuc)  
		left join [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
		ON clientes.ComAux = com.ComCod
		left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
		ON clientes.CiuAux = ciu.CiuCod
		WHERE	CodAux = ''' + @vchrCodAux + '''
		AND		RutAux = (CASE ''' + @vchrRutAux + ''' WHEN '''' THEN RutAux ELSE ''' + @vchrRutAux + ''' END) 
		'
	end
	
	print (@query)
	EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarContactos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[JS_ListarContactos]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE Procedure [dbo].[JS_ListarContactos]
(
	@vchrCodAux VARCHAR(50) = ''
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
	declare @query varchar(max)

	select @query = ''

	-- ==========================================================================================  
	-- Lista los clientes filtrados por Codigo Aux y RUT Aux  
	-- ==========================================================================================  
	select @query = @query + '
	SELECT	CodAux = contacto.CodAuc
	,		NomCon = contacto.[NomCon]
	,		FonCon = ISNULL(contacto.[FonCon],'''')
	,		EMail = ISNULL(contacto.EMail,'''')  
	FROM	[' + @pv_BaseDatos + '].[softland].[cwtaxco] contacto  
	WHERE	contacto.CodAuc = ''' + @vchrCodAux + '''

	 '
	exec (@query)
end  

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarListaDePrecio]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[JS_ListarListaDePrecio]
as
BEGIN

Select CodLista, DesLista from 
KUPPEL.[softland].iw_tlispre 

END

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarMisClientes]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[JS_ListarMisClientes]
    @cod nchar(10)
,	@ID int
,	@pv_BaseDatos varchar(100)
AS
BEGIN
	declare @lb_ClienteBloqueado BIT
	declare @lb_ClienteInactivo BIT
	declare @lb_ListaClientesTodos BIT

	SELECT	@lb_ClienteBloqueado = a.CreacionNotaVentaUsuariosBloqueados
	,		@lb_ClienteInactivo = a.CreacionNotaVentaUsuariosInactivos
	,		@lb_ListaClientesTodos = a.ListaClientesTodos
	FROM	DS_Parametros a
		inner join DS_Empresa b
			on a.IdEmpresa = b.Id
	where	BaseDatos = @pv_BaseDatos

	declare @query nvarchar(max)
	
	select	@query = ''

	select	@query = @query + '
	SELECT	distinct --top 500 
			c.CodAux
	,		c.NomAux
	,		c.DirAux
	,		c.DirNum
	,		c.RutAux
	,		FonAux1 = 
			CASE	WHEN c.FonAux1 IS NOT NULL 
						THEN c.FonAux1 
					WHEN c.FonAux2 IS NOT NULL 
						THEN c.FonAux2 
						ELSE c.FonAux3 
			END
	,		Notas = convert(varchar(max), C.Notas)
	FROM	['+@pv_BaseDatos+'].softland.cwtauxi C 
		LEFT JOIN ['+@pv_BaseDatos+'].softland.[cwtauxven] A 
			ON (c.CodAux = a.CodAux) 
		LEFT JOIN ['+@pv_BaseDatos+'].softland.cwtvend B 
			ON (b.VenCod = a.VenCod) 
		LEFT JOIN ['+@pv_BaseDatos+'].softland.cwtcvcl as vcl 
			ON vcl.CodAux = A.CodAux
	WHERE	(1=1)
	--and c.codaux = ''76033522'' 
	' + CASE WHEN @lb_ListaClientesTodos = 1 THEN '' else '	and		b.VenCod in (''' + @cod + ''', ''OFI'')' end + '
	' + CASE WHEN @lb_ClienteBloqueado = 1 THEN '' else '	AND		C.Bloqueado	<> ''S''' end + '
	' + CASE WHEN @lb_ClienteInactivo = 1 THEN '' else '	AND		C.ActAux	<> ''N''' end + '
	
	UNION

	select distinct --top 500 
			c.CodAux collate SQL_Latin1_General_CP1_CI_AS CodAux
	,		c.NomAux collate SQL_Latin1_General_CP1_CI_AS NomAux
	,		c.DirAux collate SQL_Latin1_General_CP1_CI_AS DirAux
	,		c.DirNum collate SQL_Latin1_General_CP1_CI_AS DirNum
	,		c.RutAux collate SQL_Latin1_General_CP1_CI_AS RutAux
	,		 
			CASE	WHEN c.FonAux1   IS NOT NULL 
						THEN c.FonAux1  collate SQL_Latin1_General_CP1_CI_AS
					WHEN c.FonAux2 IS NOT NULL 
						THEN c.FonAux2  collate SQL_Latin1_General_CP1_CI_AS
						ELSE c.FonAux3  collate SQL_Latin1_General_CP1_CI_AS
			END as FonAux1
	,		Notas = convert(varchar(max), C.Notas collate SQL_Latin1_General_CP1_CI_AS)
	FROM	DS_cwtauxi C 
	where   sincronizado=0
	'
	print (@query)
	EXEC(@query)
END

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarMisClientes_concredito]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--sp_helptext JS_ListarMisClientes
/*------------------------------------------------------------------------------*/  
/*-- Empresa   : DISOFI            */  
/*-- Tipo    : Procedimiento           */  
/*-- Nombre    : [dbo].[JS_ListarMisClientes]        */  
/*-- Detalle   :              */  
/*-- Autor    : FDURAN            */  
/*-- Modificaciones  :              */  
/*------------------------------------------------------------------------------*/  
CREATE PROCEDURE [dbo].[JS_ListarMisClientes_concredito]  
 @cod nchar(10)  
, @ID int  
, @pv_BaseDatos varchar(100)  
AS  
BEGIN  
 declare @query nvarchar(max)  
   
 select @query = ''  
  
 select @query = @query + '  
 SELECT c.CodAux  
 ,  c.NomAux  
 ,  c.DirAux  
 ,  c.DirNum  
 ,  FonAux1 =   
   CASE WHEN c.FonAux1 IS NOT NULL   
      THEN c.FonAux1   
     WHEN c.FonAux2 IS NOT NULL   
      THEN c.FonAux2   
      ELSE c.FonAux3   
   END  
 ,  C.Notas  
 --,  DeudaVencida =   
 --  (  
 --   SELECT isnull(convert (numeric,( (sum(sub_a.DEBE)) - (sum(sub_a.HABER)))), 0)  
 --   from [' + @pv_BaseDatos + '].[softland].[CWDocSaldos] sub_a  
 --   where sub_a.CODAUX = c.CodAux  
 --   and  (  
 --      select isnull(min(MovFv), '''')  
 --      from [' + @pv_BaseDatos + '].[softland].[cwmovim] sub_cwom  
 --      where sub_cwom.MovNumDocRef = sub_a.MOVNUMDOCREF and ttdcod= ''FV''  
 --     ) < getdate()  
 --   and  MONTH  
 --     (  
 --      (  
 --       select isnull(min(MovFv), '''')  
 --       from [' + @pv_BaseDatos + '].[softland].[cwmovim] sub_cwom  
 --       where sub_cwom.MovNumDocRef = sub_a.MOVNUMDOCREF and ttdcod= ''FV''  
 --      )  
 --     ) < getdate()  
 --  )  
 --,  Deuda =   
 --  (  
 --   SELECT isnull(convert (numeric,( (sum(sub_a.DEBE)) - (sum(sub_a.HABER)))), 0)  
 --   FROM [' + @pv_BaseDatos + '].[softland].[CWDocSaldos] sub_a  
 --   where sub_a.CODAUX = c.CodAux  
 --  )  
 ,  Credito =   
   CASE WHEN (  
        (Select CONVERT(numeric(18,2),vcl.MtoCre)  
        From [' + @pv_BaseDatos + '].softland.cwtcvcl as vcl INNER JOIN [' + @pv_BaseDatos + '].softland.CWDocSaldos as doc on  
         vcl.CodAux = c.CodAux and doc.CodAux= vcl.CodAux   
        Group by vcl.MtoCre ) -   
        (SELECT convert (numeric,( (sum(DEBE)) - (sum(HABER)) ))  from  [' + @pv_BaseDatos + '].softland.CWDocSaldos  
        where CODAUX= c.CodAux)  
       ) < 0   
      then ''$0''  
      ELSE FORMAT((Select CONVERT(numeric(18,2),vcl.MtoCre)  
        From [' + @pv_BaseDatos + '].softland.cwtcvcl as vcl INNER JOIN [' + @pv_BaseDatos + '].softland.CWDocSaldos as doc on  
         vcl.CodAux = c.CodAux and doc.CodAux= vcl.CodAux   
        Group by vcl.MtoCre ) -   
        (SELECT convert (numeric,( (sum(DEBE)) - (sum(HABER)) ))  from  [' + @pv_BaseDatos + '].softland.CWDocSaldos  
        where CODAUX= c.CodAux),''$0'')    
   END  
 FROM [' + @pv_BaseDatos + '].softland.[cwtauxven] A   
  INNER JOIN [' + @pv_BaseDatos + '].softland.cwtauxi C   
   ON (c.CodAux = a.CodAux)   
  INNER JOIN [' + @pv_BaseDatos + '].softland.cwtvend B   
   ON (b.VenCod = a.VenCod)   
  INNER JOIN [dbo].[DS_UsuarioEmpresa] D   
   ON (b.VenCod collate Modern_Spanish_CI_AS = d.VenCod)   
  LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtcvcl as vcl   
   ON vcl.CodAux = A.CodAux  
 WHERE D.VenCod = ' + @cod + '  
 and  D.ID = ' + CONVERT(VARCHAR(20), @ID ) + '  
 AND  C.Bloqueado <> ''S''  
 '  
  
 EXEC (@query)  
END  
  

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarNVDETALLENM]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[JS_ListarNVDETALLENM]  
  @nvId INT,  
  @pv_BaseDatos varchar(100)  
  as  
  BEGIN  
  DECLARE @query varchar(max)  
  SELECT @query = ''  
-- ==========================================================================================  
-----------------Lista detalle  nota de venta según NVNumero-------------------------  
-- ==========================================================================================  
  SELECT @query = @query + '  
   SELECT  
    a.nvLinea,  
    a.CodProd,  
    DesProd = isnull(b.DesProd, c.DesProd),  
    a.nvCant,  
    a.CodUMed,  
    a.nvPrecio,  
    a.nvSubTotal,  
    a.NVNumero,  
    ROUND(a.nvDPorcDesc01,0) as nvDPorcDesc01,  
    ROUND(a.nvDPorcDesc02,0) as nvDPorcDesc02,  
    ROUND(a.nvDPorcDesc03,0) as nvDPorcDesc03,  
    ROUND(a.nvDPorcDesc04,0) as nvDPorcDesc04,  
    ROUND(a.nvDPorcDesc05,0) as nvDPorcDesc05,  
    ROUND(a.nvDDescto01,0) as nvDDescto01,  
    ROUND(a.nvDDescto02,0) as nvDDescto02,  
    ROUND(a.nvDDescto03,0) as nvDDescto03,  
    ROUND(a.nvDDescto04,0) as nvDDescto04,  
    ROUND(a.nvDDescto05,0) as nvDDescto05,  
    a.nvTotLinea  
   FROM  [dbo].[DS_NotasVentaDetalle] a 
		left JOIN ['+@pv_BaseDatos+'].[softland].[iw_tprod] b 
			ON (a.CodProd = b.CodProd collate Modern_Spanish_CI_AS)  
		left JOIN DS_ProductosNuevos c
			ON (a.CodProd collate Modern_Spanish_CI_AS = c.CodProd) 
   WHERE  
    a.IdNotaVenta = '+convert(varchar(100),@nvId)+'  
   ORDER BY  
    a.nvLinea  
    '  
    EXEC (@query)  
  END  

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarNVNM]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[JS_ListarNVNM] --25 'transporte'
		@nvId INT,
		@pv_BaseDatos varchar(100)
		as
		BEGIN
		DECLARE @query varchar(max)
		SELECT @query = ''
-- ==========================================================================================
-----------------Lista la cabecera de la nota de venta según NVNumero-------------------------
-- ==========================================================================================
		SELECT @query = @query + '
			SELECT
				nv.Id,
				nv.NVNumero,
				vend.VenCod,
				vend.VenDes,
				vend.Usuario,
				conven.CveCod,
				conven.CveDes,
				nv.nvFem,
				nv.nvFeEnt,
				lista.CodLista,
				lista.DesLista,
				cliente.CodAux,
				cliente.NomAux,
				nv.NomCon,
				cc.CodiCC,
				cc.DescCC,
				nv.nvObser,
				nv.nvSubTotal,
				nv.TotalBoleta,
				nv.ErrorAprobador,
				nv.ErrorAprobadorMensaje,
				nv.NumOC
			FROM
				[dbo].[DS_NotasVenta] nv INNER JOIN
				['+@pv_BaseDatos+'].[softland].[cwtauxi] cliente ON (cliente.CodAux collate Modern_Spanish_CI_AS = nv.CodAux) left JOIN
				['+@pv_BaseDatos+'].[softland].cwtvend vend ON (vend.VenCod collate Modern_Spanish_CI_AS = nv.VenCod) left JOIN
				['+@pv_BaseDatos+'].[softland].cwtconv conven ON (conven.CveCod collate Modern_Spanish_CI_AS = nv.CveCod) left JOIN
				['+@pv_BaseDatos+'].[softland].iw_tlispre lista ON (lista.CodLista collate Modern_Spanish_CI_AS = nv.CodLista) left JOIN
				['+@pv_BaseDatos+'].[softland].[cwtccos] cc ON (cc.CodiCC collate Modern_Spanish_CI_AS = nv.CodiCC)
			WHERE
				nv.Id = '+convert(varchar(100),@nvId)+'
				'
		EXEC (@query)
		END

GO
/****** Object:  StoredProcedure [dbo].[JS_ListarVendorVenCod]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[JS_ListarVendorVenCod]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE Procedure [dbo].[JS_ListarVendorVenCod]
(
	@VenCod VARCHAR(10)  
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
	declare @query varchar(max)

	select @query = ''
	
-- ==========================================================================================  
-- Lista los clientes filtrados por Codigo Aux y RUT Aux  
-- ========================================================================================== 
	select @query = @query + '
	SELECT	Nombre = du.Nombre,
			VenCod = due.VenCod,
			Email = du.email,
			Contrasena = du.ContrasenaCorreo
	FROM	dbo.DS_Usuarios du join dbo.DS_UsuarioEmpresa due on du.Id = due.IdUsuario
		join DS_Empresa emp on due.IdEmpresa = emp.Id
	WHERE	due.VenCod = ''' + @VenCod + '''
	AND		emp.BaseDatos = ''' + @pv_BaseDatos + '''
	'

	exec (@query)
end  

GO
/****** Object:  StoredProcedure [dbo].[Js_NvSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[Js_NvSoftland] 
	as begin
	select max([NVNumero]) as NVNumero FROM KUPPEL.[softland].[nw_nventa]
	end

GO
/****** Object:  StoredProcedure [dbo].[productosStock]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[productosStock]
@codProd varchar(100)
as
select  CodigoProducto = v.CodProd
,DescripcionProducto =  p.DesProd
,UnidadMedida = isnull(p.CodUMed,0)
,Grupo = isnull(p.CodGrupo,'0')
,SubGrupo = isnull(p.CodSubGr,'0')
,CodigoBodega = v.CodBode
,DescripcionBodega = bod.DesBode
,StockTipo = v.StockTipo
,CantidadStock = v.StockCantidad
,Costo = EMELTEC.softland.IW_fdblCostoPromedio(v.CodProd,CONVERT (date, SYSDATETIME()))
,V.StockCantidad * EMELTEC.softland.IW_fdblCostoPromedio(v.CodProd,CONVERT (date, SYSDATETIME())) as CostoTotal
from dbo.StockPorTipoBodega v
left join EMELTEC.softland.iw_tbode bod on v.CodBode = bod.CodBode
left join EMELTEC.softland.iw_tprod p on v.CodProd = p.CodProd
left join EMELTEC.softland.iw_nivst n on (v.CodProd = n.CodProd and v.CodBode=n.CodBode) 
where (p.Inventariable = -1) and p.CodProd = @codProd AND p.Inactivo = 0
order by v.CodProd 

GO
/****** Object:  StoredProcedure [dbo].[reporteStock]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[reporteStock]
as
select  Marca = isnull(tg.DesGrupo,'')
,CodigoProducto = v.CodProd
,DescripcionProducto =  p.DesProd
,UnidadMedida = isnull(p.CodUMed,0)
,Grupo = isnull(p.CodGrupo,'0')
,DesProd2 = isnull(p.DesProd2,'')
,SubGrupo = isnull(p.CodSubGr,'0')
,CodigoBodega = v.CodBode
,DescripcionBodega = bod.DesBode
,StockTipo = v.StockTipo
,CantidadStock = v.StockCantidad
from dbo.StockPorTipoBodega v
left join EMELTEC.softland.iw_tbode bod on v.CodBode = bod.CodBode
left join EMELTEC.softland.iw_tprod p on v.CodProd = p.CodProd
left join EMELTEC.softland.iw_tgrupo tg on tg.CodGrupo = p.CodGrupo
left join EMELTEC.softland.iw_nivst n on (v.CodProd = n.CodProd and v.CodBode=n.CodBode) 
where p.Inventariable = -1 and p.Inactivo = 0 order by v.CodProd 

GO
/****** Object:  StoredProcedure [dbo].[RRA_ActualizaEstadoNW]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[RRA_ActualizaEstadoNW]
 @nvId    varchar(30),
 @pv_BaseDatos varchar (100)
as
begin

	declare @obser varchar(200)
	DECLARE @query varchar (max)
	
	set @obser = (select nvObser from dbo.DS_NotasVenta where dbo.DS_NotasVenta.Id = @nvId )

	--exec dbo.insertaNVSoftland @nvId,@pv_BaseDatos
	
	declare @ultimaNvNumero nvarchar(100) ;
	DECLARE @query1 nvarchar(max);
	SELECT @query1 = N'SELECT @ultimaNvOUT =  max([NVNumero]) + 1  FROM ' + @pv_BaseDatos + '.[softland].[nw_nventa] 
						if (@ultimaNvOUT is null)
						set @ultimaNvOUT = 1';
	DECLARE @ParmDefinition nvarchar(500);  
	SET @ParmDefinition = N'@ultimaNvOUT int OUTPUT';  	
	EXEC sp_executesql @query1, @ParmDefinition, @ultimaNvOUT=@ultimaNvNumero OUTPUT;   

	DECLARE @IdDetalleNotaVenta int
	DECLARE	@VerificadorSoftland BIT
	DECLARE	@MensajeSoftland VARCHAR(MAX)

	EXEC [FR_AgregarNVCabeceraSoftland]
			@pi_IdNotaVenta = @nvId
		,	@pv_BaseDatos = @pv_BaseDatos
		,	@pb_Verificador = @VerificadorSoftland out
		,	@pv_Mensaje = @MensajeSoftland out
		,	@pi_NVNumero = @ultimaNvNumero out
	

	DECLARE CursorNVD CURSOR FOR SELECT	id FROM dbo.DS_NotasVentaDetalle dnvd where idnotaventa = @nvId
	OPEN CursorNVD
	FETCH NEXT FROM CursorNVD INTO @IdDetalleNotaVenta	
	WHILE @@fetch_status = 0
	BEGIN
	EXEC [FR_AgregarNVDetalleSoftland]
			@pi_IdNotaVenta = @nvId
		,	@pv_BaseDatos = @pv_BaseDatos
		,	@pi_IdDetalleNotaVenta = @IdDetalleNotaVenta
		,	@pb_Verificador = @VerificadorSoftland out
		,	@pv_Mensaje = @MensajeSoftland out
		,	@pi_NVNumero = @ultimaNvNumero out	
		FETCH NEXT FROM CursorNVD INTO @IdDetalleNotaVenta	
	END
	CLOSE cursorNVD
	DEALLOCATE CursorNVD	
	
	set @obser = (select nvObser from dbo.DS_NotasVenta where dbo.DS_NotasVenta.Id = @nvId)

	update dbo.DS_NotasVenta  set EstadoNP = 'A',nvEstado = 'A', nvObser = 'N. Int: '+@nvId+' Obs: '+@obser  where dbo.DS_NotasVenta.Id = @nvId

	UPDATE dbo.DS_NotasVenta set RutSolicitante = @ultimaNvNumero, nvObser = ' NV: '+@ultimaNvNumero+' Obs: '+@obser  where dbo.DS_NotasVenta.Id = @nvId

	SELECT convert(int,@ultimaNvNumero) 'NVNumero'
END

GO
/****** Object:  StoredProcedure [dbo].[SP_COT_GET_ObtenerNombreVendedor]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_COT_GET_ObtenerNombreVendedor]
@Codigo varchar(4),
@BaseDatos varchar(100)
as
begin
declare @query varchar(max)

	select	@query = '
    select VenDes 
	from ['+@BaseDatos+'].[softland].[cwtvend] where VenCod='''+@Codigo+'''
	'
--print @query
exec (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DEL_COT_Consideracion]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_DEL_COT_Consideracion]
@Id int
as
begin
delete ds_consideracion where id= @Id

SELECT	Verificador = cast(1 as bit), Mensaje = 'Elimina exitosamente'
end
GO
/****** Object:  StoredProcedure [dbo].[SP_DEL_COT_EliminaDetalle]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_DEL_COT_EliminaDetalle]
@Linea int,
@IdCotizacion int
as
begin
 delete DS_CotizacionDetalle where nvLinea=@Linea and IdNotaVenta=@IdCotizacion

    --INI UPDATE CABECERA
   declare @TotalConIva float
   select @TotalConIva = ((select sum(nvPrecio) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion)*19/100)+(select sum(nvPrecio) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion)
   update DS_Cotizacion set 
   nvSubTotal=(select sum(nvTotLinea) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion),
   nvMonto=@TotalConIva,
   nvNetoAfecto=(select sum(nvSubTotal) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion),
   TotalBoleta=@TotalConIva
    where Id = @IdCotizacion
   --FIN UPDATE CABECERA

 	SELECT	Verificador = cast(1 as bit), Mensaje = 'Linea detalle Cotizacion eliminada'
end

GO
/****** Object:  StoredProcedure [dbo].[SP_DEL_NV_RutaArchivoAdjunto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_DEL_NV_RutaArchivoAdjunto]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[SP_DEL_NV_RutaArchivoAdjunto]
@CodAux varchar(50)
as
delete RutaArchivoAdjunto where CodAux = @CodAux

select cast(1 as bit) Verificador
GO
/****** Object:  StoredProcedure [dbo].[SP_DELALL_UsuarioEmpresa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_DELALL_UsuarioEmpresa]
(
	@pi_IdUsuario INT
)
AS
BEGIN
	DECLARE @lb_Verificador BIT
	DECLARE @lv_Mensaje VARCHAR(MAX)

	DELETE FROM dbo.DS_UsuarioEmpresa
	WHERE	IdUsuario = @pi_IdUsuario

	SELECT	@lb_Verificador = 1
	,		@lv_Mensaje = 'Usuario empresa eliminado exitosamente'
	
	SELECT	Verificador = @lb_Verificador 
	,		Mensaje = @lv_Mensaje 
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_Ciudad]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_Ciudad]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[SP_GET_Ciudad]
(
	@pv_BaseDatos varchar(100)
)
AS
	declare @query varchar(max)

	select @query = ''

	select	@query = @query + '
	SELECT	CiuCod
	,		CiuDes 
	FROM	[' + @pv_BaseDatos + '].[softland].[cwtciud] c 
	'

	exec (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_Comuna]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_Comuna]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[SP_GET_Comuna]
(
	@pv_BaseDatos varchar(100)
)
AS
	declare @query varchar(max)

	select @query = ''

	select	@query = @query + '
	SELECT	ComCod
	,		ComDes 
	FROM	[' + @pv_BaseDatos + '].[softland].[cwtcomu] c 
	'

	exec (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_CorreoManager]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GET_CorreoManager]
@NvId int
as
select Email
from DS_CorreosManager cm
join nventa_manag nm 
on nm.idCorreoManag = cm.Id
where idNotaVenta = @NvId

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_CorreosProManag]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_GET_CorreosProManag]
as
select Nombre,Email,Id from DS_CorreosManager

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_BuscarCotizacionCabecera]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_GET_COT_BuscarCotizacionCabecera] 
	@nvId   INT,
	@pv_BaseDatos varchar(100)
AS
--declare @CodCliente varchar(10)
--select @CodCliente=CodAux from DS_Cotizacion where Id=@nvId
DECLARE @query varchar (max)
SELECT @query = ''
SELECT @query = @query + '

BEGIN
SELECT * FROM (
SELECT  nv.Id, 
		nv.NVNumero, 
		vend.VenCod, 
		vend.VenDes, 
		vend.Usuario, 
		isnull(conven.CveCod,'''')CveCod, 
		isnull(conven.CveDes,''Sin condicion'')CveDes,
		CONVERT(datetime, nv.nvFem, 103) AS nvFem, 
		CONVERT(datetime, nv.nvFeEnt, 103) AS nvFeEnt, 
		lista.CodLista, 
		lista.DesLista, 
        cliente.CodAux COLLATE SQL_Latin1_General_CP1_CI_AI AS CodAux, 
		cliente.NomAux COLLATE SQL_Latin1_General_CP1_CI_AI AS NomAux, 
		nv.NomCon, cc.CodiCC, cc.DescCC, CONVERT(nvarchar(MAX), nv.nvObser) AS nvObser, nv.nvSubTotal, nv.TotalBoleta, 
		nv.ErrorAprobador, nv.ErrorAprobadorMensaje, cliente.DirAux COLLATE SQL_Latin1_General_CP1_CI_AI AS DirAux, DS_NotaVentaExtras.FechaCierre,
		nv.nvNetoAfecto,
		cliente.FonAux1 COLLATE SQL_Latin1_General_CP1_CI_AI AS Fono
FROM    DS_Cotizacion AS nv INNER JOIN
        DS_cwtauxi AS cliente ON cliente.CodAux COLLATE Modern_Spanish_CI_AS = nv.CodAux LEFT OUTER JOIN
        DS_NotaVentaExtras ON nv.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = nv.VenCod LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtconv AS conven ON conven.CveCod COLLATE Modern_Spanish_CI_AS = nv.CveCod LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].iw_tlispre AS lista ON lista.CodLista COLLATE Modern_Spanish_CI_AS = nv.CodLista LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtccos AS cc ON cc.CodiCC COLLATE Modern_Spanish_CI_AS = nv.CodiCC
	where cliente.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1

		
	UNION
	SELECT
	    NV.Id,
		nv.NVNumero,
		vend.VenCod,
		vend.VenDes,
		vend.Usuario,
		isnull(conven.CveCod,'''')CveCod,
		isnull(conven.CveDes,''Sin condicion'')CveDes,
		convert(datetime, nv.nvFem,103) nvFem,
		convert(datetime, nv.nvFeEnt,103) nvFeEnt,
		lista.CodLista,
		lista.DesLista,
		cliente.CodAux,
		cliente.NomAux,
		nv.NomCon,
		cc.CodiCC,
		cc.DescCC,
		convert(nvarchar(max),nv.nvObser)nvObser,
		nv.nvSubTotal,
		nv.TotalBoleta,
		nv.ErrorAprobador,
		nv.ErrorAprobadorMensaje,
		cliente.DirAux,
		DS_NotaVentaExtras.FechaCierre,
		nv.nvNetoAfecto,
		cliente.FonAux1 as Fono
	FROM    DS_Cotizacion AS nv INNER JOIN
       ['+@pv_BaseDatos+'].[softland].[cwtauxi] AS cliente ON cliente.CodAux COLLATE Modern_Spanish_CI_AS = nv.CodAux LEFT OUTER JOIN
        DS_NotaVentaExtras ON nv.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = nv.VenCod LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtconv AS conven ON conven.CveCod COLLATE Modern_Spanish_CI_AS = nv.CveCod LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].iw_tlispre AS lista ON lista.CodLista COLLATE Modern_Spanish_CI_AS = nv.CodLista LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].[cwtccos] AS cc ON cc.CodiCC COLLATE Modern_Spanish_CI_AS = nv.CodiCC
	   where DS_NotaVentaExtras.EsCotizacion=1	
		)TB
WHERE Id = '+convert(varchar(100),@nvId)+'

END

'
print @query
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_BuscarCotizacionCabecera_X_NV]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_COT_BuscarCotizacionCabecera_X_NV] 
	@NroCotizacion varchar(20),
	@pv_BaseDatos varchar(100),
	@CodCliente varchar(10),
	@VenCod varchar(10)
AS
DECLARE @nvId INT
select @nvId=IdNotaVenta from DS_NotaVentaExtras where NroFinalCotizacion=@NroCotizacion


DECLARE @query varchar (max)
SELECT @query = ''

declare @sincronizado int
select @sincronizado=COUNT(*) from DS_cwtauxi where CodAux=@CodCliente and sincronizado=0

if @Sincronizado>0
begin
SELECT @query = @query + '
SELECT top(1)  nv.Id, 
        nv.NVNumero, 
        vend.VenCod, 
		vend.VenDes, 
		vend.Usuario, 
		conven.CveCod, 
		conven.CveDes, 
		CONVERT(datetime, nv.nvFem, 103) AS nvFem, 
		CONVERT(datetime, nv.nvFeEnt, 103) AS nvFeEnt, 
		lista.CodLista, 
		lista.DesLista, 
        cliente.CodAux, 
		cliente.NomAux, 
		nv.NomCon, 
		cc.CodiCC, 
		cc.DescCC, 
		CONVERT(nvarchar(MAX), nv.nvObser) AS nvObser, 
		nv.nvSubTotal, 
		nv.TotalBoleta, 
		nv.ErrorAprobador, 
		nv.ErrorAprobadorMensaje, 
        cliente.DirAux, 
		DS_NotaVentaExtras.FechaCierre, 
		nv.CodMon, 
		nv.EstadoNP, 
		DS_cwtauxd.DirDch, 
		comunas.ComDes, 
		ciudades.CiuDes,
		DS_cwtauxd.NomDch
FROM    DS_Cotizacion AS nv INNER JOIN
        DS_cwtauxi AS cliente ON cliente.CodAux COLLATE Modern_Spanish_CI_AS = nv.CodAux LEFT OUTER JOIN
        DS_cwtauxd ON cliente.CodAux = DS_cwtauxd.CodAxD LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtcomu AS comunas ON comunas.ComCod COLLATE Modern_Spanish_CI_AS = DS_cwtauxd.ComDch INNER JOIN
        DS_NotaVentaExtras ON nv.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtciud AS ciudades ON ciudades.CiuCod COLLATE Modern_Spanish_CI_AS = DS_cwtauxd.CiuDch LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = nv.VenCod LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtconv AS conven ON conven.CveCod COLLATE Modern_Spanish_CI_AS = nv.CveCod LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].iw_tlispre AS lista ON lista.CodLista COLLATE Modern_Spanish_CI_AS = nv.CodLista LEFT OUTER JOIN
        ['+@pv_BaseDatos+'].[softland].cwtccos AS cc ON cc.CodiCC COLLATE Modern_Spanish_CI_AS = nv.CodiCC
WHERE   (cliente.sincronizado = 0) and 
		nv.Id = '+convert(varchar(100),@nvId)+' and 
		nv.CodAux='''+@CodCliente+''' and 
		nv.EstadoNP=''P'' and 
		nv.VenCod = ''' + @VenCod + ''' and
		DS_NotaVentaExtras.EsCotizacion=1	
'
end
if @sincronizado=0
begin
SELECT @query = @query + '
	SELECT nv.Id, 
	       nv.NVNumero, 
		   vend.VenCod, 
		   vend.VenDes, 
		   vend.Usuario, 
		   conven.CveCod, 
		   conven.CveDes, 
		   CONVERT(datetime, nv.nvFem, 103) AS nvFem, 
		   CONVERT(datetime, nv.nvFeEnt, 103) AS nvFeEnt, 
		   lista.CodLista, 
		   lista.DesLista, 
		   cliente.CodAux, 
		   cliente.NomAux, 
		   nv.NomCon, 
		   cc.CodiCC, 
		   cc.DescCC, 
		   CONVERT(nvarchar(MAX), nv.nvObser) AS nvObser, 
		   nv.nvSubTotal, 
		   nv.TotalBoleta, 
		   nv.ErrorAprobador, 
		   nv.ErrorAprobadorMensaje, 
		   cliente.DirAux, 
	       DS_NotaVentaExtras.FechaCierre, 
		   nv.CodMon, 
		   nv.EstadoNP, 
		   cwtauxd.DirDch, 
		   comunas.ComDes, 
		   ciudades.CiuDes,
		   cwtauxd.NomDch
	FROM   DS_Cotizacion AS nv LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].cwtauxi AS cliente ON cliente.CodAux COLLATE Modern_Spanish_CI_AS = nv.CodAux LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].cwtauxd ON  ['+@pv_BaseDatos+'].[softland].cwtauxd.CodAxD = cliente.CodAux LEFT OUTER JOIN
	DS_NotaVentaExtras ON nv.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].cwtcomu AS comunas ON comunas.ComCod =  ['+@pv_BaseDatos+'].[softland].cwtauxd.ComDch LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = nv.VenCod LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].cwtconv AS conven ON conven.CveCod COLLATE Modern_Spanish_CI_AS = nv.CveCod LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].iw_tlispre AS lista ON lista.CodLista COLLATE Modern_Spanish_CI_AS = nv.CodLista LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].cwtccos AS cc ON cc.CodiCC COLLATE Modern_Spanish_CI_AS = nv.CodiCC LEFT OUTER JOIN
	 ['+@pv_BaseDatos+'].[softland].cwtciud AS ciudades ON ciudades.CiuCod =  ['+@pv_BaseDatos+'].[softland].cwtauxd.CiuDch
	WHERE nv.Id = '+convert(varchar(100),@nvId)+' and 
	nv.CodAux='''+@CodCliente+''' and 
	nv.EstadoNP=''P'' and 
	nv.VenCod = ''' + @VenCod + ''' and
	DS_NotaVentaExtras.EsCotizacion=1	
'
end

--print(@query)
EXEC (@query)


GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_BuscarDetalle]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_COT_BuscarDetalle]  
 @nvId INT,
 @pv_BaseDatos varchar(100)  
AS 
--DECLARE @nvId INT
--select @nvId=IdNotaVenta from DS_NotaVentaExtras where NroFinalCotizacion=@NroCotizacion 

DECLARE @query varchar (max)  
  
 declare @lv_bodega varchar(50)  
  
select top 1   
  @lv_bodega =   
    case when a.stockproductoesbodega = 1   
       then isnull(stockproductocodigobodega, '')   
       else ''   
    end   
from ds_parametros a   
 inner join ds_empresa b   
  on a.idempresa = b.id   
where b.basedatos = @pv_BaseDatos  
  
SELECT @query = ''  
SELECT @query = @query + '  
begin  
select   
a.Id,
a.nvLinea,  
a.CodProd,   
EsProductoNuevo = CAST(case when tp.DesProd collate Modern_Spanish_CI_AS is not null then 0 else 1 end AS BIT),
DesProd = case when tp.DesProd collate Modern_Spanish_CI_AS is not null then tp.DesProd collate Modern_Spanish_CI_AS else productosNuevos.DesProd collate Modern_Spanish_CI_AS end,
CodMon = case when tp.CodMonOrig collate Modern_Spanish_CI_AS is not null then tp.CodMonOrig collate Modern_Spanish_CI_AS else productosNuevos.CodMonOrig collate Modern_Spanish_CI_AS end,
CodGrupo = case when tp.CodGrupo collate Modern_Spanish_CI_AS is not null then tp.CodGrupo collate Modern_Spanish_CI_AS else productosNuevos.CodGrupo collate Modern_Spanish_CI_AS end,
CodSubGr = case when tp.CodSubGr collate Modern_Spanish_CI_AS is not null then tp.CodSubGr collate Modern_Spanish_CI_AS else productosNuevos.CodSubGr collate Modern_Spanish_CI_AS end,
ValorNetoProducto = case when tp.PrecioVta is not null then tp.PrecioVta else productosNuevos.PrecioVta end,
a.Partida,  
a.Pieza,  
a.nvCant,   
a.CodUMed,   
a.nvPrecio,   
a.nvSubTotal,  
ROUND(a.nvDPorcDesc01,0) as nvDPorcDesc01,   
ROUND(a.nvDPorcDesc02,0) as nvDPorcDesc02,   
ROUND(a.nvDPorcDesc03,0) as nvDPorcDesc03,   
ROUND(a.nvDPorcDesc04,0) as nvDPorcDesc04,   
ROUND(a.nvDPorcDesc05,0) as nvDPorcDesc05,   
a.nvTotLinea,  
  Stock = ISNULL((    
      select  Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible    
      FROM  [' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro))     
      WHERE  Fecha <= GETDATE()    
      and   CodProd = tp.CodProd   ' +  
  case when @lv_bodega = '' then '' else ' and codbode = ''' + @lv_bodega + '''' end + '  
      GROUP BY CodProd    
     ), 0),
	 a.nvDDescto01  
from [dbo].[DS_CotizacionDetalle] a  
left JOIN ['+@pv_BaseDatos+'].[softland].[iw_tprod] AS tp on a.CodProd collate Modern_Spanish_CI_AS = tp.CodProd   
left JOIN DS_ProductosNuevos as productosNuevos on a.CodProd collate Modern_Spanish_CI_AS = productosNuevos.CodProd   
where a.IdNotaVenta = '+convert(varchar(100),@nvId)+'  
order by a.nvLinea  
end  
'  
EXEC (@query)  
PRINT (@query)  
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_BuscarDirecDespa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_GET_COT_BuscarDirecDespa]
(
	@CodAxD varchar(10)
,	@pv_BaseDatos varchar(100)
)
as
begin
   declare @query varchar(max)
   select @query = ''
  
   declare @Sincronizado int
   select @Sincronizado=count(*) from DS_cwtauxi where CodAux=@CodAxD and sincronizado=0

   if @Sincronizado>0
   begin
   select @query = @query + '
   select	NomDch collate SQL_Latin1_General_CP1_CI_AI NomDch
	,		DirDch collate SQL_Latin1_General_CP1_CI_AI DirDch
	,		ComDch collate SQL_Latin1_General_CP1_CI_AI ComDch
	,		com.ComDes
	,		CiuDch collate SQL_Latin1_General_CP1_CI_AI CiuDch
	,		ciu.CiuDes
	from	ds_cwtauxd clientes
	       left join [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
				ON clientes.ComDch  collate SQL_Latin1_General_CP1_CI_AI = com.ComCod
			left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
				ON clientes.CiuDch  collate SQL_Latin1_General_CP1_CI_AI = ciu.CiuCod
	where CodAxD=''' + @CodAxD + '''
	'
	EXEC (@query)
   end

   else
   begin
   		
		select @query = @query + '
		select	clientes.NomDch 
		,		clientes.DirDch
		,		clientes.ComDch
		,		com.ComDes
		,		clientes.CiuDch
		,		ciu.CiuDes
		from	[' + @pv_BaseDatos + '].[Softland].cwtauxd clientes
			left join [' + @pv_BaseDatos + '].[softland].[cwtcomu] com
				ON clientes.ComDch = com.ComCod
			left join [' + @pv_BaseDatos + '].[softland].[cwtciud] ciu
				ON clientes.CiuDch = ciu.CiuCod
		where	CodAxD = ''' + @CodAxD + ''''

		EXEC (@query)
	end

end  
    

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_Consideracion]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_COT_Consideracion]
as
begin
select * from DS_consideracion
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_ExisteCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_COT_ExisteCliente]
@CodAux varchar (15),
@pv_BaseDatos varchar (100)
AS

	declare @existeSf int 
	DECLARE @query1 nvarchar(max)
	SELECT @query1 = 
	'SELECT @ExisteOUT = count(*) FROM ['+@pv_BaseDatos+'].softland.cwtauxi where CodAux = '''+@CodAux+''' '
	DECLARE @ParmDefinition nvarchar(500);  
	SET @ParmDefinition = N'@ExisteOUT int OUTPUT';  	
	EXEC sp_executesql @query1, @ParmDefinition, @ExisteOUT= @existeSf OUTPUT; 


	if @existeSf=0
	begin

		DECLARE @existe int 
		SET @existe = (SELECT count(*) FROM DS_cwtauxi where CodAux = @CodAux and sincronizado=0)
		if(@existe > 0)
		BEGIN
			SELECT Verificador = cast(1 AS bit),
			Mensaje = 'Cliente ya existe'
		END
		else
		BEGIN
			SELECT Verificador = cast(0 AS bit),
			Mensaje = 'Cliente no existe'
		END
    END
    else

	    SELECT Verificador = cast(1 AS bit), Mensaje = 'Cliente ya existe'



GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_FiltroCotizacionesPendientes]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_COT_FiltroCotizacionesPendientes]
	@pv_BaseDatos varchar (100),
	@numCotizacion nvarchar(20),
	@vendedor varchar(4),
	@tienefechaCot int,
	@fechaCotizacion datetime,
	@tienefechaCierre int,
	@fechaCierre datetime
AS
BEGIN
declare @IdCotizacion int
if @numCotizacion<>'0'
   select @IdCotizacion=IdNotaVenta from DS_NotaVentaExtras where NroFinalCotizacion=@numCotizacion

if @IdCotizacion is null
   set @IdCotizacion=0

declare @fecha_cierre nvarchar(10)
select @fecha_cierre=convert(nvarchar(10),@fechaCierre,120)
declare @fecha_Cotizacion nvarchar(10)
select @fecha_Cotizacion=convert(nvarchar(10),@fechaCotizacion,120)
  DECLARE @query VARCHAR(MAX)
  SELECT  @query = ''
  if @IdCotizacion>0 --and @vendedor='-1'
  begin
    SELECT
      @query = @query + '
	select * from (
	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux,
	clientes.[NomAux],
	clientes.DirAux,
	clientes.FonAux1,
	convert(nvarchar(max),a.nvObser)Observacion,
	clientes.[RutAux],
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto
	FROM DS_Cotizacion AS a INNER JOIN
    DS_cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
    DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
    DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
    DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where clientes.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1
	
	union

	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
	clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
	convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
	clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto
	FROM DS_Cotizacion AS a INNER JOIN
	[' + @pv_BaseDatos + '].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
	DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
	DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
	DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where DS_NotaVentaExtras.EsCotizacion=1
	)tb
	where estadoNP=''P'' and id = '+convert(nvarchar(20),@IdCotizacion)+'
	 order by Id desc

	'
  end

  if @tienefechaCot>0
  begin
  SELECT
      @query = @query + '
	select * from (
	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux,
	clientes.[NomAux],
	clientes.DirAux,
	clientes.FonAux1,
	convert(nvarchar(max),a.nvObser)Observacion,
	clientes.[RutAux],
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto,
	a.VenCod
	FROM DS_Cotizacion AS a INNER JOIN
    DS_cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
    DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
    DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
    DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where clientes.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1
	
	union

	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
	clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
	convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
	clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto,
	a.VenCod
	FROM DS_Cotizacion AS a INNER JOIN
	[' + @pv_BaseDatos + '].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
	DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
	DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
	DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where DS_NotaVentaExtras.EsCotizacion=1
	)tb
	where estadoNP=''P'' and nvFem = '''+@fecha_Cotizacion+''' and VenCod='''+@vendedor+'''
	 order by Id desc

	'
  end

  if @tienefechaCierre>0
  begin
  SELECT
      @query = @query + '
	select * from (
	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux,
	clientes.[NomAux],
	clientes.DirAux,
	clientes.FonAux1,
	convert(nvarchar(max),a.nvObser)Observacion,
	clientes.[RutAux],
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto,
	DS_NotaVentaExtras.FechaCierre,
	a.VenCod
	FROM DS_Cotizacion AS a INNER JOIN
    DS_cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
    DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
    DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
    DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where clientes.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1
	union

	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
	clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
	convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
	clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto,
	DS_NotaVentaExtras.FechaCierre,
	a.VenCod
	FROM DS_Cotizacion AS a INNER JOIN
	[' + @pv_BaseDatos + '].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
	DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
	DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
	DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
   --WHERE DS_NotaVentaExtras.FechaCierre='''+@fecha_cierre+'''
	where DS_NotaVentaExtras.EsCotizacion=1
	)tb
	where estadoNP=''P'' and FechaCierre='''+@fecha_cierre+''' and VenCod='''+@vendedor+'''
	 order by Id desc
	 '
  end

  IF @vendedor<>'-1' and @IdCotizacion=0 and @tienefechaCierre='-1' and @tienefechaCot='-1'
  begin 
  SELECT
      @query = @query + '
	  select * from (
	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux,
	clientes.[NomAux],
	clientes.DirAux,
	clientes.FonAux1,
	convert(nvarchar(max),a.nvObser)Observacion,
	clientes.[RutAux],
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	vend.VenCod,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto
	FROM DS_Cotizacion AS a INNER JOIN
    DS_cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
    DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
    DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
    DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where clientes.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1
	
	union

	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
	clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
	convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
	clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	vend.VenCod,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto
	FROM DS_Cotizacion AS a INNER JOIN
	[' + @pv_BaseDatos + '].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
	DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
	DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
	DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where DS_NotaVentaExtras.EsCotizacion=1
	)tb
	where estadoNP=''P'' and VenCod = '''+@vendedor+'''
	order by Id desc
	'
  end


  if @IdCotizacion=0 and @tienefechaCierre='-1' and @tienefechaCot='-1'
  begin
    SELECT
      @query = @query + '
	select * from (
	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux,
	clientes.[NomAux],
	clientes.DirAux,
	clientes.FonAux1,
	convert(nvarchar(max),a.nvObser)Observacion,
	clientes.[RutAux],
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto
	FROM DS_Cotizacion AS a INNER JOIN
    DS_cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
    DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
    DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
    DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
    [' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where clientes.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1
	
	union

	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
	clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
	convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
	clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion,
	convert(int,a.nvMonto)nvMonto
	FROM DS_Cotizacion AS a INNER JOIN
	[' + @pv_BaseDatos + '].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
	DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
	DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
	DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
	[' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	where DS_NotaVentaExtras.EsCotizacion=1
	)tb
	where estadoNP=''P''
	 order by Id desc

	'
  end
  print (@query)
  EXEC (@query)
 
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_FiltroCotizacionesPendientesII]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_COT_FiltroCotizacionesPendientesII]
	@pv_BaseDatos varchar (100),
	@vendedor varchar(4)
AS
BEGIN
  DECLARE @query VARCHAR(MAX)
  SELECT  @query = ''
 
  begin 
  SELECT
      @query = @query + '
	  select * from (
	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux,
	clientes.[NomAux],
	clientes.DirAux,
	clientes.FonAux1,
	convert(nvarchar(max),a.nvObser)Observacion,
	clientes.[RutAux],
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	vend.VenCod
	from [dbo].[DS_Cotizacion] a
	inner join DS_cwtauxi clientes on  clientes.CodAux collate Modern_Spanish_CI_AS = a.CodAux 
	LEFT JOIN DS_CotizacionDetalle b on a.NVNumero = b.NVNumero
	LEFT JOIN  ['+@pv_BaseDatos+'].[softland].[iw_tprod] AS tp on b.CodProd = tp.CodProd collate SQL_Latin1_General_CP1_CI_AS
	left join DS_CotizacionDetalle c on a.Id = c.IdNotaVenta
	left join  ['+@pv_BaseDatos+'].[softland].cwtvend vend on vend.VenCod collate Modern_Spanish_CI_AS = a.VenCod
	where clientes.sincronizado=0
	
	union

	select	distinct 
	a.NVNumero,
	a.Id,
	a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
	clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
	clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
	convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
	clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
	a.nvEstado,
	a.nvFem, 
	a.nvFeEnt, 
	a.NomCon, 
	a.nvNetoAfecto, 
	a.TotalBoleta, 
	a.EstadoNP, 
	a.nvSubTotal,
	vend.VenDes,
	vend.VenCod
	from [dbo].[DS_Cotizacion] a
	inner join  ['+@pv_BaseDatos+'].[softland].[cwtauxi] clientes on  clientes.CodAux collate Modern_Spanish_CI_AS = a.CodAux 
	LEFT JOIN [dbo].[DS_CotizacionDetalle] b on a.NVNumero = b.NVNumero
	LEFT JOIN  ['+@pv_BaseDatos+'].[softland].[iw_tprod] AS tp on b.CodProd = tp.CodProd collate SQL_Latin1_General_CP1_CI_AS
	left join [dbo].[DS_CotizacionDetalle] c on a.Id = c.IdNotaVenta
	left join  ['+@pv_BaseDatos+'].[softland].cwtvend vend on vend.VenCod collate Modern_Spanish_CI_AS = a.VenCod
	
	)tb
	where estadoNP=''P'' and VenCod = '''+@vendedor+'''
	order by Id desc
	'
  end

  print @query
    EXEC (@query)
 
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_ListarCotizacionesPendientes]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GET_COT_ListarCotizacionesPendientes]
	@pv_BaseDatos varchar (100),
	@pi_IdEmpresaInterna int,
	@codigoVendedor varchar(4)
AS
BEGIN
  DECLARE @query VARCHAR(MAX)

    SELECT
      @query = ''

	  if @codigoVendedor='-1'
	  begin
	  SELECT
		  @query = @query + '
		select * from (
		select	distinct 
		a.NVNumero,
		a.Id,
		a.CodAux,
		clientes.[NomAux],
		clientes.DirAux,
		clientes.FonAux1,
		convert(nvarchar(max),a.nvObser)Observacion,
		clientes.[RutAux],
		a.nvEstado,
		a.nvFem, 
		a.nvFeEnt, 
		a.NomCon, 
		a.nvNetoAfecto, 
		a.TotalBoleta, 
		a.EstadoNP, 
		a.nvSubTotal,
		vend.VenDes,
		a.VenCod,
		a.nvMonto,
		ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion
		FROM DS_Cotizacion AS a INNER JOIN
        DS_cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
        DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
        DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
        [' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
        DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
        [' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
		where clientes.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1
	
		union

		select	distinct 
		a.NVNumero,
		a.Id,
		a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
		clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
		clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
		clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
		convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
		clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
		a.nvEstado,
		a.nvFem, 
		a.nvFeEnt, 
		a.NomCon, 
		a.nvNetoAfecto, 
		a.TotalBoleta, 
		a.EstadoNP, 
		a.nvSubTotal,
		vend.VenDes,
		a.VenCod,
		a.nvMonto,
		ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion
		FROM DS_Cotizacion AS a INNER JOIN
		[' + @pv_BaseDatos + '].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
		DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
		DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
		[' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
		DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
		[' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	    where DS_NotaVentaExtras.EsCotizacion=1
		)tb
		where estadoNP=''P'' 
		 order by Id desc
		'
	  end
	  else
	  begin
		SELECT
		  @query = @query + '
		select * from (
		select	distinct 
		a.NVNumero,
		a.Id,
		a.CodAux,
		clientes.[NomAux],
		clientes.DirAux,
		clientes.FonAux1,
		convert(nvarchar(max),a.nvObser)Observacion,
		clientes.[RutAux],
		a.nvEstado,
		a.nvFem, 
		a.nvFeEnt, 
		a.NomCon, 
		a.nvNetoAfecto, 
		a.TotalBoleta, 
		a.EstadoNP, 
		a.nvSubTotal,
		vend.VenDes,
		a.VenCod,
		a.nvMonto,
		ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion
		FROM DS_Cotizacion AS a INNER JOIN
        DS_cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
        DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
        DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
        [' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
        DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
        [' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
		where clientes.sincronizado=0 and DS_NotaVentaExtras.EsCotizacion=1
	
		union

		select	distinct 
		a.NVNumero,
		a.Id,
		a.CodAux collate SQL_Latin1_General_CP1_CI_AI,
		clientes.[NomAux] collate SQL_Latin1_General_CP1_CI_AI,
		clientes.DirAux collate SQL_Latin1_General_CP1_CI_AI,
		clientes.FonAux1 collate SQL_Latin1_General_CP1_CI_AI,
		convert(nvarchar(max),a.nvObser collate SQL_Latin1_General_CP1_CI_AI)Observacion,
		clientes.[RutAux] collate SQL_Latin1_General_CP1_CI_AI,
		a.nvEstado,
		a.nvFem, 
		a.nvFeEnt, 
		a.NomCon, 
		a.nvNetoAfecto, 
		a.TotalBoleta, 
		a.EstadoNP, 
		a.nvSubTotal,
		vend.VenDes,
		a.VenCod,
		a.nvMonto,
		ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,'''')NroFinalCotizacion
		FROM DS_Cotizacion AS a INNER JOIN
		[' + @pv_BaseDatos + '].softland.cwtauxi AS clientes ON clientes.CodAux COLLATE Modern_Spanish_CI_AS = a.CodAux LEFT OUTER JOIN
		DS_NotaVentaExtras ON a.Id = DS_NotaVentaExtras.IdNotaVenta LEFT OUTER JOIN
		DS_CotizacionDetalle AS b ON a.NVNumero = b.NVNumero LEFT OUTER JOIN
		[' + @pv_BaseDatos + '].softland.iw_tprod AS tp ON b.CodProd = tp.CodProd COLLATE SQL_Latin1_General_CP1_CI_AS LEFT OUTER JOIN
		DS_CotizacionDetalle AS c ON a.Id = c.IdNotaVenta LEFT OUTER JOIN
		[' + @pv_BaseDatos + '].softland.cwtvend AS vend ON vend.VenCod COLLATE Modern_Spanish_CI_AS = a.VenCod
	    where DS_NotaVentaExtras.EsCotizacion=1
		)tb
		where estadoNP=''P'' and VenCod='''+@codigoVendedor+'''
		 order by Id desc

		'
	end

    EXEC (@query)
 
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_PdfCotizacion]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_COT_PdfCotizacion]
@Id int,
@pv_BaseDatos AS varchar(100)
as
begin
DECLARE @query AS nvarchar(max)
declare @CodAux varchar(10)
declare @Moneda varchar(60)
declare @CodEdi varchar(3)

select @CodAux = CodAux from DS_Cotizacion where Id=@Id

SELECT @query = ''
SELECT @query = @query + '

select @MonedaAux=DesMon, @CodEdiAux=CodEdi
from [' + @pv_BaseDatos + '].softland.cwtmone 
where CodMon=(select CodMon COLLATE SQL_Latin1_General_CP1_CI_AI from DS_Cotizacion where Id='+convert(char(30),@Id)+')'

DECLARE @Param NVARCHAR(max)
set  @Param=  N'@MonedaAux varchar(60) OUTPUT
               ,@CodEdiAux VARCHAR(10) OUTPUT'

EXEC sp_executesql	@query,
					@Param,
					@MonedaAux = @Moneda output,
					@CodEdiAux=@CodEdi output			  

select @query=''

select @query='

SELECT  c.Id, c.IdEmpresaInterna, c.EstadoNP, c.NVNumero, c.nvFem, c.nvEstado, c.nvEstFact, c.nvEstDesp, c.nvEstRese, c.nvEstConc, c.CotNum, c.NumOC, c.nvFeEnt, c.CodAux, c.VenCod, c.CodMon, c.CodLista, c.nvObser, 
        c.nvCanalNV, c.CveCod, c.NomCon, c.CodiCC, c.CodBode, c.nvSubTotal, c.nvPorcDesc01, c.nvDescto01, c.nvPorcDesc02, c.nvDescto02, c.nvPorcDesc03, c.nvDescto03, c.nvPorcDesc04, c.nvDescto04, c.nvPorcDesc05, 
        c.nvDescto05, c.nvMonto, c.nvFeAprob, c.NumGuiaRes, c.nvPorcFlete, c.nvValflete, c.nvPorcEmb, c.nvValEmb, c.nvEquiv, c.nvNetoExento, c.nvNetoAfecto, c.nvTotalDesc, c.ConcAuto, c.CodLugarDesp, c.SolicitadoPor, 
        c.DespachadoPor, c.Patente, c.RetiradoPor, c.CheckeoPorAlarmaVtas, c.EnMantencion, c.Usuario, c.UsuarioGeneraDocto, c.FechaHoraCreacion, c.Sistema, c.ConcManual, c.RutSolicitante, c.proceso, c.TotalBoleta, c.NumReq, 
        c.CodVenWeb, c.CodBodeWms, c.CodLugarDocto, c.RutTransportista, c.Cod_Distrib, c.Nom_Distrib, c.MarcaWG, c.ErrorAprobador, c.ErrorAprobadorMensaje, ISNULL(DS_NotaVentaExtras.nroSolicitud, 0) AS NroCotizacion,
		ISNULL(['+@pv_BaseDatos +'].softland.cwtvend.VenDes,'' '') nombreVendedor, 
		ISNULL(['+@pv_BaseDatos +'].softland.cwtvend.EMail,'' '')EmailVendedor,
		ISNULL(cv.CveDes,'' '') FormaPago,
		ISNULL(cv.CveDias,'' '') PlazoPago,
		ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,0) NroFinalCotizacion,
		ISNULL(DS_Usuarios.Telefono,'' '')Telefono, 
		ISNULL(DS_Usuarios.Direccion,'' '')Direccion,
		ISNULL(c.nvObser,'' '')Observaciones
FROM    ['+@pv_BaseDatos +'].softland.cwtconv AS cv RIGHT OUTER JOIN
        DS_Cotizacion AS c ON cv.CveCod = c.CveCod COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
        DS_UsuarioEmpresa LEFT OUTER JOIN
        DS_Usuarios ON DS_UsuarioEmpresa.IdUsuario = DS_Usuarios.ID ON c.VenCod = DS_UsuarioEmpresa.VenCod LEFT OUTER JOIN
        ['+@pv_BaseDatos +'].softland.cwtconv RIGHT OUTER JOIN
        ['+@pv_BaseDatos +'].softland.cwtcvcl ON ['+@pv_BaseDatos +'].softland.cwtconv.CveCod = ['+@pv_BaseDatos +'].softland.cwtcvcl.ConVta ON c.CodAux COLLATE SQL_Latin1_General_CP1_CI_AI = ['+@pv_BaseDatos +'].softland.cwtcvcl.CodAux LEFT OUTER JOIN
        ['+@pv_BaseDatos +'].softland.cwtvend ON c.VenCod COLLATE SQL_Latin1_General_CP1_CI_AI = ['+@pv_BaseDatos +'].softland.cwtvend.VenCod LEFT OUTER JOIN
        DS_NotaVentaExtras ON c.Id = DS_NotaVentaExtras.IdNotaVenta
WHERE   (c.Id = '+convert(char(30),@Id)+')  and DS_NotaVentaExtras.EsCotizacion=1

SELECT      DS_CotizacionDetalle.Id, DS_CotizacionDetalle.IdNotaVenta, DS_CotizacionDetalle.NVNumero, DS_CotizacionDetalle.nvLinea, DS_CotizacionDetalle.nvCorrela, DS_CotizacionDetalle.nvFecCompr, 
            DS_CotizacionDetalle.CodProd, DS_CotizacionDetalle.nvCant, DS_CotizacionDetalle.nvPrecio, DS_CotizacionDetalle.nvEquiv, DS_CotizacionDetalle.nvSubTotal, DS_CotizacionDetalle.nvDPorcDesc01, 
            DS_CotizacionDetalle.nvDDescto01, DS_CotizacionDetalle.nvDPorcDesc02, DS_CotizacionDetalle.nvDDescto02, DS_CotizacionDetalle.nvDPorcDesc03, DS_CotizacionDetalle.nvDDescto03, 
            DS_CotizacionDetalle.nvDPorcDesc04, DS_CotizacionDetalle.nvDDescto04, DS_CotizacionDetalle.nvDPorcDesc05, DS_CotizacionDetalle.nvDDescto05, DS_CotizacionDetalle.nvTotDesc, DS_CotizacionDetalle.nvTotLinea, 
            DS_CotizacionDetalle.nvCantDesp, DS_CotizacionDetalle.nvCantProd, DS_CotizacionDetalle.nvCantFact, DS_CotizacionDetalle.nvCantDevuelto, DS_CotizacionDetalle.nvCantNC, DS_CotizacionDetalle.nvCantBoleta, 
            DS_CotizacionDetalle.nvCantOC, DS_CotizacionDetalle.DetProd, DS_CotizacionDetalle.CheckeoMovporAlarmaVtas, DS_CotizacionDetalle.KIT, DS_CotizacionDetalle.CodPromocion, DS_CotizacionDetalle.CodUMed, 
            DS_CotizacionDetalle.CantUVta, DS_CotizacionDetalle.Partida, DS_CotizacionDetalle.Pieza, DS_CotizacionDetalle.FechaVencto, DS_CotizacionDetalle.CantidadKit, DS_CotizacionDetalle.MarcaWG, 
            DS_CotizacionDetalle.PorcIncidenciaKit, DS_NotaVentaExtras.FechaCierre
FROM        DS_CotizacionDetalle LEFT OUTER JOIN
            DS_NotaVentaExtras ON DS_CotizacionDetalle.IdNotaVenta = DS_NotaVentaExtras.IdNotaVenta
where DS_CotizacionDetalle.IdNotaVenta='+convert(char(30),@Id)+' and DS_NotaVentaExtras.EsCotizacion=1

select * from (
  
SELECT   C.CodAux COLLATE SQL_Latin1_General_CP1_CI_AI codaux, 
         C.NomAux COLLATE SQL_Latin1_General_CP1_CI_AI cliente, 
         C.NoFAux COLLATE SQL_Latin1_General_CP1_CI_AI NoFAux, 
		 C.RutAux COLLATE SQL_Latin1_General_CP1_CI_AI rut, 
		 C.DirAux COLLATE SQL_Latin1_General_CP1_CI_AI direccion, 
		 C.FonAux1 COLLATE SQL_Latin1_General_CP1_CI_AI fono,  
		 ISNULL(Ciu.CiuDes, '''') AS Ciudad, 
		 ISNULL(Com.ComDes, '''') AS Comuna,
		 '''+@Moneda+'''  Moneda,
		 '''+@CodEdi+''' CodEdi,
		 ISNULL(DS_cwtaxco.NomCon COLLATE SQL_Latin1_General_CP1_CI_AI,''-'') AS NombreContacto,  
		 ISNULL(DS_cwtaxco.FonCon COLLATE SQL_Latin1_General_CP1_CI_AI,''-'') AS FonoContacto, 
		 ISNULL(DS_cwtaxco.Email COLLATE SQL_Latin1_General_CP1_CI_AI,''-'') AS EmailContacto
FROM     DS_cwtauxi AS C LEFT OUTER JOIN
		 DS_cwtaxco ON C.CodAux = DS_cwtaxco.CodAuc LEFT OUTER JOIN
		 ['+@pv_BaseDatos +'].softland.cwtciud AS Ciu ON C.CiuAux COLLATE SQL_Latin1_General_CP1_CI_AI = Ciu.CiuCod LEFT OUTER JOIN
		 ['+@pv_BaseDatos +'].softland.cwtcomu AS Com ON C.ComAux COLLATE SQL_Latin1_General_CP1_CI_AI = Com.ComCod
where    c.sincronizado=0
UNION
SELECT   C.CodAux, C.NomAux, 
         isnull(C.NoFAux,'''')NoFAux, C.RutAux, 
		 isnull(C.DirAux,'''')DirAux, C.FonAux1, ISNULL(Ciu.CiuDes, '''') AS Ciudad, ISNULL(Com.ComDes, '''') AS Comuna,
		 '''+@Moneda+'''  Moneda,
		 '''+@CodEdi+''' CodEdi,
		 ISNULL((select NomCon from DS_Cotizacion d where d.id = '+convert(char(30),@Id)+'),''-'') NombreContacto, 
		 isnull((select top 1 FonCon from ['+@pv_BaseDatos +'].softland.cwtaxco where nomcon=(select NomCon collate Latin1_General_CI_AS from DS_Cotizacion where id='+convert(char(30),@Id)+')),'' - '')FonoContacto,
		 isnull((select top 1 Email from ['+@pv_BaseDatos +'].softland.cwtaxco where nomcon=(select NomCon collate Latin1_General_CI_AS from DS_Cotizacion where id='+convert(char(30),@Id)+')),'' - '')EmailContacto
FROM     ['+@pv_BaseDatos +'].softland.cwtauxi AS C LEFT OUTER JOIN
         ['+@pv_BaseDatos +'].softland.cwtaxco ON C.CodAux = ['+@pv_BaseDatos +'].softland.cwtaxco.CodAuc LEFT OUTER JOIN
		 ['+@pv_BaseDatos +'].softland.cwtciud AS Ciu ON C.CiuAux = Ciu.CiuCod LEFT OUTER JOIN
		 ['+@pv_BaseDatos +'].softland.cwtcomu AS Com ON C.ComAux = Com.ComCod
 )tb
WHERE    (CodAux = '''+@CodAux+''')
select rtrim(dire)+'+''','''+ '+comu direccionEmpresa,RutE,NomB, Giro,fono,EMailDTE from ['+@pv_BaseDatos +'].softland.soempre
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_Producto_X_Modificar]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_COT_Producto_X_Modificar]
@nroLinea int,
@codProd varchar(20),
@Id int,
@pv_BaseDatos nvarchar(100)
as
begin
declare @query nvarchar(max)
declare @Local int
 select @Local= count(*) from DS_ProductosNuevos where CodProd=@codProd and AgregadoASoftland=0

 if @Local>0
 begin
SELECT  CD.Id, 
        CD.CodProd, 
		CD.DetProd AS DesProd, 
        producto.CodGrupo, 
		producto.CodSubGr, 
		CD.nvPrecio AS PrecioVta, 
		CD.nvCant AS Cantidad, 
		CD.nvDPorcDesc01 AS PorcentajeDescuento, 
		producto.CodMonOrig AS CodMon, 
        1 AS ProdLocal, 
		isnull(Moneda.DesMon,'Sin Tipo') DescripcionMoneda, 
		isnull(subgrupo.DesSubGr,'Sin SubGrupo') DescripcionSubGrupo, 
		isnull(grupo.DesGrupo,'Sin Grupo') DescripcionGrupo,
		CD.nvFecCompr,
		producto.DesProd2 CodigoFabrica
FROM    DS_ProductosNuevos AS producto RIGHT OUTER JOIN
        DS_CotizacionDetalle AS CD ON producto.CodProd = CD.CodProd COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
        DS_Cotizacion ON CD.IdNotaVenta = DS_Cotizacion.Id LEFT OUTER JOIN
        [EMELTEC].softland.cwtmone AS Moneda ON producto.CodMonOrig = Moneda.CodMon COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
        [EMELTEC].softland.iw_tgrupo AS grupo ON producto.CodGrupo = grupo.CodGrupo COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
        [EMELTEC].softland.iw_tsubgr AS subgrupo ON producto.CodSubGr = subgrupo.CodSubGr COLLATE SQL_Latin1_General_CP1_CI_AI
	where CD.IdNotaVenta = @Id AND CD.nvLinea = @nroLinea AND CD.CodProd = @codProd
 end

 if @Local=0
 begin
	 select @query='

	 	SELECT CD.Id, 
		   CD.CodProd, 
		   CD.DetProd AS DesProd, 
		   producto.CodGrupo, 
		   producto.CodSubGr, 
		   CD.nvPrecio AS PrecioVta, 
		   CD.nvCant AS Cantidad, 
		   CD.nvDPorcDesc01 AS PorcentajeDescuento, 
		   DS_Cotizacion.CodMon,
		   0 ProdLocal,
		   isnull(Moneda.DesMon,''Sin Tipo'') DescripcionMoneda, 
		   isnull(subgrupo.DesSubGr,''Sin SubGrupo'') DescripcionSubGrupo, 
		   isnull(grupo.DesGrupo,''Sin Grupo'') DescripcionGrupo,
		   CD.nvFecCompr,
		   producto.DesProd2 CodigoFabrica
	FROM   [' + @pv_BaseDatos + '].[softland].[iw_tprod] as producto RIGHT OUTER JOIN
		   DS_CotizacionDetalle AS CD ON producto.CodProd = CD.CodProd COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
		   DS_Cotizacion ON CD.IdNotaVenta = DS_Cotizacion.Id LEFT OUTER JOIN
		   [' + @pv_BaseDatos + '].softland.cwtmone AS Moneda ON producto.CodMonOrig = Moneda.CodMon COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
           [' + @pv_BaseDatos + '].softland.iw_tgrupo AS grupo ON producto.CodGrupo = grupo.CodGrupo COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
           [' + @pv_BaseDatos + '].softland.iw_tsubgr AS subgrupo ON producto.CodSubGr = subgrupo.CodSubGr COLLATE SQL_Latin1_General_CP1_CI_AI
	WHERE  CD.IdNotaventa = '+convert(varchar(100),@Id)+' AND CD.nvLinea = '+convert(varchar(100),@nroLinea)+' AND CD.CodProd = '''+@codProd+'''
	'
exec (@query)

 end


end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_COT_Vendedores]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_COT_Vendedores]
@pv_BaseDatos varchar(100)
as
begin

declare @query nvarchar(MAX)

SELECT @query='
	SELECT DISTINCT ven.VenCod, ven.VenDes as Nombre
     FROM ['+@pv_BaseDatos+'].[softland].[cwtvend] AS ven INNER JOIN
     DS_UsuarioEmpresa AS u ON u.VenCod COLLATE SQL_Latin1_General_CP1_CI_AI = ven.VenCod LEFT OUTER JOIN
     DS_Usuarios ON u.Idusuario = DS_Usuarios.ID
	 where DS_Usuarios.Estado = 1 and tipoUsuario <> 3
	 order by ven.VenCod
'
exec(@query)
 end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetInformeVentasMesAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetInformeVentasMesAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[SP_GET_DASH_GetInformeVentasMesAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
					sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaMesAnterior
					FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
							ON GMOVI.CODPROD = PROD.CODPROD 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
							ON GMOVI.KIT = PKIT.CODPROD 
						INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
							ON GMOVI.NROINT = GSAEN.NROINT 
							AND GSAEN.TIPO = GMOVI.Tipo 
						left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
							ON gsaen.codaux = cwt.codaux 
						left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
							ON vendedor.VenCod = GSAEN.CodVendedor  
						left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
							ON cl.CodAux = GSAEN.CodAux  
						left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
							ON descCl.CatCod = cl.CatCli  
						left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
							ON grupo.CodGrupo = prod.CodGrupo  
						left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
							ON subGrupo.CodSubGr = prod.CodSubGr  
						left join [' + @pv_BaseDatos + '].softland.cwtmone mone
							on GSAEN.CodMoneda = mone.CodMon
					WHERE	GSAEN.tipo!=''s''
					AND		GSAEN.tipo!=''a''
					AND		GSAEN.tipo!=''e''
					AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
					AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
					sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaMesAnterior
					FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
							ON GMOVI.CODPROD = PROD.CODPROD 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
							ON GMOVI.KIT = PKIT.CODPROD 
						INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
							ON GMOVI.NROINT = GSAEN.NROINT 
							AND GSAEN.TIPO = GMOVI.Tipo 
						left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
							ON gsaen.codaux = cwt.codaux 
						left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
							ON vendedor.VenCod = GSAEN.CodVendedor  
						left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
							ON cl.CodAux = GSAEN.CodAux  
						left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
							ON descCl.CatCod = cl.CatCli  
						left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
							ON grupo.CodGrupo = prod.CodGrupo  
						left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
							ON subGrupo.CodSubGr = prod.CodSubGr  
						left join [' + @pv_BaseDatos + '].softland.cwtmone mone
							on GSAEN.CodMoneda = mone.CodMon
					WHERE	GSAEN.tipo!=''s''
					AND		GSAEN.tipo!=''a''
					AND		GSAEN.tipo!=''e''
					AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
					AND		GSAEN.Estado = ''V''
					AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetPeriodoAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetPeriodoAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[SP_GET_DASH_GetPeriodoAnterior]
@pv_BaseDatos AS varchar(100)
AS
DECLARE @query AS nvarchar(max)
SELECT @query = ''

SELECT @query = @query + '
SELECT
				month(GSAEN.Fecha) as mesAnterior, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR ( dateadd(year,-1,getdate()) ),''-01-'',''01''),120) AND convert(datetime,CONCAT (YEAR ( dateadd(year,-1,getdate()) ),''-12-'',''31''),120)
				AND		GSAEN.Estado = ''V''
				group by month(GSAEN.Fecha)
				order by month(GSAEN.Fecha)
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetPeriodoTercero]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetPeriodoTercero]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[SP_GET_DASH_GetPeriodoTercero]
@pv_BaseDatos AS varchar(100)
AS
DECLARE @query AS nvarchar(max)
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				month(GSAEN.Fecha) as mesTercero, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalTercero
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR ( dateadd(year,-2,getdate()) ),''-01-'',''01''),120) AND convert(datetime,CONCAT (YEAR ( dateadd(year,-2,getdate()) ),''-12-'',''31''),120)
				AND		GSAEN.Estado = ''V''
				group by month(GSAEN.Fecha)
				order by month(GSAEN.Fecha)
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetVentaTotalDia]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetVentaTotalDia]               			*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[SP_GET_DASH_GetVentaTotalDia]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
begin
DECLARE @query AS nvarchar(max)

declare @dia varchar(25)
set @dia = (select datename(dw,getdate()))

SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalDia
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e'' '
				if(@dia = 'Lunes' or @dia = 'Monday')begin
				select @query = @query + 'AND GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-3,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)'
				end
				else begin
				select @query = @query + 'AND GSAEN.Fecha = convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) '
				end
				if(@pv_VenCod = '-3') begin
				select @query = @query + 'AND GSAEN.Estado = ''V'' '
				end
				else begin
				select @query = @query + 'AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + ''' '
				end
				select @query = @query + 'ORDER BY VentaTotalDia DESC'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetVentaTotalDiaAnterior]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetVentaTotalDiaAnterior]    			*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[SP_GET_DASH_GetVentaTotalDiaAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT    
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
				ORDER BY VentaTotalDiaAnterior DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT    
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				ORDER BY VentaTotalDiaAnterior DESC
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DatosCorreoVend]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_GET_DatosCorreoVend]
@NvNumero int,
 @pv_BaseDatos varchar(100)
as
SELECT	dnv.VenCod
,		u.email
,		u.ContrasenaCorreo
FROM	dbo.DS_NotasVenta dnv
	JOIN ds_usuarios u
		ON u.Id in (select sub_a.idUsuario from ds_usuarioEmpresa sub_a inner join ds_empresa sub_b on sub_a.idempresa = sub_b.id where sub_b.basedatos = @pv_BaseDatos and sub_a.VenCod = dnv.VenCod)
WHERE	dnv.NVNumero = @NvNumero

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_EnvioCorreo_PorCentroCosto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_EnvioCorreo_PorCentroCosto]
	@pv_CodigoCC varchar (100),
	@pi_IdEmpresa int
AS
BEGIN
	select	CodigoCC 
	,		IdEmpresa
	,		Email
	from	DS_EnvioCorreo_PorCentroCosto
	where	CodigoCC = @pv_CodigoCC
	and		IdEmpresa = @pi_IdEmpresa
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_ExisteCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_ExisteCliente]
@CodAux varchar (15),
@pv_BaseDatos varchar (100)
AS
	DECLARE @query varchar (max)
	SELECT @query = ''
	SELECT @query = '

	DECLARE @existe int 

	SET @existe = (SELECT count(*) FROM ['+@pv_BaseDatos+'].softland.cwtauxi where CodAux = '''+@CodAux+''')
	if(@existe > 0)
	BEGIN
		SELECT Verificador = cast(1 AS bit),
		Mensaje = ''Cliente ya existe''
	END
	else
	BEGIN
		SELECT Verificador = cast(0 AS bit),
		Mensaje = ''Cliente no existe''
	END
	'
	EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_Giro]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_Giro]
@pv_BaseDatos varchar(100)
AS
DECLARE @query varchar(max)

SELECT @query = ''

SELECT @query = @query + '
SELECT GirCod,GirDes 
FROM ['+@pv_BaseDatos+'] .softland.cwtgiro c	
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_Grupos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_Grupos]
@pv_BaseDatos varchar (100)
AS
BEGIN
	DECLARE @query varchar (max)

	SELECT @query = ''

	SELECT @query = @query + '
		SELECT	CodigoGrupo = a.CodGrupo
		,		DescripcionGrupo = a.DesGrupo
		FROM	['+@pv_BaseDatos+'].softland.iw_tgrupo a
	'

	EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_ListaContactos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  Procedure [dbo].[SP_GET_ListaContactos]
(
	@vchrCodAux VARCHAR(50) = ''
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
	declare @query varchar(max)

	select @query = ''

	-- ==========================================================================================  
	-- Lista los clientes filtrados por Codigo Aux y RUT Aux  
	-- ==========================================================================================  
	select @query = @query + '
	SELECT	CodAux = contacto.CodAuc
	,		NomCon = contacto.[NomCon]
	,		FonCon = ISNULL(contacto.[FonCon],'''')
	,		EMail = ISNULL(contacto.EMail,'''')  
	FROM	[' + @pv_BaseDatos + '].[softland].[cwtaxco] contacto  
	WHERE	contacto.CodAuc = ''' + @vchrCodAux + '''
	
	union 

	select CodAux =CodAux collate SQL_Latin1_General_CP1_CI_AS
	,      NomCon = NomAux collate SQL_Latin1_General_CP1_CI_AS
	,      FonCon=''''
	,      Email = Email  collate SQL_Latin1_General_CP1_CI_AS
	 from DS_cwtauxi
	 where CodAux = ''' + @vchrCodAux + '''
	 '
	exec (@query)
end  

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_LOGIN]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_LOGIN]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE procedure [dbo].[SP_GET_LOGIN]  
	@Nombre varchar(15),       
	@Contrasena varchar(50)       
as 
begin
	set nocount on       
	
	select	u.Id
	,		u.Usuario
	,		u.TipoUsuario
	,		u.CodigoUsuario
	from	DS_Usuarios u       
	where	u.Usuario = @Nombre 
	and		u.Contrasena = @Contrasena 
	AND		u.Estado = 1  
	
	set nocount OFF       
end  

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_Menu]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_Menu]
@TipoUsuario INT
as  
SELECT m.Id_Menu,m.Clase,m.PieMenu,m.Titulo,m.[Action],m.Controller
from Menu m 
where m.TipoUsuario = @TipoUsuario and m.Activo = 1 order by m.Orden 

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_MenuII]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[SP_GET_MenuII] 
@IdUsuario INT
AS
BEGIN
  SELECT
    m.Id_Menu
   ,m.Clase
   ,m.PieMenu
   ,m.Titulo
   ,m.[Action]
   ,Controller = m.Controller
  FROM Menu m
  INNER JOIN DS_Acceso da
    ON da.IdMenu = m.Id_Menu
  WHERE da.UsuarioId = @IdUsuario
  AND m.TipoUsuario = 1
  AND m.Activo = 1
  ORDER BY m.Orden
END
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_Monedas]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_Monedas]
@pv_BaseDatos varchar (100)
AS
BEGIN
	DECLARE @query varchar (max)

	SELECT @query = ''

	SELECT @query = @query + '
		SELECT	CodigoMoneda = a.CodMon
		,		DescripcionMoneda = a.DesMon
		,		SimboloMoneda = a.SimMon
		,		DecimalesPrecioMoneda = convert(int, a.DecMonPre)
		FROM	['+@pv_BaseDatos+'].softland.cwtmone a
	'

	EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_NV_ObtenerCubicajePorProducto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_NV_ObtenerCubicajePorProducto](@codprod varchar(40), @bd varchar(100))
as
begin
	declare @strsql varchar(max)

	set @strsql = 'declare @cubicaje float; '
	set @strsql = @strsql + ' set @cubicaje =(select isnull(pesokgs,0) cubicaje  from [' + @bd + '].softland.iw_tprod where CodProd = '''+@codprod+''')'
	set @strsql = @strsql + ' select @cubicaje as Cubicaje'
	
	exec sp_sqlexec @strsql
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_NV_RutaArchivoAdjunto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_NV_RutaArchivoAdjunto]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[SP_GET_NV_RutaArchivoAdjunto]
@CodAux varchar(50)
as
select Ruta, CodAux from RutaArchivoAdjunto where CodAux = @CodAux --and Estado = 0
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_ObtenerNombreVendedor]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_ObtenerNombreVendedor]
@Codigo varchar(4),
@BaseDatos varchar(100)
as
begin
declare @query varchar(max)

	select	@query = '
    select VenDes 
	from ['+@BaseDatos+'].[softland].[cwtvend] where VenCod='''+@Codigo+'''
	'
--print @query
exec (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_ProductosNuevos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_GET_ProductosNuevos]  
 @pv_BaseDatos varchar(100)  
AS  
begin
	select	*
	from	DS_ProductosNuevos
end

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_SubGrupos]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_SubGrupos]
@pv_BaseDatos varchar (100)
AS
BEGIN
	DECLARE @query varchar (max)

	SELECT @query = ''

	SELECT @query = @query + '
		SELECT	CodigoSubGrupo = a.CodSubGr
		,		DescripcionSubGrupo = a.DesSubGr
		FROM	['+@pv_BaseDatos+'].softland.iw_tsubgr a
	'

	EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GET_ValidaNumOc]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GET_ValidaNumOc]
(
	@NumOc int
)
AS
BEGIN
	DECLARE @lb_Verificador BIT
	DECLARE @lv_Mensaje VARCHAR(MAX)
	
	SELECT	@lb_Verificador = 0
	,		@lv_Mensaje = 'NO EXISTE'

	IF EXISTS
		(
			SELECT	top 1 1
			FROM	dbo.DS_NotasVenta 
			WHERE	NumOC	 = @NumOc
		) BEGIN
		SELECT	@lb_Verificador = 1
		,		@lv_Mensaje = 'EXISTE'
	END
	
	SELECT	Verificador = @lb_Verificador
	,		Mensaje = @lv_Mensaje
END

GO
/****** Object:  StoredProcedure [dbo].[SP_GetCodProducto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetCodProducto]
@pv_BaseDatos varchar(100)
AS
DECLARE @query varchar (max)

SELECT @query = ''

SELECT @query = @query + '
	select
	Stock = ISNULL((select Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible
						FROM		emeltec.softland.IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro)) 
						WHERE		Fecha <= GETDATE()  
						and			CodProd = tp.CodProd 
						GROUP BY	CodProd), 0)
	,CodProd
	,DesProd
	FROM	Emeltec.softland.iw_tprod tp
	where (tp.Inventariable = -1 )
	and tp.Inactivo =0
	order by CodProd
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[SP_GetCodVendedor]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_GetCodVendedor]
@pv_BaseDatos varchar(100)
AS
DECLARE @query varchar (max)

SELECT @query = ''

SELECT @query = @query + '
	select	VenCod
	,		VenDes
	from	(
				select	VenCod
				,		VenDes
				,		esnumero = isnumeric(vencod)
				FROM	['+@pv_BaseDatos+'].softland.cwtvend
			) a
	order by esnumero desc, (case when esnumero = 1 then convert(int, vencod) else 999999999 end) asc
'

EXEC (@query)


GO
/****** Object:  StoredProcedure [dbo].[SP_INS_AgregaImpuestoNV]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_AgregaImpuestoNV]
(
	@pv_BaseDatos varchar (100)
,	@pv_nvNumero varchar (100)
,	@pi_IdNotaVenta varchar (100)
)
AS
BEGIN
	DECLARE @query1 nvarchar(max);

	set	@query1 = ''

	set	@query1 = @query1 + '
	declare @codprod varchar(max)
	declare @codImpuesto varchar(max)
	declare @valorImpuesto int

	set @codImpuesto = ''''
	set @valorImpuesto = 0


	declare @IdNotaVenta int
	declare @IdNotaVentaDetalle int
	declare @nvSubTotal int

	declare @total_afecto int
	declare @total_impuesto int

	set @IdNotaVenta = ' + convert(varchar(20), @pi_IdNotaVenta) + '

	declare cursor_lineas cursor for
		select	Id
		,		nvSubTotal
		,		codprod
		from	DS_notasVentaDetalle
		where	idNotaVenta = @IdNotaVenta

	open cursor_lineas 

	fetch next from cursor_lineas 
	into @IdNotaVentaDetalle, @nvSubTotal, @codprod

	select	@total_afecto = 0

	while @@fetch_status = 0 begin
		SELECT top 1 @codImpuesto = CodImpto FROM  [' + @pv_BaseDatos + '].[softland].[iw_timprod] where codprod = @codprod
		SELECT top 1 @valorImpuesto = ValPctIni FROM  [' + @pv_BaseDatos + '].[softland].[iw_timpval] where CodImpto = @codImpuesto and getdate() between FecIniVig and FecFinVig

		select	@total_afecto = (@total_afecto + @nvSubTotal)
	
		fetch next from cursor_lineas 
		into @IdNotaVentaDetalle, @nvSubTotal, @codprod
	end

	select @total_impuesto = (@total_afecto * @valorImpuesto) / 100

	close cursor_lineas 
	deallocate cursor_lineas 

	insert into [' + @pv_BaseDatos + '].[softland].[nw_impto](nvnumero, codimpto, valpctini, afectoImpto, impto)
	select	' + @pv_nvNumero + ', @codImpuesto, @valorImpuesto, @total_afecto, @total_impuesto
	
	select	Verificador = cast(1 as bit)
	,		Mensaje = ''Se agrega impuesto''
	'

	exec (@query1)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_COT_AgregarDireccionDespacho]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_COT_AgregarDireccionDespacho]
(
	@pv_CodAux varchar(500)
,	@pv_DirDch varchar(500)
,	@pv_ComDch varchar(500)
,	@pv_NomDch varchar(500)
,	@pv_CiuDch varchar(500)
,	@pv_BaseDatos varchar(100)
)
AS  
BEGIN  
   declare @Sincronizado int
   select @Sincronizado=count(*) from DS_cwtauxi where CodAux=@pv_CodAux and sincronizado=0

   if @Sincronizado>0
   begin
        DECLARE @nomdch varchar(100)
   		DECLARE @paso BIT
		DECLARE @contador INT

		select @paso = 0
		select @contador = 1
      WHILE @paso = 0 begin
			SELECT	@nomdch = 'SUC ' + CONVERT(VARCHAR(MAX), @contador)
			IF NOT EXISTS (SELECT top 1 1 FROM DS_cwtauxd WHERE CodAxD = @pv_CodAux AND NomDch = @nomdch) BEGIN
				select	@paso = 1
			END
			select	@contador = @contador + 1
		end

      INSERT INTO DS_cwtauxd 
	  (CodAxD, NomDch, DirDch, ComDch, CiuDch, PaiDch, Fon1Dch, Fon2Dch, Fon3Dch, FaxDch, AteDch, ProviDch, RegionDch, CodPostalDch, Usuario, Proceso, FechaUlMod, Sistema, CodGLN, CodEspWalMart)
      VALUES 
	  (@pv_CodAux, @nomdch, @pv_DirDch, @pv_ComDch, @pv_CiuDch, '', '', '', '', '', '', '', 0, DEFAULT, '', '', GETDATE(), '', '', DEFAULT);

	  SELECT	Verificador = Cast(1 as bit), Mensaje = 'Se agrega direccion de despacho satisfactoriamente'
	end

    if @Sincronizado=0
	begin

		declare @query varchar(max)

		select @query = ''

		select @query = @query + '
		DECLARE @nomdch varchar(100)
		DECLARE @paso BIT
		DECLARE @contador INT

		select @paso = 0
		select @contador = 1


		WHILE @paso = 0 begin
			SELECT	@nomdch = ''SUC '' + CONVERT(VARCHAR(MAX), @contador)
			IF NOT EXISTS (SELECT top 1 1 FROM [' + @pv_BaseDatos + '].[softland].[cwtauxd] WHERE CodAxD = ''' + @pv_CodAux + ''' AND NomDch = @nomdch) BEGIN
				select	@paso = 1
			END

			select	@contador = @contador + 1
		end

		--IF NOT EXISTS (SELECT TOP 1 1 FROM [' + @pv_BaseDatos + '].[softland].[cwtauxd] WHERE CodAxD = ''' + @pv_CodAux + ''' AND NomDch = ''' + @pv_NomDch + ''') BEGIN
			INSERT INTO [' + @pv_BaseDatos + '].[softland].[cwtauxd]
			(
				CodAxD
			,	DirDch
			,	ComDch
			,	NomDch
			,	CiuDch
			)
			VALUES
			(
				''' + @pv_CodAux + '''
			,	''' + @pv_DirDch + '''
			,	''' + @pv_ComDch + '''
			,	@nomdch
			,	''' + @pv_CiuDch + '''
			)

			SELECT	Verificador = Cast(1 as bit)
			,		Mensaje = ''Se agrega direccion de despacho satisfactoriamente''
		--END
		--ELSE BEGIN
		--	SELECT	Verificador = Cast(0 as bit)
		--	,		Mensaje = ''Direccion ingresada ya existe''
		--END
		'

		EXEC (@query)
	end

END  

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_COT_Cabecera]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_COT_Cabecera]
(
	/*--------------------------- CAMPOS DISOFI ---------------------------*/
	@pi_IdEmpresaInterna int
,	@pv_EstadoNP [varchar](1) = 'P'
,	@pv_BaseDatos [varchar](100)
,	@pb_InsertaDisofi BIT
,	@pb_InsertaSoftland BIT
,	@pi_IdNotaVenta INT = null
	/*--------------------------- CAMPOS SOFTLAND ---------------------------*/
,	@pi_NVNumero [int]
,	@pd_nvFem [datetime] = NULL
,	@pv_nvEstado [varchar](1) = NULL
,	@pi_nvEstFact [int] = NULL
,	@pi_nvEstDesp [int] = NULL
,	@pi_nvEstRese [int] = NULL
,	@pi_nvEstConc [int] = NULL
,	@pi_CotNum [int] = NULL
,	@pv_NumOC [varchar](12)
,	@pd_nvFeEnt [datetime] = NULL
,	@pv_CodAux [varchar](10) = NULL
,	@pv_VenCod [varchar](4) = NULL
,	@pv_CodMon [varchar](2) = NULL
,	@pv_CodLista [varchar](3) = NULL
,	@pt_nvObser [text] = NULL
,	@pv_nvCanalNV [varchar](3) = NULL
,	@pv_CveCod [varchar](3) = NULL
,	@pv_NomCon [varchar](30) = NULL
,	@pv_CodiCC [varchar](8) = NULL
,	@pv_CodBode [varchar](10) = NULL
,	@pf_nvSubTotal [float] = NULL
,	@pf_nvPorcDesc01 [float] = NULL
,	@pf_nvDescto01 [float] = NULL
,	@pf_nvPorcDesc02 [float] = NULL
,	@pf_nvDescto02 [float] = NULL
,	@pf_nvPorcDesc03 [float] = NULL
,	@pf_nvDescto03 [float] = NULL
,	@pf_nvPorcDesc04 [float] = NULL
,	@pf_nvDescto04 [float] = NULL
,	@pf_nvPorcDesc05 [float] = NULL
,	@pf_nvDescto05 [float] = NULL
,	@pf_nvMonto [float] = NULL
,	@pd_nvFeAprob [datetime] = NULL
,	@pi_NumGuiaRes [int] = NULL
,	@pf_nvPorcFlete [float] = NULL
,	@pf_nvValflete [float] = NULL
,	@pf_nvPorcEmb [float] = NULL
,	@pf_nvValEmb [float] = NULL
,	@pf_nvEquiv [float] = NULL
,	@pf_nvNetoExento [float] = NULL
,	@pf_nvNetoAfecto [float] = NULL
,	@pf_nvTotalDesc [float] = NULL
,	@pv_ConcAuto [varchar](1) = NULL
,	@pv_CodLugarDesp [varchar](30) = NULL
,	@pv_SolicitadoPor [varchar](30) = NULL
,	@pv_DespachadoPor [varchar](30) = NULL
,	@pv_Patente [varchar](9) = NULL
,	@pv_RetiradoPor [varchar](30) = NULL
,	@pv_CheckeoPorAlarmaVtas [varchar](1) = NULL
,	@pi_EnMantencion [int] = NULL
,	@pv_Usuario [varchar](8) = NULL
,	@pv_UsuarioGeneraDocto [varchar](8) = NULL
,	@pd_FechaHoraCreacion [datetime] = NULL
,	@pv_Sistema [varchar](2) = NULL
,	@pv_ConcManual [varchar](1) = NULL
,	@pv_RutSolicitante [varchar](20) = NULL
,	@pv_proceso [varchar](50) = NULL
,	@pf_TotalBoleta [float] = NULL
,	@pi_NumReq [int]
,	@pv_CodVenWeb [varchar](50) = NULL
,	@pv_CodBodeWms [varchar](10) = NULL
,	@pv_CodLugarDocto [varchar](30) = NULL
,	@pv_RutTransportista [varchar](20) = NULL
,	@pv_Cod_Distrib [varchar](10) = NULL
,	@pv_Nom_Distrib [varchar](60) = NULL
,	@pi_MarcaWG [int] = NULL
,	@pb_ErrorAprobador [bit] = NULL
,	@pv_ErrorAprobadorMensaje varchar(max) = NULL
,	@pv_IdCorreoManag int
,   @iniciales char(2)
)
AS
BEGIN
	DECLARE	@VerificadorDisofi BIT
	DECLARE	@MensajeDisofi VARCHAR(MAX)

	DECLARE	@VerificadorSoftland BIT
	DECLARE	@MensajeSoftland VARCHAR(MAX)

	SELECT	@VerificadorDisofi = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'
	,		@VerificadorSoftland = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'

	IF @pb_InsertaDisofi = 1 BEGIN
		INSERT INTO [dbo].[DS_Cotizacion]
		(
			IdEmpresaInterna
		,	EstadoNP
		,	NVNumero
		,	nvFem
		,	nvEstado
		,	nvEstFact
		,	nvEstDesp
		,	nvEstRese
		,	nvEstConc
		,	CotNum
		,	NumOC
		,	nvFeEnt
		,	CodAux
		,	VenCod
		,	CodMon
		,	CodLista
		,	nvObser
		,	nvCanalNV
		,	CveCod
		,	NomCon
		,	CodiCC
		,	CodBode
		,	nvSubTotal
		,	nvPorcDesc01
		,	nvDescto01
		,	nvPorcDesc02
		,	nvDescto02
		,	nvPorcDesc03
		,	nvDescto03
		,	nvPorcDesc04
		,	nvDescto04
		,	nvPorcDesc05
		,	nvDescto05
		,	nvMonto
		,	nvFeAprob
		,	NumGuiaRes
		,	nvPorcFlete
		,	nvValflete
		,	nvPorcEmb
		,	nvValEmb
		,	nvEquiv
		,	nvNetoExento
		,	nvNetoAfecto
		,	nvTotalDesc
		,	ConcAuto
		,	CodLugarDesp
		,	SolicitadoPor
		,	DespachadoPor
		,	Patente
		,	RetiradoPor
		,	CheckeoPorAlarmaVtas
		,	EnMantencion
		,	Usuario
		,	UsuarioGeneraDocto
		,	FechaHoraCreacion
		,	Sistema
		,	ConcManual
		,	RutSolicitante
		,	proceso
		,	TotalBoleta
		,	NumReq
		,	CodVenWeb
		,	CodBodeWms
		,	CodLugarDocto
		,	RutTransportista
		,	Cod_Distrib
		,	Nom_Distrib
		,	MarcaWG
		,	ErrorAprobador
		,	ErrorAprobadorMensaje
		)
		VALUES
		(
			@pi_IdEmpresaInterna
		,	@pv_EstadoNP
		,	@pi_NVNumero
		,	@pd_nvFem
		,	@pv_nvEstado
		,	@pi_nvEstFact
		,	@pi_nvEstDesp
		,	@pi_nvEstRese
		,	@pi_nvEstConc
		,	@pi_CotNum
		,	@pv_NumOC
		,	@pd_nvFeEnt
		,	@pv_CodAux
		,	@pv_VenCod
		,	@pv_CodMon
		,	@pv_CodLista
		,	@pt_nvObser
		,	@pv_nvCanalNV
		,	@pv_CveCod
		,	@pv_NomCon
		,	@pv_CodiCC
		,	@pv_CodBode
		,	@pf_nvSubTotal
		,	@pf_nvPorcDesc01
		,	@pf_nvDescto01
		,	@pf_nvPorcDesc02
		,	@pf_nvDescto02
		,	@pf_nvPorcDesc03
		,	@pf_nvDescto03
		,	@pf_nvPorcDesc04
		,	@pf_nvDescto04
		,	@pf_nvPorcDesc05
		,	@pf_nvDescto05
		,	@pf_nvMonto
		,	@pd_nvFeAprob
		,	@pi_NumGuiaRes
		,	@pf_nvPorcFlete
		,	@pf_nvValflete
		,	@pf_nvPorcEmb
		,	@pf_nvValEmb
		,	@pf_nvEquiv
		,	@pf_nvNetoExento
		,	@pf_nvNetoAfecto
		,	@pf_nvTotalDesc
		,	@pv_ConcAuto
		,	@pv_CodLugarDesp
		,	@pv_SolicitadoPor
		,	@pv_DespachadoPor
		,	@pv_Patente
		,	@pv_RetiradoPor
		,	@pv_CheckeoPorAlarmaVtas
		,	@pi_EnMantencion
		,	@pv_Usuario
		,	@pv_UsuarioGeneraDocto
		,	@pd_FechaHoraCreacion
		,	@pv_Sistema
		,	@pv_ConcManual
		,	@pv_RutSolicitante
		,	@pv_proceso
		,	@pf_TotalBoleta
		,	@pi_NumReq
		,	@pv_CodVenWeb
		,	@pv_CodBodeWms
		,	@pv_CodLugarDocto
		,	@pv_RutTransportista
		,	@pv_Cod_Distrib
		,	@pv_Nom_Distrib
		,	@pi_MarcaWG
		,	@pb_ErrorAprobador
		,	@pv_ErrorAprobadorMensaje
		)

		SELECT	@pi_IdNotaVenta = @@identity
		
		SELECT	@VerificadorDisofi = 1
		,		@MensajeDisofi = 'Se agrego en disofi satisfactoriamente'

		UPDATE	[dbo].[DS_NotasVenta]
		set		nvObser = ('N. Int: ' + convert(varchar(20), @pi_IdNotaVenta) + ' Obs: ' + convert(varchar(max), isnull(nvObser, '')))
		where	Id = @pi_IdNotaVenta
	END
	
	--exec SP_INS_DatosCorreoManag @pi_IdNotaVenta,@pv_IdCorreoManag

	--INI FOLIO COTIZACION
	    --INI REINICIO FOLIO
		declare @Fecha datetime
		declare @FechaIni datetime
		declare @reinicio int
		--set @Fecha= @pd_nvFem--GETDATE()
		set @FechaIni=DATEADD(dd,-(DAY(@Fecha)-1),@Fecha)
		select @reinicio = Reiniciado from DS_Cot_Folios where Vendedor= @pv_VenCod
		if (@Fecha = @FechaIni) and @reinicio=0
			update DS_Cot_Folios set NroCotizacion=0, Reiniciado=1 where Vendedor= @pv_VenCod
		--FIN REINICIO FOLIO



		declare @NroFinalCotizacion nvarchar(20)
		declare @anno char(4)
		declare @mes  char(4)
		declare @folioCotizacion int
		set @anno = YEAR(getdate())
		set @mes = case when MONTH(getdate())<10 then '0'+ convert(char(1),MONTH(GETDATE())) end 
		select @folioCotizacion=isnull(max(NroCotizacion),0)+1 from DS_Cot_Folios where Vendedor=@pv_VenCod
		if @folioCotizacion<10
		set @NroFinalCotizacion=@anno+LTRIM(RTRIM(@mes))+ rtrim(@Iniciales) + '00' + CONVERT(char(3),@folioCotizacion)
		if @folioCotizacion>=10 and @folioCotizacion<100
		set @NroFinalCotizacion=@anno+LTRIM(RTRIM(@mes))+ rtrim(@Iniciales) + '0' + CONVERT(char(3),@folioCotizacion)
		if @folioCotizacion>=100
		set @NroFinalCotizacion=@anno+LTRIM(RTRIM(@mes))+ rtrim(@Iniciales) + CONVERT(char(3),@folioCotizacion)


		DECLARE @ExisteVendedor int
		select @ExisteVendedor=count(*) from DS_Cot_Folios where Vendedor=@pv_VenCod

		if @ExisteVendedor>0
			BEGIN
			UPDATE DS_Cot_Folios 
			SET NroCotizacion = (select isnull(max(NroCotizacion),0)+1 from DS_Cot_Folios where Vendedor=@pv_VenCod ),
			NroFinalCotizacion=@NroFinalCotizacion,
			Reiniciado=0
			WHERE Vendedor=@pv_VenCod
			END

		ELSE

			BEGIN
				INSERT INTO DS_Cot_Folios (Vendedor, NroCotizacion,Iniciales,NroFinalCotizacion,Reiniciado)
				VALUES (@pv_VenCod, 1,@Iniciales,@NroFinalCotizacion,0)
			END
	--FIN FOLIO COTIZACION

	SELECT	IdNotaVenta = @pi_IdNotaVenta
	,		NVNumero = @pi_NVNumero
	,		VerificadorDisofi = @VerificadorDisofi
	,		MensajeDisofi = @MensajeDisofi
	,		VerificadorSoftland = @VerificadorSoftland
	,		MensajeSoftland = @MensajeSoftland
	,       NroFinalCotizacion = @NroFinalCotizacion

END
GO
/****** Object:  StoredProcedure [dbo].[SP_INS_COT_Cliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_COT_Cliente]
@CodAux varchar (15),
@NomAux varchar(60),
@RutAux varchar (20),
@FonAux1 varchar (15),
@Email varchar (250),
@GirAux varchar (3),
@DirAux varchar (250),
@pv_BaseDatos varchar (100),
@EmailDte varchar (150),
@VenCod varchar (25),
@ComAux varchar(7),
@CiuAux varchar(7)
AS

BEGIN
	DECLARE @existe int 
	
	SET @existe = (SELECT count(*) FROM DS_cwtauxi where CodAux = @CodAux)
	if(@existe = 0)
	BEGIN
		INSERT INTO DS_cwtauxi 
		(Codaux,NomAux,NoFAux,DirAux,RutAux,ComAux,CiuAux,ActAux,GirAux,FonAux1,ClaCli,ClaPro,ClaEmp,ClaSoc,ClaDis,ClaOtr,Bloqueado,Email,eMailDTE,esReceptorDTE,sincronizado) 
		values
		(@CodAux,@NomAux,@NomAux,@DirAux,@RutAux,@ComAux,@CiuAux,'S',@GirAux,@FonAux1,'S','N','N','N','N','N','N',@Email,@EmailDte,'N',0);

		SELECT Verificador = cast(1 AS bit),
		Mensaje = 'Cliente Creado'
	END
	else
	BEGIN
		SELECT Verificador = cast(0 AS bit),
		Mensaje = 'Cliente ya Existe'
	END
END	

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_COT_ClienteSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[SP_INS_COT_ClienteSoftland]
@CodCliente varchar(10),
@BaseDatos varchar(100)
as
begin
	DECLARE @lb_Verificador BIT
	DECLARE @lv_Mensaje VARCHAR(MAX)
	declare @Sincronizado int

	select @Sincronizado = count(*) from DS_cwtauxi where CodAux=@CodCliente and sincronizado=0
	declare @query varchar(max)

	select @query = ''

	if @Sincronizado>0 begin
	select @query = @query + '
		INSERT INTO ['+@BaseDatos+'].[softland].[cwtauxi]
		(CodAux, NomAux, NoFAux, RutAux, ActAux, GirAux, ComAux, CiuAux, PaiAux, ProvAux, DirAux, DirNum, FonAux1, FonAux2, FonAux3, FaxAux1, FaxAux2, ClaCli, ClaPro, ClaEmp, ClaSoc, ClaDis, ClaOtr, DiaPlazo, Bloqueado, EMail, Casilla, WebSite, Notas, Region, TipoSaludo, DirDpto, DirOtro, CodPostal, CodAreaFon, AnexoFon, CodAreaFax, FechaNacim, Username, Password, PalabraSecreta, PreguntaSecreta, ClienteDesde, TipoUsuario, eMailDTE, esReceptorDTE, BloqueadoPro, ClaPros, CodCamp, CodOrigen, Id_RecepExtranjero, PaisRecepExtranjero, Usuario, Sistema, Proceso, FechaUlMod, CtaCliente, CtaCliMonExt, PasswordResetToken)
		SELECT CodAux, NomAux, NoFAux, RutAux, ActAux, GirAux, ComAux, CiuAux, PaiAux, ProvAux, DirAux, DirNum, FonAux1, FonAux2, FonAux3, FaxAux1, FaxAux2, ClaCli, ClaPro, ClaEmp, ClaSoc, ClaDis, ClaOtr, DiaPlazo, Bloqueado, EMail, Casilla, WebSite, Notas, Region, TipoSaludo, DirDpto, DirOtro, CodPostal, CodAreaFon, AnexoFon, CodAreaFax, FechaNacim, Username, Password, PalabraSecreta, PreguntaSecreta, ClienteDesde, TipoUsuario, eMailDTE, esReceptorDTE, BloqueadoPro, ClaPros, CodCamp, CodOrigen, Id_RecepExtranjero, PaisRecepExtranjero, Usuario, Sistema, Proceso, FechaUlMod, CtaCliente, CtaCliMonExt, PasswordResetToken
		FROM DS_cwtauxi dc
		WHERE dc.CodAux='''+@CodCliente+'''

		UPDATE DS_cwtauxi SET sincronizado=1 WHERE CodAux='''+@CodCliente+'''
		
		INSERT INTO ['+@BaseDatos+'].[softland].[cwtauxd] 
		(CodAxD, NomDch, DirDch, ComDch, CiuDch, PaiDch, Fon1Dch, Fon2Dch, Fon3Dch, FaxDch, AteDch, ProviDch, RegionDch, CodPostalDch, Usuario, Proceso, FechaUlMod, Sistema, CodGLN, CodEspWalMart)
		SELECT * FROM DS_cwtauxd dc WHERE dc.CodAxD='''+@CodCliente+'''

		INSERT INTO softland.cwtaxco (CodAuc, NomCon, CarCon, FonCon, FonCon2, FonCon3, FaxCon, Casilla, Email, IDNotas, TipoSaludo, Usuario, Proceso, FechaUlMod, Sistema, FechaUltEnvCorreo)
		select * from DS_cwtaxco WHERE CodAuc='''+@CodCliente+'''

  '
	  set 	@lb_Verificador = 1
	  set   @lv_Mensaje = 'Cliente ingresado softland exitosamente'
  end 
  else 
  begin
	  set @lb_Verificador = 1
	  set @lv_Mensaje = 'Cliente existe'
  end

 exec (@query)
 SELECT	@lb_Verificador Verificador, @lv_Mensaje Mensaje
end

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_COT_Consideracion]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_INS_COT_Consideracion]
@Titulo char(200),
@Consideracion nvarchar(max)
as
begin
  INSERT INTO DS_Consideracion (Consideracion, Titulo)
  VALUES (@Consideracion, @Titulo)


SELECT	Verificador = cast(1 as bit), Mensaje = 'Graba exitosamente'
end

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_COT_Detalle]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_COT_Detalle]
(
	/*--------------------------- CAMPOS DISOFI ---------------------------*/
	@pv_BaseDatos [varchar](100)
,	@pb_InsertaDisofi BIT
,	@pb_InsertaSoftland BIT
,	@pi_IdNotaVenta INT
	/*--------------------------- CAMPOS SOFTLAND ---------------------------*/
,	@pi_NVNumero int
,	@pf_nvLinea float
,	@pf_nvCorrela float = NULL
,	@pd_nvFecCompr datetime = NULL
,	@pv_CodProd varchar(20) = NULL
,	@pf_nvCant float = NULL
,	@pf_nvPrecio float = NULL
,	@pf_nvEquiv float = NULL
,	@pf_nvSubTotal float = NULL
,	@pf_nvDPorcDesc01 float = NULL
,	@pf_nvDDescto01 float = NULL
,	@pf_nvDPorcDesc02 float = NULL
,	@pf_nvDDescto02 float = NULL
,	@pf_nvDPorcDesc03 float = NULL
,	@pf_nvDDescto03 float = NULL
,	@pf_nvDPorcDesc04 float = NULL
,	@pf_nvDDescto04 float = NULL
,	@pf_nvDPorcDesc05 float = NULL
,	@pf_nvDDescto05 float = NULL
,	@pf_nvTotDesc float = NULL
,	@pf_nvTotLinea float = NULL
,	@pf_nvCantDesp float = NULL
,	@pf_nvCantProd float = NULL
,	@pf_nvCantFact float = NULL
,	@pf_nvCantDevuelto float = NULL
,	@pf_nvCantNC float = NULL
,	@pf_nvCantBoleta float = NULL
,	@pf_nvCantOC float = NULL
,	@pt_DetProd text = NULL
,	@pv_CheckeoMovporAlarmaVtas varchar(1) = NULL
,	@pv_KIT varchar(20) = NULL
,	@pi_CodPromocion int = NULL
,	@pv_CodUMed varchar(6) = NULL
,	@pf_CantUVta float = NULL
,	@pv_Partida varchar(20) = NULL
,	@pv_Pieza varchar(20) = NULL
,	@pd_FechaVencto datetime = NULL
,	@pf_CantidadKit float
,	@pi_MarcaWG int = NULL
,	@pf_PorcIncidenciaKit float
)
AS
BEGIN
	DECLARE	@IdDetalleNotaVenta INT
	DECLARE	@VerificadorDisofi BIT
	DECLARE	@MensajeDisofi VARCHAR(MAX)

	DECLARE	@VerificadorSoftland BIT
	DECLARE	@MensajeSoftland VARCHAR(MAX)

	SELECT	@VerificadorDisofi = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'
	,		@VerificadorSoftland = 0
	,		@MensajeDisofi = 'No se ejecuto la insercion en disofi'
	
	DECLARE @query1 nvarchar(max);
	DECLARE @ParmDefinition nvarchar(500);  
	
	 IF @pt_DetProd IS NULL or CONVERT(VARCHAR(MAX), @pt_DetProd) = '' BEGIN  
	  SELECT @query1 = N'SELECT @DetProdOUT = convert(varchar(max), desprod) FROM ' + @pv_BaseDatos + '.[softland].[iw_tprod] WHERE CodProd = ''' + @pv_CodProd + '''';  
  
	  SET @ParmDefinition = N'@DetProdOUT varchar(max) OUTPUT';     
  
	  EXEC sp_executesql @query1, @ParmDefinition, @DetProdOUT=@pt_DetProd OUTPUT;    
	 END  
	 IF @pv_CodUMed IS NULL or @pv_CodUMed = '' BEGIN  
	  SELECT @query1 = N'SELECT @CodUMedOUT = convert(varchar(max), codumed) FROM ' + @pv_BaseDatos + '.[softland].[iw_tprod] WHERE CodProd = ''' + @pv_CodProd + '''';  
  
	  SET @ParmDefinition = N'@CodUMedOUT varchar(max) OUTPUT';     
  
	  EXEC sp_executesql @query1, @ParmDefinition, @CodUMedOUT=@pv_CodUMed OUTPUT;    
	 END  

	IF @pb_InsertaDisofi = 1 BEGIN
		INSERT INTO [dbo].[DS_CotizacionDetalle]
		(
			IdNotaVenta
		,	NVNumero
		,	nvLinea
		,	nvCorrela
		,	nvFecCompr
		,	CodProd
		,	nvCant
		,	nvPrecio
		,	nvEquiv
		,	nvSubTotal
		,	nvDPorcDesc01
		,	nvDDescto01
		,	nvDPorcDesc02
		,	nvDDescto02
		,	nvDPorcDesc03
		,	nvDDescto03
		,	nvDPorcDesc04
		,	nvDDescto04
		,	nvDPorcDesc05
		,	nvDDescto05
		,	nvTotDesc
		,	nvTotLinea
		,	nvCantDesp
		,	nvCantProd
		,	nvCantFact
		,	nvCantDevuelto
		,	nvCantNC
		,	nvCantBoleta
		,	nvCantOC
		,	DetProd
		,	CheckeoMovporAlarmaVtas
		,	KIT
		,	CodPromocion
		,	CodUMed
		,	CantUVta
		,	Partida
		,	Pieza
		,	FechaVencto
		,	CantidadKit
		,	MarcaWG
		,	PorcIncidenciaKit
		)
		VALUES
		(
			@pi_IdNotaVenta
		,	@pi_NVNumero
		,	@pf_nvLinea
		,	@pf_nvCorrela
		,	@pd_nvFecCompr
		,	@pv_CodProd
		,	@pf_nvCant
		,	@pf_nvPrecio
		,	@pf_nvEquiv
		,	@pf_nvSubTotal
		,	@pf_nvDPorcDesc01
		,	@pf_nvDDescto01
		,	@pf_nvDPorcDesc02
		,	@pf_nvDDescto02
		,	@pf_nvDPorcDesc03
		,	@pf_nvDDescto03
		,	@pf_nvDPorcDesc04
		,	@pf_nvDDescto04
		,	@pf_nvDPorcDesc05
		,	@pf_nvDDescto05
		,	@pf_nvTotDesc
		,	@pf_nvTotLinea
		,	@pf_nvCantDesp
		,	@pf_nvCantProd
		,	@pf_nvCantFact
		,	@pf_nvCantDevuelto
		,	@pf_nvCantNC
		,	@pf_nvCantBoleta
		,	@pf_nvCantOC
		,	@pt_DetProd
		,	@pv_CheckeoMovporAlarmaVtas
		,	@pv_KIT
		,	@pi_CodPromocion
		,	@pv_CodUMed
		,	@pf_CantUVta
		,	@pv_Partida
		,	@pv_Pieza
		,	@pd_FechaVencto
		,	@pf_CantidadKit
		,	@pi_MarcaWG
		,	@pf_PorcIncidenciaKit
		)

		SELECT	@IdDetalleNotaVenta = @@identity
		
		SELECT	@VerificadorDisofi = 1
		,		@MensajeDisofi = 'Se agrego en disofi satisfactoriamente'
	END

	SELECT	IdNotaVenta = @pi_IdNotaVenta
	,		IdDetalleNotaVenta = @IdDetalleNotaVenta
	,		NVNumero = @pi_NVNumero
	,		VerificadorDisofi = @VerificadorDisofi
	,		MensajeDisofi = @MensajeDisofi
	,		VerificadorSoftland = @VerificadorSoftland
	,		MensajeSoftland = @MensajeSoftland
END

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_COT_ProductoNuevoDisofi]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_INS_COT_ProductoNuevoDisofi]
(
	@pv_BaseDatos varchar (100)
,	@pv_CodProdAntiguo	varchar	(20)
,	@pv_CodProd	varchar	(20)
,	@pv_DesProd	varchar	(60)
,	@pv_CodGrupo	varchar	(10)
,	@pv_CodSubGr	varchar	(10)
,	@pf_PrecioVta float
,	@pv_CodMon	varchar	(2)
,   @pv_CodFabrica varchar(60)
)
AS
BEGIN
	IF @pv_CodProdAntiguo = @pv_CodProd	BEGIN
		IF NOT EXISTS (SELECT TOP 1 1 FROM DS_ProductosNuevos WHERE BaseDatos = @pv_BaseDatos and CodProd = @pv_CodProd) BEGIN
			INSERT INTO DS_ProductosNuevos
			(
				BaseDatos
			,	CodProd
			,	DesProd
			,	DesProd2
			,	CodRapido
			,	CodBarra
			,	CodUMed
			,	Origen
			,	CodMonOrig
			,	CodGrupo
			,	CodSubGr
			,	CodCateg
			,	CodMonPVta
			,	PrecioVta
			,	PrecioBol
			,	FichaTec
			,	EsConfig
			,	FactorConfig
			,	Impuesto
			,	Inventariable
			,	EsSerie
			,	EsTallaColor
			,	EsPartida
			,	EsCaducidad
			,	EsPieza
			,	CantPieza
			,	PesoKgs
			,	CtaActivo
			,	CtaVentas
			,	CtaGastos
			,	CtaCosto
			,	FecUltCom
			,	ValorUltCom
			,	CostoRep
			,	FecCostoRep
			,	FecCMonet
			,	ValorCMonet
			,	NivMin
			,	NivRep
			,	NivMax
			,	Inamovible
			,	ManejaDim
			,	Ancho
			,	esUbicPar
			,	CtaDevolucion
			,	TipProd
			,	esParaVenta
			,	esParaCompra
			,	EsTalla
			,	EsColor
			,	MetodoCosteo
			,	CodUMedVta1
			,	EquivUMVta1
			,	PrecioVtaUM1
			,	PrecioBolUM1
			,	CodUMedVta2
			,	EquivUMVta2
			,	PrecioVtaUM2
			,	PrecioBolUM2
			,	UMDefecto
			,	ManProdAnticipo
			,	ImprimeEnBoleta
			,	EsParaAutoservicio
			,	Inactivo
			,	Usuario
			,	Proceso
			,	FechaUlMod
			)
			SELECT	BaseDatos = @pv_BaseDatos
			,		CodProd = @pv_CodProd
			,		DesProd = @pv_DesProd
			,		DesProd2 = @pv_CodFabrica
			,		CodRapido = ''
			,		CodBarra = ''
			,		CodUMed = 'UN'
			,		Origen = -1
			,		CodMonOrig = @pv_CodMon
			,		CodGrupo = @pv_CodGrupo
			,		CodSubGr = @pv_CodSubGr
			,		CodCateg = null
			,		CodMonPVta = @pv_CodMon
			,		PrecioVta = @pf_PrecioVta-- + (@pf_PrecioVta /0.48)
			,		PrecioBol = @pf_PrecioVta * 1.19
			,		FichaTec = ''
			,		EsConfig = ''
			,		FactorConfig = ''
			,		Impuesto = -1
			,		Inventariable = -1
			,		EsSerie = 0
			,		EsTallaColor = 0
			,		EsPartida = 0
			,		EsCaducidad = 0
			,		EsPieza = 0
			,		CantPieza = ''
			,		PesoKgs = 0
			,		CtaActivo = null
			,		CtaVentas = null
			,		CtaGastos = null
			,		CtaCosto = null
			,		FecUltCom = ''
			,		ValorUltCom = ''
			,		CostoRep = ''
			,		FecCostoRep = ''
			,		FecCMonet = ''
			,		ValorCMonet = ''
			,		NivMin = ''
			,		NivRep = ''
			,		NivMax = ''
			,		Inamovible = ''
			,		ManejaDim = ''
			,		Ancho = ''
			,		esUbicPar = ''
			,		CtaDevolucion = null
			,		TipProd = 'OT'
			,		esParaVenta = -1
			,		esParaCompra = 0
			,		EsTalla = 0
			,		EsColor = 0
			,		MetodoCosteo = 'P'
			,		CodUMedVta1 = null
			,		EquivUMVta1 = ''
			,		PrecioVtaUM1 = ''
			,		PrecioBolUM1 = ''
			,		CodUMedVta2 = null
			,		EquivUMVta2 = ''
			,		PrecioVtaUM2 = ''
			,		PrecioBolUM2 = ''
			,		UMDefecto = ''
			,		ManProdAnticipo = ''
			,		ImprimeEnBoleta = -1
			,		EsParaAutoservicio = 0
			,		Inactivo = 0
			,		Usuario = 'softland'
			,		Proceso = 'Ficha de Producto:Insert'
			,		FechaUlMod = GETDATE()
	
			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = 'Producto agregado'
		END
		ELSE BEGIN
			update	DS_ProductosNuevos
			set		CodProd = @pv_CodProd
			,		DesProd = @pv_DesProd
			,		CodMonOrig = @pv_CodMon
			,		CodGrupo = @pv_CodGrupo
			,		CodSubGr = @pv_CodSubGr
			,		CodMonPVta = @pv_CodMon
			,		PrecioVta = @pf_PrecioVta
			,		FechaUlMod = GETDATE()
			where	BaseDatos = @pv_BaseDatos
			and		CodProd = @pv_CodProdAntiguo
	
			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = 'Producto modificado'
		END
	END
	ELSE BEGIN
		update	DS_ProductosNuevos
		set		CodProd = @pv_CodProd
		,		DesProd = @pv_DesProd
		,		CodMonOrig = @pv_CodMon
		,		CodGrupo = @pv_CodGrupo
		,		CodSubGr = @pv_CodSubGr
		,		CodMonPVta = @pv_CodMon
		,		PrecioVta = @pf_PrecioVta
		where	BaseDatos = @pv_BaseDatos
		and		CodProd = @pv_CodProdAntiguo
	
		SELECT	Verificador = cast(1 as bit)
		,		Mensaje = 'Producto modificado'
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_INS_DatosCorreoManag]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[SP_INS_DatosCorreoManag]
@IdNotaVenta int,
@IdCorreoManag int
as
insert into nventa_manag (idNotaVenta,idCorreoManag) 
values (@IdNotaVenta,@IdCorreoManag)

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_NP_AprobadoPor]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_INS_NP_AprobadoPor]
@NVNumero int,
@nvId int,
@IdAprobador int,
@IdEmpresaInterna int,
@estado char(1)
as
begin

INSERT INTO DS_NotaVentaExtras 
  (IdNotaVenta,
   Cubicaje, 
   Peso, 
   TieneDescuento, 
   EsCotizacion, 
   FechaCierre, 
   nroSolicitud, 
   NroFinalCotizacion, 
   IdEmpresaInterna, 
   IdUsuario, 
   Estado)
   VALUES 
   (@nvId, 
    0, 
    0, 
    0,
	0, 
	GETDATE(), 
	0, 
	0, 
	@IdEmpresaInterna, 
	@IdAprobador, 
	@estado);

	SELECT	Verificador = cast(1 as bit)
	end

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_NV_Extras]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_NV_Extras]
@pi_IdCotizacion INT,
@FechaCierre datetime,
@nroSolicitud int,
@NroFinalCotizacion varchar(20)
AS
BEGIN
DECLARE @Resp INT=0


INSERT INTO DS_NotaVentaExtras 
(IdNotaVenta,  Cubicaje, Peso, TieneDescuento, EsCotizacion, FechaCierre, nroSolicitud, NroFinalCotizacion,  IdEmpresaInterna,  IdUsuario,  Estado)
   VALUES 
(@pi_IdCotizacion, '0', '0',0,1,@FechaCierre,@nroSolicitud,@NroFinalCotizacion,0,0,'')
  

  SELECT @resp = @@identity
  SELECT @Resp

END

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_NV_RutaArchivoAdjunto]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_INS_NV_RutaArchivoAdjunto]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[SP_INS_NV_RutaArchivoAdjunto]
@Path varchar(max),
@CodAux varchar(50)
as
insert into RutaArchivoAdjunto (Ruta,CodAux,Estado) values (@Path,@CodAux,0)

select cast(1 as bit) as Verificador
GO
/****** Object:  StoredProcedure [dbo].[SP_INS_ProductoNuevoDisofi]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_INS_ProductoNuevoDisofi]
(
	@pv_BaseDatos varchar (100)
,	@pv_CodProdAntiguo	varchar	(20)
,	@pv_CodProd	varchar	(20)
,	@pv_DesProd	varchar	(60)
,	@pv_CodGrupo	varchar	(10)
,	@pv_CodSubGr	varchar	(10)
,	@pf_PrecioVta float
,	@pv_CodMon	varchar	(2)
)
AS
BEGIN
	IF @pv_CodProdAntiguo = @pv_CodProd	BEGIN
		IF NOT EXISTS (SELECT TOP 1 1 FROM DS_ProductosNuevos WHERE BaseDatos = @pv_BaseDatos and CodProd = @pv_CodProd) BEGIN
			INSERT INTO DS_ProductosNuevos
			(
				BaseDatos
			,	CodProd
			,	DesProd
			,	DesProd2
			,	CodRapido
			,	CodBarra
			,	CodUMed
			,	Origen
			,	CodMonOrig
			,	CodGrupo
			,	CodSubGr
			,	CodCateg
			,	CodMonPVta
			,	PrecioVta
			,	PrecioBol
			,	FichaTec
			,	EsConfig
			,	FactorConfig
			,	Impuesto
			,	Inventariable
			,	EsSerie
			,	EsTallaColor
			,	EsPartida
			,	EsCaducidad
			,	EsPieza
			,	CantPieza
			,	PesoKgs
			,	CtaActivo
			,	CtaVentas
			,	CtaGastos
			,	CtaCosto
			,	FecUltCom
			,	ValorUltCom
			,	CostoRep
			,	FecCostoRep
			,	FecCMonet
			,	ValorCMonet
			,	NivMin
			,	NivRep
			,	NivMax
			,	Inamovible
			,	ManejaDim
			,	Ancho
			,	esUbicPar
			,	CtaDevolucion
			,	TipProd
			,	esParaVenta
			,	esParaCompra
			,	EsTalla
			,	EsColor
			,	MetodoCosteo
			,	CodUMedVta1
			,	EquivUMVta1
			,	PrecioVtaUM1
			,	PrecioBolUM1
			,	CodUMedVta2
			,	EquivUMVta2
			,	PrecioVtaUM2
			,	PrecioBolUM2
			,	UMDefecto
			,	ManProdAnticipo
			,	ImprimeEnBoleta
			,	EsParaAutoservicio
			,	Inactivo
			,	Usuario
			,	Proceso
			,	FechaUlMod
			)
			SELECT	BaseDatos = @pv_BaseDatos
			,		CodProd = @pv_CodProd
			,		DesProd = @pv_DesProd
			,		DesProd2 = ''
			,		CodRapido = ''
			,		CodBarra = ''
			,		CodUMed = 'UN'
			,		Origen = -1
			,		CodMonOrig = @pv_CodMon
			,		CodGrupo = @pv_CodGrupo
			,		CodSubGr = @pv_CodSubGr
			,		CodCateg = null
			,		CodMonPVta = @pv_CodMon
			,		PrecioVta = @pf_PrecioVta
			,		PrecioBol = @pf_PrecioVta * 1.19
			,		FichaTec = ''
			,		EsConfig = ''
			,		FactorConfig = ''
			,		Impuesto = -1
			,		Inventariable = -1
			,		EsSerie = 0
			,		EsTallaColor = 0
			,		EsPartida = 0
			,		EsCaducidad = 0
			,		EsPieza = 0
			,		CantPieza = ''
			,		PesoKgs = 0
			,		CtaActivo = null
			,		CtaVentas = null
			,		CtaGastos = null
			,		CtaCosto = null
			,		FecUltCom = ''
			,		ValorUltCom = ''
			,		CostoRep = ''
			,		FecCostoRep = ''
			,		FecCMonet = ''
			,		ValorCMonet = ''
			,		NivMin = ''
			,		NivRep = ''
			,		NivMax = ''
			,		Inamovible = ''
			,		ManejaDim = ''
			,		Ancho = ''
			,		esUbicPar = ''
			,		CtaDevolucion = null
			,		TipProd = 'OT'
			,		esParaVenta = -1
			,		esParaCompra = 0
			,		EsTalla = 0
			,		EsColor = 0
			,		MetodoCosteo = 'P'
			,		CodUMedVta1 = null
			,		EquivUMVta1 = ''
			,		PrecioVtaUM1 = ''
			,		PrecioBolUM1 = ''
			,		CodUMedVta2 = null
			,		EquivUMVta2 = ''
			,		PrecioVtaUM2 = ''
			,		PrecioBolUM2 = ''
			,		UMDefecto = ''
			,		ManProdAnticipo = ''
			,		ImprimeEnBoleta = -1
			,		EsParaAutoservicio = 0
			,		Inactivo = 0
			,		Usuario = 'softland'
			,		Proceso = 'Ficha de Producto:Insert'
			,		FechaUlMod = GETDATE()
	
			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = 'Producto agregado'
		END
		ELSE BEGIN
			update	DS_ProductosNuevos
			set		CodProd = @pv_CodProd
			,		DesProd = @pv_DesProd
			,		CodMonOrig = @pv_CodMon
			,		CodGrupo = @pv_CodGrupo
			,		CodSubGr = @pv_CodSubGr
			,		CodMonPVta = @pv_CodMon
			,		PrecioVta = @pf_PrecioVta
			,		FechaUlMod = GETDATE()
			where	BaseDatos = @pv_BaseDatos
			and		CodProd = @pv_CodProdAntiguo
	
			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = 'Producto modificado'
		END
	END
	ELSE BEGIN
		update	DS_ProductosNuevos
		set		CodProd = @pv_CodProd
		,		DesProd = @pv_DesProd
		,		CodMonOrig = @pv_CodMon
		,		CodGrupo = @pv_CodGrupo
		,		CodSubGr = @pv_CodSubGr
		,		CodMonPVta = @pv_CodMon
		,		PrecioVta = @pf_PrecioVta
		where	BaseDatos = @pv_BaseDatos
		and		CodProd = @pv_CodProdAntiguo
	
		SELECT	Verificador = cast(1 as bit)
		,		Mensaje = 'Producto modificado'
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_ProductoNuevoSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_INS_ProductoNuevoSoftland]
(
	@pv_BaseDatos varchar (100)
,	@pv_CodProd	varchar	(20)
)
AS
BEGIN
	DECLARE @query nvarchar(max)

	select @query = ''
	
	select @query = @query + '
	IF NOT EXISTS (SELECT TOP 1 1 FROM ' + @pv_BaseDatos + '.softland.iw_tprod WHERE CodProd = ''' + @pv_CodProd + ''') BEGIN
		BEGIN TRY
			INSERT INTO ' + @pv_BaseDatos + '.softland.iw_tprod
			(
				CodProd
			,	DesProd
			,	DesProd2
			,	CodRapido
			,	CodBarra
			,	CodUMed
			,	Origen
			,	CodMonOrig
			,	CodGrupo
			,	CodSubGr
			,	CodCateg
			,	CodMonPVta
			,	PrecioVta
			,	PrecioBol
			,	FichaTec
			,	EsConfig
			,	FactorConfig
			,	Impuesto
			,	Inventariable
			,	EsSerie
			,	EsTallaColor
			,	EsPartida
			,	EsCaducidad
			,	EsPieza
			,	CantPieza
			,	PesoKgs
			,	CtaActivo
			,	CtaVentas
			,	CtaGastos
			,	CtaCosto
			,	FecUltCom
			,	ValorUltCom
			,	CostoRep
			,	FecCostoRep
			,	FecCMonet
			,	ValorCMonet
			,	NivMin
			,	NivRep
			,	NivMax
			,	Inamovible
			,	ManejaDim
			,	Ancho
			,	esUbicPar
			,	CtaDevolucion
			,	TipProd
			,	esParaVenta
			,	esParaCompra
			,	EsTalla
			,	EsColor
			,	MetodoCosteo
			,	CodUMedVta1
			,	EquivUMVta1
			,	PrecioVtaUM1
			,	PrecioBolUM1
			,	CodUMedVta2
			,	EquivUMVta2
			,	PrecioVtaUM2
			,	PrecioBolUM2
			,	UMDefecto
			,	ManProdAnticipo
			,	ImprimeEnBoleta
			,	EsParaAutoservicio
			,	Inactivo
			,	Usuario
			,	Proceso
			,	FechaUlMod
			)
			SELECT	CodProd
			,		DesProd
			,		DesProd2
			,		CodRapido
			,		CodBarra
			,		CodUMed
			,		Origen
			,		CodMonOrig
			,		CodGrupo
			,		CodSubGr
			,		CodCateg
			,		CodMonPVta
			,		PrecioVta
			,		PrecioBol
			,		FichaTec
			,		EsConfig
			,		FactorConfig
			,		Impuesto
			,		Inventariable
			,		EsSerie
			,		EsTallaColor
			,		EsPartida
			,		EsCaducidad
			,		EsPieza
			,		CantPieza
			,		PesoKgs
			,		CtaActivo
			,		CtaVentas
			,		CtaGastos
			,		CtaCosto
			,		FecUltCom
			,		ValorUltCom
			,		CostoRep
			,		FecCostoRep
			,		FecCMonet
			,		ValorCMonet
			,		NivMin
			,		NivRep
			,		NivMax
			,		Inamovible
			,		ManejaDim
			,		Ancho
			,		esUbicPar
			,		CtaDevolucion
			,		TipProd
			,		esParaVenta
			,		esParaCompra
			,		EsTalla
			,		EsColor
			,		MetodoCosteo
			,		CodUMedVta1
			,		EquivUMVta1
			,		PrecioVtaUM1
			,		PrecioBolUM1
			,		CodUMedVta2
			,		EquivUMVta2
			,		PrecioVtaUM2
			,		PrecioBolUM2
			,		UMDefecto
			,		ManProdAnticipo
			,		ImprimeEnBoleta
			,		EsParaAutoservicio
			,		Inactivo
			,		Usuario
			,		Proceso
			,		FechaUlMod
			from	DS_ProductosNuevos
			where	CodProd = ''' + @pv_CodProd + '''
			
			insert into ' + @pv_BaseDatos + '.softland.[iw_timprod] (CodImpto, CodProd, Usuario, Sistema)
			select ''IVA'', ''' + @pv_CodProd + ''', ''softland'',''IW''

			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = ''Producto agregado a softland''
		END TRY
		BEGIN CATCH
			SELECT	Verificador = cast(0 as bit)
			,		Mensaje = ''Error en SQL'' + ERROR_MESSAGE()
		END CATCH
	end
	ELSE BEGIN
		SELECT	Verificador = cast(1 as bit)
		,		Mensaje = ''Producto ya esta agregado en softland''
	END
	'

	update	DS_ProductosNuevos
	set		AgregadoASoftland = 1
	where	CodProd = @pv_CodProd

	exec(@query)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_UsuarioEmpresa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_UsuarioEmpresa]
(
	@pi_IdUsuario INT
,	@pi_IdEmpresa INT
,	@pv_VenCod VARCHAR(50)
)
AS
BEGIN
	DECLARE @lb_Verificador BIT
	DECLARE @lv_Mensaje VARCHAR(MAX)

	INSERT INTO dbo.DS_UsuarioEmpresa
	(
	    IdUsuario,
	    IdEmpresa,
	    VenCod
	)
	VALUES
	(
		@pi_IdUsuario
	,	@pi_IdEmpresa
	,	@pv_VenCod
	)

	SELECT	@lb_Verificador = 1
	,		@lv_Mensaje = 'Usuario empresa insertado exitosamente'
	
	SELECT	Verificador = @lb_Verificador 
	,		Mensaje = @lv_Mensaje 
END

GO
/****** Object:  StoredProcedure [dbo].[SP_INT_COT_FOLIOS]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INT_COT_FOLIOS]
@pv_VenCod CHAR(4),
@Iniciales char(2),
@NroFinalCotizacion nvarchar(20) OUTPUT
AS
BEGIN
declare @Nro_FinalCotizacion nvarchar(20)
declare @anno char(4)
declare @mes  char(4)
declare @folioCotizacion int 

set @anno = YEAR(getdate())
set @mes = case when MONTH(getdate())<10 then '0'+ convert(char(1),MONTH(GETDATE())) end 
select @folioCotizacion=isnull(max(NroCotizacion),0)+1 from DS_Cot_Folios where Vendedor=@pv_VenCod

if @folioCotizacion<10
set @Nro_FinalCotizacion=@anno+LTRIM(RTRIM(@mes))+@Iniciales + '00' + CONVERT(char(3),@folioCotizacion)
if @folioCotizacion>=10 and @folioCotizacion<100
set @Nro_FinalCotizacion=@anno+LTRIM(RTRIM(@mes))+@Iniciales + '0' + CONVERT(char(3),@folioCotizacion)
if @folioCotizacion>=100
set @Nro_FinalCotizacion=@anno+LTRIM(RTRIM(@mes))+@Iniciales + CONVERT(char(3),@folioCotizacion)

DECLARE @ExisteVendedor int
 select @ExisteVendedor=count(*) from DS_Cot_Folios where Vendedor=@pv_VenCod

 if @ExisteVendedor>0
	 BEGIN
		UPDATE DS_Cot_Folios 
		SET NroCotizacion = (select isnull(max(NroCotizacion),0)+1 from DS_Cot_Folios where Vendedor=@pv_VenCod ),
		NroFinalCotizacion=@Nro_FinalCotizacion
		WHERE Vendedor=@pv_VenCod
	 END

 ELSE

	 BEGIN
		  INSERT INTO DS_Cot_Folios (Vendedor, NroCotizacion,Iniciales,NroFinalCotizacion)
		  VALUES (@pv_VenCod, 1,@Iniciales,@Nro_FinalCotizacion);
	 END
	 set @NroFinalCotizacion =@Nro_FinalCotizacion
	 select @NroFinalCotizacion
END

GO
/****** Object:  StoredProcedure [dbo].[SP_ObtenerCanalVenta]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_ObtenerCanalVenta]
(
	@pv_BaseDatos varchar (100)
)
AS
BEGIN
	declare @query varchar(max)

	select @query = ''

	select @query = @query + '
	SELECT	*
	FROM	[' + @pv_BaseDatos + '].[softland].[cwtcana]
	'

	EXEC (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_ObtenerVendedorCliente]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_ObtenerVendedorCliente]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[SP_ObtenerVendedorCliente]
	@pv_CodAux varchar(100)
,	@pv_BaseDatos varchar(100)
AS
BEGIN
	declare @query nvarchar(max)
	
	select	@query = ''

	select	@query = @query + '
	SELECT	top 1 
			a.VenCod
	,		b.VenDes
	FROM	['+@pv_BaseDatos+'].softland.[cwtauxven] A 
		inner join ['+@pv_BaseDatos+'].softland.[cwtvend] B
			on a.VenCod = B.VenCod
	where	a.CodAux = ''' + @pv_CodAux + '''
	'

	exec (@query)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_SET_RutaArchivoSoftland]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[SP_SET_RutaArchivoSoftland]
@NvNum AS varchar(50),
@RutaArchivo AS varchar(150),
@TipoArchivo AS varchar (10)
AS
INSERT INTO EMELTEC.softland.nw_nvdoctos 
(nvnumero,nvnombre,docObserv,nvTipoDoc,drive)
VALUES
(CONVERT(int,@NvNum),@RutaArchivo,null,@TipoArchivo,'C')

SELECT Verificador = cast(1 AS bit)

GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_COT_Cabecera]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_UPD_COT_Cabecera]
(
	@Id int,
	@fechaEmision datetime,
	@fechaEntrega datetime,
	@fechaCierre datetime,
	@contacto nvarchar(30),
	@observacion text
)
AS
BEGIN
UPDATE DS_Cotizacion 
SET 
   nvFem = @fechaEmision
   ,nvFeEnt = @fechaEntrega
   ,NomCon = @contacto
   ,nvObser = @observacion
WHERE Id = @Id;

  UPDATE DS_NotaVentaExtras 
SET FechaCierre = @fechaCierre
WHERE IdNotaVenta = @Id;
	
	SELECT	Verificador = cast(1 as bit)
	,		Mensaje = 'Cotizacion modificada'
END

GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_COT_Consideracion]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_UPD_COT_Consideracion]
@Id int,
@Titulo varchar(200),
@Consideracion varchar(max)
as
begin
UPDATE DS_Consideracion 
SET Consideracion = @Consideracion
   ,Titulo = @Titulo
WHERE Id=@Id

SELECT	Verificador = cast(1 as bit), Mensaje = 'Modifica exitosamente'
end
GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_COT_Detalle]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_UPD_COT_Detalle]
  @Id INT,
  @precio FLOAT,
  @grupo VARCHAR(10),
  @subGrupo VARCHAR(10),
  @codmon VARCHAR(10),
  @cantidad FLOAT,
  @descuento FLOAT,
  @pv_BaseDatos VARCHAR(100),
  @valorDescuento float,
  @fechaEntrega datetime,
  @codigoFabrica varchar(60),
  @codProd varchar(20)
AS
  BEGIN
    DECLARE @descuentoMonto FLOAT
	declare @IdCotizacion int
	declare @Sincronizado int
	select @Sincronizado = convert(int,AgregadoASoftland) from DS_ProductosNuevos where CodProd=@codProd
	if @Sincronizado is null
	   set @Sincronizado=1

	if @Sincronizado=0
	   update DS_ProductosNuevos set DesProd2= @codigoFabrica where CodProd = @codProd


	select @IdCotizacion = IdNotaVenta from DS_CotizacionDetalle where Id = @Id

    UPDATE DS_CotizacionDetalle 
	SET nvCant = @cantidad
   ,nvPrecio = @precio
   ,nvSubTotal = (@cantidad*@precio)
   ,nvDPorcDesc01 = @descuento
   --,nvDDescto01 = @descuentoMonto
   ,nvTotLinea = (@cantidad*@precio) - (@valorDescuento*@cantidad)
   --,nvTotDesc = @descuentoMonto
   ,nvDDescto01= @valorDescuento
   ,nvTotDesc = @valorDescuento
   ,nvFecCompr = @fechaEntrega
   WHERE Id = @Id;

   --INI UPDATE CABECERA
   declare @TotalConIva float
   --select @TotalConIva = ((select sum(nvtotlinea) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion)*19/100)+(select sum(nvPrecio) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion)
   select @TotalConIva = ((select sum(nvtotlinea) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion)*19/100)+(select sum(nvtotlinea) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion)
   update DS_Cotizacion set 
   nvSubTotal=(select sum(nvSubTotal) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion),
   nvMonto=@TotalConIva,
   --nvNetoAfecto=(select sum(nvSubTotal) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion),
   nvNetoAfecto=(select sum(nvTotLinea) from DS_CotizacionDetalle where IdNotaVenta = @IdCotizacion),
   --nvPorcDesc01=@descuento,
   --nvDescto01=@valorDescuento,
   TotalBoleta=@TotalConIva
    where Id = @IdCotizacion
   --FIN UPDATE CABECERA

  	SELECT	Verificador = cast(1 as bit)
	,		Mensaje = 'Cotizacion modificada'
    	
  END

GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_NotaVentaProductoNuevo]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_UPD_NotaVentaProductoNuevo]
(
	@pv_BaseDatos varchar (100)
,	@pi_IdDetalle int
,	@pv_CodProd	varchar	(20)
)
AS
BEGIN
	update	DS_NotasVentaDetalle
	set		CodProd = @pv_CodProd
	where	Id = @pi_IdDetalle
	
	SELECT	Verificador = cast(1 as bit)
	,		Mensaje = 'Producto modificado'
END

GO
/****** Object:  StoredProcedure [dbo].[SP_ValidaExisteUsuarioEmpresa]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_ValidaExisteUsuarioEmpresa]
(
	@pv_VenCod VARCHAR(20)
,	@pi_IdEmpresa int
)
AS
BEGIN
	DECLARE @lb_Verificador BIT
	DECLARE @lv_Mensaje VARCHAR(MAX)
	
	SELECT	@lb_Verificador = 0
	,		@lv_Mensaje = 'NO EXISTE'

	IF EXISTS
		(
			SELECT	top 1 1
			FROM	DS_UsuarioEmpresa
			WHERE	Vencod = @pv_VenCod
			and		IdEmpresa = @pi_IdEmpresa
		) BEGIN
		SELECT	@lb_Verificador = 1
		,		@lv_Mensaje = 'EXISTE'
	END
	
	SELECT	Verificador = @lb_Verificador
	,		Mensaje = @lv_Mensaje
END

GO
/****** Object:  StoredProcedure [dbo].[uSP_GET_Reporte_Comisiones]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[uSP_GET_Reporte_Comisiones]								*/
/*-- Detalle			:														*/
/*-- Autor				: YVEGA												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE procedure [dbo].[uSP_GET_Reporte_Comisiones]  
	@pv_BaseDatos varchar(100)
,	@pv_IdVendedores varchar(100)
,	@pv_FechaDesde datetime
,	@pv_FechaHasta datetime
as 
begin
	set nocount on 
	
	DECLARE	@tituloInforme nvarchar(max)
	DECLARE	@campoPorcentaje nvarchar(max)

	DECLARE	@query01 nvarchar(max)
	DECLARE	@query02 nvarchar(max)
	DECLARE	@query03 nvarchar(max)
	DECLARE	@query04 nvarchar(max)
	DECLARE	@query05 nvarchar(max)
	DECLARE	@query06 nvarchar(max)
	
	select	@query01 = ''
	select	@query02 = ''
	select	@query03 = ''
	select	@query04 = ''
	select	@query05 = ''
	select	@query06 = ''
	
	select	@pv_IdVendedores = '''' + replace(@pv_IdVendedores, ';', ''',''') + ''''
	
	select	@tituloInforme = ''
	
	select	@tituloInforme = 'Informe de Comision por Producto desde: ' + convert(varchar(10), @pv_FechaDesde, 103) + ' hasta: ' + convert(varchar(10), @pv_FechaHasta, 103)
	
	select	@campoPorcentaje = ''

	select	@campoPorcentaje = 'a.PorcentajeProducto'
											



	select	@query01 = @query01 + '
	select	[CodigoVendedor] = a.CodVendedor
	,		[TipoFactura] = isnull(a.Tipo, '''') + '' '' + isnull(CONVERT(VARCHAR(100), a.Folio), '''')
	,		[Fecha] = isnull(convert(varchar(20), a.fecha, 103), '''')
	,		[Codigo] = isnull(a.CodProd, '''')
	,		[Descripcion] = isnull(a.desprod, '''')
	,		[Cantidad] = isnull(a.CantFacturada, 0)
	,		[Precio] = isnull(a.preunimb, 0)
	,		[Totales] = isnull(a.TotLinea, 0)
	,		[Porcentaje] = isnull(' + @campoPorcentaje + ', 0)
	,		[Comision] = isnull(cast(0.0 as float), 0.0)
	,		[Cliente] = isnull(a.NomAux, '''')
			
	--		[Tipo] = isnull(a.tipo, '''')
	--,		[Folio] = isnull(a.folio, 0)
	--,		[C.Costos] = isnull(a.CentroDeCosto, '''')
	--,		[Cliente] = isnull(a.codaux, '''')
	--,		[Nombre Cliente] = isnull(a.NomAux, '''')
	--,		[Cod.Producto] = isnull(a.CodProd, '''')
	--,		[Descrip.Producto] = isnull(a.desprod, '''')
	--,		[CodGrupo] = isnull(a.CodGrupo, '''')
	--,		[DesGrupo] = isnull(a.DesGrupo, '''')
	--,		[CodSubGr] = isnull(a.CodSubGr, '''')
	--,		[DesSubGr] = isnull(a.DesSubGr, '''')
	--,		[Cantidad] = isnull(a.CantFacturada, 0)
	--,		[PrecioVenta] = isnull(a.preunimb, 0)
	--,		[Total Desc] = isnull(a.TotalDescMov, 0)
	--,		[TotLinea] = isnull(a.TotLinea, 0)
	--,		[Fecha] = isnull(convert(varchar(20), a.fecha, 103), '''')
	--,		[CostoProm] = isnull(a.CostoProm, 0)
	--,		[CostoTotalProm] = isnull(a.costoTotalProm, 0)
	--,		[CodVendedor] = isnull(a.CodVendedor, '''')
	--,		[Descrip.Vendedor] = isnull(a.VenDes, '''')
	--,		[TipoDocto] = isnull(a.tipoDocto, '''')
	--,		[Fecha Vencimiento] = isnull(convert(varchar(20), a.fechaVenc, 103), '''')

	--,		[Cod.Caja] = isnull(a.codcaja, '''')
	--,		[Descrip.Caja] = isnull(a.descaja, '''')
	--,		[aaaaaaaaaaaaaa] = isnull(convert(varchar(20), a.FecHoraCreacionVW, 103), '''')
	--,		[aaaaaaaaaaaaaa] = isnull(convert(varchar(20), a.linea, 103), '''')
	--,		[aaaaaaaaaaaaaa] = isnull(a.eskit, 0)
	--,		[aaaaaaaaaaaaaa] = isnull(a.kit, '''')
	--,		[aaaaaaaaaaaaaa] = isnull(a.CKit, '''')
	--,		[aaaaaaaaaaaaaa] = isnull(a.DKit, '''')
	--,		[TipoCambio] = isnull(a.TipoCambio, '''')
	--,		[Partida] = isnull(a.Partida, '''')
	--,		[Pieza] = isnull(a.Pieza, '''')
	--,		[aaaaaaaaaaaaaa] = isnull(a.CatDes, '''')
	--,		[aaaaaaaaaaaaaa] = isnull(a.DetProd, '''')
	--,		[aaaaaaaaaaaaaa] = isnull(a.TotalBoleta, 0)
	--,		[ValorconDescuento] = isnull(a.ValorconDescuento, 0)
	--,		[aaaaaaaaaaaaaa] = isnull(a.CODKIT, '''')
	--,		[aaaaaaaaaaaaaa] = isnull(a.Nombrekit, '''')'
	select	@query02 = @query02 + '
	into	#temporal_comisiones
	from	(
				select	distinct 
						gsaen.tipo
				,		gsaen.codcaja
				,		gsaen.CentroDeCosto
				,		caja.descaja
				,		gsaen.FecHoraCreacionVW
				,		gmovi.linea
				,		gsaen.codaux
				,		cwt.NomAux
				,		gsaen.folio as folio
				,		gmovi.CodProd
				,		prod.esconfig as eskit
				,		gmovi.kit
				,		(
							case	when gmovi.kit is null 
										then gmovi.codprod 
									else gmovi.kit 
							end
						) as CKit
				,		(
							case	when gmovi.kit is null 
										then prod.desprod 
									else PKIT.DesProd 
							end
						) as DKit
				,		prod.desprod
				,		prod.CodGrupo
				,		grupo.DesGrupo 
				,		prod.CodSubGr
				,		subGrupo.DesSubGr
				,		gmovi.CantFacturada
				,		gmovi.preunimb
				,		round(gmovi.TotLinea, 1) as TotLinea
				,		gsaen.fecha
				,		'''' as TipoCambio
				--,		[dbo].costoprom(gmovi.CodProd, gsaen.fecha) as CostoProm
				,		CostoProm = 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0)
				--,		(gmovi.cantFacturada * dbo.costoprom(gmovi.CodProd, gsaen.fecha)) as costoTotalProm
				,		costoTotalProm = 
						(gmovi.cantFacturada * 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0))
				,		gmovi.Partida
				,		gmovi.Pieza
				,		descCl.CatDes
				,		gsaen.CodVendedor
				,		vendedor.VenDes
				,		cast(gmovi.DetProd as varchar(max)) AS DetProd
				,		gmovi.TotalBoleta
				--,		round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / gsaen.SubTotal) * gsaen.TotalDesc) / 100)), 1) as ValorconDescuento
				,		gmovi.TotalDescMov
				,		case	when gsaen.SubTipoDocto=''A'' then ''Afecto''
								when gsaen.SubTipoDocto = ''E'' then ''Exento''
								when gsaen.SubTipoDocto = ''x'' then ''Exportacion''
								when gsaen.SubTipoDocto = ''T'' then ''Docto Electronico''
								when gsaen.SubTipoDocto = ''L'' then ''Liquidacion''
								when gsaen.SubTipoDocto = ''N'' then ''Liquidacion Factura''
								when gsaen.SubTipoDocto = ''O'' then ''Liquidacion electronica''
								when gsaen.SubTipoDocto = ''C'' then ''Documento Interno de Venta''
								else ''--''
						end AS tipoDocto
				,		IsNull(GMOVI.KIT, '''') as CODKIT
				,		IsNull(
								(
									SELECT DesProd FROM 
									[' + @pv_BaseDatos + '].softland.iw_tprod
									WHERE codprod = GMOVI.KIT
								), '''') as Nombrekit
				,		GSAEN.fechaVenc
				,		PorcentajeProducto = ISNULL(PROD.pesokgs, 0)
				,		PorcentajeVendedor = ISNULL(CASE WHEN ISNUMERIC(vendedor.email) = 1 THEN CAST(vendedor.email AS FLOAT) ELSE NULL END, 0)'
				select	@query03 = @query03 + '
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT AND GSAEN.TIPO = GMOVI.Tipo
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor 
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux 
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli 
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo 
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].vw_tcaja caja 
						ON caja.codCaja = gsaen.CodCaja
				WHERE	GSAEN.tipo != ''s'' 
				and		GSAEN.tipo != ''n'' 
				and		GSAEN.tipo != ''e'' 
				and		GSAEN.FECHA BETWEEN CONVERT(DATETIME,''' + convert(varchar(8), @pv_FechaDesde, 112) + ''',103) AND CONVERT(DATETIME,''' + convert(varchar(8), @pv_FechaHasta, 112) + ''',103)
				AND		GSAEN.Estado = ''V'''
				select	@query04 = @query04 + '
				union all
				select	distinct 
						gsaen.tipo
				,		gsaen.codcaja 
				,		'''' as descaja 
				,		gsaen.CentroDeCosto
				,		gmovi.linea 
				,		gsaen.FecHoraCreacionVW
				,		gsaen.codaux
				,		cwt.NomAux
				,		gsaen.folio
				,		gmovi.CodProd
				,		prod.esconfig as eskit 
				,		gmovi.kit 
				,		(case when gmovi.kit is null then gmovi.codprod else gmovi.kit end) as CKit 
				,		(case when gmovi.kit is null then prod.desprod else PKIT.DesProd end) as DKit
				,		prod.desprod 
				,		prod.CodGrupo
				,		grupo.DesGrupo 
				,		prod.CodSubGr 
				,		subGrupo.DesSubGr 
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				,		gsaen.fecha
				,		'''' as TipoCambio 
				--,		[dbo].costoprom(gmovi.CodProd,gsaen.fecha) as CostoProm 
				,		CostoProm = 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0)
				--,		(gmovi.cantFacturada * dbo.costoprom(gmovi.CodProd,gsaen.fecha) ) as costoTotalProm 
				,		costoTotalProm = 
						(gmovi.cantFacturada * 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0))
				,		gmovi.Partida 
				,		gmovi.Pieza 
				,		descCl.CatDes 
				,		gsaen.CodVendedor 
				,		vendedor.VenDes 
				,		cast(gmovi.DetProd as varchar(max)) AS DetProd 
				,		gmovi.TotalBoleta 
				--,		round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / gsaen.SubTotal) * gsaen.TotalDesc) / 100)), 1) as ValorconDescuento
				,		gmovi.TotalDescMov
				,		case	when gsaen.SubTipoDocto = ''A'' then ''Afecto'' 
								when gsaen.SubTipoDocto = ''E'' then ''Exento'' 
								when gsaen.SubTipoDocto = ''x'' then ''Exportacion'' 
								when gsaen.SubTipoDocto = ''T'' then ''Docto Electronico'' 
								when gsaen.SubTipoDocto = ''L'' then ''Liquidacion'' 
								when gsaen.SubTipoDocto = ''N'' then ''Liquidacion Factura'' 
								when gsaen.SubTipoDocto = ''O'' then ''Liquidacion electronica'' 
								when gsaen.SubTipoDocto = ''C'' then ''Documento Interno de Venta'' 
								else ''''
						end AS tipoDocto
				,		IsNull(GMOVI.KIT, '''') as CODKIT
				,		IsNull(
								(
									SELECT DesProd FROM 
									[' + @pv_BaseDatos + '].softland.iw_tprod
									WHERE codprod = GMOVI.KIT
								), '''') as Nombrekit 
				,		GSAEN.fechaVenc
				,		PorcentajeProducto = ISNULL(PROD.pesokgs, 0)
				,		PorcentajeVendedor = ISNULL(CASE WHEN ISNUMERIC(vendedor.email) = 1 THEN CAST(vendedor.email AS FLOAT) ELSE NULL END, 0)'
				select	@query05 = @query05 + '
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.tipo!=''b''
				AND		GSAEN.FECHA BETWEEN CONVERT(DATETIME,''' + convert(varchar(8), @pv_FechaDesde, 112) + ''',103) AND CONVERT(DATETIME,''' + convert(varchar(8), @pv_FechaHasta, 112) + ''',103) 
				AND		GSAEN.Estado = ''V''
			) a
	where	a.CodVendedor in (' + @pv_IdVendedores + ')
	order by a.folio desc



	
	SELECT	*
	FROM	(
				SELECT	Titulo = ''' + @tituloInforme + '''
			) a
	
	UPDATE	#temporal_comisiones
	SET		Comision = ((a.Totales * a.Porcentaje) / 100)
	FROM	#temporal_comisiones a

	SELECT		a.[CodigoVendedor]
	,			a.[TipoFactura]
	,			a.[Fecha]
	,			a.[Codigo]
	,			a.[Descripcion]
	,			a.[Cantidad]
	,			a.[Precio]
	,			a.[Totales]
	,			a.[Porcentaje]
	,			a.[Comision]
	,			a.[Cliente]
	FROM		#temporal_comisiones a

	SELECT		CodigoVendedor = a.[CodigoVendedor]
    ,			TotalFacturado = SUM(CASE WHEN a.[Totales] > 0 THEN a.[Totales] ELSE 0 END)
    ,			TotalNotaCredito = SUM(CASE WHEN a.[Totales] < 0 THEN a.[Totales] ELSE 0 END)
    ,			TotalVendidoReal = SUM(a.[Totales])
    ,			TotalComision = SUM(a.[Comision])
	
    ,			TotalFacturadoGeneral = SUM(CASE WHEN a.[Totales] > 0 THEN a.[Totales] ELSE 0 END)
    ,			TotalNotaCreditoGeneral = SUM(CASE WHEN a.[Totales] < 0 THEN a.[Totales] ELSE 0 END)
    ,			TotalVendidoRealGeneral = SUM(a.[Totales])
    ,			TotalComisionGeneral = SUM(a.[Comision])
	FROM		#temporal_comisiones a
	GROUP BY	a.[CodigoVendedor]
	'
	
	exec (@query01 + @query02 + @query03 + @query04 + @query05 + @query06)
	/*
	print (@query01)
	print (@query02)
	print (@query03)
	print (@query04)
	print (@query05)
	print (@query06)
	*/
	
	set nocount OFF
end  

GO
/****** Object:  StoredProcedure [dbo].[uSP_INS_NotaVentaDetalleValorAdicional]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uSP_INS_NotaVentaDetalleValorAdicional]
	@pv_BaseDatos varchar (100)
,	@pi_IdDetalleNotaVenta INT
,	@pi_ValorAdicional INT
AS
BEGIN
	DECLARE	@Id INT
	DECLARE	@Verificador BIT
	DECLARE	@Mensaje VARCHAR(MAX)

	INSERT INTO DS_NotasVentaDetalleValorAdicional
	(
		IdNotaVentaDetalle
	,	ValorAdicional
	)
	VALUES
	(
		@pi_IdDetalleNotaVenta
	,	@pi_ValorAdicional
	)
	
	SELECT	@Id = @@identity

	SELECT	Id = @Id
	,		Verificador = @Verificador
	,		Mensaje = @Mensaje
END

GO
/****** Object:  StoredProcedure [dbo].[uSP_Login]    Script Date: 18/06/2021 8:55:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[uSP_Login]							*/
/*-- Detalle			:														*/
/*-- Autor				: YVEGA													*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROCEDURE [dbo].[uSP_Login]
(
	@pv_Usuario VARCHAR(200)
,	@pv_Contrasena VARCHAR(200)
)
AS
BEGIN
	DECLARE	@lb_Verificador BIT
	DECLARE	@li_Id INT
	DECLARE	@lv_Mensaje VARCHAR(MAX)

	DECLARE	@lb_Estado BIT
	DECLARE	@lv_Contrasena VARCHAR(MAX)

	SELECT	@lb_Verificador = 0
	,		@lv_Mensaje = 'Error de usuario y/o contrasena'
	
	SELECT	@li_Id = Id
	,		@lb_Estado = Estado
	,		@lv_Contrasena = Contrasena
	FROM	Usuario
	WHERE	Usuario = @pv_Usuario

	IF @lb_Estado IS NOT NULL BEGIN
		IF @lb_Estado = 1 BEGIN
			IF @lv_Contrasena = @pv_Contrasena BEGIN
				SELECT	@lb_Verificador = 1
				,		@lv_Mensaje = 'Usuario autorizado'
			END
			ELSE BEGIN
				SELECT	@lb_Verificador = 0
				,		@lv_Mensaje = 'Error de usuario y/o contrasena'
			END
		END
		ELSE BEGIN
			SELECT	@lb_Verificador = 0
			,		@lv_Mensaje = 'Usuario desactivado'
		END
	END
	ELSE BEGIN
		SELECT	@lb_Verificador = 0
		,		@lv_Mensaje = 'Error de usuario y/o contrasena'
	END
	
	SELECT	Id = @li_Id
	,		Verificador = @lb_Verificador
	,		Mensaje = @lv_Mensaje
END


GO
USE [master]
GO
ALTER DATABASE [AplicativoEstandar] SET  READ_WRITE 
GO
