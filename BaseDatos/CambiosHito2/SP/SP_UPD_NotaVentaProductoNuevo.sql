﻿USE [DSNotaVenta]
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_ObtenerDatosUsuario]    Script Date: 16-10-2019 13:27:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_UPD_NotaVentaProductoNuevo]
(
	@pv_BaseDatos varchar (100)
,	@pi_IdDetalle int
,	@pv_CodProd	varchar	(20)
)
AS
BEGIN
	update	DS_NotasVentaDetalle
	set		CodProd = @pv_CodProd
	where	Id = @pi_IdDetalle
	
	SELECT	Verificador = cast(1 as bit)
	,		Mensaje = 'Producto modificado'
END