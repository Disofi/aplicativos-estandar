﻿USE [DSNotaVenta]
GO

alter procedure [dbo].[FR_BuscarNVDetalle]  
 @nvId   int,  
 @pv_BaseDatos varchar(100)  
AS  
DECLARE @query varchar (max)  
  
 declare @lv_bodega varchar(50)  
  
select top 1   
  @lv_bodega =   
    case when a.stockproductoesbodega = 1   
       then isnull(stockproductocodigobodega, '')   
       else ''   
    end   
from ds_parametros a   
 inner join ds_empresa b   
  on a.idempresa = b.id   
where b.basedatos = @pv_BaseDatos  
  
SELECT @query = ''  
SELECT @query = @query + '  
begin  
select   
a.Id,
a.nvLinea,  
a.CodProd,   
EsProductoNuevo = CAST(case when tp.DesProd collate Modern_Spanish_CI_AS is not null then 0 else 1 end AS BIT),
DesProd = case when tp.DesProd collate Modern_Spanish_CI_AS is not null then tp.DesProd collate Modern_Spanish_CI_AS else productosNuevos.DesProd collate Modern_Spanish_CI_AS end,
CodMon = case when tp.CodMonOrig collate Modern_Spanish_CI_AS is not null then tp.CodMonOrig collate Modern_Spanish_CI_AS else productosNuevos.CodMonOrig collate Modern_Spanish_CI_AS end,
CodGrupo = case when tp.CodGrupo collate Modern_Spanish_CI_AS is not null then tp.CodGrupo collate Modern_Spanish_CI_AS else productosNuevos.CodGrupo collate Modern_Spanish_CI_AS end,
CodSubGr = case when tp.CodSubGr collate Modern_Spanish_CI_AS is not null then tp.CodSubGr collate Modern_Spanish_CI_AS else productosNuevos.CodSubGr collate Modern_Spanish_CI_AS end,
ValorNetoProducto = case when tp.PrecioVta is not null then tp.PrecioVta else productosNuevos.PrecioVta end,
a.Partida,  
a.Pieza,  
a.nvCant,   
a.CodUMed,   
a.nvPrecio,   
a.nvSubTotal,  
ROUND(a.nvDPorcDesc01,0) as nvDPorcDesc01,   
ROUND(a.nvDPorcDesc02,0) as nvDPorcDesc02,   
ROUND(a.nvDPorcDesc03,0) as nvDPorcDesc03,   
ROUND(a.nvDPorcDesc04,0) as nvDPorcDesc04,   
ROUND(a.nvDPorcDesc05,0) as nvDPorcDesc05,   
a.nvTotLinea,  
  Stock = ISNULL((    
      select  Sum (CASE WHEN TipoBod = ''D'' THEN Ingresos - Egresos ELSE 0 END) * 1 AS StockDisponible    
      FROM  [' + @pv_BaseDatos + '].[softland].IW_vsnpMovimStockTipoBod WITH (INDEX(IW_GMOVI_BodPro))     
      WHERE  Fecha <= GETDATE()    
      and   CodProd = tp.CodProd   ' +  
  case when @lv_bodega = '' then '' else ' and codbode = ''' + @lv_bodega + '''' end + '  
      GROUP BY CodProd    
     ), 0)--Stock = [dbo].[stock2018](tp.CodProd)    
from [dbo].[DS_NotasVentaDetalle] a  
left JOIN ['+@pv_BaseDatos+'].[softland].[iw_tprod] AS tp on a.CodProd collate Modern_Spanish_CI_AS = tp.CodProd   
left JOIN DS_ProductosNuevos as productosNuevos on a.CodProd collate Modern_Spanish_CI_AS = productosNuevos.CodProd   
where a.IdNotaVenta = '+convert(varchar(100),@nvId)+'  
order by a.nvLinea  
end  
'  
EXEC (@query)  
PRINT (@query)  