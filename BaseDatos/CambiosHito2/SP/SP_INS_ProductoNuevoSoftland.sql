﻿USE [DSNotaVenta]
GO
alter procedure [dbo].[SP_INS_ProductoNuevoSoftland]
(
	@pv_BaseDatos varchar (100)
,	@pv_CodProd	varchar	(20)
)
AS
BEGIN
	DECLARE @query nvarchar(max)

	select @query = ''
	
	select @query = @query + '
	IF NOT EXISTS (SELECT TOP 1 1 FROM ' + @pv_BaseDatos + '.softland.iw_tprod WHERE CodProd = ''' + @pv_CodProd + ''') BEGIN
		BEGIN TRY
			INSERT INTO ' + @pv_BaseDatos + '.softland.iw_tprod
			(
				CodProd
			,	DesProd
			,	DesProd2
			,	CodRapido
			,	CodBarra
			,	CodUMed
			,	Origen
			,	CodMonOrig
			,	CodGrupo
			,	CodSubGr
			,	CodCateg
			,	CodMonPVta
			,	PrecioVta
			,	PrecioBol
			,	FichaTec
			,	EsConfig
			,	FactorConfig
			,	Impuesto
			,	Inventariable
			,	EsSerie
			,	EsTallaColor
			,	EsPartida
			,	EsCaducidad
			,	EsPieza
			,	CantPieza
			,	PesoKgs
			,	CtaActivo
			,	CtaVentas
			,	CtaGastos
			,	CtaCosto
			,	FecUltCom
			,	ValorUltCom
			,	CostoRep
			,	FecCostoRep
			,	FecCMonet
			,	ValorCMonet
			,	NivMin
			,	NivRep
			,	NivMax
			,	Inamovible
			,	ManejaDim
			,	Ancho
			,	esUbicPar
			,	CtaDevolucion
			,	TipProd
			,	esParaVenta
			,	esParaCompra
			,	EsTalla
			,	EsColor
			,	MetodoCosteo
			,	CodUMedVta1
			,	EquivUMVta1
			,	PrecioVtaUM1
			,	PrecioBolUM1
			,	CodUMedVta2
			,	EquivUMVta2
			,	PrecioVtaUM2
			,	PrecioBolUM2
			,	UMDefecto
			,	ManProdAnticipo
			,	ImprimeEnBoleta
			,	EsParaAutoservicio
			,	Inactivo
			,	Usuario
			,	Proceso
			,	FechaUlMod
			)
			SELECT	CodProd
			,		DesProd
			,		DesProd2
			,		CodRapido
			,		CodBarra
			,		CodUMed
			,		Origen
			,		CodMonOrig
			,		CodGrupo
			,		CodSubGr
			,		CodCateg
			,		CodMonPVta
			,		PrecioVta
			,		PrecioBol
			,		FichaTec
			,		EsConfig
			,		FactorConfig
			,		Impuesto
			,		Inventariable
			,		EsSerie
			,		EsTallaColor
			,		EsPartida
			,		EsCaducidad
			,		EsPieza
			,		CantPieza
			,		PesoKgs
			,		CtaActivo
			,		CtaVentas
			,		CtaGastos
			,		CtaCosto
			,		FecUltCom
			,		ValorUltCom
			,		CostoRep
			,		FecCostoRep
			,		FecCMonet
			,		ValorCMonet
			,		NivMin
			,		NivRep
			,		NivMax
			,		Inamovible
			,		ManejaDim
			,		Ancho
			,		esUbicPar
			,		CtaDevolucion
			,		TipProd
			,		esParaVenta
			,		esParaCompra
			,		EsTalla
			,		EsColor
			,		MetodoCosteo
			,		CodUMedVta1
			,		EquivUMVta1
			,		PrecioVtaUM1
			,		PrecioBolUM1
			,		CodUMedVta2
			,		EquivUMVta2
			,		PrecioVtaUM2
			,		PrecioBolUM2
			,		UMDefecto
			,		ManProdAnticipo
			,		ImprimeEnBoleta
			,		EsParaAutoservicio
			,		Inactivo
			,		Usuario
			,		Proceso
			,		FechaUlMod
			from	DS_ProductosNuevos
			where	CodProd = ''' + @pv_CodProd + '''
			
			insert into ' + @pv_BaseDatos + '.softland.[iw_timprod] (CodImpto, CodProd, Usuario, Sistema)
			select ''IVA'', ''' + @pv_CodProd + ''', ''softland'',''IW''

			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = ''Producto agregado a softland''
		END TRY
		BEGIN CATCH
			SELECT	Verificador = cast(0 as bit)
			,		Mensaje = ''Error en SQL'' + ERROR_MESSAGE()
		END CATCH
	end
	ELSE BEGIN
		SELECT	Verificador = cast(1 as bit)
		,		Mensaje = ''Producto ya esta agregado en softland''
	END
	'

	update	DS_ProductosNuevos
	set		AgregadoASoftland = 1
	where	CodProd = @pv_CodProd

	exec(@query)
END