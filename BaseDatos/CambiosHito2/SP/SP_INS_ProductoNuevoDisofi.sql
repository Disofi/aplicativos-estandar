﻿USE [DSNotaVenta]
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_ObtenerDatosUsuario]    Script Date: 16-10-2019 13:27:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
alter procedure [dbo].[SP_INS_ProductoNuevoDisofi]
(
	@pv_BaseDatos varchar (100)
,	@pv_CodProdAntiguo	varchar	(20)
,	@pv_CodProd	varchar	(20)
,	@pv_DesProd	varchar	(60)
,	@pv_CodGrupo	varchar	(10)
,	@pv_CodSubGr	varchar	(10)
,	@pf_PrecioVta float
,	@pv_CodMon	varchar	(2)
)
AS
BEGIN
	IF @pv_CodProdAntiguo = @pv_CodProd	BEGIN
		IF NOT EXISTS (SELECT TOP 1 1 FROM DS_ProductosNuevos WHERE BaseDatos = @pv_BaseDatos and CodProd = @pv_CodProd) BEGIN
			INSERT INTO DS_ProductosNuevos
			(
				BaseDatos
			,	CodProd
			,	DesProd
			,	DesProd2
			,	CodRapido
			,	CodBarra
			,	CodUMed
			,	Origen
			,	CodMonOrig
			,	CodGrupo
			,	CodSubGr
			,	CodCateg
			,	CodMonPVta
			,	PrecioVta
			,	PrecioBol
			,	FichaTec
			,	EsConfig
			,	FactorConfig
			,	Impuesto
			,	Inventariable
			,	EsSerie
			,	EsTallaColor
			,	EsPartida
			,	EsCaducidad
			,	EsPieza
			,	CantPieza
			,	PesoKgs
			,	CtaActivo
			,	CtaVentas
			,	CtaGastos
			,	CtaCosto
			,	FecUltCom
			,	ValorUltCom
			,	CostoRep
			,	FecCostoRep
			,	FecCMonet
			,	ValorCMonet
			,	NivMin
			,	NivRep
			,	NivMax
			,	Inamovible
			,	ManejaDim
			,	Ancho
			,	esUbicPar
			,	CtaDevolucion
			,	TipProd
			,	esParaVenta
			,	esParaCompra
			,	EsTalla
			,	EsColor
			,	MetodoCosteo
			,	CodUMedVta1
			,	EquivUMVta1
			,	PrecioVtaUM1
			,	PrecioBolUM1
			,	CodUMedVta2
			,	EquivUMVta2
			,	PrecioVtaUM2
			,	PrecioBolUM2
			,	UMDefecto
			,	ManProdAnticipo
			,	ImprimeEnBoleta
			,	EsParaAutoservicio
			,	Inactivo
			,	Usuario
			,	Proceso
			,	FechaUlMod
			)
			SELECT	BaseDatos = @pv_BaseDatos
			,		CodProd = @pv_CodProd
			,		DesProd = @pv_DesProd
			,		DesProd2 = ''
			,		CodRapido = ''
			,		CodBarra = ''
			,		CodUMed = 'UN'
			,		Origen = -1
			,		CodMonOrig = @pv_CodMon
			,		CodGrupo = @pv_CodGrupo
			,		CodSubGr = @pv_CodSubGr
			,		CodCateg = null
			,		CodMonPVta = @pv_CodMon
			,		PrecioVta = @pf_PrecioVta
			,		PrecioBol = @pf_PrecioVta * 1.19
			,		FichaTec = ''
			,		EsConfig = ''
			,		FactorConfig = ''
			,		Impuesto = -1
			,		Inventariable = -1
			,		EsSerie = 0
			,		EsTallaColor = 0
			,		EsPartida = 0
			,		EsCaducidad = 0
			,		EsPieza = 0
			,		CantPieza = ''
			,		PesoKgs = 0
			,		CtaActivo = null
			,		CtaVentas = null
			,		CtaGastos = null
			,		CtaCosto = null
			,		FecUltCom = ''
			,		ValorUltCom = ''
			,		CostoRep = ''
			,		FecCostoRep = ''
			,		FecCMonet = ''
			,		ValorCMonet = ''
			,		NivMin = ''
			,		NivRep = ''
			,		NivMax = ''
			,		Inamovible = ''
			,		ManejaDim = ''
			,		Ancho = ''
			,		esUbicPar = ''
			,		CtaDevolucion = null
			,		TipProd = 'OT'
			,		esParaVenta = -1
			,		esParaCompra = 0
			,		EsTalla = 0
			,		EsColor = 0
			,		MetodoCosteo = 'P'
			,		CodUMedVta1 = null
			,		EquivUMVta1 = ''
			,		PrecioVtaUM1 = ''
			,		PrecioBolUM1 = ''
			,		CodUMedVta2 = null
			,		EquivUMVta2 = ''
			,		PrecioVtaUM2 = ''
			,		PrecioBolUM2 = ''
			,		UMDefecto = ''
			,		ManProdAnticipo = ''
			,		ImprimeEnBoleta = -1
			,		EsParaAutoservicio = 0
			,		Inactivo = 0
			,		Usuario = 'softland'
			,		Proceso = 'Ficha de Producto:Insert'
			,		FechaUlMod = GETDATE()
	
			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = 'Producto agregado'
		END
		ELSE BEGIN
			update	DS_ProductosNuevos
			set		CodProd = @pv_CodProd
			,		DesProd = @pv_DesProd
			,		CodMonOrig = @pv_CodMon
			,		CodGrupo = @pv_CodGrupo
			,		CodSubGr = @pv_CodSubGr
			,		CodMonPVta = @pv_CodMon
			,		PrecioVta = @pf_PrecioVta
			,		FechaUlMod = GETDATE()
			where	BaseDatos = @pv_BaseDatos
			and		CodProd = @pv_CodProdAntiguo
	
			SELECT	Verificador = cast(1 as bit)
			,		Mensaje = 'Producto modificado'
		END
	END
	ELSE BEGIN
		update	DS_ProductosNuevos
		set		CodProd = @pv_CodProd
		,		DesProd = @pv_DesProd
		,		CodMonOrig = @pv_CodMon
		,		CodGrupo = @pv_CodGrupo
		,		CodSubGr = @pv_CodSubGr
		,		CodMonPVta = @pv_CodMon
		,		PrecioVta = @pf_PrecioVta
		where	BaseDatos = @pv_BaseDatos
		and		CodProd = @pv_CodProdAntiguo
	
		SELECT	Verificador = cast(1 as bit)
		,		Mensaje = 'Producto modificado'
	END
END