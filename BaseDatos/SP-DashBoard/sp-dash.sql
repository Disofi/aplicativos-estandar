USE [DSNotaVentaEmel]
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelNotasDeVenta]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelNotasDeVenta]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelNotasDeVenta]
@pv_BaseDatos AS varchar(100),
@pv_FechaDesde varchar(20),
@pv_FechaHasta varchar(20),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT DISTINCT
					nv.CodAux as CodigoCliente
				,   auxi.NomAux as NombreCliente
				,	nv.NVNumero	as NotaDeVenta	
				,   nv.VenCod as CodigoVendedor
				,   ven.VenDes as NombreVendedor		
				,	pro.DesProd as Producto,
				CASE 
					 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
						THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
						ELSE  det.nvcant
				END as ''Cantidad'',								
				--((CASE 
				--	 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
				--		ELSE  det.nvcant
				--END)
				(det.nvcant*nvprecio) as ''ValorNV''
				,	convert(varchar,nv.nvFem,105) AS ''FechaEmision''
				,	convert(varchar,nvFecCompr,105) as ''FechaDespacho''
				,	convert(varchar,nvFeEnt,105) as ''FechaEntrega'' 
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
							ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					LEFT JOIN [' + @pv_BaseDatos + '].softland.nw_nventa nv 
							ON nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero	
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
							on ven.VenCod = nv.VenCod
					left join [' + @pv_BaseDatos + '].softland.cwtauxi auxi
							on auxi.CodAux = nv.CodAux
				WHERE nv.nvEstado in (''a'',''c'',''p'')
				and nvFem BETWEEN convert(datetime,''' + @pv_FechaDesde + ''',120) AND convert(datetime,''' + @pv_FechaHasta + ''',120)
					group by nv.NVNumero, nvFecCompr,pro.DesProd, det.CodProd, nv.nvEstado, det.nvCant, det.nvPrecio, nvTotLinea
					, nv.nvFem, nvFecCompr, nvFeEnt,nv.CodAux,nv.NomCon,nv.VenCod,ven.VenDes,auxi.NomAux
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT DISTINCT
					nv.CodAux as CodigoCliente
				,   auxi.NomAux as NombreCliente
				,	nv.NVNumero	as NotaDeVenta	
				,   nv.VenCod as CodigoVendedor
				,   ven.VenDes as NombreVendedor		
				,	pro.DesProd as Producto,
				CASE 
					 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
						THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
						ELSE  det.nvcant
				END as ''Cantidad'',								
				--((CASE 
				--	 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
				--		ELSE  det.nvcant
				--END)
				(det.nvcant*nvprecio) as ''ValorNV''
				,	convert(varchar,nv.nvFem,105) AS ''FechaEmision''
				,	convert(varchar,nvFecCompr,105) as ''FechaDespacho''
				,	convert(varchar,nvFeEnt,105) as ''FechaEntrega'' 
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
							ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					LEFT JOIN [' + @pv_BaseDatos + '].softland.nw_nventa nv 
							ON nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero	
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
							on ven.VenCod = nv.VenCod
					left join [' + @pv_BaseDatos + '].softland.cwtauxi auxi
							on auxi.CodAux = nv.CodAux
				WHERE nv.nvEstado in (''a'',''c'',''p'')
				and nvFem BETWEEN convert(datetime,''' + @pv_FechaDesde + ''',120) AND convert(datetime,''' + @pv_FechaHasta + ''',120)
				and nv.VenCod = ''' + @pv_VenCod + '''
					group by nv.NVNumero, nvFecCompr,pro.DesProd, det.CodProd, nv.nvEstado, det.nvCant, det.nvPrecio, nvTotLinea
					, nv.nvFem, nvFecCompr, nvFeEnt,nv.CodAux,nv.NomCon,nv.VenCod,ven.VenDes,auxi.NomAux
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelNotasDeVentaBacklog]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelNotasDeVentaBacklog]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelNotasDeVentaBacklog]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT 
				NombreCliente = (select nomaux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = nv.CodAux)
				,nv.codAux as CodigoCliente
				,nv.nvNumero as NotaDeVenta
				,nv.VenCod as CodigoVendedor
				,ven.VenDes as NombreVendedor
				,Producto = (select desprod from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd)
				,det.nvTotLinea as ValorNV
				,convert(varchar,nv.nvFem,105) AS FechaEmision
				,convert(varchar,nvFecCompr,105) as FechaDespacho
				,convert(varchar,nvFeEnt,105) as FechaEntrega 
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
						ON nv.NVNumero= det.NVNumero
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and det.nvnumero = nvprod.nvnumero		
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
						on ven.VenCod = nv.VenCod 		 
					WHERE 
						nvEstado=''A''
						AND nvFem BETWEEN convert(datetime,''2000-01-01'',120) AND convert(datetime,getdate(),120) 						
				group by nv.nvEstado, nv.NVNumero, nv.nvFem, nv.CodAux, nv.nvSubTotal
					, det.nvTotLinea,det.CodProd,nvFecCompr,nvFeEnt,nv.VenCod, ven.VenDes
				having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0
				order by CodAux asc
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT 
				NombreCliente = (select nomaux from [' + @pv_BaseDatos + '].softland.cwtauxi where CodAux = nv.CodAux)
				,nv.codAux as CodigoCliente
				,nv.nvNumero as NotaDeVenta
				,nv.VenCod as CodigoVendedor
				,ven.VenDes as NombreVendedor
				,Producto = (select desprod from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd)
				,det.nvTotLinea as ValorNV
				,convert(varchar,nv.nvFem,105) AS FechaEmision
				,convert(varchar,nvFecCompr,105) as FechaDespacho
				,convert(varchar,nvFeEnt,105) as FechaEntrega 
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
						ON nv.NVNumero= det.NVNumero
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and det.nvnumero = nvprod.nvnumero		
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
						on ven.VenCod = nv.VenCod 		 
					WHERE 
						nvEstado=''A''
						AND nvFem BETWEEN convert(datetime,''2000-01-01'',120) AND convert(datetime,getdate(),120) 						
						AND nv.VenCod = ''' + @pv_VenCod + '''
				group by nv.nvEstado, nv.NVNumero, nv.nvFem, nv.CodAux, nv.nvSubTotal
					, det.nvTotLinea,det.CodProd,nvFecCompr,nvFeEnt,nv.VenCod, ven.VenDes
				having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0
				order by CodAux asc
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelVentas]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelVentas]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelVentas]
@pv_BaseDatos AS varchar(100),
@pv_FechaDesde varchar(20),
@pv_FechaHasta varchar(20),
@pv_VenCod varchar(5)
AS
DECLARE	@query nvarchar(max)

if(@pv_VenCod = '-3') begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
'
EXEC ( @query)
end
else begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC ( @query)
end


GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelVentasDia]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelVentasDia]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_GET_DASH_ExcelVentasDia]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	select distinct 
						gsaen.tipo
				,		gsaen.folio
				,		convert(varchar,gsaen.fecha,105) AS fecha
				--,		gsaen.codbode
				,		gsaen.codaux
				--,		cwt.NomAux
				--,		cwt.DirAux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes 
				--,		gmovi.linea 
				--,		prod.CodGrupo
				--,		grupo.DesGrupo 
				--,		prod.CodSubGr 
				--,		subGrupo.DesSubGr 
				,		gmovi.CodProd
				,		prod.desprod 
				--,		cast(gmovi.DetProd as varchar(max)) AS DetProd 
				--,		mone.DesMon
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				--,       (case when gsaen.porcdesc01>0 then gsaen.porcdesc01 else gmovi.porcdescmov01 end) as TotalDescMov
				,		round(gmovi.TotLinea,1) as TotLinea
				/*
				,		gmovi.TotalBoleta 
				,		gsaen.codcaja
				,		'''' as descaja 
				,		gsaen.CentroDeCosto
				,		'''' as TipoCambio
				,		CostoProm = 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0)
				,		costoTotalProm = 
						(gmovi.cantFacturada * 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0))
				,		gmovi.Partida 
				,		gmovi.Pieza 
				,		descCl.CatDes
				,		case	when gsaen.SubTipoDocto = ''A'' then ''Afecto'' 
								when gsaen.SubTipoDocto = ''E'' then ''Exento'' 
								when gsaen.SubTipoDocto = ''x'' then ''Exportacion'' 
								when gsaen.SubTipoDocto = ''T'' then ''Docto Electronico'' 
								when gsaen.SubTipoDocto = ''L'' then ''Liquidacion'' 
								when gsaen.SubTipoDocto = ''N'' then ''Liquidacion Factura'' 
								when gsaen.SubTipoDocto = ''O'' then ''Liquidacion electronica'' 
								when gsaen.SubTipoDocto = ''C'' then ''Documento Interno de Venta'' 
								else ''''
						end AS tipoDocto
				,		IsNull(GMOVI.KIT, '''') as CODKIT
				,		IsNull(
								(
									SELECT DesProd FROM 
									[' + @pv_BaseDatos + '].softland.iw_tprod
									WHERE codprod = GMOVI.KIT
								), '''') as Nombrekit 
				,		convert(varchar,GSAEN.fechaVenc,105) AS fechaVenc
				,		prod.esconfig as eskit 
				,		gmovi.kit 
				,		(case when gmovi.kit is null then gmovi.codprod else gmovi.kit end) as CKit 
				,		(case when gmovi.kit is null then prod.desprod else PKIT.DesProd end) as DKit
				,		gsaen.FecHoraCreacionVW
				*/
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) AND convert(datetime,getdate(),120)	
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	select distinct 
						gsaen.tipo
				,		gsaen.folio
				,		convert(varchar,gsaen.fecha,105) AS fecha
				--,		gsaen.codbode
				,		gsaen.codaux
				--,		cwt.NomAux
				--,		cwt.DirAux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes 
				--,		gmovi.linea 
				--,		prod.CodGrupo
				--,		grupo.DesGrupo 
				--,		prod.CodSubGr 
				--,		subGrupo.DesSubGr 
				,		gmovi.CodProd
				,		prod.desprod 
				--,		cast(gmovi.DetProd as varchar(max)) AS DetProd 
				--,		mone.DesMon
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				--,       (case when gsaen.porcdesc01>0 then gsaen.porcdesc01 else gmovi.porcdescmov01 end) as TotalDescMov
				,		round(gmovi.TotLinea,1) as TotLinea
				/*
				,		gmovi.TotalBoleta 
				,		gsaen.codcaja
				,		'''' as descaja 
				,		gsaen.CentroDeCosto
				,		'''' as TipoCambio
				,		CostoProm = 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0)
				,		costoTotalProm = 
						(gmovi.cantFacturada * 
						ISNULL((
							select	CostoUnitario 
							from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
							where	CodProd = gmovi.CodProd 
							AND		Fecha =		isnull((
													select	max(Fecha) 
													from	[' + @pv_BaseDatos + '].softland.IW_CostoP 
													where	CodProd = gmovi.CodProd
													and		fecha <= gsaen.fecha
												), convert(datetime,''01/01/1900'',103))
						), 0))
				,		gmovi.Partida 
				,		gmovi.Pieza 
				,		descCl.CatDes
				,		case	when gsaen.SubTipoDocto = ''A'' then ''Afecto'' 
								when gsaen.SubTipoDocto = ''E'' then ''Exento'' 
								when gsaen.SubTipoDocto = ''x'' then ''Exportacion'' 
								when gsaen.SubTipoDocto = ''T'' then ''Docto Electronico'' 
								when gsaen.SubTipoDocto = ''L'' then ''Liquidacion'' 
								when gsaen.SubTipoDocto = ''N'' then ''Liquidacion Factura'' 
								when gsaen.SubTipoDocto = ''O'' then ''Liquidacion electronica'' 
								when gsaen.SubTipoDocto = ''C'' then ''Documento Interno de Venta'' 
								else ''''
						end AS tipoDocto
				,		IsNull(GMOVI.KIT, '''') as CODKIT
				,		IsNull(
								(
									SELECT DesProd FROM 
									[' + @pv_BaseDatos + '].softland.iw_tprod
									WHERE codprod = GMOVI.KIT
								), '''') as Nombrekit 
				,		convert(varchar,GSAEN.fechaVenc,105) AS fechaVenc
				,		prod.esconfig as eskit 
				,		gmovi.kit 
				,		(case when gmovi.kit is null then gmovi.codprod else gmovi.kit end) as CKit 
				,		(case when gmovi.kit is null then prod.desprod else PKIT.DesProd end) as DKit
				,		gsaen.FecHoraCreacionVW
				*/
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) AND convert(datetime,getdate(),120)	
				AND		GSAEN.Estado = ''V''
				AND		GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_TopProductosActual]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_TopProductosActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_GET_DASH_TopProductosActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar (5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProducto,
				PROD.DesProd as nombreProducto,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProducto
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProducto DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProducto,
				PROD.DesProd as nombreProducto,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProducto
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProducto DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_TopProductosFiltro]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_TopProductosFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_GET_DASH_TopProductosFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProductoFiltro,
				PROD.DesProd as nombreProductoFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProductoFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProductoFiltro DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				PROD.CodProd as codProductoFiltro,
				PROD.DesProd as nombreProductoFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalProductoFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY PROD.CodProd,PROD.DesProd
				ORDER BY VentaTotalProductoFiltro DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetCantProductos]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetCantProductos]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetCantProductos]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductos
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductos
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetCantProductosFiltro]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetCantProductosFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetCantProductosFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductosFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(PROD.CodProd)) AS CantProductosFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetClientesTotales]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetClientesTotales]								*/
/*-- Detalle			: Clientes y montos totales de ventas														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetClientesTotales]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@nvEstado varchar(1),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
SELECT 
	codaux as CodCliente,nomaux as NomCliente,sum(suma) as Total
		FROM (
				SELECT 
					--CASE 
					--	WHEN nv.nvEstado = ''C''  
					--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
					--	ELSE nvSubTotal 
					--END
					nvSubTotal as ''suma'', nv.CodAux as ''codaux'', aux.NomAux as ''nomaux''
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
				LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
					ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS	
				WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta+ ''',120 )
					and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
		) tmp	
	group by codaux, nomaux
	ORDER BY
		Total desc
'	
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
SELECT 
	codaux as CodCliente,nomaux as NomCliente,sum(suma) as Total
		FROM (
				SELECT 
					--CASE 
					--	WHEN nv.nvEstado = ''C''  
					--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
					--	ELSE nvSubTotal 
					--END
					nvSubTotal as ''suma'', nv.CodAux as ''codaux'', aux.NomAux as ''nomaux''
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
				LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
					ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS	
				WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta+ ''',120 )
					and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
					and nv.VenCod = ''' + @pv_VenCod + '''
		) tmp	
	group by codaux, nomaux
	ORDER BY
		Total desc
'	
EXEC (@query)
end



GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetClientesTotalesBlacklog]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetClientesTotales]								*/
/*-- Detalle			: Clientes y montos totales de ventas historico													*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetClientesTotalesBlacklog]
@pv_BaseDatos AS varchar(100),
@nvEstado varchar(1),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT DISTINCT
				nv.CodAux as CodCliente,aux.NomAux as NomCliente, det.nvTotLinea as Total
				,det.nvnumero --SUM(NSUBTOTAL)
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det
						on nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
						ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS		
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd
						and det.nvnumero = nvprod.nvnumero
					WHERE 
						nv.nvEstado='''+@nvEstado+''' 						
					group by det.NVNumero,nvFecCompr, det.nvCant, nvPrecio, nvTotLinea
					, det.CodProd,nv.CodAux,aux.NomAux, nv.nvSubTotal
					having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0
'	
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT DISTINCT
				nv.CodAux as CodCliente,aux.NomAux as NomCliente, det.nvTotLinea as Total
				,det.nvnumero --SUM(NSUBTOTAL)
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det
						on nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtauxi aux
						ON nv.CodAux = aux.CodAux COLLATE Modern_Spanish_CI_AS		
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd
						and det.nvnumero = nvprod.nvnumero
					WHERE 
						nv.nvEstado='''+@nvEstado+'''
						and nv.VenCod = ''' + @pv_VenCod + '''
					group by det.NVNumero,nvFecCompr, det.nvCant, nvPrecio, nvTotLinea
					, det.CodProd,nv.CodAux,aux.NomAux, nv.nvSubTotal
					having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0
'	
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetDetalleVentasPorCliente]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetDetalleVentasPorCliente]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN						*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetDetalleVentasPorCliente]
@pv_BaseDatos AS varchar (100),
@fechaDesde AS varchar (10),
@fechaHasta varchar (10),
@codaux varchar (100)
AS
declare @query as nvarchar(max)

SELECT @query = ''

SELECT @query = @query + '
	SELECT

				gsaen.Folio,CONVERT(varchar,gsaen.Fecha,105) as Fecha, gsaen.Tipo, sum(gsaen.NetoAfecto) as Total
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 			 					
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,'''+@fechaDesde+''',120) AND convert(datetime,'''+@fechaHasta+''',120)
				AND		GSAEN.Estado = ''V''
				AND     gsaen.codaux = '''+@codaux+'''				
				GROUP BY GSAEN.FOLIO, gsaen.Fecha, GSAEN.TIPO, total
				ORDER BY Total DESC
'
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeCantVendedoresActual]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeCantVendedoresActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeCantVendedoresActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeCantVendedoresActualAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeCantVendedoresActualAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeCantVendedoresActualAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
'
exec(@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
exec(@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeCantVendedoresFiltro]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeCantVendedoresFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeCantVendedoresFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.CodVendedor)) AS CantVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActual]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActualAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosActualAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeClientesAtendidosActualAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
as
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesActualAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltro]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltroAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesAtendidosFiltroAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeClientesAtendidosFiltroAnterior]
@pv_BaseDatos AS varchar(100),
@fechaHasta varchar(12),
@fechaDesde varchar(12),
@pv_VenCod varchar(5)
as
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesCompraActual]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesCompraActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeClientesCompraActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreCliente,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotal
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotal DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreCliente,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotal
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotal DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesCompraFiltro]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesCompraFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeClientesCompraFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreClienteFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalCliFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotalCliFiltro DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    top(5)
				cwt.nomaux as nombreClienteFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalCliFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY cwt.nomaux
				ORDER BY VentaTotalCliFiltro DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesVentaDia]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesVentaDia]				*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetInformeClientesVentaDia]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDia
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00 '' ,120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDia
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00 '' ,120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeClientesVentaDiaAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeClientesVentaDiaAnterior]		*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetInformeClientesVentaDiaAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT
				count(distinct(GSAEN.codaux)) AS CantClientesDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaActual]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetInformeVentaActual]
@pv_BaseDatos AS varchar (100),
@pv_CodVendedor as varchar (100)
AS
declare @query as nvarchar(max)

if(@pv_CodVendedor = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalHoy
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalHoy
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_CodVendedor + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaActualAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaActualAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetInformeVentaActualAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalAnterior
				FROM	['+@pv_BaseDatos+'].[softland].IW_GMOVI GMOVI 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN ['+ @pv_BaseDatos +'].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join ['+ @pv_BaseDatos +'].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join ['+ @pv_BaseDatos +'].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join ['+ @pv_BaseDatos +'].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
'
exec (@query)
end
else begin
SELECT @query = ''
SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalAnterior
				FROM	['+@pv_BaseDatos+'].[softland].IW_GMOVI GMOVI 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN ['+ @pv_BaseDatos +'].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN ['+ @pv_BaseDatos +'].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join ['+ @pv_BaseDatos +'].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join ['+ @pv_BaseDatos +'].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join ['+ @pv_BaseDatos +'].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join ['+ @pv_BaseDatos +'].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join ['+ @pv_BaseDatos +'].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate())-1,''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,DATEADD(year, -1,GETDATE()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
exec (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaFiltro]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeVentaFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentaFiltroAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentaFiltroAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeVentaFiltroAnterior]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalFiltroAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,DATEADD(dd,-(DAY(DATEADD(mm,1,''' + @fechaDesde + '''))-1),DATEADD(mm,-1,''' + @fechaDesde + ''')),120) AND convert(datetime,DATEADD(dd,-(DAY(''' + @fechaHasta + ''')),''' + @fechaHasta + '''),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentasVendedoresActual]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentasVendedoresActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeVentasVendedoresActual]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedor,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedor
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedor DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedor,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedor
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-'',MONTH (getdate()),''-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedor DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetInformeVentasVendedoresFiltro]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetInformeVentasVendedoresFiltro]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetInformeVentasVendedoresFiltro]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3')begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedorFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedorFiltro DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				vendedor.VenDes as nombreVendedorFiltro,
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalVendedorFiltro
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @fechaDesde + ''',120) AND convert(datetime,''' + @fechaHasta + ''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY vendedor.VenDes
				ORDER BY VentaTotalVendedorFiltro DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetNotasDeVentaPorCliente]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetNotasDeVentaPorCliente]								*/
/*-- Detalle			: Detalle Notas de Venta por cliente														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetNotasDeVentaPorCliente]
@pv_BaseDatos AS varchar(100),
@codAux AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@nvEstado varchar(12)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
	SELECT 
				nv.nvestado,nv.nvNumero, CONVERT(varchar,nvfem,105) as nvfem, codaux, VenDes, 
				CASE 
					 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
						THEN (select sum(subtotal) from[' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
						ELSE  nvSubTotal
				END as ''nvmonto''
					FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
						LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtvend ven
							ON nv.VenCod= ven.VenCod	
						LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero
						WHERE 
							nv.codaux='''+@codAux+''' 
							AND nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120 )
							AND nvEstado in ('''+@nvEstado+''',''C'',''P'')	
							group by nv.nvNumero, nvfem, codaux, VenDes, nvSubTotal, nv.nvestado
						ORDER BY
							nvfem desc
'	
EXEC (@query)


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetNotasDeVentaPorClienteBlacklog]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetClientesTotalesBlacklog]								*/
/*-- Detalle			: Detalle Notas de Venta por cliente														*/
/*-- Autor				: SIABA												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetNotasDeVentaPorClienteBlacklog]
@pv_BaseDatos AS varchar(100),
@codAux AS varchar(100),
@nvEstado varchar(12)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
	SELECT
				nv.nvestado, nv.nvNumero, CONVERT(varchar,nvfem,105) as nvfem, nv.codAux, VenDes
				, nv.nvSubTotal as nvmonto, det.nvTotLinea
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					LEFT JOIN [' + @pv_BaseDatos + '].softland.cwtvend ven
						ON nv.VenCod= ven.VenCod
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
						ON nv.NVNumero= det.NVNumero
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and det.nvnumero = nvprod.nvnumero				 
					WHERE 
						nvEstado='''+@nvEstado+''' and codaux='''+@codAux+''' 						
				group by nv.nvEstado, nv.NVNumero, nv.nvFem, nv.CodAux, ven.VenDes, nv.nvSubTotal
					, det.nvTotLinea
				having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0	
'	
EXEC (@query)

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetPeriodoActual]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetPeriodoActual]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[DS_SP_DASH_GetPeriodoActual]
@pv_BaseDatos AS varchar(100)
AS
DECLARE @query AS nvarchar(max)
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				month(GSAEN.Fecha) as mes, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalActual
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR (getdate()),''-01-'',''01''),120) AND convert(datetime,getdate(),120)
				AND		GSAEN.Estado = ''V''
				group by month(GSAEN.Fecha)
				order by month(GSAEN.Fecha)
'
exec (@query)
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetProductos]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetProductos]								*/
/*-- Detalle			: Productos por nota de venta														*/
/*-- Autor				: SIABA												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetProductos]
@pv_BaseDatos AS varchar(100),
@NVNumero AS varchar(100)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
	SELECT DISTINCT
				nv.NVNumero, CONVERT(varchar,nvFecCompr,105) as nvFecCompr, pro.DesProd,
				CASE 
					 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
						THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
						ELSE  det.nvcant
				END as ''nvCant'',				
				nvPrecio,
				((CASE 
					 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
						THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
						ELSE  det.nvcant
				END)*nvprecio) as ''nvTotLinea''	
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
							ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					LEFT JOIN [' + @pv_BaseDatos + '].softland.nw_nventa nv 
							ON nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero
					WHERE 
						nv.NVNumero='''+@NVNumero+'''		
					group by nv.NVNumero, nvFecCompr,pro.DesProd, det.CodProd, nv.nvEstado, det.nvCant, det.nvPrecio, nvTotLinea									
'	
EXEC (@query)


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetProductosBlacklog]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetProductos]								*/
/*-- Detalle			: Productos por nota de venta														*/
/*-- Autor				: SIABA												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetProductosBlacklog]
@pv_BaseDatos AS varchar(100),
@NVNumero AS varchar(100)
AS
DECLARE @query AS nvarchar (max)

SELECT @query = ''
SELECT @query = '
	SELECT
				det.NVNumero, CONVERT(varchar,nvFecCompr,105) as nvFecCompr, pro.DesProd, det.nvCant, nvPrecio, nvTotLinea 
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
						ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and det.nvnumero = nvprod.nvnumero		 
					WHERE 
						det.NVNumero='''+@NVNumero+'''
					group by det.NVNumero,nvFecCompr, pro.DesProd, det.nvCant, nvPrecio, nvTotLinea, det.CodProd
					having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0
					order by det.CodProd
'	
EXEC (@query)
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetProductosVenta]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetProductosVenta]								*/
/*-- Detalle			:														*/
/*-- Autor				: FUDRAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE PROC [dbo].[DS_SP_DASH_GetProductosVenta]
@pv_BaseDatos AS varchar (100),
@folio varchar (100),
@tipo varchar (1)
AS
declare @query as nvarchar(max)

SELECT @query = ''

SELECT @query = @query + '
	SELECT
				gsaen.Folio, DetProd, CantFacturada, PreUniMB, TotLinea 
				FROM [' + @pv_BaseDatos + '].softland.iw_gsaen gsaen 
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_gmovi mov
						ON gsaen.NroInt = mov.NroInt
				WHERE gsaen.Folio = '''+@folio+''' 
					  AND mov.tipo='''+@tipo+'''
					  AND GSAEN.tipo!=''s''
					  AND GSAEN.tipo!=''a''
					  AND GSAEN.tipo!=''e''
					  AND GSAEN.Estado = ''V''
				ORDER BY Total DESC
'
EXEC (@query)
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetTotalNotasDeVenta]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetTotalNotasDeVenta]								*/
/*-- Detalle			: Total Montos Notas de Venta														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetTotalNotasDeVenta]
@pv_BaseDatos AS varchar(100),
@fechaDesde varchar(12),
@fechaHasta varchar(12),
@nvEstado varchar(1),
@pv_CodVendedor varchar (5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_CodVendedor = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT sum(suma) as Total
	FROM (
		SELECT 
				--CASE 
				--	WHEN nv.nvEstado = ''C''  
				--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
				--	ELSE nvSubTotal 
				--END
				nvSubTotal as ''suma'' 
			FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
			WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120 )
				and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
	) tmp
'
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT sum(suma) as Total
	FROM (
		SELECT 
				--CASE 
				--	WHEN nv.nvEstado = ''C''  
				--	THEN (select sum(SubTotal) from [' + @pv_BaseDatos + '].softland.iw_gsaen g where g.nvnumero = nv.NVNumero and g.tipo=''f'')
				--	ELSE nvSubTotal 
				--END
				nvSubTotal as ''suma'' 
			FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
			WHERE nvFem BETWEEN convert(datetime,''' + @fechaDesde + ''',120 ) AND convert(datetime,''' + @fechaHasta + ''',120 )
				and nv.nvestado IN ('''+@nvEstado+''',''c'',''p'')
				and nv.VenCod = ''' + @pv_CodVendedor + '''
	) tmp
'
EXEC (@query)
end


GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetTotalNotasDeVentaBlacklog]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetTotalNotasDeVenta]								*/
/*-- Detalle			: Total Montos Notas de Venta blacklog														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetTotalNotasDeVentaBlacklog]
@pv_BaseDatos AS varchar(100),
@nvEstado varchar(1),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar (max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''
SELECT @query = '
	SELECT
				det.nvTotLinea as Total,nv.nvnumero
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
				left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
						on det.NVNumero = nv.NVNumero
				left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd
						and det.nvnumero = nvprod.nvnumero
					WHERE 
						nvEstado='''+@nvEstado+'''	
					group by det.nvTotLinea,nv.nvnumero
					having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0					
'
EXEC (@query)
end
else begin
SELECT @query = ''
SELECT @query = '
	SELECT
				det.nvTotLinea as Total,nv.nvnumero
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
				left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
						on det.NVNumero = nv.NVNumero
				left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd
						and det.nvnumero = nvprod.nvnumero
					WHERE 
						nv.nvEstado='''+@nvEstado+'''
						and nv.VenCod = ''' + @pv_VenCod + '''
					group by det.nvTotLinea,nv.nvnumero
					having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0					
'
EXEC (@query)
end

GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetVendedoresSoftland]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetVendedoresSoftland]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
CREATE proc [dbo].[DS_SP_DASH_GetVendedoresSoftland]
@pv_BaseDatos varchar(100)
as
begin

declare @query nvarchar(max)

select @query = ''

select @query = @query + '
	select VenCod, VenDes, CodTipV, EMail, Usuario 
	from [' + @pv_BaseDatos + '].softland.cwtvend
'
exec(@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_SP_DASH_GetVentasPorCliente]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_SP_GetVentasPorCliente]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create PROC [dbo].[DS_SP_DASH_GetVentasPorCliente]
@pv_BaseDatos AS varchar (100),
@fechaDesde AS varchar (10),
@fechaHasta varchar (10),
@pv_VenCod varchar(5)
AS
declare @query as nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				gsaen.codaux as CodCliente, cwt.NomAux as NomCliente, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as Total
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,'''+@fechaDesde+''',120) AND convert(datetime,'''+@fechaHasta+''',120)
				AND		GSAEN.Estado = ''V''
				GROUP BY gsaen.codaux, cwt.NomAux
				ORDER BY Total DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT

				gsaen.codaux as CodCliente, cwt.NomAux as NomCliente, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as Total
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,'''+@fechaDesde+''',120) AND convert(datetime,'''+@fechaHasta+''',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				GROUP BY gsaen.codaux, cwt.NomAux
				ORDER BY Total DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetInformeVentasMesAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetInformeVentasMesAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[SP_GET_DASH_GetInformeVentasMesAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
					sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaMesAnterior
					FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
							ON GMOVI.CODPROD = PROD.CODPROD 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
							ON GMOVI.KIT = PKIT.CODPROD 
						INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
							ON GMOVI.NROINT = GSAEN.NROINT 
							AND GSAEN.TIPO = GMOVI.Tipo 
						left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
							ON gsaen.codaux = cwt.codaux 
						left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
							ON vendedor.VenCod = GSAEN.CodVendedor  
						left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
							ON cl.CodAux = GSAEN.CodAux  
						left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
							ON descCl.CatCod = cl.CatCli  
						left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
							ON grupo.CodGrupo = prod.CodGrupo  
						left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
							ON subGrupo.CodSubGr = prod.CodSubGr  
						left join [' + @pv_BaseDatos + '].softland.cwtmone mone
							on GSAEN.CodMoneda = mone.CodMon
					WHERE	GSAEN.tipo!=''s''
					AND		GSAEN.tipo!=''a''
					AND		GSAEN.tipo!=''e''
					AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
					AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	DECLARE @mydate DATETIME
	SELECT @mydate = GETDATE()
	SELECT
					sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaMesAnterior
					FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
							ON GMOVI.CODPROD = PROD.CODPROD 
						LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
							ON GMOVI.KIT = PKIT.CODPROD 
						INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
							ON GMOVI.NROINT = GSAEN.NROINT 
							AND GSAEN.TIPO = GMOVI.Tipo 
						left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
							ON gsaen.codaux = cwt.codaux 
						left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
							ON vendedor.VenCod = GSAEN.CodVendedor  
						left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
							ON cl.CodAux = GSAEN.CodAux  
						left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
							ON descCl.CatCod = cl.CatCli  
						left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
							ON grupo.CodGrupo = prod.CodGrupo  
						left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
							ON subGrupo.CodSubGr = prod.CodSubGr  
						left join [' + @pv_BaseDatos + '].softland.cwtmone mone
							on GSAEN.CodMoneda = mone.CodMon
					WHERE	GSAEN.tipo!=''s''
					AND		GSAEN.tipo!=''a''
					AND		GSAEN.tipo!=''e''
					AND		GSAEN.FECHA BETWEEN DATEADD(dd,-(DAY(DATEADD(mm,1,@mydate))-1),DATEADD(mm,-1,@mydate)) AND DATEADD(dd,-(DAY(@mydate)),@mydate)
					AND		GSAEN.Estado = ''V''
					AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetPeriodoAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetPeriodoAnterior]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[SP_GET_DASH_GetPeriodoAnterior]
@pv_BaseDatos AS varchar(100)
AS
DECLARE @query AS nvarchar(max)
SELECT @query = ''

SELECT @query = @query + '
SELECT
				month(GSAEN.Fecha) as mesAnterior, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR ( dateadd(year,-1,getdate()) ),''-01-'',''01''),120) AND convert(datetime,CONCAT (YEAR ( dateadd(year,-1,getdate()) ),''-12-'',''31''),120)
				AND		GSAEN.Estado = ''V''
				group by month(GSAEN.Fecha)
				order by month(GSAEN.Fecha)
'
EXEC (@query)
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetPeriodoTercero]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetPeriodoTercero]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[SP_GET_DASH_GetPeriodoTercero]
@pv_BaseDatos AS varchar(100)
AS
DECLARE @query AS nvarchar(max)
SELECT @query = ''

SELECT @query = @query + '
	SELECT
				month(GSAEN.Fecha) as mesTercero, sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 0)) as VentaTotalTercero
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,CONCAT (YEAR ( dateadd(year,-2,getdate()) ),''-01-'',''01''),120) AND convert(datetime,CONCAT (YEAR ( dateadd(year,-2,getdate()) ),''-12-'',''31''),120)
				AND		GSAEN.Estado = ''V''
				group by month(GSAEN.Fecha)
				order by month(GSAEN.Fecha)
'
EXEC (@query)
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetVentaTotalDia]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetVentaTotalDia]               			*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[SP_GET_DASH_GetVentaTotalDia]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalDia
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha = convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) 
				AND		GSAEN.Estado = ''V''
				ORDER BY VentaTotalDia DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT    
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalDia
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha = convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) 
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				ORDER BY VentaTotalDia DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_DASH_GetVentaTotalDiaAnterior]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_GetVentaTotalDiaAnterior]    			*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
create proc [dbo].[SP_GET_DASH_GetVentaTotalDiaAnterior]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT    
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
				ORDER BY VentaTotalDiaAnterior DESC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT    
				sum(round((gmovi.TotLinea - ((((gmovi.TotLinea * 100) / (case when gsaen.SubTotal =0 THEN 1 ELSE gsaen.SubTotal end)) * gsaen.TotalDesc) / 100)), 1)) as VentaTotalDiaAnterior
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				and     GSAEN.Fecha between convert(datetime, convert(varchar,dateadd(dd,-1,getdate()),111) + '' 00:00:00 '',120) AND convert(datetime,dateadd(dd,-1,getdate()),120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
				ORDER BY VentaTotalDiaAnterior DESC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[SP_GET_LOGIN]    Script Date: 22/01/2021 11:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Empresa			: DISOFI												*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[SP_GET_LOGIN]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
ALTER procedure [dbo].[SP_GET_LOGIN]  
	@Nombre varchar(15),       
	@Contrasena varchar(50)       
as 
begin
	set nocount on       
	
	select	u.Id
	,		u.Usuario
	,		u.TipoUsuario
	,		u.CodigoUsuario
	from	DS_Usuarios u       
	where	u.Usuario = @Nombre 
	and		u.Contrasena = @Contrasena 
	AND		u.Estado = 1  
	
	set nocount OFF       
end  
GO
