GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelVentas]    Script Date: 31/03/2021 19:35:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_DASH_ExcelVentas]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
ALTER PROC [dbo].[DS_GET_DASH_ExcelVentas]
@pv_BaseDatos AS varchar(100),
@pv_FechaDesde varchar(20),
@pv_FechaHasta varchar(20),
@pv_VenCod varchar(5)
AS
DECLARE	@query nvarchar(max)

if(@pv_VenCod = '-3') begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		PROD.CodGrupo
				,		grupo.DesGrupo
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
'
EXEC ( @query)
end
else begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		PROD.CodGrupo
				,		grupo.DesGrupo
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC ( @query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelVentasDia]    Script Date: 31/03/2021 19:37:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelVentasDia]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
ALTER PROC [dbo].[DS_GET_DASH_ExcelVentasDia]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	select distinct 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				--,		gsaen.codbode
				,		gsaen.codaux
				--,		cwt.NomAux
				--,		cwt.DirAux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes 
				--,		gmovi.linea 
				,		gmovi.CodProd
				,		prod.CodGrupo
				,		grupo.DesGrupo 
				--,		prod.CodSubGr 
				--,		subGrupo.DesSubGr 
				,		prod.desprod 
				--,		cast(gmovi.DetProd as varchar(max)) AS DetProd 
				--,		mone.DesMon
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				--,       (case when gsaen.porcdesc01>0 then gsaen.porcdesc01 else gmovi.porcdescmov01 end) as TotalDescMov
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) AND convert(datetime,getdate(),120)	
				AND		GSAEN.Estado = ''V''
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	select distinct 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				--,		gsaen.codbode
				,		gsaen.codaux
				--,		cwt.NomAux
				--,		cwt.DirAux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes 
				--,		gmovi.linea 
				,		gmovi.CodProd
				,		prod.CodGrupo
				,		grupo.DesGrupo 
				--,		prod.CodSubGr 
				--,		subGrupo.DesSubGr 
				,		prod.desprod 
				--,		cast(gmovi.DetProd as varchar(max)) AS DetProd 
				--,		mone.DesMon
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				--,       (case when gsaen.porcdesc01>0 then gsaen.porcdesc01 else gmovi.porcdescmov01 end) as TotalDescMov
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND     GSAEN.Fecha between convert(datetime,convert(varchar,getdate(),111) + '' 00:00:00'' ,120) AND convert(datetime,getdate(),120)	
				AND		GSAEN.Estado = ''V''
				AND		GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelVentas]    Script Date: 31/03/2021 19:40:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelVentas]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
ALTER PROC [dbo].[DS_GET_DASH_ExcelVentas]
@pv_BaseDatos AS varchar(100),
@pv_FechaDesde varchar(20),
@pv_FechaHasta varchar(20),
@pv_VenCod varchar(5)
AS
DECLARE	@query nvarchar(max)

if(@pv_VenCod = '-3') begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		PROD.CodGrupo
				,		grupo.DesGrupo
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
'
EXEC ( @query)
end
else begin
	select	@query = ''

	select	@query = @query + '
		select 
						gsaen.tipo
				,		gsaen.folio
				,		NumOc = (select NumOc from [' + @pv_BaseDatos + '].softland.nw_nventa where nvnumero = GSAEN.nvnumero)
				,		convert(varchar,gsaen.fecha,105) AS fecha
				,		gsaen.codaux
				,		gsaen.CodVendedor 
				,		vendedor.VenDes  
				,		gmovi.CodProd
				,		PROD.CodGrupo
				,		grupo.DesGrupo
				,		prod.desprod
				,		gmovi.CantFacturada
				,		gmovi.preunimb 
				,		round(gmovi.TotLinea,1) as TotLinea
				FROM	[' + @pv_BaseDatos + '].[softland].IW_GMOVI GMOVI 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PROD 
						ON GMOVI.CODPROD = PROD.CODPROD 
					LEFT JOIN [' + @pv_BaseDatos + '].[softland].IW_TPROD PKIT 
						ON GMOVI.KIT = PKIT.CODPROD 
					INNER JOIN [' + @pv_BaseDatos + '].[softland].IW_GSAEN GSAEN 
						ON GMOVI.NROINT = GSAEN.NROINT 
						AND GSAEN.TIPO = GMOVI.Tipo 
					left join [' + @pv_BaseDatos + '].[softland].cwtauxi cwt 
						ON gsaen.codaux = cwt.codaux 
					left join [' + @pv_BaseDatos + '].[softland].cwtvend vendedor 
						ON vendedor.VenCod = GSAEN.CodVendedor  
					left join [' + @pv_BaseDatos + '].[softland].cwtcvcl cl 
						ON cl.CodAux = GSAEN.CodAux  
					left join [' + @pv_BaseDatos + '].[softland].cwtcgau descCl 
						ON descCl.CatCod = cl.CatCli  
					left join [' + @pv_BaseDatos + '].[softland].iw_tgrupo grupo 
						ON grupo.CodGrupo = prod.CodGrupo  
					left join [' + @pv_BaseDatos + '].[softland].iw_tsubgr subGrupo 
						ON subGrupo.CodSubGr = prod.CodSubGr  
					left join [' + @pv_BaseDatos + '].softland.cwtmone mone
						on GSAEN.CodMoneda = mone.CodMon
				WHERE	GSAEN.tipo!=''s''
				AND		GSAEN.tipo!=''a''
				AND		GSAEN.tipo!=''e''
				AND		GSAEN.FECHA BETWEEN convert(datetime,''' + @pv_FechaDesde + ''' + '' 00:00:00'',120) AND convert(datetime,''' + @pv_FechaHasta + ''' + '' 00:00:00'',120)
				AND		GSAEN.Estado = ''V''
				AND     GSAEN.CodVendedor = ''' + @pv_VenCod + '''
'
EXEC ( @query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelNotasDeVenta]    Script Date: 31/03/2021 19:41:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelNotasDeVenta]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
ALTER PROC [dbo].[DS_GET_DASH_ExcelNotasDeVenta]
@pv_BaseDatos AS varchar(100),
@pv_FechaDesde varchar(20),
@pv_FechaHasta varchar(20),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT DISTINCT
					nv.CodAux as CodigoCliente
				,   auxi.NomAux as NombreCliente
				,	nv.NVNumero	as NotaDeVenta	
				,   nv.NumOC as NumeroOc
				,   nv.VenCod as CodigoVendedor
				,   ven.VenDes as NombreVendedor
				,   CodigoProducto = (select CodProd from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd)
				,	pro.DesProd as Producto
				,   CodGrupo = pro.CodGrupo
				,   DesGrupo = (select DesGrupo from [' + @pv_BaseDatos + '].[softland].iw_tgrupo where CodGrupo = pro.CodGrupo)
				,   CASE 
					 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
						THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
						ELSE  det.nvcant
				END as ''Cantidad'',								
				--((CASE 
				--	 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
				--		ELSE  det.nvcant
				--END)
				(det.nvcant*nvprecio) as ''ValorNV''
				,	convert(varchar,nv.nvFem,105) AS ''FechaEmision''
				,	convert(varchar,nvFecCompr,105) as ''FechaDespacho''
				,	convert(varchar,nvFeEnt,105) as ''FechaEntrega'' 
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
							ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					LEFT JOIN [' + @pv_BaseDatos + '].softland.nw_nventa nv 
							ON nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero	
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
							on ven.VenCod = nv.VenCod
					left join [' + @pv_BaseDatos + '].softland.cwtauxi auxi
							on auxi.CodAux = nv.CodAux
				WHERE nv.nvEstado in (''a'',''c'',''p'')
				and nvFem BETWEEN convert(datetime,''' + @pv_FechaDesde + ''',120) AND convert(datetime,''' + @pv_FechaHasta + ''',120)
					group by nv.NVNumero, nvFecCompr,pro.DesProd, det.CodProd, nv.nvEstado, det.nvCant, det.nvPrecio, nvTotLinea
					, nv.nvFem, nvFecCompr, nvFeEnt,nv.CodAux,nv.NomCon,nv.VenCod,ven.VenDes,auxi.NomAux, pro.CodGrupo, nv.NumOC
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
	SELECT DISTINCT
					nv.CodAux as CodigoCliente
				,   auxi.NomAux as NombreCliente
				,	nv.NVNumero	as NotaDeVenta	
				,   nv.NumOC as NumeroOc
				,   nv.VenCod as CodigoVendedor
				,   ven.VenDes as NombreVendedor
				,   CodigoProducto = (select CodProd from [' + @pv_BaseDatos + '].softland.iw_tprod where CodProd = det.CodProd)
				,	pro.DesProd as Producto
				,   CodGrupo = pro.CodGrupo
				,   DesGrupo = (select DesGrupo from [' + @pv_BaseDatos + '].[softland].iw_tgrupo where CodGrupo = pro.CodGrupo)
				,   CASE 
					 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
						THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
						ELSE  det.nvcant
				END as ''Cantidad'',								
				--((CASE 
				--	 WHEN (nv.nvEstado=''C'' and sum(vis.nvcant)!=sum(vis.NVCantFact))
				--		THEN (select nvCantFact from [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis where vis.nvnumero = nv.nvnumero and vis.CodProd = det.CodProd)
				--		ELSE  det.nvcant
				--END)
				(det.nvcant*nvprecio) as ''ValorNV''
				,	convert(varchar,nv.nvFem,105) AS ''FechaEmision''
				,	convert(varchar,nvFecCompr,105) as ''FechaDespacho''
				,	convert(varchar,nvFeEnt,105) as ''FechaEntrega'' 
				FROM [' + @pv_BaseDatos + '].softland.nw_detnv det
					LEFT JOIN [' + @pv_BaseDatos + '].softland.iw_tprod pro
							ON det.CodProd=pro.CodProd COLLATE Modern_Spanish_CI_AS		
					LEFT JOIN [' + @pv_BaseDatos + '].softland.nw_nventa nv 
							ON nv.NVNumero = det.NVNumero
					LEFT JOIN [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd vis
							ON nv.NVNumero = vis.NVNumero	
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
							on ven.VenCod = nv.VenCod
					left join [' + @pv_BaseDatos + '].softland.cwtauxi auxi
							on auxi.CodAux = nv.CodAux
				WHERE nv.nvEstado in (''a'',''c'',''p'')
				and nvFem BETWEEN convert(datetime,''' + @pv_FechaDesde + ''',120) AND convert(datetime,''' + @pv_FechaHasta + ''',120)
				and nv.VenCod = ''' + @pv_VenCod + '''
					group by nv.NVNumero, nvFecCompr,pro.DesProd, det.CodProd, nv.nvEstado, det.nvCant, det.nvPrecio, nvTotLinea
					, nv.nvFem, nvFecCompr, nvFeEnt,nv.CodAux,nv.NomCon,nv.VenCod,ven.VenDes,auxi.NomAux, pro.CodGrupo, nv.NumOC
'
EXEC (@query)
end
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_DASH_ExcelNotasDeVentaBacklog]    Script Date: 31/03/2021 19:42:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*------------------------------------------------------------------------------*/
/*-- Tipo				: Procedimiento											*/
/*-- Nombre				: [dbo].[DS_GET_ExcelNotasDeVentaBacklog]								*/
/*-- Detalle			:														*/
/*-- Autor				: FDURAN												*/
/*-- Modificaciones		:														*/
/*------------------------------------------------------------------------------*/
ALTER PROC [dbo].[DS_GET_DASH_ExcelNotasDeVentaBacklog]
@pv_BaseDatos AS varchar(100),
@pv_VenCod varchar(5)
AS
DECLARE @query AS nvarchar(max)

if(@pv_VenCod = '-3') begin
SELECT @query = ''

SELECT @query = @query + '
SELECT 
				NombreCliente = (select nomaux from emeltec.softland.cwtauxi where CodAux = nv.CodAux)
				,nv.codAux as CodigoCliente
				,nv.nvNumero as NotaDeVenta
				,nv.NumOC
				,nv.VenCod as CodigoVendedor
				,ven.VenDes as NombreVendedor
				,CodigoProducto = (select CodProd from emeltec.softland.iw_tprod where CodProd = det.CodProd)
				,Producto = (select desprod from Emeltec.softland.iw_tprod where CodProd = det.CodProd)
				,CodGrupo = (select CodGrupo from Emeltec.softland.iw_tprod where CodProd = det.CodProd)
				,DesGrupo = (select DesGrupo from EMELTEC.[softland].iw_tgrupo where CodGrupo = (select CodGrupo from Emeltec.softland.iw_tprod where CodProd = det.CodProd))
				,det.nvTotLinea as ValorNV
				,convert(varchar,nv.nvFem,105) AS FechaEmision
				,convert(varchar,nvFecCompr,105) as FechaDespacho
				,convert(varchar,nvFeEnt,105) as FechaEntrega 
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
						ON nv.NVNumero= det.NVNumero
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and det.nvnumero = nvprod.nvnumero		
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
						on ven.VenCod = nv.VenCod 		 
					WHERE 
						nvEstado=''A''
						AND nvFem BETWEEN convert(datetime,''2000-01-01'',120) AND convert(datetime,getdate(),120) 						
				group by nv.nvEstado, nv.NVNumero, nv.nvFem, nv.CodAux, nv.nvSubTotal
					, det.nvTotLinea,det.CodProd,nvFecCompr,nvFeEnt,nv.VenCod, ven.VenDes
					, nv.NumOC
				having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0
				order by CodAux asc
'
EXEC (@query)
end
else begin
SELECT @query = ''

SELECT @query = @query + '
SELECT 
				NombreCliente = (select nomaux from emeltec.softland.cwtauxi where CodAux = nv.CodAux)
				,nv.codAux as CodigoCliente
				,nv.nvNumero as NotaDeVenta
				,nv.NumOC
				,nv.VenCod as CodigoVendedor
				,ven.VenDes as NombreVendedor
				,CodigoProducto = (select CodProd from emeltec.softland.iw_tprod where CodProd = det.CodProd)
				,Producto = (select desprod from Emeltec.softland.iw_tprod where CodProd = det.CodProd)
				,CodGrupo = (select CodGrupo from Emeltec.softland.iw_tprod where CodProd = det.CodProd)
				,DesGrupo = (select DesGrupo from EMELTEC.[softland].iw_tgrupo where CodGrupo = (select CodGrupo from Emeltec.softland.iw_tprod where CodProd = det.CodProd))
				,det.nvTotLinea as ValorNV
				,convert(varchar,nv.nvFem,105) AS FechaEmision
				,convert(varchar,nvFecCompr,105) as FechaDespacho
				,convert(varchar,nvFeEnt,105) as FechaEntrega 
				FROM [' + @pv_BaseDatos + '].softland.nw_nventa nv
					left join [' + @pv_BaseDatos + '].softland.nw_detnv det 
						ON nv.NVNumero= det.NVNumero
					left join [' + @pv_BaseDatos + '].softland.NW_vsnpDetNVProd nvprod 
						ON det.CodProd = nvprod.CodProd	
						and det.nvnumero = nvprod.nvnumero		
					left join [' + @pv_BaseDatos + '].softland.cwtvend ven
						on ven.VenCod = nv.VenCod 		 
					WHERE 
						nvEstado=''A''
						AND nvFem BETWEEN convert(datetime,''2000-01-01'',120) AND convert(datetime,getdate(),120) 						
						AND nv.VenCod = ''' + @pv_VenCod + '''
				group by nv.nvEstado, nv.NVNumero, nv.nvFem, nv.CodAux, nv.nvSubTotal
					, det.nvTotLinea,det.CodProd,nvFecCompr,nvFeEnt,nv.VenCod, ven.VenDes
					, nv.NumOC
				having SUM( (nvprod.nvcant - nvprod.NVCantDesp) + (nvprod.nvCantNC * -1)) > 0
				order by CodAux asc
'
EXEC (@query)
end