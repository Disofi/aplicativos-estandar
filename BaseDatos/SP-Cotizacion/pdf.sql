ALTER procedure [dbo].[SP_GET_COT_PdfCotizacion]
@Id int,
@pv_BaseDatos varchar(100)
as
begin
declare @CodAux varchar(10)
declare @Moneda varchar(60)
declare @CodEdi varchar(3)
declare @query nvarchar(max)
 select @query = ''  
  
 select @query = @query + ' 

select '''+@CodAux +'''= CodAux 
from DS_Cotizacion where Id=' + CONVERT(VARCHAR(20), @Id ) + '

select '''+@Moneda+'''=DesMon, 
       '''+@CodEdi+'''=CodEdi
from [' + @pv_BaseDatos + '].[softland].[cwtmone]
where CodMon=(select CodMon COLLATE SQL_Latin1_General_CP1_CI_AI from DS_Cotizacion where Id=' + CONVERT(VARCHAR(20), @Id ) + ')

SELECT  c.Id, c.IdEmpresaInterna, c.EstadoNP, c.NVNumero, c.nvFem, c.nvEstado, c.nvEstFact, c.nvEstDesp, c.nvEstRese, c.nvEstConc, c.CotNum, c.NumOC, c.nvFeEnt, c.CodAux, c.VenCod, c.CodMon, c.CodLista, c.nvObser, 
        c.nvCanalNV, c.CveCod, c.NomCon, c.CodiCC, c.CodBode, c.nvSubTotal, c.nvPorcDesc01, c.nvDescto01, c.nvPorcDesc02, c.nvDescto02, c.nvPorcDesc03, c.nvDescto03, c.nvPorcDesc04, c.nvDescto04, c.nvPorcDesc05, 
        c.nvDescto05, c.nvMonto, c.nvFeAprob, c.NumGuiaRes, c.nvPorcFlete, c.nvValflete, c.nvPorcEmb, c.nvValEmb, c.nvEquiv, c.nvNetoExento, c.nvNetoAfecto, c.nvTotalDesc, c.ConcAuto, c.CodLugarDesp, c.SolicitadoPor, 
        c.DespachadoPor, c.Patente, c.RetiradoPor, c.CheckeoPorAlarmaVtas, c.EnMantencion, c.Usuario, c.UsuarioGeneraDocto, c.FechaHoraCreacion, c.Sistema, c.ConcManual, c.RutSolicitante, c.proceso, c.TotalBoleta, c.NumReq, 
        c.CodVenWeb, c.CodBodeWms, c.CodLugarDocto, c.RutTransportista, c.Cod_Distrib, c.Nom_Distrib, c.MarcaWG, c.ErrorAprobador, c.ErrorAprobadorMensaje, ISNULL(DS_NotaVentaExtras.nroSolicitud, 0) AS NroCotizacion,
		ISNULL([' + @pv_BaseDatos + '].[softland].[cwtvend].VenDes,'''') nombreVendedor, 
		ISNULL([' + @pv_BaseDatos + '].[softland].[cwtvend].EMail,''contacto@emeltec.cl'')EmailVendedor,
		ISNULL([' + @pv_BaseDatos + '].[softland].[cwtconv].CveDes,'''') FormaPago,
		ISNULL([' + @pv_BaseDatos + '].[softland].[cwtconv].CveDias,'''') PlazoPago,
		ISNULL(DS_NotaVentaExtras.NroFinalCotizacion,''0'') NroFinalCotizacion
FROM    [' + @pv_BaseDatos + '].[softland].[cwtconv] RIGHT OUTER JOIN
        [' + @pv_BaseDatos + '].[softland].[cwtcvcl] ON EMELTEC.softland.cwtconv.CveCod = 
		[' + @pv_BaseDatos + '].[softland].[cwtcvcl].ConVta RIGHT OUTER JOIN DS_Cotizacion AS c ON 
		[' + @pv_BaseDatos + '].[softland].[cwtcvcl].CodAux = c.CodAux COLLATE SQL_Latin1_General_CP1_CI_AI LEFT OUTER JOIN
        [' + @pv_BaseDatos + '].[softland].[cwtvend] ON c.VenCod COLLATE SQL_Latin1_General_CP1_CI_AI = 
		[' + @pv_BaseDatos + '].[softland].[cwtvend].VenCod LEFT OUTER JOIN
        DS_NotaVentaExtras ON c.Id = DS_NotaVentaExtras.IdNotaVenta
		
WHERE   (c.Id = ' + CONVERT(VARCHAR(20), @Id ) + ')


SELECT      DS_CotizacionDetalle.Id, DS_CotizacionDetalle.IdNotaVenta, DS_CotizacionDetalle.NVNumero, DS_CotizacionDetalle.nvLinea, DS_CotizacionDetalle.nvCorrela, DS_CotizacionDetalle.nvFecCompr, 
            DS_CotizacionDetalle.CodProd, DS_CotizacionDetalle.nvCant, DS_CotizacionDetalle.nvPrecio, DS_CotizacionDetalle.nvEquiv, DS_CotizacionDetalle.nvSubTotal, DS_CotizacionDetalle.nvDPorcDesc01, 
            DS_CotizacionDetalle.nvDDescto01, DS_CotizacionDetalle.nvDPorcDesc02, DS_CotizacionDetalle.nvDDescto02, DS_CotizacionDetalle.nvDPorcDesc03, DS_CotizacionDetalle.nvDDescto03, 
            DS_CotizacionDetalle.nvDPorcDesc04, DS_CotizacionDetalle.nvDDescto04, DS_CotizacionDetalle.nvDPorcDesc05, DS_CotizacionDetalle.nvDDescto05, DS_CotizacionDetalle.nvTotDesc, DS_CotizacionDetalle.nvTotLinea, 
            DS_CotizacionDetalle.nvCantDesp, DS_CotizacionDetalle.nvCantProd, DS_CotizacionDetalle.nvCantFact, DS_CotizacionDetalle.nvCantDevuelto, DS_CotizacionDetalle.nvCantNC, DS_CotizacionDetalle.nvCantBoleta, 
            DS_CotizacionDetalle.nvCantOC, DS_CotizacionDetalle.DetProd, DS_CotizacionDetalle.CheckeoMovporAlarmaVtas, DS_CotizacionDetalle.KIT, DS_CotizacionDetalle.CodPromocion, DS_CotizacionDetalle.CodUMed, 
            DS_CotizacionDetalle.CantUVta, DS_CotizacionDetalle.Partida, DS_CotizacionDetalle.Pieza, DS_CotizacionDetalle.FechaVencto, DS_CotizacionDetalle.CantidadKit, DS_CotizacionDetalle.MarcaWG, 
            DS_CotizacionDetalle.PorcIncidenciaKit, DS_NotaVentaExtras.FechaCierre
FROM        DS_CotizacionDetalle LEFT OUTER JOIN
            DS_NotaVentaExtras ON DS_CotizacionDetalle.Id = DS_NotaVentaExtras.IdNotaVenta
where DS_CotizacionDetalle.IdNotaVenta=' + CONVERT(VARCHAR(20), @Id ) + '

select * from (
  
SELECT   C.CodAux COLLATE SQL_Latin1_General_CP1_CI_AI codaux, 
         C.NomAux COLLATE SQL_Latin1_General_CP1_CI_AI cliente, 
         C.NoFAux COLLATE SQL_Latin1_General_CP1_CI_AI NoFAux, 
		 C.RutAux COLLATE SQL_Latin1_General_CP1_CI_AI rut, 
		 C.DirAux COLLATE SQL_Latin1_General_CP1_CI_AI direccion, 
		 C.FonAux1 COLLATE SQL_Latin1_General_CP1_CI_AI fono,  
		 ISNULL(Ciu.CiuDes, '''') AS Ciudad, 
		 ISNULL(Com.ComDes, '''') AS Comuna,
		 '''+@Moneda+'''  Moneda,
		 '''+@CodEdi+''' CodEdi,
		 ISNULL(DS_cwtaxco.NomCon COLLATE SQL_Latin1_General_CP1_CI_AI,''-'') AS NombreContacto,  
		 ISNULL(DS_cwtaxco.FonCon COLLATE SQL_Latin1_General_CP1_CI_AI,''-'') AS FonoContacto, 
		 ISNULL(DS_cwtaxco.Email COLLATE SQL_Latin1_General_CP1_CI_AI,''-'') AS EmailContacto
FROM     DS_cwtauxi AS C LEFT OUTER JOIN
		 DS_cwtaxco ON C.CodAux = DS_cwtaxco.CodAuc LEFT OUTER JOIN
		 [' + @pv_BaseDatos + '].[softland].[cwtciud] AS Ciu ON C.CiuAux COLLATE SQL_Latin1_General_CP1_CI_AI = Ciu.CiuCod LEFT OUTER JOIN
		 [' + @pv_BaseDatos + '].[softland].[cwtcomu] AS Com ON C.ComAux COLLATE SQL_Latin1_General_CP1_CI_AI = Com.ComCod
where    c.sincronizado=0
UNION
SELECT   C.CodAux, C.NomAux, 
         isnull(C.NoFAux,'')NoFAux, C.RutAux, 
		 isnull(C.DirAux,'')DirAux, C.FonAux1, ISNULL(Ciu.CiuDes, '') AS Ciudad, ISNULL(Com.ComDes, '') AS Comuna,
		 '''+@Moneda+'''  Moneda,
		 '''+@CodEdi+''' CodEdi,
		 ISNULL([' + @pv_BaseDatos + '].[softland].[cwtaxco].NomCon,''-'') NombreContacto, 
		 ISNULL([' + @pv_BaseDatos + '].[softland].[cwtaxco].FonCon,''-'') FonoContacto, 
		 ISNULL([' + @pv_BaseDatos + '].[softland].[cwtaxco].Email,''-'') EmailContacto
FROM     [' + @pv_BaseDatos + '].[cwtauxi] AS C LEFT OUTER JOIN
         [' + @pv_BaseDatos + '].[softland].[cwtaxco] ON C.CodAux = EMELTEC.softland.cwtaxco.CodAuc LEFT OUTER JOIN
		 [' + @pv_BaseDatos + '].[softland].[cwtciud] AS Ciu ON C.CiuAux = Ciu.CiuCod LEFT OUTER JOIN
		 [' + @pv_BaseDatos + '].[softland].[cwtcomu] AS Com ON C.ComAux = Com.ComCod
 )tb
WHERE    (CodAux ='''+ @CodAux+''')
'
print @query
exec (@query)
end
