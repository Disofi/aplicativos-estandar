USE [DSNotaVenta]
GO
ALTER TABLE ds_parametros ADD StockProductoCodigoBodegaAdicional VARCHAR(MAX)
GO
ALTER TABLE ds_parametros ADD ManejaValorAdicional BIT
GO
CREATE TABLE DS_NotasVentaDetalleValorAdicional
(
	Id INT IDENTITY(1, 1)
,	IdNotaVentaDetalle INT
,	ValorAdicional INT
)