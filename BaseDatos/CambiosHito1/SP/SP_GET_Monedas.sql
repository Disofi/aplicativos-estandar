﻿USE [DSNotaVenta]
GO
/****** Object:  StoredProcedure [dbo].[DS_GET_ObtenerDatosUsuario]    Script Date: 16-10-2019 13:27:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_GET_Monedas]
@pv_BaseDatos varchar (100)
AS
BEGIN
	DECLARE @query varchar (max)

	SELECT @query = ''

	SELECT @query = @query + '
		SELECT	CodigoMoneda = a.CodMon
		,		DescripcionMoneda = a.DesMon
		,		SimboloMoneda = a.SimMon
		,		DecimalesPrecioMoneda = convert(int, a.DecMonPre)
		FROM	['+@pv_BaseDatos+'].softland.cwtmone a
	'

	EXEC (@query)
END