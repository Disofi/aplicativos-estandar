﻿USE [DSNotaVenta]
GO
  
alter Procedure [dbo].[JS_ListarNVDETALLENM]  
  @nvId INT,  
  @pv_BaseDatos varchar(100)  
  as  
  BEGIN  
  DECLARE @query varchar(max)  
  SELECT @query = ''  
-- ==========================================================================================  
-----------------Lista detalle  nota de venta según NVNumero-------------------------  
-- ==========================================================================================  
  SELECT @query = @query + '  
   SELECT  
    a.nvLinea,  
    a.CodProd,  
    DesProd = isnull(b.DesProd, c.DesProd),  
    a.nvCant,  
    a.CodUMed,  
    a.nvPrecio,  
    a.nvSubTotal,  
    a.NVNumero,  
    ROUND(a.nvDPorcDesc01,0) as nvDPorcDesc01,  
    ROUND(a.nvDPorcDesc02,0) as nvDPorcDesc02,  
    ROUND(a.nvDPorcDesc03,0) as nvDPorcDesc03,  
    ROUND(a.nvDPorcDesc04,0) as nvDPorcDesc04,  
    ROUND(a.nvDPorcDesc05,0) as nvDPorcDesc05,  
    ROUND(a.nvDDescto01,0) as nvDDescto01,  
    ROUND(a.nvDDescto02,0) as nvDDescto02,  
    ROUND(a.nvDDescto03,0) as nvDDescto03,  
    ROUND(a.nvDDescto04,0) as nvDDescto04,  
    ROUND(a.nvDDescto05,0) as nvDDescto05,  
    a.nvTotLinea  
   FROM  [dbo].[DS_NotasVentaDetalle] a 
		left JOIN ['+@pv_BaseDatos+'].[softland].[iw_tprod] b 
			ON (a.CodProd = b.CodProd collate Modern_Spanish_CI_AS)  
		left JOIN DS_ProductosNuevos c
			ON (a.CodProd collate Modern_Spanish_CI_AS = c.CodProd) 
   WHERE  
    a.IdNotaVenta = '+convert(varchar(100),@nvId)+'  
   ORDER BY  
    a.nvLinea  
    '  
    EXEC (@query)  
  END  