﻿USE [DSNotaVenta]
GO
CREATE TABLE DS_ProductosNuevos
(
	Id INT IDENTITY(1, 1)
,	BaseDatos	varchar	(200)
,	CodProd	varchar	(20)
,	DesProd	varchar	(60)
,	DesProd2	varchar	(60)
,	CodRapido	varchar	(3)
,	CodBarra	varchar	(20)
,	CodUMed	varchar	(6)
,	Origen	int
,	CodMonOrig	varchar	(2)
,	CodGrupo	varchar	(10)
,	CodSubGr	varchar	(10)
,	CodCateg	varchar	(3)
,	CodMonPVta	varchar	(2)
,	PrecioVta	float
,	PrecioBol	float
,	FichaTec	int
,	EsConfig	int
,	FactorConfig	float
,	Impuesto	int
,	Inventariable	int
,	EsSerie	int
,	EsTallaColor	int
,	EsPartida	int
,	EsCaducidad	int
,	EsPieza	int
,	CantPieza	int
,	PesoKgs	float
,	CtaActivo	varchar	(18)
,	CtaVentas	varchar	(18)
,	CtaGastos	varchar	(18)
,	CtaCosto	varchar	(18)
,	FecUltCom	datetime
,	ValorUltCom	float
,	CostoRep	float
,	FecCostoRep	datetime
,	FecCMonet	datetime
,	ValorCMonet	float
,	NivMin	float
,	NivRep	float
,	NivMax	float
,	Inamovible	int
,	ManejaDim	int
,	Ancho	float
,	esUbicPar	int
,	CtaDevolucion	varchar(18)
,	TipProd	varchar(2)
,	esParaVenta	int
,	esParaCompra	int
,	EsTalla	int
,	EsColor	int
,	MetodoCosteo	varchar(1)
,	CodUMedVta1	varchar(6)
,	EquivUMVta1	float
,	PrecioVtaUM1	float
,	PrecioBolUM1	float
,	CodUMedVta2	varchar(6)
,	EquivUMVta2	float
,	PrecioVtaUM2	float
,	PrecioBolUM2	float
,	UMDefecto	int
,	ManProdAnticipo	int
,	ImprimeEnBoleta	int
,	EsParaAutoservicio	int
,	Inactivo	int
,	Usuario	varchar(8)
,	Proceso	varchar(50)
,	FechaUlMod	datetime
)